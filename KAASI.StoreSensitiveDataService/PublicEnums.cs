﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.StoreSensitiveDataService
{
    public enum CryptoTypes
    {
        encTypeDES = 0,
        encTypeRC2,
        encTypeRijndael,
        encTypeTripleDES
    }
}
