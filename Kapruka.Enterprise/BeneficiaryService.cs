﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class BeneficiaryService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public BeneficiaryService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Beneficiary entity)
        {
            this._unitOfWork.BeneficiaryRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Beneficiary entity)
        {
            this._unitOfWork.BeneficiaryRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Beneficiary entity)
        {
            this._unitOfWork.BeneficiaryRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Beneficiary> GetAll()
        {
            return this._unitOfWork.BeneficiaryRepository.GetAll();
        }

        public IEnumerable<Beneficiary> GetAll(Expression<Func<Beneficiary, bool>> filter, Func<IQueryable<Beneficiary>, IOrderedQueryable<Beneficiary>> orderBy, string includeProperties)
        {
            return this._unitOfWork.BeneficiaryRepository.Get(filter, orderBy, includeProperties);
        }
       
    }
}
