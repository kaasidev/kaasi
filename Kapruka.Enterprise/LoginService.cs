﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class LoginService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public LoginService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Login entity)
        {
            this._unitOfWork.LoginRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Login entity)
        {
            this._unitOfWork.LoginRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Login entity)
        {
            this._unitOfWork.LoginRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }
        public Login GetCustomerById(int id)
        {
            return this._unitOfWork.LoginRepository.GetByID(id);
        }

        public Login GetLoginByName(string userName)
        {

            var login = this._unitOfWork.LoginRepository.GetAll().Where(x => x.Username == userName).SingleOrDefault();

            return login;
        }

        public Login GetLoginByEmail(string userName, string transactionType, string Password)
        {
            var login = this._unitOfWork.LoginRepository.GetAll().Where(x => x.Username == userName
                && x.TransactionDirection == transactionType && x.Password == Password).SingleOrDefault();

            return login;
        }

        public IEnumerable<Login> GetAll()
        {
            return this._unitOfWork.LoginRepository.GetAll();
        }

        public IEnumerable<Login> GetAll(Expression<Func<Login, bool>> filter,
            Func<IQueryable<Login>, IOrderedQueryable<Login>> orderBy, string includeProperties)
        {
            return this._unitOfWork.LoginRepository.Get(filter, orderBy, includeProperties);
        }

        public IEnumerable<Login> GetGivenCustomer(int custId)
        {
            string sql = "select * from Customers where  CustomerID=" + custId;
            return this.entities.Database.SqlQuery(typeof(Login), sql, new object[0]).Cast<Login>();
        }
    }
}
