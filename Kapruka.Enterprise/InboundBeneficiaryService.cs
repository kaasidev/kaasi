﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class InboundBeneficiaryService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public InboundBeneficiaryService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(InboundIFTIBeneficiary entity)
        {
            this._unitOfWork.InboundIFTIBeneficiaryRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(InboundIFTIBeneficiary entity)
        {
            this._unitOfWork.InboundIFTIBeneficiaryRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(InboundIFTIBeneficiary entity)
        {
            this._unitOfWork.InboundIFTIBeneficiaryRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<InboundIFTIBeneficiary> GetAll()
        {
            return this._unitOfWork.InboundIFTIBeneficiaryRepository.GetAll();
        }

        public IEnumerable<InboundIFTIBeneficiary> GetAll(Expression<Func<InboundIFTIBeneficiary, bool>> filter, Func<IQueryable<InboundIFTIBeneficiary>, IOrderedQueryable<InboundIFTIBeneficiary>> orderBy, string includeProperties)
        {
            return this._unitOfWork.InboundIFTIBeneficiaryRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
