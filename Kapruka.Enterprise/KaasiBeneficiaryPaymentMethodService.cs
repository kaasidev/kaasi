﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class KaasiBeneficiaryPaymentMethodService:ServiceBase<BeneficiaryPaymentMethod>
    {

        public KaasiBeneficiaryPaymentMethodService(KaasiEntitiesDbContext repository) : base(repository)
        {

        }

        public IList<BeneficiaryPaymentMethod> GetBeneficiaryPaymentMethodByCustomerAndBeneFiciaryId
            (int customerID,string beneficiaryId)
        {

            var query = (from bpm in kaasicontext.DbEntities.BeneficiaryPaymentMethods
                         where bpm.CustomerID == customerID && bpm.BeneficiaryID == beneficiaryId
                         select bpm).ToList();

            return query;
        }
    }
}
