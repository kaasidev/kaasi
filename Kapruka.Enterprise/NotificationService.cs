﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class NotificationService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public NotificationService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Notification entity)
        {
            this._unitOfWork.NotificationRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Notification entity)
        {
            this._unitOfWork.NotificationRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Notification entity)
        {
            this._unitOfWork.NotificationRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Notification> GetAll()
        {
            return this._unitOfWork.NotificationRepository.GetAll();
        }

        public IEnumerable<Notification> GetAll(Expression<Func<Notification, bool>> filter, Func<IQueryable<Notification>, IOrderedQueryable<Notification>> orderBy, string includeProperties)
        {
            return this._unitOfWork.NotificationRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
