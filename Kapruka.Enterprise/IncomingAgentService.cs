﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace Kapruka.Enterprise
{
  public  class IncomingAgentService
    {
       private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public IncomingAgentService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(IncomingAgent entity)
        {
            this._unitOfWork.IncomingAgentRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(IncomingAgent entity)
        {
            this._unitOfWork.IncomingAgentRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(IncomingAgent entity)
        {
            this._unitOfWork.AgentRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<IncomingAgent> GetAll()
        {
            return this._unitOfWork.IncomingAgentRepository.GetAll();
        }

        public IEnumerable<IncomingAgent> GetAll(Expression<Func<IncomingAgent, bool>> filter, Func<IQueryable<IncomingAgent>, IOrderedQueryable<IncomingAgent>> orderBy, string includeProperties)
        {
            return this._unitOfWork.IncomingAgentRepository.Get(filter, orderBy, includeProperties);
        }
       
    }
}


