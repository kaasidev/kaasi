﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class SettingsService
    {
              private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public SettingsService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Setting entity)
        {
            this._unitOfWork.SettingRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Setting entity)
        {
            this._unitOfWork.SettingRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Setting entity)
        {
            this._unitOfWork.SettingRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Setting> GetAll()
        {
            return this._unitOfWork.SettingRepository.GetAll();
        }

        public IEnumerable<Setting> GetAll(Expression<Func<Setting, bool>> filter, Func<IQueryable<Setting>, IOrderedQueryable<Setting>> orderBy, string includeProperties)
        {
            return this._unitOfWork.SettingRepository.Get(filter, orderBy, includeProperties);
        }
       
    }
}
