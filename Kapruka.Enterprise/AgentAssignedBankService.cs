﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AgentAssignedBankService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentAssignedBankService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentAssignedBank entity)
        {
            this._unitOfWork.AgentAssignedBankRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentAssignedBank entity)
        {
            this._unitOfWork.AgentAssignedBankRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentAssignedBank entity)
        {
            this._unitOfWork.AgentAssignedBankRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentAssignedBank> GetAll()
        {
            return this._unitOfWork.AgentAssignedBankRepository.GetAll();
        }

        public IEnumerable<AgentAssignedBank> GetAll(Expression<Func<AgentAssignedBank, bool>> filter, Func<IQueryable<AgentAssignedBank>, IOrderedQueryable<AgentAssignedBank>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentAssignedBankRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
