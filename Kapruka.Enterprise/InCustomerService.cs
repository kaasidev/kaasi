﻿
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Kapruka.Enterprise
{
    public class InCustomerService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public InCustomerService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(InCustomerService entity)
        {
            // this._unitOfWork.inc.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(InCustomer entity)
        {
            this._unitOfWork.InCustomerRepositoryRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(InCustomer entity)
        {
            this._unitOfWork.InCustomerRepositoryRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<InCustomer> GetAll()
        {
            return this._unitOfWork.InCustomerRepositoryRepository.GetAll();
        }

        public IEnumerable<InCustomer> GetAll(Expression<Func<InCustomer, bool>> filter, Func<IQueryable<InCustomer>, IOrderedQueryable<InCustomer>> orderBy, string includeProperties)
        {
            return this._unitOfWork.InCustomerRepositoryRepository.Get(filter, orderBy, includeProperties);
        }

    }
}



