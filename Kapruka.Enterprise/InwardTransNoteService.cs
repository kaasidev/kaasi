﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class InwardTransNoteService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public InwardTransNoteService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(InwardTransNote entity)
        {
            this._unitOfWork.InwardTransNoteRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(InwardTransNote entity)
        {
            this._unitOfWork.InwardTransNoteRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(InwardTransNote entity)
        {
            this._unitOfWork.InwardTransNoteRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<InwardTransNote> GetAll()
        {
            return this._unitOfWork.InwardTransNoteRepository.GetAll();
        }

        public IEnumerable<InwardTransNote> GetAll(Expression<Func<InwardTransNote, bool>> filter, Func<IQueryable<InwardTransNote>, IOrderedQueryable<InwardTransNote>> orderBy, string includeProperties)
        {
            return this._unitOfWork.InwardTransNoteRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
