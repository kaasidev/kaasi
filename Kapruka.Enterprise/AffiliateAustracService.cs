﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AffiliateAustracService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AffiliateAustracService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AffiliateAustracRecord entity)
        {
            this._unitOfWork.AffiliateAustracRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AffiliateAustracRecord entity)
        {
            this._unitOfWork.AffiliateAustracRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AffiliateAustracRecord entity)
        {
            this._unitOfWork.AffiliateAustracRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AffiliateAustracRecord> GetAll()
        {
            return this._unitOfWork.AffiliateAustracRepository.GetAll();
        }

        public IEnumerable<AffiliateAustracRecord> GetAll(Expression<Func<AffiliateAustracRecord, bool>> filter, Func<IQueryable<AffiliateAustracRecord>, IOrderedQueryable<AffiliateAustracRecord>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AffiliateAustracRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
