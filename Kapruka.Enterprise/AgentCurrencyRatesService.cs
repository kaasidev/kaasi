﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AgentCurrencyRatesService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentCurrencyRatesService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentCurrencyRate entity)
        {
            this._unitOfWork.AgentCurrencyRateRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentCurrencyRate entity)
        {
            this._unitOfWork.AgentCurrencyRateRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentCurrencyRate entity)
        {
            this._unitOfWork.AgentCurrencyRateRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentCurrencyRate> GetAll()
        {
            return this._unitOfWork.AgentCurrencyRateRepository.GetAll();
        }

        public IEnumerable<AgentCurrencyRate> GetAll(Expression<Func<AgentCurrencyRate, bool>> filter, Func<IQueryable<AgentCurrencyRate>, IOrderedQueryable<AgentCurrencyRate>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentCurrencyRateRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
