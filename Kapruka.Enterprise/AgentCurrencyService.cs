﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AgentCurrencyService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentCurrencyService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentCurrency entity)
        {
            this._unitOfWork.AgentCurrencyRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentCurrency entity)
        {
            this._unitOfWork.AgentCurrencyRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentCurrency entity)
        {
            this._unitOfWork.AgentCurrencyRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentCurrency> GetAll()
        {
            return this._unitOfWork.AgentCurrencyRepository.GetAll();
        }

        public IEnumerable<AgentCurrency> GetAll(Expression<Func<AgentCurrency, bool>> filter, Func<IQueryable<AgentCurrency>, IOrderedQueryable<AgentCurrency>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentCurrencyRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
