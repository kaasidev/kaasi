﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class KaasiBankService : ServiceBase<Bank>
    {
        public KaasiBankService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }

        public Bank GetBankByName(string name)
        {
            var bank = (from bn in kaasicontext.DbEntities.Banks where bn.BankName == name select bn).SingleOrDefault();
            return bank;
        }

        public string GetBankBranchCode(string bankName, string branchName)
        {
            var branchId = "";
            var branch = (from ba in kaasicontext.DbEntities.Banks
                          join br in kaasicontext.DbEntities.BankBranches
             on ba.BankCode equals (br.BankCode)
                          where ba.BankName == bankName && br.BranchName == branchName
                          select br).SingleOrDefault();
            if (branch != null)
            {
                branchId = branch.BranchCode;
            }
            return branchId;
        }
    }
}
