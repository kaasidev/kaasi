﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class KaasiCurrencyService : ServiceBase<Currency>
    {

        public KaasiCurrencyService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }

        public Currency GetCurrencyByCurrencyCode(string currencyCode)
        {
            var currency = (from cr in kaasicontext.DbEntities.Currencies
                            where cr.CurrencyCode == currencyCode select cr).SingleOrDefault();
            return currency;
        }
    }
}
