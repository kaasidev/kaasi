﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class KaasiCustomerService : ServiceBase<Customer>
    {

        public KaasiCustomerService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }

        public int GetMaxCustomerId()
        {
          var lastCustomerID=  (from cs in kaasicontext.DbEntities.Customers select cs.CustomerID).Max()
                .GetValueOrDefault();
            return lastCustomerID;
        }

        public Customer GetCustomerByFirstNameAndLastNameDOB(string firstName,string lastName,string dob)
        {

            var query = (from cs in kaasicontext.DbEntities.Customers where cs.FirstName == firstName && cs.LastName == lastName && cs.DOB == dob select cs).SingleOrDefault();

            return query;
        }

    }
}
