﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AgentNotificationService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentNotificationService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentNotification entity)
        {
            this._unitOfWork.AgentNotificationRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentNotification entity)
        {
            this._unitOfWork.AgentNotificationRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentNotification entity)
        {
            this._unitOfWork.AgentNotificationRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentNotification> GetAll()
        {
            return this._unitOfWork.AgentNotificationRepository.GetAll();
        }

        public IEnumerable<AgentNotification> GetAll(Expression<Func<AgentNotification, bool>> filter, Func<IQueryable<AgentNotification>, IOrderedQueryable<AgentNotification>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentNotificationRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
