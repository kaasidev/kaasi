﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AgentCurrencyMarginService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentCurrencyMarginService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentCurrencyMargin entity)
        {
            this._unitOfWork.AgentCurrencyMarginRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentCurrencyMargin entity)
        {
            this._unitOfWork.AgentCurrencyMarginRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentCurrencyMargin entity)
        {
            this._unitOfWork.AgentCurrencyMarginRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentCurrencyMargin> GetAll()
        {
            return this._unitOfWork.AgentCurrencyMarginRepository.GetAll();
        }

        public IEnumerable<AgentCurrencyMargin> GetAll(Expression<Func<AgentCurrencyMargin, bool>> filter, Func<IQueryable<AgentCurrencyMargin>, IOrderedQueryable<AgentCurrencyMargin>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentCurrencyMarginRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
