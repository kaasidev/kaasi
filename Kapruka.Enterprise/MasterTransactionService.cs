﻿using Kapruka.PublicModels;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class MasterTransactionService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public MasterTransactionService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Transaction entity)
        {
            this._unitOfWork.TransactionRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Transaction entity)
        {
            this._unitOfWork.TransactionRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Transaction entity)
        {
            this._unitOfWork.TransactionRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Transaction> GetAll()
        {
            return this._unitOfWork.TransactionRepository.GetAll();
        }

        public IEnumerable<Transaction> GetAll(Expression<Func<Transaction, bool>> filter, Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>> orderBy, string includeProperties)
        {
            return this._unitOfWork.TransactionRepository.Get(filter, orderBy, includeProperties);
        }

        public void UpdateTransactionTable(string fileName)
        {
            string sql = "update Transactions set InsertedIntoAustracFileByMaster='Y',InsertedIntoAustracFileNameByMaster='" + fileName + "',InsertedIntoAustracFileDateTimeByMaster=getdate() where InsertedIntoAustracFileByMaster='N' ";
            this.entities.Database.ExecuteSqlCommand(sql, null);
        }
    }
}
