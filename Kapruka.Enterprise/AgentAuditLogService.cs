﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AgentAuditLogService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentAuditLogService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentAuditLog entity)
        {
            this._unitOfWork.AgentAuditLogRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentAuditLog entity)
        {
            this._unitOfWork.AgentAuditLogRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentAuditLog entity)
        {
            this._unitOfWork.AgentAuditLogRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentAuditLog> GetAll()
        {
            return this._unitOfWork.AgentAuditLogRepository.GetAll();
        }

        public IEnumerable<AgentAuditLog> GetAll(Expression<Func<AgentAuditLog, bool>> filter, Func<IQueryable<AgentAuditLog>, IOrderedQueryable<AgentAuditLog>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentAuditLogRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
