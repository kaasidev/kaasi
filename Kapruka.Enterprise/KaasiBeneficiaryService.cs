﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class KaasiBeneficiaryService : ServiceBase<Beneficiary>
    {

        public KaasiBeneficiaryService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }

        public IList<Beneficiary> GetBeneficiariesBYCustomerId(int customerId)
        {
            var query = (from cs in kaasicontext.DbEntities.Beneficiaries
                         where cs.CustomerID == customerId
                         select cs).ToList();

            return query;

        }
    }
}
