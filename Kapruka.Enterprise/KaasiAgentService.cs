﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kapruka.Repository;
namespace Kapruka.Enterprise
{
   public class KaasiAgentService : ServiceBase<Agent>
    {

        public KaasiAgentService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }

        public Agent GetAgentByAgentId(int agentId)
        {
            var agent = (from ag in kaasicontext.DbEntities.Agents
                         where ag.AgentID == agentId
                         select ag).SingleOrDefault();
            return agent;
        }

    }
}
