﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class InMasterAustracRecordService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public InMasterAustracRecordService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(InMasterAustracRecord entity)
        {
            this._unitOfWork.InMasterAustracRecordRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(InMasterAustracRecord entity)
        {
            this._unitOfWork.InMasterAustracRecordRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(InMasterAustracRecord entity)
        {
            this._unitOfWork.InMasterAustracRecordRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<InMasterAustracRecord> GetAll()
        {
            return this._unitOfWork.InMasterAustracRecordRepository.GetAll();
        }

        public IEnumerable<InMasterAustracRecord> GetAll(Expression<Func<InMasterAustracRecord, bool>> filter, Func<IQueryable<InMasterAustracRecord>, IOrderedQueryable<InMasterAustracRecord>> orderBy, string includeProperties)
        {
            return this._unitOfWork.InMasterAustracRecordRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
