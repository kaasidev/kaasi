﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class AgentAssignedCountryService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentAssignedCountryService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentAssignedCountry entity)
        {
            this._unitOfWork.AgentAssignedCountryRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentAssignedCountry entity)
        {
            this._unitOfWork.AgentAssignedCountryRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentAssignedCountry entity)
        {
            this._unitOfWork.AgentAssignedCountryRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentAssignedCountry> GetAll()
        {
            return this._unitOfWork.AgentAssignedCountryRepository.GetAll();
        }

        public IEnumerable<AgentAssignedCountry> GetAll(Expression<Func<AgentAssignedCountry, bool>> filter, Func<IQueryable<AgentAssignedCountry>, IOrderedQueryable<AgentAssignedCountry>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentAssignedCountryRepository.Get(filter, orderBy, includeProperties);
        }
       
    }
}
