﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class CustomerService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public CustomerService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Customer entity)
        {
            this._unitOfWork.CustomerRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Customer entity)
        {
            this._unitOfWork.CustomerRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Customer entity)
        {
            this._unitOfWork.CustomerRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }
        public Customer GetCustomerById(int id)
        {
            return this._unitOfWork.CustomerRepository.GetByID(id);
        }

        public IEnumerable<Customer> GetAll()
        {
            return this._unitOfWork.CustomerRepository.GetAll();
        }

        public IEnumerable<Customer> GetAll(Expression<Func<Customer, bool>> filter, Func<IQueryable<Customer>, IOrderedQueryable<Customer>> orderBy, string includeProperties)
        {
            return this._unitOfWork.CustomerRepository.Get(filter, orderBy, includeProperties);
        }
        public IEnumerable<Customer> GetGivenCustomer(int custId)
        {
            string sql = "select * from Customers where  CustomerID=" + custId;
            return this.entities.Database.SqlQuery(typeof(Customer), sql, new object[0]).Cast<Customer>();
        }
    }
}
