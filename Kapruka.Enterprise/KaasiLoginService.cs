﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kapruka.Repository;
namespace Kapruka.Enterprise
{
   public class KaasiLoginService : ServiceBase<Login>
    {

        public KaasiLoginService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }

        public Login GetuserByName(string userName)
        {
            var login = (from ag in kaasicontext.DbEntities.Logins
                         where ag.Username == userName
                         select ag).SingleOrDefault();
            return login;
        }
    }
}
