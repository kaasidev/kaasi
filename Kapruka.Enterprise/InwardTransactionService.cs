﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class InwardTransactionService
    {
         private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public InwardTransactionService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(InboundTransaction entity)
        {
            this._unitOfWork.InBoundTransactionRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(InboundTransaction entity)
        {
            this._unitOfWork.InBoundTransactionRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(InboundTransaction entity)
        {
            this._unitOfWork.InBoundTransactionRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<InboundTransaction> GetAll()
        {
            return this._unitOfWork.InBoundTransactionRepository.GetAll();
        }

        public IEnumerable<InboundTransaction> GetAll(Expression<Func<InboundTransaction, bool>> filter, Func<IQueryable<InboundTransaction>, IOrderedQueryable<InboundTransaction>> orderBy, string includeProperties)
        {
            return this._unitOfWork.InBoundTransactionRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
