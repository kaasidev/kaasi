﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class AgentForeignBankSettingService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentForeignBankSettingService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(AgentForeignBankSetting entity)
        {
            this._unitOfWork.AgentForeignBankSettingRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(AgentForeignBankSetting entity)
        {
            this._unitOfWork.AgentForeignBankSettingRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(AgentForeignBankSetting entity)
        {
            this._unitOfWork.AgentForeignBankSettingRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<AgentForeignBankSetting> GetAll()
        {
            return this._unitOfWork.AgentForeignBankSettingRepository.GetAll();
        }

        public IEnumerable<AgentForeignBankSetting> GetAll(Expression<Func<AgentForeignBankSetting, bool>> filter, Func<IQueryable<AgentForeignBankSetting>, IOrderedQueryable<AgentForeignBankSetting>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentForeignBankSettingRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
