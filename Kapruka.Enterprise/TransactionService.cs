﻿using Kapruka.PublicModels;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class TransactionService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public TransactionService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Transaction entity)
        {
            this._unitOfWork.TransactionRepository.Add(entity);
            this._unitOfWork.Save();
            var er = this.entities.GetValidationErrors();
            return true;
        }



        public bool Update(Transaction entity)
        {
            this._unitOfWork.TransactionRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Transaction entity)
        {
            this._unitOfWork.TransactionRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Transaction> GetAll()
        {
            return this._unitOfWork.TransactionRepository.GetAll();
        }

        public IEnumerable<Transaction> GetAll(Expression<Func<Transaction, bool>> filter, Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>> orderBy, string includeProperties)
        {
            return this._unitOfWork.TransactionRepository.Get(filter, orderBy, includeProperties);
        }

        public IEnumerable<Transaction> UpdateTransactionTable(string fileName)
        {
            string sql = "update Transactions set InsertedIntoAustracFileByMaster='Y',InsertedIntoAustracFileNameByMaster='" + fileName + "',InsertedIntoAustracFileDateTimeByMaster=getdate() where InsertedIntoAustracFileByMaster='N' " +
                       " select * from Transactions ";
            return this.entities.Database.SqlQuery(typeof(Transaction), sql, new object[0]).Cast<Transaction>();
        }
        public IEnumerable<Transaction> UpdateTransactionTableWithAgent(string fileName,int agentId)
        {
            string sql = "update Transactions set InsertedIntoAustracFile='Y',InsertedIntoAustracFileName='" + fileName + "',InsertedIntoAustracFileDateTime=getdate() where InsertedIntoAustracFile='N' and AgentID=" + agentId+""+
                " select * from Transactions ";
            return this.entities.Database.SqlQuery(typeof(Transaction), sql, new object[0]).Cast<Transaction>();
           // this.entities.Database.ExecuteSqlCommand(sql, null);
        }

        public Transaction GetTransactionById(int id)
        {
            return this._unitOfWork.TransactionRepository.GetByID(id);
        }

    }
}
