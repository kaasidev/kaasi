﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class CustomerDocumentService
    {
       private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public CustomerDocumentService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(CustomerDocument entity)
        {
            this._unitOfWork.CustomerDocumentRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(CustomerDocument entity)
        {
            this._unitOfWork.CustomerDocumentRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(CustomerDocument entity)
        {
            this._unitOfWork.CustomerDocumentRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<CustomerDocument> GetAll()
        {
            return this._unitOfWork.CustomerDocumentRepository.GetAll();
        }

        public IEnumerable<CustomerDocument> GetAll(Expression<Func<CustomerDocument, bool>> filter, Func<IQueryable<CustomerDocument>, IOrderedQueryable<CustomerDocument>> orderBy, string includeProperties)
        {
            return this._unitOfWork.CustomerDocumentRepository.Get(filter, orderBy, includeProperties);
        }

        public IEnumerable<CustomerDocument> GetDocumentList(int custId)
        {
            string sql = "SET DATEFORMAT dmy select * from CustomerDocuments where convert(datetime,ExpiryDate) > getdate() and CustomerID=" + custId;
            return this.entities.Database.SqlQuery(typeof(CustomerDocument), sql, new object[0]).Cast<CustomerDocument>();
        }
       
    }
}
