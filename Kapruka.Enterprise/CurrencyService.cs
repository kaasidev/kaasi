﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class CurrencyService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public CurrencyService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Currency entity)
        {
            this._unitOfWork.CurrencyRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Currency entity)
        {
            this._unitOfWork.CurrencyRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Currency entity)
        {
            this._unitOfWork.CurrencyRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Currency> GetAll()
        {
            return this._unitOfWork.CurrencyRepository.GetAll();
        }

        public IEnumerable<Currency> GetAll(Expression<Func<Currency, bool>> filter, Func<IQueryable<Currency>, IOrderedQueryable<Currency>> orderBy, string includeProperties)
        {
            return this._unitOfWork.CurrencyRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
