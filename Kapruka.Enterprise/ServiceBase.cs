﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
  public  class ServiceBase<T> where T : class
    {
        internal KaasiEntitiesDbContext kaasicontext;

        public ServiceBase(KaasiEntitiesDbContext repository)
        {
            this.kaasicontext = repository;
        }

        //public ServiceBase()
        //{
        //this.RepositoryBurpDaddy = new Repository(EntityContext.GetCurrentContext());
        //}

        public T Get(object id)
        {
            return kaasicontext.DbEntities.Set<T>().Find(id);
        }

        public IList<T> GetAll()
        {
            return kaasicontext.DbEntities.Set<T>().ToList();
        }

        public virtual void Insert(T entity)
        {
            kaasicontext.DbEntities.Set<T>().Add(entity);
            kaasicontext.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            //do nothing as ef saves when savechanges method is called.
            if (kaasicontext.DbEntities.Entry(entity).State == System.Data.Entity.EntityState.Detached)
                kaasicontext.DbEntities.Set<T>().Attach(entity);
            kaasicontext.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            kaasicontext.DbEntities.Set<T>().Remove(entity);
            kaasicontext.SaveChanges();
        }
    }
}
