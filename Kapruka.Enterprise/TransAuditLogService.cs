﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class TransAuditLogService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public TransAuditLogService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(TransAuditLog entity)
        {
            this._unitOfWork.TransAuditLogRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(TransAuditLog entity)
        {
            this._unitOfWork.TransAuditLogRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(TransAuditLog entity)
        {
            this._unitOfWork.TransAuditLogRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<TransAuditLog> GetAll()
        {
            return this._unitOfWork.TransAuditLogRepository.GetAll();
        }

        public IEnumerable<TransAuditLog> GetAll(Expression<Func<TransAuditLog, bool>> filter, Func<IQueryable<TransAuditLog>, IOrderedQueryable<TransAuditLog>> orderBy, string includeProperties)
        {
            return this._unitOfWork.TransAuditLogRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
