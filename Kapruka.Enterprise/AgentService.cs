﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
  public  class AgentService
    {
       private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public AgentService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Agent entity)
        {
            this._unitOfWork.AgentRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Agent entity)
        {
            this._unitOfWork.AgentRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Agent entity)
        {
            this._unitOfWork.AgentRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Agent> GetAll()
        {
            return this._unitOfWork.AgentRepository.GetAll();
        }

        public IEnumerable<Agent> GetAll(Expression<Func<Agent, bool>> filter, Func<IQueryable<Agent>, IOrderedQueryable<Agent>> orderBy, string includeProperties)
        {
            return this._unitOfWork.AgentRepository.Get(filter, orderBy, includeProperties);
        }
       
    }
}
