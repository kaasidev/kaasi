﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class MasterAustracRecordService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public MasterAustracRecordService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(MasterAustracRecord entity)
        {
            this._unitOfWork.MasterAustracRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(MasterAustracRecord entity)
        {
            this._unitOfWork.MasterAustracRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(MasterAustracRecord entity)
        {
            this._unitOfWork.MasterAustracRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<MasterAustracRecord> GetAll()
        {
            return this._unitOfWork.MasterAustracRepository.GetAll();
        }

        public IEnumerable<MasterAustracRecord> GetAll(Expression<Func<MasterAustracRecord, bool>> filter, Func<IQueryable<MasterAustracRecord>, IOrderedQueryable<MasterAustracRecord>> orderBy, string includeProperties)
        {
            return this._unitOfWork.MasterAustracRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
