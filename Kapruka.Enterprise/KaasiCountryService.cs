﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
  public  class KaasiCountryService : ServiceBase<Country>
    {

        public KaasiCountryService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }

        public Country GetCountryByName(string name)
        {
            var country = (from cs in kaasicontext.DbEntities.Countries where cs.CountryName == name select cs).SingleOrDefault();
            return country;

        }

        public Country GetCouontryByCurrencyCode(string currencyCode)
        {
            var country = (from cs in kaasicontext.DbEntities.Countries where cs.MasterCurrencyCode == currencyCode select cs).SingleOrDefault();
            return country;
        }
    }
}
