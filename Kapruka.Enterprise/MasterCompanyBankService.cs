﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class MasterCompanyBankService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public MasterCompanyBankService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(MasterCompanyBank entity)
        {
            this._unitOfWork.MasterCompanyBankRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(MasterCompanyBank entity)
        {
            this._unitOfWork.MasterCompanyBankRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(MasterCompanyBank entity)
        {
            this._unitOfWork.MasterCompanyBankRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<MasterCompanyBank> GetAll()
        {
            return this._unitOfWork.MasterCompanyBankRepository.GetAll();
        }

        public IEnumerable<MasterCompanyBank> GetAll(Expression<Func<MasterCompanyBank, bool>> filter, Func<IQueryable<MasterCompanyBank>, IOrderedQueryable<MasterCompanyBank>> orderBy, string includeProperties)
        {
            return this._unitOfWork.MasterCompanyBankRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
