﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class SettingAuditLogService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public SettingAuditLogService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(SettingAuditLog entity)
        {
            this._unitOfWork.SettingAuditLogRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(SettingAuditLog entity)
        {
            this._unitOfWork.SettingAuditLogRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(SettingAuditLog entity)
        {
            this._unitOfWork.SettingAuditLogRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<SettingAuditLog> GetAll()
        {
            return this._unitOfWork.SettingAuditLogRepository.GetAll();
        }

        public IEnumerable<SettingAuditLog> GetAll(Expression<Func<SettingAuditLog, bool>> filter, Func<IQueryable<SettingAuditLog>, IOrderedQueryable<SettingAuditLog>> orderBy, string includeProperties)
        {
            return this._unitOfWork.SettingAuditLogRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
