﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class CustAuditLogService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public CustAuditLogService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(CustAuditLog entity)
        {
            this._unitOfWork.CustAuditLogRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(CustAuditLog entity)
        {
            this._unitOfWork.CustAuditLogRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(CustAuditLog entity)
        {
            this._unitOfWork.CustAuditLogRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<CustAuditLog> GetAll()
        {
            return this._unitOfWork.CustAuditLogRepository.GetAll();
        }

        public IEnumerable<CustAuditLog> GetAll(Expression<Func<CustAuditLog, bool>> filter, Func<IQueryable<CustAuditLog>, IOrderedQueryable<CustAuditLog>> orderBy, string includeProperties)
        {
            return this._unitOfWork.CustAuditLogRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
