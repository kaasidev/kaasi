﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class BankService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public BankService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Bank entity)
        {
            this._unitOfWork.BankRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Bank entity)
        {
            this._unitOfWork.BankRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Bank entity)
        {
            this._unitOfWork.BankRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Bank> GetAll()
        {
            return this._unitOfWork.BankRepository.GetAll();
        }

        public IEnumerable<Bank> GetAll(Expression<Func<Bank, bool>> filter, Func<IQueryable<Bank>, IOrderedQueryable<Bank>> orderBy, string includeProperties)
        {
            return this._unitOfWork.BankRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
