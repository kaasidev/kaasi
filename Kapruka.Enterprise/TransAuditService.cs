﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class TransAuditService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public TransAuditService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(TransAuditLog entity)
        {
            this._unitOfWork.TransAuditRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(TransAuditLog entity)
        {
            this._unitOfWork.TransAuditRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(TransAuditLog entity)
        {
            this._unitOfWork.AgentRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<TransAuditLog> GetAll()
        {
            return this._unitOfWork.TransAuditRepository.GetAll();
        }

        public IEnumerable<TransAuditLog> GetAll(Expression<Func<TransAuditLog, bool>> filter, Func<IQueryable<TransAuditLog>, IOrderedQueryable<TransAuditLog>> orderBy, string includeProperties)
        {
            return this._unitOfWork.TransAuditRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
