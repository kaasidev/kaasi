﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
  public class KaasiBankBranchService : ServiceBase<BankBranch>
    {
        public KaasiBankBranchService(KaasiEntitiesDbContext repository) : base(repository)

        {
        }
    }

}
