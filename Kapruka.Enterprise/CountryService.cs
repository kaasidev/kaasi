﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class CountryService
    {
       private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public CountryService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(Country entity)
        {
            this._unitOfWork.CountryRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(Country entity)
        {
            this._unitOfWork.CountryRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(Country entity)
        {
            this._unitOfWork.CountryRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<Country> GetAll()
        {
            return this._unitOfWork.CountryRepository.GetAll();
        }

        public IEnumerable<Country> GetAll(Expression<Func<Country, bool>> filter, Func<IQueryable<Country>, IOrderedQueryable<Country>> orderBy, string includeProperties)
        {
            return this._unitOfWork.CountryRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
