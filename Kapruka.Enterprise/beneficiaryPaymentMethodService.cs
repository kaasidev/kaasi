﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
   public class beneficiaryPaymentMethodService
    {
       private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public beneficiaryPaymentMethodService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(BeneficiaryPaymentMethod entity)
        {
            this._unitOfWork.BeneficiaryPaymentMethodRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(BeneficiaryPaymentMethod entity)
        {
            this._unitOfWork.BeneficiaryPaymentMethodRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(BeneficiaryPaymentMethod entity)
        {
            this._unitOfWork.BeneficiaryPaymentMethodRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<BeneficiaryPaymentMethod> GetAll()
        {
            return this._unitOfWork.BeneficiaryPaymentMethodRepository.GetAll();
        }

        public IEnumerable<BeneficiaryPaymentMethod> GetAll(Expression<Func<BeneficiaryPaymentMethod, bool>> filter, Func<IQueryable<BeneficiaryPaymentMethod>, IOrderedQueryable<BeneficiaryPaymentMethod>> orderBy, string includeProperties)
        {
            return this._unitOfWork.BeneficiaryPaymentMethodRepository.Get(filter, orderBy, includeProperties);
        }
       
    }
}
