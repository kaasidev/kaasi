﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class CreditTransactionService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public CreditTransactionService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(CreditTransaction entity)
        {
            this._unitOfWork.CreditTransactionRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(CreditTransaction entity)
        {
            this._unitOfWork.CreditTransactionRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(CreditTransaction entity)
        {
            this._unitOfWork.CreditTransactionRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<CreditTransaction> GetAll()
        {
            return this._unitOfWork.CreditTransactionRepository.GetAll();
        }

        public IEnumerable<CreditTransaction> GetAll(Expression<Func<CreditTransaction, bool>> filter, Func<IQueryable<CreditTransaction>, IOrderedQueryable<CreditTransaction>> orderBy, string includeProperties)
        {
            return this._unitOfWork.CreditTransactionRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
