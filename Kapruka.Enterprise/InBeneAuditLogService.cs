﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Enterprise
{
    public class InBeneAuditLogService
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public InBeneAuditLogService(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(InBeneAuditLog entity)
        {
            this._unitOfWork.InBeneAuditLogRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(InBeneAuditLog entity)
        {
            this._unitOfWork.InBeneAuditLogRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(InBeneAuditLog entity)
        {
            this._unitOfWork.InBeneAuditLogRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<InBeneAuditLog> GetAll()
        {
            return this._unitOfWork.InBeneAuditLogRepository.GetAll();
        }

        public IEnumerable<InBeneAuditLog> GetAll(Expression<Func<InBeneAuditLog, bool>> filter, Func<IQueryable<InBeneAuditLog>, IOrderedQueryable<InBeneAuditLog>> orderBy, string includeProperties)
        {
            return this._unitOfWork.InBeneAuditLogRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
