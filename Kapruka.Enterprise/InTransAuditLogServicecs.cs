﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Kapruka.Enterprise
{
    public class InTransAuditLogServicecs
    {
        private UnitOfWorks _unitOfWork;
        private KaprukaEntities entities;

        public InTransAuditLogServicecs(UnitOfWorks unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.entities = new KaprukaEntities();
        }

        public bool Add(InTransAuditLog entity)
        {
            this._unitOfWork.InTransAuditLogRepository.Add(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Update(InTransAuditLog entity)
        {
            this._unitOfWork.InTransAuditLogRepository.Update(entity);
            this._unitOfWork.Save();
            return true;
        }

        public bool Delete(InTransAuditLog entity)
        {
            this._unitOfWork.InTransAuditLogRepository.Delete(entity);
            this._unitOfWork.Save();
            return true;
        }

        public IEnumerable<InTransAuditLog> GetAll()
        {
            return this._unitOfWork.InTransAuditLogRepository.GetAll();
        }

        public IEnumerable<InTransAuditLog> GetAll(Expression<Func<InTransAuditLog, bool>> filter, Func<IQueryable<InTransAuditLog>, IOrderedQueryable<InTransAuditLog>> orderBy, string includeProperties)
        {
            return this._unitOfWork.InTransAuditLogRepository.Get(filter, orderBy, includeProperties);
        }
    }
}
