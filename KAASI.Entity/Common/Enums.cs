﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.Common
{
    public static class Enums
    {
        public enum ResponseCodes
        {
            ServerError = 500,
            SuccessResponse = 200,
            NotFound = 404,
            NotAuthorized = 403
        };
    }
}
