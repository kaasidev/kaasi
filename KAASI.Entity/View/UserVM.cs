﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.View
{
    public class UserVM
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string TwoFactorAuthPhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        //  [Required]
        public string PartyDescription { get; set; }
        public int PartyID { get; set; }

        public static UserVM ToViewModel(UserDM model)
        {
            return new UserVM
            {
                UserId = model.ID,
                Email = model.Email,
                EmailConfirmed = model.EmailConfirmed,
                PasswordHash = model.PasswordHash,
                SecurityStamp = model.SecurityStamp,
                TwoFactorAuthPhoneNumber = model.TwoFactorAuthPhoneNumber,
                PhoneNumberConfirmed = model.PhoneNumberConfirmed,
                TwoFactorEnabled = model.TwoFactorEnabled,
                LockoutEndDateUtc = model.LockoutEndDateUtc,
                LockoutEnabled = model.LockoutEnabled,
                AccessFailedCount = model.AccessFailedCount,
                UserName = model.UserName,
                CreatedBy = model.CreatedBy,
                CreatedDate = model.CreatedDate,
                UpdatedBy = model.UpdatedBy,
                UpdatedDate = model.UpdatedDate,
                PartyID = model.PartyID,
                PartyDescription = model.PartyName
            };
        }
        public UserDM ToEntityModel()
        {
            return new UserDM
            {
                ID = this.UserId,
                Email = this.Email,
                EmailConfirmed = this.EmailConfirmed,
                PasswordHash = this.PasswordHash,
                SecurityStamp = this.SecurityStamp,
                TwoFactorAuthPhoneNumber = this.TwoFactorAuthPhoneNumber,
                PhoneNumberConfirmed = this.PhoneNumberConfirmed,
                TwoFactorEnabled = this.TwoFactorEnabled,
                LockoutEndDateUtc = this.LockoutEndDateUtc,
                LockoutEnabled = this.LockoutEnabled,
                AccessFailedCount = this.AccessFailedCount,
                UserName = this.UserName,
                CreatedBy = this.CreatedBy,
                CreatedDate = this.CreatedDate,
                UpdatedBy = this.UpdatedBy,
                UpdatedDate = this.UpdatedDate,
                PartyID = this.PartyID,
                PartyName = this.PartyDescription
            };
        }
    }
}
