﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.View
{
   public class ResultVM
    {
        public int IsSuccess { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public int Code { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
