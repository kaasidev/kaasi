﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.View
{
    public class RoleDetailVM
    {
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public string ResourceType { get; set; }
        public int ResourceTypeID { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string UserID { get; set; }
        public string ParentResourceName { get; set; }
        public string PermissionType { get; set; }

        public static RoleDetailVM ToViewModel(RoleDetailDM model)
        {
            return new RoleDetailVM
            {
                ResourceID = model.ResourceID,
                ResourceName = model.ResourceName,
                ResourceType = model.ResourceType,
                ResourceTypeID = model.ResourceTypeID,
                RoleID = model.RoleID,
                RoleName = model.RoleName,
                UserName = model.UserName,
                UserID = model.UserID,
                ParentResourceName = model.ParentResourceName,
                PermissionType = model.PermissionType
            };
        }
        public RoleDetailDM ToEntityModel()
        {
            return new RoleDetailDM
            {
                ResourceID = this.ResourceID,
                ResourceName = this.ResourceName,
                ResourceType = this.ResourceType,
                ResourceTypeID = this.ResourceTypeID,
                RoleID = this.RoleID,
                RoleName = this.RoleName,
                UserName = this.UserName,
                UserID = this.UserID,
                ParentResourceName = this.ParentResourceName,
                PermissionType = this.PermissionType
            };
        }


    }
}
