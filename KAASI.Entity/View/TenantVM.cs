﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.View
{
    public class TenantVM
    {
        public int ID { get; set; }
        public Guid SecretID { get; set; }
        public string Description { get; set; }
        public static TenantVM ToViewModel(TenantDM model)
        {
            return new TenantVM
            {
                ID = model.ID,
                SecretID = model.SecretID,
                Description = model.Description
            };
        }
        public TenantDM ToEntityModel()
        {
            return new TenantDM
            {
                ID = this.ID,
                SecretID = this.SecretID,
                Description = this.Description
            };
        }
    }
}
