﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.View
{
    public class LanguageAndCurrencyVM
    {
        public int LanguageAndCurrencyId { get; set; }

        [Required(ErrorMessage = "Please enter Language")]
        public string Language { get; set; }

        [Required(ErrorMessage = "Please enter Text.")]
        public string Text { get; set; }

        [Required(ErrorMessage = "Please enter currency.")]
        public double Currency { get; set; }

        public string CurrencyCulture { get; set; }

        public static LanguageAndCurrencyVM ToViewModel(LanguageAndCurrencyDM model)
        {
            return new LanguageAndCurrencyVM
            {
                LanguageAndCurrencyId = model.Id,
                Language = model.Language,
                Text = model.Text,
                Currency = model.Currency
            };
        }
        public LanguageAndCurrencyDM ToEntityModel()
        {
            return new LanguageAndCurrencyDM
            {
                Id = this.LanguageAndCurrencyId,
                Language = this.Language,
                Text = this.Text,
                Currency = this.Currency
            };
        }
    }
}
