﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.View
{
    public class RoleTypeVM
    {
        public int RoleId { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 2, ErrorMessage = "Role Description has to greater than 2characters and less than 10 character.")]
        public string Description { get; set; }
        // public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public static RoleTypeVM ToViewModel(RoleTypeDM model)
        {
            return new RoleTypeVM
            {
                RoleId = model.ID,
                Description = model.Description
            };
        }
        public RoleTypeDM ToEntityModel()
        {
            return new RoleTypeDM
            {
                ID = this.RoleId,
                Description = this.Description
            };
        }
    }
}
