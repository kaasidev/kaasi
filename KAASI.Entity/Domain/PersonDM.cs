﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.Domain
{
   public class PersonDM
    {
        public int? PartyRoleID { get; set; }
      
        public string PartyRelationshipTypeList { get; set; }
      
        public string ElectronicAddressList { get; set; }
     
        public string PartyIdentificationTypeList { get; set; }
     
        public string PartySettingsTypeList { get; set; }
        
        public string TelecommunicationsNumberList { get; set; }
        public string Name { get; set; }
        public string ID { get; set; }
        public int LanguageTypeID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string TwoFactorAuthPhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? FromPartyRoleID { get; set; }
        public int RoleTypeID { get; set; }
        public string RoleTypeDescription { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ThruDate { get; set; }
        public string Status { get; set; }
        public string LastSeen { get; set; }

        // Party Settings Type
        public string IsUserPrimaryContact { get; set; }
        public string IsPasswordReqChange { get; set; }

        // Party Contact Type
        public string EmailAddressPrimary { get; set; }
        public string ExtensionCode { get; set; }

        // Telecommunications Number
        public string PhoneWork { get; set; }
        public string HasPrimaryContact { get; set; }
    }
}
