﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.Domain
{
    public class RoleDetailDM
    {
        //  public int RoleDetailId { get; set; }
        // // public string ResourceType { get; set; }
        //  public string ResourceId { get; set; }
        ////  public string ResourceName { get; set; }
        //  public bool ModifyAccess { get; set; }
        //  public string UpdatedUserId { get; set; }
        //  public string RoleId { get; set; }


        //New changes
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public string ResourceType { get; set; }
        public int ResourceTypeID { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string UserID { get; set; }
        public string ParentResourceName { get; set; }
        public string PermissionType { get; set; }
    }
}
