﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.Domain
{
    public class TenantDM
    {
        public int ID { get; set; }
        public Guid SecretID { get; set; }
        public string Description { get; set; }
    }
}
