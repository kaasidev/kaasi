﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Entity.Domain
{
    public class LanguageAndCurrencyDM
    {
        public int Id { get; set; }
        public string Language { get; set; }
        public string Text { get; set; }
        public double Currency { get; set; }
    }
}
