﻿using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Abstract
{
   public interface ICustomerManagerRepo
    {
        ResultVM AddCustomer(CustomerVM customer);
    }
}
