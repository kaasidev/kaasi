﻿using System;

namespace KAASI.DAL.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();

        bool Commit();
    }
}
