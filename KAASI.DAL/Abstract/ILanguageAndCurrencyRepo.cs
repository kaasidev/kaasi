﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Abstract
{
    public interface ILanguageAndCurrencyRepo
    {
        string SaveLanguageAndCurrency(LanguageAndCurrencyDM dataModel);
        IEnumerable<LanguageAndCurrencyDM> GetAll();
    }
}
