﻿using System.Data;

namespace KAASI.DAL.Abstract
{
    public interface IDbConnectionFactory
    {
        IDbConnection Create();
    }
}
