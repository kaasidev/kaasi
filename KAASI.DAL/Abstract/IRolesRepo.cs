﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Abstract
{
    public interface IRolesRepo
    {
        void SaveUserRole(RoleTypeDM role);
        void RemoveRoleById(int Id);
        IQueryable<RoleTypeDM> GetAllRoles();
        RoleTypeDM GetRoleById(int id);
    }
}
