﻿using KAASI.Entity.Domain;
using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Abstract
{
    public interface IPartyManagerRepo
    {

      
        List<PersonDM> GetAllStaff(int partyRoleId, int partyRelationshipTypeID, int primaryContactPartyRelationshipTypeId, int regionPartyRelationshipTypeId);
        PersonDM GetStaff(int partyRoleId, int partyRelationshipTypeID, int primaryContactPartyRelationshipTypeId);
        string ValidatePrimaryContactStatus(int partyRoleId, int partyRelationshipTypeId);
        //string ValidateNewUserName(string UserName,int? partyRoleId);
        string ValidateNewUserName(string UserName);
     
        IQueryable<PersonDM> GetUserDetailsListByUserName(string userName);
        PersonDM GetUserDetailsByUserName(string userName);
        IQueryable<TenantDM> GetAllClientIds();
        List<RoleDetailDM> GetRoleDetailsByUserName(string userName);
        List<string> GetRolesByUserId(string userId);
       
        string DeleteStaff(int partyRoleId, int partyRelationshipTypeId, int primaryContactPartyRelationshipTypeId);
        string DeleteOrganization(int partyRoleID);
        string InactivateOrganization(int partyRoleID);
      
    }
}
