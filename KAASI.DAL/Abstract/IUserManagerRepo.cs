﻿using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Abstract
{
    public interface IUserManagerRepo
    {
        string RegisterUser(UserDM user);
        UserDM AuthenticateUserByEmailAndPassword(string email, string passwordHash);
        UserDM GetUserDetailsByUserName(string userName);
        UserDM GetUserDetailsByUserId(string userId);
        void EditUserDetailsByUserId(UserDM user);
        void ChangePasswordByUserId(string userId, string passwordHash);
        List<string> GetRolesByUserId(string userId);
        string CreateUserRole(string userId, string role);
        IQueryable<UserDM> GetUserDetailsListByUserName(string userName);
        void RemoveUserLogin(string Id);
        List<RoleDetailDM> GetRoleDetailsByUserName(string userName);
        IQueryable<RoleTypeDM> GetAllRoles();
        void RemoveUserRolesByUserId(string UserId);
        IQueryable<TenantDM> GetAllClientIds();
    }
}
