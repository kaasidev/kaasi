﻿using KAASI.DAL.Abstract;
using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KAASI.DAL.Concrete
{
   public class CustomerManagerRepo:ICustomerManagerRepo
    {

        public ResultVM AddCustomer(CustomerVM customer)
        {
            ResultVM resultModel = new ResultVM();
            try
            {
                Kapruka.Repository.Customer cust = new Kapruka.Repository.Customer();
                Kapruka.Enterprise.CustomerService custSer = new Kapruka.Enterprise.CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                if (customer.CustomerID.HasValue)
                {
                    int custId = customer.CustomerID.Value;
                    var custLst = custSer.GetAll(x=>x.CustomerID==custId,null,"").ToList();
                    if (custLst.Count == 0)
                    {
                        resultModel.Code = int.Parse(Entity.Common.Enums.ResponseCodes.NotFound.ToString());
                        resultModel.Exception = "Customer not exist for given CustomerID " + customer.CustomerID.Value;
                        resultModel.IsSuccess = 0;
                        resultModel.DateAdded = DateTime.Now;
                        return resultModel;

                    }
                    else
                    {
                        cust = custLst.SingleOrDefault();
                        cust.ABN = customer.ABN;
                        cust.AccountBalance = customer.AccountBalance;
                        cust.AccountManager = customer.AccountManager;
                        cust.AccountPassword = customer.AccountPassword;
                        cust.Active = customer.Active;
                        cust.AddressLine1 = customer.AddressLine1;
                        cust.AddressLine2 = customer.AddressLine2;
                        cust.AgentID = customer.AgentID;
                        cust.agentTransactionID = customer.agentTransactionID;
                        cust.AKA = customer.AKA;
                        cust.AmendedBy = customer.AmendedBy;

                        return resultModel;
                    }
                }
                else
                {
                    return resultModel;
                }
            }
            catch (Exception ex)
            {
                return resultModel;
            }


            
        }
    }
}
