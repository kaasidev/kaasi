﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using KAASI.DAL.Abstract;

namespace KAASI.DAL.Concrete
{
    public class DBContext
    {
        public class DbContext : IDisposable, IDbContext
        {
            private IDbConnection _connection;
            private readonly  DbConnectionFactory _connectionFactory;
            //private readonly ReaderWriterLockSlim _rwLock = new ReaderWriterLockSlim();
            //private readonly LinkedList<UnitOfWork> _workItems = new LinkedList<UnitOfWork>();

            public DbContext(string connectionStringName)
            {
                _connectionFactory = new DbConnectionFactory(connectionStringName);
            }

            private void CreateOrReuseConnection()
            {
                if (_connection != null) return;

                _connection = _connectionFactory.Create();
            }


            private IDbTransaction GetCurrentTransaction()
            {
                IDbTransaction currentTransaction = null;
                //_rwLock.EnterReadLock();
                //if (_workItems.Any()) currentTransaction = _workItems.First.Value.Transaction;
                //_rwLock.ExitReadLock();

                return currentTransaction;
            }

            private async Task<IDbTransaction> GetCurrentTransactionAsync()
            {
                IDbTransaction currentTransaction = null;
                //_rwLock.EnterReadLock();
                //if (_workItems.Any()) currentTransaction = _workItems.First.Value.Transaction;
                //_rwLock.ExitReadLock();

                return currentTransaction;
            }


            public IEnumerable<dynamic> Query(string sql, dynamic param = null, int? commandTimeout = null,
                CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query<dynamic>(_connection, sql, param, GetCurrentTransaction(), true, commandTimeout,
                    commandType);
            }


            public IEnumerable<T> Query<T>(string sql, dynamic param = null, int? commandTimeout = null,
                CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query<T>(_connection, sql, param, GetCurrentTransaction(), true, commandTimeout,
                    commandType);
            }

            public IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map,
                dynamic param = null, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TReturn> map, dynamic param = null, string splitOn = "Id",
                int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TFourth, TReturn> map, dynamic param = null, string splitOn = "Id",
                int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, dynamic param = null, string splitOn = "Id",
                int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, dynamic param = null,
                string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(
                string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map,
                dynamic param = null, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public SqlMapper.GridReader QueryMultiple(string sql, dynamic param = null, int? commandTimeout = null,
                CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.QueryMultiple(_connection, sql, param, GetCurrentTransaction(), commandTimeout, commandType);
            }

            public int Execute(string sql, dynamic param = null, int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper expects a connection to be open when calling Execute, so we'll have to open it.
                bool wasClosed = _connection.State == ConnectionState.Closed;
                if (wasClosed) _connection.Open();
                try
                {
                    return SqlMapper.Execute(_connection, sql, param, GetCurrentTransaction(), commandTimeout, commandType);
                }
                finally
                {
                    if (wasClosed) _connection.Close();
                }
            }


            //async methods
            public Task<IEnumerable<dynamic>> QueryAsync(string sql, dynamic param = null, int? commandTimeout = null,
                CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.Query<dynamic>(_connection, sql, param, GetCurrentTransaction(), true, commandTimeout,
                    commandType);
            }


            public async Task<IEnumerable<T>> QueryAsync<T>(string sql, dynamic param = null, int? commandTimeout = null,
                CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                var currTrans = GetCurrentTransaction();
                //Dapper will open and close the connection for us if necessary.
                return await SqlMapper.QueryAsync<T>(_connection, sql, param, currTrans, commandTimeout,
                    commandType);
            }

            public async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map,
                dynamic param = null, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return await SqlMapper.QueryAsync(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TReturn> map, dynamic param = null, string splitOn = "Id",
                int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return await SqlMapper.QueryAsync(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TFourth, TReturn> map, dynamic param = null, string splitOn = "Id",
                int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return await SqlMapper.QueryAsync(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, dynamic param = null, string splitOn = "Id",
                int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return await SqlMapper.QueryAsync(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql,
                Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, dynamic param = null,
                string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return await SqlMapper.Query(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(
                string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map,
                dynamic param = null, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return SqlMapper.QueryAsync(_connection, sql, map, param, GetCurrentTransaction(), true, splitOn, commandTimeout,
                    commandType);
            }

            public async Task<SqlMapper.GridReader> QueryMultipleAsync(string sql, dynamic param = null, int? commandTimeout = null,
                CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper will open and close the connection for us if necessary.
                return await SqlMapper.QueryMultiple(_connection, sql, param, GetCurrentTransaction(), commandTimeout, commandType);
            }

            public async Task<int> ExecuteAsync(string sql, dynamic param = null, int? commandTimeout = null, CommandType? commandType = null)
            {
                CreateOrReuseConnection();
                //Dapper expects a connection to be open when calling Execute, so we'll have to open it.
                bool wasClosed = _connection.State == ConnectionState.Closed;
                if (wasClosed) _connection.Open();
                try
                {
                    return await SqlMapper.ExecuteAsync(_connection, sql, param, GetCurrentTransaction(), commandTimeout, commandType);
                }
                finally
                {
                    if (wasClosed) _connection.Close();
                }
            }





            /// <summary>
            /// Implements <see cref="IDisposable.Dispose"/>.
            /// </summary>
            public void Dispose()
            {
                //Use an upgradeable lock, because when we dispose a unit of work,
                //one of the removal methods will be called (which enters a write lock)
                //_rwLock.EnterUpgradeableReadLock();
                //try
                //{
                //    while (_workItems.Any())
                //    {
                //        var workItem = _workItems.First;
                //        workItem.Value.Dispose();
                //        //rollback, will remove the item from the LinkedList because it calls either RemoveTransaction or RemoveTransactionAndCloseConnection
                //    }
                //}
                //finally
                //{
                //    //_rwLock.ExitUpgradeableReadLock();
                //}

                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }
            }
        }
    }
}
