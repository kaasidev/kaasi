﻿using KAASI.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace KAASI.DAL.Concrete
{
    public class UnitOfWork : IUnitOfWork
    {
        private TransactionScope _transaction;

        public TransactionScope Transaction
        {
            set
            {
                this._transaction = value;
            }
        }

        public void BeginTransaction()
        {
            this.Transaction = new TransactionScope();
        }

        public bool Commit()
        {
            try
            {
                this._transaction.Complete();
                return true;
            }
            catch
            {
                throw;
            }

        }

        #region " Dispose "

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (this._transaction != null)
                    {
                        this._transaction.Dispose();
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposedValue);
            GC.SuppressFinalize(this);
        }

        #endregion " Dispose "

    }
}
