﻿using KAASI.DAL.Abstract;
using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Concrete
{
    public class LanguageAndCurrencyRepo : ILanguageAndCurrencyRepo
    {

        private readonly IDbContext _context;
        public LanguageAndCurrencyRepo(IDbContext context)
        {
            _context = context;
        }
        #region SQL Stmts
        private const string Save_LanguageAndCurrency = @"[sp_SaveLanguageAndCurrency] @LanguageAndCurrencyId, @Language,@Text,@Currency";
        private const string Get_AllLanguageAndCurrency = @"[sp_GetAllLanguageAndCurrency]";
        #endregion

        public string SaveLanguageAndCurrency(LanguageAndCurrencyDM dataModel)
        {
            var result = _context.Query<string>(Save_LanguageAndCurrency, new { LanguageAndCurrencyId = dataModel.Id, dataModel.Language, dataModel.Text, dataModel.Currency });
            return "Saved SuccessFully.";
        }

        public IEnumerable<LanguageAndCurrencyDM> GetAll()
        {
            return _context.Query<LanguageAndCurrencyDM>(Get_AllLanguageAndCurrency);
        }
    }
}
