﻿using KAASI.DAL.Abstract;
using KAASI.Entity.Domain;
using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace KAASI.DAL.Concrete
{
    public class PartyManagerRepo : IPartyManagerRepo
    {

        private readonly IDbContext _context;
        public PartyManagerRepo(IDbContext context)
        {
            _context = context;
        }

        #region SQL Stmts
        private const string usp_Party_Save = @"EXEC [Party].[usp_Party_Save]
	                                                @Name = @Name,
                                                    @Status = @Status,
                                                    @LanguageTypeID = @LanguageTypeID,
                                                    @Logo = @Logo,
                                                    @RoleTypeID = @RoleTypeID,
                                                    @StartDate = @StartDate,
                                                    @ThruDate = @ThruDate,
                                                    @PartyIdentificationTypeList = @PartyIdentificationTypeList,
                                                    @PartySettingsTypeList = @PartySettingsTypeList,
                                                    @PartyRelationshipTypeList = @PartyRelationshipTypeList,
                                                    @PostalAddressList = @PostalAddressList,
                                                    @ElectronicAddressList = @ElectronicAddressList,
                                                    @TelecommunicationsNumberList = @TelecommunicationsNumberList
                                                ";
        private const string usp_Party_Get = @"[Party].[usp_Party_Get] @PartyRoleID,@PrimaryContactPartyRelationshipTypeID";
        private const string usp_Party_GetAll = @"[Party].[usp_Party_GetAll] @PartyRoleID, @PartyRelationshipTypeID, @PrimaryContactPartyRelationshipTypeID, @TargetPartyRelationshipTypeID";
        private const string usp_PartyByPartyRelationship_Get = @"[Party].[usp_PartyByPartyRelationship_Get] @PartyRelationshipTypeID,@PartyRoleID";
        private const string usp_Party_Update = @"EXEC [Party].[usp_Party_Update]
                                                    @PartyRoleID = @PartyRoleID,
	                                                @Name = @Name,
                                                    @Status = @Status,
                                                    @LanguageTypeID = @LanguageTypeID,
                                                    @Logo = @Logo,
                                                    @RoleTypeID = @RoleTypeID,
                                                    @StartDate = @StartDate,
                                                    @ThruDate = @ThruDate,
                                                    @PartyIdentificationTypeList = @PartyIdentificationTypeList,
                                                    @PartySettingsTypeList = @PartySettingsTypeList,
                                                    @PartyRelationshipTypeList = @PartyRelationshipTypeList,
                                                    @PostalAddressList = @PostalAddressList,
                                                    @ElectronicAddressList = @ElectronicAddressList,
                                                    @TelecommunicationsNumberList = @TelecommunicationsNumberList
                                                ";
        private const string usp_Country_Get = @"[Party].[usp_Country_Get]";
        private const string usp_State_Get = @"[Party].[usp_State_Get] @ID";
        private const string usp_TimeZone_Get = @"[Party].[usp_TimeZone_Get]";
        private const string usp_Language_Get = @"[Party].[usp_Language_Get]";
        private const string usp_Staff_Save = @"EXEC Party.usp_Staff_Save
                                                    @PartyRelationshipTypeList = @PartyRelationshipTypeList,
                                                    @ID = @ID,
                                                    @LanguageTypeID = @LanguageTypeID,
                                                    @ElectronicAddressList = @ElectronicAddressList,
                                                    @PartySettingsTypeList = @PartySettingsTypeList,
                                                    @PartyIdentificationTypeList = @PartyIdentificationTypeList,
                                                    @TelecommunicationsNumberList = @TelecommunicationsNumberList,
                                                    @FirstName = @FirstName,
                                                    @MiddleName = @MiddleName,
                                                    @LastName = @LastName,
                                                    @EmailConfirmed = @EmailConfirmed,
                                                    @PasswordHash = @PasswordHash,
                                                    @SecurityStamp = @SecurityStamp,
                                                    @TwoFactorAuthPhoneNumber = @TwoFactorAuthPhoneNumber,
                                                    @PhoneNumberConfirmed = @PhoneNumberConfirmed,
                                                    @TwoFactorEnabled = @TwoFactorEnabled,
                                                    @LockoutEndDateUtc = @LockoutEndDateUtc,
                                                    @LockoutEnabled = @LockoutEnabled,
                                                    @AccessFailedCount = @AccessFailedCount,
                                                    @UserName = @UserName,
                                                    @RoleTypeID = @RoleTypeID,
                                                    @StartDate = @StartDate,
                                                    @ThruDate = @ThruDate,
                                                    @Status = @Status
                                                ";
        private const string usp_Staff_Get = @"[Party].[usp_Staff_Get] @PartyRoleID, @PartyRelationshipTypeID, @PrimaryContactPartyRelationshipTypeID";
        private const string usp_Staff_GetAll = @"[Party].[usp_Staff_GetAll] @PartyRoleID, @PartyRelationshipTypeID, @PrimaryContactPartyRelationshipTypeID";
        private const string usp_Staff_Update = @"EXEC Party.usp_Staff_Update
	                                                @PartyRoleID=@PartyRoleID,
                                                    @LanguageTypeID = @LanguageTypeID,
                                                    @ElectronicAddressList = @ElectronicAddressList,
                                                    @PartySettingsTypeList = @PartySettingsTypeList,
                                                    @PartyIdentificationTypeList = @PartyIdentificationTypeList,
                                                    @TelecommunicationsNumberList = @TelecommunicationsNumberList,
                                                    @PartyRelationshipTypeList = @PartyRelationshipTypeList,
                                                    @UserName = @UserName,
                                                    @FirstName = @FirstName,
                                                    @MiddleName = @MiddleName,
                                                    @LastName = @LastName,
                                                    @EmailConfirmed = @EmailConfirmed,
                                                    @SecurityStamp = @SecurityStamp,
                                                    @TwoFactorAuthPhoneNumber = @TwoFactorAuthPhoneNumber,
                                                    @PhoneNumberConfirmed = @PhoneNumberConfirmed,
                                                    @TwoFactorEnabled = @TwoFactorEnabled,
                                                    @LockoutEndDateUtc = @LockoutEndDateUtc,
                                                    @LockoutEnabled = @LockoutEnabled,
                                                    @AccessFailedCount = @AccessFailedCount,
                                                    @RoleTypeID = @RoleTypeID,
                                                    @StartDate = @StartDate,
                                                    @ThruDate = @ThruDate,
                                                    @Status = @Status
                                                ";
        private const string Get_User = @"[Party].[usp_PersonByUserName_Get] @UserName";
        private const string Get_ClientIds = @"[Security].[usp_AllClientId_Get]";
        private const string Get_RoleDetails = @"[Party].[usp_RoleDetailsByUserName_Get] @UserName";
        private const string GET_ROLES = @"[Party].[usp_UserRolesByUserId_Get] @UserId";
        private const string usp_PrimaryContact_Get = @"[Party].[usp_PrimaryContact_Get] @PartyRoleID,@PartyRelationshipTypeID";
        private const string usp_Staff_Delete = @"[Party].[usp_Staff_Delete] @PartyRoleID,@PartyRelationshipTypeId,@PrimaryContactPartyRelationshipTypeID";
        private const string usp_StaffPassword_Update = @"EXEC Party.usp_StaffPassword_Update
                                                    @UserName = @UserName,
	                                                @PartyRoleID = @PartyRoleID,
                                                    @Password = @Password,
                                                    @PasswordHash = @PasswordHash,
                                                    @PartyContactTypeID = @PartyContactTypeID,
                                                    @PartySettingsTypeID = @PartySettingsTypeID
                                                ";
        private const string usp_StaffBarcodes_GetAll = @"EXEC Party.usp_StaffBarcodes_GetAll
	                                                @PartyRoleID = @PartyRoleID,
                                                    @PartyRelationshipTypeID = @PartyRelationshipTypeID
                                                ";
        private const string usp_Organization_Delete = @"[Party].[usp_Organization_Delete] @PartyRoleID";
        private const string usp_Organization_Inactivate = @"[Party].[usp_Organization_Inactivate] @PartyRoleID";
        //private const string usp_ValidateUserName_Get = @"[Party].[usp_ValidateUserName_Get] @UserName,@PartyRoleID";
        private const string usp_ValidateUserName_Get = @"[Party].[usp_ValidateUserName_Get] @UserName";
        #endregion

      
        public static string ConvertToInnerXML(XElement el)
        {
            var reader = el.CreateReader();
            reader.MoveToContent();
            return reader.ReadInnerXml();
        }
       
        public IQueryable<PersonDM> GetUserDetailsListByUserName(string userName)
        {
            var user = _context.Query<PersonDM>(Get_User, new { userName }).AsQueryable();
            return user;
        }
        public PersonDM GetUserDetailsByUserName(string userName)//Function used to Log In to the system
        {
            var user = _context.Query<PersonDM>(Get_User, new { userName }).SingleOrDefault();
            return user;
        }
     
      
        public string ValidatePrimaryContactStatus(int partyRoleId, int partyRelationshipTypeId)
        {
            var status = _context.Query<string>(usp_PrimaryContact_Get, new { partyRoleId, partyRelationshipTypeId }).SingleOrDefault();
            return status;
        }
        public List<PersonDM> GetAllStaff(int partyRoleId, int partyRelationshipTypeID, int primaryContactPartyRelationshipTypeId, int regionPartyRelationshipTypeId)
        {
            return _context.Query<PersonDM>(usp_Staff_GetAll, new { partyRoleId, partyRelationshipTypeID, primaryContactPartyRelationshipTypeId, regionPartyRelationshipTypeId }).ToList();
        }
        public PersonDM GetStaff(int partyRoleId, int partyRelationshipTypeID, int primaryContactPartyRelationshipTypeId)
        {
            return _context.Query<PersonDM>(usp_Staff_Get, new { partyRoleId, partyRelationshipTypeID, primaryContactPartyRelationshipTypeId }).SingleOrDefault();
        }
       
        public IQueryable<TenantDM> GetAllClientIds()
        {
            var roles = _context.Query<TenantDM>(Get_ClientIds).AsQueryable();
            return roles;
        }
        public List<RoleDetailDM> GetRoleDetailsByUserName(string userName)
        {
            var user = _context.Query<RoleDetailDM>(Get_RoleDetails, new { userName }).ToList();
            return user;
        }
        public List<string> GetRolesByUserId(string userId)
        {
            var user = _context.Query<string>(GET_ROLES, new { userId }).ToList();
            return user;
        }
        public string DeleteOrganization(int partyRoleId)
        {
            var status = _context.Query<string>(usp_Organization_Delete, new { partyRoleId }).FirstOrDefault();
            return status;
        }
        public string InactivateOrganization(int partyRoleId)
        {
            var status = _context.Query<string>(usp_Organization_Inactivate, new { partyRoleId }).FirstOrDefault();
            return status;
        }
        public string DeleteStaff(int partyRoleId, int partyRelationshipTypeId, int primaryContactPartyRelationshipTypeId)
        {
            var status = _context.Query<string>(usp_Staff_Delete, new { partyRoleId, partyRelationshipTypeId, primaryContactPartyRelationshipTypeId }).SingleOrDefault();
            return status;
        }
      
        //public string ValidateNewUserName(string UserName,int? partyRoleId)
        //{
        //    var status = _context.Query<string>(usp_ValidateUserName_Get, new { UserName , partyRoleId }).SingleOrDefault();
        //    return status;
        //}
        public string ValidateNewUserName(string UserName)
        {
            var status = _context.Query<string>(usp_ValidateUserName_Get, new { UserName }).SingleOrDefault();
            return status;
        }
    }
}
