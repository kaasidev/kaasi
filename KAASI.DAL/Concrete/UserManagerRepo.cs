﻿using KAASI.DAL.Abstract;
using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Concrete
{
    public class UserManagerRepo : IUserManagerRepo
    {

        private readonly IDbContext _context;
        public UserManagerRepo(IDbContext context)
        {
            _context = context;
        }



        #region SQL Stmts
        private const string INSERT_User = @"EXEC sp_RegisterUser
	                                                @ID=@ID,
                                                    @Email=@Email,
                                                    @EmailConfirmed=@EmailConfirmed,
                                                    @PasswordHash=@PasswordHash,
                                                    @SecurityStamp=@SecurityStamp,
                                                    @TwoFactorAuthPhoneNumber=@TwoFactorAuthPhoneNumber,
                                                    @PhoneNumberConfirmed=@PhoneNumberConfirmed,
                                                    @TwoFactorEnabled=@TwoFactorEnabled,
                                                    @LockoutEndDateUtc=@LockoutEndDateUtc,
                                                    @LockoutEnabled=@LockoutEnabled,
                                                    @AccessFailedCount=@AccessFailedCount,
                                                    @UserName=@UserName,
                                                    @PartyID = @PartyID
                                                ";
        private const string GET_AUTH = @"[sp_UserAuthenticate] @Email, @PasswordHash";
        private const string Get_User = @"[sp_GetUserByUserName] @UserName  ";
        private const string Get_UserByUserId = @"[sp_GetUserById] @UserId";
        private const string EDIT_User = @"EXEC sp_EditUser
	                                                @ID=@ID,
                                                    @Email=@Email,
                                                    @EmailConfirmed=@EmailConfirmed,
                                                    @SecurityStamp=@SecurityStamp,
                                                    @TwoFactorAuthPhoneNumber=@TwoFactorAuthPhoneNumber,
                                                    @PhoneNumberConfirmed=@PhoneNumberConfirmed,
                                                    @TwoFactorEnabled=@TwoFactorEnabled,
                                                    @LockoutEndDateUtc=@LockoutEndDateUtc,
                                                    @LockoutEnabled=@LockoutEnabled,
                                                    @AccessFailedCount=@AccessFailedCount,
                                                    @UserName=@UserName
                                                ";
        private const string CHANGE_Password = @"[sp_ChangePassword] @UserId, @PasswordHash";
        private const string GET_ROLES = @"[sp_GetUserRolesByUserId] @UserId";
        private const string SAVE_ROLES = @"[sp_SaveUserRoles] @UserId, @Role";
        private const string REMOVE_User = @"EXEC [sp_RemoveUserLogin] @UserId=@UserId";
        private const string Get_RoleDetails = @"[sp_GetRoleDetailsByUserName] @UserName";
        private const string Get_AllRoles = @"[sp_GetAllRoles]";
        private const string Get_ClientIds = @"[sp_GetAllClientId]";
        private const string Remove_UserRoles = @"EXEC [sp_DeleteUserRolesByUserId] @UserId=@UserId";
        #endregion



        public int Insert(UserDM userDM)
        {
            return _context.Execute(INSERT_User, userDM);
        }

        public string RegisterUser(UserDM user)
        {
            var result = _context.Query<string>(INSERT_User, user).FirstOrDefault();
            return result;
        }



        public UserDM AuthenticateUserByEmailAndPassword(string email, string passwordHash)
        {
            var user = _context.Query<UserDM>(GET_AUTH, new { email, passwordHash }).SingleOrDefault();
            return user;
        }


        public UserDM GetUserDetailsByUserName(string userName)
        {
            var user = _context.Query<UserDM>(Get_User, new { userName }).SingleOrDefault();
            return user;
        }
        public UserDM GetUserDetailsByUserId(string userId)
        {
            var user = _context.Query<UserDM>(Get_UserByUserId, new { userId }).SingleOrDefault();
            return user;
        }
        public void EditUserDetailsByUserId(UserDM user)
        {
            _context.Execute(EDIT_User, user);
        }
        public void ChangePasswordByUserId(string UserId, string passwordHash)
        {
            _context.Query(CHANGE_Password, new { UserId, passwordHash });
        }

        public List<string> GetRolesByUserId(string userId)
        {
            var user = _context.Query<string>(GET_ROLES, new { userId }).ToList();
            return user;
        }

        public List<RoleDetailDM> GetRoleDetailsByUserName(string userName)
        {
            var user = _context.Query<RoleDetailDM>(Get_RoleDetails, new { userName }).ToList();
            return user;
        }

        public string CreateUserRole(string UserId, string Role)
        {
            var result = _context.Query<string>(SAVE_ROLES, new { UserId, Role });
            return string.Empty;
        }

        public IQueryable<UserDM> GetUserDetailsListByUserName(string userName)
        {
            var user = _context.Query<UserDM>(Get_User, new { userName }).AsQueryable();
            return user;
        }

        public void RemoveUserLogin(string UserId)
        {
            _context.Query(REMOVE_User, new { UserId });
        }

        public void RemoveUserRolesByUserId(string UserId)
        {
            _context.Query(Remove_UserRoles, new { UserId });
        }

        public IQueryable<RoleTypeDM> GetAllRoles()
        {
            var roles = _context.Query<RoleTypeDM>(Get_AllRoles).AsQueryable();
            return roles;
        }

        public IQueryable<TenantDM> GetAllClientIds()
        {
            var roles = _context.Query<TenantDM>(Get_ClientIds).AsQueryable();
            return roles;
        }


    }
}
