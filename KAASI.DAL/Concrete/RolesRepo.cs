﻿using KAASI.DAL.Abstract;
using KAASI.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.DAL.Concrete
{
    public class RolesRepo : IRolesRepo
    {
        private readonly IDbContext _context;
        public RolesRepo(IDbContext context)
        {
            _context = context;
        }

        #region SQL Stmts
        private const string Save_Roles = @"EXEC sp_SaveRoles
                                            @Id=@Id,
                                            @Description=@Description
                                                ";
        private const string Remove_Role = @"EXEC sp_RemoveRoleByRoleId @Id=@Id";
        private const string Get_AllRoles = @"[sp_GetAllRoles]";
        private const string Get_RoleByRoleId = @"[sp_GetRoleByRoleId] @Id";
        #endregion

        public void SaveUserRole(RoleTypeDM role)
        {
            _context.Execute(Save_Roles, role);
        }

        public void RemoveRoleById(int Id)
        {
            _context.Query(Remove_Role, new { Id });
        }

        public IQueryable<RoleTypeDM> GetAllRoles()
        {
            var roles = _context.Query<RoleTypeDM>(Get_AllRoles).AsQueryable();
            return roles;
        }

        public RoleTypeDM GetRoleById(int id)
        {
            var user = _context.Query<RoleTypeDM>(Get_RoleByRoleId, new { id }).SingleOrDefault();
            return user;
        }

    }
}
