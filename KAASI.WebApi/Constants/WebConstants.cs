﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace KAASI.WebApi.Constants
{
    public static class WebConstants
    {
        public enum AuthenticationTypes
        {
            AspNetIdentity,
            Custom,
            AwsCongnito
        }

        public static string RegisteredOwinType
        {
            get { return ConfigurationManager.AppSettings.Get("Authentication.Type"); }
        }

        public static double TokenExpirationTime
        {
            get
            {
                var timeInMinutes = ConfigurationManager.AppSettings.Get("Owin.TokenExpirationInMinutes");
                return double.Parse(timeInMinutes);
            }
        }

     
        public static string ClientId
        {
            get { return ConfigurationManager.AppSettings.Get("Jwt.ClientId"); }
        }
        public static string SecretKey
        {
            get { return ConfigurationManager.AppSettings.Get("Jwt.SecretKey"); }
        }
        public static string ResourceServer
        {
            get { return ConfigurationManager.AppSettings.Get("Jwt.ResourceServer"); }
        }

        public static string TokenEndPoint
        {
            get { return ConfigurationManager.AppSettings.Get("Owin.TokenEndPoint"); }
        }

        public static string JWTTokenIssuer
        {
            get { return ConfigurationManager.AppSettings.Get("Jwt.TokenIssuer"); }
        }

        public static string AuthorizeEndpointPath
        {
            get { return ConfigurationManager.AppSettings.Get("AuthorizeEndpointPath"); }
        }
        public static bool AllowInsecureHttp
        {
            get 
            { 
                var allowInSecureHttp = ConfigurationManager.AppSettings.Get("AllowInsecureHttp");
                return Convert.ToBoolean(allowInSecureHttp);
            }
        }
        public static string DefaultUserRole
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("DefaultUserRole");
            }
        }

        public static string LoggerName
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("LoggerName");
            }
        }

        public static string AuthorizationDeniedErrorMessage
        {
            get { return ConfigurationManager.AppSettings.Get("Security.AuthorizationDeniedErrorMessage"); }
        }
        public static string UserNotAuthenticatedErrorMessage
        {
            get { return ConfigurationManager.AppSettings.Get("Security.UserNotAuthenticatedErrorMessage"); }
        }

        public static string ConnectionName
        {
            get { return ConfigurationManager.AppSettings.Get("ConnectionName"); }
        }

    }
}