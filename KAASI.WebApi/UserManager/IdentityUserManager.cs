﻿using KAASI.WebApi.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
//using Ninject.Activation;
using System;
using System.Threading.Tasks;
using Owin;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using KAASI.Entity.View;
using KAASI.BL.User;
using System.Web.Security;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using KAASI.WebApi.Constants;
using KAASI.WebApi.Common;
using NLog;
//using KAASI.Utilities.ErrorLogger;

namespace KAASI.WebApi.UserManager
{
    public class IdentityUserManager : IUserManager
    {
        private const string LocalLoginProvider = "Local";
        // Logger logger = LogManager.GetLogger(WebConstants.LoggerName);

        public IdentityUserManager()
        {
        }
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async Task<string> Register(RegisterBindingModel model)
        {

            var user = new ApplicationUser() { UserName = model.UserName, Email = model.UserName };
            string[] roles = null;
            //Validate Roles
            //Assign default role, if role is null or empty
            //if (string.IsNullOrEmpty(model.Role))
            //{
            //    roles = new string[] { WebConstants.DefaultUserRole };
            //}
            //else
            //{
            //    roles = model.Role.Split(',');
            //}

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Failed With errors.")
                };
                throw new HttpResponseException(message);

                //return "Failed With errors.";
            }

            // If succeed the account creation , assign roles to the user
            var roleResult = await this.UserManager.AddToRolesAsync(user.Id, roles); //AddToRoleAsync(user.Id, model.Role);
            if (!roleResult.Succeeded)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Created an User. Failed in User Roles Creation. Make user the roles exists the database.")
                };
                throw new HttpResponseException(message);

                // return "Created an User. Failed in User Roles Creation. Make user the roles exists the database.";
            }

            return string.Format("User Registered Successfully and assigned the roles");

        }

        public async Task<string> EditUser(EditBindingModel model)
        {

            var user = new ApplicationUser() { UserName = model.UserName, Email = model.UserName };
            var userDetails = UserManager.FindById(model.UserName);

            if (userDetails.Id == null)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("User id is invalid.")
                };
                throw new HttpResponseException(message);
                //return "User id is not valid";
            }

            userDetails.Email = model.Email;
            userDetails.EmailConfirmed = model.EmailConfirmed;
            // userDetails.SecurityStamp = model.SecurityStamp;
            userDetails.PhoneNumber = model.PhoneNumber;
            userDetails.PhoneNumberConfirmed = model.PhoneNumberConfirmed;
            userDetails.TwoFactorEnabled = model.TwoFactorEnabled;
            userDetails.LockoutEndDateUtc = model.LockoutEndDateUtc;
            userDetails.LockoutEnabled = model.LockoutEnabled;
            //userDetails.AccessFailedCount = model.AccessFailedCount;
            //userDetails.UserName = model.UserName;

            IdentityResult result = await UserManager.UpdateAsync(userDetails);
            if (result.Succeeded)
            {


                return "Udated successfully.";
            }
            else
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Failed with errors.")
                };
                throw new HttpResponseException(message);
                //return "Failed with errors";
            }

        }


        public async Task<string> ChangePassword(ChangePasswordBindingModel model)
        {
            IdentityResult result = await UserManager.ChangePasswordAsync(model.UserName, model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Failed with errors.")
                };
                throw new HttpResponseException(message);
                //return "Failed with errors.";
            }

            return "Password changed successfully.";
        }

        public async Task<string> RemoveLogin(RemoveLoginBindingModel model)
        {

            if (string.IsNullOrWhiteSpace(model.UserName))
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("User Name is invalid.")
                };
                throw new HttpResponseException(message);
                //return "User id is invalid.";
            }

            IdentityResult result;
            //if (model.LoginProvider == LocalLoginProvider)
            //{
            var userDetails = UserManager.FindById(model.UserName);
            result = await UserManager.RemovePasswordAsync(userDetails.Id);
            //}
            //else
            //{
            //    result = await UserManager.RemoveLoginAsync(model.Id, new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            //}

            if (!result.Succeeded)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Failed with errors.")
                };
                throw new HttpResponseException(message);
                //return "Failed with errors.";
            }

            return "Removed the login successfully.";
        }


        #region " Dispose "

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: free managed resources 
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposedValue);
            GC.SuppressFinalize(this);
        }

        #endregion " Dispose "
    }
}
