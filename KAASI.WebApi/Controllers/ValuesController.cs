﻿using KAASI.WebApi.Attributes;
using NLog;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace KAASI.WebApi.Controllers
{
    [Authorize]
    [CustomAuthorizeAttribute]
    public class ValuesController : ApiController
    {

        public IEnumerable<string> Get()
        {
            return new string[] { "Welcome", "To", "KAASI Api" };
        }

        public string Get(int id)
        {
            return "value";
        }

        public void Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
