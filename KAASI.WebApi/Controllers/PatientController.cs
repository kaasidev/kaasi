﻿using KAASI.WebApi.Attributes;
using System.Collections.Generic;
using System.Web.Http;

namespace KAASI.WebApi.Controllers
{
    [Authorize]
    [CustomAuthorizeAttribute]
    public class PatientController : ApiController
    {
        public IEnumerable<string> Get()
        {
            return new string[] { "Welcome", "To", "Patient Functionality" };
        }

        public string Get(int id)
        {
            return "value";
        }

        public string Post(string value)
        {
            return string.Format("{0} saved sucessfully.", value);
        }

        public string Put(string value)
        {
            return string.Format("{0} updated sucessfully.", value);
        }

        public string Delete(int id)
        {
            return string.Format("{0} deleted sucessfully.", id);
        }
    }
}
