﻿using KAASI.WebApi.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KAASI.WebApi.Controllers
{
    [Authorize]
    [CustomAuthorizeAttribute]
    public class DoctorController : ApiController
    {
        // GET api/doctor
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "This is", "Doctor World" };
        }

        // GET api/doctor/5
        [HttpGet]
        public string Get(int id)
        {
            return "Dr. SomeOne";
        }

        // POST api/doctor
        [HttpPost]
        public string Post(string value)
        {
            return string.Format("{0} Saved sucessfully.", value);
        }

        // PUT api/doctor/5
        [HttpPut]
        public string Put(string value)
        {
            return string.Format("{0} updated sucessfully.", value);
        }

        // DELETE api/doctor/5
        [HttpDelete]
        public string Delete(int id)
        {
            return string.Format("{0} Saved sucessfully.", id);
        }
    }
}
