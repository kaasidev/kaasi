﻿using ECG.BL.LanguageAndCurrency;
using ECG.Entity.View;
using ECG.WebApi.Attributes;
using System.Collections.Generic;
using System.Web.Http;
using NLog;
using ECG.WebApi.Constants;
using System;
using ECG.Utilities.LoggingService;
using ECG.WebApi.Common;
using System.Web;

namespace ECG.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/LanguageAndCurrency")]
    public class LanguageAndCurrencyController : ApiController
    {

        private ILanguageAndCurrencyManager _languageAndCurrencyManager;
        private ILoggingService _loggingService;
        Logger logger = LogManager.GetLogger(WebConstants.LoggerName);

        public LanguageAndCurrencyController(ILanguageAndCurrencyManager lanaguageAndCurrencyManager, ILoggingService loggingService)
        {
            this._languageAndCurrencyManager = lanaguageAndCurrencyManager;
        }

        public IEnumerable<LanguageAndCurrencyVM> Get()
        {
            try
            {
                var languages = HttpContext.Current.Request.UserLanguages;
                return _languageAndCurrencyManager.GetAll();
            }
            catch (Exception ex)
            {
                _loggingService.LogError(ex);
                throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
            }
        }

        public string Post(LanguageAndCurrencyVM model)
        {
            try
            {
                return _languageAndCurrencyManager.SaveLanguageAndCurrency(model);
            }
            catch (Exception ex)
            {
                _loggingService.LogError(ex);
                throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
            }
        }
    }
}
