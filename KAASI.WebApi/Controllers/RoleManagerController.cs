﻿using KAASI.DAL.Abstract;
using KAASI.Entity.View;

using KAASI.WebApi.Models;
using NLog;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using KAASI.BL.RoleManager;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System;
using KAASI.WebApi.Common;
using System.Web;
using KAASI.WebApi.Attributes;
using KAASI.WebApi.Constants;


namespace KAASI.WebApi.Controllers
{
    [Authorize]
    [CustomAuthorizeAttribute]
    public class RoleManagerController : ApiController
    {

        private IRolesManager _rolesManager;

        Logger logger = LogManager.GetLogger(WebConstants.LoggerName);

        public RoleManagerController(IRolesManager rolesManager)
        {
            this._rolesManager = rolesManager;
        }


        // GET api/RoleManager
        public IEnumerable<RoleTypeVM> Get()
        {

            return _rolesManager.GetAllRoles();

        }

        // GET api/RoleManager/5
        public RoleTypeVM Get(int id)
        {
            return _rolesManager.GetRoleById(id);
        }

        // POST api/RoleManager
        public async Task<IHttpActionResult> Create(string role)
        {

            var rolesModel = new RoleTypeVM { Description = role };
            var result = _rolesManager.SaveUserRole(rolesModel);

            if (result.ToLower().Contains("fail"))
            {
                return BadRequest(result);
            }
            return Ok(result);

        }

        // POST api/RoleManager
        [HttpPut]
        public async Task<IHttpActionResult> Update(RoleTypeVM value)
        {

            if (value.RoleId == 0)
            {
                return BadRequest("Id shouldn't be zero");
            }
            var result = _rolesManager.SaveUserRole(value);
            if (result.ToLower().Contains("fail"))
            {
                return BadRequest(result);
            }
            return Ok(result);

        }

        // DELETE api/RoleManager/5
        public async Task<IHttpActionResult> Delete(int id)
        {

            var rolesByid = _rolesManager.GetRoleById(id);
            if (rolesByid != null)
            {
                _rolesManager.RemoveRoleById(id);
            }
            else
            {
                return BadRequest("Failed, Id is not valid");
            }

            return Ok("Success");
        }
    }
}
