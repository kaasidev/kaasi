﻿using KAASI.BL.CustomerManager;
using KAASI.Entity.View;
using KAASI.Utilities.LoggingService;
using KAASI.WebApi.Attributes;
using KAASI.WebApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;

namespace KAASI.WebApi.Controllers
{
    [Authorize]
    [CustomAuthorizeAttribute]
    public class CustomerController : ApiController
    {

        private ILoggingService _loggingService;
        private ICustomerManager _customerManager;
        public CustomerController(ILoggingService loggingService, ICustomerManager customerManager)
        {
            this._loggingService = loggingService;
            this._customerManager = customerManager;
        }
        public async Task<IHttpActionResult> Post(CustomerVM model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                
                var result = await _customerManager.Post(model);
               // _loggingService.LogInfo(MethodBase.GetCurrentMethod().DeclaringType.Name + " : " +MethodBase.GetCurrentMethod().Name, "Region Added", null);
                return Ok(result);
            }
            catch (Exception ex)
            {
                ResultVM resultModel = new ResultVM();
                // _loggingService.LogError(ex);
                // throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
                resultModel.Code = int.Parse(KAASI.Entity.Common.Enums.ResponseCodes.ServerError.ToString());
                resultModel.Exception = ex.ToString();
                resultModel.IsSuccess = 0;
                resultModel.DateAdded = DateTime.Now;
                return Ok(resultModel);
            }
        }
    }
}
