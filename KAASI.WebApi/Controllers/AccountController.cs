﻿
using KAASI.Entity.View;
using KAASI.Utilities.LoggingService;
using KAASI.WebApi.Common;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace KAASI.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private ILoggingService _loggingService;
        private BL.User.IUserManager _userManager;

        public AccountController(BL.User.IUserManager userManager, ILoggingService loggingService)
        {
            this._userManager = userManager;
            this._loggingService = loggingService;
        }

        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _userManager.Register(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
               // _loggingService.LogError(ex);
                throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
            }
        }


        [Route("EditUser")]
        public async Task<IHttpActionResult> EditUser(EditBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _userManager.EditUser(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
              //  _loggingService.LogError(ex);
                throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
            }
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _userManager.ChangePassword(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
              //  _loggingService.LogError(ex);
                throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
            }
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _userManager.RemoveLogin(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
               // _loggingService.LogError(ex);
                throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
            }
        }

        #region HelperMethods
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
        #endregion
    }
}
