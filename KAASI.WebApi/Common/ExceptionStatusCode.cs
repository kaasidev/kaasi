﻿using KAASI.WebApi.Constants;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace KAASI.WebApi.Common
{
    public static class ExceptionStatusCode
    {
        public static HttpStatusCode GetHttpStatusByExceptionMessage(string message)
        {
            if (message.ToLower().Contains("could not find"))
            {
                return HttpStatusCode.NotFound;
            }
            else if (message.ToLower().Contains("not supplied"))
            {
                return HttpStatusCode.BadRequest;
            }
            else if (message.ToLower().Contains("not implemented"))
            {
                return HttpStatusCode.NotImplemented;
            }
            return HttpStatusCode.BadRequest;
        }

        public static HttpResponseException ThrowHttpErrorResponseAndMessage(Exception ex)
        {
            string errorMessage = string.Empty;
            var statusCode = HttpStatusCode.InternalServerError;
            return (HttpResponseException)ex;
            //switch (ex.GetType().Name)
            //{
            //    case "HttpResponseException":
            //        return (HttpResponseException)ex;
            //        break;
            //    case "NotImplementedException":
            //        statusCode = HttpStatusCode.NotImplemented;
            //        errorMessage = "The method or operation is not implemented.";
            //        break;
            //    case "SystemException":
            //        statusCode = HttpStatusCode.InternalServerError;
            //        errorMessage = "A failed run time check; used as a base class for other exceptions.";
            //        break;
            //    case "ArgumentException":
            //        errorMessage = "An argument to a method was invalid.";
            //        break;
            //    case "ArgumentNullException":
            //        errorMessage = "A null argument was passed to a method that does not accept it.";
            //        break;
            //    case "BadImageFormatException":
            //        statusCode = HttpStatusCode.UnsupportedMediaType;
            //        errorMessage = "Image is in wrong format.";
            //        break;
            //    case "DivideByZeroException":
            //        errorMessage = "An attempt was made to divide by Zero.";
            //        break;
            //    case "FormatException":
            //        errorMessage = "The format of an argument is wrong.";
            //        break;
            //    case "InvalidCastException":
            //        errorMessage = "An attempt was made to cast to an invalid object/property.";
            //        break;
            //    case "InvalidOperationException":
            //        errorMessage = "A method was called at an invalid time.";
            //        break;
            //    case "NotSupportedException":
            //        errorMessage = "Indicates that a method is not implemented by a class.";
            //        break;
            //    case "NullReferenceException":
            //        errorMessage = "Indicates that a method is not implemented by a class.";
            //        break;
            //    case "OutOfMemoryException":
            //        errorMessage = "Not enough memory to continue execution.";
            //        break;
            //    case "StackOverflowException":
            //        errorMessage = "A Stack has overflowed";
            //        break;
            //    case "SqlException":
            //        statusCode = HttpStatusCode.ServiceUnavailable;
            //        errorMessage = "Data base error.";
            //        break;
            //    default:
            //        errorMessage = "Un handeled exception";
            //        break;
            //}
           // Logger logger = LogManager.GetLogger(WebConstants.LoggerName);
            
            //var responseMessage = new HttpResponseMessage(statusCode)
            //{
            //    Content = new StringContent(errorMessage)
            //};
            //return new HttpResponseException(responseMessage);
        }
    }
}