﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.WebApi.Common
{
    public static class Enums
    {
        public  enum ExceptionType
        {
            HttpResponseException,
            NotImplementedException,
            SystemException,
            ArgumentException,
            ArgumentNullException,
            BadImageFormatException,
            DivideByZeroException,
            FormatException,
            InvalidCastException,
            InvalidOperationException,
            NotSupportedException,
            NullReferenceException,
            OutOfMemoryException,
            StackOverflowException,
        }
        
    }
}