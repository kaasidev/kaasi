﻿using System;
using ECG.Portal.WebApi.Constants;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using ECG.Portal.WebApi.Formats;

namespace ECG.Portal.WebApi.Providers
{
    public class OAuthEcgAuthorizationServerOptions : IOAuthAuthorizationServerOptions
    {
        private IOAuthAuthorizationServerProvider _provider;

        public OAuthEcgAuthorizationServerOptions(IOAuthAuthorizationServerProvider provider)
        {
            _provider = provider;
        }

        public Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerOptions GetOptions()
        {

            return new Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerOptions()
            {
                TokenEndpointPath = new PathString(WebConstants.GetAppSetting("TokenEndPoint")),
                Provider = _provider,
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(WebConstants.TokenExpirationTime),
                //TODO: In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true,
                AccessTokenFormat = new JwtFormat(WebConstants.GetAppSetting("Issuer"))
                // RefreshTokenProvider = _tokenProvider
            };
        }
    }
}