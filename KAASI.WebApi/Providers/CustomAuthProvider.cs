﻿using KAASI.BL.Common;
using KAASI.DAL.Abstract;
using KAASI.DAL.Concrete;
using KAASI.Utilities.LoggingService;
using KAASI.WebApi.Common;
using KAASI.WebApi.Constants;
using KAASI.WebApi.Formats;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace KAASI.WebApi.Providers
{
    public class CustomAuthProvider : OAuthAuthorizationServerProvider
    {
        // Logger logger = LogManager.GetLogger(WebConstants.LoggerName);

        private readonly string _publicClientId = "self";

        private IPartyManagerRepo _partyManagerRepo;
        private ILoggingService _loggingService;
        public CustomAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                IDbContext dbContext = new KAASI.DAL.Concrete.DBContext.DbContext(WebConstants.ConnectionName);
                _partyManagerRepo = new PartyManagerRepo(dbContext);
                var userDetails = _partyManagerRepo.GetUserDetailsByUserName(context.UserName);
                if (userDetails == null)
                {
                    context.SetError("invalid_grant", "Invalid Credentials");
                    return;
                }

                bool isValidPWD = SecurityUtils.VerifyHashedPassword(userDetails.PasswordHash, context.Password);
                if (!isValidPWD)
                {
                    context.SetError("invalid_grant", "Invalid Credentials");
                    return;
                }

                var userRoles = _partyManagerRepo.GetRolesByUserId(userDetails.ID);
                var claimsIdentity = new ClaimsIdentity("JWT");

                claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                claimsIdentity.AddClaim(new Claim("clientID", context.ClientId));
                foreach (var userRole in userRoles)
                {
                    claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, userRole));
                }

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                         "audience", (context.ClientId == null) ? string.Empty : context.ClientId
                    }
                });

                var ticket = new AuthenticationTicket(claimsIdentity, props);
                context.Validated(ticket);
                //Add user name for logging purpose
                GlobalDiagnosticsContext.Set("UserName", context.UserName);
                // throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                _loggingService = new LoggingService();
                _loggingService.LogError(ex);
                throw ExceptionStatusCode.ThrowHttpErrorResponseAndMessage(ex);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            string symmetricKeyAsBase64 = string.Empty;

            //first try to get the client details from the Authorization Basic header
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                //no details in the Authorization Header so try to find matching post values
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (string.IsNullOrWhiteSpace(context.ClientId))
            {
                context.SetError("invalid_clientId", "client_Id is not set");
                return Task.FromResult<object>(null);
            }

            //ToDo: Discuss and change the audience whether fetch from db or file like Audience store specified below
            AudiencesStore audienceStore = new AudiencesStore();
            var audience = audienceStore.FindAudience(context.ClientId);

            if (audience == null)
            {
                context.SetError("invalid_clientId", string.Format("Invalid client_id '{0}'", context.ClientId));
                return Task.FromResult<object>(null);
            }

            context.Validated();
            return Task.FromResult<object>(null);
        }


        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }

    }
}