﻿namespace KAASI.WebApi.Providers
{
    public interface IOAuthAuthorizationServerOptions
    {
        Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerOptions GetOptions();
    }
}
