﻿using System;

using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using KAASI.WebApi.Formats;
using KAASI.WebApi.Constants;

namespace KAASI.WebApi.Providers
{
    public class OAuthKaasiAuthorizationServerOptions : IOAuthAuthorizationServerOptions
    {
        private IOAuthAuthorizationServerProvider _provider;

        public OAuthKaasiAuthorizationServerOptions(IOAuthAuthorizationServerProvider provider)
        {
            _provider = provider;
        }

        public Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerOptions GetOptions()
        {

            return new Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerOptions()
            {
                TokenEndpointPath = new PathString(WebConstants.TokenEndPoint),
                Provider = _provider,
                AuthorizeEndpointPath = new PathString(WebConstants.AuthorizeEndpointPath),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(WebConstants.TokenExpirationTime),
                AllowInsecureHttp = WebConstants.AllowInsecureHttp,
                AccessTokenFormat = new JwtFormat(WebConstants.JWTTokenIssuer)
            };
        }
    }
}