﻿using KAASI.DAL.Abstract;
using KAASI.WebApi.Formats;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Amazon.CognitoIdentityProvider.Model;
using System.Configuration;
using Amazon.CognitoIdentityProvider;
using System.IdentityModel.Tokens;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon;

namespace KAASI.WebApi.Providers
{
    public class CognitoAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId = "self";
        private readonly AmazonCognitoIdentityProviderClient _client =
           new AmazonCognitoIdentityProviderClient(ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretAccessKey"], RegionEndpoint.USWest2);
        private readonly string _clientId = ConfigurationManager.AppSettings["CLIENT_ID"];
        private readonly string _poolId = ConfigurationManager.AppSettings["USERPOOL_ID"];

        public CognitoAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }


     
    }
}