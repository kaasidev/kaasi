﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using Microsoft.Owin;
using Ninject;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using KAASI.WebApi.Constants;

[assembly: OwinStartup(typeof(KAASI.WebApi.Startup))]

namespace KAASI.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            app.UseNinjectMiddleware(NinjectConfig.CreateKernel);

            //Checking whether to use Owin+Identity or Owin+Custom implementation
            switch (WebConstants.RegisteredOwinType)
            {
                case "AspNetIdentity":
                    ConfigureOAuthTokenConsumption(app);
                    ConfigureIdentityAuth(app);
                    break;
                case "Custom":
                    ConfigureCustomAuth(app);
                    ConfigureOAuthTokenConsumption(app);
                    break;
                case "AwsCongnito":
                    ConfigureAwsCongnitoAuth(app);
                    ConfigureOAuthTokenConsumption(app);
                    break;
                default:
                    break;
            }

            var config = new HttpConfiguration();

            WebApiConfig.Register(config);

            app.UseNinjectWebApi(config);
        }

        public void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            var issuer = WebConstants.JWTTokenIssuer;
            var audience = WebConstants.ClientId;
            var secret = TextEncodings.Base64Url.Decode(WebConstants.SecretKey);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { audience },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, secret)
                    },
                    Provider = new OAuthBearerAuthenticationProvider
                    {
                        OnValidateIdentity = context =>
                        {
                            context.Ticket.Identity.AddClaim(new System.Security.Claims.Claim("newCustomClaim", "newValue"));
                            return Task.FromResult<object>(null);
                        }
                    }
                });

        }


    }
}
