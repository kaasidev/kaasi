﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

using NLog;
using System.Web.Http.Filters;
using ECG.WebApi.Common;
using System.Configuration;
using System.Web;
using System.Web.Http;
using ECG.WebApi.Constants;

namespace ECG.WebApi.Attributes
{
    public class CustomHandleErrorAttribute : ExceptionFilterAttribute 
    {
        Logger logger = LogManager.GetLogger(WebConstants.LoggerName);

        public override void OnException(HttpActionExecutedContext context)
        {

            if (context.Exception != null)
            {
                var statusCode = ExceptionStatusCode.GetHttpStatusByExceptionMessage(context.Exception.Message);
                GlobalDiagnosticsContext.Set("UserName", (HttpContext.Current.User.Identity).Name);
                logger.Error(statusCode.ToString(), context.Exception);
                if (context.Exception is HttpResponseException)
                {
                    throw context.Exception;
                }
                else
                {
                    var message = new HttpResponseMessage(statusCode)
                    {
                        Content = new StringContent(context.Exception.Message)
                    };
                    throw new HttpResponseException(message);
                }
            }

        }
    }
}
