﻿using KAASI.DAL.Abstract;
using KAASI.DAL.Concrete;
using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using KAASI.BL.Constants;
using KAASI.WebApi.Constants;
using Ninject;
using NLog;
using System.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace KAASI.WebApi.Attributes
{
    public class CustomAuthorizeAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
       

        public bool CheckExpire(string tokenString)
        {
            var payloadBytes = tokenString.Split('.')[1];

            //=> Padding the raw payload with "=" chars to reach a length that is multiple of 4
            var mod4 = payloadBytes.Length % 4;
            if (mod4 > 0) payloadBytes += new string('=', 4 - mod4);

            //=> Decoding the base64 string
            var payloadBytesDecoded = Convert.FromBase64String(payloadBytes);

            //=> Retrieve the "exp" property of the payload's JSON
            var payloadStr = System.Text.Encoding.UTF8.GetString(payloadBytesDecoded, 0, payloadBytesDecoded.Length);
            var payload = JsonConvert.DeserializeAnonymousType(payloadStr, new { Exp = 0UL });

            //=> Comparing the exp timestamp to the current timestamp
            var currentTimestamp = (ulong)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

            var boolVal = currentTimestamp < (payload.Exp - 60);

            return boolVal;

        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            var context = actionContext.Request.Headers.Authorization.ToString();
            ClaimsIdentity claimsIdentity = HttpContext.Current.User.Identity as ClaimsIdentity;
            var jwtEncodedString = context.Substring(7); // trim 'Bearer ' from the start since its just a prefix for the token string
            JwtSecurityToken token = null;
            var checkExpire = false;
            string errorMessage;
            if (jwtEncodedString != "undefined")
            {
                token = new JwtSecurityToken(jwtEncodedString: jwtEncodedString);
                checkExpire = CheckExpire(context);


                
                    string controllerName = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                string actionName = actionContext.ActionDescriptor.ActionName;
                var Role = "";
                var userName = "";
                var mainUsername = "";
                if (checkExpire != false) { 
                var username = token.Claims.Where(x => x.Type == "unique_name").Single().Value;
                    
                if (username == "Guest")
                {
                        mainUsername = username;
                        Role = token.Claims.Where(x => x.Type == "role").Single().Value;
                    userName = token.Claims.Where(x => x.Type == "personID").Single().Value;
                }
                else
                {
                    Role = "Customer";
                    userName = token.Claims.Where(x => x.Type == "unique_name").Single().Value;
                }

                }
                var accessResult = checkExpire;


                //Get User Name from Claims

                // var roleDetails = _userManagerRepo.GetRoleDetailsByUserName(userName);

                var mainusername = token.Claims.Where(x => x.Type == "unique_name").Single().Value;

                if (mainusername == "Guest")
                {
                    accessResult = true;
                }

                if (!accessResult)
                {
                    errorMessage = Constant.AuthorizationDeniedErrorMessage;
                    throw new HttpResponseException(actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, errorMessage));
                }
                else
                {
                    GlobalDiagnosticsContext.Set("UserName", userName);
                    //var roleDetail = (from detail in roleDetails where detail.ResourceId.ToUpper() == controllerName.ToUpper() select detail).First();

                    if (!actionContext.Request.Headers.Contains("HasDataManipulationAccess"))
                    {
                        actionContext.Request.Headers.Add("HasDataManipulationAccess", Role);
                    }

                    base.OnActionExecuting(actionContext);
                }

            }
            else {

                errorMessage = Constant.NotLoginErrorMessage;
                throw new HttpResponseException(actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, errorMessage));
            }
        }
    }
}