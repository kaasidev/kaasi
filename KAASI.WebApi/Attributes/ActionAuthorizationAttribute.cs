﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace KAASI.WebApi.Attributes
{
    public class ActionAuthorizationAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string errorMessage;
            var manipulationCheckHeader = actionContext.Request.Headers.Where(x => x.Key == "HasDataManipulationAccess").FirstOrDefault();

            if (manipulationCheckHeader.Key == null)
            {
                errorMessage = "You are not authorized to perform this operation.";
                throw new HttpResponseException(actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, errorMessage));
            }
            else
            {
                var hasAccessForDataManipulation = manipulationCheckHeader.Value.FirstOrDefault();
                if (hasAccessForDataManipulation == null || hasAccessForDataManipulation.ToLower() == "false")
                {
                    errorMessage = "You are not authorized to perform this operation.";
                    throw new HttpResponseException(actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, errorMessage));
                }
                else
                {
                    base.OnActionExecuting(actionContext);
                }
            }
        }
    }
}