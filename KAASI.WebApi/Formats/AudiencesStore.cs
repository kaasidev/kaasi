﻿using KAASI.DAL.Abstract;
using KAASI.DAL.Concrete;
using KAASI.WebApi.Constants;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Ninject;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace KAASI.WebApi.Formats
{
    public class AudiencesStore
    {
        public ConcurrentDictionary<string, Audience> AudiencesList = new ConcurrentDictionary<string, Audience>();

        private IPartyManagerRepo _partyManagerRepo;
        public AudiencesStore()
        {
            IDbContext dbContext = new KAASI.DAL.Concrete.DBContext.DbContext(WebConstants.ConnectionName);
            _partyManagerRepo = new PartyManagerRepo(dbContext);
            var clientIds = _partyManagerRepo.GetAllClientIds();
            foreach (var client in clientIds)
            {
                AudiencesList.TryAdd(client.SecretID.ToString().ToUpper(),
                    new Audience
                    {
                        ClientId = client.SecretID.ToString().ToUpper(),
                        Base64Secret = WebConstants.SecretKey,
                        Name = WebConstants.ResourceServer
                    });
            }
        }

        public Audience AddAudience(string name)
        {
            var clientId = Guid.NewGuid().ToString("N");

            var key = new byte[32];
            RNGCryptoServiceProvider.Create().GetBytes(key);
            var base64Secret = TextEncodings.Base64Url.Encode(key);

            Audience newAudience = new Audience { ClientId = clientId, Base64Secret = base64Secret, Name = name };
            AudiencesList.TryAdd(clientId, newAudience);
            return newAudience;
        }

        public Audience FindAudience(string clientId)
        {
            Audience audience = null;
            if (AudiencesList.TryGetValue(clientId.ToUpper(), out audience))
            {
                return audience;
            }
            return null;
        }
    }
}