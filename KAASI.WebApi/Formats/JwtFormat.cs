﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.IdentityModel.Tokens;

namespace KAASI.WebApi.Formats
{
    public class JwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private const string AudiencePropertyKey = "audience";

        private readonly string _issuer = string.Empty;

        public JwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            string audienceId = data.Properties.Dictionary.ContainsKey(AudiencePropertyKey) ? data.Properties.Dictionary[AudiencePropertyKey] : null;

            if (string.IsNullOrWhiteSpace(audienceId))
            {
                throw new InvalidOperationException("AuthenticationTicket. Properties does not include audience");
            }
            AudiencesStore audienceStore = new AudiencesStore();
            Audience audience = audienceStore.FindAudience(audienceId);

            string symmetricKeyAsBase64 = audience.Base64Secret;

            var symmetricKeyInByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);

            var signingKeyByAlgorithm = new SigningAlgorithms(symmetricKeyInByteArray);

            var issuedDateTimeInUTC = data.Properties.IssuedUtc;
            var expiresIn = data.Properties.ExpiresUtc;

            var jwtSecurityToken = new JwtSecurityToken(_issuer, audienceId, data.Identity.Claims, issuedDateTimeInUTC.Value.UtcDateTime, expiresIn.Value.UtcDateTime, signingKeyByAlgorithm);

            var jwtSecurityTokenhandler = new JwtSecurityTokenHandler();

            var jwt = jwtSecurityTokenhandler.WriteToken(jwtSecurityToken);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }

}