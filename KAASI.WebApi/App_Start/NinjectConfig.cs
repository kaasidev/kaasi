using System.Web.Http;
using KAASI.WebApi.Attributes;
using KAASI.WebApi.Constants;
using KAASI.WebApi.Models;
using KAASI.WebApi.Providers;
using Microsoft.Owin.Security.OAuth;
using Ninject.Web.WebApi;

namespace KAASI.WebApi
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using KAASI.BL.User;
    using KAASI.DAL.Abstract;
    using KAASI.DAL.Concrete;
    using KAASI.BL.RoleManager;
    using KAASI.WebApi.UserManager;
    using Ninject.Web.WebApi.FilterBindingSyntax;
    using KAASI.Utilities.LoggingService;
    using KAASI.BL.LanguageAndCurrency;
    using BL.CustomerManager;

    public static class NinjectConfig
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static Action<IKernel> RebindAction { get; set; } // should be used only in integration testing scenarios

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel(new NinjectSettings() { InjectNonPublic = true });
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            // Comment code sections based on settings
            #region Identity
            //kernel.Bind<IUserManager>().To<IdentityUserManager>();
            #endregion Identity

            #region Custom
            kernel.Bind<IDbContext>().To<DBContext.DbContext>().InRequestScope().WithConstructorArgument("connectionStringName", WebConstants.ConnectionName);
            kernel.Bind<IDbConnectionFactory>().To<DbConnectionFactory>().InRequestScope();
            kernel.Bind<IUserManagerRepo>().To<UserManagerRepo>();
            kernel.Bind<IUserManager>().To<CustomUserManager>();
            kernel.Bind<IRolesManager>().To<RolesManager>();
            kernel.Bind<IRolesRepo>().To<RolesRepo>();
            kernel.Bind<ILanguageAndCurrencyRepo>().To<LanguageAndCurrencyRepo>();
            kernel.Bind<ILanguageAndCurrencyManager>().To<LanguageAndCurrencyManager>();
            kernel.Bind<ICustomerManagerRepo>().To<CustomerManagerRepo>();
            kernel.Bind<ICustomerManager>().To<CustomerManager>();

            #endregion Custom

            #region Error logging - NLog
            kernel.Bind<ILoggingService>().To<LoggingService>();
            #endregion
        }
    }
}
