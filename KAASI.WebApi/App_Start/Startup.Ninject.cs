﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using ECG.Portal;
using ECG.Portal.WebAPI.Models;
using ECG.Portal.WebAPI.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

namespace ECS.Portal
{
    public partial class Startup
    {
        public IKernel ConfigureNinject(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var kernel = CreateKernel();
            app.UseNinjectMiddleware(() => kernel)
               .UseNinjectWebApi(config);

            return kernel;
        }

        public IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            return kernel;
        }

        private void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IOAuthAuthorizationServerOptions>()
                .To<OAuthECGAuthorizationServerOptions>();
            kernel.Bind<IOAuthAuthorizationServerProvider>()
                .To<ApplicationOAuthProvider>();
            //kernel.Bind<IAuthenticationTokenProvider>().To<RefreshTokenProvider>();
            //kernel.Bind<UserManager<ApplicationUser>>().To<new ApplicationUserManager.Create>();
        }
    }

    }