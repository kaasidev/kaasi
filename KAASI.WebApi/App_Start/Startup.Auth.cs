﻿using System;
using System.Collections.Generic;
using System.Linq;
using KAASI.WebApi.Models;
using KAASI.WebApi.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;



namespace KAASI.WebApi
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public static string PublicClientId { get; private set; }

        //Registering Owin to use AspNet Identity
        public void ConfigureIdentityAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";

            IdentityAuthProvider provider = new IdentityAuthProvider(PublicClientId);
            IOAuthAuthorizationServerOptions oAuthAuthorizationServerOptions = new OAuthKaasiAuthorizationServerOptions(provider);

            app.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions.GetOptions());
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }

        public void ConfigureCustomAuth(IAppBuilder app)
        {
            //app.CreatePerOwinContext(ApplicationDbContext.Create);
            //app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";

            CustomAuthProvider provider = new CustomAuthProvider(PublicClientId);
            IOAuthAuthorizationServerOptions oAuthAuthorizationServerOptions = new OAuthKaasiAuthorizationServerOptions(provider);

            app.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions.GetOptions());
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        public void ConfigureAwsCongnitoAuth(IAppBuilder app)
        {
            //app.CreatePerOwinContext(ApplicationDbContext.Create);
            //app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";

            CognitoAuthProvider provider = new CognitoAuthProvider(PublicClientId);
            IOAuthAuthorizationServerOptions oAuthAuthorizationServerOptions = new OAuthKaasiAuthorizationServerOptions(provider);

            app.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions.GetOptions());
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

    }
}
