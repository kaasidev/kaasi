﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.WinApplication
{
    public class DailyScheduleTask
    {
        private ILog logger;
        public DailyScheduleTask(UnitOfWorks xmlUnitOfWork)
        {

            logger = log4net.LogManager.GetLogger(typeof(DailyScheduleTask));
          

            // loggertbl = new LogService();
        }
        internal void ActiveToPullData()
        {
            //  string currentDate = apiClient.GetCurrentDateTime().ToShortDateString();

            //loggertbl.Log("date:" + currentDate, Enterprise.Enums.LogModes.Info, "", "", "");
            //    bool isSchedullerAlreadyRun = CheckAlreadyRunScheduller(currentDate);
            //  if (isSchedullerAlreadyRun == false)
            // {
            RunTask();
            // }
            // else
            // {
            //   logger.Info("Skipped date:" + currentDate);
            // loggertbl.Log("Skipped date:" + currentDate, Enterprise.Enums.LogModes.Info, "", "", "");
            //}
        }
        private void RunTask()
        {
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            TransactionService service = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var proccesingList = service.GetAll(x => x.Status == "PROCESSING", null, "").ToList();//is checking status PROCESSING or COMPLIANCE CHECK?
            foreach (var item in proccesingList)
            {
                Customer cust = custSer.GetAll(x => x.CustomerID == item.CustomerID.Value, null, "").SingleOrDefault();
                bool isAnalyseMonthlyDollarLimit = AnalyseMonthlyDollarLimit(item.CustomerID.Value, service);
                bool isAnalyseMonthlyNbTransLimit = AnalyseMonthlyNbTransLimit(item.CustomerID.Value, service);
                bool isAnalyseDocumentExpiry = AnalyseDocumentExpiry(item.CustomerID.Value);
                bool isAnalyseMonthlyVariation = AnalyseMonthlyVariation(item.CustomerID.Value, service);
                if (cust.RiskLevel == 4)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Customer Risk level is 4", service);
                }
                else if (isAnalyseMonthlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                }
                else if (isAnalyseMonthlyNbTransLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                }
                else if (isAnalyseDocumentExpiry)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Primary Document(s) Expired", service);
                    ChangeRiskLevel(cust, 4, custSer);
                }
                else if (isAnalyseMonthlyVariation)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly Variation Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                }
                else
                {
                    ChangeTransactionStatus(item, "PENDING", "", service);
                }
            }

        }
        private bool disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
               
            this.disposed = true;
        }

        public static void ChangeRiskLevel(Customer customer, int riskLevel, CustomerService service)
        {
            customer.RiskLevel = riskLevel;

            service.Update(customer);
        }

        public async Task<int> AnalyseTransaction(String TransactionID)
        {
            int x = await Task.FromResult(0);
            return x;

            // If all of the below method are not true, then put the transaction [dbo].[Transactions].[Status] into 'PENDING'
        }
        public void ChangeTransactionStatus(Transaction trans, string status, string reason, TransactionService service)
        {
            trans.Status = status;
            if (!string.IsNullOrEmpty(reason))
            {
                trans.ComplianceReason = reason;
            }
             service.Update(trans);
        }
        public bool AnalyseMonthlyDollarLimit(int custId, TransactionService ser)
        {
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            var customer = custSer.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault();
            decimal spendingPower = 0;

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));
            if (customer.SpendingPower.HasValue)
            {
                spendingPower = customer.SpendingPower.Value;
            }
            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate, null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);

            if (spendingPower < totAmount)
            {

                return true;
            }
            else
            {
                return false;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer has a monthly limit set in [dbo].[Customers].[SpendingPower].
            // If combined Transaction AUD Total [dbo].[Transactions].[DollarAmount] for this month exceed above SpendingPower, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly AUD Limit Exceeded'
        }

        public bool AnalyseMonthlyNbTransLimit(int custId, TransactionService ser)
        {

            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var settings = setSer.GetAll(x => x.SettingName == "MonthlyAUDLimit", null, "").SingleOrDefault();
            decimal monthlyLimit = Convert.ToDecimal(settings.SettingsVariable);

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate, null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);

            if (totAmount > monthlyLimit)
            {
                return true;
            }
            else
            {
                return false;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Global setting in [dbo].[Settings].[SettingName] = MonthlyTransLimit gives the maximum amount of transactions per month before it gets flagged
            // If COUNT(*) Transactions for that customer for this month exceed above MonthlyTransLimit, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Number of Transaction Limit Exceeded'
        }

        public bool AnalyseDocumentExpiry(int custId)
        {
            CustomerDocumentService ser = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            var docList = ser.GetDocumentList(custId).ToList();
            if (docList.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer Primary Documents are in [dbo].[CustomerDocuments].[TypeOfDocument] = 'PRIMARY'
            // There can be multiple primary documents. If ALL primary documents have expired, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Primary Document(s) Expired'
        }

        public bool AnalyseMonthlyVariation(int custId, TransactionService trSer)
        {
            SettingsService ser = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            int varMonths = int.Parse(ser.GetAll(x => x.SettingName == "MonthlyVariationMonths", null, "").SingleOrDefault().SettingsVariable);
            decimal varValue = Convert.ToDecimal(ser.GetAll(x => x.SettingName == "MonthlyVariation", null, "").SingleOrDefault().SettingsVariable);

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trthismonthList = trSer.GetAll(x => x.CreatedDateTime >= monthFirstDate, null, "").ToList();
            decimal totAmount = trthismonthList.Sum(x => x.DollarAmount.Value);
            int thisMonth = DateTime.Now.Month;
            int thisYear = DateTime.Now.Year;

            for (int i = 1; i <= varMonths; i++)
            {
                thisMonth = thisMonth - i;
                if (thisMonth == 0)
                {
                    thisMonth = 12;
                    thisYear = thisYear - 1;
                }
                DateTime fromDate = Convert.ToDateTime("01/" + thisMonth.ToString("00") + "/" + thisYear);
                DateTime toDate = fromDate.AddMonths(1).AddDays(-1);
                var trList = trSer.GetAll(x => x.CustomerID == custId && x.CreatedDateTime < toDate && x.CreatedDateTime > fromDate, null, "").ToList();
                decimal totTrVal = trList.Sum(x => x.DollarAmount.Value);
                if (Math.Abs(totAmount - totTrVal) > 1000)
                {
                    return true;
                }
            }

            return false;
            // CID --> CustomerID
            // TID --> TransactionID

            // Look at the total for the last x Months ([dbo].[Settings].[SettingName] = MonthlyVariationMonths) 
            //of transactions in Dollar Amount.
            // The allowed variation is in [dbo].[Settings].[SettingName] = 'MonthlyVariation'
            // If the variation between the total for each month is greater than the MonthlyVariation, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW' 
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Variation Exceeded'
        }
    }
}
