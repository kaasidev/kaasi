﻿using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kapruka.WinApplication
{
   class StartService
    {
        log4net.ILog logger;

        // LogService loggertbl;
        public StartService()
        {
            logger = log4net.LogManager.GetLogger(typeof(StartService));
            log4net.Config.XmlConfigurator.Configure();
            logger.Info("Service started");
        }
        public void Start()
        {
            var t = new Thread(new ThreadStart(delegate
            {
                while (true)
                {
                    try
                    {
                        DailyScheduleTask shedCls = new DailyScheduleTask(new UnitOfWorks(new KaprukaEntities()));
                        shedCls.ActiveToPullData();
                        logger.Info("Success");
                       

                    }
                    catch (Exception ex)
                    {
                        logger.Error("Service exception", ex);
                        logger.Info("service stopped");
                       
                        // throw;
                    }
                    logger.Info("Time cheking datetime:" + DateTime.Now.ToString());
                    Thread.Sleep(120000);//sleep for 2mins
                }
            }));

            t.Start();
        }

        public static void Stop(string serviceName, int timeoutMilliseconds)
        {
            //ServiceController service = new ServiceController(serviceName);
            //try
            //{
            //    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

            //    service.Stop();
            //    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            //}
            //catch
            //{
            //    // ...
            //}
        }
    }
}
