﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.WinApplication
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }
        private void DoWork()
        {
            StartService scheduller = new StartService();
            scheduller.Start();
        }
        protected override void OnStart(string[] args)
        {
            DoWork();
        }

        protected override void OnStop()
        {
        }
    }
}
