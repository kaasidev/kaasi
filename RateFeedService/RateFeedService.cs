﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace RateFeedService
{
    public partial class RateFeedService : ServiceBase
    {
        public RateFeedService()
        {
            InitializeComponent();
        }

        public class RateFeedService
        {
            public string CurrencyCode { get; set; }
            public float rate { get; set; }
        }

        protected override void OnStart(string[] args)
        {
            string URL = "https://openexchangerates.org/api/latest.json";
            string urlParameters = "?app_id=f9eb582f496e4f12bd20d45ed12a959d";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var dataObjects = response.Content.ReadAsAsync<IEnumerable<RateFeedService>>().Result;
                foreach (var d in dataObjects)
                {
                    Console.WriteLine("{0}", d.CurrencyCode);
                }
            }
        }

        protected override void OnStop()
        {
        }
    }
}
