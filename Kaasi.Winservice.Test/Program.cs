﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Kaasi.Winservice.Test
{
    class Program
    {
       
        static void Main(string[] args)
        {
            try
            {
                int id = 7199;
              // getCustomerPassword();
                CustomerService ser = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                var cust = ser.GetGivenCustomer(id);
                string objt = JsonConvert.SerializeObject(cust);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }
            //CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            //TransactionService service = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            //var proccesingList = service.GetAll(x => x.Status == "PROCESSING", null, "").ToList();
            //foreach (var item in proccesingList)
            //{
            //    Customer cust = custSer.GetAll(x => x.CustomerID == item.CustomerID.Value, null, "").SingleOrDefault();
            //    bool isAnalyseMonthlyDollarLimit = AnalyseMonthlyDollarLimit(item.CustomerID.Value, service);
            //    bool isAnalyseMonthlyNbTransLimit = AnalyseMonthlyNbTransLimit(item.CustomerID.Value, service);
            //    bool isAnalyseDocumentExpiry = AnalyseDocumentExpiry(item.CustomerID.Value);
            //    bool isAnalyseMonthlyVariation = AnalyseMonthlyVariation(item.CustomerID.Value, service);

            //    if (isAnalyseMonthlyDollarLimit)
            //    {
            //        ChangeTransactionStatus(item, "REVIEW", "Monthly AUD Limit Exceeded", service);
            //    }
            //    else if (isAnalyseMonthlyNbTransLimit)
            //    {
            //        ChangeTransactionStatus(item, "REVIEW", "Monthly AUD Limit Exceeded", service);
            //    }
            //    else if (isAnalyseDocumentExpiry)
            //    {
            //        ChangeTransactionStatus(item, "REVIEW", "Primary Document(s) Expired", service);
            //    }
            //    else if (isAnalyseMonthlyVariation)
            //    {
            //        ChangeTransactionStatus(item, "REVIEW", "Monthly Variation Exceeded", service);
            //    }
            //    else
            //    {
            //        ChangeTransactionStatus(item, "PENDING", "", service);
            //    }
            //}
            TestWebApi("");
        }
        public static void getCustomerPassword()
        {
           
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            string sql = "select * from Customers where CustomerID=7199";
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            Kapruka.Repository.Customer cust = new Customer();
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    string dd = idr["PreviousCustomerID"].ToString();
                    cust.ABN= idr["ABN"].ToString();
                    if(!string.IsNullOrEmpty(idr["PreviousCustomerID"].ToString()))
                    {
                        cust.PreviousCustomerID = int.Parse(idr["PreviousCustomerID"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["AccountBalance"].ToString()))
                    {
                        cust.AccountBalance = decimal.Parse(idr["AccountBalance"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["UnclearedFunds"].ToString()))
                    {
                        cust.UnclearedFunds = decimal.Parse(idr["UnclearedFunds"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["MembershipStatus"].ToString()))
                    {
                        cust.MembershipStatus = int.Parse(idr["MembershipStatus"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["Verified"].ToString()))
                    {
                        cust.Verified = int.Parse(idr["Verified"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["HundredPoints"].ToString()))
                    {
                        cust.HundredPoints = int.Parse(idr["HundredPoints"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["RiskLevel"].ToString()))
                    {
                        cust.RiskLevel = int.Parse(idr["RiskLevel"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["SpendingPower"].ToString()))
                    {
                        cust.SpendingPower = decimal.Parse(idr["SpendingPower"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["AgentID"].ToString()))
                    {
                        cust.AgentID = int.Parse(idr["AgentID"].ToString());
                    }
                    if (!string.IsNullOrEmpty(idr["CustomerType"].ToString()))
                    {
                        cust.CustomerType = int.Parse(idr["CustomerType"].ToString());
                    }
                    cust.CustomerID= int.Parse(idr["RiskLevel"].ToString());

                }
            }
            conn.Close();
           // return CustomerPassword;
        }

        public async Task<int> AnalyseTransaction(String TransactionID)
        {
            int x = await Task.FromResult(0);
            return x;

            // If all of the below method are not true, then put the transaction [dbo].[Transactions].[Status] into 'PENDING'
        }
        public static void ChangeTransactionStatus(Transaction trans, string status, string reason, TransactionService service)
        {
            trans.Status = status;
            if (!string.IsNullOrEmpty(reason))
            {
                trans.ComplianceReason = reason;
            }
            // service.Update(trans);
        }
        public static void ChangeRiskLevel(Customer customer, int riskLevel, CustomerService service)
        {
            customer.RiskLevel = riskLevel;

            service.Update(customer);
        }
        public static bool AnalyseMonthlyDollarLimit(int custId, TransactionService ser)
        {
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            var customer = custSer.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault();
            decimal spendingPower = 0;

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));
            if (customer.SpendingPower.HasValue)
            {
                spendingPower = customer.SpendingPower.Value;
            }
            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate, null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);

            if (spendingPower < totAmount)
            {

                return true;
            }
            else
            {
                return false;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer has a monthly limit set in [dbo].[Customers].[SpendingPower].
            // If combined Transaction AUD Total [dbo].[Transactions].[DollarAmount] for this month exceed above SpendingPower, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly AUD Limit Exceeded'
        }

        public static bool AnalyseMonthlyNbTransLimit(int custId, TransactionService ser)
        {

            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var settings = setSer.GetAll(x => x.SettingName == "MonthlyAUDLimit", null, "").SingleOrDefault();
            decimal monthlyLimit = Convert.ToDecimal(settings.SettingsVariable);

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate, null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);

            if (totAmount > monthlyLimit)
            {
                return true;
            }
            else
            {
                return false;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Global setting in [dbo].[Settings].[SettingName] = MonthlyTransLimit gives the maximum amount of transactions per month before it gets flagged
            // If COUNT(*) Transactions for that customer for this month exceed above MonthlyTransLimit, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Number of Transaction Limit Exceeded'
        }

        public static bool AnalyseDocumentExpiry(int custId)
        {
            CustomerDocumentService ser = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            var docList = ser.GetDocumentList(custId).ToList();
            if (docList.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer Primary Documents are in [dbo].[CustomerDocuments].[TypeOfDocument] = 'PRIMARY'
            // There can be multiple primary documents. If ALL primary documents have expired, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Primary Document(s) Expired'
        }

        public static bool AnalyseMonthlyVariation(int custId, TransactionService trSer)
        {
            SettingsService ser = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            int varMonths = int.Parse(ser.GetAll(x => x.SettingName == "MonthlyVariationMonths", null, "").SingleOrDefault().SettingsVariable);
            decimal varValue = Convert.ToDecimal(ser.GetAll(x => x.SettingName == "MonthlyVariation", null, "").SingleOrDefault().SettingsVariable);

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trthismonthList = trSer.GetAll(x => x.CreatedDateTime >= monthFirstDate, null, "").ToList();
            decimal totAmount = trthismonthList.Sum(x => x.DollarAmount.Value);
            int thisMonth = DateTime.Now.Month;
            int thisYear = DateTime.Now.Year;

            for (int i = 1; i <= varMonths; i++)
            {
                thisMonth = thisMonth - i;
                if (thisMonth == 0)
                {
                    thisMonth = 12;
                    thisYear = thisYear - 1;
                }
                DateTime fromDate = Convert.ToDateTime("01/" + thisMonth.ToString("00") + "/" + thisYear);
                DateTime toDate = fromDate.AddMonths(1).AddDays(-1);
                var trList = trSer.GetAll(x => x.CustomerID == custId && x.CreatedDateTime < toDate && x.CreatedDateTime > fromDate, null, "").ToList();
                decimal totTrVal = trList.Sum(x => x.DollarAmount.Value);
                if (Math.Abs(totAmount - totTrVal) > 1000)
                {
                    return true;
                }
            }

            return false;
            // CID --> CustomerID
            // TID --> TransactionID

            // Look at the total for the last x Months ([dbo].[Settings].[SettingName] = MonthlyVariationMonths) 
            //of transactions in Dollar Amount.
            // The allowed variation is in [dbo].[Settings].[SettingName] = 'MonthlyVariation'
            // If the variation between the total for each month is greater than the MonthlyVariation, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW' 
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Variation Exceeded'
        }

        public static void TestWebApi(string paramss)
        {

            var baseAddress = "http://kyc.novatti.com:9090/api/1.0/kyc/customer";

            var anonString = new { title = "mr", man = "ajeets", bfn = "Ajeet", bln = "Singh" };

            var json = JsonConvert.SerializeObject(anonString);

            var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            string parsedContent = json;
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();
            // http.
            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
        }

    }
}
