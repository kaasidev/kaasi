﻿using ECG.Portal.DAL.Abstract;
using ECG.Portal.Entity.Domain;
using ECG.Portal.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECG.Portal.BL
{
    public class PortalMgmt
    {
        private readonly IPortalRepo _portalRepo;

        public PortalMgmt(IPortalRepo portalRepo)
        {
            _portalRepo = portalRepo;
        }

        public int Save(PortalVM vVM)
        {
            return _portalRepo.Insert(new PortalDM
                {
                    PortalName = vVM.PortalName,
                    PatientName = vVM.PatientName,
                    PatientId = vVM.PatientId,
                    IsBereavement = vVM.IsBereavement,
                    InterventionId = vVM.InterventionId,
                    InterventionsAddressedById = vVM.InterventionsAddressedById,
                    UnableToVisit = vVM.UnableToVisit,
                    UnableToVisitReason = vVM.UnableToVisitReason,
                    AttemptedVisit = vVM.AttemptedVisit,
                    VeternVisit = vVM.VeternVisit,
                    IsThereAnythingElse = vVM.IsThereAnythingElse,
                    VisitDate = vVM.VisitDate,
                    TimeIn = TimeSpan.Parse(vVM.TimeIn),
                    TimeOut = TimeSpan.Parse(vVM.TimeOut),
                    Mileage = vVM.Mileage,
                    TravelTimeHours = vVM.TravelTimeHours,
                    TravelTimeMins = vVM.TravelTimeMins,
                    VisitLocation = vVM.VisitLocation,
                    ProgramHierarchy_NEWDivId = vVM.ProgramHierarchy_NEWDivId,
                    IsCorrection = vVM.IsCorrection,
                    Certify = vVM.Certify,
                    CreatedDate = vVM.CreatedDate
                }
            );
        }

    }
}
