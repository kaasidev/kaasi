﻿using KAASI.Entity.Domain;
using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.BL.RoleManager
{
    public interface IRolesManager
    {
        string SaveUserRole(RoleTypeVM role);
        void RemoveRoleById(int Id);
        IQueryable<RoleTypeVM> GetAllRoles();
        RoleTypeVM GetRoleById(int id);
    }
}
