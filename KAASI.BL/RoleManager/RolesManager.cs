﻿using KAASI.DAL.Abstract;
using KAASI.Entity.Domain;
using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.BL.RoleManager
{
    public class RolesManager : IRolesManager
    {
        private readonly IRolesRepo _rolesMgmtRepo;
        public RolesManager(IRolesRepo roleMgmtRepo)
        {
            _rolesMgmtRepo = roleMgmtRepo;
        }

        public string SaveUserRole(RoleTypeVM role)
        {
            var roles = _rolesMgmtRepo.GetAllRoles();
            bool isRoleExists = false;
            if (role.RoleId == 0)
            {
                isRoleExists = roles.Where(x => x.Description.ToLower() == role.Description.ToLower()).Any();
            }
            else
            {
                isRoleExists = roles.Where(x => (x.Description.ToLower() == role.Description.ToLower()) && (x.ID != role.RoleId)).Any();
            }
            if (!isRoleExists)
            {
                _rolesMgmtRepo.SaveUserRole(role.ToEntityModel());
                return "Success";
            }
            else
            {
                return "Failed, role name has to be unique.";
            }
        }
        public void RemoveRoleById(int Id)
        {
            _rolesMgmtRepo.RemoveRoleById(Id);
        }
        public IQueryable<RoleTypeVM> GetAllRoles()
        {
            return ConvertRolesDMToVM(_rolesMgmtRepo.GetAllRoles());
        }
        public RoleTypeVM GetRoleById(int id)
        {
            var rolesDM = _rolesMgmtRepo.GetRoleById(id);

            return rolesDM == null ? null : RoleTypeVM.ToViewModel(rolesDM);
        }


        #region Helpers
        private IQueryable<RoleTypeVM> ConvertRolesDMToVM(IQueryable<RoleTypeDM> rolesDM)
        {
            return (from role in rolesDM
                    select new RoleTypeVM
                    {
                        RoleId = role.ID,
                        Description = role.Description
                    });
        }
        #endregion
    }
}
