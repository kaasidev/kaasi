﻿using ECG.Portal.DAL.Abstract;
using ECG.Portal.Entity.Domain;
using ECG.Portal.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ECG.Portal.BL
{
    public class UserMgmt
    {
        private readonly IUserMgmtRepo _userMgmtRepo;
        public UserMgmt(IUserMgmtRepo userMgmtRepo)
        {
            _userMgmtRepo = userMgmtRepo;
        }
        public void Register(UserVM user,string password)
        {
            user.PasswordHash = EncryptPassword(password);
            user.SecurityStamp = Guid.NewGuid().ToString();
            _userMgmtRepo.RegisterUser(user.ToEntityModel());
        }

        public UserVM AuthenticateUserByEmailAndPassword(string email, string password)
        {
            string passwordHash = EncryptPassword(password);
           UserDM userDm = _userMgmtRepo.AuthenticateUserByEmailAndPassword(email, passwordHash);
           return UserVM.ToViewModel(userDm);
        }

        private string EncryptPassword(string password)
        {
            try
            {
                MD5 md5Encryption = new MD5CryptoServiceProvider();
                //compute hash from the bytes of text
                md5Encryption.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));

                //get hash result after compute it
                byte[] result = md5Encryption.Hash;

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    //change it into 2 hexadecimal digits
                    //for each byte
                    strBuilder.Append(result[i].ToString("x2"));
                }
                return strBuilder.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
