﻿using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.BL.LanguageAndCurrency
{
    public interface ILanguageAndCurrencyManager
    {
        string SaveLanguageAndCurrency(LanguageAndCurrencyVM dataModel);
        IEnumerable<LanguageAndCurrencyVM> GetAll();
    }
}
