﻿using KAASI.DAL.Abstract;
using KAASI.Entity.Domain;
using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.BL.LanguageAndCurrency
{
    public class LanguageAndCurrencyManager : ILanguageAndCurrencyManager
    {
        private readonly ILanguageAndCurrencyRepo _languageAndCurrencyRepo;

        public LanguageAndCurrencyManager(ILanguageAndCurrencyRepo languageAndCurrencyRepo)
        {
            _languageAndCurrencyRepo = languageAndCurrencyRepo;
        }

        public string SaveLanguageAndCurrency(LanguageAndCurrencyVM viewModel)
        {
            return _languageAndCurrencyRepo.SaveLanguageAndCurrency(viewModel.ToEntityModel());
        }

        public IEnumerable<LanguageAndCurrencyVM> GetAll()
        {
            return ConvertEntityToModel(_languageAndCurrencyRepo.GetAll());
        }
        private IEnumerable<LanguageAndCurrencyVM> ConvertEntityToModel(IEnumerable<LanguageAndCurrencyDM> vieModel)
        {
            return (from model in vieModel
                    select new LanguageAndCurrencyVM
                    {
                        Language = model.Language,
                        Text = model.Text,
                        Currency = model.Currency
                    });
        }
    }
}
