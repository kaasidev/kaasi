﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace KAASI.BL.Constants
{
    public static class Constant
    {
        public static string DefaultUserRole
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("DefaultUserRole");
            }
        }
        public static string UserNotAuthenticatedErrorMessage
        {
            get { return ConfigurationManager.AppSettings.Get("Security.UserNotAuthenticatedErrorMessage"); }
        }
        public static string AuthorizationDeniedErrorMessage
        {
            get { return ConfigurationManager.AppSettings.Get("Security.AuthorizationDeniedErrorMessage"); }
        }
        public static string NotLoginErrorMessage
        {
            get { return ConfigurationManager.AppSettings.Get("Security.UserNotLoggedInErrorMessage"); }
        }
    }
}