﻿using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.BL.User
{
    public interface IUserManager : IDisposable
    {
        Task<string> Register(RegisterBindingModel model);
        Task<string> EditUser(EditBindingModel model);
        Task<string> ChangePassword(ChangePasswordBindingModel model);
        Task<string> RemoveLogin(RemoveLoginBindingModel model);
    }
}
