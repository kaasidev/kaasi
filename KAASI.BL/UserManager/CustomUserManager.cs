﻿using KAASI.BL.Common;
using KAASI.BL.Constants;
using KAASI.BL.User;
using KAASI.DAL.Abstract;
using KAASI.Entity.View;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
namespace KAASI.BL.User
{
    public class CustomUserManager : IUserManager
    {

        private readonly IUserManagerRepo _userManagerRepo;
        public CustomUserManager(IUserManagerRepo userMgmtRepo)
        {
            _userManagerRepo = userMgmtRepo;
        }



        public async Task<string> Register(RegisterBindingModel model)
        {
            #region UserName/Email Validation
            //Pattern check
            string isUserNameValid = IsUserNameValid(model.UserName);
            if (!string.IsNullOrWhiteSpace(isUserNameValid))
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(isUserNameValid)
                };
                throw new HttpResponseException(message);
                // return isUserNameValid;
            }
            #endregion

            #region Password Validation
            string isPasswordValid = IsPasswordValid(model.Password);
            if (!string.IsNullOrWhiteSpace(isPasswordValid))
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(isPasswordValid)
                };
                throw new HttpResponseException(message);
                // return isPasswordValid;
            }
            #endregion

            #region Roles Validation
            //   string[] roles = null;
            //Validate Roles
            //Assign default role, if role is null or empty
            //if (string.IsNullOrEmpty(model.Role))
            //{
            //    //return "Please enter the role.";
            //    roles = new string[] { Constant.DefaultUserRole };
            //}
            //else
            //{
            //    roles = model.Role.Split(',');
            //}
            ////Get the roles exists in Database
            //var rolesExistsInSystem = _userManagerRepo.GetAllRoles();

            //foreach (string role in roles)
            //{
            //    if (!rolesExistsInSystem.Any(x => x.Description.ToLower() == role.ToLower()))
            //    {
            //        var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
            //        {
            //            Content = new StringContent(string.Format("{0} Role is not exists in the system", role))
            //        };
            //        throw new HttpResponseException(message);
            //        //return string.Format("{0} Role is not exists in the system", role);
            //    }
            //}

            #endregion

            UserVM userVM = new UserVM();
            userVM.PasswordHash = SecurityUtils.HashPassword(model.Password);
            userVM.SecurityStamp = Guid.NewGuid().ToString();
            userVM.UserId = Guid.NewGuid().ToString();
            userVM.UserName = model.UserName;
            userVM.Email = model.UserName;
            userVM.EmailConfirmed = false;
            userVM.PhoneNumberConfirmed = false;
            userVM.TwoFactorEnabled = false;
            userVM.LockoutEnabled = false;
            userVM.CreatedDate = DateTime.Now;
            userVM.CreatedBy = model.UserName;
            //userVM.PartyDescription = model.PartyName;
            userVM.PartyID = model.PartyID;

            var userSaveResult = _userManagerRepo.RegisterUser(userVM.ToEntityModel());

            if (userSaveResult != null)
            {
                return userSaveResult;
            }
            //Create roles
            //if (string.IsNullOrEmpty(model.Role))
            //{
            //    model.Role = Constant.DefaultUserRole;
            //}
            //foreach (string role in roles)
            //{
            //    _userManagerRepo.CreateUserRole(userVM.UserId, role);
            //}

            return "User registered successfully.";
        }

        public async Task<string> EditUser(EditBindingModel model)
        {
            var userDetails = _userManagerRepo.GetUserDetailsByUserName(model.UserName);
            if (userDetails == null)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("User id is not valid.")
                };
                throw new HttpResponseException(message);
            }

            //Validate the username and email
            var isValidUser = IsUserNameValidToUpdate(model.UserName, userDetails.ID);
            if (!string.IsNullOrWhiteSpace(isValidUser))
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(isValidUser)
                };
                throw new HttpResponseException(message);
                //return isValidUser;
            }
            userDetails.Email = model.Email;
            userDetails.EmailConfirmed = model.EmailConfirmed;
            userDetails.TwoFactorAuthPhoneNumber = model.PhoneNumber;
            userDetails.PhoneNumberConfirmed = model.PhoneNumberConfirmed;
            userDetails.TwoFactorEnabled = model.TwoFactorEnabled;
            userDetails.LockoutEndDateUtc = model.LockoutEndDateUtc;
            userDetails.LockoutEnabled = model.LockoutEnabled;
            userDetails.UpdatedBy = model.UserName;
            userDetails.UpdatedDate = DateTime.Now;

            _userManagerRepo.EditUserDetailsByUserId(userDetails);

            //Roles Validation
            //if (!string.IsNullOrEmpty(model.Role))
            //{
            //    ManageRole(model.Role, userDetails.ID);
            //}
            return "Udated successfully";
        }

        public async Task<string> ChangePassword(ChangePasswordBindingModel model)
        {
            //Check user is valid or not
            var userDetails = _userManagerRepo.GetUserDetailsByUserName(model.UserName);

            if (userDetails == null)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Please enter valid User Name.")
                };
                throw new HttpResponseException(message);
            }

            //Valdiate old password
            var oldPassword = userDetails.PasswordHash;
            if (!SecurityUtils.VerifyHashedPassword(oldPassword, model.OldPassword))
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Please enter valid old password.")
                };
                throw new HttpResponseException(message);

            }
            //Validate new password
            string isNewPasswordValid = IsPasswordValid(model.NewPassword);
            if (!string.IsNullOrWhiteSpace(isNewPasswordValid))
            {
                //return isNewPasswordValid;
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(isNewPasswordValid)
                };
                throw new HttpResponseException(message);
            }

            //Change the password
            _userManagerRepo.ChangePasswordByUserId(userDetails.ID, SecurityUtils.HashPassword(model.NewPassword));
            return "Password changed successfully.";
        }

        public async Task<string> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (string.IsNullOrWhiteSpace(model.UserName))
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("User Name is invalid.")
                };
                throw new HttpResponseException(message);
                //return "User id is invalid.";
            }
            var userDetails = _userManagerRepo.GetUserDetailsByUserName(model.UserName);
            if (userDetails == null)
            {
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("User Name doesn't exists.")
                };
                throw new HttpResponseException(message);
            }
            _userManagerRepo.RemoveUserLogin(userDetails.ID);
            return "Removed the user login successfully.";
        }

        #region " Dispose "

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: free managed resources 
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposedValue);
            GC.SuppressFinalize(this);
        }

        #endregion " Dispose "

        #region Helpers
        private string IsPasswordValid(string password)
        {
            string PasswordPattern = @"^(?=.*[0-9])(?=.*[!@#$%^&*])[0-9a-zA-Z!@#$%^&*0-9]{8,}$";

            //Minimum length validation
            if (string.IsNullOrEmpty(password) || password.Length < 6)
            {
                return string.Format("Minimum Password Length Required is:{0}", 6);
            }

            if (!Regex.IsMatch(password, PasswordPattern))
            {
                return string.Format("The Password must have at least one numeric, more than 2 characters and one special character.");
            }
            return string.Empty;
        }

        private string IsUserNameValid(string userName)
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                    + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                    + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            if (!regex.IsMatch(userName))
            {
                return "Please enter valid Email address";
            }
            //Check is email address is unique or not. If not, return
            var userDetails = _userManagerRepo.GetUserDetailsByUserName(userName);
            if (userDetails != null)
            {
                return "UserName already exists. Please select different username";
            }
            return string.Empty;
        }

        private string IsUserNameValidToUpdate(string userName, string id)
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                   + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                   + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            if (!regex.IsMatch(userName))
            {
                return "Please enter valid Email address";
            }
            //Check is email address is unique or not. If not, return
            var userDetails = _userManagerRepo.GetUserDetailsListByUserName(userName);
            if (userDetails.Where(x => x.ID != id).Any())
            {
                return "UserName already exists. Please select different username";
            }
            return string.Empty;
        }

        #endregion


        private void ManageRole(string inRole, string id)
        {
            var roles = inRole.Split(',');
            //Get the roles exists in Database
            var rolesExistsInSystem = _userManagerRepo.GetAllRoles();

            foreach (string role in roles)
            {
                if (!rolesExistsInSystem.Any(x => x.Description.ToLower() == role.ToLower()))
                {
                    string errorMessage = string.Format("{0} Role is not exists in the system", role);
                    var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(errorMessage)
                    };
                    throw new HttpResponseException(message);
                }
            }

            //Delete the roles associated to the user by id
            _userManagerRepo.RemoveUserRolesByUserId(id);

            //Assign the new roles
            foreach (string role in roles)
            {
                _userManagerRepo.CreateUserRole(id, role);
            }
        }
    }
}
