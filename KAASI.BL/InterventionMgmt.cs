﻿using ECG.Portal.DAL.Abstract;
using ECG.Portal.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECG.Portal.BL
{
    public class InterventionMgmt
    {
        private readonly IInterventionRepo _interventionRepo;

        public InterventionMgmt(IInterventionRepo interventionRepo)
        {
            _interventionRepo = interventionRepo;
        }

        public List<InterventionDM> GetAll()
        {
            return _interventionRepo.GetAll();
        }

    }
}
