﻿using KAASI.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.BL.CustomerManager
{
   public interface ICustomerManager
    {
        Task<ResultVM> Post(CustomerVM model);
    }
}
