﻿using KAASI.DAL.Abstract;
using KAASI.Entity.View;
using KAASI.Utilities.LoggingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.BL.CustomerManager
{
   public class CustomerManager:ICustomerManager
    {
        private readonly ICustomerManagerRepo _customerManagerRepo;
        private ILoggingService _loggingService;
        public CustomerManager(ICustomerManagerRepo customerManageRepo, ILoggingService loggingMgmtService)
        {
            _customerManagerRepo = customerManageRepo;
            _loggingService = loggingMgmtService;
        }

        public async Task<ResultVM> Post(CustomerVM model)
        {
            return _customerManagerRepo.AddCustomer(model); ;
        }
    }
}
