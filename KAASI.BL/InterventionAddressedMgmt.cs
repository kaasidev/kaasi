﻿using ECG.Portal.DAL.Abstract;
using ECG.Portal.Entity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECG.Portal.BL
{
    public class InterventionAddressedMgmt
    {
        private readonly IInterventionsAddressedByRepo _interventionAddressedRepo;

        public InterventionAddressedMgmt(IInterventionsAddressedByRepo _interventionAddressed)
        {
            _interventionAddressedRepo = _interventionAddressed;
        }

        public List<InterventionsAddressedByDM> GetAll()
        {
            return _interventionAddressedRepo.GetAll();
        }

    }
}
