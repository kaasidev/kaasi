﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace Kapruka.Domain
{
    public class MyContextKaasiModelQ : DbContext
    {
        public MyContextKaasiModelQ()
          : base("MydbConnModel")  
       {
           Database.SetInitializer<MyContextKaasiModelQ>(new CreateDatabaseIfNotExists<MyContextKaasiModelQ>());  
             
       }  

      public DbSet<InboundIFTI> InboundIFTIs { get; set; }
     
    }
}
