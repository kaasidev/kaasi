﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
   public class CustomerOrMerchantResponseKyced
    {
       public string agentTransactionId { get; set; }
       public string transactionId { get; set; }
       public string resultCode { get; set; }
       public string resultDescription { get; set; }

    }
}
