﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
   public class CustomerKyced
    {
       public string profile { get; set; }
       public string title { get; set; }
       public string man { get; set; }
       public string bfn { get; set; }
       public string bln { get; set; }
       public string dob { get; set; }
       public string securityNumber { get; set; }
       public string tea { get; set; }
       public string bsn { get; set; }
       public string bco { get; set; }
        public string bz { get; set; }
        public string bs { get; set; }
        public string bc { get; set; }
        public string pw { get; set; }
        public string ip { get; set; }
       public string documentImageFront { get; set; }
       public string documentImageBack { get; set; }
       public string faceImage { get; set; }


    }
}
