﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
    public enum CustomerType
    {
        Individual = 1,
        Merchant = 2,
    }

    public enum CustomerINKYCE
    {
        Yes=1,
        No=2,
    }

    public enum LoginTypes
    {
        Inwards = 1,
        Outwards = 2,
        Both = 3
    }
}
