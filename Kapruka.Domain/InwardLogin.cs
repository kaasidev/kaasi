﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Kapruka.Domain
{
    public class InwardLogin
    {
        [Key]
        public int LoginID { get; set; }
        public String CompanyName { get; set; }
        public String LoginEmail { get; set; }
        public String FullName { get; set; }
        public String Status { get; set; }
        public String LoginType { get; set; }
    }
}
