﻿using System;
using System.IO;
using System.Reflection;

namespace Kapruka.Domain
{
    public class LogWriter
    {
        private string m_exePath = string.Empty;

        public LogWriter(string logmessage)
        {
            LogWrite(logmessage);
        }

        public void LogWrite(string logmessage)
        {
            m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "\\" + "kaasilog.txt"))
                {
                    Log(logmessage, w);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Log(string logmessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                txtWriter.WriteLine(" :");
                txtWriter.WriteLine(" :{0}", logmessage);
                txtWriter.WriteLine("------------------------------");
            }
            catch (Exception ex)
            {

            }
        }
    }
}
