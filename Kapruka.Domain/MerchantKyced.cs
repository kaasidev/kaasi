﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
   public class MerchantKyced
    {
       public string profile { get; set; }
       public string amn { get; set; }
       public string ataxId { get; set; }
       public string afn { get; set; }
       public string aln { get; set; }
       public string asn { get; set; }
       public string ac { get; set; }
       public string @as { get; set; }
       public string aco { get; set; }
       public string az { get; set; }
       public string aph { get; set; }
       public string dba { get; set; }
       public string bin { get; set; }
       public string website { get; set; }

       
    
    }
}
