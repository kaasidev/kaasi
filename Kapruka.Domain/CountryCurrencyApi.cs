﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
   public class CountryCurrencyApi
    {
       public int CurrencyID { get; set; }
       public string CurrencyName { get; set; }
       public string CurrencyCode { get; set; }
      
    }

   public class MasterCompanyBuyRatesData
   {
       public int CurrencyID { get; set; }
       public string Createdby { get; set; }
   }
}
