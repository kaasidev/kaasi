﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
    public class CustomerForKyc
    {
        public int RecordID { get; set; }
        public int CustomerID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string DOB { get; set; }
        public string EmailAddress { get; set; }
        public string HasKYC { get; set; }
        public int CustomerType { get; set; }
        public string ABN { get; set; }
        public string BusinessName { get; set; }
        public string TradingName { get; set; }
        public string StreetName { get; set; }
        public string City { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string RiskLevel { get; set; }
        public byte[] DocumentImage1 { get; set; }
        public byte[] FaceImage { get; set; }
        public string DocumentType { get; set; }
        public DateTime KYCCheckDate { get; set; }
        public string KCYResult { get; set; }
        public string KCYRunBy { get; set; }
        public string agentTransactionID { get; set; }
        public string resultDescription { get; set; }
        public string TransactionID { get; set; }
        public string resultCode { get; set; }
        public bool IsIndividual { get; set; }

    }

    public class CustomerDocs
    {

        public int RecordID { get; set; }
        public int CustomerID { get; set; }
        public bool hasDriverLicence { get; set; }
        public bool hasPassport { get; set; }
        public string DocumentDriverImage1 { get; set; }
        public byte[] DocumentPassportImage1 { get; set; }
        public string DocumentName { get; set; }
        public string TypeOfDocument { get; set; }
        public string TypeOfDocumentID { get; set; }
        public string DocumentNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string DocumentGUID { get; set; }

    }



    public class ResultKycCall
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }

    public class KYCAuditIDRun
    {
        
        public int CustomerID { get; set; }
        public string agentTransactionID { get; set; }
        public string Result { get; set; }
        public string RunBy { get; set; }
        public string OverrideNotes { get; set; }

    }

    public class AgentKYC
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string AgentAddress1 { get; set; }
        public string AgentCity { get; set; }
        public string AgentState { get; set; }
        public string AgentPostcode { get; set; }
        public string AgentPhone { get; set; }
        public string AgentFax { get; set; }
        public string AgentEmail { get; set; }
        public string AgentABN { get; set; }
        public string AgentACN { get; set; }
        public string AustracNumber { get; set; }
        public string StreetName { get; set; }
        public string Country { get; set; }
    }

}
