﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
    public class ValidOldestDoc
    {
        public int RecordID { get; set; }
        public int CustomerID { get; set; }
        public bool hasDriverLicence { get; set; }
        public bool hasPassport { get; set; }
        public string DocumentDriverImage1 { get; set; }
        public byte[] DocumentPassportImage1 { get; set; }
        public string DocumentName { get; set; }
        public string TypeOfDocument { get; set; }
        public string TypeOfDocumentID { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string DocumentGUID { get; set; }
    }
}
