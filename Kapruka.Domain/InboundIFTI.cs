﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;  
namespace Kapruka.Domain
{
    public class InboundIFTI
    {
        // Transaction Details
        public DateTime MoneyRecFromCustDateTime { get; set; }
        public DateTime MoneyAvaToBeneDateTime { get; set; }
        public String CurrencyCode { get; set; }
        public float TotalAmount { get; set; }
        public String TypeOfTransfer { get; set; }
        public String DescOfProperty { get; set; }
        public String TransRefNumber { get; set; }

        // Ordering Customer
        public String FullName { get; set; }
        public String AKA { get; set; }
        public DateTime? DOB { get; set; }

        // Ordering Customer Contact Details
        public String Address { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Postcode { get; set; }
        public String Country { get; set; }
        public String PostalAddress { get; set; }
        public String PACity { get; set; }
        public String PAState { get; set; }
        public String PAPostcode { get; set; }
        public String PACountry { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }

        // Ordering Customer Business Details
        public String Occupation { get; set; }
        public String ABN { get; set; }
        public String CustomerNumber { get; set; }
        public String AccountNumber { get; set; }
        public String BusinessStructure { get; set; }

        // Ordering Customer Identification Details -- THIS SECTION IS NOT PRESENT IN INWARDS TRANSFERS
        public String IDType { get; set; }
        public String IDTypeOther { get; set; }
        public String IDNumber { get; set; }
        public String Issuer { get; set; }
        public String IDType2 { get; set; }
        public String IDType2Other { get; set; }
        public String IDType2Number { get; set; }
        public String IDType2Issuer { get; set; }
        public String ElectronicDataSource { get; set; }

        // Beneficiary Customer
        public String BeneFullName { get; set; }
        public DateTime? BeneDOB { get; set; }
        public String BeneBusinessName { get; set; }

        // Beneficiary Customer Contact Details
        public String BeneAddress { get; set; }
        public String BeneCity { get; set; }
        public String BeneState { get; set; }
        public String BenePostcode { get; set; }
        public String BeneCountry { get; set; }
        public String BenePostalAddress { get; set; }
        public String BenePACity { get; set; }
        public String BenePAState { get; set; }
        public String BenePAPostcode { get; set; }
        public String BenePACountry { get; set; }
        public String BenePhone { get; set; }
        public String BeneEmail { get; set; }

        // Beneficiary Business Details
        public String BeneOccupation { get; set; }
        public String BeneABN { get; set; }
        public String BeneBusinessStructure { get; set; }

        // Beneficiary Account Details
        public String BeneAccountNumber { get; set; }
        public String BeneNameOfInstitution { get; set; }
        public String BeneAccountCity { get; set; }
        public String BeneAccountCountry { get; set; }

        // Organisation Accepting the Transfer Instructions
        public String IDOfRetail { get; set; } // NOT PRESENT IN INWARDS FILE
        public String RetailFullName { get; set; }
        public String RetailOtherName { get; set; }
        public String RetailAKA { get; set; }
        public DateTime? RetailDOB { get; set; }
        public String RetailAddress { get; set; }
        public String RetailCity { get; set; }
        public String RetailState { get; set; }
        public String RetailPostcode { get; set; }
        public String RetailCountry { get; set; }
        public String RetailPostalAddress { get; set; }
        public String RetailPACity { get; set; }
        public String RetailPAState { get; set; }
        public String RetailPAPostcode { get; set; }
        public String RetailPACountry { get; set; }
        public String RetailPhone { get; set; }
        public String RetailEmail { get; set; }
        public String RetailAcceptingMoney { get; set; }
        public String RetailSendingInstructions { get; set; }


        // Organisation Accepting Business Details
        public String RetailOccupation { get; set; }
        public String RetailABN { get; set; }
        public String RetailBusinessStructure { get; set; }

        // Organisation Accepting the Money 
        public String MAOFullName { get; set; }
        public String MAOAddress { get; set; }
        public String MAOCity { get; set; }
        public String MAOState { get; set; }
        public String MAOPostcode { get; set; }
        public String MAOCountry { get; set; }

        // Organistion Sending The Transfer Instructions
        public String OSIFullName { get; set; }
        public String OSIOtherName { get; set; }
        public String OSIAKA { get; set; }
        public DateTime? OSIDOB { get; set; }
        public String OSIAddress { get; set; }
        public String OSICity { get; set; }
        public String OSIState { get; set; }
        public String OSIPostcode { get; set; }
        public String OSICountry { get; set; }
        public String OSIPostalAddress { get; set; }
        public String OSIPACity { get; set; }
        public String OSIPAState { get; set; }
        public String OSIPAPostcode { get; set; }
        public String OSIPACountry { get; set; }
        public String OSIPhone { get; set; }
        public String OSIEmail { get; set; }
        public String OSIOccupation { get; set; }
        public String OSIABN { get; set; }
        public String OSIBusinessStructure { get; set; }

        // Organisation Receiving Transfer Instruction
        public String ORIFullName { get; set; }
        public String ORIAddress { get; set; }
        public String ORICity { get; set; }
        public String ORIState { get; set; }
        public String ORIPostcode { get; set; }
        public String ORICountry { get; set; }
        public String ORIDistributing { get; set; }
        public String ORISeparateRetail { get; set; }

        // Organisation Distrubing Money
        public String ODMFullName { get; set; }
        public String ODMAddress { get; set; }
        public String ODMCity { get; set; }
        public String ODMState { get; set; }
        public String ODMPostcode { get; set; }
        public String ODMCountry { get; set; }

        // Retail Money is Distributed
        public String RMDFullName { get; set; }
        public String RMDAddress { get; set; }
        public String RMDCity { get; set; }
        public String RMDState { get; set; }
        public String RMDPostcode { get; set; }
        public String RMDCountry { get; set; }

        // Reason
        public String Reason { get; set; }

        // Person Completing This Report
        public String PCRFullName { get; set; }
        public String PCRJobTitle { get; set; }
        public String PCRPhone { get; set; }
        public String PCREmail { get; set; }

        // Import Results
        public String ImportStatus { get; set; }
        public String ErrorList { get; set; }

        // Custom Variables
        public int AgentID { get; set; }
        [Key]
        public int RowNumber { get; set; }
        public DateTime uploadDateTime { get; set; }
    }


    public class InboundIFTIBeneficiaryData
    {
        //Beneficiary FullName
        public String BeneFullName { get; set; }
        // Beneficiary Account Details
        public String BeneAccountNumber { get; set; }
        public string KYCCheckDate { get; set; } 
        public string agentTransactionId { get; set; }
        public string transactionId { get; set; }
        public string resultCode { get; set; }
        public string resultDescription { get; set; }
        public string KYCBy { get; set; }
        public int ID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
       
    }

    public class InboundIFTIBeneficiaryDataUI
    {
    }
}
