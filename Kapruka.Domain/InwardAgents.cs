﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Kapruka.Domain
{
    public class InwardAgents
    {
        [Key]
        public int AgentID { get; set; }
        public String AgentName { get; set; }
        public String AgentCountry { get; set; }
        public String Status { get; set; }
        public string View { get; set; }
    }
}
