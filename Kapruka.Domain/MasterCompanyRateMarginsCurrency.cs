﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Domain
{
   public class MasterCompanyRateMarginsCurrency
    {
       public int CurrencyID { get; set; }
       public string Margin { get; set; }
    }

    public class AgentForRateMarginUpdate{
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public string AgentCommissionStructure { get; set; }

    }

    public class AgentCurrencyMarginsCurrency
    {
        public int AgentID { get; set; }
        public int CurrencyID { get; set; }
        public string Margin { get; set; }
    }

    public class AgentCurrencyRatesCurrency
    {
        public int AgentID { get; set; }
        public int CurrencyID { get; set; }
        public string Margin { get; set; }
    }
}
