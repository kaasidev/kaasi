﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Kapruka.Domain
{
    public class InboundCustomer
    {
        [Key]
        public int CustomerID { get; set; }
        public String FullName { get; set; }
        public DateTime? DOB { get; set; }
        public String BusinessName { get; set; }
        public String AddressLine1 { get; set; }
        public String Suburb { get; set; }
        public String State { get; set; }
        public String Postcode { get; set; }
        public String Country { get; set; }
        public String BusinessStructure { get; set; }
    }

    public class KycResultInward
    {
        public string Status { get; set; }
        public string KycBy { get; set; }
        public string KyCheckDate { get; set; }
        public string TransactionId { get; set; }
        public string ResultDescription { get; set; }

    }
}
