﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kapruka.Domain
{
    class InboundCustomerAccounts
    {
        [Key]
        public int AccountID { get; set; }
        public int CustomerID { get; set; }
        public String BSB { get; set; }
        public String AccountNumber { get; set; }
        public String BankName { get; set; }
        public String Branch { get; set; }
        public String Country { get; set; }

        [ForeignKey("CustomerID")]
        public InboundCustomer InboundCustomer { get; set; }
    }
}
