﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Utilities.Constants
{
    public static class Constants
    {
        public static string LoggerName
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("LoggerName");
            }
        }
    }
}
