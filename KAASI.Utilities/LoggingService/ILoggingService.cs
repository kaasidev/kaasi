﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Utilities.LoggingService
{
    public interface ILoggingService
    {

        void LogError(Exception ex);
        void LogError(string message);

        void LogWarn(Exception ex);
        void LogWarn(string message);

        void LogDebug(Exception ex);

        void LogDebug(string message);

    }
}
