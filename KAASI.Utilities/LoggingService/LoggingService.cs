﻿using NLog;
using NLog.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KAASI.Utilities.LoggingService
{
    public class LoggingService : NLog.Logger, ILoggingService
    {
        private string _loggerName = Constants.Constants.LoggerName;
        private Logger logger = LogManager.GetLogger(Constants.Constants.LoggerName);

        public void LogError(Exception ex)
        {
            logger.Error(ex, ex.Message);
        }
        public void LogError(string message)
        {
            logger.Error(message);
        }

        public void LogWarn(Exception ex)
        {
            logger.Warn(ex, ex.Message);
        }
        public void LogWarn(string message)
        {
            logger.Warn(message);
        }

        public void LogDebug(Exception ex)
        {
            logger.Debug(ex, ex.Message);
        }

        public void LogDebug(string message)
        {
            logger.Debug(message);
        }
    }
}
