﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECG.Utilities.ErrorLogger
{
    public static class ErrorLog
    {
        static Logger logger = LogManager.GetLogger(Constants.Constants.LoggerName);

        public static void LogError(Exception ex)
        {
            logger.Error(ex, ex.Message);
        }
        public static void LogError(string message)
        {
            logger.Error(message);
        }

        public static void LogWarn(Exception ex)
        {
            logger.Warn(ex, ex.Message);
        }
        public static void LogWarn(string message)
        {
            logger.Warn(message);
        }

        public static void LogDebug(Exception ex)
        {
            logger.Debug(ex, ex.Message);
        }

        public static void LogDebug(string message)
        {
            logger.Debug(message);
        }

    }
}
