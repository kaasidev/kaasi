﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(iAffiliate.Startup))]
namespace iAffiliate
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
