﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace iAffiliate.Models
{
    public class InwardLogins
    {
        [Key]
        public int LoginID { get; set; }
        public String FullName { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public String CreatedBy { get; set; }
        public DateTime? AmendedDateTime { get; set; }
        public String AmendedBy { get; set; }
    }
}