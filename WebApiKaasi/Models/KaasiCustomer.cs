﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiKaasi.Models
{
    public class KaasiCustomer
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string DOB { get; set; }
        public string EmailAddress { get; set; }
        public string PlaceOfBirth { get; set; }
        public string CountryOfBirth { get; set; }
        public string Nationality { get; set; }
        public string AKA { get; set; }
        public string Occupation { get; set; }
        public string UnitNo { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string StreetType { get; set; }
        public string City { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string EmailAddressAlternative { get; set; }
        public bool IsIndividual { get; set; }
    }

    public class ResultKaasi
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }

    public class KaasiCustomerResult : ResultKaasi
    {
        public int KaasiCustomerId { get; set; }
    }

    public class Customer
    {
        public string AffiliateCustomerID { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string Title { get; set; }
        public string UnitNo { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string StreetType { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Mobile { get; set; }
        public string TelHome { get; set; }
        public string TelWork { get; set; }
        public string DOB { get; set; }
        public string EmailAddress { get; set; }
        public string Notes { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public string CountryOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string AKA { get; set; }
        public string CustomerType { get; set; }
        public string ABN { get; set; }
    }

    public class BeneficiaryAccount
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string PaymentMethod { get; set; }
        public string Currency { get; set; }
        public string Remarks { get; set; }
        public string Country { get; set; }
    }

    public class Beneficiary
    {
        public string AffiliateBeneficairyID { get; set; }
        public string BeneficiaryFirstName { get; set; }
        public string BeneficiaryLastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string TelHome { get; set; }
        public string TelWork { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string NationalityCardID { get; set; }
        public string  Nationality { get; set; }
        public string Relationship { get; set; }
        public string Notes { get; set; }
        public string CountryOfBirth { get; set; }
        public string DOB { get; set; }
        public string PlaceOfBirth { get; set; }
        public string AKA { get; set; }
        public BeneficiaryAccount BeneficiaryAccount { get; set; }
    }

    public class Transaction
    {
        public string AffilicateTransactionID { get; set; }
      
        public string DollarAmount { get; set; }
        public string ServiceCharge { get; set; }
        public string ExchangeRate { get; set; }
        public string TotalCharge { get; set; }
        public string RemittedAmount { get; set; }
        public string RemittedCurrency { get; set; }
        public string Notes { get; set; }
        public string CreatedDateTime { get; set; }
        public string ApprovalDateTime { get; set; }
        public string Purpose { get; set; }
        public string SourceOfFunds { get; set; }
    }

    public class GlobalTransaction
    {
        public int TransactionDirection { get; set; }
        public Customer Customer { get; set; }
        public Beneficiary Beneficiary { get; set; }
        public Transaction Transaction { get; set; }
    }

    public enum Transactiondirection
    {
        OutBound=1,
        Inbound=2
    }
}