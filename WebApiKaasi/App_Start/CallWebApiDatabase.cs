﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac.Integration.WebApi;
using System.Reflection;
using Kapruka.Enterprise;
using System.Web.Http;
using WebApiKaasi.Controllers;
using Kapruka.Repository;
using Newtonsoft.Json.Serialization;

namespace WebApiKaasi
{
    public class CallWebApiDatabase
    {
        public static void RunMe()
        {
            var builder = new ContainerBuilder();

           // builder.re
          builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //    //  builder.RegisterType<SessionManager>().PropertiesAutowired();
             builder.RegisterType<KaprukaEntities>().InstancePerRequest();


           builder.RegisterAssemblyTypes(typeof(ServiceBase<object>).Assembly)
                      .Where(t => t.Name.EndsWith("Service")).InstancePerRequest();

            builder.RegisterType<Kapruka.Repository.KaasiEntitiesDbContext>().InstancePerRequest();

            builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);

            builder.Register(c => new KaasiAuthorizeAttribute(c.Resolve<KaasiLoginService>()))
                                 .AsWebApiAuthorizationFilterFor<ServiceController>()
                                .InstancePerRequest();
          
            //  builder.RegisterFilterProvider();

            IContainer container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //ObjectMapper.Configure();
        }
    }
}