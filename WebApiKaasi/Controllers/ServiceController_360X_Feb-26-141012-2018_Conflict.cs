﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using WebApiKaasi.Models;

namespace WebApiKaasi.Controllers
{
    public class ServiceController : ApiController
    {
        //
        // GET: /Service/

        [KaasiAuthorizeAttribute]
        [Route("api/getstatus")]
        [HttpGet]
        public string GetStatus()
        {

            return "Hi, Kaasi Service is Up.";
        }


        private static KaasiCustomerResult InsertCustomerRecord(Kapruka.Repository.Customer csObject,
            KaasiCustomerResult csResult)
        {
            try
            {
                CustomerService ser = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                csObject.CustomerID = GetlastCustomerId();
                ser.Add(csObject);

                csResult.Status = true;
                csResult.Message = "Successfully Created.";
            }
            catch (Exception ex)
            {
                csResult.Status = false;
                csResult.Message = "An Error Occurred.";
            }


            return csResult;
        }

        private static void FillCustomer(Models.Customer customer, Kapruka.Repository.Customer customerEntity)
        {
            customerEntity.IsIndividual = true;
            // customerEntity.PreviousCustomerID = customer.AffiliateCustomerID;
            customerEntity.FirstName = customer.CustomerFirstName;
            customerEntity.LastName = customer.CustomerLastName;
            customerEntity.FullName = customer.CustomerFirstName + " " + customer.CustomerLastName;
            customerEntity.CountryOfBirth = customer.CountryOfBirth;
            customerEntity.DOB = customer.DOB;
            customerEntity.PlaceOfBirth = customer.PlaceOfBirth;
            // customerEntity.Nationality = customer.na;
            customerEntity.AKA = customer.AKA;
            customerEntity.Occupation = customer.Occupation;
            customerEntity.UnitNo = customer.UnitNo;
            customerEntity.StreetNo = customer.StreetNo;
            customerEntity.StreetName = customer.StreetName;
            customerEntity.StreetType = customer.StreetType;
            customerEntity.Suburb = customer.Suburb;
            customerEntity.State = customer.State;
            customerEntity.Postcode = customer.Postcode;
            customerEntity.Country = customer.Country;
            customerEntity.TelHome = customer.TelHome;
            customerEntity.TelWork = customer.TelWork;
            customerEntity.Mobile = customer.Mobile;
            customerEntity.EmailAddress = customer.EmailAddress;
            // customerEntity.EmailAddress2 = customer.e;
            customerEntity.CreatedBy = "Kaasi WEB API";
            customerEntity.CreatedDateTime = DateTime.Now;
            customerEntity.Active = "Y";
        }


        private static int GetlastCustomerId()
        {
            var csId = 0;
            var respo = new KaprukaEntities();
            var csIdMax = (from cs in respo.Customers select cs.CustomerID).Max().GetValueOrDefault();

            csId = csIdMax + 1;
            return csId;
        }

        private Agent GetAgent(string userName)
        {
            int agentId = 0;
            var respo = new KaprukaEntities();
            var agentLogin = (from ls in respo.Logins where ls.Username == userName select ls).SingleOrDefault();

            var agent = (from ag in respo.Agents where ag.AgentID == agentLogin.AgentID select ag).SingleOrDefault();

            return agent;
        }

        private Agent GetAgentById(int agentId)
        {
            var agent = new Agent();
            var respo = new KaprukaEntities();
            agent = (from ag in respo.Agents where ag.AgentID == agentId select ag).SingleOrDefault();


            return agent;
        }
        private void WriteFile(string message)
        {
            var fileName = "logmessage.txt";
            string path = @"C:\temp\" + fileName;

            message = DateTime.Now + " : " + message;
            string text2write = message;

            System.IO.StreamWriter writer = new System.IO.StreamWriter(path);
            writer.Write(text2write);
            writer.Close();
        }

        public ResultKaasi CreateCustomerTransaction(GlobalTransaction globalTransaction)
        {
            var result = new ResultKaasi();
            if (globalTransaction == null)
            {
                result.Status = false;
                result.Message = "No object found.";
                return result;
            }

            var userKey = Thread.CurrentPrincipal.Identity.Name;

            var agent = GetAgent(userKey);

            if (globalTransaction.TransactionDirection != (int)Transactiondirection.OutBound ||
                globalTransaction.TransactionDirection != (int)Transactiondirection.Inbound)
            {
                result.Status = false;
                result.Message = "Transaction direction is not found.";
                return result;
            }
            if (globalTransaction.TransactionDirection == (int)Transactiondirection.OutBound)
            {
                var customer = globalTransaction.Customer;
                if (customer == null)
                {
                    result.Status = false;
                    result.Message = "Customer is not found.";
                    return result;
                }
                var beneficiary = globalTransaction.Beneficiary;
                if (beneficiary == null)
                {
                    result.Status = false;
                    result.Message = "Beneficiary is not found.";
                    return result;
                }
                var beneficiaryAccount = globalTransaction.Beneficiary.BeneficiaryAccount;
                if (beneficiaryAccount == null)
                {
                    result.Status = false;
                    result.Message = "Beneficiary Account is not found.";
                    return result;
                }

                var transaction = globalTransaction.Transaction;
                if (transaction == null)
                {
                    result.Status = false;
                    result.Message = "Transaction is not found.";
                    return result;
                }
                var entityCustomer = new Kapruka.Repository.Customer();

                FillCustomer(customer, entityCustomer);

                var beneficiaryEntity = new Kapruka.Repository.Beneficiary();
                FillBeneficiary(beneficiary, beneficiaryEntity, agent, entityCustomer);

                var beneficiaryPayment = new Kapruka.Repository.BeneficiaryPaymentMethod();
                FillBeneficiaryAccounts(beneficiaryAccount, beneficiaryEntity, beneficiaryPayment);

            }

            return result;
        }

      

        private void FillBeneficiary(Models.Beneficiary beneficiary,
            Kapruka.Repository.Beneficiary beneEntity, Kapruka.Repository.Agent agent,
            Kapruka.Repository.Customer customer)
        {
            beneEntity.AddressLine1 = beneficiary.AddressLine1;
            beneEntity.Active = "Y";
            beneEntity.CustomerID = customer.CustomerID;
            beneEntity.AddressLine2 = beneficiary.AddressLine2;
            beneEntity.AKA = beneficiary.AKA;
            beneEntity.BeneficiaryFirstNames = beneficiary.BeneficiaryFirstName;
            beneEntity.BeneficiaryLastName = beneficiary.BeneficiaryLastName;
            beneEntity.Country = beneficiary.Country;
            beneEntity.CountryOfBirth = beneficiary.CountryOfBirth;
            if (!string.IsNullOrEmpty(beneficiary.Country))
                beneEntity.CountryID = getCountryByName(beneficiary.Country);
            beneEntity.CreatedBy = agent.AgentName;
            if (!string.IsNullOrEmpty(beneficiary.DOB))
                beneEntity.DOB = Convert.ToDateTime(beneficiary.DOB);
            beneEntity.EmailAddress = beneficiary.EmailAddress;
            beneEntity.Mobile = beneficiary.Mobile;
            beneEntity.NationalityCardID = beneficiary.NationalityCardID;// beneficiary.
            beneEntity.Notes = beneficiary.Notes;
            beneEntity.PlaceofBirth = beneficiary.PlaceOfBirth;
            beneEntity.Postcode = beneficiary.Postcode;
            beneEntity.Relationship = beneficiary.Relationship;
            beneEntity.State = beneficiary.State;
            beneEntity.Suburb = beneficiary.Suburb;
            beneEntity.TelHome = beneficiary.TelHome;
            beneEntity.TelWork = beneficiary.TelWork;



        }

        private int getCountryByName(string name)
        {
            var respo = new KaprukaEntities();
            var couId = 0;
            var country = (from cs in respo.Countries where cs.CountryName == name select cs).ToList();
            if (country.Count() > 0)
            {
                couId = country[0].CountryID;
            }

            return couId;

        }

        private void FillBeneficiaryAccounts(Models.BeneficiaryAccount beneAccount,
          Kapruka.Repository.Beneficiary beneEntity,
          Kapruka.Repository.BeneficiaryPaymentMethod beneEntityPaymentMethod)
        {
            beneEntityPaymentMethod.AccountName = beneAccount.AccountName;
            beneEntityPaymentMethod.AccountNumber = beneAccount.AccountNumber;
            beneEntityPaymentMethod.BankName = beneAccount.BankCode;
            beneEntityPaymentMethod.BankName = beneAccount.BankName;
            beneEntityPaymentMethod.BeneficiaryID = beneEntity.BeneficiaryID.ToString();
            if (!string.IsNullOrEmpty(beneAccount.BankName) && !string.IsNullOrEmpty(beneAccount.BranchName))
            {
                var branchCode = GetBankBranchId(beneAccount.BankName, beneAccount.BranchName);
                if (!string.IsNullOrEmpty(branchCode))
                    beneEntityPaymentMethod.BranchID = Convert.ToInt32(branchCode);
            }
            beneEntityPaymentMethod.BranchName = beneAccount.BranchName;

            beneEntityPaymentMethod.Currency = beneAccount.Currency;
            beneEntityPaymentMethod.PaymentMethod = beneAccount.PaymentMethod;
            beneEntityPaymentMethod.Remarks = beneAccount.Remarks;



        }

        private string GetBankBranchId(string bankName, string branchName)
        {
            var branchId = "";
            var respo = new KaprukaEntities();
            var branch = (from ba in respo.Banks
                          join br in respo.BankBranches
    on ba.BankCode equals (br.BankCode)
                          where ba.BankName == bankName && br.BranchName == branchName
                          select br).SingleOrDefault();
            if (branch != null)
            {
                branchId = branch.BranchCode;
            }

            return branchId;

        }

        private static void FillTransaction()
        {

        }

        private String getCurrencyID(String CurrencyCode)
        {
            var respo = new KaprukaEntities();
            var currency = (from cur in respo.Currencies
                            where cur.CurrencyCode == CurrencyCode
                            select cur).SingleOrDefault();
            var theID = currency.CurrencyID;
            return theID.ToString();
        }

        private String getCountryID(String CurrencyCode)
        {
            var respon = new KaprukaEntities();
            var Country = (from cou in respon.Countries
                           where cou.MasterCurrencyCode == CurrencyCode
                           select cou).SingleOrDefault();
            var theID = Country.CountryID;
            return theID.ToString();
        }


        private static KaasiCustomerResult ValidateCustomer(KaasiCustomer customer, KaasiCustomerResult addCustomerResult)
        {

            if (string.IsNullOrEmpty(customer.FirstName))
            {
                addCustomerResult.Message = "Firstname is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.LastName))
            {
                addCustomerResult.Message = "Lastname is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }


            if (string.IsNullOrEmpty(customer.DOB))
            {
                addCustomerResult.Message = "Date of birth is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }


            if (string.IsNullOrEmpty(customer.PlaceOfBirth))
            {
                addCustomerResult.Message = "Place of birth is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.StreetNo))
            {
                addCustomerResult.Message = "Street number is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.StreetName))
            {
                addCustomerResult.Message = "Street name is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }
            if (string.IsNullOrEmpty(customer.StreetType))
            {
                addCustomerResult.Message = "Street type is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }
            if (string.IsNullOrEmpty(customer.Suburb))
            {
                addCustomerResult.Message = "Suburb is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.State))
            {
                addCustomerResult.Message = "State is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.Postcode))
            {
                addCustomerResult.Message = "Postcode is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }
            if (string.IsNullOrEmpty(customer.Mobile))
            {
                addCustomerResult.Message = "Mobile is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.EmailAddress))
            {
                addCustomerResult.Message = "EmailAddress is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }




            return addCustomerResult;
        }

       
    }
}
