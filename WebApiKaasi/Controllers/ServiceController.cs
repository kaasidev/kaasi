﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Http;
using WebApiKaasi.Models;
using System.Transactions;
using Kapruka.Service;

namespace WebApiKaasi.Controllers
{
    public class ServiceController : ApiController
    {


        KaasiLoginService _loginservice;
        KaasiAgentService _kaasiAgentService;
        KaasiCountryService _kaasicountryservice;
        KaasiCurrencyService _kaasicurrencyService;
        KaasiBankService _kaasiBankService;
        KaasiBankBranchService _kaasiBankBranchService;
        KaasiCurrencyService _kaasicurrencyservice;
        KaasiCustomerService _customerservice;
        KaasiBeneficiaryService _kaasibeneficiaryservice;
        KaasiBeneficiaryPaymentMethodService _kaasibeneficiarypaymentmethodservice;
        KaasiTransactionService _kaasitransactionservice;

        public ServiceController(KaasiLoginService loginservice,
            KaasiCustomerService customerservice,
            KaasiAgentService kaasiAgentService,
             KaasiCountryService kaasicountryservice,
              KaasiCurrencyService kaasicurrencyService, KaasiBankService kaasiBankService,
              KaasiBankBranchService kaasiBankBranchService, KaasiCurrencyService kaasicurrencyservice,
             KaasiBeneficiaryService kaasibeneificiaryservice, KaasiBeneficiaryPaymentMethodService kaasibeneficiarypaymentmethodservice,
             KaasiTransactionService kaasitransactionservice)
        {
            this._loginservice = loginservice;
            this._customerservice = customerservice;
            this._kaasiAgentService = kaasiAgentService;
            this._kaasicountryservice = kaasicountryservice;
            this._kaasicurrencyService = kaasicurrencyService;
            this._kaasiBankService = kaasiBankService;
            this._kaasiBankBranchService = kaasiBankBranchService;
            this._kaasicurrencyservice = kaasicurrencyservice;
            this._kaasibeneficiaryservice = kaasibeneificiaryservice;
            this._kaasibeneficiarypaymentmethodservice = kaasibeneficiarypaymentmethodservice;
            this._kaasitransactionservice = kaasitransactionservice;

        }
        // protected 
        //public ServiceController()
        //{
        //   // this.kaasiDbaaaContext = dbKaasiContext;
        //}

        [KaasiAuthorizeAttribute]
        [Route("api/getstatus")]
        [HttpGet]
        public string GetStatus()
        {

            return "Hi, Kaasi Service is Up.";
        }



        private void WriteFile(string message)
        {
            var fileName = "logmessage.txt";
            var filepath = Utilities.ReadConfigValue("webapipathlog", @"C:\temp\");
            string path = filepath + fileName;

            message = DateTime.Now + " : " + message;
            string text2write = message;

            System.IO.StreamWriter writer = new System.IO.StreamWriter(path);
            writer.Write(text2write);
            writer.Close();
        }


        [KaasiAuthorizeAttribute]
        [Route("api/createtransaction")]
        [HttpPost]
        public ResultKaasi CreateCustomerTransaction(GlobalTransaction globalTransaction)
        {
            return CustomerTransaction(globalTransaction);
        }

        private ResultKaasi CustomerTransaction(GlobalTransaction globalTransaction)
        {
            var result = new ResultKaasi();
            if (globalTransaction == null)
            {
                result.Status = false;
                result.Message = "No object found.";
                return result;
            }
            var userKey = Thread.CurrentPrincipal.Identity.Name;

            if (globalTransaction.TransactionDirection == (int)Transactiondirection.OutBound)
            {

                var customer = globalTransaction.Customer;
                if (customer == null)
                {
                    result.Status = false;
                    result.Message = "Customer is not found.";
                    return result;
                }
                var beneficiary = globalTransaction.Beneficiary;
                if (beneficiary == null)
                {
                    result.Status = false;
                    result.Message = "Beneficiary is not found.";
                    return result;
                }
                var beneficiaryAccount = globalTransaction.Beneficiary.BeneficiaryAccount;
                if (beneficiaryAccount == null)
                {
                    result.Status = false;
                    result.Message = "Beneficiary Account is not found.";
                    return result;
                }

                var transaction = globalTransaction.Transaction;
                if (transaction == null)
                {
                    result.Status = false;
                    result.Message = "Transaction is not found.";
                    return result;
                }
                var entityCustomer = new Kapruka.Repository.Customer();
                var agent = GetAgent(userKey);

                // if (ValidateCustomer(customer).Status == true)
                // {
                // if (ValidateBeneficiary(globalTransaction.Beneficiary).Status == true)
                // {
                //   if (ValidateBeneficiarypaymentMethod(globalTransaction.Beneficiary.BeneficiaryAccount).Status == true)
                //   {
                // if (ValidateTrasaction(globalTransaction.Transaction).Status == true)
                //  {
                var customerData = ChekcCustomerExist(customer.CustomerFirstName,
                      customer.CustomerLastName, customer.DOB);
                int? customerId = 0;
                IList<Kapruka.Repository.Beneficiary> beneList = new List<Kapruka.Repository.Beneficiary>();
                if (customerData == null)
                {
                    FillCustomer(customer, entityCustomer, agent);
                    _customerservice.Insert(entityCustomer);
                    customerId = entityCustomer.CustomerID;
                }
                else
                {
                    customerId = customerData.CustomerID;
                }

                beneList = _kaasibeneficiaryservice.
                    GetBeneficiariesBYCustomerId(customerId.GetValueOrDefault());
                var benficiaryIdString = "01";
                var actualBeneId = 0;
                if (beneList.Count() == 0)
                {
                    actualBeneId = customerId.GetValueOrDefault() + Convert.ToInt32(benficiaryIdString);
                }
                else
                {
                    actualBeneId = GetBeneFiciaryID(beneficiary, beneList);

                }


                var beneficiaryEntity = new Kapruka.Repository.Beneficiary();
                beneficiaryEntity.BeneficiaryID = actualBeneId;

                //FillBeneficiary(globalTransaction.Beneficiary, beneficiaryEntity, agent, entityCustomer);


                var accountIdString = "01";
                var benepayId = 0;
                var benePaymentMethods = _kaasibeneficiarypaymentmethodservice.
                    GetBeneficiaryPaymentMethodByCustomerAndBeneFiciaryId(customerId.GetValueOrDefault(),
                    actualBeneId.ToString());

                if (benePaymentMethods.Count() == 0)
                {
                    benepayId = actualBeneId + Convert.ToInt32(accountIdString);
                }

                else
                {
                    var accountExist = (from bm in benePaymentMethods
                                        where bm.AccountNumber == globalTransaction.Beneficiary.BeneficiaryAccount.AccountNumber
                                        select bm).ToList();

                    if (accountExist.Count() == 0)
                    {
                        benepayId = benePaymentMethods.Max(x => x.BeneficiaryID).Max();
                    }
                    else
                    {
                        benepayId = Convert.ToInt32(accountExist[0].BeneficiaryID);

                    }

                }
                var beneficiaryPayment = new BeneficiaryPaymentMethod();
                beneficiaryPayment.AccountID = "0";
                beneficiaryPayment.BeneficiaryID = actualBeneId.ToString();
                beneficiaryPayment.CustomerID = customerId;

                //FillBeneficiaryAccounts(globalTransaction.Beneficiary.BeneficiaryAccount, agent,
                //    beneficiaryEntity, beneficiaryPayment);
                //var transactionEntity = new Kapruka.Repository.Transaction();
                //FillTransaction(globalTransaction.Transaction, entityCustomer, agent, beneficiaryEntity,
                //    beneficiaryPayment, transactionEntity);

            }
            else
            {
                result.Status = false;
                result.Message = "Transaction direction is not found.";
                return result;
            }

            return result;
        }

        private static int GetBeneFiciaryID(Models.Beneficiary beneficiary, IList<Kapruka.Repository.Beneficiary> beneList)
        {
            int actualBeneId;
            var chekcBeneficiary = (from bene in beneList
                                    where
              bene.BeneficiaryName == beneficiary.BeneficiaryFirstName && bene.BeneficiaryLastName == beneficiary.BeneficiaryLastName
                                    select bene).ToList();

            if (chekcBeneficiary.Count() > 0)
            {
                actualBeneId = chekcBeneficiary[0].BeneficiaryID.GetValueOrDefault();

            }
            else
            {
                var maxBeneId = chekcBeneficiary.Max(x => x.BeneficiaryID).GetValueOrDefault();
                actualBeneId = maxBeneId + 1;
            }

            return actualBeneId;
        }

        private void FillCustomer(Models.Customer customer,
            Kapruka.Repository.Customer customerEntity, Kapruka.Repository.Agent agent)
        {
            customerEntity.IsIndividual = true;
            customerEntity.CustomerID = GetlastCustomerId();
            // customerEntity.PreviousCustomerID = customer.AffiliateCustomerID;
            customerEntity.FirstName = customer.CustomerFirstName;
            customerEntity.LastName = customer.CustomerLastName;
            customerEntity.FullName = customer.CustomerFirstName + " " + customer.CustomerLastName;
            customerEntity.CountryOfBirth = customer.CountryOfBirth;
            customerEntity.DOB = customer.DOB;
            customerEntity.PlaceOfBirth = customer.PlaceOfBirth;
            // customerEntity.Nationality = customer.na;
            customerEntity.AKA = customer.AKA;
            customerEntity.Occupation = customer.Occupation;
            customerEntity.UnitNo = customer.UnitNo;
            customerEntity.StreetNo = customer.StreetNo;
            customerEntity.StreetName = customer.StreetName;
            customerEntity.StreetType = customer.StreetType;
            customerEntity.Suburb = customer.Suburb;
            customerEntity.State = customer.State;
            customerEntity.Postcode = customer.Postcode;
            customerEntity.Country = customer.Country;
            customerEntity.TelHome = customer.TelHome;
            customerEntity.TelWork = customer.TelWork;
            customerEntity.Mobile = customer.Mobile;
            customerEntity.EmailAddress = customer.EmailAddress;
            // customerEntity.EmailAddress2 = customer.e;
            customerEntity.CreatedBy = agent.AgentName;
            customerEntity.CreatedDateTime = DateTime.Now;
            customerEntity.Active = "Y";
        }


        private int GetlastCustomerId()
        {
            var csId = 0;
            csId = _customerservice.GetMaxCustomerId() + 1;
            return csId;
        }

        private Agent GetAgent(string userName)
        {
            var agentLogin = _loginservice.GetuserByName(userName);
            var agent = _kaasiAgentService.GetAgentByAgentId(agentLogin.LoginID);
            return agent;
        }


        private void FillBeneficiary(Models.Beneficiary beneficiary,
            Kapruka.Repository.Beneficiary beneEntity, Kapruka.Repository.Agent agent,
            Kapruka.Repository.Customer customer)
        {
            beneEntity.AddressLine1 = beneficiary.AddressLine1;
            beneEntity.Active = "Y";
            beneEntity.CustomerID = customer.CustomerID;
            beneEntity.AddressLine2 = beneficiary.AddressLine2;
            beneEntity.AKA = beneficiary.AKA;
            beneEntity.BeneficiaryFirstNames = beneficiary.BeneficiaryFirstName;
            beneEntity.BeneficiaryLastName = beneficiary.BeneficiaryLastName;
            beneEntity.Country = beneficiary.Country;
            beneEntity.CountryOfBirth = beneficiary.CountryOfBirth;
            if (!string.IsNullOrEmpty(beneficiary.Country))
                beneEntity.CountryID = getCountryByName(beneficiary.Country);
            beneEntity.CreatedBy = agent.AgentName;
            if (!string.IsNullOrEmpty(beneficiary.DOB))
            {
                DateTime dobMin;
                if (DateTime.TryParse(beneficiary.DOB, out dobMin))
                {
                    beneEntity.DOB = dobMin;
                }
            }
            beneEntity.Nationality = beneficiary.Nationality;
            beneEntity.EmailAddress = beneficiary.EmailAddress;
            beneEntity.Mobile = beneficiary.Mobile;
            beneEntity.NationalityCardID = beneficiary.NationalityCardID;// beneficiary.
            beneEntity.Notes = beneficiary.Notes;
            beneEntity.PlaceofBirth = beneficiary.PlaceOfBirth;
            beneEntity.Postcode = beneficiary.Postcode;
            beneEntity.Relationship = beneficiary.Relationship;
            beneEntity.State = beneficiary.State;
            beneEntity.Suburb = beneficiary.Suburb;
            beneEntity.TelHome = beneficiary.TelHome;
            beneEntity.TelWork = beneficiary.TelWork;
            beneEntity.CreatedDateTime = DateTime.Now;


        }

        private int getCountryByName(string name)
        {
            var country = _kaasicountryservice.GetCountryByName(name);
            var couId = 0;
            if (country != null)
            {
                couId = country.CountryID;
            }

            return couId;

        }

        private void FillBeneficiaryAccounts(Models.BeneficiaryAccount beneAccount, Kapruka.Repository.Agent agent,
          Kapruka.Repository.Beneficiary beneEntity,
          Kapruka.Repository.BeneficiaryPaymentMethod beneEntityPaymentMethod)
        {
            beneEntityPaymentMethod.AccountName = beneAccount.AccountName;
            beneEntityPaymentMethod.AccountNumber = beneAccount.AccountNumber;
            beneEntityPaymentMethod.BankName = beneAccount.BankCode;
            beneEntityPaymentMethod.BankName = beneAccount.BankName;
            beneEntityPaymentMethod.BeneficiaryID = beneEntity.BeneficiaryID.ToString();
            if (!string.IsNullOrEmpty(beneAccount.BankName) && !string.IsNullOrEmpty(beneAccount.BranchName))
            {
                var branchCode = GetBankBranchId(beneAccount.BankName, beneAccount.BranchName);
                if (!string.IsNullOrEmpty(branchCode))
                    beneEntityPaymentMethod.BranchID = Convert.ToInt32(branchCode);
            }
            beneEntityPaymentMethod.BranchName = beneAccount.BranchName;
            beneEntityPaymentMethod.Active = "Y";
            beneEntityPaymentMethod.NRFC = "NO";
            beneEntityPaymentMethod.AcctStatus = "Y";
            beneEntityPaymentMethod.Currency = beneAccount.Currency;
            beneEntityPaymentMethod.PaymentMethod = beneAccount.PaymentMethod;
            beneEntityPaymentMethod.Remarks = beneAccount.Remarks;
            beneEntityPaymentMethod.CreatedDateTime = DateTime.Now;
            beneEntityPaymentMethod.CreatedBy = agent.AgentName;


        }

        private string GetBankBranchId(string bankName, string branchName)
        {
            var branchId = "";
            branchId = _kaasiBankService.GetBankBranchCode(bankName, branchName);

            return branchId;

        }

        private void FillTransaction(Models.Transaction trasaction,
            Kapruka.Repository.Customer customer,
             Kapruka.Repository.Agent agent,
            Kapruka.Repository.Beneficiary beneEntity,
            Kapruka.Repository.BeneficiaryPaymentMethod beneaccount,
            Kapruka.Repository.Transaction transac)
        {

            var status = "PENDING";
            transac.AccountID = Convert.ToInt32(beneaccount.AccountID);
            transac.AgentID = agent.AgentID;
            transac.TransactionAgentOwnership = agent.AgentID;
            transac.CustomerID = customer.CustomerID;
            transac.Country = GetCountryName(trasaction.RemittedCurrency);
            transac.CreatedBy = agent.AgentName;
            transac.BeneficiaryID = beneEntity.BeneficiaryID;
            // transac.CustomerLastName=item.c
            transac.DollarAmount = Convert.ToDecimal(trasaction.DollarAmount);
            transac.RemittedAmount = Convert.ToDecimal(trasaction.RemittedAmount);
            transac.RemittedCurrency = trasaction.RemittedCurrency;
            transac.ServiceCharge = Convert.ToDecimal(trasaction.ServiceCharge);
            transac.TotalCharge = Convert.ToDecimal(trasaction.TotalCharge);
            transac.Rate = Convert.ToDecimal(trasaction.ExchangeRate);
            transac.Status = status;
            transac.Purpose = trasaction.Purpose;
            transac.SourceOfFunds = trasaction.SourceOfFunds;
            //  transac.CustomTransactionID = trasaction.CustomTransactionID;
            transac.CreatedDateTime = DateTime.Now;
            var BankID = GetBankID(beneaccount.BankName);
            transac.RoutingBank = BankID;
            transac.CountryID = Int32.Parse(getCountryID(trasaction.RemittedCurrency));


        }

        private String GetBankID(String bankName)
        {
            var Bank = _kaasiBankService.GetBankByName(bankName);

            if (Bank == null)
            {
                return "";
            }
            else
            {
                var theID = Bank.RecordID;

                return theID.ToString();
            }

        }

        private String getCurrencyID(String currencyCode)
        {
            var currency = _kaasicurrencyservice.GetCurrencyByCurrencyCode(currencyCode);
            var theID = currency.CurrencyID;
            return theID.ToString();
        }

        private String getCountryID(String currencyCode)
        {
            var theID = 0;
            var Country = _kaasicountryservice.GetCouontryByCurrencyCode(currencyCode);
            if (Country != null)
                theID = Country.CountryID;
            return theID.ToString();
        }

        private string GetCountryName(string CurrencyCode)
        {
            var name = "";
            var Country = _kaasicountryservice.GetCouontryByCurrencyCode(CurrencyCode);
            if (Country != null)
                name = Country.CountryName;
            return name;
        }


        private ResultKaasi ValidateCustomer(Models.Customer customer
            )
        {
            var addCustomerResult = new ResultKaasi();
            addCustomerResult.Status = true;
            if (string.IsNullOrEmpty(customer.CustomerFirstName))
            {
                addCustomerResult.Message = "Customer Firstname is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.CustomerLastName))
            {
                addCustomerResult.Message = "Customer Lastname is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }


            if (string.IsNullOrEmpty(customer.DOB))
            {
                addCustomerResult.Message = "Customer Date of birth is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (!string.IsNullOrEmpty(customer.DOB))
            {
                DateTime dobMin;
                if (DateTime.TryParse(customer.DOB, out dobMin))
                {

                }
                else
                {
                    addCustomerResult.Message = "Customer Date of birth is invalid.";
                    addCustomerResult.Status = false;
                    return addCustomerResult;
                }
            }


            if (string.IsNullOrEmpty(customer.PlaceOfBirth))
            {
                addCustomerResult.Message = "Customer Place of birth is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.StreetNo))
            {
                addCustomerResult.Message = "Customer Street number is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.StreetName))
            {
                addCustomerResult.Message = "Customer Street name is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }
            if (string.IsNullOrEmpty(customer.StreetType))
            {
                addCustomerResult.Message = "Customer Street type is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }
            if (string.IsNullOrEmpty(customer.Suburb))
            {
                addCustomerResult.Message = "Customer Suburb is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.State))
            {
                addCustomerResult.Message = "Customer State is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.Postcode))
            {
                addCustomerResult.Message = "Customer Postcode is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }
            if (string.IsNullOrEmpty(customer.Mobile))
            {
                addCustomerResult.Message = "Customer Mobile is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }

            if (string.IsNullOrEmpty(customer.EmailAddress))
            {
                addCustomerResult.Message = "Customer EmailAddress is empty.";
                addCustomerResult.Status = false;
                return addCustomerResult;
            }



            return addCustomerResult;
        }

        private ResultKaasi ValidateBeneficiary(Models.Beneficiary beneficiary)
        {
            var result = new ResultKaasi();
            result.Status = true;
            if (string.IsNullOrEmpty(beneficiary.BeneficiaryFirstName))
            {
                result.Message = "Beneficiary Firstname is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiary.BeneficiaryLastName))
            {
                result.Message = "Beneficiary Lastname is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiary.Relationship))
            {
                result.Message = "Beneficiary Relationship is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiary.Nationality))
            {
                result.Message = "Beneficiary Nationality is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiary.AddressLine1))
            {
                result.Message = "Beneficiary AddressLine1 is empty.";
                result.Status = false;
                return result;
            }

            return result;
        }

        private ResultKaasi ValidateBeneficiarypaymentMethod(Models.BeneficiaryAccount beneficiaryaccount
           )
        {
            var result = new ResultKaasi();
            result.Status = true;
            if (string.IsNullOrEmpty(beneficiaryaccount.Country))
            {
                result.Message = "Beneficiary Account Country is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiaryaccount.BankName))
            {
                result.Message = "Beneficiary Account Bank Name is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiaryaccount.BranchName))
            {
                result.Message = "Beneficiary Account Branch Name is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiaryaccount.AccountName))
            {
                result.Message = "Beneficiary Account Name is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiaryaccount.AccountNumber))
            {
                result.Message = "Beneficiary Account Number is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(beneficiaryaccount.Currency))
            {
                result.Message = "Beneficiary Account Currency is empty.";
                result.Status = false;
                return result;
            }

            return result;
        }

        private ResultKaasi ValidateTrasaction(Models.Transaction transaction)
        {
            var result = new ResultKaasi();
            result.Status = true;

            if (string.IsNullOrEmpty(transaction.DollarAmount))
            {
                result.Message = "transaction DollarAmount is empty.";
                result.Status = false;
                return result;
            }
            decimal doamount;
            if (!decimal.TryParse(transaction.DollarAmount, out doamount))
            {
                result.Message = "transaction DollarAmount is invalid.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(transaction.ServiceCharge))
            {
                result.Message = "transaction Service Charges is empty.";
                result.Status = false;
                return result;
            }
            decimal serCharge;
            if (!decimal.TryParse(transaction.ServiceCharge, out serCharge))
            {
                result.Message = "transaction Service Charges is invalid.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(transaction.ExchangeRate))
            {
                result.Message = "transaction Exchange Rate is empty.";
                result.Status = false;
                return result;
            }

            decimal exCharge;
            if (!decimal.TryParse(transaction.ExchangeRate, out exCharge))
            {
                result.Message = "transaction Exchange Rate is invalid.";
                result.Status = false;
                return result;
            }



            if (string.IsNullOrEmpty(transaction.Purpose))
            {
                result.Message = "transaction Purpose is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(transaction.SourceOfFunds))
            {
                result.Message = "transaction Source Of Funds is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(transaction.Notes))
            {
                result.Message = "transaction Notes is empty.";
                result.Status = false;
                return result;
            }

            if (string.IsNullOrEmpty(transaction.RemittedCurrency))
            {
                result.Message = "transaction Remitted Currency is empty.";
                result.Status = false;
                return result;
            }

            decimal remiAmmount;
            if (!decimal.TryParse(transaction.RemittedAmount, out remiAmmount))
            {
                result.Message = "transaction Remitted Amount is invalid.";
                result.Status = false;
                return result;
            }
            decimal totalCharge;
            if (!decimal.TryParse(transaction.TotalCharge, out totalCharge))
            {
                result.Message = "transaction Total Charge is invalid.";
                result.Status = false;
                return result;
            }
            return result;
        }



        private ResultKaasi SaveAllDetails(GlobalTransaction globaltransaction,
            Kapruka.Repository.Agent agent,
            Kapruka.Repository.Customer customer,
            ResultKaasi result)
        {
            try
            {

                _customerservice.Insert(customer);
                var beneficiaryEntity = new Kapruka.Repository.Beneficiary();
                FillBeneficiary(globaltransaction.Beneficiary, beneficiaryEntity, agent, customer);
                _kaasibeneficiaryservice.Insert(beneficiaryEntity);

                var beneficiaryPayment = new BeneficiaryPaymentMethod();
                FillBeneficiaryAccounts(globaltransaction.Beneficiary.BeneficiaryAccount, agent,
                    beneficiaryEntity, beneficiaryPayment);
                _kaasibeneficiarypaymentmethodservice.Insert(beneficiaryPayment);

                var transactionEntity = new Kapruka.Repository.Transaction();
                FillTransaction(globaltransaction.Transaction, customer, agent, beneficiaryEntity,
                    beneficiaryPayment, transactionEntity);

                _kaasitransactionservice.Insert(transactionEntity);


                result.Status = true;
                result.Message = "Successfully Created.";

            }
            catch (Exception ex)
            {
                WriteFile("An Error Occurred during the Web Api call " + ex.Message);
                result.Status = false;
                result.Message = "An Error Occurred during the Web Api call.";
            }

            return result;
        }


        private Kapruka.Repository.Customer ChekcCustomerExist(string firstName, string lastName, string dob)
        {
            var customer = _customerservice.GetCustomerByFirstNameAndLastNameDOB(firstName, lastName, dob);
            return customer;
        }


    }
}
