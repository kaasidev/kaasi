﻿using Autofac.Integration.WebApi;
using Kapruka.Enterprise;
using Kapruka.Repository;
using Kapruka.Service;
using Kapruka.Service.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;

namespace WebApiKaasi
{
    public class KaasiAuthorizeAttribute: AuthorizationFilterAttribute, IAutofacAuthorizationFilter
    {
        KaasiLoginService _loginservice;

        public KaasiAuthorizeAttribute(KaasiLoginService loginservice)
        {
            this._loginservice = loginservice;
        }
        public KaasiAuthorizeAttribute()
        {
        }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //Case that user is authenticated using forms authentication
            //so no need to check header for basic authentication.
            if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                return;
            }

            var authHeader = actionContext.Request.Headers.Authorization;

            if (authHeader != null)
            {
                //string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(un + ":" + pw));
                //client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                if (authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) &&
                !String.IsNullOrWhiteSpace(authHeader.Parameter))
                {
                    var credArray = GetCredentials(authHeader);
                    var userName = credArray[0];
                    var password = credArray[1];
                    //password = Utilities.DoEncrypt(password);
                    var user = GetLogin(userName);
                    if (user != null)
                    {
                        if (user.Password == password)
                        {
                            SetUserIdentity(userName);
                            //  actionContext.Response.Headers.Add("AuthenticationStatus", "Authorized");
                            //  actionContext.Response.ReasonPhrase = "Success";

                            return;
                        }
                        else
                        {
                            actionContext.Response = new HttpResponseMessage
                            {
                                StatusCode = HttpStatusCode.Forbidden,
                                Content = new StringContent("You are unauthorized to access this resource")
                            };
                        }


                    }
                    else
                    {
                        actionContext.Response = new HttpResponseMessage
                        {
                            StatusCode = HttpStatusCode.Forbidden,
                            Content = new StringContent("You are unauthorized to access this resource")
                        };
                    }

                }
            }
            else
            {
                IEnumerable<string> headerValues;
                try
                {
                    var keyFound = actionContext.Request.Headers.TryGetValues("kaasiheader", out headerValues);
                    if (keyFound)
                    {
                        string[] custom = (String[])actionContext.Request.Headers.GetValues("kaasiheader");
                        if (!String.IsNullOrWhiteSpace(custom[0]))
                        {

                            var decode = Utilities.Base64Decode(custom[0]);
                           var decrypt = Utilities.DoDecrypt(decode);
                            var userName = decrypt.Split(':')[0];
                            var id = decrypt.Split(':')[1];
                            var user = GetLogin(userName);
                            if (user != null && user.LoginID == Convert.ToInt32(id))
                            {
                                SetUserIdentity(userName);
                                // actionContext.Response.Headers.Add("AuthenticationStatus", "Authorized");
                                // actionContext.Response.ReasonPhrase = "Success";
                                return;
                            }
                            else
                            {
                                //  actionContext.Response.Headers.Add("ErrorMessage", "Invalid token.");
                                // actionContext.Response.Headers.Add("AuthenticationStatus", "Unauthorize");
                                // actionContext.Response.ReasonPhrase = "Please provide valid inputs";

                            }

                        }
                    }
                  
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(typeof(KaasiAuthorizeAttribute)).Error("Error occurred KaasiAuthorizeAttribute method." + ex.Message, ex);
                }
            }

            HandleUnauthorizedRequest(actionContext);
        }

        private string[] GetCredentials(System.Net.Http.Headers.AuthenticationHeaderValue authHeader)
        {
            //Base 64 encoded string
            var rawCred = authHeader.Parameter;
            var encoding = Encoding.GetEncoding("iso-8859-1");
            var cred = encoding.GetString(Convert.FromBase64String(rawCred));

            var credArray = cred.Split(':');

            return credArray;
        }

        private Login GetLogin(string userName)
        {
            var login = _loginservice.GetuserByName(userName);
            return login;
        }

        private static void SetUserIdentity(string userName)
        {
            var currentPrincipal = new GenericPrincipal(new GenericIdentity(userName), null);
            Thread.CurrentPrincipal = currentPrincipal;
            return;
        }

        private void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //  actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);

          //  actionContext.Response.Headers.Add("WWW-Authenticate",
          //  "Basic Scheme='eLearning' location='http://localhost:8323/account/login'");
            //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Forbidden,
                Content = new StringContent("You are unauthorized to access this resource")
            };

        }
    }
}