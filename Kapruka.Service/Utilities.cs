﻿using Kapruka.Service.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Service
{
    public class Utilities
    {
       
        public static string ReadConfigValue(string key, string defalultValue)
        {
            try
            {
                string val = ConfigurationManager.AppSettings[key];
                return val ?? defalultValue;
            }
            catch (Exception)
            {
                return defalultValue;
            }
        }

        public static string ImageToBase64(Image image,
  System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);
             
            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static string ConvertFileToBase64(string imagePath)
        {
            try
            {
                Byte[] bytes = File.ReadAllBytes(imagePath);
                String file = Convert.ToBase64String(bytes);

                return file;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static void Decrypt(string inputFilePath, string outputfilePath)
        {
            try
            {
                var EncryptionKey = ReadConfigValue("EncryptionKey", "KAASI2SPBNI99212");
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (FileStream fsInput = new FileStream(inputFilePath, FileMode.Open))
                    {
                        using (CryptoStream cs = new CryptoStream(fsInput, encryptor.CreateDecryptor(), CryptoStreamMode.Read))
                        {
                            using (FileStream fsOutput = new FileStream(outputfilePath, FileMode.Create))
                            {
                                int data;
                                while ((data = cs.ReadByte()) != -1)
                                {
                                    fsOutput.WriteByte((byte)data);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        } 

        public static void Encrypt(string inputFilePath, string outputfilePath)
        {
            try
            {
                var EncryptionKey = ReadConfigValue("EncryptionKey", "KAASI2SPBNI99212");
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (FileStream fsOutput = new FileStream(outputfilePath, FileMode.Create))
                    {
                        using (CryptoStream cs = new CryptoStream(fsOutput, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            using (FileStream fsInput = new FileStream(inputFilePath, FileMode.Open))
                            {
                                int data;
                                while ((data = fsInput.ReadByte()) != -1)
                                {
                                    cs.WriteByte((byte)data);
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
            }
        }

        public static string ConvertImageToBase64String(string imagePath)
        {
            using (Image image = Image.FromFile(imagePath,true))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
            return null;

        }
        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }
        //public System.Drawing.Image Base64ToImage()
        //{
        //    byte[] imageBytes = Convert.FromBase64String(base64String);
        //    MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
        //    ms.Write(imageBytes, 0, imageBytes.Length);
        //    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
        //    return image;
        //}  

        //public String WriteImage(TryImage image, string filePath)
        //{
        //    TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        //    Bitmap bitmap1 = (Bitmap)tc.ConvertFrom(image.Data);
        //    Stream stream = new MemoryStream(image.Data);
        //    String filepath2 = HttpContext.Current.Server.MapPath("~/Temp/Posts/") + image.Id + image.Extension;


        //    bitmap1.Save(filepath2);




        //    return image.FileName;
        //}

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        private static string key;
        private static string value;
        public static string DoEncrypt(string sValue)
        {
            try
            {
                EnsureEncryptionKey();

                Byte[] KEY_64 = Encoding.ASCII.GetBytes(key);
                Byte[] IV_64 = Encoding.ASCII.GetBytes(value);
                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateEncryptor(KEY_64, IV_64), CryptoStreamMode.Write);
                StreamWriter sw = new StreamWriter(cs);

                sw.Write(sValue);
                sw.Flush();
                cs.FlushFinalBlock();
                ms.Flush();

                // convert back to a string
                return Convert.ToBase64String(ms.GetBuffer(), 0, Convert.ToInt32(ms.Length));
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(Utilities)).Error(ex.Message, ex);

            }


            return string.Empty;
        }

        private static void EnsureEncryptionKey()
        {
            if (key == null)
            {
                key = (ConfigurationManager.AppSettings["keyPasswordEncryption"]);
                value = (ConfigurationManager.AppSettings["ivValuePasswordEncryption"]);

                if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(value))
                    throw new ApplicationException("Cannot decrypt as the key/value was not found");
            }
        }

        public static string DoDecrypt(string sValue)
        {
            try
            {
                EnsureEncryptionKey();

                Byte[] KEY_64 = Encoding.ASCII.GetBytes(key);
                Byte[] IV_64 = Encoding.ASCII.GetBytes(value);

                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                Byte[] buffer = Convert.FromBase64String(sValue);
                MemoryStream ms = new MemoryStream(buffer);
                CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateDecryptor(KEY_64, IV_64), CryptoStreamMode.Read);
                StreamReader sr = new StreamReader(cs);

                return sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(Utilities)).Error(ex.Message, ex);

            }

            return string.Empty;

        }
    }
}
