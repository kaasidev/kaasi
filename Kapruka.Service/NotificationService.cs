﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Kapruka.Service
{
    public class NotificationService
    {
        public class Notifications
        {
            public int NotificationID { get; set; }
            public int GlobalNotificationID { get; set; }
            public int LoginID { get; set; }
            public String Type { get; set; }
            public int RecordID { get; set; }
            public String Description { get; set; }
            public String CreatedDateTime { get; set; }
            public String Action { get; set; }
        }
        
        public void AddNotification(Notifications notif)
        {
            Random rnd = new Random();
            int GlobalID = rnd.Next(999999999);
            String result = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "INSERT INTO dbo.Notifications (GlobalNotificiationID, Type, RecordID, Description, CreatedDateTime) VALUES (@GlobalNotificiationID, @Type, @RecordID, @Description, CURRENT_TIMESTAMP)";
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@GlobalNotificiationID", GlobalID);
                    cmd.Parameters.AddWithValue("@Type", notif.Type.ToString());
                    cmd.Parameters.AddWithValue("@Description", notif.Description.ToString());
                    cmd.Parameters.AddWithValue("@RecordID", notif.RecordID);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                result = "OK";
                //return result;
            }
            catch (Exception ex)
            {
                result = "FAILED | " + ex.Message.ToString();
                // return result;
            }

        }

        public String NbOfNotifications()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT COUNT(*) AS TOTALCOUNT FROM dbo.Notifications";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    output = sdr["TOTALCOUNT"].ToString();
                }

                conn.Close();
            }

            return output;
        }
    }
}
