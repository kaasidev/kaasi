﻿using Kapruka.Data;
using Kapruka.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Service
{
  public  class InboundIFTIService
    {


      public InboundIFTIBeneficiaryData GetInboundBene(string accountNumber)
      {

          var obj = new InboundIFTIBeneficiaryData();

          var queryString = @"select *
                           from InboundIFTIBeneficiary  where BeneAccountNumber=@accountNumber  ";
          SqlParameter[] parameter = { new SqlParameter("@accountNumber", accountNumber) };
          try
          {
              var customerData = DbConnectionKASI.FillDataSet(queryString, parameter);
              var dataTable = customerData.Tables[0];
              foreach (DataRow row in dataTable.Rows)
              {
                  obj.ID = Convert.ToInt32(row["ID"].ToString());
                  if (row["BeneAccountNumber"] != DBNull.Value)
                      obj.BeneAccountNumber = row["BeneAccountNumber"].ToString();
                  if (row["BeneFullName"] != DBNull.Value)
                      obj.BeneFullName = row["BeneFullName"].ToString();
                  if (row["KYCCheckDate"] != DBNull.Value)
                      obj.KYCCheckDate = row["KYCCheckDate"].ToString();
                  if (row["agentTransactionId"] != DBNull.Value)
                      obj.agentTransactionId = row["agentTransactionId"].ToString();
                  if (row["transactionId"] != DBNull.Value)
                      obj.transactionId = row["transactionId"].ToString();
                  if (row["resultCode"] != DBNull.Value)
                      obj.resultCode = row["resultCode"].ToString();
                  if (row["resultDescription"] != DBNull.Value)
                      obj.resultCode = row["resultDescription"].ToString();
                  if (row["KYCBy"] != DBNull.Value)
                      obj.resultCode = row["KYCBy"].ToString();
                  
                 

              }
          }
          catch (Exception ex)
          {
              return null;
          }
          return obj;
      }

      public InboundIFTIBeneficiaryData GetInboundBeneById(int beneinboundId)
      {

          var obj = new InboundIFTIBeneficiaryData();

          var queryString = @"select *
                           from InboundIFTIBeneficiary  where ID=@ID  ";
          SqlParameter[] parameter = { new SqlParameter("@ID", beneinboundId) };
          try
          {
              var customerData = DbConnectionKASI.FillDataSet(queryString, parameter);
              var dataTable = customerData.Tables[0];
              foreach (DataRow row in dataTable.Rows)
              {
                  obj.ID = Convert.ToInt32(row["ID"].ToString());
                  if (row["BeneAccountNumber"] != DBNull.Value)
                      obj.BeneAccountNumber = row["BeneAccountNumber"].ToString();
                  if (row["BeneFullName"] != DBNull.Value)
                      obj.BeneFullName = row["BeneFullName"].ToString();
                  if (row["KYCCheckDate"] != DBNull.Value)
                      obj.KYCCheckDate = row["KYCCheckDate"].ToString();
                  if (row["agentTransactionId"] != DBNull.Value)
                      obj.agentTransactionId = row["agentTransactionId"].ToString();
                  if (row["transactionId"] != DBNull.Value)
                      obj.transactionId = row["transactionId"].ToString();
                  if (row["resultCode"] != DBNull.Value)
                      obj.resultCode = row["resultCode"].ToString();
                  if (row["resultDescription"] != DBNull.Value)
                      obj.resultDescription = row["resultDescription"].ToString();
                  if (row["KYCBy"] != DBNull.Value)
                      obj.KYCBy = row["KYCBy"].ToString();



              }
          }
          catch (Exception ex)
          {
              return null;
          }
          return obj;
      }

      public bool UpdateInboundBeneficiaryByKASIWEBAPIData(CustomerOrMerchantResponseKyced csObj,
          int csId,  string calledUser, bool cancallAudit = true)
      {
          var suditKycCheck = new KYCAuditIDRun();
          suditKycCheck.CustomerID = csId;
          suditKycCheck.RunBy = calledUser;
          suditKycCheck.Result = csObj.resultDescription;
          suditKycCheck.agentTransactionID = csObj.transactionId;
          //if (cancallAudit)
          //    InsertKCYAuditRecord(suditKycCheck);

          string SqlStmt = @"update InboundIFTIBeneficiary SET agentTransactionID = @agentTransactionID,resultDescription = @resultDescription, 
                           TransactionID = @transactionID,resultCode = @resultCode,
                         KYCCheckDate=CURRENT_TIMESTAMP, KYCBy=@kycby where ID=@ID";

         
          if (csObj.agentTransactionId == null)
              csObj.agentTransactionId = "";
          if (csObj.resultCode == null)
              csObj.resultCode = "";
          if (csObj.resultDescription == null)
              csObj.resultDescription = "";

          SqlParameter[] parameter = {new SqlParameter("@agentTransactionID",csObj.agentTransactionId),
                                        new SqlParameter("@resultDescription",csObj.resultDescription),
                                        new SqlParameter("@transactionID",csObj.transactionId),
                                        new SqlParameter("@resultCode",csObj.resultCode),
                                         new SqlParameter("@ID",csId),
                                        new SqlParameter("@kycby", calledUser)};

          return DbConnectionKASI.ExecuteNonQuery(SqlStmt, parameter);

            
      } 
    }
}
