﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Linq;
using Kapruka.Data;
using Kapruka.Domain;
namespace Kapruka.Service
{
    public class TranfertoCurrencyService
    {
        public string api_Key { get; set; }
        public string api_secret { get; set; }
        public string serviceUrlTransferToUrl { get; set; }

        public TranfertoCurrencyService()
        {
            api_Key = Utilities.ReadConfigValue("transferto_key", "d692e3ff-1457-50cc-843e-7996f6170d39");
            api_secret = Utilities.ReadConfigValue("transferto_secret", "6dbc3e3c-1623-5c67-8b22-aeaf934f0970");
            serviceUrlTransferToUrl = Utilities.ReadConfigValue("transferto_Url", "https://api-pre.mm.transferto.com/");
        }

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient();
            var credentials = Encoding.ASCII.GetBytes(api_Key + ":" + api_secret);
            //  var stContent = new StringContent("", Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public void TestPingTransferTo()
        {
            var methodUrl = "ping";
            var date = DateTime.Now;
            var nonunce = DateTime.Now.Ticks;

            string gmtdateTime = String.Format("{0:r}", DateTime.Now);

            var fullmethodUrl = serviceUrlTransferToUrl + methodUrl;
            var client = GetHttpClient();

            client.BaseAddress = new Uri(fullmethodUrl);
            try
            {
                var resultApi = client.GetAsync(fullmethodUrl).Result;
                if (resultApi.IsSuccessStatusCode)
                {
                    var json = resultApi.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                //return dynamicJson;
            }
        }

        public void GetAllPayers()
        {
            var methodUrl = "v1/money-transfer/payers";

            var fullmethodUrl = serviceUrlTransferToUrl + methodUrl;

            var client = GetHttpClient();

            client.BaseAddress = new Uri(fullmethodUrl);
            try
            {
                var resultApi = client.GetAsync(fullmethodUrl).Result;
                if (resultApi.IsSuccessStatusCode)
                {
                    var json = resultApi.Content.ReadAsStringAsync().Result;
                    // Deserialize to a dynamic object
                    var dynamicJson = JsonConvert.DeserializeObject<List<Payer>>(json);
                    // dynamicJson.IsSuccess = true;

                    GetCurrencyRateBuyPayer(dynamicJson);

                }
            }
            catch (Exception ex)
            {
                //return dynamicJson;
            }
        }

        private void GetCurrencyRateBuyPayer(List<Payer> payer)
        {
            var listCurrencyPayer = new List<CurrencyAndPayer>();
            var currencydata = new CountryCurrencyHandlerData();
            var currencyCountryList = currencydata.GetCountryCurrency();
            foreach (var item in currencyCountryList)
            {
                var payerIdByCurrencyName = (from pa in payer where pa.currency == item.CurrencyCode select pa).ToList();
                if (payerIdByCurrencyName.Count() > 0)
                {
                    var obj = new CurrencyAndPayer();
                    obj.KassiCurrencyId = item.CurrencyID;
                    obj.KassiCurrencyName = item.CurrencyCode;
                    obj.PayerIdByCurrencyFromTransferTo = payerIdByCurrencyName[0].id;

                    var result = GetRatesByPayer(obj.PayerIdByCurrencyFromTransferTo);

                    if (result != null)
                    {
                        if (result.destination_currency == obj.KassiCurrencyName)
                        {
                            if (result.rates != null)
                            {
                                if (result.rates.AUD != null)
                                {
                                    if (result.rates.AUD.Count() > 0)
                                    {
                                        if (result.rates.AUD[0].wholesale_fx_rate != null)
                                            obj.wholesale_fx_rate = result.rates.AUD[0].wholesale_fx_rate;
                                    }
                                }
                            }
                        }
                    }

                    listCurrencyPayer.Add(obj);
                }
            }

            var listCurreCou = new List<FilteredCurrency>();
            foreach (var item in listCurrencyPayer)
            {
                var obj = new FilteredCurrency();
                obj.CurrencyID = item.KassiCurrencyId;
                obj.CurrencValue = item.wholesale_fx_rate.ToString();
                obj.CurrencyCode = item.KassiCurrencyName;
                listCurreCou.Add(obj);
            }

            currencydata.UpdateMasterCompanyTransferBuyRates(listCurreCou);


        }

        public PayerRates GetRatesByPayer(int payerId)
        {
            // var methodUrl = "v1/money-transfer/payers/{id}/rates";

            var methodUrl = "v1/money-transfer/payers/{0}/rates";
            methodUrl = string.Format(methodUrl, payerId);
            var payerRates = new PayerRates();
            var fullmethodUrl = serviceUrlTransferToUrl + methodUrl;

            var client = GetHttpClient();

            client.BaseAddress = new Uri(fullmethodUrl);
            try
            {
                var resultApi = client.GetAsync(fullmethodUrl).Result;
                if (resultApi.IsSuccessStatusCode)
                {
                    var json = resultApi.Content.ReadAsStringAsync().Result;
                    // Deserialize to a dynamic object
                    payerRates = JsonConvert.DeserializeObject<PayerRates>(json);
                    // dynamicJson.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                return payerRates;
            }

            return payerRates;
        }

        public class CurrencyAndPayer
        {
            public int KassiCurrencyId { get; set; }
            public string KassiCurrencyName { get; set; }
            public int PayerIdByCurrencyFromTransferTo { get; set; }
            public double wholesale_fx_rate { get; set; }
        }

        public class AUD
        {
            public object source_amount_max { get; set; }
            public int source_amount_min { get; set; }
            public double wholesale_fx_rate { get; set; }
        }

        public class Rates
        {
            public IList<AUD> AUD { get; set; }
        }

        public class PayerRates
        {
            public string destination_currency { get; set; }
            public Rates rates { get; set; }
        }

        public class Payer
        {
            public string country_iso_code { get; set; }
            public IList<IList<object>> credit_party_identifiers_accepted { get; set; }
            public CreditPartyInformation credit_party_information { get; set; }
            public CreditPartyVerification credit_party_verification { get; set; }
            public string currency { get; set; }
            public int id { get; set; }
            public double increment { get; set; }
            public double? maximum_transaction_amount { get; set; }
            public double? minimum_transaction_amount { get; set; }
            public string name { get; set; }
            public int precision { get; set; }
            public IList<IList<object>> required_beneficiary_fields { get; set; }
            public IList<IList<object>> required_sender_fields { get; set; }
            public Service service { get; set; }
        }

        public class CreditPartyInformation
        {
            public IList<IList<object>> credit_party_identifiers_accepted { get; set; }
        }

        public class CreditPartyVerification
        {
            public IList<IList<object>> credit_party_identifiers_accepted { get; set; }
            public IList<IList<object>> required_beneficiary_fields { get; set; }
        }

        public class Service
        {
            public int id { get; set; }
            public string name { get; set; }
        }
    }
}