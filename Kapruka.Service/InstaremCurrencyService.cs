﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Service
{
    public class InstaremCurrencyService
    {

        public void GetAuthenticationToken()
        {
            var client_key = Utilities.ReadConfigValue("instaremclient_key", "646328ryCV1T1Qb230681S1g0N1ayQZ");
            var client_secret = Utilities.ReadConfigValue("instaremclient_secret", "0dfc9110-51a1-11e7-9c6a-efd0a4ece369");
            var serviceUrlInstaremUrl = Utilities.ReadConfigValue("instaremauthenticationUrl", "https://api-test.instarem.com:4803/api/v1/authentication");

            //var client = new RestClient("https://api-test.instarem.com:4803/api/v1/authentication");
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("client_key", "646328ryCV1T1Qb230681S1g0N1ayQZ");
            //request.AddHeader("client_secret", "0dfc9110-51a1-11e7-9c6a-efd0a4ece369");
            //request.AddParameter("application/json", @"{}", ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);

            var client = new HttpClient();
            
            var stContent = new StringContent("", Encoding.UTF8, "application/json");
       
           // client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Add("client_key", client_key);
         //  client.DefaultRequestHeaders.TryAddWithoutValidation("client_secret", client_secret);
            stContent.Headers.Add("client_key", client_key);
            stContent.Headers.Add("client_secret", client_secret);
            client.BaseAddress = new Uri(serviceUrlInstaremUrl);
            try
            {

                var resultApi = client.PostAsync(serviceUrlInstaremUrl, stContent).Result;
                if (resultApi.IsSuccessStatusCode)
                {

                    var json = resultApi.Content.ReadAsStringAsync().Result;
                    // Deserialize to a dynamic object
                  // var   dynamicJson = JsonConvert.DeserializeObject<CurrencyApiResult>(json);
                   // dynamicJson.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                //return dynamicJson;

            }

        }


        public void GetFXRates(string currencySell, string currencyBuy,string authToken)
        {
            //var client = new RestClient("https://api-test.instarem.com:4803/api/v1/bookfx/rate?sell_currency=&buy_currency=");
            //var request = new RestRequest(Method.GET);
            //request.AddHeader("authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0OTgxMzQ4OTQsImRhdGEiOnsic3ViIjoiNTk0MjNmMzZjZWEyMTQ2OTI2ODczOWE1IiwiY2xpZW50IjoiNTk0MjNmMzZjZWEyMTQ2OTI2ODczOWExIn0sImlhdCI6MTQ5ODA0ODQ5NH0.pevwSDpk-JDNtVIpqFu_m0Xu1UnDVumrAYStcrxf8sM");
            //request.AddHeader("content-type", "application/json");
            //request.AddParameter("application/json", @"{}", ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);


            var serviceUrlInstaremUrl = Utilities.ReadConfigValue("instaremauthenticationUrl", "https://api-test.instarem.com:4803/api/v1/bookfx/rate?sell_currency={0}&buy_currency={1}");
            serviceUrlInstaremUrl = string.Format(serviceUrlInstaremUrl, currencySell, currencyBuy);

            var client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("authorization", authToken);
            client.BaseAddress = new Uri(serviceUrlInstaremUrl);

            try
            {
                var resultApi = client.GetAsync(serviceUrlInstaremUrl).Result;
                if (resultApi.IsSuccessStatusCode)
                {

                    var json = resultApi.Content.ReadAsStringAsync().Result;
                    // Deserialize to a dynamic object
                    // var   dynamicJson = JsonConvert.DeserializeObject<CurrencyApiResult>(json);
                    // dynamicJson.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
            }
        }

    }
}
