﻿using Kapruka.Data;
using Kapruka.Domain;
using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Kapruka.Service.Logging;

namespace Kapruka.Service
{
    public class CurrencyHandlerService
    {

        private CurrencyApiResult GetCurrency()
        {
            var serviceUrl = Utilities.ReadConfigValue("currencyApiUrl", "https://openexchangerates.org/api/latest.json?app_id={0}");
            var appId = Utilities.ReadConfigValue("currencyApiAppID", "f9eb582f496e4f12bd20d45ed12a959d");
            var finalUrl = string.Format(serviceUrl, appId);
            CurrencyApiResult dynamicJson = new CurrencyApiResult();
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(finalUrl);
            try
            {

                var resultApi = client.GetAsync(finalUrl).Result;
                if (resultApi.IsSuccessStatusCode)
                {

                    var json = resultApi.Content.ReadAsStringAsync().Result;
                    // Deserialize to a dynamic object
                    dynamicJson = JsonConvert.DeserializeObject<CurrencyApiResult>(json);
                    dynamicJson.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                return dynamicJson;

            }

            return dynamicJson;
        }


        private CurrencyApiLayerResult GetCurrencyLayer()
        {
            var serviceUrl = Utilities.ReadConfigValue("currencyApilayerUrl", "https://apilayer.net/api/live?access_key={0}&source=AUD&format=1");
            var appId = Utilities.ReadConfigValue("currencyApilayerAppID", "70c3adc4ba36c8acbde9b2f9de5225db");
            var finalUrl = string.Format(serviceUrl, appId);
            CurrencyApiLayerResult dynamicJson = new CurrencyApiLayerResult();
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(finalUrl);
            try
            {

                var resultApi = client.GetAsync(finalUrl).Result;
                if (resultApi.IsSuccessStatusCode)
                {

                    var json = resultApi.Content.ReadAsStringAsync().Result;
                    // Deserialize to a dynamic object
                    dynamicJson = JsonConvert.DeserializeObject<CurrencyApiLayerResult>(json);

                }
            }
            catch (Exception ex)
            {
                return dynamicJson;

            }

            return dynamicJson;
        }

        public void FilterByCountryCurrencyFromDataBase()
        {
            var currencydata = new CountryCurrencyHandlerData();
            var currencyCountryList = currencydata.GetCountryCurrency();
            var filteredList = new List<FilteredCurrency>();
            //  var currencyFeed = GetCurrency();

            var currencyFeed = GetCurrencyLayer();
            if (currencyFeed != null)
            {
                var rates = currencyFeed.quotes;

                foreach (var item in currencyCountryList)
                {
                    var currencyCode = "AUD" + item.CurrencyCode;
                    var valueByKey = GetPropValue(rates, currencyCode);
                    if (valueByKey != null)
                    {
                        var obj = new FilteredCurrency();
                        obj.CurrencyID = item.CurrencyID;
                        obj.CurrencyName = item.CurrencyName;
                        obj.CurrencyCode = item.CurrencyCode;
                        obj.CurrencValue = valueByKey.ToString();
                        filteredList.Add(obj);
                    }
                }
            }


            try
            {
                currencydata.UpdateMasterCompanyBuyRates(filteredList);

                UpDateAllAgentRelatedTables(filteredList);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(CurrencyHandlerService))
                    .Error("Error occurred FilterByCountryCurrencyFromDataBase   method." + ex
    + " Currency Api");
            }
        }


        private void UpDateAllAgentRelatedTables(List<FilteredCurrency> obj)
        {

            var currencydata = new CountryCurrencyHandlerData();
            var commissionStrucutre = "FXGAIN";
            try
            {
                var agentsByFXGAIN = currencydata.GetAgentsByCommissionStructure(commissionStrucutre);

                var masterCompanyRates = currencydata.GetMasterCompanyRateMargins();

                currencydata.UpdateAgentCurrencyRateAndAgentSetRate(agentsByFXGAIN, masterCompanyRates,
                    obj);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(CurrencyHandlerService))
                    .Error("Error occurred UpDateAllAgentRelatedTables   method." + ex
    + " Currency update tables");
            }
        }

        protected static object GetPropValue(object src, string propName)
        {
            var prop = src.GetType().GetProperty(propName);
            if (prop != null)
            {
                return prop.GetValue(src, null);
            }

            return null;
        }
    }


}






public class CurrencyApiResult
{
    public bool IsSuccess { get; set; }
    public string disclaimer { get; set; }
    public string license { get; set; }
    public string timestamp { get; set; }
    public string @base { get; set; }
    public CountryRates rates { get; set; }
}

public class CountryRates
{
    public string AED { get; set; }
    public string AFN { get; set; }
    public string ALL { get; set; }
    public string AMD { get; set; }
    public string ANG { get; set; }
    public string AOA { get; set; }
    public string ARS { get; set; }
    public string AUD { get; set; }
    public string AWG { get; set; }
    public string AZN { get; set; }
    public string BAM { get; set; }
    public string BBD { get; set; }
    public string BDT { get; set; }
    public string BGN { get; set; }
    public string BHD { get; set; }
    public string BIF { get; set; }
    public string BMD { get; set; }

    public string BND { get; set; }
    public string BOB { get; set; }
    public string BRL { get; set; }
    public string BSD { get; set; }
    public string BTC { get; set; }
    public string BTN { get; set; }
    public string BWP { get; set; }
    public string BYN { get; set; }
    public string BZD { get; set; }
    public string CAD { get; set; }
    public string CDF { get; set; }
    public string CHF { get; set; }

    public string CLF { get; set; }
    public string CLP { get; set; }
    public string CNH { get; set; }
    public string CNY { get; set; }
    public string COP { get; set; }
    public string CRC { get; set; }
    public string CUC { get; set; }
    public string CUP { get; set; }
    public string CVE { get; set; }
    public string CZK { get; set; }
    public string DJF { get; set; }

    public string DKK { get; set; }
    public string DOP { get; set; }
    public string DZD { get; set; }
    public string EGP { get; set; }
    public string ERN { get; set; }
    public string ETB { get; set; }
    public string EUR { get; set; }
    public string FJD { get; set; }
    public string FKP { get; set; }
    public string GBP { get; set; }
    public string GEL { get; set; }
    public string GGP { get; set; }
    public string GHS { get; set; }
    public string GIP { get; set; }
    public string GMD { get; set; }
    public string GNF { get; set; }
    public string GTQ { get; set; }
    public string GYD { get; set; }
    public string HKD { get; set; }
    public string HNL { get; set; }
    public string HRK { get; set; }
    public string HTG { get; set; }

    public string HUF { get; set; }
    public string IDR { get; set; }
    public string ILS { get; set; }
    public string IMP { get; set; }
    public string INR { get; set; }
    public string IQD { get; set; }
    public string IRR { get; set; }
    public string ISK { get; set; }
    public string JEP { get; set; }
    public string JMD { get; set; }
    public string JOD { get; set; }
    public string JPY { get; set; }
    public string KES { get; set; }
    public string KGS { get; set; }
    public string KHR { get; set; }
    public string KMF { get; set; }
    public string KPW { get; set; }
    public string KRW { get; set; }
    public string KWD { get; set; }
    public string KYD { get; set; }
    public string KZT { get; set; }
    public string LAK { get; set; }

    public string LBP { get; set; }
    public string LKR { get; set; }
    public string LRD { get; set; }
    public string LSL { get; set; }
    public string LYD { get; set; }
    public string MAD { get; set; }
    public string MDL { get; set; }
    public string MGA { get; set; }
    public string MKD { get; set; }
    public string MMK { get; set; }
    public string MNT { get; set; }
    public string MOP { get; set; }
    public string MRO { get; set; }
    public string MUR { get; set; }
    public string MVR { get; set; }
    public string MWK { get; set; }
    public string MXN { get; set; }
    public string MYR { get; set; }
    public string MZN { get; set; }
    public string NAD { get; set; }
    public string NGN { get; set; }
    public string NIO { get; set; }

    public string NOK { get; set; }
    public string NPR { get; set; }
    public string NZD { get; set; }
    public string OMR { get; set; }
    public string PAB { get; set; }
    public string PEN { get; set; }
    public string PGK { get; set; }
    public string PHP { get; set; }
    public string PKR { get; set; }
    public string PLN { get; set; }
    public string PYG { get; set; }
    public string QAR { get; set; }
    public string RON { get; set; }
    public string RSD { get; set; }
    public string RUB { get; set; }
    public string RWF { get; set; }
    public string SAR { get; set; }
    public string SBD { get; set; }
    public string SCR { get; set; }
    public string SDG { get; set; }
    public string SEK { get; set; }
    public string SGD { get; set; }

    public string SHP { get; set; }
    public string SLL { get; set; }
    public string SOS { get; set; }
    public string SRD { get; set; }
    public string SSP { get; set; }
    public string STD { get; set; }
    public string SVC { get; set; }
    public string SYP { get; set; }
    public string SZL { get; set; }
    public string THB { get; set; }
    public string TJS { get; set; }
    public string TMT { get; set; }
    public string TND { get; set; }
    public string TOP { get; set; }
    public string TRY { get; set; }
    public string TTD { get; set; }
    public string TWD { get; set; }
    public string TZS { get; set; }
    public string UAH { get; set; }
    public string UGX { get; set; }
    public string USD { get; set; }
    public string UYU { get; set; }

    public string UZS { get; set; }
    public string VEF { get; set; }
    public string VND { get; set; }
    public string VUV { get; set; }
    public string WST { get; set; }
    public string XAF { get; set; }
    public string XAG { get; set; }
    public string XAU { get; set; }
    public string XCD { get; set; }
    public string XDR { get; set; }
    public string XOF { get; set; }
    public string XPD { get; set; }
    public string XPF { get; set; }
    public string XPT { get; set; }

    public string YER { get; set; }
    public string ZAR { get; set; }
    public string ZMW { get; set; }
    public string ZWL { get; set; }


}


public class CurrencyApiLayerResult
{
    public bool success { get; set; }
    public string terms { get; set; }
    public string privacy { get; set; }
    public string timestamp { get; set; }
    public string source { get; set; }
    public CountryQuotesRates quotes { get; set; }
}

public class CountryQuotesRates
{
    public string AUDAED { get; set; }
    public string AUDAFN { get; set; }
    public string AUDALL { get; set; }
    public string AUDAMD { get; set; }
    public string AUDANG { get; set; }
    public string AUDAOA { get; set; }
    public string AUDARS { get; set; }
    public string AUDAUD { get; set; }
    public string AUDAWG { get; set; }
    public string AUDAZN { get; set; }
    public string AUDBAM { get; set; }
    public string AUDBBD { get; set; }
    public string AUDBDT { get; set; }
    public string AUDBGN { get; set; }
    public string AUDBHD { get; set; }
    public string AUDBIF { get; set; }
    public string AUDBMD { get; set; }

    public string AUDBND { get; set; }
    public string AUDBOB { get; set; }
    public string AUDBRL { get; set; }
    public string AUDBSD { get; set; }
    public string AUDBTC { get; set; }
    public string AUDBTN { get; set; }
    public string AUDBWP { get; set; }
    public string AUDBYN { get; set; }
    public string AUDBZD { get; set; }
    public string AUDCAD { get; set; }
    public string AUDCDF { get; set; }
    public string AUDCHF { get; set; }

    public string AUDCLF { get; set; }
    public string AUDCLP { get; set; }
    public string AUDCNH { get; set; }
    public string AUDCNY { get; set; }
    public string AUDCOP { get; set; }
    public string AUDCRC { get; set; }
    public string AUDCUC { get; set; }
    public string AUDCUP { get; set; }
    public string AUDCVE { get; set; }
    public string AUDCZK { get; set; }
    public string AUDDJF { get; set; }

    public string AUDDKK { get; set; }
    public string AUDDOP { get; set; }
    public string AUDDZD { get; set; }
    public string AUDEGP { get; set; }
    public string AUDERN { get; set; }
    public string AUDETB { get; set; }
    public string AUDEUR { get; set; }
    public string AUDFJD { get; set; }
    public string AUDFKP { get; set; }
    public string AUDGBP { get; set; }
    public string AUDGEL { get; set; }
    public string AUDGGP { get; set; }
    public string AUDGHS { get; set; }
    public string AUDGIP { get; set; }
    public string AUDGMD { get; set; }
    public string AUDGNF { get; set; }
    public string AUDGTQ { get; set; }
    public string AUDGYD { get; set; }
    public string AUDHKD { get; set; }
    public string AUDHNL { get; set; }
    public string AUDHRK { get; set; }
    public string AUDHTG { get; set; }

    public string AUDHUF { get; set; }
    public string AUDIDR { get; set; }
    public string AUDILS { get; set; }
    public string AUDIMP { get; set; }
    public string AUDINR { get; set; }
    public string AUDIQD { get; set; }
    public string AUDIRR { get; set; }
    public string AUDISK { get; set; }
    public string AUDJEP { get; set; }
    public string AUDJMD { get; set; }
    public string AUDJOD { get; set; }
    public string AUDJPY { get; set; }
    public string AUDKES { get; set; }
    public string AUDKGS { get; set; }
    public string AUDKHR { get; set; }
    public string AUDKMF { get; set; }
    public string AUDKPW { get; set; }
    public string AUDKRW { get; set; }
    public string AUDKWD { get; set; }
    public string AUDKYD { get; set; }
    public string AUDKZT { get; set; }
    public string AUDLAK { get; set; }

    public string AUDLBP { get; set; }
    public string AUDLKR { get; set; }
    public string AUDLRD { get; set; }
    public string AUDLSL { get; set; }
    public string AUDLYD { get; set; }
    public string AUDMAD { get; set; }
    public string AUDMDL { get; set; }
    public string AUDMGA { get; set; }
    public string AUDMKD { get; set; }
    public string AUDMMK { get; set; }
    public string AUDMNT { get; set; }
    public string AUDMOP { get; set; }
    public string AUDMRO { get; set; }
    public string AUDMUR { get; set; }
    public string AUDMVR { get; set; }
    public string AUDMWK { get; set; }
    public string AUDMXN { get; set; }
    public string AUDMYR { get; set; }
    public string AUDMZN { get; set; }
    public string AUDNAD { get; set; }
    public string AUDNGN { get; set; }
    public string AUDNIO { get; set; }

    public string AUDNOK { get; set; }
    public string AUDNPR { get; set; }
    public string AUDNZD { get; set; }
    public string AUDOMR { get; set; }
    public string AUDPAB { get; set; }
    public string AUDPEN { get; set; }
    public string AUDPGK { get; set; }
    public string AUDPHP { get; set; }
    public string AUDPKR { get; set; }
    public string AUDPLN { get; set; }
    public string AUDPYG { get; set; }
    public string AUDQAR { get; set; }
    public string AUDRON { get; set; }
    public string AUDRSD { get; set; }
    public string AUDRUB { get; set; }
    public string AUDRWF { get; set; }
    public string AUDSAR { get; set; }
    public string AUDSBD { get; set; }
    public string AUDSCR { get; set; }
    public string AUDSDG { get; set; }
    public string AUDSEK { get; set; }
    public string AUDSGD { get; set; }

    public string AUDSHP { get; set; }
    public string AUDSLL { get; set; }
    public string AUDSOS { get; set; }
    public string AUDSRD { get; set; }
    public string AUDSSP { get; set; }
    public string AUDSTD { get; set; }
    public string AUDSVC { get; set; }
    public string AUDSYP { get; set; }
    public string AUDSZL { get; set; }
    public string AUDTHB { get; set; }
    public string AUDTJS { get; set; }
    public string AUDTMT { get; set; }
    public string AUDTND { get; set; }
    public string AUDTOP { get; set; }
    public string AUDTRY { get; set; }
    public string AUDTTD { get; set; }
    public string AUDTWD { get; set; }
    public string AUDTZS { get; set; }
    public string AUDUAH { get; set; }
    public string AUDUGX { get; set; }
    public string AUDUSD { get; set; }
    public string AUDUYU { get; set; }

    public string AUDUZS { get; set; }
    public string AUDVEF { get; set; }
    public string AUDVND { get; set; }
    public string AUDVUV { get; set; }
    public string AUDWST { get; set; }
    public string AUDXAF { get; set; }
    public string AUDXAG { get; set; }
    public string AUDXAU { get; set; }
    public string AUDXCD { get; set; }
    public string AUDXDR { get; set; }
    public string AUDXOF { get; set; }
    public string AUDXPD { get; set; }
    public string AUDXPF { get; set; }
    public string AUDXPT { get; set; }

    public string AUDYER { get; set; }
    public string AUDZAR { get; set; }
    public string AUDZMW { get; set; }
    public string AUDZWL { get; set; }


}
