﻿using Kapruka.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Kapruka.Service.Logging;

using Kapruka.Data;
using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace Kapruka.Service
{
    public class CustomerHandlerService
    {
        string serviceUrl = Utilities.ReadConfigValue("kycedServiceUrl", "http://kyc.novatti.com:9090/api/1.0/");

        private void TestWebApi(string paramss, string url)
        {

            var baseAddress = "http://kyc.novatti.com:9090/api/1.0/kyc/customer";

            var http = (HttpWebRequest)WebRequest.Create(new Uri(url));

            http.Accept = "application/json";
            http.ContentType = "text/plain";
            http.Method = "POST";

            string parsedContent = paramss;
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);
            http.ContentLength = bytes.Length;
            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();
            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
        }

        private void CallEcho()
        {
            var baseAddress = "http://kyc.novatti.com:9090/api/1.0/echo/ping";

            var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));

            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";

            ASCIIEncoding encoding = new ASCIIEncoding();
            //  Byte[] bytes = encoding.GetBytes(parsedContent);

            //Stream newStream = http.GetRequestStream();
            //newStream.Write(bytes, 0, bytes.Length);
            //newStream.Close();
            // http.
            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
        }

        private void Test2Api(string par)
        {
            var baseAddress = "http://kyc.novatti.com:9090/api/1.0/kyc/customer";
            const string DATA = @"{""userid"":""some@soms.com"",""rkey"":""BAR4G"",""password"":""pswrd"",""authType"":""PGQS""}";
            WebRequest request = WebRequest.Create(baseAddress);
            request.Method = "POST";
            string postData = DATA;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
           // request.Headers.Add(
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();
            var stt = (((HttpWebResponse)response).StatusDescription);
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            var ddd = (responseFromServer);
            reader.Close();
            dataStream.Close();
            response.Close();
        }

        public ResultKycCall CreateKycedIncoimmgCustomer(CustomerKyced cKyced, int customerId,
          int recordCustomerId, string calledUser)
        {

            //   CallEcho();
            var baseAddress = "http://kyc.novatti.com:9090/api/1.0/kyc/customer";
            var baseAddressMerchant = "http://kyc.novatti.com:9090/api/1.0/kyc/merchant";

            var resulCall = new ResultKycCall();
            var client = new HttpClient();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.ExpectContinue = false;
            ServicePointManager.Expect100Continue = false;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(serviceUrl);
            var urlForMethod = Utilities.ReadConfigValue("kycedcreateCustomermethod", "kyc/customer");
            HttpResponseMessage resultApiCall = null;
            client.DefaultRequestHeaders.Add("X-Version", "1");


            try
            {
                
                var jsonObj = JsonConvert.SerializeObject(
             cKyced,
             new JsonSerializerSettings()
             {
                 NullValueHandling = NullValueHandling.Ignore,
                 DefaultValueHandling = DefaultValueHandling.Ignore

             });
                var stContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");

                //  var request = new HttpRequestMessage(HttpMethod.Post, urlWithMethod);
                //  request.Content = stContent;
                client.DefaultRequestHeaders.ExpectContinue = false;

               // WriteKcyObjects(jsonObj);

                resultApiCall = client.PostAsync(urlForMethod, stContent).Result;
                if (resultApiCall.IsSuccessStatusCode)
                {
                    var jsona = resultApiCall.Content.ReadAsStringAsync().Result;
                    resulCall.Status = true;

                    // Deserialize to a dynamic object
                    CustomerOrMerchantResponseKyced dynamicJson =
                        JsonConvert.DeserializeObject<CustomerOrMerchantResponseKyced>(jsona);
                    resulCall.Message = dynamicJson.resultDescription;

                    new InboundIFTIService().
                        UpdateInboundBeneficiaryByKASIWEBAPIData(dynamicJson, customerId, calledUser);
                    resulCall.Message = " Result: " + dynamicJson.resultDescription + " .Checked date :" + DateTime.Now.Date.ToShortDateString();

                    //if (dynamicJson.resultCode == "1")
                    //{
                    //    var notificationService = new NotificationService();
                    //    var notify = new Kapruka.Service.NotificationService.Notifications();
                    //    notify.Type = "CUSTOMER";
                    //    notify.RecordID = customerId;
                    //    notify.Description = "Customer " + customerId + " has failed KYC - KYC Transaction ID: " + dynamicJson.transactionId;
                    //    notificationService.AddNotification(notify);

                    //}

                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //dynamic item = serializer.Deserialize<object>(getString);
                    //string name = item["name"];

                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedCustomer  method customer created successfully.");
                    WriteFile("error Api" + resultApiCall);
                }
                else
                {
                    resulCall.Message = "customer created failed";
                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedCustomer  method customer created failed." + resultApiCall);
                    WriteFile("error Api" + resultApiCall);


                    var errorObject = new CustomerOrMerchantResponseKyced();
                    errorObject.resultCode = "-1";
                    errorObject.resultDescription = " KYC Called error occurred.";
                    errorObject.transactionId = "";
                    // if (!string.IsNullOrEmpty(resultApiCall.ReasonPhrase))
                    //   errorObject.resultDescription = errorObject.resultDescription + resultApiCall.ReasonPhrase;

                    new InboundIFTIService().
                      UpdateInboundBeneficiaryByKASIWEBAPIData(errorObject, customerId, calledUser);
                    return resulCall;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(CustomerHandlerService)).Error("Error occurred CreateKycedCustomer method." + ex
                    + " error Api");
                resulCall.Message = "customer created failed";
                WriteFile("error Api" + ex);

                var errorObject = new CustomerOrMerchantResponseKyced();
                errorObject.resultCode = "-1";
                errorObject.resultDescription = " KYC Called error occurred.";
                errorObject.transactionId = "";
                //if (!string.IsNullOrEmpty(resultApiCall.ReasonPhrase))
                //   errorObject.resultDescription = errorObject.resultDescription + resultApiCall.ReasonPhrase;

                errorObject.resultDescription = errorObject.resultDescription + " " + ex.Message;
                new InboundIFTIService().
                   UpdateInboundBeneficiaryByKASIWEBAPIData(errorObject, customerId, calledUser);

                return resulCall;
            }

            return resulCall;
        }


        public ResultKycCall CreateKycedCustomer(CustomerKyced cKyced, int customerId,
            int recordCustomerId, string calledUser)
        {

            //   CallEcho();
            var baseAddress = "http://kyc.novatti.com:9090/api/1.0/kyc/customer";
            var baseAddressMerchant = "http://kyc.novatti.com:9090/api/1.0/kyc/merchant";

            var resulCall = new ResultKycCall();
            var client = new HttpClient();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.ExpectContinue = false;
            ServicePointManager.Expect100Continue = false;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(serviceUrl);
            var urlForMethod = Utilities.ReadConfigValue("kycedcreateCustomermethod", "kyc/customer");
            HttpResponseMessage resultApiCall = null;
            client.DefaultRequestHeaders.Add("X-Version", "1");

            // sample data format 
            //var objnew = new CustomerKyced()
            //{
            //    title = "Mr",
            //    man = "ajeets",
            //    bfn = "string",
            //    bln = "string",
            //    dob = "1999-01-01",
            //    securityNumber = "S123434-01",
            //    tea = "test@domain.com",
            //    bsn = "10 Swanston Street",
            //    bco = "AU",
            //    bz = "3000",
            //    bs = "VIC",
            //    bc = "Melbourne",
            //    pw = "Mobile Phone Number",
            //    ip = "string",
            //    documentImageFront = "Passport / Driving License",
            //    documentImangeBack = "string",
            //    faceImage = "string"
            //};


            try
            {
                // var urlWithMethod = serviceUrl+ urlForMethod;
                //  var jsonObj = JsonConvert.SerializeObject(cKyced);

                var jsonObj = JsonConvert.SerializeObject(
             cKyced,
             new JsonSerializerSettings()
             {
                 NullValueHandling = NullValueHandling.Ignore,
                 DefaultValueHandling = DefaultValueHandling.Ignore

             });
                var stContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");

                //  var request = new HttpRequestMessage(HttpMethod.Post, urlWithMethod);
                //  request.Content = stContent;
                client.DefaultRequestHeaders.ExpectContinue = false;

                WriteKcyObjects(jsonObj);

                // var response = client.SendAsync(request).ConfigureAwait(false);

                // var echoString="echo/ping";
                // var testrRes = client.GetAsync(echoString);

                resultApiCall = client.PostAsync(urlForMethod, stContent).Result;
                if (resultApiCall.IsSuccessStatusCode)
                {
                    var jsona = resultApiCall.Content.ReadAsStringAsync().Result;
                    resulCall.Status = true;

                    // Deserialize to a dynamic object
                    CustomerOrMerchantResponseKyced dynamicJson =
                        JsonConvert.DeserializeObject<CustomerOrMerchantResponseKyced>(jsona);
                    resulCall.Message = dynamicJson.resultDescription;

                    new CustomerHandlerData().
                        UpdateCustomerByKASIWEBAPIData(dynamicJson, customerId, recordCustomerId, calledUser);
                    resulCall.Message = " Result: " + dynamicJson.resultDescription + " .Checked date :" + DateTime.Now.Date.ToShortDateString();

                    if (dynamicJson.resultCode == "1")
                    {
                        var notificationService = new NotificationService();
                        var notify = new Kapruka.Service.NotificationService.Notifications();
                        notify.Type = "CUSTOMER";
                        notify.RecordID = customerId;
                        notify.Description = "Customer " + customerId + " has failed KYC - KYC Transaction ID: " + dynamicJson.transactionId;
                        notificationService.AddNotification(notify);

                    }

                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //dynamic item = serializer.Deserialize<object>(getString);
                    //string name = item["name"];

                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedCustomer  method customer created successfully.");
                    WriteFile("error Api" + resultApiCall);
                }
                else
                {
                    resulCall.Message = "customer created failed";
                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedCustomer  method customer created failed." + resultApiCall);
                    WriteFile("error Api" + resultApiCall);


                    var errorObject = new CustomerOrMerchantResponseKyced();
                    errorObject.resultCode = "-1";
                    errorObject.resultDescription = " KYC Called error occurred." ;
                    errorObject.transactionId = "";
                   // if (!string.IsNullOrEmpty(resultApiCall.ReasonPhrase))
                     //   errorObject.resultDescription = errorObject.resultDescription + resultApiCall.ReasonPhrase;

                    new CustomerHandlerData().
                       UpdateCustomerByKASIWEBAPIData(errorObject, customerId, recordCustomerId, calledUser);
                    return resulCall;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(typeof(CustomerHandlerService)).Error("Error occurred CreateKycedCustomer method." + ex
                    + " error Api");
                resulCall.Message = "customer created failed";
                WriteFile("error Api" + ex);

                var errorObject = new CustomerOrMerchantResponseKyced();
                errorObject.resultCode = "-1";
                errorObject.resultDescription = " KYC Called error occurred." ;
                errorObject.transactionId = "";
                //if (!string.IsNullOrEmpty(resultApiCall.ReasonPhrase))
                //   errorObject.resultDescription = errorObject.resultDescription + resultApiCall.ReasonPhrase;

                errorObject.resultDescription = errorObject.resultDescription + " " + ex.Message;
                new CustomerHandlerData().
                   UpdateCustomerByKASIWEBAPIData(errorObject, customerId, recordCustomerId, calledUser);

                return resulCall;
            }

            return resulCall;
        }

        private void WriteKcyObjects(dynamic yourObject)
        {
            //string s = JsonConvert.SerializeObject(yourObject);
            var file = "log" + DateTime.Now.Ticks;
            var fileName = file + "message.txt";
            string path = @"C:\temp\" + fileName;
            string text2write = "Logging Information: " + yourObject;

            System.IO.StreamWriter writer = new System.IO.StreamWriter(path);
            writer.Write(text2write);
            writer.Close();
        }

        private void WriteFile(string message)
        {
            var fileName = "logmessage.txt";
            string path = @"C:\temp\" + fileName;
            message = DateTime.Now + " : " + message;
            string text2write = message;

            System.IO.StreamWriter writer = new System.IO.StreamWriter(path);
            writer.Write(text2write);
            writer.Close();
        }

        public int GetRecordIdBYCustomerId(int customerId)
        {
            return new CustomerHandlerData().GetCustomerRecordIdByCustomerId(customerId);
        }

        public ResultKycCall CreateKycedMerchant(MerchantKyced merchantObj,
            int customerId, int recordCustomerId, string calledUser)
        {
            WriteKcyObjects(merchantObj);


            var client = new HttpClient();
            var resulCall = new ResultKycCall();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.ExpectContinue = false;
            ServicePointManager.Expect100Continue = false;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(serviceUrl);
            HttpResponseMessage resultApiCall = null;
            client.DefaultRequestHeaders.Add("X-Version", "1");
            var urlForMethod = Utilities.ReadConfigValue("kycedcreateCustomermethod", "kyc/merchant");


            //data format accepted by service
            //var newR = new MerchantKyced()

            //    {
            //        amn = "string",
            //        ataxId = "string",
            //        afn = "string",
            //        aln = "string",
            //        asn = "string",
            //        ac = "Melbourne",
            //        aco = "AU",
            //        az = "3000",
            //        aph = "string",
            //        dba = "string",
            //        bin = "string",
            //        website = "string"
            //    };
            var jsonObj = JsonConvert.SerializeObject(merchantObj);
            var stContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.ExpectContinue = false;

            try
            {
                resultApiCall = client.PostAsync(urlForMethod, stContent).Result;
                if (resultApiCall.IsSuccessStatusCode)
                {

                    var json = resultApiCall.Content.ReadAsStringAsync().Result;

                    resulCall.Status = true;
                    // Deserialize to a dynamic object
                    CustomerOrMerchantResponseKyced dynamicJson = JsonConvert.DeserializeObject<CustomerOrMerchantResponseKyced>(json);
                    resulCall.Message = dynamicJson.resultDescription;

                    new CustomerHandlerData().
                                          UpdateCustomerByKASIWEBAPIData(dynamicJson, customerId,
                                          recordCustomerId, calledUser);

                    resulCall.Message = " Result: " + dynamicJson.resultDescription + " .Checked date :" + DateTime.Now.Date.ToShortDateString();

                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedMerchant  method customer created successfully.");

                }
                else
                {
                    resulCall.Message = "customer created failed";
                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedMerchant  method customer created failed." + resultApiCall);
                    return resulCall;

                }
            }
            catch (Exception ex)
            {
                resulCall.Message = "customer created failed";
                LogManager.GetLogger(typeof(CustomerHandlerService)).Error("Error occurred CreateKycedMerchant  method." + ex.Message, ex);
                return resulCall;
            }

            return resulCall;
        }

        public ResultKycCall CreateKycedAgent(MerchantKyced merchantObj, int agentId, string calledUser)
        {

            //WriteKcyObjects(merchantObj);
            var client = new HttpClient();
            var resulCall = new ResultKycCall();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.ExpectContinue = false;
            ServicePointManager.Expect100Continue = false;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(serviceUrl);
            HttpResponseMessage resultApiCall = null;
            client.DefaultRequestHeaders.Add("X-Version", "1");
            var urlForMethod = Utilities.ReadConfigValue("kycedcreateCustomermethod", "kyc/merchant");


            //data format accepted by service
            //var newR = new MerchantKyced()

            //    {
            //        amn = "string",
            //        ataxId = "string",
            //        afn = "string",
            //        aln = "string",
            //        asn = "string",
            //        ac = "Melbourne",
            //        aco = "AU",
            //        az = "3000",
            //        aph = "string",
            //        dba = "string",
            //        bin = "string",
            //        website = "string"
            //    };
            var jsonObj = JsonConvert.SerializeObject(
                merchantObj,
                new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore
                });
            var stContent = new StringContent(jsonObj, Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.ExpectContinue = false;
            WriteKcyObjects(jsonObj);

            try
            {
                resultApiCall = client.PostAsync(urlForMethod, stContent).Result;
                if (resultApiCall.IsSuccessStatusCode)
                {

                    var json = resultApiCall.Content.ReadAsStringAsync().Result;

                    resulCall.Status = true;
                    // Deserialize to a dynamic object
                    CustomerOrMerchantResponseKyced dynamicJson = JsonConvert.DeserializeObject<CustomerOrMerchantResponseKyced>(json);
                    resulCall.Message = dynamicJson.resultDescription;

                    new CustomerHandlerData().
                                          UpdateAgentByKASIWEBAPIData(dynamicJson, agentId, calledUser);

                    resulCall.Message = " KYC Result: " + dynamicJson.resultDescription + " .Checked date :" + DateTime.Now.Date.ToShortDateString();

                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedMerchant  method customer created successfully.");

                }
                else
                {
                    resulCall.Message = "customer created failed";
                    LogManager.GetLogger(typeof(CustomerHandlerService)).Info("CreateKycedMerchant  method customer created failed." + resultApiCall);
                    return resulCall;

                }
            }
            catch (Exception ex)
            {
                resulCall.Message = "customer created failed";
                LogManager.GetLogger(typeof(CustomerHandlerService)).Error("Error occurred CreateKycedMerchant  method." + ex.Message, ex);
                return resulCall;
            }

            return resulCall;
        }
        public bool UpdateCustomerByKASIWEBAPIData(CustomerOrMerchantResponseKyced csObj, int csId,
            int recordId, string runBy, bool cancallAudit = true)
        {

            return new CustomerHandlerData().UpdateCustomerByKASIWEBAPIData(csObj, csId, recordId, runBy, cancallAudit);
        }

        public bool UpdateAgentByKASIWEBAPIData(CustomerOrMerchantResponseKyced csObj, int agentId, string runBy)
        {
            return new CustomerHandlerData().UpdateAgentByKASIWEBAPIData(csObj, agentId, runBy);
        }

        public CustomerForKyc GetCustomerById(int customerId)
        {
            return new CustomerHandlerData().GetCustomerById(customerId);
        }

        public CustomerDocs GetCustomerDocs(int customerId)
        {
            return new CustomerHandlerData().GetCustomerDocs(customerId);
        }

        public List<CustomerDocs> GetCustomerAllDocs(int customerId)
        {
            return new CustomerHandlerData().GetCustomerAllDocs(customerId);
        }

        public List<CustomerDocs> GetCustomerAllDocsByDocumentNumber(string documentNumber)
        {
            return new CustomerHandlerData().GetCustomerAllDocsByDocumentNumber(documentNumber);
        }

        public AgentKYC GetAgentById(int agentId)
        {
            return new CustomerHandlerData().GetAgentById(agentId);
        }

    }
}
