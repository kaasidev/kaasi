﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Repository
{
    public class UnitOfWorks 
    {
        private KaprukaEntities context;
        private GenericRepository<Transaction> transactionRepository;
        private GenericRepository<Customer> customerRepository;
        private GenericRepository<Beneficiary> beneficiaryRepository;
        private GenericRepository<BeneficiaryPaymentMethod> beneficiaryPaymentMethodRepository;
        private GenericRepository<Agent> agentRepository;
        private GenericRepository<CustomerDocument> customerDocumentRepository;
        private GenericRepository<Country> countryRepository;
        private GenericRepository<AgentAssignedCountry> agencyAssCountryRepository;
        private GenericRepository<Setting> settingRepository;
        private GenericRepository<Login> loginRepository;
        private GenericRepository<Currency> currencyRepository;
        private GenericRepository<CreditTransaction> creditTransactionRepository;
        private GenericRepository<MasterAustracRecord> masterAustracRepository;
        private GenericRepository<AffiliateAustracRecord> affiliateAustracRepository;
        private GenericRepository<IncomingAgent> incomingAgentRepository;
        private GenericRepository<InCustomer> inCustomerRepository;
        private GenericRepository<InboundTransaction> inboundTransactionRepository;
        private GenericRepository<AgentCurrencyRate> agentCurrencyRateRepository;
        private GenericRepository<AgentCurrencyMargin> agentCurrencyMarginRepository;
        private GenericRepository<TransAuditLog> transAuditLogRepository;
        private GenericRepository<InTransAuditLog> inTransAuditLogRepository;
        private GenericRepository<InwardTransNote> inwardTransNoteRepository;
        private GenericRepository<Notification> notificationRepository;
        private GenericRepository<Bank> bankRepository;
        private GenericRepository<InboundIFTIBeneficiary> inboundIFTIBeneficiaryRepository;
        private GenericRepository<CustAuditLog> custAuditLogRepository;
        private GenericRepository<InBeneAuditLog> inBeneAuditLogRepository;
        private GenericRepository<InMasterAustracRecord> inMasterAustracRecordRepository;
        private GenericRepository<AgentNotification> agentNotificationRepository;
        private GenericRepository<AgentForeignBankSetting> agentForeignBankSettingRepository;
        private GenericRepository<SettingAuditLog> settingAuditLogRepository;
        private GenericRepository<AgentAuditLog> agentAuditLogRepository;
        private GenericRepository<MasterCompanyBank> masterCompanyBankRepository;
        private GenericRepository<AgentAssignedBank> agentAssignedBankRepository;
        private GenericRepository<AgentCurrency> agentCurrencyRepository;
       
        //private GenericRepository<Product> productRepository;

        private bool disposed;

        public GenericRepository<AgentAssignedCountry> AgentAssignedCountryRepository
        {
            get
            {
                if (this.agencyAssCountryRepository == null)
                    this.agencyAssCountryRepository = new GenericRepository<AgentAssignedCountry>(this.context);
                return this.agencyAssCountryRepository;
            }
        }

        public GenericRepository<AgentCurrency> AgentCurrencyRepository
        {
            get
            {
                if (this.agentCurrencyRepository == null)
                    this.agentCurrencyRepository = new GenericRepository<AgentCurrency>(this.context);
                return this.agentCurrencyRepository;
            }
        }
        
        public GenericRepository<AgentAssignedBank> AgentAssignedBankRepository
        {
            get
            {
                if (this.agentAssignedBankRepository == null)
                    this.agentAssignedBankRepository = new GenericRepository<AgentAssignedBank>(this.context);
                return this.agentAssignedBankRepository;
            }
        }

        public GenericRepository<MasterCompanyBank> MasterCompanyBankRepository
        {
            get
            {
                if (this.masterCompanyBankRepository == null)
                    this.masterCompanyBankRepository = new GenericRepository<MasterCompanyBank>(this.context);
                return this.masterCompanyBankRepository;
            }
        }

        public GenericRepository<AgentAuditLog> AgentAuditLogRepository
        {
            get
            {
                if (this.agentAuditLogRepository == null)
                    this.agentAuditLogRepository = new GenericRepository<AgentAuditLog>(this.context);
                return this.agentAuditLogRepository;
            }
        }

        public GenericRepository<SettingAuditLog> SettingAuditLogRepository
        {
            get
            {
                if (this.settingAuditLogRepository == null)
                    this.settingAuditLogRepository = new GenericRepository<SettingAuditLog>(this.context);
                return this.settingAuditLogRepository;
            }
        }

        public GenericRepository<AgentForeignBankSetting> AgentForeignBankSettingRepository
        {
            get
            {
                if (this.agentForeignBankSettingRepository == null)
                    this.agentForeignBankSettingRepository = new GenericRepository<AgentForeignBankSetting>(this.context);
                return this.agentForeignBankSettingRepository;
            }
        }

        public GenericRepository<AgentNotification> AgentNotificationRepository
        {
            get
            {
                if (this.agentNotificationRepository == null)
                    this.agentNotificationRepository = new GenericRepository<AgentNotification>(this.context);
                return this.agentNotificationRepository;
            }
        }
        
        public GenericRepository<InMasterAustracRecord> InMasterAustracRecordRepository
        {
            get
            {
                if (this.inMasterAustracRecordRepository == null)
                    this.inMasterAustracRecordRepository = new GenericRepository<InMasterAustracRecord>(this.context);
                return this.inMasterAustracRecordRepository;
            }
        }

        public GenericRepository<InBeneAuditLog> InBeneAuditLogRepository
        {
            get
            {
                if (this.inBeneAuditLogRepository == null)
                    this.inBeneAuditLogRepository = new GenericRepository<InBeneAuditLog>(this.context);
                return this.inBeneAuditLogRepository;
            }
        }

        public GenericRepository<CustAuditLog> CustAuditLogRepository
        {
            get
            {
                if (this.custAuditLogRepository == null)
                    this.custAuditLogRepository = new GenericRepository<CustAuditLog>(this.context);
                return this.custAuditLogRepository;
            }
        }

        public GenericRepository<InboundIFTIBeneficiary> InboundIFTIBeneficiaryRepository
        {
            get
            {
                if (this.inboundIFTIBeneficiaryRepository == null)
                    this.inboundIFTIBeneficiaryRepository = new GenericRepository<InboundIFTIBeneficiary>(this.context);
                return this.inboundIFTIBeneficiaryRepository;
            }
        }

        public GenericRepository<Bank> BankRepository
        {
            get
            {
                if (this.bankRepository == null)
                    this.bankRepository = new GenericRepository<Bank>(this.context);
                return this.bankRepository;
            }
        }

        public GenericRepository<Notification> NotificationRepository
        {
            get
            {
                if (this.notificationRepository == null)
                    this.notificationRepository = new GenericRepository<Notification>(this.context);
                return this.notificationRepository;
            }
        }

        public GenericRepository<InwardTransNote> InwardTransNoteRepository
        {
            get
            {
                if (this.inwardTransNoteRepository == null)
                    this.inwardTransNoteRepository = new GenericRepository<InwardTransNote>(this.context);
                return this.inwardTransNoteRepository;
            }
        }

        public GenericRepository<InTransAuditLog> InTransAuditLogRepository
        {
            get
            {
                if (this.inTransAuditLogRepository == null)
                    this.inTransAuditLogRepository = new GenericRepository<InTransAuditLog>(this.context);
                return this.inTransAuditLogRepository;
            }
        }

        public GenericRepository<TransAuditLog> TransAuditLogRepository
        {
            get
            {
                if (this.transAuditLogRepository == null)
                    this.transAuditLogRepository = new GenericRepository<TransAuditLog>(this.context);
                return this.transAuditLogRepository;
            }
        }

        public GenericRepository<AgentCurrencyMargin> AgentCurrencyMarginRepository
        {
            get
            {
                if (this.agentCurrencyMarginRepository == null)
                    this.agentCurrencyMarginRepository = new GenericRepository<AgentCurrencyMargin>(this.context);
                return this.agentCurrencyMarginRepository;
            }
        }

        public GenericRepository<AgentCurrencyRate> AgentCurrencyRateRepository
        {
            get
            {
                if (this.agentCurrencyRateRepository == null)
                    this.agentCurrencyRateRepository = new GenericRepository<AgentCurrencyRate>(this.context);
                return this.agentCurrencyRateRepository;
            }
        }

        public GenericRepository<InboundTransaction> inboundTransaction
        {
            get
            {
                if (this.inboundTransactionRepository == null)
                    this.inboundTransactionRepository = new GenericRepository<InboundTransaction>(this.context);
                return this.inboundTransactionRepository;
            }
        }

        public GenericRepository<AffiliateAustracRecord> AffiliateAustracRepository
        {
            get
            {
                if (this.affiliateAustracRepository == null)
                    this.affiliateAustracRepository = new GenericRepository<AffiliateAustracRecord>(this.context);
                return this.affiliateAustracRepository;
            }
        }

        public GenericRepository<MasterAustracRecord> MasterAustracRepository
        {
            get
            {
                if (this.masterAustracRepository == null)
                    this.masterAustracRepository = new GenericRepository<MasterAustracRecord>(this.context);
                return this.masterAustracRepository;
            }
        }

        public GenericRepository<CreditTransaction> CreditTransactionRepository
        {
            get
            {
                if (this.creditTransactionRepository == null)
                    this.creditTransactionRepository = new GenericRepository<CreditTransaction>(this.context);
                return this.creditTransactionRepository;
            }
        }

        public GenericRepository<Currency> CurrencyRepository
        {
            get
            {
                if (this.currencyRepository == null)
                    this.currencyRepository = new GenericRepository<Currency>(this.context);
                return this.currencyRepository;
            }
        }


        public GenericRepository<Country> CountryRepository
        {
            get
            {
                if (this.countryRepository == null)
                    this.countryRepository = new GenericRepository<Country>(this.context);
                return this.countryRepository;
            }
        }

        public GenericRepository<BeneficiaryPaymentMethod> BeneficiaryPaymentMethodRepository
        {
            get
            {
                if (this.beneficiaryPaymentMethodRepository == null)
                    this.beneficiaryPaymentMethodRepository = new GenericRepository<BeneficiaryPaymentMethod>(this.context);
                return this.beneficiaryPaymentMethodRepository;
            }
        }
        public GenericRepository<CustomerDocument> CustomerDocumentRepository
        {
            get
            {
                if (this.customerDocumentRepository == null)
                    this.customerDocumentRepository = new GenericRepository<CustomerDocument>(this.context);
                return this.customerDocumentRepository;
            }
        }
        public GenericRepository<Login> LoginRepository
        {
            get
            {
                if (this.loginRepository == null)
                    this.loginRepository = new GenericRepository<Login>(this.context);
                return this.loginRepository;
            }
        }
        public GenericRepository<Agent> AgentRepository
        {
            get
            {
                if (this.agentRepository == null)
                    this.agentRepository = new GenericRepository<Agent>(this.context);
                return this.agentRepository;
            }
        }

        public GenericRepository<Transaction> TransactionRepository
        {
            get
            {
                if (this.transactionRepository == null)
                    this.transactionRepository = new GenericRepository<Transaction>(this.context);
                return this.transactionRepository;
            }
        }

        public GenericRepository<Customer> CustomerRepository
        {
            get
            {
                if (this.customerRepository == null)
                    this.customerRepository = new GenericRepository<Customer>(this.context);
                return this.customerRepository;
            }
        }

        public GenericRepository<Beneficiary> BeneficiaryRepository
        {
            get
            {
                if (this.beneficiaryRepository == null)
                    this.beneficiaryRepository = new GenericRepository<Beneficiary>(this.context);
                return this.beneficiaryRepository;
            }
        }

        public GenericRepository<IncomingAgent> IncomingAgentRepository
        {
            get
            {
                if (this.incomingAgentRepository == null)
                    this.incomingAgentRepository = new GenericRepository<IncomingAgent>(this.context);
                return this.incomingAgentRepository;
            }
        }
        public GenericRepository<InCustomer> InCustomerRepositoryRepository
        {
            get
            {
                if (this.inCustomerRepository == null)
                    this.inCustomerRepository = new GenericRepository<InCustomer>(this.context);
                return this.inCustomerRepository;
            }
        }

        public GenericRepository<InboundTransaction> InBoundTransactionRepository
        {
            get
            {
                if (this.inboundTransactionRepository == null)
                    this.inboundTransactionRepository = new GenericRepository<InboundTransaction>(this.context);
                return this.inboundTransactionRepository;
            }
        }
        public GenericRepository<Setting> SettingRepository
        {
            get
            {
                if (this.settingRepository == null)
                    this.settingRepository = new GenericRepository<Setting>(this.context);
                return this.settingRepository;
            }
        }


        //public DbSet<UserProfileView> GetUserRoles
        //{
        //    get
        //    {
        //        return this.context.UserProfileViews;
        //    }
        //}

        public UnitOfWorks(KaprukaEntities entities)
        {
            this.context = entities;
        }

        public void Save()
        {
            this.context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
                this.context.Dispose();
            this.disposed = true;
        }
        public interface IDisposable
        {
            void Dispose();
        }
    }
}
