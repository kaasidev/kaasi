﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Repository
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal KaprukaEntities context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(KaprukaEntities context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters)
        {
            return (IEnumerable<TEntity>)this.dbSet.SqlQuery(query, parameters).ToList<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> source = filter == null ? (IQueryable<TEntity>)this.dbSet : this.dbSet.Where<TEntity>(filter);
            string str = includeProperties;
            char[] separator = new char[1] { ',' };
            int num = 1;
            foreach (string path in str.Split(separator, (StringSplitOptions)num))
                source = source.Include<TEntity>(path);
            if (orderBy != null)
                return (IEnumerable<TEntity>)orderBy(source).ToList<TEntity>();
            return (IEnumerable<TEntity>)source.ToList<TEntity>();
        }

        public virtual IList<IGrouping<string, TEntity>> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<TEntity, string> groupby = null, int? takeCount = null, string includeProperties = "")
        {
            IQueryable<TEntity> source1 = (IQueryable<TEntity>)null;
            IQueryable<TEntity> source2 = filter == null ? (IQueryable<TEntity>)this.dbSet : source1.Where<TEntity>(filter);
            string str = includeProperties;
            char[] separator = new char[1] { ',' };
            int num = 1;
            foreach (string path in str.Split(separator, (StringSplitOptions)num))
                source2 = source2.Include<TEntity>(path);
            if (orderBy != null)
                source2 = (IQueryable<TEntity>)orderBy(source2);
            if (groupby == null)
                return (IList<IGrouping<string, TEntity>>)null;
            if (takeCount.HasValue)
                return (IList<IGrouping<string, TEntity>>)source2.GroupBy<TEntity, string>(groupby).Take<IGrouping<string, TEntity>>(takeCount.Value).ToList<IGrouping<string, TEntity>>();
            return (IList<IGrouping<string, TEntity>>)source2.GroupBy<TEntity, string>(groupby).ToList<IGrouping<string, TEntity>>();
        }

        public virtual TEntity Get(Expression<Func<TEntity, bool>> filter = null)
        {
            if (filter != null)
                return this.dbSet.FirstOrDefault<TEntity>(filter);
            return default(TEntity);
        }

        public virtual IEnumerable<long> GetIdList(bool isLimitNeeded, int takeCount, Func<TEntity, long> idSelectExpression, Expression<Func<TEntity, bool>> filter = null)
        {
            return filter == null ? (!isLimitNeeded ? (IEnumerable<long>)this.dbSet.Select<TEntity, long>(idSelectExpression).AsQueryable<long>() : (IEnumerable<long>)this.dbSet.Select<TEntity, long>(idSelectExpression).AsQueryable<long>().Take<long>(takeCount)) : (!isLimitNeeded ? (IEnumerable<long>)this.dbSet.Where<TEntity>(filter).Select<TEntity, long>(idSelectExpression).AsQueryable<long>() : (IEnumerable<long>)this.dbSet.Where<TEntity>(filter).Select<TEntity, long>(idSelectExpression).AsQueryable<long>().Take<long>(takeCount));
        }

        public virtual TEntity GetByID(object id)
        {
            return this.dbSet.Find(id);
        }

        public virtual TEntity GetByUniqueID(Func<TEntity, bool> predicate)
        {
            return this.dbSet.FirstOrDefault<TEntity>(predicate);
        }

        public virtual void Add(TEntity entity)
        {
            this.dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            this.Delete(this.dbSet.Find(id));
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (this.context.Entry<TEntity>(entityToDelete).State == EntityState.Detached)
                this.dbSet.Attach(entityToDelete);
            this.dbSet.Remove(entityToDelete);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return this.dbSet.AsEnumerable<TEntity>();
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            this.dbSet.Attach(entityToUpdate);
            this.context.Entry<TEntity>(entityToUpdate).State = EntityState.Modified;
        }
    }
}
