﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kapruka.Repository
{
    internal class DataAccessHelper
    {
        private static object obj = new object();
        private const string DATACONTEXTKEY = "EHScontext";
        private static volatile KaprukaEntities instance;

        public static KaprukaEntities _Context
        {
            get
            {
                KaprukaEntities lunchPacketEntities = DataAccessHelper.DataContext;
                if (lunchPacketEntities == null)
                {
                    lock (DataAccessHelper.obj)
                    {
                        if (lunchPacketEntities == null)
                        {
                            lunchPacketEntities = new KaprukaEntities();
                            DataAccessHelper.DataContext = lunchPacketEntities;
                        }
                    }
                }
                return lunchPacketEntities;
            }
        }

        private static KaprukaEntities DataContext
        {
            get
            {
                if (DataAccessHelper.IsInWebContext)
                    return (KaprukaEntities)HttpContext.Current.Items[(object)"EHScontext"];
                return (KaprukaEntities)CallContext.GetData("EHScontext");
            }
            set
            {
                if (DataAccessHelper.IsInWebContext)
                    HttpContext.Current.Items[(object)"EHScontext"] = (object)value;
                else
                    CallContext.SetData("EHScontext", (object)value);
            }
        }

        private static bool IsInWebContext
        {
            get
            {
                return HttpContext.Current != null;
            }
        }
    }
}
