//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kapruka.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditTrail
    {
        public int AuditID { get; set; }
        public Nullable<int> AgentID { get; set; }
        public string LoggedInUserName { get; set; }
        public string LoggedInAgentName { get; set; }
        public string Comments { get; set; }
        public Nullable<System.DateTime> AuditDateTime { get; set; }
    }
}
