//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kapruka.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Agent
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string AgentAddress1 { get; set; }
        public string AgentAddress2 { get; set; }
        public string AgentCity { get; set; }
        public string AgentState { get; set; }
        public string AgentPostcode { get; set; }
        public string AgentPhone { get; set; }
        public string AgentFax { get; set; }
        public string AgentCommissionStructure { get; set; }
        public Nullable<double> AgentFeeCommission { get; set; }
        public Nullable<decimal> AgentFeeFlat { get; set; }
        public Nullable<double> AgentTransGainCommission { get; set; }
        public Nullable<decimal> AgentRevenue { get; set; }
        public Nullable<int> AgentPendingTransactions { get; set; }
        public Nullable<int> AgentApprovedTransactions { get; set; }
        public string AgentTradingCurrency { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string AlteredBy { get; set; }
        public Nullable<System.DateTime> AlteredDateTime { get; set; }
        public string CustomAgentID { get; set; }
        public string AgentEmail { get; set; }
        public string AgentABN { get; set; }
        public string AgentACN { get; set; }
        public string AustracNumber { get; set; }
        public Nullable<System.DateTime> AustracRegDate { get; set; }
        public Nullable<System.DateTime> AustracRenewalDate { get; set; }
        public string AustracReNumber { get; set; }
        public Nullable<decimal> MonthlyAUDLimit { get; set; }
        public Nullable<decimal> YearlyAUDsLimit { get; set; }
        public Nullable<int> MonthlyTransLimit { get; set; }
        public Nullable<int> YearlyTransLimit { get; set; }
        public Nullable<int> FirstTimeLogin { get; set; }
        public string SMTPServerName { get; set; }
        public string SMTPPort { get; set; }
        public string SSLEnabled { get; set; }
        public string SMTPUsername { get; set; }
        public string SMTPassword { get; set; }
        public string SMTPEmailAddress { get; set; }
        public string HasKYC { get; set; }
        public string KYCResult { get; set; }
        public Nullable<System.DateTime> KYCCheckDate { get; set; }
        public string agentTransactionID { get; set; }
        public string TransactionID { get; set; }
        public string resultCode { get; set; }
        public string resultDescription { get; set; }
        public string CCEmailAddress { get; set; }
    }
}
