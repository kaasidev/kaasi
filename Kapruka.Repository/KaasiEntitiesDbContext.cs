﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Kapruka.Repository
{
    public class KaasiEntitiesDbContext : IDisposable
    {
        private readonly KaprukaEntities _entities;
        bool disposed = false;

        public KaasiEntitiesDbContext(KaprukaEntities entities)
        {
            this._entities = entities;
            //LogManager.GetLogger(this.GetType()).Debug("Created Repository!!!");
        }

       

        public KaprukaEntities DbEntities { get { return this._entities; } }

        public void SaveChanges()
        {
            this._entities.SaveChanges();
        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
            //LogManager.GetLogger(this.GetType()).Debug("Dispose Repository!!!");
        }
        protected virtual void Dispose(bool disposing)
        {
            var er = this._entities.GetValidationErrors();
            this._entities.Dispose();

            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here. 
                this._entities.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }

    }

}