//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kapruka.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class CountryCurrency
    {
        public int CountriesCurrencyID { get; set; }
        public Nullable<int> CountryID { get; set; }
        public Nullable<int> CurrencyID { get; set; }
    }
}
