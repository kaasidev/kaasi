﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.Inwards
{
    public partial class inSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadAgentList();
            putMonthsinDDL();
        }

        private void loadAgentList()
        {
            IncomingAgentService IAServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));
            var IADetails = IAServ.GetAll().ToList();

            DDL_AgentList.DataTextField = "AgentName";
            DDL_AgentList.DataValueField = "AgentID";
            DDL_AgentList.DataSource = IADetails;
            DDL_AgentList.DataBind();

            DDL_AgentList.Items.Insert(0, new ListItem("-- SELECT AGENT --"));
        }

        private void putMonthsinDDL()
        {
            DDL_NbMonthKYCExpires.Items.Insert(0, new ListItem("3"));
            DDL_NbMonthKYCExpires.Items.Insert(1, new ListItem("6"));
            DDL_NbMonthKYCExpires.Items.Insert(2, new ListItem("9"));
            DDL_NbMonthKYCExpires.Items.Insert(3, new ListItem("12"));
            DDL_NbMonthKYCExpires.Items.Insert(4, new ListItem("18"));
            DDL_NbMonthKYCExpires.Items.Insert(5, new ListItem("24"));

            setSystemSettings();
        }

        private void setSystemSettings()
        {
            SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var DollarLimit = SetServ.GetAll(x => x.SettingName == "InwardDollarLimit", null, "").SingleOrDefault();
            var SetDetails = SetServ.GetAll(x => x.SettingName == "InwardNbMonthsKYCExpire", null, "").SingleOrDefault();

            LBL_DollarLimit.InnerText = DollarLimit.SettingsVariable;
            TXT_DollarLimit.Text = DollarLimit.SettingsVariable;

            DDL_NbMonthKYCExpires.SelectedValue = SetDetails.SettingsVariable;
            LBL_NbMonthsKYCExpire.InnerText = SetDetails.SettingsVariable;
        }
    }
}