﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend
{
    public partial class InitialImport : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"].ToString() == "Agent")
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                String FullName = String.Empty;
                String CompaneName = String.Empty;
                String AID = Session["LoggedUserID"].ToString();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT FullName FROM dbo.Logins WHERE LoginID = " + Session["LoggedUserID"].ToString();
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            FullName = sdr["FullName"].ToString();
                        }
                    }
                    conn.Close();
                }

                using (SqlCommand cmd2 = new SqlCommand())
                {
                    cmd2.CommandText = "SELECT AgentName FROM dbo.Agents WHERE AgentID = " + Session["AgentID"].ToString();
                    cmd2.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd2.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CompaneName = sdr["AgentName"].ToString();
                        }
                    }
                    conn.Close();
                }

                AdminNameTitle.Text = CompaneName + " (" + FullName + ")";

            }
            else
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                String FullName = String.Empty;
                String AID = Session["LoggedID"].ToString();

                using (SqlCommand cmd2 = new SqlCommand())
                {
                    cmd2.CommandText = "SELECT FullName FROM dbo.Logins WHERE LoginID = " + Session["LoggedUserID"].ToString();
                    cmd2.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd2.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            FullName = sdr["FullName"].ToString();
                        }
                    }
                    conn.Close();
                }

                AdminNameTitle.Text = "FlexeWallet" + " (" + FullName + ") - You are logged in as Administrator";

                // ********** CSS manipulation for theming the program **********************
                //DIV_Container.Attributes.CssStyle.Remove("background-color");
                //DIV_Container.Attributes.CssStyle.Add("background-color", "red");
            }
        }
    }
}