﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.PublicModels;
using Kapruka.Repository;

namespace KASI_Extend.Inwards
{
    public partial class inReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/InwardTransactionReport.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/NonKYCCustomers.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/MonthlyRevenueReport.aspx");
        }

        protected void btnAustract_Click(object sender, EventArgs e)
        {
            InwardTransactionService ITServ = new InwardTransactionService(new UnitOfWorks(new KaprukaEntities()));
            var TransList = ITServ.GetAll(x => x.InsertedIntoIFTIFile == false, null, "").ToList();

            InMasterAustracRecordService MARServ = new InMasterAustracRecordService(new UnitOfWorks(new KaprukaEntities()));

            DateTime today = DateTime.Now.Date;
            String todaysdate = DateTime.Now.ToString("yyyyMMdd");
            String justthedate = today.ToString("dd/MM/yyyy");
            var theNextNumber = "";

            var theSequenceNumber = MARServ.GetAll(x => x.CreatedDate == today, null, "").SingleOrDefault();

            if (theSequenceNumber != null)
            {
                theNextNumber = (Int32.Parse(theSequenceNumber.FileSequenceNumber) + 1).ToString().PadLeft(2, '0');
                theSequenceNumber.FileSequenceNumber = theNextNumber;
                MARServ.Update(theSequenceNumber);
            }
            else
            {
                var NewSequence = new InMasterAustracRecord();
                theNextNumber = "01";
                NewSequence.CreatedDate = today;
                NewSequence.FileSequenceNumber = theNextNumber;
                MARServ.Add(NewSequence);
            }

            String filename = "IFTI-E" + todaysdate + theNextNumber;
            IFTIModel.IftidraList IFTIModel = new IFTIModel.IftidraList();
            IFTIModel.VersionMajor = "1";
            IFTIModel.VersionMinor = "1";

            IFTIModel = GenerateIFTIReportXML(TransList, filename);
           
        }

        private Kapruka.PublicModels.IFTIModel.IftidraList GenerateIFTIReportXML(List<InboundTransaction> transactionList, String filename)
        {
            Kapruka.PublicModels.IFTIModel.IftidraList model = new Kapruka.PublicModels.IFTIModel.IftidraList();

            SettingsService SETServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var theReNumber = SETServ.GetAll(x => x.SettingName == "AUSTRACReportingNumber", null, "").SingleOrDefault().SettingsVariable;
            InCustomerService ICServ = new InCustomerService(new UnitOfWorks(new KaprukaEntities()));
            CustomerDocumentService CDServ = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            IncomingAgentService IAServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));

            CustomerDocument custDoc = new CustomerDocument();

            model.FileName = filename + ".xml";
            model.xmlns = null;

            model.renumber = theReNumber;

            model.Iftidra = new List<IFTIModel.Iftidra>();
            if(transactionList != null)
            {
                model.ReportCount = transactionList.Count();
            }
            else
            {
                model.ReportCount = 0;
            }

            foreach(InboundTransaction item in transactionList)
            {
                var ICDetails = ICServ.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                var CDDetails = CDServ.GetAll(x => x.CustomerID == item.CustomerID, null, "").ToList();
                var IADetails = IAServ.GetAll(x => x.AgentID == item.AgentID, null, "").SingleOrDefault();

                if (CDDetails.Count == 0)
                {
                    custDoc = null;
                }
                else
                {
                    custDoc = CDDetails[0];
                }

                IFTIModel.Iftidra dra = new IFTIModel.Iftidra();
                dra.Id = "ID_" + item.ID.ToString();
                dra.Header = new IFTIModel.Header();
                dra.Header.Id = "ID_" + item.ID.ToString() + "-0";
                dra.Header.InterceptFlag = "N";
                dra.Header.TxnRefNo = item.ID.ToString();

                dra.Transaction = new IFTIModel.Transaction();
                dra.Transaction.Id = "ID_" + item.ID.ToString() + "-1";
                dra.Transaction.TxnDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");
                dra.Transaction.CurrencyAmount = new IFTIModel.CurrencyAmount();
                dra.Transaction.CurrencyAmount.Id = "ID_" + item.ID.ToString() + "-11";
                dra.Transaction.CurrencyAmount.Amount = item.DollarAmount.Value.ToString("N2");
                dra.Transaction.CurrencyAmount.Currency = "AUD";

                dra.Transaction.direction = "I";
                dra.Transaction.TfrType = new IFTIModel.TfrType();
                dra.Transaction.TfrType.Type = "M";
                dra.Transaction.TfrType.Id = "ID_" + item.ID.ToString() + "-12";
                dra.Transaction.ValueDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");

                dra.Transferor = new IFTIModel.Transferor();
                dra.Transferor.Id = "ID_" + item.ID.ToString() + "-2";
                dra.Transferor.FullName = ICDetails.FullName;
                dra.Transferor.MainAddress = new IFTIModel.MainAddress();
                dra.Transferor.MainAddress.Addr = ICDetails.AddressLine1 + " " + ICDetails.AddressLine2;
                dra.Transferor.MainAddress.Country = ICDetails.Country;
                dra.Transferor.MainAddress.Id = "ID_" + item.ID.ToString() + "-21";
                dra.Transferor.MainAddress.Postcode = ICDetails.Postcode;
                dra.Transferor.MainAddress.State = ICDetails.State;
                dra.Transferor.MainAddress.Suburb = ICDetails.Suburb;
                dra.Transferor.MainAddress.Country = ICDetails.Country;

                dra.Transferor.Phone = ICDetails.Mobile;
                dra.Transferor.Email = ICDetails.EmailAddress;
                dra.Transferor.CustNo = ICDetails.CustomerID.Value.ToString();

                if(!String.IsNullOrEmpty(ICDetails.DOB))
                {
                    dra.Transferor.Dob = ICDetails.DOB.ToString();
                }
                dra.Transferor.Identification = new IFTIModel.Identification();
                dra.Transferor.Identification.Id = "ID_" + item.ID.ToString() + "-22";
                if (custDoc != null)
                {
                    String TypeOfDoc = String.Empty;
                    if (custDoc.TypeOfDocument == "Driver's Licence")
                    {
                        TypeOfDoc = "D";
                    }
                    else if (custDoc.TypeOfDocument == "Passport")
                    {
                        TypeOfDoc = "P";
                    }
                    dra.Transferor.Identification.Issuer = custDoc.IssuingAuthority;
                    dra.Transferor.Identification.Number = custDoc.DocumentNumber;
                    dra.Transferor.Identification.Type = TypeOfDoc;
                }
                else
                {
                    dra.Transferor.Identification.Issuer = "NO INFO";
                    dra.Transferor.Identification.Number = "NO INFO";
                    dra.Transferor.Identification.Type = "D";
                }

                dra.OrderingInstn = new IFTIModel.OrderingInstn();
                dra.OrderingInstn.Id = "ID_" + item.ID.ToString() + "-3";
                dra.OrderingInstn.Branch = new IFTIModel.Branch();
                dra.OrderingInstn.Branch.Id = "ID_" + item.ID.ToString() + "-31";
                dra.OrderingInstn.Branch.MainAddress = new IFTIModel.MainAddress();
                dra.OrderingInstn.Branch.MainAddress.Id = "ID_" + item.ID.ToString() + "-32";
                dra.OrderingInstn.Branch.MainAddress.Addr = IADetails.AgentAddress1;
                dra.OrderingInstn.Branch.MainAddress.Country = IADetails.AgentCountry;
                dra.OrderingInstn.Branch.MainAddress.Suburb = IADetails.AgentCity;
                dra.OrderingInstn.Branch.MainAddress.State = IADetails.AgentState;
                dra.OrderingInstn.Branch.MainAddress.Postcode = IADetails.AgentPostcode;
                dra.OrderingInstn.Branch.FullName = IADetails.AgentName;

                if (IADetails.AgentCountry == "AUSTRALIA" || IADetails.AgentCountry == "AUS")
                {
                    dra.OrderingInstn.ForeignBased = "N";
                }
                else
                {
                    dra.OrderingInstn.ForeignBased = "Y";
                }

            }

            return model;
        }
    }
}