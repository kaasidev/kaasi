﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend.inward.Processor
{
    public partial class ManualPassKYC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var BeneID = Int32.Parse(Request.QueryString["BID"].ToString());
            var ManualKYComments = Request.QueryString["KYCComments"].ToString();

            try
            {
                InboundBeneficiaryService IBServ = new InboundBeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
                var IBDetails = IBServ.GetAll(x => x.ID == BeneID, null, "").SingleOrDefault();

                IBDetails.ManualKYC = true;
                IBDetails.ManualKYCNotes = ManualKYComments;
                IBDetails.ManualKYCDateTime = DateTime.Now;
                IBDetails.resultCode = "0";
                IBDetails.resultDescription = "Success! (KYC Manually Approved)";

                IBServ.Update(IBDetails);

                InBeneAuditLogService IBALServ = new InBeneAuditLogService(new UnitOfWorks(new KaprukaEntities()));
                InBeneAuditLog newIBAL = new InBeneAuditLog();

                newIBAL.ActionType = "EDIT";
                newIBAL.AgentName = Session["LoggedCompany"].ToString();
                newIBAL.Username = Session["LoggedPersonName"].ToString();
                newIBAL.AuditDateTime = DateTime.Now;
                newIBAL.BeneID = BeneID;
                newIBAL.Description = "has been manually KYCed as SUCCESS";

                IBALServ.Add(newIBAL);


                InwardTransactionService ITServ = new InwardTransactionService(new UnitOfWorks(new KaprukaEntities()));
                var ITDetails = ITServ.GetAll(x => x.InboundBeneficiaryID == BeneID, null, "").ToList();

                InTransAuditLogServicecs ITALServ = new InTransAuditLogServicecs(new UnitOfWorks(new KaprukaEntities()));
                SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
                var DollarLimit = SetServ.GetAll(x => x.SettingName == "InwardDollarLimit", null, "").SingleOrDefault();

                foreach (var trans in ITDetails)
                {
                    if (Decimal.Parse(trans.DollarAmount.ToString()) > Decimal.Parse(DollarLimit.SettingsVariable.ToString()))
                    {
                        trans.Status = "PENDING";
                        ITServ.Update(trans);

                        InTransAuditLog newITAL = new InTransAuditLog();
                        newITAL.ActionType = "ADD";
                        newITAL.AgentName = Session["LoggedCompany"].ToString();
                        newITAL.Username = Session["LoggedPersonName"].ToString();
                        newITAL.AuditDateTime = DateTime.Now;
                        newITAL.TransactionID = trans.ID;
                        newITAL.Description = "KYC manually approved. Dollar Limit Exceeded. Transaction will remain in PENDING";

                        ITALServ.Add(newITAL);
                    }
                    else
                    {
                        trans.Status = "SUCCESSFUL";
                        ITServ.Update(trans);

                        InTransAuditLog newITAL = new InTransAuditLog();
                        newITAL.ActionType = "ADD";
                        newITAL.AgentName = Session["LoggedCompany"].ToString();
                        newITAL.Username = Session["LoggedPersonName"].ToString();
                        newITAL.AuditDateTime = DateTime.Now;
                        newITAL.TransactionID = trans.ID;
                        newITAL.Description = "has been changed to SUCCESSFUL after KYC was manually approved.";

                        ITALServ.Add(newITAL);
                    }

                    

                    
                }




                Response.Write("OK");

            }
            catch (Exception ex)
            {
                Response.Write("An error has occured. Please contact your administrator");
            }
            
        }
    }
}