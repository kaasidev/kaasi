﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.inward.Processor
{
    public partial class addNewRemitter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IncomingAgentService IAServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));

            IncomingAgent IAgent = new IncomingAgent();
            if (Request.QueryString["AgentId"] != null)
            {
                var agentId = Convert.ToInt32(Request.QueryString["AgentId"]);
                IAgent = IAServ.GetAll().Where(x => x.AgentID == agentId).SingleOrDefault();
                IAgent.AlteredDateTime = DateTime.Now;
                IAgent.AlteredBy = Session["usernameLogged"].ToString();

            }
            IAgent.AgentName = Request.QueryString["NR"].ToString();
            IAgent.AgentAddress1 = Request.QueryString["ADL1"].ToString();
            IAgent.AgentAddress2 = Request.QueryString["ADL2"].ToString();
            IAgent.AgentCity = Request.QueryString["City"].ToString();
            IAgent.AgentState = Request.QueryString["State"].ToString();
            IAgent.AgentPostcode = Request.QueryString["PCode"].ToString();
            IAgent.AgentCountry = Request.QueryString["Country"].ToString();
            IAgent.AgentPhone = Request.QueryString["Phone"].ToString();
            IAgent.AgentEmail = Request.QueryString["Email1"].ToString();
            IAgent.AgentEmail2 = Request.QueryString["Email2"].ToString();
            IAgent.Website = Request.QueryString["Web"].ToString();
            IAgent.Status = "Y";
            if (Request.QueryString["AgentId"] != null)
            {
                IAServ.Update(IAgent);
            }
            else
            {
                IAgent.CreatedDateTime = DateTime.Now;
                IAgent.CreatedBy = Session["usernameLogged"].ToString();
                IAServ.Add(IAgent);
            }
        }
    }
}