﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend.inward.Processor
{
    public partial class DeclineManualKYC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var BeneID = Int32.Parse(Request.QueryString["BID"].ToString());
                var ManualKYComments = Request.QueryString["KYCComments"].ToString();

                InboundBeneficiaryService IBServ = new InboundBeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
                var IBDetails = IBServ.GetAll(x => x.ID == BeneID, null, "").SingleOrDefault();

                IBDetails.ManualKYC = true;
                IBDetails.ManualKYCNotes = ManualKYComments;
                IBDetails.ManualKYCDateTime = DateTime.Now;
                IBDetails.resultCode = "-1";
                IBDetails.resultDescription = "Failed! (KYC Manually Declined)";

                IBServ.Update(IBDetails);

                InBeneAuditLogService IBALServ = new InBeneAuditLogService(new UnitOfWorks(new KaprukaEntities()));
                InBeneAuditLog newIBAL = new InBeneAuditLog();

                newIBAL.ActionType = "EDIT";
                newIBAL.AgentName = Session["LoggedCompany"].ToString();
                newIBAL.Username = Session["LoggedPersonName"].ToString();
                newIBAL.AuditDateTime = DateTime.Now;
                newIBAL.BeneID = BeneID;
                newIBAL.Description = "KYC has been manually failed";

                IBALServ.Add(newIBAL);

                Response.Write("OK");
            }
            catch (Exception ex)
            {
                Response.Write("An error occured. Please contact your system administrator");
            }
            
        }
    }
}