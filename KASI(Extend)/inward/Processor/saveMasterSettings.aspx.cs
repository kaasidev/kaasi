﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.inward.Processor
{
    public partial class saveMasterSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
                var SetDetails = SetServ.GetAll(x => x.SettingName == "InwardNbMonthsKYCExpire", null, "").SingleOrDefault();
                var DollarLimit = SetServ.GetAll(x => x.SettingName == "InwardDollarLimit", null, "").SingleOrDefault();

                SetDetails.SettingsVariable = Request.QueryString["NbMonths"].ToString();
                DollarLimit.SettingsVariable = Request.QueryString["DLimit"].ToString();

                SetServ.Update(SetDetails);
                SetServ.Update(DollarLimit);

                Response.Write("OK");
            }
            catch (Exception ex)
            {
                Response.Write("An error has occured. Please contact your system Administrator");
            }
           
        }
    }
}