﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;

namespace KASI_Extend.inward.Reports
{
    public partial class MonthlyRevenueReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void ShowReport()
        {
            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(DateTime.Parse(txtfrom.Text), DateTime.Parse(txtto.Text));
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = @"inward\Reports\MonthlyRevenueReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("fromDate", txtfrom.Text),
                new ReportParameter("toDate", txtto.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();
        }

        private DataTable GetData(DateTime fromDate, DateTime toDate)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = new SqlCommand("REPORT_getMonthlyTranscations", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            return dt;
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtfrom.Text) || String.IsNullOrEmpty(txtto.Text))
            {
                LBL_ErrorMessage.Style.Add("display", "block");
                LBL_ErrorMessage.Text = "Please select date range before running report.";
            }
            else
            {
                LBL_ErrorMessage.Style.Add("display", "none");
                LBL_ErrorMessage.Text = "";
                ShowReport();
            }
            
        }
    }
}