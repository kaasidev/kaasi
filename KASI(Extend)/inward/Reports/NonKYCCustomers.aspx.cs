﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;

namespace KASI_Extend.inward.Reports
{
    public partial class NonKYCCustomers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void ShowReport()
        {
            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData( AgentID.Text);
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = @"inward\Reports\NonKYCCustomers.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("AID", AgentID.ToString()),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();
        }

        private DataTable GetData(String AID)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = new SqlCommand("REPORT_getInwardNONKYCCustomers", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AgentID", SqlDbType.Int).Value = AID;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            return dt;
        }
    }
}