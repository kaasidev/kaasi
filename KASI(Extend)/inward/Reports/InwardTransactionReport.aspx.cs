﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.inward.Reports
{
    public partial class InwardTransactionReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IncomingAgentService InAGTServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));
                var InAGTDetails = InAGTServ.GetAll().ToList();


                DDL_AgentID.DataTextField = "AgentName";
                DDL_AgentID.DataValueField = "AgentID";
                DDL_AgentID.DataSource = InAGTDetails;
                DDL_AgentID.DataBind();
                DDL_AgentID.Items.Insert(0, new ListItem("-- SELECT AGENT --"));
            }
            
            
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtfrom.Text) || String.IsNullOrEmpty(txtto.Text))
            {
                LBL_ErrorMessage.Style.Add("display", "block");
                LBL_ErrorMessage.Text = "Please select date range before running report.";
            }
            else
            {
                if (DDL_AgentID.SelectedIndex == 0)
                {
                    LBL_ErrorMessage.Style.Add("display", "block");
                    LBL_ErrorMessage.Text = "Please select agent before running report.";
                }
                else
                {
                    LBL_ErrorMessage.Style.Add("display", "none");
                    LBL_ErrorMessage.Text = "";
                    ShowReport();
                }
            }
           
            
        }

        private void ShowReport()
        {
            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(DateTime.Parse(txtfrom.Text), DateTime.Parse(txtto.Text), DDL_AgentID.SelectedValue);
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = @"inward\Reports\InwardTransactionReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("fromDate", txtfrom.Text),
                new ReportParameter("toDate", txtto.Text),
                new ReportParameter("AID", DDL_AgentID.SelectedValue.ToString()),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();
        }

        private DataTable GetData(DateTime fromDate, DateTime toDate, String AID)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = new SqlCommand("REPORT_getInwardTransactions", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                cmd.Parameters.Add("@AgentID", SqlDbType.Int).Value = AID;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            return dt;
        }
    }
}