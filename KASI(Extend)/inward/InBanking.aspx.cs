﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;
using System.Text;
using System.IO;

namespace KASI_Extend.Inwards
{
    public partial class InBanking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAllAgents();
                showABAStats();
            }

            
        }

        private void GetAllAgents()
        {
            IncomingAgentService ser = new IncomingAgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            //agentDrop.Items.Add(new ListItem("-- SELECT -- ", "0"));
            var agList = ser.GetAll().ToList();

            agentDrop.DataSource = agList;
            agentDrop.DataTextField = "AgentName";
            agentDrop.DataValueField = "AgentID";
            agentDrop.DataBind();

            agentDrop.Items.Insert(0, new ListItem("-- SELECT -- "));
        }

        protected void btn_CreateABAFile_Click(object sender, EventArgs e)
        {
            IncomingTransactionService ITServ = new IncomingTransactionService(new UnitOfWorks(new KaprukaEntities()));
            var SelAgentID = Int32.Parse(agentDrop.SelectedValue);
            var ITDetails = ITServ.GetAll(x => x.AgentID == SelAgentID && x.InsertedIntoABAFile == false && x.Status == "SUCCESSFUL", null, "");

            SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var SetDetails = SetServ.GetAll(x => x.SettingName == "MasterCompany", null, "").SingleOrDefault();

            float TotalAmount = 0;
            int TotTrans = 0;

            List<String> TransIDList = new List<string>();

            StringBuilder sb = new StringBuilder();
            sb.Append("0"); // Record Type 0
            sb.Append("".PadRight(17, ' ')); // Must be left blank
            sb.Append("01"); // Reel Sequence Number which has to be calculated based on the # of bank file uploads per day
            sb.Append("NAB"); //Name of the instituion abbreviated
            sb.Append("".PadRight(7, ' ')); // Must be left blank
            sb.Append(SetDetails.SettingsVariable.PadRight(26, ' ')); // Name of Use supplying file
            sb.Append("11111".PadLeft(6, '0')); // User Identification Number allocated by APCA
            sb.Append("REMITTANCE".PadRight(12, ' ')); //Description of entries
            DateTime today = new DateTime().Date;
            String theFormattedDate = today.ToString("dd/MM/yy").Replace("/", "");
            sb.Append(theFormattedDate); // Date to be processed;
            sb.Append("".PadLeft(40, ' '));
            sb.Append("\r\n");
            foreach(var Transaction in ITDetails)
            {
                sb.Append("1"); // Record Type 1
                var TheNewString = "";
                if (Transaction.BSB.Length < 7)
                {
                    TheNewString = Transaction.BSB.Substring(0, 3) + "-" + Transaction.BSB.Substring(3, 3);
                }
                else
                {
                    TheNewString = Transaction.BSB;
                }
                sb.Append(TheNewString); //BSB Number
                sb.Append(Transaction.AccountNumber.PadLeft(9, '0'));
                sb.Append(" "); // Indication
                sb.Append("53"); // Transaction Code refer to as PAY need to double check
                sb.Append(String.Format("{0:N2}", float.Parse(Transaction.DollarAmount.ToString())).Replace(",", "").Replace(".", "").PadLeft(10, '0'));
                if (Transaction.BeneficiaryName.Length > 32)
                {
                    sb.Append(Transaction.BeneficiaryName.Substring(0, 32));
                }
                else
                {
                    sb.Append(Transaction.BeneficiaryName.PadRight(32, ' '));
                }
                sb.Append(Transaction.ID.ToString().PadRight(18, ' '));
                sb.Append("063-123"); // Master Company BSB
                sb.Append("5161561".PadLeft(9, ' ')); // Master Company Account Number
                sb.Append(SetDetails.SettingsVariable.ToString().PadRight(16, ' '));
                sb.Append("".PadRight(8, '0'));
                sb.Append("\r\n");

                TransIDList.Add(Transaction.ID.ToString());
                TotalAmount += float.Parse(Transaction.DollarAmount.ToString());
                TotTrans++;
            }

            sb.Append("7"); // Record Type 
            sb.Append("999-999"); // BSB Format Filler
            sb.Append("".PadRight(12, ' ')); // Must be left blank
            sb.Append(String.Format("{0:N2}", float.Parse(TotalAmount.ToString())).Replace(",", "").Replace(".", "").PadLeft(10, '0'));
            sb.Append("".PadLeft(10, '0'));
            sb.Append(String.Format("{0:N2}", float.Parse(TotalAmount.ToString())).Replace(",", "").Replace(".", "").PadLeft(10, '0'));
            sb.Append("".PadLeft(24, ' '));
            sb.Append(TotTrans.ToString().PadLeft(6, '0'));
            sb.Append("".PadLeft(40, ' '));

            bool writeSuccess = false;
            String filename = agentDrop.SelectedItem + "_" + theFormattedDate + ".ABA";

            try
            {
                
                string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                StreamWriter file = new StreamWriter(filePath);
                file.WriteLine(sb.ToString());
                file.Close();
                writeSuccess = true;
            }
            catch (Exception ex)
            {

            }

            if (writeSuccess == true)
            {
                markTransactionAsInsertedintoABAFile(TransIDList, filename);
            }
            showABAStats();




            string redirect = "<script>window.open('/FileDownloader.aspx?file=" + filename + "&RC=ABABANK','_blank');</script>";
            Response.Write(redirect);

        }

        private void markTransactionAsInsertedintoABAFile(List<String> TransList, String filename)
        {
            InwardTransactionService ITServ = new InwardTransactionService(new UnitOfWorks(new KaprukaEntities()));
            InTransAuditLogServicecs ITALServ = new InTransAuditLogServicecs(new UnitOfWorks(new KaprukaEntities()));

            foreach (var TransID in TransList)
            {
                var IntTransID = Int32.Parse(TransID);
                var ITDetails = ITServ.GetAll(x => x.ID == IntTransID, null, "").SingleOrDefault();

                ITDetails.InsertedIntoABAFile = true;
                ITDetails.InsertedIntoABAFileBy = Session["LoggedPersonName"].ToString();
                ITDetails.InsertedIntoABAFileDateTime = DateTime.Now;

                ITServ.Update(ITDetails);

                InTransAuditLog newITAL = new InTransAuditLog();
                newITAL.ActionType = "EDIT";
                newITAL.AgentName = Session["LoggedCompany"].ToString();
                newITAL.Username = Session["LoggedPersonName"].ToString();
                newITAL.AuditDateTime = DateTime.Now;
                newITAL.TransactionID = IntTransID;
                newITAL.Description = "has been successfully entered into " + filename;

                ITALServ.Add(newITAL);
            }

            
        }

        private void showABAStats()
        {
            KaprukaEntities context = new KaprukaEntities();
            var ITDetails = (from c in context.InboundTransactions
                             join d in context.IncomingAgents
                             on c.AgentID equals d.AgentID
                             where c.Status == "SUCCESSFUL" && c.InsertedIntoABAFile == false
                             group new { c, d } by new { d.AgentName } into g
                             select new TotalTransInfo
                             {
                                 AgentName = g.Key.AgentName,
                                 InTransList = g.Select(x => x.c).ToList()

                             }).ToList();
            // select new TotalTransInfo { AgentName = d.Name , InTransList=g.ToList() });

            TableHeaderRow header = new TableHeaderRow();
            TableHeaderCell headerTableCell1 = new TableHeaderCell();
            TableHeaderCell headerTableCell2 = new TableHeaderCell();
            TableHeaderCell headerTableCell3 = new TableHeaderCell();

            headerTableCell1.Text = "Remitter";
            headerTableCell1.Attributes.Add("Class", "bank_aba_heading_01");
            headerTableCell2.Text = "";
            headerTableCell2.Attributes.Add("Class", "bank_aba_heading_02");
            headerTableCell3.Text = "TXNs";
            headerTableCell3.Attributes.Add("Class", "bank_aba_heading_03");

            header.Cells.Add(headerTableCell1);
            header.Cells.Add(headerTableCell2);
            header.Cells.Add(headerTableCell3);

            TBL_ABAStats.Rows.Add(header);

            foreach (var Trans in ITDetails)
            {
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                TableCell cell3 = new TableCell();
                TableRow trow1 = new TableRow();
                Literal lt = new Literal();

                cell1.Text = Trans.AgentName;
                cell1.Attributes.Add("Class", "bank_aba_contents_01");
                cell2.Controls.Add(lt);
                cell3.Text = Trans.InTransList.Count().ToString();
                cell3.Attributes.Add("Class", "bank_aba_contents_03");

                trow1.Cells.Add(cell1);
                trow1.Cells.Add(cell2);
                trow1.Cells.Add(cell3);

                TBL_ABAStats.Rows.Add(trow1);
            }



            //TBL_ABAStats
        }

        public class TotalTransInfo
        {
            public string AgentName { get; set; }
            public List<InboundTransaction> InTransList { get; set; }
        }
    }
}