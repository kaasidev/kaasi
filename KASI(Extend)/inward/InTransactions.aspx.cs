﻿using Kapruka.Domain;
using Kapruka.Enterprise;
using Kapruka.Repository;
using Kapruka.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend.Inwards
{
    public partial class InTransactions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SPN_LoggedUserName.InnerText = Session["LoggedPersonName"].ToString();
        }
        [WebMethod()]
        public static KycResultInward CheckKCYStatus(int inboundBeneID)
        {
            return GetKycCheckStatus(inboundBeneID);
        }

        private static KycResultInward GetKycCheckStatus(int inboundBeneID)
        {
            var obResult = new KycResultInward();
            var status = "-2";

            var inBoundObj = new InboundIFTIService().GetInboundBeneById(inboundBeneID);
            if (inBoundObj != null)
            {
                if (inBoundObj.resultCode != null && inBoundObj.resultCode == "0")
                    status = "0";
                else if (inBoundObj.resultCode != null && inBoundObj.resultCode == "-1")
                    status = "-1";

                obResult.Status = status;

                if (inBoundObj.KYCBy != null)
                    obResult.KycBy = inBoundObj.KYCBy;
                if (inBoundObj.KYCCheckDate != null)
                    obResult.KyCheckDate = inBoundObj.KYCCheckDate;
                if (inBoundObj.transactionId != null)
                    obResult.TransactionId = inBoundObj.transactionId;
                if (inBoundObj.resultDescription != null)
                    obResult.ResultDescription = inBoundObj.resultDescription;
            }

            return obResult;
        }


        [WebMethod()]
        public static KycResultInward RunKycCheckStatus(int inboundBeneID)
        {
            var accouBeneDetai = new InboundIFTIService().GetInboundBeneById(inboundBeneID); ;

            CheckKycCustomer(accouBeneDetai.BeneFullName, accouBeneDetai.BeneFullName,
                accouBeneDetai.BeneFullName, null, accouBeneDetai.ID, null, null, null, null, null, null, accouBeneDetai.ID);


            var accouResBeneDetai = GetKycCheckStatus(inboundBeneID);
            if(accouResBeneDetai!=null){
                if (accouResBeneDetai.Status == "0")
                    UpdateAlltransactionsByBeneficiaryId(inboundBeneID);
            }
            return accouResBeneDetai;
        }

        private static void UpdateAlltransactionsByBeneficiaryId(int beneID)
        {
            var respo = new KaprukaEntities();

            var allPendingTransactionByBeneId = (from beneTran in respo.InboundTransactions
                                                 where beneTran.InboundBeneficiaryID == beneID
                                                 select beneTran).ToList();
            foreach (var item in allPendingTransactionByBeneId)
            {
                item.Status = "SUCCESSFUL";
            }
            var inservice = new IncomingTransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            foreach (var item in allPendingTransactionByBeneId)
            {
                inservice.Update(item);

            }
        }

        



        private static void CheckKycCustomer(string title, string firstName, string lastName,
         string dob, int customerId, string customerCity, string customerCountry,
          string customerState, string postcode, string email, string phone, int customerRecoId)
        {
            var cusKyc = new CustomerKyced();
            cusKyc.profile = "FW2";
            if (title != null)
                cusKyc.title = title;
            else
                cusKyc.title = null;
            if (firstName != null)
                cusKyc.bfn = firstName;
            else
                cusKyc.bfn = null;

            if (lastName != null)
                cusKyc.bln = lastName;
            else
                cusKyc.bln = null;
            if (dob != null)
            {

                cusKyc.dob = ConvertDateFormat(dob);
            }
            else
                cusKyc.dob = null;
            if (customerId > 0)
                cusKyc.man = customerId.ToString();
            else
                cusKyc.man = null;
            if (customerCity != null)
                cusKyc.bc = customerCity;
            else
                cusKyc.bc = null;
            if (customerCountry != null)
            {
                if (customerCountry.Length >= 2)
                    cusKyc.bco = customerCountry.Substring(0, 2).ToUpper();
                else
                    cusKyc.bco = "AU";
            }
            else
                cusKyc.bco = "AU";

            if (customerState != null)
                cusKyc.bs = customerState;
            else
                cusKyc.bs = null;

            cusKyc.bsn = null;
            if (postcode != null)
                cusKyc.bz = postcode;
            else
                cusKyc.bz = null;
            if (phone != null)
                cusKyc.pw = phone;
            else
                cusKyc.pw = null;
            if (email != null)
                cusKyc.tea = email;
            else
                cusKyc.tea = "";
            cusKyc.faceImage = null;

            cusKyc.documentImageFront = null;
            //cusKyc.documentImangeBack = "";
            cusKyc.securityNumber = null;
            cusKyc.ip = null;

            var runBy = "";
            var cService = new Kapruka.Service.CustomerHandlerService();
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            cService.CreateKycedIncoimmgCustomer(cusKyc, customerId, customerRecoId, runBy);
        }

        private static string ConvertDateFormat(string date)
        {
            var dateS = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
            return dateS;
        }
    }
}