﻿<%@ Page Title="" Language="C#" MasterPageFile="Import.Master" AutoEventWireup="true" CodeBehind="InBanking.aspx.cs" Inherits="KASI_Extend.Inwards.InBanking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- JS FILES -->
    <script type="text/javascript" charset="utf-8" src="../js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/jquery.dataTables.min.js"></script>

    <!-- CSS FILES -->
    <link href="../css/jquery-ui.min.css" rel="stylesheet" />
    <link href="../css/datatables.min.transactions.css" rel="stylesheet" />

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet" />

    <style type="text/css">
        .auto-style4 {
            height: 20px;
        }
        .remitter_dropdown {
            font-family: 'Gotham Narrow Book' !important;
            font-size: 13px !important;
            height: 35px !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <form id="form1" runat="server">
        <div class="bg_DCDCDC">
            <table class="tbl_width_1200">
                <tr class="spacer20"><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td class="page_headings">Inward Banking</td>
                                <td style="text-align:right;"><asp:Button runat="server" ID="AustractBtn" CssClass="aa_btn_red"  Text="Create IFTI-IN File"  /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer20"><td>&nbsp;</td></tr>
            </table>
        </div>

    <div class="bg_EFEFEF">
        <div>
            <table class="tbl_width_1200">
                <tr class="spacer20"><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td style="width:280px; vertical-align:top;">
                                    <table class="tbl_width_280">
                                        <tr class="bg_3498DB">
                                            <td>
                                                <table class="tbl_width_260">
                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr><td class="banking_left_heading">Transactions to be remitted...</td></tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="bg_F4F5F7"><td><asp:Table ID="TBL_ABAStats" runat="server"></asp:Table></td></tr>
                                    </table>
                                </td>
                                <td style="width:20px;">&nbsp;</td>
                                <td style="width:900px; vertical-align:top;">
                                    <table class="tbl_width_900">
                                        <tr class="bg_FFFFFF spacer20"><td>&nbsp;</td></tr>
                                        <tr class="bg_FFFFFF">
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr><td class="txn_tab_headings">Create Australian Bankers Association (ABA) Files</td></tr>
                                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                                    <tr><td class="page_contents_small">Please select the Remitter (Company) from the drop down menu below and click on the Create ABA File button to create the ABA File to download a copy.</td></tr>
                                                    <tr class="spacer20"><td>&nbsp;</td></tr>
                                                    <tr><td><asp:DropDownList ID="agentDrop" runat="server" class="modal_input_dropdown" style="width:430px;"></asp:DropDownList></td></tr>
                                                    <tr><td class="auto-style4"></td></tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr><td><asp:Button ID="btn_CreateABAFile" runat="server" Text="Create ABA File" CssClass="in_btn_blue" OnClick="btn_CreateABAFile_Click" /></td></tr>
                                                    <tr><td>&nbsp;</td></tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="bg_FFFFFF spacer20"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer20"><td>&nbsp;</td></tr>
                <tr class="spacer20"><td>&nbsp;</td></tr>
            </table>
        </div>
    </div>

    </form>

</asp:Content>
