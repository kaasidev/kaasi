﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Domain;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Text;
using Kapruka.Enterprise;
using Kapruka.Repository;
using System.Data.SqlClient;
using System.Configuration;
using Kapruka.Service;

namespace KASI_Extend
{
    public partial class importIFTI : System.Web.UI.Page
    {
        List<InboundIFTI> listOfInboundTrans = new List<InboundIFTI>();
        public string canCall { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BTN_Browse.Style.Add("display", "none");
                TR_FileUploadManagement.Style.Add("display", "none");
                GetAllAgents();
                if (Session["insertedFileData"] != null)
                {
                    Session["listOfInboundTransFailure"] = null;
                    Session["listOfInboundTransSuccess"] = null;
                    Session["agentName"] = null;
                    Session["insertedFileData"] = null;
                }
                if (Request.QueryString["u"] != null)
                {
                    if (Request.QueryString["u"] == "1")
                    {
                        if (Session["canCall"] != null)
                        {
                            canCall = Session["canCall"].ToString();
                            Session["canCall"] = null;
                            var url = Request.Url.AbsolutePath;
                            // Response.Redirect(url);
                            // Page.Server.Transfer(url);
                        }

                        SetFailureGrid();
                        SetSuccessGrid();


                    }
                }

            }

            if (IsPostBack)
            {
                if (Request.QueryString["q"] != null)
                    UploadFile(sender, e);

            }
            getTodayTransactions();
            getStats();

            if (Session["insertedFileData"] != null)
            {
                Session["listOfInboundTransFailure"] = null;
                Session["listOfInboundTransSuccess"] = null;
                Session["agentName"] = null;
                Session["insertedFileData"] = null;
            }
        }

        protected void UploadFile(object sender, EventArgs e)
        {


            var filePath = Server.MapPath("~/UploadINboudFile/");
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            System.IO.DirectoryInfo di = new DirectoryInfo(filePath);

            Session["listOfInboundTransFailure"] = null;
            Session["listOfInboundTransSuccess"] = null;
            Session["agentName"] = null;
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            HttpFileCollection fileCollection = Request.Files;

            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile upload = fileCollection[i];



                ViewState["cancall"] = true;

                if (!string.IsNullOrEmpty(upload.FileName))
                {
                    string fileName = Path.GetFileName(upload.FileName);

                    upload.SaveAs(filePath + fileName);
                    ReadFile(filePath + fileName);



                }


            }

            Response.AddHeader("test", "test");
        }




        protected void Button1_Click(object sender, EventArgs e)
        {
            var fileName = FileUpload1.PostedFile.FileName;
            if (fileName == "")
            {
                lbl_ErrorMessage.Text = "The selected file is incorrect. Please select correct file for upload. Excel TAB must be IFTI-DRA IN";
                canCall = "The selected file is incorrect. Please select correct file for upload. Excel TAB must be IFTI-DRA IN";
                //  Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenErrorMsg", @"<script type='text/javascript'>errorDialog();</script>");
                resetButton.Visible = true;
                return;
            }
            //var temFile = @"c:\temp\";
            //temFile = temFile + fileName;

            var filePath = Server.MapPath("~/UploadINboudFile/");
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            System.IO.DirectoryInfo di = new DirectoryInfo(filePath);

            Session["listOfInboundTransFailure"] = null;
            Session["listOfInboundTransSuccess"] = null;
            Session["agentName"] = null;
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }


            FileUpload1.SaveAs(filePath + fileName);
            ReadFile(filePath + fileName);

        }

        private void GetAllAgents()
        {
            IncomingAgentService ser = new IncomingAgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            //agentDrop.Items.Add(new ListItem("-- SELECT -- ", "0"));
            var agList = ser.GetAll().ToList();

            agentDrop.DataSource = agList;
            agentDrop.DataTextField = "AgentName";
            agentDrop.DataValueField = "AgentID";
            agentDrop.DataBind();
            agentDrop.Items.Insert(0, new ListItem("-- SELECT --"));
        }

        private string getConnectionString(string filePath)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            props["Provider"] = "Microsoft.ACE.OLEDB.12.0";
            props["Extended Properties"] = "Excel 12.0";
            props["Data Source"] = filePath;

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }

        private bool ValidateColumnStart(string column)
        {
            var flag = false;

            if(column.Length> 0)
            {
                if (column.Contains('-'))
                     flag=true;
            }
            return flag;

        }

        private void ReadFile(string filePath)
        {
            Session["canCall"] = null;
            if (selectedDropAgent.Value == "0")
            {
                canCall = "Please select agent before uploading file.";
                Session["canCall"] = canCall;
                return;
            }
            else
            {
                DataSet ds = new DataSet();

                Session["listOfInboundTransFailure"] = null;
                Session["listOfInboundTransSuccess"] = null;
                Session["agentName"] = null;

                string connectionString = getConnectionString(filePath);

                using (OleDbConnection conn = new OleDbConnection(connectionString))
                {
                    try
                    {
                        conn.Open();
                    }
                    catch(Exception ex)
                    {
                        TR_ErrorMessage.Style.Add("display", "block");

                        lbl_ErrorMessage.Text = "The selected file is incorrect. Please select correct file for upload. Excel TAB must be IFTI-DRA IN";
                        // Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenErrorMsg", @"<script type='text/javascript'>errorDialog();</script>");
                        //canCall = "The selected file is incorrect. Please select correct file for upload. Excel TAB must be IFTI-DRA IN";
                        // Session["canCall"] = canCall;
                        resetButton.Visible = true;
                        return;
                    }
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = conn;

                    DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    DataTable dtExcelSchema;
                    dtExcelSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                    // var be1 = "IFTI-DRA OUT$";
                    // var na = "Beneficiaries$";
                    //  var cus = "Customers$";
                    //  var tra = "Transactions$";

                    String[] excelSheets = new String[dtExcelSchema.Rows.Count];
                    int j = 0;
                    foreach (DataRow row in dtExcelSchema.Rows)
                    {
                        excelSheets[j] = row["TABLE_NAME"].ToString();
                        j++;
                    }

                    var listSheets = new List<string>();

                    // listSheets.Add(be1);
                    // listSheets.Add(na);
                    // listSheets.Add(cus);
                    //  listSheets.Add(tra);
                    DataTable dt = new DataTable();



                    cmd.CommandText = "SELECT * FROM [IFTI-DRA IN$]";



                    var targetSheet = "'IFTI-DRA IN$'";
                    var result = Array.Exists(excelSheets, s => s.Equals(targetSheet));

                    if (!result)
                    {
                        TR_ErrorMessage.Style.Add("display", "block");

                        lbl_ErrorMessage.Text = "The selected file is incorrect. Please select correct file for upload. Excel TAB must be IFTI-DRA IN";
                        // Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenErrorMsg", @"<script type='text/javascript'>errorDialog();</script>");
                        //canCall = "The selected file is incorrect. Please select correct file for upload. Excel TAB must be IFTI-DRA IN";
                        // Session["canCall"] = canCall;
                        resetButton.Visible = true;
                        return;
                    }
                    else
                    {
                        TR_ErrorMessage.Style.Add("display", "none");
                        dt.TableName = "'IFTI-DRA IN$'";
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(dt);


                        //   dt.Rows.RemoveAt(0);
                        //  dt.Rows.RemoveAt(1);
                        //  dt.Rows.RemoveAt(2);
                        //  dt.Rows.RemoveAt(3);
                        //  dt.Rows.RemoveAt(4);
                        // dt.Rows.RemoveAt(5);
                        // dt.Rows.RemoveAt(6);
                        //var listItems = (from DataRow dr in dt.Rows
                        //                 select new InboundIFTI()
                        //                 {
                        //                     CurrencyCode = dr[2].ToString(),
                        //                     TotalAmount = float.Parse(dr[3].ToString()),
                        //                 }).ToList();

                        var i = 1;
                        double price;
                        foreach (DataRow dr in dt.Rows)
                        {
                            i = i + 1;
                            if (dr[2].ToString().Length==3)
                            {

                                if (Double.TryParse(dr[3].ToString(), out price))
                                {

                                    var tranactionComplete = DateTime.Parse(dr[1].ToString());
                                    var dateChe = "1/01/2000";
                                    if (tranactionComplete.Date > Convert.ToDateTime(dateChe))
                                    {

                                        var obj = new InboundIFTI();
                                        obj.RowNumber = i;
                                        obj.uploadDateTime = DateTime.Now;
                                       
                                        if (!string.IsNullOrEmpty(dr[2].ToString()))
                                            obj.CurrencyCode = dr[2].ToString();
                                        if (!string.IsNullOrEmpty(dr[3].ToString()))
                                            obj.TotalAmount = float.Parse(dr[3].ToString());

                                        obj.TypeOfTransfer = dr[4].ToString();
                                        obj.DescOfProperty = dr[5].ToString();
                                        obj.TransRefNumber = dr[6].ToString();
                                        obj.FullName = dr[7].ToString();
                                        obj.AKA = dr[8].ToString();

                                        obj.Address = dr[10].ToString();
                                        obj.City = dr[11].ToString();
                                        obj.State = dr[12].ToString();
                                        obj.Postcode = dr[13].ToString();
                                        obj.Country = dr[14].ToString();
                                        obj.PostalAddress = dr[15].ToString();
                                        obj.PACity = dr[16].ToString();
                                        obj.PAState = dr[17].ToString();
                                        obj.PAPostcode = dr[18].ToString();
                                        obj.PACountry = dr[19].ToString();
                                        obj.Phone = dr[20].ToString();
                                        obj.Email = dr[21].ToString();
                                        obj.Occupation = dr[22].ToString();
                                        obj.ABN = dr[23].ToString();
                                        obj.CustomerNumber = dr[24].ToString();
                                        obj.AccountNumber = dr[25].ToString();
                                        obj.BusinessStructure = dr[26].ToString();

                                        obj.BeneFullName = dr[27].ToString();
                                        obj.BeneBusinessName = dr[29].ToString();

                                        obj.BeneAddress = dr[30].ToString();
                                        obj.BeneCity = dr[31].ToString();
                                        obj.BeneState = dr[32].ToString();
                                        obj.BenePostcode = dr[33].ToString();
                                        obj.BeneCountry = dr[34].ToString();
                                        obj.BenePostalAddress = dr[35].ToString();
                                        obj.BenePACity = dr[36].ToString();
                                        obj.BenePAState = dr[37].ToString();
                                        obj.BenePAPostcode = dr[38].ToString();
                                        obj.BenePACountry = dr[39].ToString();

                                        obj.BenePhone = dr[40].ToString();
                                        obj.BeneEmail = dr[41].ToString();

                                        obj.BeneOccupation = dr[42].ToString();
                                        obj.BeneABN = dr[43].ToString();
                                        obj.BeneBusinessStructure = dr[44].ToString();
                                        obj.BeneAccountNumber = System.Text.RegularExpressions.Regex.Replace(dr[45].ToString(), @"[^a-zA-Z 0-9'.@]", string.Empty).Trim(); 
                                        obj.BeneNameOfInstitution = dr[46].ToString();
                                        obj.BeneAccountCity = dr[47].ToString();
                                        obj.BeneAccountCountry = dr[48].ToString();
                                        obj.RetailFullName = dr[49].ToString();

                                        obj.RetailAKA = dr[50].ToString();
                                        obj.RetailAddress = dr[52].ToString();
                                        obj.RetailCity = dr[53].ToString();
                                        obj.RetailState = dr[54].ToString();
                                        obj.RetailPostcode = dr[55].ToString();
                                        obj.RetailCountry = dr[56].ToString();
                                        obj.RetailPostalAddress = dr[57].ToString();
                                        obj.RetailPACity = dr[58].ToString();
                                        obj.RetailPAState = dr[59].ToString();
                                        obj.RetailPAPostcode = dr[60].ToString();
                                        obj.RetailPACountry = dr[61].ToString();

                                        obj.RetailPhone = dr[62].ToString();
                                        obj.RetailEmail = dr[63].ToString();

                                        obj.RetailOccupation = dr[64].ToString();
                                        obj.RetailBusinessStructure = dr[65].ToString();

                                        obj.RetailAcceptingMoney = dr[66].ToString();
                                        obj.RetailSendingInstructions = dr[67].ToString();


                                        obj.MAOFullName = dr[68].ToString();
                                        obj.MAOAddress = dr[69].ToString();
                                        obj.MAOCity = dr[70].ToString();
                                        obj.MAOState = dr[71].ToString();
                                        obj.MAOPostcode = dr[72].ToString();
                                        obj.MAOCountry = dr[73].ToString();

                                        obj.OSIFullName = dr[74].ToString();
                                        obj.OSIOtherName = dr[75].ToString();

                                        if (verifyItIsDateTime(dr[76].ToString()))
                                        {
                                            obj.OSIDOB = DateTime.Parse(dr[76].ToString());
                                        }



                                        obj.OSIAddress = dr[77].ToString();
                                        obj.OSICity = dr[78].ToString();
                                        obj.OSIState = dr[79].ToString();
                                        obj.OSIPostcode = dr[80].ToString();
                                        obj.OSICountry = dr[81].ToString();

                                        obj.OSIPostalAddress = dr[82].ToString();
                                        obj.OSIPACity = dr[83].ToString();
                                        obj.OSIPAState = dr[84].ToString();
                                        obj.OSIPAPostcode = dr[85].ToString();
                                        obj.OSIPACountry = dr[86].ToString();

                                        obj.OSIPhone = dr[87].ToString();
                                        obj.OSIEmail = dr[88].ToString();


                                        obj.OSIOccupation = dr[89].ToString();
                                        obj.OSIABN = dr[90].ToString();
                                        obj.OSIBusinessStructure = dr[91].ToString();

                                        obj.ORIFullName = dr[92].ToString();
                                        obj.ORIAddress = dr[93].ToString();
                                        obj.ORICity = dr[94].ToString();
                                        obj.ORIState = dr[95].ToString();
                                        obj.ORIPostcode = dr[96].ToString();


                                        obj.ORIDistributing = dr[97].ToString();

                                        obj.ORISeparateRetail = dr[98].ToString();

                                        obj.ODMFullName = dr[99].ToString();
                                        obj.ODMAddress = dr[100].ToString();
                                        obj.ODMCity = dr[101].ToString();
                                        obj.ODMState = dr[102].ToString();
                                        obj.ODMPostcode = dr[103].ToString();


                                        obj.RMDFullName = dr[105].ToString();
                                        obj.RMDAddress = dr[106].ToString();
                                        obj.RMDCity = dr[017].ToString();
                                        obj.RMDState = dr[108].ToString();
                                        obj.RMDPostcode = dr[109].ToString();

                                        obj.Reason = dr[110].ToString();


                                        obj.PCRFullName = dr[111].ToString();
                                        obj.PCRJobTitle = dr[112].ToString();
                                        obj.PCRPhone = dr[113].ToString();
                                        obj.PCREmail = dr[114].ToString();

                                        obj.ErrorList = "Ready to import";
                                        obj.ImportStatus = "0";

                                        if (verifyItIsDateTime(dr[51].ToString()))
                                        {
                                            obj.RetailDOB = DateTime.Parse(dr[51].ToString());
                                        }

                                        if (verifyItIsDateTime(dr[28].ToString()))
                                        {
                                            obj.BeneDOB = DateTime.Parse(dr[28].ToString());
                                        }

                                        if (verifyItIsDateTime(dr[0].ToString()))
                                        {
                                            obj.MoneyRecFromCustDateTime = DateTime.Parse(dr[0].ToString());
                                        }
                                        else
                                        {
                                            obj.ErrorList = "Error: Money Received Date  " + dr[0].ToString() + " Is Invalid";
                                            obj.ImportStatus = "-1";
                                        }


                                        if (verifyItIsDateTime(dr[1].ToString()))
                                        {
                                            obj.MoneyAvaToBeneDateTime = DateTime.Parse(dr[1].ToString());
                                        }
                                        else
                                        {
                                            obj.ErrorList = "Error: Money Made Available Date " + dr[1].ToString() + " Is Invalid";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[2].ToString()))
                                        {
                                            obj.ErrorList = "Error: Currency Code Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[3].ToString()))
                                        {
                                            obj.ErrorList = "Error: Total Amount/Value Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[4].ToString()))
                                        {
                                            obj.ErrorList = "ErrorL Type Of Transfer Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }


                                        if (string.IsNullOrEmpty(dr[6].ToString()))
                                        {
                                            obj.ErrorList = "Error: Transaction Reference Number Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        if (string.IsNullOrEmpty(dr[7].ToString()))
                                        {
                                            obj.ErrorList = "Error: Ordering Customer Full Name Is Invalid";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (verifyItIsDateTime(dr[9].ToString()))
                                        {
                                            obj.DOB = DateTime.Parse(dr[9].ToString());
                                        }


                                        if (string.IsNullOrEmpty(dr[10].ToString()))
                                        {
                                            obj.ErrorList = "Error: Ordering Customer Street Number and Address Are Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        if (string.IsNullOrEmpty(dr[11].ToString()))
                                        {
                                            obj.ErrorList = "Error: Ordering Customer City Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }


                                        if (string.IsNullOrEmpty(dr[14].ToString()))
                                        {
                                            obj.ErrorList = "Error: Ordering Customer Country Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }


                                        if (string.IsNullOrEmpty(dr[27].ToString()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary Customer Full Name Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[30].ToString()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary Street Number and Name Are Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[31].ToString()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary City Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[34].ToString()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary Country Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }


                                        if (string.IsNullOrEmpty(System.Text.RegularExpressions.Regex.Replace(dr[45].ToString(), @"[^a-zA-Z 0-9'.@]", string.Empty).Trim()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary Bank Account Number Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        else
                                        {
                                            if (System.Text.RegularExpressions.Regex.Replace(dr[45].ToString(), @"[^a-zA-Z 0-9'.@]", string.Empty).Trim().Length < 8)
                                            {
                                                obj.ErrorList = "Error: Beneficiary Bank Account Number " + System.Text.RegularExpressions.Regex.Replace(dr[45].ToString(), @"[^a-zA-Z 0-9'.@]", string.Empty).Trim() + " Is Invalid. Must include BSB & Account Number";
                                                obj.ImportStatus = "-1";
                                            }
                                        }



                                        if (string.IsNullOrEmpty(dr[46].ToString()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary Bank Name Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        else
                                        {
                                            if (dr[46].ToString().Length > 3)
                                            {
                                                obj.ErrorList = "Error: Bank must be standard 3 letter abbreviation";
                                                obj.ImportStatus = "-1";
                                            }
                                        }

                                        if (string.IsNullOrEmpty(dr[47].ToString()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary Bank City Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        if (string.IsNullOrEmpty(dr[48].ToString()))
                                        {
                                            obj.ErrorList = "Error: Beneficiary Bank Country Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[49].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Accepting Transfer Instruction's Full name Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[52].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Accepting Transfer Instruction's Business/Residential Address Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[53].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Accepting Transfer Instruction's City/Town/Suburb Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[55].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Accepting Transfer Instruction's Postcode Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[56].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Accepting Transfer Instruction's Country Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }




                                        if (string.IsNullOrEmpty(dr[62].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Accepting Transfer Instruction's Phone Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        if (string.IsNullOrEmpty(dr[63].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Accepting Transfer Instruction's Email Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }


                                        if (string.IsNullOrEmpty(dr[92].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Receiving Transfer Instruction's Full name Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        if (string.IsNullOrEmpty(dr[93].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Receiving Transfer Instruction's Business/Residential Address Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[94].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person/Organisation Receiving Transfer Instruction's City/Town/Suburb Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }


                                        if (string.IsNullOrEmpty(dr[110].ToString()))
                                        {
                                            obj.ErrorList = "Error: Reason For The Transfer Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }
                                        if (string.IsNullOrEmpty(dr[111].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person Completing This Report's Full Name Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[112].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person Completing This Report's Job title Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[113].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person Completing This Report's Phone Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (string.IsNullOrEmpty(dr[114].ToString()))
                                        {
                                            obj.ErrorList = "Error: Person Completing This Report's Email Is Mandatory";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (CheckTransactionsHaveAltTransId(obj.TransRefNumber))
                                        {
                                            obj.ErrorList = "Error: Duplicate Transaction - Transaction has already been imported. ";
                                            obj.ImportStatus = "-1";
                                        }

                                        if (listOfInboundTrans.Any(x => x.TransRefNumber == obj.TransRefNumber))
                                        {
                                            obj.ErrorList = "Error: Duplicate Transaction - Transaction has already in the file. ";
                                            obj.ImportStatus = "-1";
                                        }

                                        listOfInboundTrans.Add(obj);
                                    }
                                }
                                
                            }
                           
                        }

                        SetGridViewData(listOfInboundTrans);
                    }

                    // MyContextKaasi contex = new MyContextKaasi();
                    //  var first = listOfInboundTrans[5];
                    //   contex.InboundIFTIs.Add(first);
                    //   contex.SaveChanges();
                    // Session["listOfInboundTrans"] = listOfInboundTrans;
                    // GridView1.DataSource = listOfInboundTrans;
                    // GridView1.DataBind();


                }
            }




        }

        private void ManageCustomersTrans()
        {
            int totcounter = 0;
            int pendcounter = 0;
            int succounter = 0;

            if (Session["InboundIFTIs"] != null)
            {
                var listINbound = Session["InboundIFTIs"] as List<InboundIFTI>;
                InCustomerService ser = new InCustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                var cusList = ser.GetAll();
                var agentId = Convert.ToInt32(selectedDropAgent.Value);
                var customerId = 0;
                foreach (var item in listINbound)
                {
                    var fullName = item.FullName.Trim();
                    var country = item.Country.Trim();
                    var postcode = item.Postcode.Trim();

                    var cust = (from cs in cusList
                                where cs.FullName == fullName &&
                                    cs.Postcode == postcode && cs.Country == country
                                select cs).SingleOrDefault();
                    var transactionStatus = "PENDING";
                    var BeneAddress = item.BeneAddress;
                    if (cust != null)
                    {
                        customerId = cust.CustomerID.GetValueOrDefault();


                    }
                    else
                    {
                        customerId = GetlastCustomerId();
                        var customerKaasi = new InCustomer();

                        customerKaasi.Active = "Y";
                        customerKaasi.PreviousCustomerID = 0;
                        customerKaasi.CustomerID = customerId;
                        customerKaasi.AddressLine1 = item.Address;
                        customerKaasi.AddressLine2 = "";
                        customerKaasi.StreetName = "";
                        customerKaasi.StreetNo = "";
                        customerKaasi.UnitNo = "";
                        customerKaasi.StreetType = "";
                        customerKaasi.Suburb = item.City;
                        customerKaasi.State = item.State;
                        customerKaasi.Postcode = item.Postcode;
                        customerKaasi.EmailAddress = item.Email;
                        customerKaasi.TelHome = item.Phone;
                        customerKaasi.Mobile = item.Phone;
                        if (item.DOB != null)
                            customerKaasi.DOB = item.DOB.ToString();
                        customerKaasi.Country = item.Country;
                        customerKaasi.CreatedBy = "Import Tool";
                        customerKaasi.CreatedDateTime = DateTime.Now;
                        customerKaasi.AgentID = agentId;
                        customerKaasi.FirstName = item.FullName;
                        customerKaasi.LastName = item.AKA;
                        customerKaasi.FullName = item.FullName + item.AKA;
                        customerKaasi.Nationality = "";
                        customerKaasi.Gender = "";
                        customerKaasi.IsIndividual = true;
                        InsertCustomer(customerKaasi);

                    }
                    CreateBeneficiary(item);
                    var amount = Convert.ToDecimal(item.TotalAmount);
                    var service = new InboundIFTIService();
                    var beneAccount = service.GetInboundBene(item.BeneAccountNumber);

                    if (beneAccount != null)
                    {

                        if (RunKycCheckStatus(beneAccount.ID))
                        {
                            SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
                            var LimitValue = SetServ.GetAll(x => x.SettingName == "InwardDollarLimit", null, "").SingleOrDefault().SettingsVariable;

                            if (amount > decimal.Parse(LimitValue))
                            {
                                succounter++;
                                transactionStatus = "PENDING";
                            }
                            else
                            {
                                succounter++;
                                transactionStatus = "SUCCESSFUL";
                            }

                            
                        }


                        else
                        {
                            pendcounter++;
                        }

                    }
                    totcounter++;
                    FillTransaction(agentId, customerId, amount, item.RetailCountry,
                        item.Reason, item.CurrencyCode, transactionStatus, item.RowNumber,
                        item.TransRefNumber, item.BeneFullName, item.BeneNameOfInstitution, item.BeneAccountNumber,
                        beneAccount.ID, item.MoneyRecFromCustDateTime);



                }

                Kapruka.Enterprise.NotificationService NotServ = new Kapruka.Enterprise.NotificationService(new UnitOfWorks(new KaprukaEntities()));

                Random rnd = new Random();
                int GlobalID = rnd.Next(999999999);

                Notification newNotif = new Notification();
                newNotif.CreatedDateTime = DateTime.Now;
                newNotif.GlobalNotificiationID = GlobalID;

                ResetMessageGridSuccessForImport();

                Session["InboundIFTIs"] = null;

            }

        }

        protected class TransactionINBound
        {
            public int AgentID { get; set; }
            public int CustomerID { get; set; }
            public string CreatedBy { get; set; }
            public string Status { get; set; }
            public string Purpose { get; set; }
            public string CurrencyCode { get; set; }
            public DateTime CreatedDateTime { get; set; }
            public Decimal DollarAmount { get; set; }
            public int RowNumber { get; set; }
            public String AltTransID { get; set; }
            public String BeneficiaryName { get; set; }
            public String BankName { get; set; }
            public String AccountNumber { get; set; }
            public int BeneficiaryId { get; set; }
            public DateTime TransCreatedDateTime { get; set; }
        }

        private void FillTransaction(int ageId, int custoemrID, decimal dollarAmount,
            string country, string reason, string currencyCode, string statusTran,
            int RowNumber, String TransRef, String BeneName,
            String BankName, String AccountNumber, int beneficiaryId, DateTime TransCreatedDateTime)
        {
            var transac = new TransactionINBound();

            transac.AgentID = ageId;

            transac.CustomerID = custoemrID;
            transac.RowNumber = RowNumber;
            transac.CreatedBy = Session["LoggedPersonName"].ToString() + " - " + Session["LoggedUserFullName"].ToString();
            transac.CurrencyCode = currencyCode;
            // transac.CustomerLastName=item.c
            transac.DollarAmount = Convert.ToDecimal(dollarAmount);
            transac.AltTransID = TransRef;
            transac.Status = statusTran;
            transac.Purpose = reason;
            transac.BeneficiaryName = BeneName;
            transac.BankName = BankName;
            //NEED TO GET BENEFICIARY ID
            transac.CreatedDateTime = DateTime.Now;
            transac.AccountNumber = AccountNumber;
            transac.BeneficiaryId = beneficiaryId;
            transac.TransCreatedDateTime = TransCreatedDateTime;
            //InsertTransaction(transac);
            CreateInboundTransaction(transac);
        }

        private void CanCreateCustomerAndTransaction(string transact)
        {

        }

        private bool RunKycCheckStatus(int inboundBeneID)
        {
            var canUpdate = false;
            var settings = GetSettingsMonthForKCYRun();
            var accouResBeneDetai = GetKycCheckStatus(inboundBeneID);
            if (accouResBeneDetai != null)
            {
                if (accouResBeneDetai.Status == "0")
                {
                    var currentDate = DateTime.Now.Date;
                    if (accouResBeneDetai.KyCheckDate != null)
                    {
                        var runMonth = 0;
                        runMonth = Convert.ToInt32(settings);
                        var kycRunDate = Convert.ToDateTime(accouResBeneDetai.KyCheckDate).AddMonths(runMonth);
                        if (kycRunDate < currentDate)
                        {
                            return GetUpdateKycStatus(inboundBeneID, ref canUpdate, ref accouResBeneDetai);

                        }
                        else
                        {
                            canUpdate = true;
                            return canUpdate;
                        }
                    }
                    canUpdate = true;
                    return canUpdate;
                }
                else
                {
                    return GetUpdateKycStatus(inboundBeneID, ref canUpdate, ref accouResBeneDetai);

                }
            }

            else
            {

                return GetUpdateKycStatus(inboundBeneID, ref canUpdate, ref accouResBeneDetai);

            }
            
        }

        private bool GetUpdateKycStatus(int inboundBeneID, ref bool canUpdate, ref KycResultInward accouResBeneDetai)
        {
            var accouBeneDetai = new InboundIFTIService().GetInboundBeneById(inboundBeneID); ;

            CheckKycCustomer(accouBeneDetai.BeneFullName, accouBeneDetai.BeneFullName,
                accouBeneDetai.BeneFullName, null, accouBeneDetai.ID, null, null, null, null,
                null, null, accouBeneDetai.ID);
            accouResBeneDetai = GetKycCheckStatus(inboundBeneID);
            if (accouResBeneDetai.Status == "0")
            {

                InBeneAuditLogService IBALServ = new InBeneAuditLogService(new UnitOfWorks(new KaprukaEntities()));

                InBeneAuditLog newITAL = new InBeneAuditLog();
                newITAL.ActionType = "EDIT";
                newITAL.AgentName = Session["LoggedCompany"].ToString();
                newITAL.Username = Session["LoggedPersonName"].ToString();
                newITAL.BeneID = inboundBeneID;
                newITAL.AuditDateTime = DateTime.Now;
                newITAL.Description = " has passed KYC successfully";

                IBALServ.Add(newITAL);

                canUpdate = true;
                return canUpdate;
            }
            else
            {
                InBeneAuditLogService IBALServ = new InBeneAuditLogService(new UnitOfWorks(new KaprukaEntities()));

                InBeneAuditLog newITAL = new InBeneAuditLog();
                newITAL.ActionType = "EDIT";
                newITAL.AgentName = Session["LoggedCompany"].ToString();
                newITAL.Username = Session["LoggedPersonName"].ToString();
                newITAL.BeneID = inboundBeneID;
                newITAL.AuditDateTime = DateTime.Now;
                newITAL.Description = " has failed KYC";

                IBALServ.Add(newITAL);

                return canUpdate;
            }
        }
        private static KycResultInward GetKycCheckStatus(int inboundBeneID)
        {
            var obResult = new KycResultInward();
            var status = "-2";

            var inBoundObj = new InboundIFTIService().GetInboundBeneById(inboundBeneID);
            if (inBoundObj != null)
            {
                if (inBoundObj.resultCode != null && inBoundObj.resultCode == "0")
                    status = "0";
                else if (inBoundObj.resultCode != null && inBoundObj.resultCode == "-1")
                    status = "-1";

                obResult.Status = status;

                if (inBoundObj.KYCBy != null)
                    obResult.KycBy = inBoundObj.KYCBy;
                if (inBoundObj.KYCCheckDate != null)
                    obResult.KyCheckDate = inBoundObj.KYCCheckDate;
                if (inBoundObj.transactionId != null)
                    obResult.TransactionId = inBoundObj.transactionId;
                if (inBoundObj.resultDescription != null)
                    obResult.ResultDescription = inBoundObj.resultDescription;
            }

            return obResult;
        }
        private void CheckKycCustomer(string title, string firstName, string lastName,
          string dob, int customerId, string customerCity, string customerCountry,
           string customerState, string postcode, string email, string phone, int customerRecoId)
        {
            var cusKyc = new CustomerKyced();
            cusKyc.profile = "FW2";
            if (title != null)
                cusKyc.title = title;
            else
                cusKyc.title = null;
            if (firstName != null)
                cusKyc.bfn = firstName;
            else
                cusKyc.bfn = null;

            if (lastName != null)
                cusKyc.bln = lastName;
            else
                cusKyc.bln = null;
            if (dob != null)
            {

                cusKyc.dob = ConvertDateFormat(dob);
            }
            else
                cusKyc.dob = null;
            if (customerId > 0)
                cusKyc.man = customerId.ToString();
            else
                cusKyc.man = null;
            if (customerCity != null)
                cusKyc.bc = customerCity;
            else
                cusKyc.bc = null;
            if (customerCountry != null)
            {
                if (customerCountry.Length >= 2)
                    cusKyc.bco = customerCountry.Substring(0, 2).ToUpper();
                else
                    cusKyc.bco = "AU";
            }
            else
                cusKyc.bco = "AU";

            if (customerState != null)
                cusKyc.bs = customerState;
            else
                cusKyc.bs = null;

            cusKyc.bsn = null;
            if (postcode != null)
                cusKyc.bz = postcode;
            else
                cusKyc.bz = null;
            if (phone != null)
                cusKyc.pw = phone;
            else
                cusKyc.pw = null;
            if (email != null)
                cusKyc.tea = email;
            else
                cusKyc.tea = "";
            cusKyc.faceImage = null;

            cusKyc.documentImageFront = null;
            //cusKyc.documentImangeBack = "";
            cusKyc.securityNumber = null;
            cusKyc.ip = null;

            var runBy = "";
            var cService = new Kapruka.Service.CustomerHandlerService();
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            var resultKYCCall = cService.CreateKycedIncoimmgCustomer(cusKyc, customerId, customerRecoId, runBy);
            //if (resultKYCCall != null)
            //{
            //    if (resultKYCCall. == "0")
            //    {

            //    }
            //}


        }

        private void CreateInboundTransaction(TransactionINBound obj)
        {


            var sqlStatem = @"INSERT INTO InboundTransactions (AgentID,CustomerID,Status,CreatedBy,
Purpose,CurrencyCode,DollarAmount,CreatedDateTime, RefID, AlTransID, BeneficiaryName, 
BankName, InsertedIntoABAFile, InsertedIntoIFTIFile, BSB, AccountNumber,InboundBeneficiaryID, ImportedDateTime) VALUES (@AgentID,@CustomerID,@Status,@CreatedBy,@Purpose,@CurrencyCode,@DollarAmount,@CreatedDateTime, @RefID, @AlTransID, @BeneficiaryName, @BankName, 0, 0, @BSB, @AccountNumber,@InboundBeneficiaryID, CURRENT_TIMESTAMP)";
            var AccountSeparatedBSB = "";
            var AccountSeparatedAccount = "";
            if (!string.IsNullOrEmpty(obj.AccountNumber))
            {
                var isBSBSeparated = obj.AccountNumber.Split(' ');
                if (isBSBSeparated.Count() > 1)
                {
                    AccountSeparatedBSB = obj.AccountNumber.Split(' ')[0];
                    AccountSeparatedAccount = obj.AccountNumber.Split(' ')[1];
                }
                else
                {
                    AccountSeparatedBSB = obj.AccountNumber.Substring(0, 6);
                    AccountSeparatedAccount = obj.AccountNumber.Substring(7, (obj.AccountNumber.Length - 7));
                }



            }


            SqlParameter[] parameter = {new SqlParameter("@AgentID",obj.AgentID),
                                        new SqlParameter("@CustomerID",obj.CustomerID),
                                         new SqlParameter("@Status",obj.Status),
                                          new SqlParameter("@CreatedBy",obj.CreatedBy),
                                           new SqlParameter("@Purpose",obj.Purpose),
                                           new SqlParameter("@CurrencyCode",obj.CurrencyCode),
                                           new SqlParameter("@DollarAmount",obj.DollarAmount),
                                           new SqlParameter("@RefID", obj.RowNumber),
                                           new SqlParameter("@AlTransID", obj.AltTransID) ,
                                           new SqlParameter("@BeneficiaryName", obj.BeneficiaryName),
                                           new SqlParameter("@BankName", obj.BankName),
                                           new SqlParameter("@BSB", AccountSeparatedBSB),
                                           new SqlParameter("@AccountNumber", AccountSeparatedAccount),
                                           new SqlParameter("@InboundBeneficiaryID", obj.BeneficiaryId),
                                           new SqlParameter("@CreatedDateTime", obj.TransCreatedDateTime),
                                       };

            ExecuteNonQuery(sqlStatem, parameter);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String outval = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT MAX(ID) AS MAXTRANS FROM dbo.InboundTransactions";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        outval = sdr["MAXTRANS"].ToString();
                    }
                }
                conn.Close();
            }

            InboundBeneficiaryService IIBServ = new InboundBeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            var IIBDetails = IIBServ.GetAll(x => x.ID == obj.BeneficiaryId, null, "").SingleOrDefault();

            var KYCResult = IIBDetails.resultCode;

            InTransAuditLog ITAL = new InTransAuditLog();
            ITAL.AgentName = Session["LoggedCompany"].ToString();
            ITAL.Username = Session["LoggedPersonName"].ToString();
            ITAL.AuditDateTime = DateTime.Now;

            if (obj.Status == "SUCCESSFUL")
            {
                ITAL.Description = "Transaction has been successfully imported. Limit compliant. KYC compliant. Setting transaction to SUCCESSFUL";
            }
            else
            {
                if (KYCResult == "0")
                {
                    ITAL.Description = "Transaction has been successfully imported. Escalating to compliance due to exceeded limits. Setting transaction to PENDING";
                }
                else
                {
                    ITAL.Description = "Transaction has been successfully imported. KYC failed. Setting transaction to PENDING";
                }
                

            }
            ITAL.TransactionID = Int32.Parse(outval);
            ITAL.ActionType = "ADD";

            InTransAuditLogServicecs ITALServ = new InTransAuditLogServicecs(new UnitOfWorks(new KaprukaEntities()));
            ITALServ.Add(ITAL);
        }


        private void CreateBeneficiary(InboundIFTI obj)
        {
            var service = new InboundIFTIService();
            var beneAccount = service.GetInboundBene(obj.BeneAccountNumber);

            if (beneAccount == null)
            {

                var objBene = new InboundIFTIBeneficiaryData();
                objBene.BeneAccountNumber = obj.BeneAccountNumber;
                objBene.BeneFullName = obj.BeneFullName;
                objBene.Address = obj.Address;
                objBene.City = obj.City;
                objBene.State = obj.State;
                objBene.Postcode = obj.Postcode;
                objBene.Country = obj.Country;

                var sqlStatem = @"INSERT INTO InboundIFTIBeneficiary (BeneAccountNumber,BeneFullName, Address, City, State, Postcode, Country)
                             VALUES (@BeneAccountNumber,@BeneFullName, @Address, @City, @State, @Postcode,@Country)";


                SqlParameter[] parameter = {new SqlParameter("@BeneAccountNumber",objBene.BeneAccountNumber),
                                        new SqlParameter("@BeneFullName",objBene.BeneFullName),
                                        new SqlParameter("@Address", objBene.Address),
                                        new SqlParameter("@City", objBene.City),
                                        new SqlParameter("@State", objBene.State),
                                        new SqlParameter("@Postcode", objBene.Postcode),
                                        new SqlParameter("@Country", objBene.Country),
                                       };

                ExecuteNonQuery(sqlStatem, parameter);
            }
            else
            {
                if (beneAccount != null)
                {
                    if (beneAccount.BeneAccountNumber == null)
                    {
                        var objBene = new InboundIFTIBeneficiaryData();
                        objBene.BeneAccountNumber = obj.BeneAccountNumber;
                        objBene.BeneFullName = obj.BeneFullName;
                        objBene.Address = obj.BeneAddress;
                        objBene.City = obj.BeneCity;
                        objBene.State = obj.BeneState;
                        objBene.Postcode = obj.BenePostcode;
                        objBene.Country = obj.BeneCountry;

                        var sqlStatem = @"INSERT INTO InboundIFTIBeneficiary (BeneAccountNumber,BeneFullName, Address, City, State, Postcode, Country)
                             VALUES (@BeneAccountNumber,@BeneFullName, @Address, @City, @State, @Postcode,@Country)";


                        SqlParameter[] parameter = {new SqlParameter("@BeneAccountNumber",objBene.BeneAccountNumber),
                                        new SqlParameter("@BeneFullName",objBene.BeneFullName),
                                        new SqlParameter("@Address", objBene.Address),
                                        new SqlParameter("@City", objBene.City),
                                        new SqlParameter("@State", objBene.State),
                                        new SqlParameter("@Postcode", objBene.Postcode),
                                        new SqlParameter("@Country", objBene.Country),

                                       };

                        ExecuteNonQuery(sqlStatem, parameter);
                    }
                }
            }


        }

        private string ConvertDateFormat(string date)
        {
            var dateS = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
            return dateS;
        }





        private bool CheckTransactionsHaveAltTransId(string altTransactionId)
        {
            var respo = new KaprukaEntities();
            var AgentID = Int32.Parse(selectedDropAgent.Value);

            var customTransac = (from trs in respo.InboundTransactions where trs.AlTransID == altTransactionId && trs.AgentID == AgentID select trs).ToList();

            if (customTransac.Count() > 0)
                return true;
            return false;
        }

        public static bool ExecuteNonQuery(string QueryString, params SqlParameter[] arrParam)
        {
            string qry = "";
            SqlConnection Con = new SqlConnection();


            Con.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString; ;
            Con.Open();

            if (arrParam != null)
            {

                qry = QueryString;
                SqlCommand cmd = new SqlCommand(qry, Con);
                cmd.Parameters.AddRange(arrParam);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                Con.Close();

                return true;


            }


            return false;

        }

        protected void InsertCustomer(InCustomer cs)
        {
            var respo = new KaprukaEntities();
            respo.InCustomers.Add(cs);
            respo.SaveChanges();

        }
        private string GetSettingsMonthForKCYRun()
        {
            var settigns = "";
            var respo = new KaprukaEntities();
            var valSett = (from sett in respo.Settings
                           where sett.SettingName == "InwardNbMonthsKYCExpire"
                           select sett).ToList();

            if (valSett.Count() > 0)
            {
                settigns = valSett[0].SettingsVariable;
            }

            return settigns;

        }



        protected int GetlastCustomerId()
        {
            var csId = 0;
            var respo = new KaprukaEntities();
            var csIdMax = (from cs in respo.InCustomers select cs.CustomerID).Max().GetValueOrDefault();

            csId = csIdMax + 1;
            return csId;
        }


        private void InsertTransaction(Transaction trsn)
        {
            var respo = new KaprukaEntities();
            respo.Transactions.Add(trsn);
            respo.SaveChanges();
        }



        protected void InsertRecords(object sender, EventArgs e)
        {
            if (Session["listOfInboundTransSuccess"] != null)
            {
                var sessionLit = Session["listOfInboundTransSuccess"] as List<InboundIFTI>;
                MyContextKaasiModelQ contex = new MyContextKaasiModelQ();
                contex.InboundIFTIs.AddRange(sessionLit);
                contex.SaveChanges();
                gridDivSuccess.Style.Add("display", "block");
                successlabel.Text = "Successfully inserted";
                failureLabel.Text = "";
                Session["InboundIFTIs"] = Session["listOfInboundTransSuccess"];
                Session["listOfInboundTransSuccess"] = null;
                ManageCustomersTrans();
                lastInsert.Visible = false;
                Session["insertedFileData"] = "true";
                refreshButton.Visible = true;
                resetButton.Visible = false;
               
            }

        }

        private void SetGridViewData(List<InboundIFTI> listObj)
        {
            var failureTran = (from tr in listObj where tr.ImportStatus == "-1" select tr).ToList();
            Session["listOfInboundTransFailure"] = failureTran;
            Session["agentName"] = selectedDropAgent.Value;
            var successTran = (from tr in listObj where tr.ImportStatus == "0" select tr).ToList();
            Session["listOfInboundTransSuccess"] = successTran;

            successlabel.Text = "Successful Records Count :" + successTran.Count();
            failureLabel.Text = "Failure Records Count :" + failureTran.Count();

            // successbutton.Visible = true;
            if (successTran.Count() > 0)
                gridDivSuccess.Style.Add("display", "block");

            if (failureTran.Count() > 0)
            {
                gridDivUnSuccess.Visible = true;

                //gridDivSuccess.Style.Add("display", "block");
            }
            else
                if (failureTran.Count() == 0)
            {
                lastInsert.Visible = true;

            }

            SetSuccessGrid();
            SetFailureGrid();
        }



        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;

            gridDivSuccess.Style.Add("display", "block");
            SetSuccessGrid();
        }

        private void SetSuccessGrid()
        {
            resetButton.Visible = true;
            if (Session["agentName"] != null)
            {
                agentDrop.SelectedValue = Session["agentName"].ToString();
                selectedDropAgent.Value = Session["agentName"].ToString();
            }

            if (Session["listOfInboundTransSuccess"] != null)
            {
                var sessionLit = Session["listOfInboundTransSuccess"] as List<InboundIFTI>;
                GridView1.DataSource = sessionLit;
                GridView1.DataBind();

                successlabel.Text = "Successful Records Count :" + sessionLit.Count();
                //  successbutton.Visible = true;
                if (sessionLit.Count() > 0)
                    gridDivSuccess.Style.Add("display", "block");
            }


            if (Session["listOfInboundTransFailure"] != null)
            {
                var failureTran = Session["listOfInboundTransFailure"] as List<InboundIFTI>;
                failureLabel.Text = "Failure Records Count :" + failureTran.Count();
                if (failureTran.Count() > 0)
                {

                    gridDivUnSuccess.Style.Add("display", "block");
                }
                else
                    if (failureTran.Count() == 0)
                {
                    lastInsert.Visible = true;

                }
            }
        }

        private void ResetMessageGridSuccessForImport()
        {
            if (Session["InboundIFTIs"] != null)
            {
                var sessionLit = Session["InboundIFTIs"] as List<InboundIFTI>;
                foreach (var item in sessionLit)
                {
                    item.ErrorList = "Successfully Imported";
                }
                GridView1.DataSource = sessionLit;
                GridView1.DataBind();
            }
        }
        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView2.PageIndex = e.NewPageIndex;
            gridDivUnSuccess.Style.Add("display", "block");


            SetFailureGrid();

        }

        private void SetFailureGrid()
        {

            if (Session["listOfInboundTransFailure"] != null)
            {
                var sessionLit = Session["listOfInboundTransFailure"] as List<InboundIFTI>;
                GridView2.DataSource = sessionLit;
                GridView2.DataBind();
            }
        }

        private Boolean verifyItIsDateTime(String theDate)
        {
            DateTime dateTimeConfirmed;
            return DateTime.TryParse(theDate, out dateTimeConfirmed);
        }

        protected void btn_DownloadIFTI_Click(object sender, EventArgs e)
        {
            var file = Server.MapPath(@"Templates\IFTI-DRA IN.xls");
            var filename = "IFTI-DRA IN.xls";
            Response.AddHeader("Content-disposition", "attachment; filename=" + filename);
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(file);

            Response.End();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            DIV_ImportFile.Attributes.Clear();
            DIV_ImportFile.Attributes.Add("style", "display:block");

            //Button11.Attributes.Add("style", "display:block");
        }

        private void getTodayTransactions()
        {
            KaprukaEntities context = new KaprukaEntities();

            DateTime today = DateTime.Now.Date;
            var result = (from c in context.InboundTransactions
                          where c.CreatedDateTime == today
                          select c);

            var TotCount = result.Count();

            TotalTansSPN.InnerText = TotCount.ToString();

            var result2 = (from c in context.InboundTransactions
                           where c.Status == "PENDING"
                           select c);

            var PendCount = result2.Count();

            PendTransSPN.InnerText = PendCount.ToString();

            DateTime date = new DateTime();
            date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddSeconds(-1);

            var result3 = (from c in context.InboundTransactions
                           where c.CreatedDateTime >= firstDayOfMonth && c.CreatedDateTime <= lastDayOfMonth
                           select c);

            var MthCount = result3.Count();

            ThisMonthTransSPN.InnerText = MthCount.ToString();

        }

        private void getStats()
        {
            InwardTransactionService ITSServ = new InwardTransactionService(new UnitOfWorks(new KaprukaEntities()));
            var ITSDetails = ITSServ.GetAll(x => x.Status == "PENDING", null, "").ToList();

            InboundBeneficiaryService IBServ = new InboundBeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            var IBDetails = IBServ.GetAll(x => x.resultDescription.Contains("FAILED"), null, "").ToList();

            var ITSCount = ITSDetails.Count();
            var IBCount = IBDetails.Count();

            SPN_PendingKYC.InnerText = ITSCount.ToString();
            SPN_FailedKYC.InnerText = IBCount.ToString();
        }

        protected void BTN_Uploadfile_Click(object sender, EventArgs e)
        {
            var filename = BTN_FileUpload.PostedFile.FileName;
            //if (filename != "")
            //{
            //    canCall = "The selected file is incorrect. Please select correct file for upload. Excel TAB must be IFTI-DRA IN";
            //}
            Session["listOfInboundTransFailure"] = null;
            Session["listOfInboundTransSuccess"] = null;
            Session["agentName"] = null;

            var fileExt = Path.GetExtension(filename);
            if (fileExt == ".xls" || fileExt == ".xlsx")
            {
                TR_ErrorMessage.Style.Add("display", "none");
                lbl_ErrorMessage.Text = " ";

                var filePath = Server.MapPath("~/UploadINboudFile/");
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);
                System.IO.DirectoryInfo di = new DirectoryInfo(filePath);

                Session["listOfInboundTransFailure"] = null;
                Session["listOfInboundTransSuccess"] = null;
                Session["agentName"] = null;
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                filePath = filePath + filename;
                BTN_FileUpload.SaveAs(filePath);
                ReadFile(filePath);
            }
            else
            {
                TR_ErrorMessage.Style.Add("display", "block");
                lbl_ErrorMessage.Text = "Incorrect File Type. Please select an XLS or XLSX file.";
                resetButton.Visible = true;
            }


        }

        protected void refreshPage(object sender, EventArgs e)
        {
            Session["listOfInboundTransFailure"] = null;
            Session["listOfInboundTransSuccess"] = null;
            Session["agentName"] = null;
            Session["InboundIFTIs"] = null;
            refreshButton.Visible = false;
            Response.Redirect("~/Inward/importIFTI.aspx");
        }
    }
}