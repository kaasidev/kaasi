﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Enterprise;
using Kapruka.Repository;
using Kapruka.PublicModels;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;

namespace KASI_Extend_
{
    public partial class BankingW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            String BOCLKR = TotalLKR("7010");

            if (BOCLKR != "")
            {
                BOCLKRTotal.Text = "Rs " + String.Format("{0:N2}", Math.Round(float.Parse(TotalLKR("7010")), 2));
                String BOCAUD = TotalAUD("7010");
                BOCAUDTotal.Text = String.Format("{0:C2}", float.Parse(BOCAUD));
            }
            else
            {
                BOCLKRTotal.Text = "Rs 0.00";
                BOCAUDTotal.Text = "$0.00";
            }

            BOCTransTotal.Text = TotalTrans("7010");
            BOCLastFileUp.Text = LastFileGeneration("BOC");

            String COMLKR = TotalLKR("7056");

            if (COMLKR != "")
            {
                COMLKRTotal.Text = "Rs " + String.Format("{0:N2}", Math.Round(float.Parse(TotalLKR("7056")), 2));
                String COMAUD = TotalAUD("7056");
                COMAUDTotal.Text = String.Format("{0:C2}", float.Parse(COMAUD));
            }
            else
            {
                COMLKRTotal.Text = "Rs 0.00";
                COMAUDTotal.Text = "$0.00";
            }

            COMTransTotal.Text = TotalTrans("7056");
            COMLastFileUp.Text = LastFileGeneration("COM");

            String HNBLKR = TotalLKR("7083");
            if (HNBLKR != "")
            {
                HNBLKRTotal.Text = "Rs " + String.Format("{0:N2}", Math.Round(float.Parse(TotalLKR("7083")), 2));
                String HNBAUD = TotalAUD("7083");
                HNBAUDTotal.Text = String.Format("{0:C2}", float.Parse(HNBAUD));
            }
            else
            {
                HNBLKRTotal.Text = "Rs 0.00";
                HNBAUDTotal.Text = "$0.00";
            }


            HNBTransTotal.Text = TotalTrans("7083");
            HNBLastFileUp.Text = LastFileGeneration("HNB");

            String NSBLKR = TotalLKR("7719");

            if (NSBLKR != "")
            {
                NSBLKRTotal.Text = "Rs " + String.Format("{0:N2}", Math.Round(float.Parse(TotalLKR("7719")), 2));
                String NSBAUD = TotalAUD("7719");
                NSBAUDTotal.Text = String.Format("{0:C2}", float.Parse(NSBAUD));
            }
            else
            {
                NSBLKRTotal.Text = "Rs 0.00";
                NSBAUDTotal.Text = "$0.00";
            }

            NSBTransTotal.Text = TotalTrans("7719");
            NSBLastFileUp.Text = LastFileGeneration("NSB");

            String PKBLKR = TotalLKR("7135");

            if (PKBLKR != "")
            {
                PBLKRTotal.Text = "Rs " + String.Format("{0:N2}", Math.Round(float.Parse(TotalLKR("7135")), 2));
                String PBAUD = TotalAUDNotINBank();
                PBAUDTotal.Text = String.Format("{0:C2}", float.Parse(PBAUD));
            }
            else
            {
                PBLKRTotal.Text = "Rs 0.00";
                PBAUDTotal.Text = "$0.00";
            }

            PBTransTotal.Text = TotalTransNotInBank();
            PBLastFileUp.Text = LastFileGeneration("PB");

            String SPLKR = TotalLKR("7278");

            if (SPLKR != "")
            {
                SPLKRTotal.Text = "Rs " + String.Format("{0:N2}", Math.Round(float.Parse(TotalLKR("7278")), 2));
                String SPAUD = TotalAUD("7278");
                SPAUDTotal.Text = String.Format("{0:C2}", float.Parse(SPAUD));
            }
            else
            {
                SPLKRTotal.Text = "Rs 0.00";
                SPAUDTotal.Text = "$0.00";
            }

            SPTransTotal.Text = TotalTrans("7278");
            SPLastFileUp.Text = LastFileGeneration("SP");

            String SBLKR = TotalLKR("7287");

            if (SBLKR != "")
            {
                SBLKRTotal.Text = "Rs " + String.Format("{0:N2}", Math.Round(float.Parse(TotalLKR("7287")), 2));
                String SBAUD = TotalAUD("7287");
                SBAUDTotal.Text = String.Format("{0:C2}", float.Parse(SBAUD));
            }
            else
            {
                SBLKRTotal.Text = "Rs 0.00";
                SBAUDTotal.Text = "$0.00";
            }

            SBTransTotal.Text = TotalTrans("7287");
            SBLastFileUp.Text = LastFileGeneration("SB");
          
        }

        private String LastFileGeneration(String BankName)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT TOP 1 UploadDateTime FROM dbo.FileUploadAudit WHERE BankName = '" + BankName + "' AND UploadDateTime = GETDATE()";
            String FileUploadDateTime = String.Empty;
            String returnString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            FileUploadDateTime = sdr["UploadDateTime"].ToString();
                        }
                    }
                    else
                    {
                        FileUploadDateTime = "No files created today";
                    }

                }
            }
            conn.Close();

            return FileUploadDateTime;
        }

        private String TotalLKR(String BankID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT SUM(RemittedAmount) AS Total FROM dbo.Transactions WHERE RoutingBank = " + BankID + " AND Status = 'AWAITING TRANSFER'";
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Total = sdr["Total"].ToString();
                    }
                }
            }
            conn.Close();

            return Total;
        }

        private String TotalLKRNotINBank()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT SUM(RemittedAmount) AS Total FROM dbo.Transactions WHERE AccountID IN (SELECT AccountID FROM dbo.BeneficiaryPaymentMethods WHERE BankID NOT IN (7010, 7056, 7083, 7719, 7278, 7287)) AND Status = 'AWAITING TRANSFER'";
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Total = sdr["Total"].ToString();
                    }
                }
            }
            conn.Close();

            return Total;
        }

        private String TotalAUD(String BankID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT SUM(DollarAmount) AS Total FROM dbo.Transactions WHERE RoutingBank = " + BankID + " AND Status = 'AWAITING TRANSFER'";
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Total = sdr["Total"].ToString();
                    }
                }
            }
            conn.Close();

            return Total;
        }

        private String TotalAUDNotINBank()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT SUM(DollarAmount) AS Total FROM dbo.Transactions WHERE AccountID IN (SELECT AccountID FROM dbo.BeneficiaryPaymentMethods WHERE BankID NOT IN (7010, 7056, 7083, 7719, 7278, 7287, 'AMCE')) AND Status = 'AWAITING TRANSFER'";
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["Total"].Equals(DBNull.Value))
                        {
                            Total = "0";
                        }
                        else
                        {
                            Total = sdr["Total"].ToString();
                        }

                    }
                }
            }
            conn.Close();

            return Total;
        }

        private String TotalTrans(String BankID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT COUNT(TransactionID) AS Total FROM dbo.Transactions WHERE RoutingBank = " + BankID + " AND Status = 'AWAITING TRANSFER'";
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Total = sdr["Total"].ToString();
                    }
                }
            }
            conn.Close();

            return Total;
        }

        private String TotalTransNotInBank()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT COUNT(TransactionID) AS Total FROM dbo.Transactions WHERE AccountID IN (SELECT AccountID FROM dbo.BeneficiaryPaymentMethods WHERE BankID NOT IN ('7010', '7056', '7083', '7719', '7278', '7287')) AND Status = 'AWAITING TRANSFER'";
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Total = sdr["Total"].ToString();
                    }
                }
            }
            conn.Close();

            return Total;
        }

        private void btn_CreateBank01File_Click(object sender, EventArgs e)
        {
            String filepath = String.Empty;
            String delimiter = ",";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT * FROM dbo.Settings WHERE SettingName = 'BankFilesSavingLocation'";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            filepath = sdr["SettingsVariable"].ToString();
                        }
                    }
                    else
                    {
                        filepath = "No file path exists";
                    }

                }
            }
            conn.Close();


        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            if (Session["AgentID"] != null)
            {
                int agentId = int.Parse(Session["AgentID"].ToString());
                TransactionService transactionService = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                var transactionList = transactionService.GetAll(x => x.InsertedIntoAustracFile == "N" && x.AgentID == agentId, null, "").ToList();
                //  var smrList = transactionService.GetAll(x => x.isSuspicious == "N" && x.InsertedIntoAustracFile=="N", null, "").ToList();
                // var ttrList = transactionService.GetAll(x => x.isThreshold == "N" && x.InsertedIntoAustracFile == "N", null, "").ToList();
                string fileName = "IFTI-DRA" + DateTime.Now.ToString("yyyyMMdd") + "01";
                // IftieList iftModel = new IftieList();
                SmrList smrModel = new SmrList();
                TtrfbsList ttrModel = new TtrfbsList();
                IFTIModel.IftidraList iftModel = new IFTIModel.IftidraList();
                iftModel.VersionMajor = "1";
                iftModel.VersionMinor = "1";
                //iftModel= GenerateIFTReport(transactionList, fileName);
                iftModel = GenerateIFTReportML(transactionList, fileName, agentId);
                // smrModel = GenerateSMRReport(smrList, fileName);
                //  ttrModel = GenerateTTRReport(ttrList,fileName);

                //IftieList hd = new PublicModels.IftieList();

                //hd.FileName = "te";
                //hd.Ns1 = "dsds";
                //hd.Ns2 = "ds s ";
                ////lst.Add(hd);
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                string filePath = Server.MapPath("/Files") + "\\" + fileName + ".xml";
                using (MemoryStream ms = new MemoryStream())
                {
                    using (var writer = XmlWriter.Create(ms, settings))
                    {
                        var serializer = new XmlSerializer(typeof(IFTIModel.IftidraList));
                        serializer.Serialize(writer, iftModel);
                    }

                    ms.Position = 0;
                    XDocument doc = XDocument.Load(new XmlTextReader(ms));
                    doc.Root.Add(new XAttribute("versionMinor", "1"));
                    doc.Root.Add(new XAttribute("versionMajor", "1"));
                    doc.Save(filePath);

                    DownloadTheFile(filePath, fileName);



                    //ms.Close();
                    //Response.Clear();
                    //Response.ContentType = "application/force-download";
                    //Response.AddHeader("content-disposition", "attachment;    filename=" + fileName + ".xml");
                    //byte[] bytesInStream = ms.ToArray(); // simpler way of converting to array
                    //Response.BinaryWrite(bytesInStream);
                    //Response.End();
                }
                //  SerializeObject(iftModel, filePath, "\\xml" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xml");
                UpdateTable(fileName + ".xml", agentId);
                loadMask.Style["display"] = "none";
                loadImg.Style["display"] = "none";
                string redirect = "<script>window.open('/FileDownloader.aspx?file=" + fileName + "','_blank');</script>";
                Response.Write(redirect);
            }
            else
            {
                Response.Redirect("/Login.aspx");
            }

        }
        public void UpdateTable(string fileName, int agentId)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string sql = "update Transactions set InsertedIntoAustracFileName='" + fileName + "',InsertedIntoAustracFileDateTime=getdate(),InsertedIntoAustracFile='Y' where InsertedIntoAustracFile='N' and AgentID=" + agentId;

            using (SqlCommand cmd = new SqlCommand(sql))
            {
                cmd.Connection = conn;
                conn.Open();

                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        public void DownloadTheFile(string url, string fileName)
        {
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                var fileReq = System.Net.HttpWebRequest.Create(url);

                //Create a response for this request
                var fileResp = fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xml\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
        }
        private Kapruka.PublicModels.IFTIModel.IftidraList GenerateIFTReportML(List<Transaction> transactionList, string fileName, int agent_Id)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            CustomerDocumentService custDocSer = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            beneficiaryPaymentMethodService payMethodSer = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            CountryService countrySer = new CountryService(new UnitOfWorks(new KaprukaEntities()));
            AgentAssignedCountryService agencyAssSer = new AgentAssignedCountryService(new UnitOfWorks(new KaprukaEntities()));
            Kapruka.PublicModels.IFTIModel.IftidraList model = new Kapruka.PublicModels.IFTIModel.IftidraList();

            model.FileName = fileName + ".xml";
            model.xmlns = null;

            model.renumber = GetAgentRegNumber(agent_Id);//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 

            model.Iftidra = new List<IFTIModel.Iftidra>();
            if (transactionList != null)
            {
                model.ReportCount = transactionList.Count();
            }
            else
            {
                model.ReportCount = 0;
            }

            // int kl = 0;
            foreach (Transaction item in transactionList)
            {
                //   if(kl==0)
                //   {
                Customer customerList = new Customer();
                Beneficiary beneficiaryList = new Beneficiary();
                CustomerDocument custDoc = new CustomerDocument();
                BeneficiaryPaymentMethod payMenthod = new BeneficiaryPaymentMethod();
                Kapruka.Repository.Agent agent = new Kapruka.Repository.Agent();

                customerList = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                beneficiaryList = beneService.GetAll(x => x.BeneficiaryID == item.BeneficiaryID, null, "").SingleOrDefault();
                var custDocList = custDocSer.GetAll(x => x.CustomerID == customerList.CustomerID, null, "").ToList();
                if (custDocList.Count == 0)
                {
                    custDoc = null;
                }
                else
                {
                    custDoc = custDocList[0];
                }

                string benId = beneficiaryList.BeneficiaryID.Value.ToString();
                var payMenthodList = payMethodSer.GetAll(x => x.BeneficiaryID == benId, null, "").ToList();
                if (payMenthodList.Count == 0)
                {
                    payMenthod = null;
                }
                else
                {
                    payMenthod = payMenthodList[0];
                }
                int agentId = customerList.AgentID.Value;
                agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();


                IFTIModel.Iftidra dra = new IFTIModel.Iftidra();
                dra.Id = "R" + item.TransactionID.ToString();
                dra.Header = new IFTIModel.Header();
                dra.Header.Id = "R" + item.TransactionID.ToString() + "-0";
                dra.Header.InterceptFlag = "N";
                dra.Header.TxnRefNo = item.TransactionID.ToString();

                dra.Transaction = new IFTIModel.Transaction();
                dra.Transaction.Id = "R" + item.TransactionID.ToString() + "-1";
                dra.Transaction.TxnDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");
                dra.Transaction.CurrencyAmount = new IFTIModel.CurrencyAmount();
                dra.Transaction.CurrencyAmount.Id = "R" + item.TransactionID.ToString() + "-11";
                dra.Transaction.CurrencyAmount.Amount = item.DollarAmount.Value.ToString("N2");
                dra.Transaction.CurrencyAmount.Currency = "AUD";

                dra.Transaction.direction = "O";
                dra.Transaction.TfrType = new IFTIModel.TfrType();
                dra.Transaction.TfrType.Type = "M";
                dra.Transaction.TfrType.Id = "R" + item.TransactionID.ToString() + "-12";
                dra.Transaction.ValueDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");

                dra.Transferor = new IFTIModel.Transferor();//customer
                dra.Transferor.Id = "R" + item.TransactionID.ToString() + "-2";
                dra.Transferor.FullName = customerList.FullName;
                dra.Transferor.MainAddress = new IFTIModel.MainAddress();
                dra.Transferor.MainAddress.Addr = customerList.AddressLine1 + " " + customerList.AddressLine2;
                dra.Transferor.MainAddress.Country = customerList.CountryOfBirth;
                dra.Transferor.MainAddress.Id = "R" + item.TransactionID.ToString() + "-21";
                dra.Transferor.MainAddress.Postcode = customerList.Postcode;
                dra.Transferor.MainAddress.State = customerList.State;
                dra.Transferor.MainAddress.Suburb = customerList.Suburb;
                dra.Transferor.MainAddress.Country = customerList.Country;

                dra.Transferor.Phone = customerList.Mobile;
                dra.Transferor.Email = customerList.EmailAddress;
                dra.Transferor.CustNo = customerList.CustomerID.Value.ToString();
                dra.Transferor.Dob = ChangeDOB(customerList.DOB.ToString());
                dra.Transferor.Identification = new IFTIModel.Identification();
                dra.Transferor.Identification.Id = "R" + item.TransactionID.ToString() + "-22";
                if (custDoc != null)
                {
                    String TypeOfDoc = String.Empty;
                    if (custDoc.TypeOfDocument == "Driver's Licence")
                    {
                        TypeOfDoc = "D";
                    }
                    else if (custDoc.TypeOfDocument == "Passport")
                    {
                        TypeOfDoc = "P";
                    }
                    dra.Transferor.Identification.Issuer = custDoc.IssuingAuthority;
                    dra.Transferor.Identification.Number = custDoc.DocumentNumber;
                    dra.Transferor.Identification.Type = TypeOfDoc;
                }
                else
                {
                    dra.Transferor.Identification.Issuer = "NO INFO";
                    dra.Transferor.Identification.Number = "NO INFO";
                    dra.Transferor.Identification.Type = "D";
                }

                dra.OrderingInstn = new IFTIModel.OrderingInstn();//Agent details
                dra.OrderingInstn.Id = "R" + item.TransactionID.ToString() + "-3";
                dra.OrderingInstn.Branch = new IFTIModel.Branch();
                dra.OrderingInstn.Branch.Id = "R" + item.TransactionID.ToString() + "-31";
                dra.OrderingInstn.Branch.MainAddress = new IFTIModel.MainAddress();
                dra.OrderingInstn.Branch.MainAddress.Id = "R" + item.TransactionID.ToString() + "-32";
                dra.OrderingInstn.Branch.MainAddress.Addr = agent.AgentAddress1;
                AgentAssignedCountry assC = new AgentAssignedCountry();
                assC = agencyAssSer.GetAll(x => x.AgentID == agent.AgentID, null, "").FirstOrDefault();
                Country agentCountry = countrySer.GetAll(x => x.CountryID == assC.CountryID.Value, null, "").SingleOrDefault();
                dra.OrderingInstn.Branch.MainAddress.Country = agentCountry.CountryName;
                dra.OrderingInstn.Branch.MainAddress.Suburb = agent.AgentCity;
                dra.OrderingInstn.Branch.MainAddress.State = agent.AgentState;
                dra.OrderingInstn.Branch.MainAddress.Postcode = agent.AgentPostcode;
                dra.OrderingInstn.Branch.FullName = agent.AgentName;

                dra.OrderingInstn.ForeignBased = "N";//no idea

                dra.InitiatingInstn = new IFTIModel.InitiatingInstn();//no idea
                dra.InitiatingInstn.Id = "R" + item.TransactionID.ToString() + "-4";
                dra.InitiatingInstn.SameAsOrderingInstn = "Y";

                dra.SendingInstn = new IFTIModel.SendingInstn();//no idea
                dra.SendingInstn.Id = "R" + item.TransactionID.ToString() + "-5";
                //dra.SendingInstn.SameAsOrderingInstn = "Y";

                dra.ReceivingInstn = new IFTIModel.ReceivingInstn();//no idea
                dra.ReceivingInstn.Id = "R" + item.TransactionID.ToString() + "-6";
                dra.ReceivingInstn.FullName = "PB";
                dra.ReceivingInstn.MainAddress = new IFTIModel.MainAddress();
                dra.ReceivingInstn.MainAddress.Id = "R" + item.TransactionID.ToString() + "-61";
                dra.ReceivingInstn.MainAddress.Addr = "No.75, Sir Chittampalam A. Gardiner Mawatha";
                dra.ReceivingInstn.MainAddress.Suburb = "Colombo-02";
                dra.ReceivingInstn.MainAddress.State = "Western";
                dra.ReceivingInstn.MainAddress.Country = "Sri Lanka";

                dra.BeneficiaryInstn = new IFTIModel.BeneficiaryInstn();
                dra.BeneficiaryInstn.Id = "R" + item.TransactionID.ToString() + "-7";
                dra.BeneficiaryInstn.SameAsReceivingInstn = "Y";

                dra.Transferee = new IFTIModel.Transferee();//beneficiary
                dra.Transferee.Id = "R" + item.TransactionID.ToString() + "-9";
                dra.Transferee.FullName = beneficiaryList.BeneficiaryName;
                dra.Transferee.MainAddress = new IFTIModel.MainAddress();
                dra.Transferee.MainAddress.Addr = beneficiaryList.AddressLine1 + " " + beneficiaryList.AddressLine2;
                dra.Transferee.MainAddress.Country = beneficiaryList.CountryOfBirth;
                dra.Transferee.MainAddress.Id = "R" + beneficiaryList.CustomerID.Value.ToString();
                dra.Transferee.MainAddress.Postcode = beneficiaryList.Postcode;
                dra.Transferee.MainAddress.State = beneficiaryList.State;
                dra.Transferee.MainAddress.Suburb = beneficiaryList.Suburb;
                dra.Transferee.MainAddress.Country = beneficiaryList.Country;

                dra.Transferee.Phone = beneficiaryList.Mobile;
                dra.Transferee.Account = new IFTIModel.Account();
                dra.Transferee.Account.Id = "R" + item.TransactionID.ToString() + "-92";
                if (payMenthod != null)
                {
                    dra.Transferee.Account.AcctNumber = payMenthod.AccountNumber;
                    if (string.IsNullOrEmpty(payMenthod.AccountName))
                    {
                        dra.Transferee.Account.Name = "";
                    }
                    else
                    {
                        dra.Transferee.Account.Name = payMenthod.AccountName;
                    }
                    dra.Transferee.Account.City = payMenthod.BranchName;
                    dra.Transferee.Account.Country = "SRI LANKA"; // Need to change here to get the country name
                }
                Country benCountry = countrySer.GetAll(x => x.CountryID == beneficiaryList.CountryID, null, "").SingleOrDefault();
                if (benCountry != null)
                {
                    dra.Transferee.Account.Country = benCountry.CountryName;
                }
                else
                {
                    dra.Transferee.Account.Country = "SRI LANKA";
                }


                model.Iftidra.Add(dra);
                //  }

                //  kl = kl + 1;
            }

            return model;
        }

        private string ChangeDOB(string dob)
        {
            if (!string.IsNullOrEmpty(dob))
            {
                string[] arr = dob.Split('/');
                int val1 = int.Parse(arr[1]);
                int val2 = int.Parse(arr[0]);

                string val1st = val1.ToString();
                if(val1<10)
                {
                    val1st = "0" + val1;
                }
                string val2st = val2.ToString();
                if (val2 < 10)
                {
                    val2st = "0" + val2;
                }

                return arr[2] + "-" + val1st + "-" + val2st;
            }
            else
            {
                return "";
            }
        }

        private string GetAgentRegNumber(int agentId)
        {
            AgentService ser = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            var agent = ser.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            return agent.AustracReNumber;
        }

        private TtrfbsList GenerateTTRReport(List<Transaction> ttrList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentService = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));

            TtrfbsList model = new TtrfbsList();
            model.Ttrfbs = new List<Ttrfbs>();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;

            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Ttrfbs = new List<Ttrfbs>();
            model.ReportCount = ttrList.Count().ToString("N0");

            foreach (Transaction item in ttrList)
            {
                Kapruka.Repository.Customer customer = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                Kapruka.Repository.Agent agent = agentService.GetAll(x => x.AgentID == customer.AgentID, null, "").SingleOrDefault();

                Kapruka.Repository.BeneficiaryPaymentMethod paymntMethod = paymentMethodService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                Ttrfbs ttr = new Ttrfbs();
                ttr.Header = new HeaderTTR();
                ttr.Header.InterceptFlag = "";
                ttr.Header.ReportingBranch = "";
                ttr.Header.TxnRefNo = "";

                ttr.Agent = new List<Kapruka.PublicModels.Agent>();
                ttr.Agent[0].Abn = agent.AgentABN;
                ttr.Agent[0].FullName = agent.AgentName;
                ttr.Agent[0].MainAddress = agent.AgentAddress1;
                ttr.Agent[0].PostalAddress = agent.AgentAddress1;
                ttr.Agent[0].Phone = agent.AgentPhone;
                ttr.Agent[0].IndOcc = "";
                ttr.Agent[0].Acn = agent.AgentACN;
                ttr.Agent[0].Arbn = "";
                ttr.Agent[0].BusinessStruct = "";
                ttr.Agent[0].Dob = "";
                ttr.Agent[0].Account = new AccountTTR();
                ttr.Agent[0].Account.Type = "";
                ttr.Agent[0].Identification = "";
                ttr.Agent[0].ElectDataSrc = "";
                ttr.Agent[0].PartyIsRecipient = "";

                ttr.Customer[0].FullName = customer.FullName;

                ttr.Customer[0].MainAddress = customer.AddressLine1;
                ttr.Customer[0].PostalAddress = customer.AddressLine1;
                ttr.Customer[0].Phone = customer.Mobile;
                ttr.Customer[0].IndOcc = "";
                ttr.Customer[0].Abn = "";
                ttr.Customer[0].Acn = "";
                ttr.Customer[0].Arbn = "";
                ttr.Customer[0].BusinessStruct = "";
                ttr.Customer[0].Dob = customer.DOB;
                ttr.Customer[0].Account.Type = paymntMethod.AccountType;
                ttr.Customer[0].Identification = "";
                ttr.Customer[0].ElectDataSrc = "";
                ttr.Customer[0].PartyIsRecipient = "";

                ttr.Recipient[0].Account = new AccountTTR();
                ttr.Recipient[0].Account.Type = "";
                ttr.Recipient[0].Dob = "";
                ttr.Recipient[0].FullName = "";
                ttr.Recipient[0].MainAddress = "";

                // need to set transaction details.not clear


            }

            return null;
        }

        private SmrList GenerateSMRReport(List<Transaction> smrList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));


            SmrList model = new SmrList();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;

            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Smr = new List<Smr>();
            model.ReportCount = smrList.Count().ToString("N0");

            foreach (var item in smrList)
            {
                Smr smr = new Smr();
                smr.Header = new HeaderSMR();
                smr.Header.InterceptFlag = "";
                smr.Header.ReportingBranch = "";
                smr.Header.ReReportRef = "";

                smr.SmDetails = new SmDetails();
                smr.SmDetails.DesignatedSvc = "";
                smr.SmDetails.DesignatedSvcEnquiry = "";
                smr.SmDetails.DesignatedSvcProvided = "";
                smr.SmDetails.DesignatedSvcRequested = "";
                smr.SmDetails.GrandTotal = "";
                smr.SmDetails.SuspReason = "";
                smr.SmDetails.SuspReasonOther = "";

                smr.SuspGrounds.GroundsForSuspicion = "";
                smr.SuspPerson = new List<SuspPerson>();//need to check whether customer or beneficiery



            }

            return model;

        }

        #region GenerateIFTReport
        private IftieList GenerateIFTReport(List<Transaction> transactionList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));


            IftieList model = new IftieList();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;
            model.Ns2 = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2";
            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Structured = new List<Structured>();
            model.ReportCount = transactionList.Count().ToString("N0");
            foreach (Transaction item in transactionList)
            {
                List<Customer> customerList = new List<Customer>();
                List<Beneficiary> beneficiaryList = new List<Beneficiary>();
                customerList = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").ToList();
                beneficiaryList = beneService.GetAll(x => x.BeneficiaryID == item.BeneficiaryID, null, "").ToList();

                Structured structure = new Structured();
                structure.Header = new HeaderIFT();
                structure.Transaction = new TransactionIFT();

                //Header section
                structure.Header.Id = "";
                structure.Header.InterceptFlag = "";
                structure.Header.TxnRefNo = "";

                //Transaction section
                structure.Transaction.TransferDate = item.CreatedDateTime.Value.ToString("dd MMM yyyy");
                structure.Transaction.Direction = "";//I or O in to Aus ot=r out to AUS
                structure.Transaction.CurrencyAmount = new CurrencyAmount();
                structure.Transaction.CurrencyAmount.Id = item.TransactionID.ToString();
                structure.Transaction.CurrencyAmount.Amount = item.RemittedAmount.Value.ToString("N2");//Not sure
                structure.Transaction.CurrencyAmount.Currency = item.RemittedCurrency;//not sure
                structure.Transaction.ValueDate = "";

                structure.Payer = new List<PayerIFT>();
                structure.Payee = new List<PayeeIFT>();
                //payer details
                foreach (Customer customer in customerList)
                {
                    PayerIFT payer = new PayerIFT();
                    payer.MainAddress = new MainAddress();
                    //Main Address section
                    payer.FullName = customer.FullName;
                    payer.MainAddress.Addr = customer.AddressLine1 + " " + customer.AddressLine2;
                    payer.MainAddress.Suburb = customer.Suburb;
                    payer.MainAddress.State = customer.State;
                    payer.MainAddress.Postcode = customer.Postcode;
                    payer.MainAddress.Country = customer.CountryOfBirth;

                    payer.Account = new List<AccountIFT>();
                    payer.Account = GenerateAccountDetailsForPayerIFT(item);
                    payer.Abn = "";
                    payer.Acn = "";
                    payer.Arbn = "";
                    payer.Identification = GenerateIdentificationForPayerIFT(item);
                    payer.CustNo = customer.CustomerID.Value.ToString();
                    payer.IndividualDetails = new IndividualDetailsIFT();
                    payer.IndividualDetails.Dob = customer.DOB;
                    payer.IndividualDetails.Id = customer.CustomerID.Value.ToString();
                    payer.IndividualDetails.PlaceOfBirth = new PlaceOfBirth();
                    payer.IndividualDetails.PlaceOfBirth.Country = customer.CountryOfBirth;
                    payer.IndividualDetails.PlaceOfBirth.Town = customer.PlaceOfBirth;

                    structure.Payer.Add(payer);
                }
                //payee details
                foreach (Beneficiary beneficiary in beneficiaryList)
                {
                    PayeeIFT payee = new PayeeIFT();
                    payee.MainAddress = new MainAddress();
                    //Main Address section
                    payee.FullName = beneficiary.BeneficiaryName;
                    payee.MainAddress.Addr = beneficiary.AddressLine1 + " " + beneficiary.AddressLine2;
                    payee.MainAddress.Suburb = beneficiary.Suburb;
                    payee.MainAddress.State = beneficiary.State;
                    payee.MainAddress.Postcode = beneficiary.Postcode;
                    payee.MainAddress.Country = beneficiary.CountryOfBirth;

                    payee.Account = new List<AccountIFT>();
                    payee.Account = GenerateAccountDetailsForPayerIFT(item);
                    payee.Identification = GenerateIdentificationForPayerIFT(item);


                    structure.Payee.Add(payee);
                }

                model.Structured.Add(structure);


            }
            return model;
        }

        private List<IdentificationIFT> GenerateIdentificationForPayeeIFT(Transaction item)
        {
            List<AccountIFT> accList = new List<AccountIFT>();
            return null;
        }

        private List<AccountIFT> GenerateAccountDetailsForPayeeIFT(Transaction trans)
        {
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            string transAccID = trans.AccountID.Value.ToString();
            string benId = trans.BeneficiaryID.Value.ToString();
            var lst = paymentMethodService.GetAll(x => x.AccountID == transAccID && x.BeneficiaryID == benId, null, "").ToList();
            List<AccountIFT> accList = new List<AccountIFT>();
            foreach (var item in lst)
            {
                AccountIFT acc = new AccountIFT();
                acc.Bsb = "";//no idea
                acc.Id = item.AccountID;
                acc.Number = item.AccountNumber;
                accList.Add(acc);

            }

            return accList;
        }
        //need to implement
        private List<IdentificationIFT> GenerateIdentificationForPayerIFT(Transaction item)
        {
            List<AccountIFT> accList = new List<AccountIFT>();
            return null;
        }

        private List<AccountIFT> GenerateAccountDetailsForPayerIFT(Transaction trans)
        {
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            string transAccID = trans.AccountID.Value.ToString();
            var lst = paymentMethodService.GetAll(x => x.AccountID == transAccID && x.CustomerID == trans.CustomerID, null, "").ToList();
            List<AccountIFT> accList = new List<AccountIFT>();
            foreach (var item in lst)
            {
                AccountIFT acc = new AccountIFT();
                acc.Bsb = "";//no idea
                acc.Id = item.AccountID;
                acc.Number = item.AccountNumber;
                accList.Add(acc);

            }

            return accList;
        }

        #endregion

        public void SerializeObject<T>(T serializableObject, string filePath, string name)
        {
            if (serializableObject == null) { return; }

            try
            {

                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    XmlSerializerNamespaces nam = new XmlSerializerNamespaces();

                    nam.Add("versionMinor", "1");
                    // nam.Add("xs", ":");
                    nam.Add("versionMajor", "1");

                    serializer.Serialize(stream, serializableObject, nam);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(filePath);
                    stream.Close();

                    Response.Clear();
                    Response.ContentType = "application/force-download";
                    Response.AddHeader("content-disposition", "attachment;    filename=" + name + ".xml");
                    byte[] bytesInStream = stream.ToArray(); // simpler way of converting to array
                    Response.BinaryWrite(bytesInStream);
                    Response.End();
                }





            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();
                //Log exception here
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }

            return objectOut;
        }
    }
}