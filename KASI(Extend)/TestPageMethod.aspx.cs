﻿using Kapruka.Domain;
using Kapruka.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Timers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace KASI_Extend_
{
    public partial class TestPageMethod : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // CallTestOne();
            //System.Timers.Timer timer;

            //timer = new System.Timers.Timer(10000);
            //timer.Elapsed += new ElapsedEventHandler(TestApiCurrency);
            //timer.Interval = 2000; // in miliseconds
            //timer.AutoReset = true;
            //timer.Enabled = true;
            //timer.Start();
            //TestApiCurrency();

         //   var obj = new InstaremCurrencyService();
          //  obj.GetAuthenticationToken();

            var objjj = new TranfertoCurrencyService();
            var payetId = 387;
             objjj.GetAllPayers();
           // objjj.GetRatesByPayer(payetId);
        
        }

        private void TestApiCurrency()
        {

            new CurrencyHandlerService().FilterByCountryCurrencyFromDataBase();
        }

        private void TestApiCurrency(object sender, EventArgs e)
        {

            // new CurrencyHandlerService().GetCurrency();
        }

        protected void ClickMe_Click(object sender, EventArgs e)
        {
            CallTestOne();

            //  new CustomerHandlerService().UpdateCustomerByKASIWEBAPIData(obj, 0, 34596);

            var obi = new CustomerHandlerService().GetCustomerById(7);

        }

        private static void CallTestOne()
        {
            var recId = 43386;
            var obj = new CustomerOrMerchantResponseKyced()
            {
                agentTransactionId = "testTran",
                resultCode = "success",
                resultDescription = "desc",
                transactionId = "tran"
            };
            var cService = new CustomerHandlerService();
            var customerIdss = 8795;
            var customerForKyc11 = cService.GetCustomerById(customerIdss);

            var objKyced = new CustomerKyced();
            objKyced.dob = customerForKyc11.DOB;
            objKyced.bfn = customerForKyc11.FirstName;
            objKyced.bln = customerForKyc11.LastName;
            objKyced.man = customerForKyc11.CustomerID.ToString();

            var customerService = new CustomerHandlerService();
            // var blnrecordId = customerService.GetRecordIdBYCustomerId(LastID);
            var recordId = recId;
            var LastID = 0;
            var customerApiMessage = "Webapi successfully called.";
            var customerApiResult = new CustomerHandlerService().CreateKycedCustomer(objKyced, customerIdss,
                customerForKyc11.RecordID, "");



            var customerId = 8796;
            var customerForKyc = cService.GetCustomerById(customerId);

            var merchant = new MerchantKyced();
            merchant.dba = customerForKyc.TradingName;
            merchant.amn = customerForKyc.ABN;
            merchant.website = "type";
            merchant.ac = customerForKyc.Suburb;
            merchant.aco = customerForKyc.Country;
            merchant.afn = customerForKyc.FirstName;
            merchant.aln = customerForKyc.LastName;
            merchant.aph = customerForKyc.Mobile;
            merchant.az = customerForKyc.Postcode;
            //  merchant.state = customerForKyc.State;
            merchant.asn = customerForKyc.StreetName;
            merchant.ataxId = customerForKyc.ABN;
            merchant.bin = "";
            // var  result = new CustomerHandlerService().CreateKycedMerchant(merchant, customerId, customerForKyc.RecordID);
        }

        protected void EncryptFile(object sender, EventArgs e)
        {
            //Get the Input File Name and Extension.
            string fileName = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName);
            string fileExtension = Path.GetExtension(FileUpload1.PostedFile.FileName);

            //Build the File Path for the original (input) and the encrypted (output) file.
            string input = Server.MapPath("~/temp/") + fileName + fileExtension;
            string output = Server.MapPath("~/temp/") + fileName + "_enc" + fileExtension;

            //Save the Input File, Encrypt it and save the encrypted file in output path.
            FileUpload1.SaveAs(input);
            this.Encrypt(input, output);

            //Download the Encrypted File.
            Response.ContentType = FileUpload1.PostedFile.ContentType;
            Response.Clear();
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(output));
            Response.WriteFile(output);
            Response.Flush();

            //Delete the original (input) and the encrypted (output) file.
            File.Delete(input);
            File.Delete(output);

            Response.End();
        }

        protected void checkBase64(object sender, EventArgs e)
        {
            var pathFormat = "~/CustomerDocuments/{0}";
            pathFormat = string.Format(pathFormat, "123");
            var filePath = Server.MapPath(pathFormat);
            //  filePath = string.Format(filePath, customerId);
            var files = Directory.GetFiles(filePath);
            var stringDocumentGuid = "";


            if (files != null)
            {
                foreach (var item in files)
                {
                    var resultImage = Utilities.ConvertFileToBase64(item);

                }

            }

        }


        private void Encrypt(string inputFilePath, string outputfilePath)
        {
            string EncryptionKey = "KAASI2SPBNI99212";
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (FileStream fsOutput = new FileStream(outputfilePath, FileMode.Create))
                {
                    using (CryptoStream cs = new CryptoStream(fsOutput, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        using (FileStream fsInput = new FileStream(inputFilePath, FileMode.Open))
                        {
                            int data;
                            while ((data = fsInput.ReadByte()) != -1)
                            {
                                cs.WriteByte((byte)data);
                            }
                        }
                    }
                }
            }
        }

        protected void DecryptFile(object sender, EventArgs e)
        {
            //Get the Input File Name and Extension
            string fileName = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName);
            string fileExtension = Path.GetExtension(FileUpload1.PostedFile.FileName);

            //Build the File Path for the original (input) and the decrypted (output) file
            string input = Server.MapPath("~/temp/") + fileName + fileExtension;
            string output = Server.MapPath("~/temp/") + fileName + "_dec" + fileExtension;

            //Save the Input File, Decrypt it and save the decrypted file in output path.
            FileUpload1.SaveAs(input);
            this.Decrypt(input, output);

            //Download the Decrypted File.
            Response.Clear();
            Response.ContentType = FileUpload1.PostedFile.ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(output));
            Response.WriteFile(output);
            Response.Flush();

            //Delete the original (input) and the decrypted (output) file.
            File.Delete(input);
            File.Delete(output);

            Response.End();
        }

        private void Decrypt(string inputFilePath, string outputfilePath)
        {
            string EncryptionKey = "KAASI2SPBNI99212";
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (FileStream fsInput = new FileStream(inputFilePath, FileMode.Open))
                {
                    using (CryptoStream cs = new CryptoStream(fsInput, encryptor.CreateDecryptor(), CryptoStreamMode.Read))
                    {
                        using (FileStream fsOutput = new FileStream(outputfilePath, FileMode.Create))
                        {
                            int data;
                            while ((data = cs.ReadByte()) != -1)
                            {
                                fsOutput.WriteByte((byte)data);
                            }
                        }
                    }
                }
            }
        }

    }
}