﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;
using System.Web.Security;

namespace KASI_Extend.Processor
{
    public partial class AddNewInwardLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String pword = System.Web.Security.Membership.GeneratePassword(10, 3);
            try
            {
                LoginService LGServ = new LoginService(new UnitOfWorks(new KaprukaEntities()));

                Kapruka.Repository.Login newLogin = new Kapruka.Repository.Login();

                newLogin.FirstName = Request.QueryString["Fname"];
                newLogin.LastName = Request.QueryString["LName"];
                newLogin.FullName = Request.QueryString["Fname"] + " " + Request.QueryString["LName"];
                newLogin.LoginGroup = "1";
                newLogin.CreatedDateTime = DateTime.Now;
                newLogin.CreatedBy = Session["LoggedUserFullName"].ToString();
                newLogin.Active = "Y";
                newLogin.Username = Request.QueryString["Uname"];
                newLogin.Password = pword;
                newLogin.Email = Request.QueryString["Email"];
                newLogin.AgentID = Int32.Parse(Request.QueryString["AID"].ToString());
                newLogin.InwardAgentID = Int32.Parse(Request.QueryString["AID"].ToString());
                newLogin.LoginType = "AGENT";
                newLogin.Activated = 0;
                newLogin.IsComplianceManager = false;
                newLogin.TransactionDirection = "I";

                LGServ.Add(newLogin);
                var theID = newLogin.LoginID;
                KASI_Extend_.classes.EmailCustomer EC = new KASI_Extend_.classes.EmailCustomer();
                EC.SendAffiliateEmailCreation(theID.ToString());

                    Response.Write("Login created successfully.");
            }
            catch (Exception ex)
            {
                Response.Write("An error occured during this process. Please contact your system administrator.");
            }
            

            
        }
    }
}