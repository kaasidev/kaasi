﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.Processor
{
    public partial class UpdateInwardTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int TID = Int32.Parse(Request.QueryString["TID"].ToString());

                InwardTransactionService ITServ = new InwardTransactionService(new UnitOfWorks(new KaprukaEntities()));
                var ITDetails = ITServ.GetAll(x => x.ID == TID, null, "").SingleOrDefault();

                InTransAuditLogServicecs ITALServ = new InTransAuditLogServicecs(new UnitOfWorks(new KaprukaEntities()));

                InTransAuditLog newITAL = new InTransAuditLog();
                newITAL.TransactionID = TID;
                newITAL.Username = Session["LoggedPersonName"].ToString();
                newITAL.AgentName = Session["LoggedCompany"].ToString();
                newITAL.ActionType = "EDIT";
                newITAL.Description = "has approved this transaction.";
                newITAL.AuditDateTime = DateTime.Now;

                ITALServ.Add(newITAL);


                ITDetails.Status = "SUCCESSFUL";


                ITServ.Update(ITDetails);

                Response.Write("Transaction successfully approved");
            }
            catch (Exception ex)
            {
                Response.Write("An error occured. Please contact your adminstrator");
            }
            
        }
    }
}