﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Security;
using KASI_Extend_.classes;
using Kapruka.Domain;
using Kapruka.Service;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend_.Processor
{
    public partial class AddAgent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AgentService AGTServ = new AgentService(new UnitOfWorks(new KaprukaEntities()));

            Kapruka.Repository.Agent AG = new Kapruka.Repository.Agent();


            String AgentName = Request.QueryString["AgName"].ToString();
            //String AddLine1 = Request.QueryString["AgAddLine1"].ToString();
            //String AddLine2 = Request.QueryString["AgAddLine2"].ToString();
            //String Suburb = Request.QueryString["AgSuburb"].ToString();
            //String State = Request.QueryString["AgState"].ToString();
            //String Postcode = Request.QueryString["AgPostcode"].ToString();
            //String Phone = Request.QueryString["AgPhone"].ToString();
            //String Fax = Request.QueryString["AgFax"].ToString();
            //String Email = Request.QueryString["AgEmail"].ToString();
            //String ABN = Request.QueryString["AgABN"].ToString();
            //String ACN = Request.QueryString["AgACN"].ToString();
            //String FlatFee = Request.QueryString["AgFlatFee"].ToString();
            //String AddPerc = Request.QueryString["AgAddPerc"].ToString();
            //String CommStruc = Request.QueryString["AgCommStruc"].ToString();
            //String CommPerc = Request.QueryString["AgCommPerc"].ToString();
            String Countries = Request.QueryString["AgCountries"].ToString();
            String MUName = Request.QueryString["AgMUName"].ToString();
            String MULName = Request.QueryString["AgMULName"].ToString();
            String MUPhone = Request.QueryString["AgMUPhone"].ToString();
            String MUMobile = Request.QueryString["AgMUMobile"].ToString();
            String MUEmail = Request.QueryString["AgMUEmail"].ToString();
            String BankAccounts = Request.QueryString["AgBanks"].ToString();
            String AustracRegNo = Request.QueryString["AgAUSRegNo"].ToString();
            String AustracRegDate = Request.QueryString["AgAUSRegDate"].ToString();
            String AustracRegExp = Request.QueryString["AgAUSRegExp"].ToString();
            String MAUDLimit = Request.QueryString["AgMAUDLimit"].ToString();
            String YAUDLimit = Request.QueryString["AgYAUDLimit"].ToString();
            String MNbLimit = Request.QueryString["AgMNbLimit"].ToString();
            String YNbLimit = Request.QueryString["AgYNbLimit"].ToString();

            AG.AgentName = Request.QueryString["AgName"].ToString();
            AG.AgentAddress1 = Request.QueryString["AgAddLine1"].ToString();
            AG.AgentAddress2 = Request.QueryString["AgAddLine2"].ToString();
            AG.AgentCity = Request.QueryString["AgSuburb"].ToString();
            AG.AgentState = Request.QueryString["AgState"].ToString();
            AG.AgentPostcode = Request.QueryString["AgPostcode"].ToString();
            AG.AgentPhone = Request.QueryString["AgPhone"].ToString();
            AG.AgentFax = Request.QueryString["AgFax"].ToString();
            AG.AgentEmail = Request.QueryString["AgEmail"].ToString();
            AG.AgentABN = Request.QueryString["AgABN"].ToString();
            AG.AgentACN = Request.QueryString["AgACN"].ToString();
            AG.AgentFeeFlat = Request.QueryString["AgFlatFee"].ToString()==null? 0: decimal.Parse(Request.QueryString["AgFlatFee"].ToString());
            AG.AgentTransGainCommission = Request.QueryString["AgAddPerc"].ToString() == null ? 0 : double.Parse(Request.QueryString["AgAddPerc"].ToString());
            AG.AgentCommissionStructure = Request.QueryString["AgCommStruc"].ToString();
            AG.AgentFeeCommission = Request.QueryString["AgCommPerc"].ToString() == null ? 0 : double.Parse(Request.QueryString["AgCommPerc"].ToString());
            AG.CreatedBy = Session["LoggedUserFullName"].ToString();
            AG.AustracNumber = Request.QueryString["AgAUSRegNo"].ToString()==null? DBNull.Value.ToString(): Request.QueryString["AgAUSRegNo"].ToString();
            AG.AustracRegDate = Request.QueryString["AgAUSRegDate"].ToString()==null? (DateTime?)null: Convert.ToDateTime(Request.QueryString["AgAUSRegDate"].ToString());
            AG.AustracRenewalDate = Request.QueryString["AgAUSRegExp"].ToString() == null ? (DateTime?)null : Convert.ToDateTime(Request.QueryString["AgAUSRegExp"].ToString());

            AGTServ.Add(AG);

            //SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            //String SQLStmt = "SET DATEFORMAT dmy INSERT INTO dbo.Agents (AgentName, AgentAddress1, AgentAddress2, AgentCity, AgentState, AgentPostcode, AgentPhone, AgentFax, AgentCommissionStructure, AgentFeeCommission, AgentFeeFlat, AgentTransGainCommission, Status, CreatedBy, CreatedDateTime,AgentABN,AgentACN,AgentEmail, AustracNumber, AustracRegDate, AustracRenewalDate) VALUES (@AgentName, @AgentAddress1, @AgentAddress2, @AgentCity, @AgentState, @AgentPostcode, @AgentPhone, @AgentFax, @AgentCommissionStructure, @AgentFeeCommission, @AgentFeeFlat, @AgentTransGainCommission, 'Active', @CreatedBy, CURRENT_TIMESTAMP,@AgentABN,@AgentACN,@AgentEmail, @AustracNumber, @AustracRegDate, @AustracRenewalDate)";

            //using (SqlCommand cmd = new SqlCommand(SQLStmt))
            //{
            //    cmd.Connection = conn;
            //    conn.Open();
            //    cmd.Parameters.Add("@AgentName", SqlDbType.NVarChar, 100).Value = AgentName;
            //    cmd.Parameters.Add("@AgentAddress1", SqlDbType.NVarChar, 150).Value = AddLine1;
            //    cmd.Parameters.Add("@AgentAddress2", SqlDbType.NVarChar, 150).Value = AddLine2;
            //    cmd.Parameters.Add("@AgentCity", SqlDbType.NVarChar, 100).Value = Suburb;
            //    cmd.Parameters.Add("@AgentState", SqlDbType.NVarChar, 100).Value = State;
            //    cmd.Parameters.Add("@AgentPostcode", SqlDbType.NVarChar, 5).Value = Postcode;
            //    cmd.Parameters.Add("@AgentPhone", SqlDbType.NVarChar, 15).Value = Phone;
            //    cmd.Parameters.Add("@AgentFax", SqlDbType.NVarChar, 15).Value = Fax;
            //    cmd.Parameters.Add("@AgentCommissionStructure", SqlDbType.NVarChar, 10).Value = CommStruc;
            //    cmd.Parameters.Add("@AgentFeeCommission", SqlDbType.Float).Value = AddPerc;
            //    cmd.Parameters.Add("@AgentFeeFlat", SqlDbType.Money).Value = FlatFee;
            //    cmd.Parameters.Add("@AgentTransGainCommission", SqlDbType.Float).Value = CommPerc;
            //    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 50).Value = Session["LoggedUserFullName"].ToString();
            //    cmd.Parameters.Add("@AgentABN", SqlDbType.NVarChar, 50).Value = ABN;
            //    cmd.Parameters.Add("@AgentACN", SqlDbType.NVarChar, 50).Value = ACN;
            //    cmd.Parameters.Add("@AgentEmail", SqlDbType.NVarChar, 50).Value = Email;
            //    cmd.Parameters.Add("@AustracNumber", SqlDbType.NVarChar, 100).Value = AustracRegNo;
            //    if (!String.IsNullOrEmpty(AustracRegDate))
            //    {
            //        DateTime ARD = Convert.ToDateTime(AustracRegDate);
            //        cmd.Parameters.AddWithValue("@AustracRegDate", AustracRegDate);
            //    }
            //    else
            //    {
            //        cmd.Parameters.AddWithValue("@AustracRegDate", DBNull.Value);
            //    }
                
            //    if(!String.IsNullOrEmpty(AustracRegExp))
            //    {
            //        DateTime ARE = Convert.ToDateTime(AustracRegExp);
            //        cmd.Parameters.AddWithValue("@AustracRenewalDate", AustracRegExp);
            //    }
            //    else
            //    {
            //        cmd.Parameters.AddWithValue("@AustracRenewalDate", DBNull.Value);
            //    }
                
            //    cmd.ExecuteNonQuery();
            //    conn.Close();
            //}

            String AID = CreateAgentLoginID(AgentName, MUName, MUEmail, MULName);
            AssignedAgentCountries(AID, Countries);
            AssignedAgentBankAccounts(AID, BankAccounts);
            AssignAgentLimits(AID, MAUDLimit, YAUDLimit, MNbLimit, YNbLimit);

            SettingAuditLogService SALServ = new SettingAuditLogService(new UnitOfWorks(new KaprukaEntities()));
            SettingAuditLog SALog = new SettingAuditLog();

            SALog.Username = Session["LoggedPersonName"].ToString();
            SALog.Company = Session["LoggedCompany"].ToString();
            SALog.Type = "ADD";
            SALog.Description = "has added a new affiliate called " + AgentName;
            SALog.AuditDateTime = DateTime.Now;

            SALServ.Add(SALog);

        }

        private ResultKycCall CallMerchant(int customerId, CustomerHandlerService cService,
          AgentKYC customerForKyc, ResultKycCall resulCall)
        {
            var merchant = new MerchantKyced();
            if (customerForKyc.AgentName != null)
                merchant.dba = customerForKyc.AgentName;
            else
                merchant.dba = "";

            merchant.amn = customerForKyc.AgentName;

            merchant.website = "";
            if (customerForKyc.AgentCity != null)
                merchant.ac = customerForKyc.AgentCity;
            else
                merchant.ac = "";
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    merchant.aco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    merchant.aco = "AU";
            }
            else
                merchant.aco = "AU";
            if (customerForKyc.AgentName != null)
                merchant.afn = customerForKyc.AgentName;
            else
                merchant.afn = "";
            if (customerForKyc.AgentName != null)
                merchant.aln = customerForKyc.AgentName;
            else
                merchant.aln = "";
            if (customerForKyc.AgentPhone != null)
                merchant.aph = customerForKyc.AgentPhone;
            else
                merchant.aph = "";
            if (customerForKyc.AgentPostcode != null)
                merchant.az = customerForKyc.AgentPostcode;
            else
                merchant.az = "";

            if (customerForKyc.AgentState != null)
                merchant.@as = customerForKyc.AgentState;
            else
                merchant.@as = "";
            if (customerForKyc.AgentAddress1 != null)
                merchant.asn = customerForKyc.AgentAddress1;
            else
                merchant.asn = "";
            if (customerForKyc.AgentABN != null)
                merchant.ataxId = customerForKyc.AgentABN;
            else
                merchant.ataxId = "";

            merchant.bin = "";
            var runBy = "";
            if (Session["usernameLogged"] != null)
                runBy =Session["usernameLogged"].ToString();

            resulCall = cService.CreateKycedAgent(merchant, customerId, runBy);
            return resulCall;
        }

        private void AssignedAgentBankAccounts(string AID, string BankAccounts)
        {
            string[] bankArr = BankAccounts.Split(',');

            foreach (var bank in bankArr)
            {
                if (!string.IsNullOrEmpty(bank))
                {
                    SaveBankToAgent(AID, bank);
                }
            }
        }

        private String CreateAgentLoginID(String AgentName, String MUName, String MUEmail, String MULName)
        {
            classes.Agent agt = new classes.Agent();
            String AID = agt.getAgentIDByName(AgentName);
            String pword = System.Web.Security.Membership.GeneratePassword(10, 3);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "INSERT INTO dbo.Logins (FirstName, LastName, FullName, LoginGroup, AdminOverrideCode, CreatedDateTime, CreatedBy, Active, Username, Password, Email, AgentID, LoginType, Activated) VALUES (@FirstName, @LastName, @FullName, @LoginGroup, @AdminOverrideCode, CURRENT_TIMESTAMP, @CreatedBy, 'Y', @Username, @Password, @Email, @AgentID, 'AGENT', '0')";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@FullName", SqlDbType.NVarChar, 100).Value = MUName + " " + MULName;
                cmd.Parameters.Add("@LoginGroup", SqlDbType.Int, 1).Value = 1;
                cmd.Parameters.Add("@AdminOverrideCode", SqlDbType.Int, 4).Value = 1234;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"].ToString();
                cmd.Parameters.Add("@Username", SqlDbType.NVarChar, 100).Value = MUEmail;
                cmd.Parameters.Add("@Password", SqlDbType.NVarChar, 20).Value = pword;
                cmd.Parameters.Add("@Email", SqlDbType.NVarChar, 100).Value = MUEmail;
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 15).Value = AID;
                cmd.Parameters.AddWithValue("@FirstName", MUName);
                cmd.Parameters.AddWithValue("@LastName", MULName);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            EmailCustomer cs = new EmailCustomer();
            cs.SendAgentEmailCreation(MUEmail);

            SettingAuditLogService SALServ = new SettingAuditLogService(new UnitOfWorks(new KaprukaEntities()));
            SettingAuditLog SALog = new SettingAuditLog();

            SALog.Username = Session["LoggedPersonName"].ToString();
            SALog.Company = Session["LoggedCompany"].ToString();
            SALog.Type = "ADD";
            SALog.Description = "has added a new account for " + MUName + " " + MULName + " for affiliate " + AgentName;
            SALog.AuditDateTime = DateTime.Now;

            SALServ.Add(SALog);

            return AID;
        }

        private void AssignedAgentCountries(String AID, String CountryList)
        {
            Generic gnr = new Generic();

            List<String> Countries = new List<String>();
            Countries = gnr.ConvertStringToList(CountryList, ',');

            foreach (var Country in Countries)
            {
                if (!string.IsNullOrEmpty(Country))
                {

                    saveCountryToAgent(AID, Country);
                    AssignCurrenciesToAgent(AID, Country);
                }
            }
        }

        private String FindCountryIDByName(String Country)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String CountryID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT CountryID FROM dbo.Countries WHERE CountryName = '" + Country + "'";
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        CountryID = sdr["CountryID"].ToString();
                    }
                }
                conn.Close();
            }

            return CountryID;
        }

        private void saveCountryToAgent(String AID, String Country)
        {
            AgentAssignedCountryService AACServ = new AgentAssignedCountryService(new UnitOfWorks(new KaprukaEntities()));

            int intAID = Int32.Parse(AID);
            int intCID = Int32.Parse(FindCountryIDByName(Country));

            AgentAssignedCountry AAC = new AgentAssignedCountry();
            AAC.AgentID = intAID;
            AAC.CountryID = intCID;

            AACServ.Add(AAC);

            //SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            //String SQLStmt = "INSERT INTO dbo.AgentAssignedCountries (AgentID, CountryID) VALUES (@AgentID, @CountryID)";

            //using (SqlCommand cmd = new SqlCommand(SQLStmt))
            //{
            //    cmd.Connection = conn;
            //    conn.Open();
            //    cmd.Parameters.Add("@AgentID", SqlDbType.Int, 3).Value = AID;
            //    cmd.Parameters.Add("@CountryID", SqlDbType.Int, 3).Value = FindCountryIDByName(Country);
            //    cmd.ExecuteNonQuery();
            //    conn.Close();
            //}

            AgentAuditLogService AALSer = new AgentAuditLogService(new UnitOfWorks(new KaprukaEntities()));
            AgentAuditLog AAL = new AgentAuditLog();
            AAL.Username = Session["LoggedPersonName"].ToString();
            AAL.Company = Session["LoggedCompany"].ToString();
            AAL.AgentID = Int32.Parse(AID);
            AAL.Type = "ADD";
            AAL.Description = "has been assigned the following country: " + FindCountryIDByName(Country);
            AAL.AuditDateTime = DateTime.Now;

            AALSer.Add(AAL);
        }

        private void SaveBankToAgent(String AID, String bankAccId)
        {
            int intBAID = Int32.Parse(bankAccId);
            int intAID = Int32.Parse(AID);

            MasterCompanyBankService MCBServ = new MasterCompanyBankService(new UnitOfWorks(new KaprukaEntities()));
            var MCBName = MCBServ.GetAll(x => x.BankAccountID == intBAID, null, "").SingleOrDefault().AccountName;

            AgentAssignedBankService AABServ = new AgentAssignedBankService(new UnitOfWorks(new KaprukaEntities()));
            AgentAssignedBank AAB = new AgentAssignedBank();
            AAB.AgentID = intAID;
            AAB.BankAccountID = intBAID;
            AABServ.Add(AAB);

            //SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            //String SQLStmt = "INSERT INTO dbo.AgentAssignedBanks (AgentID, MasterBankAccountID) VALUES (@AgentID, @BankAccountID)";

            //using (SqlCommand cmd = new SqlCommand(SQLStmt))
            //{
            //    cmd.Connection = conn;
            //    conn.Open();
            //    cmd.Parameters.Add("@AgentID", SqlDbType.Int, 3).Value = AID;
            //    cmd.Parameters.Add("@BankAccountID", SqlDbType.Int, 3).Value = bankAccId;
            //    cmd.ExecuteNonQuery();
            //    conn.Close();
            //}

            AgentAuditLogService AALSer = new AgentAuditLogService(new UnitOfWorks(new KaprukaEntities()));
            AgentAuditLog AAL = new AgentAuditLog();
            AAL.Username = Session["LoggedPersonName"].ToString();
            AAL.Company = Session["LoggedCompany"].ToString();
            AAL.AgentID = Int32.Parse(AID);
            AAL.Type = "ADD";
            AAL.Description = "has been assigned the following Bank Account: " + MCBName;
            AAL.AuditDateTime = DateTime.Now;

            AALSer.Add(AAL);
        }

        private void AssignCurrenciesToAgent(String AID, String Country)
        {
            classes.Countries Countries = new Countries();
            classes.Currencies curr = new Currencies();
            String CountryID = Countries.getCountryIDByName(Country);
            List<String> Currencies = new List<String>();
            Currencies = curr.getCountryCurrencies(CountryID);
            String MAUDLimit = curr.getMonthlyAUDLimit();
            String YAUDLimit = curr.getYearlyAUDLimit();

            int intAID = Int32.Parse(AID);

            AgentService AGTSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            var AgentName = AGTSer.GetAll(x => x.AgentID == intAID, null, "").SingleOrDefault().AgentName;

            CurrencyService CurServ = new CurrencyService(new UnitOfWorks(new KaprukaEntities()));

            int countValue = 0;

            foreach (var Currency in Currencies)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                int intCID = Int32.Parse(Currency);

                var CurName = CurServ.GetAll(x => x.CurrencyID == intCID, null, "").SingleOrDefault().CurrencyName;

                AgentCurrencyService ACServ = new AgentCurrencyService(new UnitOfWorks(new KaprukaEntities()));
                var ACDetails = ACServ.GetAll(x => x.AgentID == intAID && x.CurrencyID == intCID, null, "").ToList();

                if (ACDetails != null)
                {
                    countValue = ACDetails.Count;
                }
                else
                {
                    countValue = 0;
                }

                // Check if this currency is already present for the agent
                //using (SqlCommand cmd = new SqlCommand())
                //{
                //    cmd.CommandText = "SELECT COUNT(*) AS TotCount FROM dbo.AgentCurrencies WHERE AgentID = " + AID + " AND CurrencyID = " + Currency;
                //    cmd.Connection = conn;
                //    conn.Open();

                //    using (SqlDataReader sdr = cmd.ExecuteReader())
                //    {
                //        while (sdr.Read())
                //        {
                //            countValue = Int32.Parse(sdr["TotCount"].ToString());
                //        }
                //    }
                //    conn.Close();
                //}

                if (countValue == 0)
                {
                    AgentCurrency AC = new AgentCurrency();
                    AC.AgentID = intAID;
                    AC.CountryID = intCID;
                    AC.CurrencyMonthlyLimit = decimal.Parse(MAUDLimit);
                    AC.CurrencyYearlyLimit = decimal.Parse(YAUDLimit);
                    AC.AgentSetMonthlyLimit = decimal.Parse(MAUDLimit);
                    AC.AgentSetYearlyLimit = decimal.Parse(YAUDLimit);

                    ACServ.Add(AC);


                    //String SQLStmt = "INSERT INTO dbo.AgentCurrencies (AgentID, CurrencyID, CurrencyMonthlyLimit, CurrencyYearlyLimit, AgentSetMonthlyLimit, AgentSetYearlyLimit) VALUES (@AgentID, @CurrencyID, @CurrencyMonthlyLimit, @CurrencyYearlyLimit, @AgentSetMonthlyLimit, @AgentSetYearlyLimit)";

                    //using (SqlCommand cmd = new SqlCommand(SQLStmt))
                    //{
                    //    cmd.Connection = conn;
                    //    conn.Open();
                    //    cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                    //    cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = Currency;
                    //    cmd.Parameters.Add("@CurrencyMonthlyLimit", SqlDbType.Money, 20).Value = MAUDLimit;
                    //    cmd.Parameters.Add("@CurrencyYearlyLimit", SqlDbType.Money, 20).Value = YAUDLimit;
                    //    cmd.Parameters.Add("@AgentSetMonthlyLimit", SqlDbType.Money, 20).Value = MAUDLimit;
                    //    cmd.Parameters.Add("@AgentSetYearlyLimit", SqlDbType.Money, 20).Value = YAUDLimit;
                    //    cmd.ExecuteNonQuery();
                    //    conn.Close();
                    //}
                    AssignCurrencyMargins(AID, Currency);
                    AssignCurrencyRate(AID, Currency);

                    AgentAuditLogService AALSer = new AgentAuditLogService(new UnitOfWorks(new KaprukaEntities()));
                    AgentAuditLog AAL = new AgentAuditLog();
                    AAL.Username = Session["LoggedPersonName"].ToString();
                    AAL.Company = Session["LoggedCompany"].ToString();
                    AAL.AgentID = Int32.Parse(AID);
                    AAL.Type = "ADD";
                    AAL.Description = "has been assigned the following currency: " + CurName;
                    AAL.AuditDateTime = DateTime.Now;

                    AALSer.Add(AAL);

                }
                AssignTransactionFeeTiers(AID, Currency, CountryID);
            }

        }

        private void AssignCurrencyMargins(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"INSERT INTO dbo.AgentCurrencyMargins (AgentID, CurrencyID, UpDown, Margin) VALUES (@AgentID, @CurrencyID, @UpDown, @Margin)";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CID;
                cmd.Parameters.Add("@UpDown", SqlDbType.NVarChar, 20).Value = "Down";
                cmd.Parameters.Add("@Margin", SqlDbType.Money, 20).Value = 0.00;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void AssignCurrencyRate(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"INSERT INTO dbo.AgentCurrencyRates (AgentID, CurrencyID, AssignedRate, AssignedDate, CreatedDateTime) VALUES (@AgentID, @CurrencyID, @AssignedRate, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CID;
                cmd.Parameters.Add("@AssignedRate", SqlDbType.Money, 20).Value = 0.00;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void AssignTransactionFeeTiers(String AID, String CurrencyID, String CountryID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"INSERT INTO dbo.AgentTransactionFeeTiers (AgentID, CountryID, CurrencyID, Tier1From, Tier1To, Tier1Amount, Tier2From, Tier2To, Tier2Amount, Tier3From, Tier3To, Tier3Amount) 
                            VALUES (@AgentID, @CountryID, @CurrencyID, @Tier1From, @Tier1To, @Tier1Amount, @Tier2From, @Tier2To, @Tier2Amount, @Tier3From, @Tier3To, @Tier3Amount)";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CurrencyID;
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 20).Value = CountryID;
                cmd.Parameters.Add("@Tier1From", SqlDbType.Money, 5).Value = 1.00;
                cmd.Parameters.Add("@Tier1To", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier1Amount", SqlDbType.Money, 20).Value = 0.00;
                cmd.Parameters.Add("@Tier2From", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier2To", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier2Amount", SqlDbType.Money, 20).Value = 0.00;
                cmd.Parameters.Add("@Tier3From", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier3To", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier3Amount", SqlDbType.Money, 20).Value = 0.00;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void AssignAgentLimits(String AID, String MAUDLimit, String YAUDLimit, String MNbLimit, String YNbLimit)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"UPDATE dbo.Agents SET MonthlyAUDLimit=@MonthlyAUDLimit, YearlyAUDsLimit=@YearlyAUDsLimit, MonthlyTransLimit=@MonthlyTransLimit
                            , YearlyTransLimit=@YearlyTransLimit WHERE AgentID = " + AID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@MonthlyAUDLimit", MAUDLimit);
                cmd.Parameters.AddWithValue("@YearlyAUDsLimit", YAUDLimit);
                cmd.Parameters.AddWithValue("@MonthlyTransLimit", MNbLimit);
                cmd.Parameters.AddWithValue("@YearlyTransLimit", YNbLimit);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}