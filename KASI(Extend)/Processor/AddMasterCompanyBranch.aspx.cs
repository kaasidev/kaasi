﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class AddMasterCompanyBranch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id = int.Parse(Request.QueryString["Id"].ToString());
            string bankName = Request.QueryString["BankName"].ToString();
            string bsb = Request.QueryString["bsb"].ToString();
            string accountNum = Request.QueryString["account"].ToString();
            string accountName = Request.QueryString["accountName"].ToString();

            if (id == 0)
            {
               
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLStmt = "INSERT INTO dbo.MasterCompanyBanks (BankName, BSB, AccountNumber, AccountName,Active,CreatedBy,CreatedDateTime) VALUES (@BankName, @BSB, @AccountNumber, @AccountName,'Y', @CreatedBy, CURRENT_TIMESTAMP)";

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@BankName", SqlDbType.NVarChar, 100).Value = bankName;
                    cmd.Parameters.Add("@BSB", SqlDbType.NVarChar, 100).Value = bsb;
                    cmd.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 100).Value = accountNum;
                    cmd.Parameters.Add("@AccountName", SqlDbType.NVarChar, 100).Value =accountName;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"].ToString();


                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

            }
            else
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLStmt = "UPDATE dbo.MasterCompanyBanks set BankName=@BankName,BSB=@BSB,AccountNumber=@AccountNumber,AccountName=@AccountName,AlteredBy='SYSTEM',AlteredDateTime=getdate() WHERE BankAccountID="+id;

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@BankName", SqlDbType.NVarChar, 100).Value = bankName;
                    cmd.Parameters.Add("@BSB", SqlDbType.NVarChar, 100).Value = bsb;
                    cmd.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 100).Value = accountNum;
                    cmd.Parameters.Add("@AccountName", SqlDbType.NVarChar, 100).Value = accountName;

                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

            }
            Response.Write("OK");
        }
    }
}