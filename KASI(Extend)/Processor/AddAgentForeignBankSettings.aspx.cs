﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace KASI_Extend_.Processor
{
    public partial class AddAgentForeignBankSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            classes.Currencies CURR = new classes.Currencies();
            String AID = Session["AgentID"].ToString();
            String BID = Request.QueryString["BID"];
            String AccID = Request.QueryString["AccID"];
            String CID = Request.QueryString["CID"];
            String Currencies = Request.QueryString["Curr"];

            String[] CurrList = Currencies.Split(new char[0]);
            String CurrencyOutput = String.Empty;

            foreach(var Currency in CurrList)
            {
                String CurrencyID = CURR.getCurrencyIDFromCurrencyCode(Currency);
                CurrencyOutput += CurrencyID + ",";
            }

            int Length = CurrencyOutput.Length;
            CurrencyOutput = CurrencyOutput.Substring(0, (Length - 2));

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"INSERT INTO dbo.AgentForeignBankSettings (AgentID, CountryID, BankID, ForeignBankAccountNumber, CurrencyID) VALUES 
                            (@AgentID, @CountryID, @BankID, @ForeignBankAccountNumber, @CurrencyID)";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@AgentID", AID);
                cmd.Parameters.AddWithValue("@CountryID", CID);
                cmd.Parameters.AddWithValue("@BankID", BID);
                cmd.Parameters.AddWithValue("@ForeignBankAccountNumber", AccID);
                cmd.Parameters.AddWithValue("@CurrencyID", CurrencyOutput);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}