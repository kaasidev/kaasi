﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using System.Web.Security;

namespace KASI_Extend_.Processor
{
    public partial class AddNewAgentUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CompMan = Request.QueryString["CompMan"].ToString();
            Boolean CompVal;
            String pword = Membership.GeneratePassword(10, 3);

            if (CompMan == "false")
            {
                CompVal = false;
            }
            else
            {
                CompVal = true;
            }

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = "INSERT INTO dbo.Logins (FirstName, LastName, FullName, LoginGroup, Active, Username, Password, Email, AgentID, LoginType, Activated, IsComplianceManager, AdminOverrideCode) VALUES (@FirstName, @LastName, @FullName, 1, 'Y', @Username, @Password, @Email, @AgentID, @LoginType, 0, @IsComplianceManager, @AdminOverrideCode)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["FName"].ToString();
                cmd.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["LName"].ToString();
                cmd.Parameters.Add(("@FullName"), SqlDbType.NVarChar, 250).Value = Request.QueryString["FName"].ToString() + ' ' + Request.QueryString["LName"].ToString();
                cmd.Parameters.Add(("@Username"), SqlDbType.NVarChar, 100).Value = Request.QueryString["UName"].ToString();
                cmd.Parameters.Add(("@Password"), SqlDbType.NVarChar, 100).Value = pword;
                cmd.Parameters.Add(("@Email"), SqlDbType.NVarChar, 100).Value = Request.QueryString["Email"].ToString();
                cmd.Parameters.Add(("@AgentID"), SqlDbType.NVarChar, 100).Value = Request.QueryString["AID"].ToString();
                cmd.Parameters.Add(("@LoginType"), SqlDbType.NVarChar, 100).Value = Request.QueryString["AccountType"].ToString();
                cmd.Parameters.Add(("@IsComplianceManager"), SqlDbType.Bit).Value = CompVal;
                cmd.Parameters.Add(("@AdminOverrideCode"), SqlDbType.NVarChar, 100).Value = Request.QueryString["CompManPIN"].ToString();    
                cmd.ExecuteNonQuery();
                conn.Close();

            }

            EmailCustomer CEmail = new EmailCustomer();
            CEmail.SendAgentEmailCreation(Request.QueryString["Email"].ToString());
        }
    }
}