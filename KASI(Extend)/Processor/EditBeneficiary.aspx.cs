﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class EditBeneficiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String ID = Request.QueryString["BID"].ToString();
            String thefinalDate = String.Empty;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SqlStmt = "UPDATE dbo.Beneficiaries SET BeneficiaryName=@BeneficiaryName, BeneficiaryLastName=@BeneficiaryLastName, BeneficiaryFirstNames=@BeneficiaryFirstNames, AddressLine1=@AddressLine1, AddressLine2=@AddressLine2, Suburb=@Suburb, State=@State, Postcode=@Postcode, Country=@Country, CountryID=@CountryID, TelHome=@TelHome, Relationship=@Relationship, CountryOfBirth=@CountryOfBirth, DOB=@DOB, PlaceOfBirth=@PlaceOfBirth, Nationality=@Nationality, AKA=@AKA, NationalityCardID=@NationalityCardID, EmailAddress=@EmailAddress, AmendedBy=@AmendedBy, AmendedDateTime=CURRENT_TIMESTAMP WHERE BeneficiaryID = @BID";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@BID", SqlDbType.Int).Value = Request.QueryString["BID"].ToString();
                cmd.Parameters.Add("@BeneficiaryName", SqlDbType.NVarChar, 100).Value = Request.QueryString["BeneFirstName"].ToString() + ' ' + Request.QueryString["BeneLastName"].ToString();
                cmd.Parameters.Add("@BeneficiaryLastName", SqlDbType.NVarChar, 100).Value = Request.QueryString["BeneLastName"].ToString();
                cmd.Parameters.Add("@BeneficiaryFirstNames", SqlDbType.NVarChar, 300).Value = Request.QueryString["BeneFirstName"].ToString();
                cmd.Parameters.Add("@AddressLine1", SqlDbType.NVarChar, 100).Value = Request.QueryString["AddressLine1"].ToString();
                cmd.Parameters.Add("@AddressLine2", SqlDbType.NVarChar, 100).Value = Request.QueryString["AddressLine2"].ToString();
                cmd.Parameters.AddWithValue("@Suburb", Request.QueryString["Suburb"].ToString() == "-- SELECT --" ? (object) DBNull.Value : Request.QueryString["Suburb"].ToString());
                cmd.Parameters.AddWithValue("@State", Request.QueryString["State"].ToString() == "-- SELECT --" ? (object) DBNull.Value : Request.QueryString["State"].ToString());
                cmd.Parameters.Add("@Postcode", SqlDbType.NVarChar, 100).Value = Request.QueryString["Postcode"].ToString();
                cmd.Parameters.Add("@Country", SqlDbType.NVarChar, 100).Value = Request.QueryString["Country"].ToString();
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 10).Value = Request.QueryString["CountryID"].ToString();
                cmd.Parameters.Add("@TelHome", SqlDbType.NVarChar, 100).Value = Request.QueryString["ContactNumber"].ToString();
                cmd.Parameters.Add("@Relationship", SqlDbType.NVarChar, 100).Value = Request.QueryString["Relationship"].ToString();
                cmd.Parameters.AddWithValue("@CountryOfBirth", Request.QueryString["COB"].ToString() == "--Select--" ? (object) DBNull.Value : Request.QueryString["COB"].ToString());
                String theDOB = Request.QueryString["DOB"].ToString();
                if (!String.IsNullOrEmpty(theDOB))
                {
                    DateTime DOB = Convert.ToDateTime(theDOB);
                    cmd.Parameters.AddWithValue("@DOB", SqlDbType.DateTime).Value = DOB ;
                }
                else
                {
                    cmd.Parameters.AddWithValue("@DOB", SqlDbType.DateTime).Value = DBNull.Value;
                }
               
                //cmd.Parameters.AddWithValue("@DOB", theDOB == null ? (object)DBNull.Value : thefinalDate);
                cmd.Parameters.Add("@PlaceOfBirth", SqlDbType.NVarChar, 100).Value = Request.QueryString["POB"].ToString();
                cmd.Parameters.AddWithValue("@Nationality", Request.QueryString["National"].ToString() == "--Select--" ? (object) DBNull.Value : Request.QueryString["National"].ToString());
                cmd.Parameters.Add("@AKA", SqlDbType.NVarChar, 200).Value = Request.QueryString["AKA"].ToString();
                cmd.Parameters.Add("@NationalityCardID", SqlDbType.NVarChar, 100).Value = Request.QueryString["ID"].ToString();
                cmd.Parameters.Add("@EmailAddress", SqlDbType.NVarChar, 200).Value = Request.QueryString["EmailAddress"].ToString();
                cmd.Parameters.Add("@AmendedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"];
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            classes.Customer CUST = new classes.Customer();
            CUST.insertIntoAuditLog(Request.QueryString["CID"].ToString(), Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Beneficiary " + Request.QueryString["BeneFirstName"].ToString() + ' ' + Request.QueryString["BeneLastName"].ToString() + " has been edited");
        }

    }
}