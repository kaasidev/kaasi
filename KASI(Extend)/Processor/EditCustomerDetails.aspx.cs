﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace KASI_Extend_.Processor
{
    public partial class EditCustomerDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            String ID = Request.QueryString["CID"].ToString();

            String Message = UpdateCustomer(Request.QueryString["CID"].ToString(), Request.QueryString["lastname"].ToString(), Request.QueryString["firstnames"].ToString(), Request.QueryString["dob"].ToString(),
                Request.QueryString["addressline1"].ToString(), Request.QueryString["addressline2"].ToString(), Request.QueryString["suburb"].ToString(), Request.QueryString["state"].ToString(),
                Request.QueryString["postcode"].ToString(), Request.QueryString["homephone"].ToString(), Request.QueryString["workphone"].ToString(), Request.QueryString["mobile"].ToString(),
                Request.QueryString["email1"].ToString(), Request.QueryString["email2"].ToString(), Request.QueryString["notes"].ToString(), Request.QueryString["password"].ToString());

            if (Message.Equals("OK"))
            {
                Response.Write("Customer has been successfully added");
            }
            else
            {
                Response.Write("An error has occured.");
            }
        }

        public String UpdateCustomer(String CID, String lastname, String firstnames, String dob, String addressline1, String addressline2, String suburb, String state, String postcode, String homephone, String workphone, String mobile, String email1, String email2, String notes, String password)
        {
            String FullName = firstnames + ' ' + lastname;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SqlStmt = "UPDATE dbo.Customers SET FirstName=@FirstName, LastName=@LastName, AddressLine1=@AddressLine1, AddressLine2=@AddressLine2, Suburb=@Suburb, State=@State, Postcode=@Postcode, TelHome=@TelHome, TelWork=@TelWork, Mobile=@Mobile, EmailAddress=@EmailAddress, EmailAddress2=@EmailAddress2, FullName = @FullName, DOB = @DOB, notes = @notes, AccountPassword = @password, AmendedBy=@AmendedBy, AmendedDateTime=CURRENT_TIMESTAMP WHERE CustomerID = @CID";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@CID", SqlDbType.Int).Value = CID;
                cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = firstnames;
                cmd.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = lastname;
                cmd.Parameters.Add("@AddressLine1", SqlDbType.NVarChar, 100).Value = addressline1;
                cmd.Parameters.Add("@AddressLine2", SqlDbType.NVarChar, 100).Value = addressline2;
                cmd.Parameters.Add("@Suburb", SqlDbType.NVarChar, 100).Value = suburb;
                cmd.Parameters.Add("@State", SqlDbType.NVarChar, 100).Value = state;
                cmd.Parameters.Add("@Postcode", SqlDbType.NVarChar, 100).Value = postcode;
                cmd.Parameters.Add("@TelHome", SqlDbType.NVarChar, 100).Value = homephone;
                cmd.Parameters.Add("@TelWork", SqlDbType.NVarChar, 100).Value = workphone;
                cmd.Parameters.Add("@Mobile", SqlDbType.NVarChar, 100).Value = mobile;
                cmd.Parameters.Add("@DOB", SqlDbType.NVarChar, 100).Value = dob;
                cmd.Parameters.Add("@EmailAddress", SqlDbType.NVarChar, 100).Value = email1;
                cmd.Parameters.Add("@EmailAddress2", SqlDbType.NVarChar, 100).Value = email2;
                cmd.Parameters.Add("@FullName", SqlDbType.NVarChar, 250).Value = FullName;
                cmd.Parameters.Add("@notes", SqlDbType.NVarChar, 1000).Value = notes;
                cmd.Parameters.Add("@password", SqlDbType.NVarChar, 250).Value = password;
                cmd.Parameters.Add("@AmendedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"];
                cmd.ExecuteNonQuery();


                conn.Close();
            }

            classes.Customer CUST = new classes.Customer();
            CUST.insertIntoAuditLog(CID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Customer " + CID + " has been edited");

            return "OK";

        }
    }
}