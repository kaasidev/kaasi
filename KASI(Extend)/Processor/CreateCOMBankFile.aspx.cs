﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class CreateCOMBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions transClass = new classes.Transactions();
            String BankCode = Request.QueryString["BankCode"].ToString();
            String strDelimiter = ",";
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filename = "COM - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv";
            String now = String.Empty;
            now = DateTime.Now.ToString("yyyyMMdd");

            String cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            StringBuilder sb = new StringBuilder();

            sb.Append("REF_NO." + strDelimiter);
            sb.Append("ACCOUNT_NO." + strDelimiter);
            sb.Append("" + strDelimiter);
            sb.Append("CURRENCY" + strDelimiter);
            sb.Append("REM_TYPE" + strDelimiter);
            sb.Append("CHG_FROM" + strDelimiter);
            sb.Append("CUST_NAME" + strDelimiter);
            sb.Append("DETAILS" + strDelimiter);
            sb.Append("BENE_NAME" + strDelimiter);
            sb.Append("BENE_ADDRESS" + strDelimiter);
            sb.Append("BENE_TEL" + strDelimiter);
            sb.Append("BENE_ID_NO" + strDelimiter);
            sb.Append("BENE_ACCNO" + strDelimiter);
            sb.Append("BENE_BANK_CODE" + strDelimiter);
            sb.Append("BENE_BRANCH_CODE" + strDelimiter);
            sb.Append("BANK_TO_BANK_INFO" + strDelimiter);
            sb.Append("\r\n");

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = cs;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                sb.Append(sdr["TransactionID"].ToString() + strDelimiter);
                                sb.Append("1030008822" + strDelimiter);
                                sb.Append(sdr["RemittedAmount"].ToString().Replace(",", "") + strDelimiter);
                                sb.Append(sdr["RemittedCurrency"].ToString() + strDelimiter);
                                sb.Append("A" + strDelimiter);
                                sb.Append("B" + strDelimiter);
                                sb.Append(getCustomerName(sdr["CustomerID"].ToString()) + strDelimiter);
                                sb.Append("" + strDelimiter);
                                sb.Append(getBeneficiaryName(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                sb.Append(getBeneficiaryAddress(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                sb.Append(getBeneficiaryTel(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                sb.Append(getBeneficiaryID(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                sb.Append(getBeneficiaryAccountNumber(sdr["AccountID"].ToString()) + strDelimiter);
                                sb.Append("7056" + strDelimiter);
                                sb.Append("2" + strDelimiter);
                                sb.Append("" + strDelimiter);
                                sb.Append("\r\n");
                                TID.Add(sdr["TransactionID"].ToString());    
                            }
                        }
                    }
                    conn.Close();
                }
            }
            try
            {
                string filePath = Server.MapPath("/BankFileCreation") + "\\" + filename;
                StreamWriter file = new StreamWriter(filePath);
                file.WriteLine(sb.ToString());
                file.Close();
                filehasbeencomplete = true;
            }
            catch (Exception ex)
            {
                filehasbeencomplete = false;

            }

            if (filehasbeencomplete == true)
            {
                foreach (var Transaction in TID)
                {
                    transClass.updateTransBankFileInsertion(Transaction);
                    transClass.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into Commercial Bank Bank File");
                }

                Response.Write("COM File Successfully Created^" + filename);
            }
            else
            {
                Response.Write("COM File Could Not Be Created^nofile");
            }
            
        }

        private String getCustomerName(String CID)
        {

            String CustomerName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT FullName FROM dbo.Customers WHERE CustomerID = @CustomerID";
                    cmd.Parameters.AddWithValue("@CustomerID", CID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                CustomerName = sdr["FullName"].ToString();
                            }
                        }
                    }
                    conn.Close();
                }
            }

            return CustomerName;
        }

        private String getBeneficiaryName(String BID)
        {
            String BeneficiaryName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BeneficiaryName FROM dbo.Beneficiaries WHERE BeneficiaryID = @BeneficiaryID";
                    cmd.Parameters.AddWithValue("@BeneficiaryID", BID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                BeneficiaryName = sdr["BeneficiaryName"].ToString();
                            }
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryName;
        }

        private String getBeneficiaryAddress(String BID)
        {
            String BeneficiaryAddress = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AddressLine1 + ' ' + AddressLine2 + ' ' + Suburb + ' ' + State + ' ' + Postcode + ' ' + Country AS FullAddress FROM dbo.Beneficiaries WHERE BeneficiaryID = @BeneficiaryID";
                    cmd.Parameters.AddWithValue("@BeneficiaryID", BID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                BeneficiaryAddress = sdr["FullAddress"].ToString();
                            }
                        }
                    }
                    conn.Close();
                }
            }
            BeneficiaryAddress = BeneficiaryAddress.Replace(",", "");
            BeneficiaryAddress = BeneficiaryAddress.Replace("-- SELECT --", "");
            return BeneficiaryAddress;
        }

        private String getBeneficiaryTel(String BID)
        {
            String BeneficiaryTelephone = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TelHome FROM dbo.Beneficiaries WHERE BeneficiaryID = @BeneficiaryID";
                    cmd.Parameters.AddWithValue("@BeneficiaryID", BID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                BeneficiaryTelephone = sdr["TelHome"].ToString();
                            }
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryTelephone;
        }

        private String getBeneficiaryID(String BID)
        {
            String BeneficiaryID = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT NationalityCardID FROM dbo.Beneficiaries WHERE BeneficiaryID = @BeneficiaryID";
                    cmd.Parameters.AddWithValue("@BeneficiaryID", BID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                BeneficiaryID = sdr["NationalityCardID"].ToString();
                            }
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryID;
        }

        private String getBeneficiaryAccountNumber(String AID)
        {
            String AccountNumber = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AccountNumber FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = @AccountID";
                    cmd.Parameters.AddWithValue("@AccountID", AID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                AccountNumber = sdr["AccountNumber"].ToString();
                            }
                        }
                    }
                    conn.Close();
                }
            }

            return AccountNumber;
        }
    }
}