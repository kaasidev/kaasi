﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class saveAgentTransLimits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String MAUDLimit = Request.QueryString["MAUDL"];
            String YAUDLimit = Request.QueryString["YAUDL"];
            String MNBLimit = Request.QueryString["MNBL"];
            String YNBLimit = Request.QueryString["YNBL"];

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"UPDATE dbo.Agents SET MonthlyAUDLimit=@MonthlyAUDLimit, YearlyAUDsLimit=@YearlyAUDsLimit, MonthlyTransLimit=@MonthlyTransLimit,
                                YearlyTransLimit=@YearlyTransLimit WHERE AgentID = " + Session["AgentID"].ToString();

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@MonthlyAUDLimit", MAUDLimit);
                cmd.Parameters.AddWithValue("@YearlyAUDsLimit", YAUDLimit);
                cmd.Parameters.AddWithValue("@MonthlyTransLimit", MNBLimit);
                cmd.Parameters.AddWithValue("@YearlyTransLimit", YNBLimit);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}