﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class EditBeneficiaryAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            String AccountID = Request.QueryString["AID"].ToString();

            string SQLStatement = "UPDATE dbo.BeneficiaryPaymentMethods SET BankID=@BankID, BankName=@BankNAme, BranchID=@BranchID, BranchName=@BranchName, AccountNumber=@AccountNumber, AccountName=@AccountName, AccountType=@AccountType, Currency=@Currency, CountryID=@CountryID, CurrencyID=@CurrencyID, AmendedBy=@AmendedBy, AmendedDateTime=CURRENT_TIMESTAMP, AcctStatus=@AcctStatus WHERE AccountID = " + AccountID;

            using (SqlCommand cmd = new SqlCommand(SQLStatement))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@BankID", SqlDbType.NVarChar, 10).Value = Request.QueryString["BankID"].ToString();
                cmd.Parameters.Add("@BankName", SqlDbType.NVarChar, 100).Value = Request.QueryString["BankName"].ToString();
                cmd.Parameters.Add("@BranchID", SqlDbType.NVarChar, 10).Value = Request.QueryString["BranchID"].ToString();
                cmd.Parameters.Add("@BranchName", SqlDbType.NVarChar, 100).Value = Request.QueryString["BranchName"].ToString();
                cmd.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 30).Value = Request.QueryString["AccNum"].ToString();
                cmd.Parameters.Add("@AccountName", SqlDbType.NVarChar, 30).Value = Request.QueryString["AccName"].ToString();
                cmd.Parameters.Add("@AccountType", SqlDbType.NVarChar, 10).Value = Request.QueryString["CurrencyName"].ToString();
                cmd.Parameters.Add("@Currency", SqlDbType.NVarChar).Value = Request.QueryString["CurrencyName"].ToString();
                cmd.Parameters.Add("@CountryID", SqlDbType.NVarChar).Value = Request.QueryString["CountryID"].ToString();
                cmd.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = Request.QueryString["CurrencyID"].ToString();
                //cmd.Parameters.Add("@Active", SqlDbType.NVarChar, 3).Value = Request.QueryString["Act"].ToString();
                cmd.Parameters.Add("@AmendedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"];
                cmd.Parameters.Add("@AcctStatus", SqlDbType.NVarChar, 2).Value = Request.QueryString["ActStat"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            classes.Customer CUST = new classes.Customer();
            CUST.insertIntoAuditLog(Request.QueryString["CID"].ToString(), Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Account Number " + Request.QueryString["AccNum"].ToString() + " has been edited");
        }
    }
}