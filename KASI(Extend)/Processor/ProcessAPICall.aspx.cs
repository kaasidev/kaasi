﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;


namespace KASI_Extend.Processor
{
    public partial class ProcessAPICall : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int TID = Int32.Parse(Request.QueryString["TID"].ToString());

            TransactionService TransServ = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var TransDetails = TransServ.GetAll(x => x.TransactionID == TID, null, "").SingleOrDefault();

            var RoutingBank = TransDetails.RoutingBank;

            if (RoutingBank == "7287")
            {
                APICallouts.Sri_Lanka.Seylan.Transactions.TransactionAPI TAPI = new APICallouts.Sri_Lanka.Seylan.Transactions.TransactionAPI();
                TAPI.TransactionCallOut(TransDetails.TransactionID.ToString());
            }
            else if (RoutingBank == "7278")
            {
                APICallouts.Sri_Lanka.Sampath.Transactions.TransactionAPI SAPI = new APICallouts.Sri_Lanka.Sampath.Transactions.TransactionAPI();
                SAPI.processTransaction(TransDetails.TransactionID.ToString());
            }
            else if (RoutingBank == "7719")
            {
                APICallouts.Sri_Lanka.NSB.Transactions.TransactionAPI NSBAPI = new APICallouts.Sri_Lanka.NSB.Transactions.TransactionAPI();
                NSBAPI.processTransaction(TransDetails.TransactionID.ToString());
            }
            else if (RoutingBank == "7454")
            {
                APICallouts.Sri_Lanka.DFCC.AddTransaction DFCCAPI = new APICallouts.Sri_Lanka.DFCC.AddTransaction();
                DFCCAPI.callAPI();

            }
        }
    }
}