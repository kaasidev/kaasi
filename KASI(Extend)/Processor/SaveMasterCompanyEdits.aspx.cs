﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class SaveMasterCompanyEdits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SaveAddressLine1(Request.QueryString["Info1"]);
            SaveAddressLine2(Request.QueryString["Info2"]);
            SaveSuburb(Request.QueryString["Info3"]);
            SaveState(Request.QueryString["Info4"]);
            SavePostcode(Request.QueryString["Info5"]);
            SaveLandline(Request.QueryString["Info6"]);
            SaveMobile(Request.QueryString["Info7"]);
            SaveEmail(Request.QueryString["Info8"]);
            SaveABN(Request.QueryString["Info9"]);
            SaveACN(Request.QueryString["Info10"]);
          
        }

        private void SaveAddressLine1(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "SET DATEFORMAT dmy UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyAddressLine1'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        private void SaveAusRegNum(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "SET DATEFORMAT dmy UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'AustracRegNum'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        private void SaveAusRegDate(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "SET DATEFORMAT dmy UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'AustracRegDate'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        private void SaveAusRegExpDate(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "SET DATEFORMAT dmy UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'AustracExpDate'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveAddressLine2(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyAddressLine2'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveSuburb(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanySuburb'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveState(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyState'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SavePostcode(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyPostcode'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveLandline(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyPhone'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveMobile(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyFax'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveEmail(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyEmail'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveABN(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyABN'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void SaveACN(String Info)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable = @SettingsValue WHERE SettingName = 'MasterCompanyACN'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SettingsValue", SqlDbType.NVarChar, 100).Value = Info;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}