﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class performKYCUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String MS = Request.QueryString["MS"].ToString();
            String FD = Request.QueryString["FD"].ToString();
            String AURes = Request.QueryString["AURes"].ToString();
            String RS = Request.QueryString["RS"].ToString();
            String ES = Request.QueryString["ES"].ToString();
            String OC = Request.QueryString["OC"].ToString();
            String SL = Request.QueryString["SL"].ToString();
            String FR = Request.QueryString["FR"].ToString();
            String OI = Request.QueryString["OI"].ToString();
            String CM = Request.QueryString["CM"].ToString();

            String hasKYC = checkIfKYCAlreadyPresent(CID);

            if (hasKYC == "N")
            {
                AddNewKYC(CID, MS, FD, AURes, RS, ES, OC, SL, FR, OI, CM);
            }
            else
            {
                EditKYC(CID, MS, FD, AURes, RS, ES, OC, SL, FR, OI, CM);
            }
        }

        protected String checkIfKYCAlreadyPresent(String CID)
        {
            String KYCPresent = String.Empty;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM dbo.KYCInfo WHERE CustomerID = " + CID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            KYCPresent = "Y";
                        }
                        else
                        {
                            KYCPresent = "N";
                        }
                    }
                    conn.Close();
                }
            }

            return KYCPresent;
        }

        protected void AddNewKYC(String CID, String MS, String FD, String AusRes, String RS, String ES, String OC, String SL, String FR, String OI, String CMD)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

           

            String SQLStmt = "INSERT INTO dbo.KYCInfo (CustomerID, MaritalStatus, FinancialDependants, PermanentResident, ResidentialStatus, EmploymentStatus, Occupation, Salary, Frequency, OtherIncomes, Comments, CreatedBy, CreatedDateTime, ExpiryDate, AmendedBy, AmendedDateTime) VALUES (@CustomerID, @MaritalStatus, @FinancialDependants, @PermanentResident, @ResidentialStatus, @EmploymentStatus, @Occupation, @Salary, @Frequency, @OtherIncomes, @Comments, @CreatedBy, CURRENT_TIMESTAMP, DATEADD(year, 1, GETDATE()), @AmendedBy, CURRENT_TIMESTAMP)";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 10).Value = CID;
                cmd.Parameters.Add("@MaritalStatus", SqlDbType.NVarChar, 50).Value = MS;
                cmd.Parameters.Add("@FinancialDependants", SqlDbType.NVarChar, 100).Value = FD;
                cmd.Parameters.Add("@PermanentResident", SqlDbType.NVarChar, 50).Value = AusRes;
                cmd.Parameters.Add("@ResidentialStatus", SqlDbType.NVarChar, 100).Value = RS;
                cmd.Parameters.Add("@EmploymentStatus", SqlDbType.NVarChar, 100).Value = ES;
                cmd.Parameters.Add("@Occupation", SqlDbType.NVarChar, 250).Value = OC;
                if (SL == "")
                {
                    cmd.Parameters.Add("@Salary", SqlDbType.Money).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Salary", SqlDbType.Money).Value = SL;
                }
                cmd.Parameters.Add("@Frequency", SqlDbType.NVarChar, 50).Value = FR;
                cmd.Parameters.Add("@OtherIncomes", SqlDbType.NVarChar, 100).Value = OI;
                cmd.Parameters.Add("@Comments", SqlDbType.NVarChar).Value = CMD;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = "SYSTEM";
                cmd.Parameters.Add("@AmendedBy", SqlDbType.NVarChar, 100).Value = "SYSTEM";
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            Customer cust = new Customer();
            int CustRiskLevel = Int32.Parse(cust.getCustomerRiskLevel(CID));

            int newRiskLevel = CustRiskLevel - 1;

            String RLStmt = "UPDATE dbo.Customers SET RiskLevel = " + newRiskLevel + " WHERE CustomerID = " + CID;

            using (SqlCommand cmd = new SqlCommand(RLStmt))
            {
                cmd.Connection = conn;
                conn.Open();

                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void EditKYC(String CID, String MS, String FD, String AusRes, String RS, String ES, String OC, String SL, String FR, String OI, String CMD)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            String SQLStmt = "UPDATE dbo.KYCInfo SET MaritalStatus=@MaritalStatus, FinancialDependants=@FinancialDependants, PermanentResident=@PermanentResident, ResidentialStatus=@ResidentialStatus, EmploymentStatus=@EmploymentStatus, Occupation=@Occupation, Salary=@Salary, Frequency=@Frequency, OtherIncomes=@OtherIncomes, Comments=@Comments, AmendedBy=@AmendedBy, AmendedDateTime=CURRENT_TIMESTAMP, ExpiryDate=DATEADD(year, 1, AmendedDateTime) WHERE CustomerID = " + CID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@MaritalStatus", SqlDbType.NVarChar, 50).Value = MS;
                cmd.Parameters.Add("@FinancialDependants", SqlDbType.NVarChar, 100).Value = FD;
                cmd.Parameters.Add("@PermanentResident", SqlDbType.NVarChar, 50).Value = AusRes;
                cmd.Parameters.Add("@ResidentialStatus", SqlDbType.NVarChar, 100).Value = RS;
                cmd.Parameters.Add("@EmploymentStatus", SqlDbType.NVarChar, 100).Value = ES;
                cmd.Parameters.Add("@Occupation", SqlDbType.NVarChar, 250).Value = OC;
                if (SL == "")
                {
                    cmd.Parameters.Add("@Salary", SqlDbType.Money).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Salary", SqlDbType.Money).Value = SL;
                }
                
                cmd.Parameters.Add("@Frequency", SqlDbType.NVarChar, 50).Value = FR;
                cmd.Parameters.Add("@OtherIncomes", SqlDbType.NVarChar, 100).Value = OI;
                cmd.Parameters.Add("@Comments", SqlDbType.NVarChar).Value = CMD;
                cmd.Parameters.Add("@AmendedBy", SqlDbType.NVarChar, 100).Value = "SYSTEM";
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}