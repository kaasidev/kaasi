﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class createDFCCBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions TRANS = new classes.Transactions();
            classes.Customer CUST = new classes.Customer();
            classes.Beneficiary BENE = new classes.Beneficiary();
            classes.BeneficiaryAccounts BENEACC = new classes.BeneficiaryAccounts();
            Boolean filehasbeencomplete = false;
            String filename = "DFCC - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv";

            String AID = Request.QueryString["AID"].ToString();
            String BID = Request.QueryString["BID"].ToString();
            String output = String.Empty;
            String BankType = String.Empty;

            List<String> TransactionList = new List<String>();
            TransactionList = TRANS.getAwaitingTransferTransactionsForParticularBank(AID, BID);

            foreach(var TransactionID in TransactionList)
            {
                String CID = TRANS.getCustomerIDFromTransactionID(TransactionID);
                String BeneID = TRANS.getTransactionBeneficiary(TransactionID);
                output += TransactionID + "~"; // Transaction ID
                output += CUST.getCustomerFullName(CID) + "~"; // Customer's Full Name
                output += CUST.getCustomerMobile(CID) + "~"; // Customer's Mobile
                output += TRANS.getTransactionPurpose(TransactionID) + "~"; // Transaction Purpose
                output += BENE.getBeneficiaryName(BeneID) + "~"; // Beneficiary Full Name
                output += BENE.getBeneficiaryNIC(BeneID) + "~"; // Beneficiary NIC
                output += BENE.getBeneficiaryFullAddress(BeneID) + "~"; // Beneficiary Full Address
                output += BENE.getBeneficiaryMobile(BeneID) + "~"; // Beneficiary Mobile
                output += BENE.getBeneficiaryPhone(BeneID) + "~"; //Beneficiary Other Contact Number
                output += "~"; // Beneficairy Notes about transaction
                output += TRANS.getReceivedAmountFromTransaction(TransactionID) + "~"; // Remitted Amount in Local Currency
                output += TRANS.getRemittedCurrency(TransactionID) + "~"; // Remitted Currency

                String RBank = TRANS.getRoutingBankCodeForTransaction(TransactionID);

                if (RBank == "7454")
                {
                    BankType = "2";
                }
                else
                {
                    BankType = "3";
                }

                output += BankType + "~";
                output += BENEACC.getBeneficiaryBankCodeByBID(BeneID) + "~";
                output += BENEACC.getBeneficiaryBranchCodeByBID(BeneID) + "~";
                output += BENEACC.getBeneficiaryAccountNumberOnTID(TransactionID);
            }

            try
            {
                string filePath = Server.MapPath("/BankFileCreation") + "\\" + filename;
                System.IO.File.WriteAllText(filePath, output);
                filehasbeencomplete = true;
            }
            catch (Exception ex)
            {
                filehasbeencomplete = false;

            }

            if (filehasbeencomplete == true)
            {
                foreach (var Transaction in TransactionList)
                {
                    TRANS.updateTransBankFileInsertion(Transaction);
                    TRANS.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into DFCC Bank File");
                }
                Response.Write("DFCC File Successfully Created^" + filename);
            }
            else
            {
                Response.Write("DFCC File Could Not Be Created^noreport");
            }

        }
    }
}