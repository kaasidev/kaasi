﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Kapruka.Service;
using Kapruka.Domain;
using Kapruka.Enterprise;

namespace KASI_Extend_.Processor
{
    public partial class AddNewBusiness : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Kapruka.Repository.Customer customer = null;
                CustomerService ser = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                Customer cs = new Customer();
                String LastCustomerID = cs.getLastCustomerID();
                int LastID = 0;
                if (!String.IsNullOrEmpty(LastCustomerID))
                {
                    LastID = Int32.Parse(LastCustomerID);
                }

                
                LastID = LastID + 1;
                string type = Request.Form["type"];
                string btyp = Request.Form["isbusi"];

                if (type == "new")
                {
                    customer = new Kapruka.Repository.Customer();
                }
                else
                {
                    LastID = int.Parse(Request.Form["hidCustId"]);
                    customer = ser.GetAll(x => x.CustomerID == LastID, null, "").SingleOrDefault();
                }

                customer.CustomerID = LastID;
                if(btyp=="busi")
                {
                   
                    customer.BusinessName = Request.Form["BBusinessName"];
                    customer.ABN = Request.Form["BABN"];
                    customer.TradingName = Request.Form["BTradingName"];
                    customer.BusinessContact = Request.Form["BBContact"];
                    customer.BusinessEmail = Request.Form["BBEmail"];
                    customer.BusinessWebSite = Request.Form["BBWeb"];
                    customer.AddressLine1 = Request.Form["BAddressLine1"];
                    customer.PostalAddressAddressLine2 = Request.Form["BPostalAddressLine2"];
                    customer.PostalAddressUnitNo = Request.Form["pos_unitno"];
                    customer.PostalAddressStreetNo = Request.Form["pos_streetno"];
                    customer.PostalAddressStreetName = Request.Form["pos_streetname"];
                    customer.PostalAddressStreetType = Request.Form["pos_streettype"];
                    customer.PostalAddressSuburb = Request.Form["BPostalSuburb"];
                    if(Request.Form["BPostalState"] !=null)
                    {
                        customer.PostalAddressState = Request.Form["BPostalState"].ToUpper();
                    }
                    
                    customer.PostalAddressPostcode = Request.Form["BPostalPostcode"];
                    customer.PostalAddressCountry = Request.Form["pos_country"];

                    customer.PrincipleUNitNo = Request.Form["bus_unitno"];
                    customer.PrincipleStreetNo = Request.Form["bus_streetno"];
                    customer.PrincipleStreetName = Request.Form["bus_streetname"];
                    customer.PrincipleStreetType = Request.Form["bus_streettype"];
                    customer.PrincipleStreetSuburb = Request.Form["bus_suburb"];
                    if(Request.Form["bus_state"] !=null)
                    {
                        customer.PrincipleStreetState = Request.Form["bus_state"].ToUpper();
                    }
                   
                    customer.PrincipleStreetPosalCode = Request.Form["bus_postcode"];
                    customer.PrincipleCountry = Request.Form["bus_country"];

                }
               

                customer.UnitNo = Request.Form["pbus_unitno"];
                customer.StreetNo = Request.Form["pbus_streetno"];
                customer.StreetName = Request.Form["pbus_streetname"];
                customer.StreetType = Request.Form["pbus_streettype"];
                customer.AddressLine2 = Request.Form["pbus_addressline2"];
                customer.Suburb = Request.Form["ppbus_suburb"];
                if(Request.Form["ppbus_state"] !=null)
                {
                    customer.State = Request.Form["ppbus_state"].ToUpper();
                }
               
                customer.Postcode = Request.Form["ppbus_postcode"];
                customer.Country = Request.Form["pbus_country"];
                customer.CountryOfBirth = Request.Form["bcountry"];
               
                customer.FullName= Request.Form["bus_givenNames"] +" "+ Request.Form["bus_lastname"];
                customer.LastName = Request.Form["bus_lastname"];
                customer.Title = Request.Form["bus_title"];
                customer.FirstName = Request.Form["bus_givenNames"];
                customer.DOB = Request.Form["bus_dob"];
                customer.PlaceOfBirth = Request.Form["bus_placeofbirth"];
                customer.Nationality = Request.Form["bnationality"];
                customer.AKA = Request.Form["bus_aka"];
                customer.Occupation = Request.Form["bus_occupation"];
                customer.TelWork = Request.Form["pbus_phonework"];
                customer.TelHome= Request.Form["pbus_phonehome"];
                customer.Mobile = Request.Form["pbus_phonemobile"];
                customer.EmailAddress = Request.Form["pbus_email1"];
                customer.EmailAddress2 = Request.Form["pbus_email2"];
               
                customer.TelHome = Request.Form["pbus_phonehome"];
                customer.AccountPassword = Request.Form["passwordedit"];
                customer.Notes = Request.Form["TXT_Add_TRN_Notes"];

                if (type == "new")
                {
                    customer.AgentID = int.Parse(Request.Form["ctl00$ContentMainSection$hidden_agentid"]);
                    customer.CustomerType = (int)CustomerType.Merchant;
                    customer.HasKYC = CustomerType.Merchant.ToString();
                    customer.CreatedDateTime = System.DateTime.Now;
                    if (btyp=="busi")
                    {
                        customer.IsIndividual = false;
                    }
                    else
                    {
                        customer.IsIndividual = true;
                    }
                    ser.Add(customer);
                }
                else
                {
                    ser.Update(customer);
                }
                //var result = addCustomer(LastID, ,, ,
                //    , , ,
                //    , , Request.Form["pstreettype"], Request.Form["BPostalAddressLine2"],
                //    Request.Form["pstate"], Request.Form["psuburb"], Request.Form["ppostcode"], Request.Form["pcountry"],
                //    Request.Form["lastname"], Request.Form["firstnames"], Request.Form["dob"], Request.Form["unitno"],
                //    Request.Form["streetno"], Request.Form["streetname"], Request.Form["streettype"], Request.Form["suburb"],
                //    Request.Form["state"], Request.Form["postcode"], Request.Form["homephone"], Request.Form["workphone"],
                //    Request.Form["mobile"], Request.Form["email1"], Request.Form["email2"], Request.Form["hidagentid"], Request.Form["busicontact"], Request.Form["busiemail"], Request.Form["busiweb"], Request.Form["title"]);
                Response.Write("Customer has been successfully added.");
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
                throw;
            }
            //if (result)
            //{
            //    var objKyced = new CustomerKyced();
            //    objKyced.dob = Request.Form["dob"];
            //    objKyced.bfn = Request.Form["firstnames"];
            //    objKyced.bln = Request.Form["lastname"];
            //    objKyced.man = Request.Form["email1"];

            //    //var customerService = new CustomerHandlerService();
            //    //var recordId = customerService.GetRecordIdBYCustomerId(LastID);
            //    //var customerApiMessage = "Webapi successfully called.";
            //    //var customerApiResult = new CustomerHandlerService().CreateKycedCustomer(objKyced, LastID, recordId);
            //    //if (customerApiResult == false)
            //    //    customerApiMessage = "An error has occured while calling Webapi";
            //    Response.Write("Customer has been successfully added.");
            //}
            
        }
            
         
            
        
        //else
        //{
        //    string custId = Request.Form["hidagentid"];

        //    var result = modifyCustomer(custId, Request.Form["businessName"], Request.Form["ABN"], Request.Form["TradingName"],
        //        Request.Form["BAddressLine2"], Request.Form["country"], Request.Form["punitno"],
        //        Request.Form["pstreetno"], Request.Form["pstreetname"], Request.Form["pstreettype"], Request.Form["BPostalAddressLine2"],
        //        Request.Form["pstate"], Request.Form["psuburb"], Request.Form["ppostcode"], Request.Form["pcountry"],
        //        Request.Form["lastname"], Request.Form["firstnames"], Request.Form["dob"], Request.Form["unitno"],
        //        Request.Form["streetno"], Request.Form["streetname"], Request.Form["streettype"], Request.Form["suburb"],
        //        Request.Form["state"], Request.Form["postcode"], Request.Form["homephone"], Request.Form["workphone"],
        //        Request.Form["mobile"], Request.Form["email1"], Request.Form["email2"], Request.Form["hidagentid"], Request.Form["busicontact"], Request.Form["busiemail"], Request.Form["busiweb"], Request.Form["title"]);

        //    if (result)
        //    {
        //        var objKyced = new CustomerKyced();
        //        objKyced.dob = Request.Form["dob"];
        //        objKyced.bfn = Request.Form["firstnames"];
        //        objKyced.bln = Request.Form["lastname"];
        //        objKyced.man = Request.Form["email1"];

        //        //var customerService = new CustomerHandlerService();
        //        //var recordId = customerService.GetRecordIdBYCustomerId(LastID);
        //        //var customerApiMessage = "Webapi successfully called.";
        //        //var customerApiResult = new CustomerHandlerService().CreateKycedCustomer(objKyced, LastID, recordId);
        //        //if (customerApiResult == false)
        //        //    customerApiMessage = "An error has occured while calling Webapi";
        //        Response.Write("Customer has been successfully updated.");
        //    }
        //    else
        //    {
        //        Response.Write("An error has occured.");
        //    }
        //}
    
        

        //public bool addCustomer(int LastID, String businessName, String abn, 
        //    String tradingName, String addressLine2, String businessCountry, String pUnitNo,
        //    String pStreetNo, String pStreetName,  String sStreetType, String pAddressLine2, 
        //    String pState, String pSuburb, String pPostcode, String pCountry, String lastname, 
        //    String firstnames, String dob, String unitno, String streetno, String streetname,
        //    String streetype, String suburb, String state, String postcode, String homephone, 
        //    String workphone, String mobile, String email1, String email2, String AID,string businessContact,string businessEMail,string businessWeb,string title)
        //{
        //    String FullName = firstnames + ' ' + lastname;

        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

        //    string SqlStmt = @"SET DATEFORMAT dmy INSERT INTO dbo.Customers (CustomerID, BusinessName, ABN, TradingName, UnitNo, StreetNo, StreetName, 
        //    StreetType, Suburb, State, Postcode, Country, PostalAddressUnitNo, PostalAddressStreetNo, PostalAddressStreetName, 
        //    PostalAddressStreetType, PostalAddressAddressLine2, PostalAddressSuburb, PostalAddressState, 
        //    PostalAddressPostcode, PostalAddressCountry, LastName, DOB, FirstName, TelHome, 
        //    TelWork, Mobile, EmailAddress, EmailAddress2, AgentID, SpendingPower,CustomerType,HasKYC,BusinessContact,BusinessEmail,BusinessWebSite,IsIndividual,Title) 
        //    VALUES (@CustomerID, @BusinessName, @ABN, @TradingName, @UnitNo, @StreetNo, 
        //        @StreetName, @StreetType, @Suburb, @State, @Postcode, @Country, @PostalAddressUnitNo,
        //        @PostalAddressStreetNo, @PostalAddressStreetName, @PostalAddressStreetType, 
        //        @PostalAddressAddressLine2, @PostalAddressSuburb, @PostalAddressState, 
        //        @PostalAddressPostcode, @PostalAddressCountry, @LastName, @DOB, @FirstName,
        //        @TelHome, @TelWork, @Mobile, @EmailAddress, @EmailAddress2, @AgentID, @SpendingPower,@CustomerType,@HasKYC,@BusinessContact,@BusinessEmail,@BusinessWebSite,@IsIndividual,@Title)";
        //    try
        //    {
        //        using (SqlCommand cmd = new SqlCommand(SqlStmt))
        //        {
        //            cmd.Connection = conn;
        //            conn.Open();
        //            var customerType = (int)CustomerType.Merchant;
        //            var hasRecordInKyce = (int)CustomerINKYCE.No;

        //            cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 6).Value = LastID;
        //            cmd.Parameters.Add(new SqlParameter("@BusinessName", String.IsNullOrEmpty(businessName) ? (object)DBNull.Value : businessName));
        //            cmd.Parameters.Add(new SqlParameter("@ABN", String.IsNullOrEmpty(abn) ? (object)DBNull.Value : abn));
        //            cmd.Parameters.Add(new SqlParameter("@TradingName", String.IsNullOrEmpty(tradingName) ? (object)DBNull.Value : tradingName));
        //            cmd.Parameters.Add(new SqlParameter("@UnitNo", String.IsNullOrEmpty(unitno) ? (object)DBNull.Value : unitno));
        //            cmd.Parameters.Add(new SqlParameter("@StreetNo", String.IsNullOrEmpty(streetno) ? (object)DBNull.Value : streetno));
        //            cmd.Parameters.Add(new SqlParameter("@StreetName", String.IsNullOrEmpty(streetname) ? (object)DBNull.Value : streetname));
        //            cmd.Parameters.Add(new SqlParameter("@StreetType", String.IsNullOrEmpty(streetype) ? (object)DBNull.Value : streetype));
        //            //cmd.Parameters.Add(new SqlParameter("@AddressLine2", String.IsNullOrEmpty(addressLine2) ? (object)DBNull.Value : addressLine2));
        //            cmd.Parameters.Add(new SqlParameter("@State", String.IsNullOrEmpty(state) ? (object)DBNull.Value : state.ToUpper()));     
        //            cmd.Parameters.Add(new SqlParameter("@Suburb", String.IsNullOrEmpty(suburb) ? (object)DBNull.Value : suburb));
        //            cmd.Parameters.Add(new SqlParameter("@Postcode", String.IsNullOrEmpty(postcode) ? (object)DBNull.Value : postcode));
        //            cmd.Parameters.Add(new SqlParameter("@Country", String.IsNullOrEmpty(businessCountry) ? (object)DBNull.Value : businessCountry));

        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressUnitNo", String.IsNullOrEmpty(pUnitNo) ? (object)DBNull.Value : pUnitNo));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressStreetNo", String.IsNullOrEmpty(pStreetNo) ? (object)DBNull.Value : pStreetNo));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressStreetName", String.IsNullOrEmpty(pStreetName) ? (object)DBNull.Value : pStreetName));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressStreetType", String.IsNullOrEmpty(sStreetType) ? (object)DBNull.Value : sStreetType));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressAddressLine2", String.IsNullOrEmpty(pAddressLine2) ? (object)DBNull.Value : pAddressLine2));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressState", String.IsNullOrEmpty(pState) ? (object)DBNull.Value : pState.ToUpper()));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressSuburb", String.IsNullOrEmpty(pSuburb) ? (object)DBNull.Value : pSuburb));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressPostcode", String.IsNullOrEmpty(pPostcode) ? (object)DBNull.Value : pPostcode));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressCountry", String.IsNullOrEmpty(pCountry) ? (object)DBNull.Value : pCountry));

        //            cmd.Parameters.Add(new SqlParameter("@LastName", String.IsNullOrEmpty(firstnames) ? (object)DBNull.Value : lastname));
        //            cmd.Parameters.Add(new SqlParameter("@DOB", String.IsNullOrEmpty(dob) ? (object)DBNull.Value : dob));
        //            cmd.Parameters.Add(new SqlParameter("@FirstName", String.IsNullOrEmpty(lastname) ? (object)DBNull.Value : firstnames));

        //            cmd.Parameters.Add(new SqlParameter("@TelHome", String.IsNullOrEmpty(homephone) ? (object)DBNull.Value : homephone));
        //            cmd.Parameters.Add(new SqlParameter("@TelWork", String.IsNullOrEmpty(workphone) ? (object)DBNull.Value : workphone));
        //            cmd.Parameters.Add(new SqlParameter("@Mobile", String.IsNullOrEmpty(mobile) ? (object)DBNull.Value : mobile));
                    
        //            cmd.Parameters.Add(new SqlParameter("@EmailAddress", String.IsNullOrEmpty(email1) ? (object)DBNull.Value : email1));
        //            cmd.Parameters.Add(new SqlParameter("@EmailAddress2", String.IsNullOrEmpty(email2) ? (object)DBNull.Value : email2));
        //            cmd.Parameters.Add(new SqlParameter("@FullName", String.IsNullOrEmpty(FullName) ? (object)DBNull.Value : FullName));
        //            cmd.Parameters.Add(new SqlParameter("@AgentID", String.IsNullOrEmpty(AID) ? (object)DBNull.Value : AID));
        //            cmd.Parameters.Add(new SqlParameter("@SpendingPower", getDefaultMonthlyLimit()));
        //            cmd.Parameters.Add(new SqlParameter("@CustomerType", customerType));
        //            cmd.Parameters.Add(new SqlParameter("@HasKYC", hasRecordInKyce));
        //            cmd.Parameters.Add(new SqlParameter("@BusinessContact", businessContact));
        //            cmd.Parameters.Add(new SqlParameter("@BusinessEmail", businessEMail));
        //            cmd.Parameters.Add(new SqlParameter("@BusinessWebSite", businessWeb));
        //            cmd.Parameters.Add(new SqlParameter("@Title", title));
        //            cmd.Parameters.Add(new SqlParameter("@IsIndividual", "0"));
        //            cmd.ExecuteNonQuery();


        //            conn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        conn.Close();
        //        return false;
        //    }


        //    return true;

        //}

        //public bool modifyCustomer(string LastID, String businessName, String abn,
        //  String tradingName, String addressLine2, String businessCountry, String pUnitNo,
        //  String pStreetNo, String pStreetName, String sStreetType, String pAddressLine2,
        //  String pState, String pSuburb, String pPostcode, String pCountry, String lastname,
        //  String firstnames, String dob, String unitno, String streetno, String streetname,
        //  String streetype, String suburb, String state, String postcode, String homephone,
        //  String workphone, String mobile, String email1, String email2, String AID, string businessContact, string businessEMail, string businessWeb, string title)
        //{
        //    String FullName = firstnames + ' ' + lastname;

        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

        //    string SqlStmt = @"SET DATEFORMAT dmy UPDATE dbo.Customers set  BusinessName=@BusinessName,
        //    ABN=@ABN, TradingName=@TradingName, UnitNo=@UnitNo, StreetNo=@StreetNo, StreetName=@StreetName, 
        //    StreetType=@StreetType, Suburb=@Suburb, State=@State, Postcode=@Postcode, Country=@Country, 
        //    PostalAddressUnitNo=@PostalAddressUnitNo, PostalAddressStreetNo=@PostalAddressStreetNo, PostalAddressStreetName=@PostalAddressStreetName, 
        //    PostalAddressStreetType=@PostalAddressStreetType, PostalAddressAddressLine2=@PostalAddressAddressLine2, 
        //    PostalAddressSuburb=@PostalAddressSuburb, PostalAddressState=@PostalAddressState, 
        //    PostalAddressPostcode=@PostalAddressPostcode, PostalAddressCountry=@PostalAddressCountry, 
        //    LastName=@LastName, DOB=@DOB, FirstName=@FirstName, TelHome=@TelHome, 
        //    TelWork=@TelWork, Mobile=@Mobile, EmailAddress=@EmailAddress, EmailAddress2=@EmailAddress2, 
        //    SpendingPower=@SpendingPower,CustomerType=@CustomerType,HasKYC=@HasKYC,
        //    BusinessContact=@BusinessContact,BusinessEmail=@BusinessEmail,BusinessWebSite=@BusinessWebSite,IsIndividual=@IsIndividual,Title=@Title where CustomerID="+LastID;
        //    try
        //    {
        //        using (SqlCommand cmd = new SqlCommand(SqlStmt))
        //        {
        //            cmd.Connection = conn;
        //            conn.Open();
        //            var customerType = (int)CustomerType.Merchant;
        //            var hasRecordInKyce = (int)CustomerINKYCE.No;

                    
        //            cmd.Parameters.Add(new SqlParameter("@BusinessName", String.IsNullOrEmpty(businessName) ? (object)DBNull.Value : businessName));
        //            cmd.Parameters.Add(new SqlParameter("@ABN", String.IsNullOrEmpty(abn) ? (object)DBNull.Value : abn));
        //            cmd.Parameters.Add(new SqlParameter("@TradingName", String.IsNullOrEmpty(tradingName) ? (object)DBNull.Value : tradingName));
        //            cmd.Parameters.Add(new SqlParameter("@UnitNo", String.IsNullOrEmpty(unitno) ? (object)DBNull.Value : unitno));
        //            cmd.Parameters.Add(new SqlParameter("@StreetNo", String.IsNullOrEmpty(streetno) ? (object)DBNull.Value : streetno));
        //            cmd.Parameters.Add(new SqlParameter("@StreetName", String.IsNullOrEmpty(streetname) ? (object)DBNull.Value : streetname));
        //            cmd.Parameters.Add(new SqlParameter("@StreetType", String.IsNullOrEmpty(streetype) ? (object)DBNull.Value : streetype));
        //            //cmd.Parameters.Add(new SqlParameter("@AddressLine2", String.IsNullOrEmpty(addressLine2) ? (object)DBNull.Value : addressLine2));
        //            cmd.Parameters.Add(new SqlParameter("@State", String.IsNullOrEmpty(state) ? (object)DBNull.Value : state.ToUpper()));
        //            cmd.Parameters.Add(new SqlParameter("@Suburb", String.IsNullOrEmpty(suburb) ? (object)DBNull.Value : suburb));
        //            cmd.Parameters.Add(new SqlParameter("@Postcode", String.IsNullOrEmpty(postcode) ? (object)DBNull.Value : postcode));
        //            cmd.Parameters.Add(new SqlParameter("@Country", String.IsNullOrEmpty(businessCountry) ? (object)DBNull.Value : businessCountry));

        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressUnitNo", String.IsNullOrEmpty(pUnitNo) ? (object)DBNull.Value : pUnitNo));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressStreetNo", String.IsNullOrEmpty(pStreetNo) ? (object)DBNull.Value : pStreetNo));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressStreetName", String.IsNullOrEmpty(pStreetName) ? (object)DBNull.Value : pStreetName));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressStreetType", String.IsNullOrEmpty(sStreetType) ? (object)DBNull.Value : sStreetType));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressAddressLine2", String.IsNullOrEmpty(pAddressLine2) ? (object)DBNull.Value : pAddressLine2));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressState", String.IsNullOrEmpty(pState) ? (object)DBNull.Value : pState.ToUpper()));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressSuburb", String.IsNullOrEmpty(pSuburb) ? (object)DBNull.Value : pSuburb));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressPostcode", String.IsNullOrEmpty(pPostcode) ? (object)DBNull.Value : pPostcode));
        //            cmd.Parameters.Add(new SqlParameter("@PostalAddressCountry", String.IsNullOrEmpty(pCountry) ? (object)DBNull.Value : pCountry));

        //            cmd.Parameters.Add(new SqlParameter("@LastName", String.IsNullOrEmpty(firstnames) ? (object)DBNull.Value : lastname));
        //            cmd.Parameters.Add(new SqlParameter("@DOB", String.IsNullOrEmpty(dob) ? (object)DBNull.Value : dob));
        //            cmd.Parameters.Add(new SqlParameter("@FirstName", String.IsNullOrEmpty(lastname) ? (object)DBNull.Value : firstnames));

        //            cmd.Parameters.Add(new SqlParameter("@TelHome", String.IsNullOrEmpty(homephone) ? (object)DBNull.Value : homephone));
        //            cmd.Parameters.Add(new SqlParameter("@TelWork", String.IsNullOrEmpty(workphone) ? (object)DBNull.Value : workphone));
        //            cmd.Parameters.Add(new SqlParameter("@Mobile", String.IsNullOrEmpty(mobile) ? (object)DBNull.Value : mobile));

        //            cmd.Parameters.Add(new SqlParameter("@EmailAddress", String.IsNullOrEmpty(email1) ? (object)DBNull.Value : email1));
        //            cmd.Parameters.Add(new SqlParameter("@EmailAddress2", String.IsNullOrEmpty(email2) ? (object)DBNull.Value : email2));
        //            cmd.Parameters.Add(new SqlParameter("@FullName", String.IsNullOrEmpty(FullName) ? (object)DBNull.Value : FullName));
        //           // cmd.Parameters.Add(new SqlParameter("@AgentID", String.IsNullOrEmpty(AID) ? (object)DBNull.Value : AID));
        //            cmd.Parameters.Add(new SqlParameter("@SpendingPower", getDefaultMonthlyLimit()));
        //            cmd.Parameters.Add(new SqlParameter("@CustomerType", customerType));
        //            cmd.Parameters.Add(new SqlParameter("@HasKYC", hasRecordInKyce));
        //            cmd.Parameters.Add(new SqlParameter("@BusinessContact", businessContact));
        //            cmd.Parameters.Add(new SqlParameter("@BusinessEmail", businessEMail));
        //            cmd.Parameters.Add(new SqlParameter("@BusinessWebSite", businessWeb));
        //            cmd.Parameters.Add(new SqlParameter("@Title", title));
        //            cmd.Parameters.Add(new SqlParameter("@IsIndividual", "0"));
        //            cmd.ExecuteNonQuery();


        //            conn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        conn.Close();
        //        return false;
        //    }


        //    return true;

        //}

        protected float getDefaultMonthlyLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float limit = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyAUDLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        limit = float.Parse(sdr["SettingsVariable"].ToString());
                    }
                }

                conn.Close();
            }

            return limit;
        }
    }
}