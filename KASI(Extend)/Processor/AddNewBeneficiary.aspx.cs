﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Common.CommandTrees;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class AddNewBeneficiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String theSelectedID = Request.QueryString["CID"].ToString();
            String LastName = Request.QueryString["BeneLastName"].ToString();
            String FirstName = Request.QueryString["BeneFirstNames"].ToString();
            //String BeneName = Request.QueryString["BeneName"].ToString();
            String AdLine1 = Request.QueryString["AdLine1"].ToString();
            String AdLine2 = Request.QueryString["AdLine2"].ToString();
            String Suburb = Request.QueryString["Suburb"].ToString();
            String Postcode = Request.QueryString["Postcode"].ToString();
            String Country = Request.QueryString["Country"].ToString();
            String CountryofBirth = Request.QueryString["COB"].ToString();
            String DOB = Request.QueryString["DOB"].ToString();
            String PlaceOfBirth = Request.QueryString["POB"].ToString();
            String Nationality = Request.QueryString["National"].ToString();
            String Relationship = Request.QueryString["Relation"].ToString();
            String AKA = Request.QueryString["AKA"].ToString();
            String IDDetails = Request.QueryString["IDDetails"].ToString();
            String State = Request.QueryString["State"].ToString();
            String CNumber = Request.QueryString["CNumber"].ToString();
            String Email = Request.QueryString["Email"].ToString();

            performBeneAdd(theSelectedID, LastName, FirstName, AdLine1, AdLine2, Suburb, Postcode, Country, CountryofBirth, DOB, PlaceOfBirth, Nationality, Relationship, AKA, IDDetails, State, CNumber, Email);
            //test(theSelectedID, Request.QueryString["BeneName"].ToString());
        }

        protected int FindLastBeneficiaryID(String CID)
        {

            String LastCustomerID = String.Empty;
            int CLastCustomerID = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT MAX(BeneficiaryID) AS LastBene FROM dbo.Beneficiaries WHERE CustomerID = " + CID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                String LB = sdr["LastBene"].ToString();
                                if (sdr["LastBene"].ToString() == "")
                                {
                                    LastCustomerID = CID + "00";
                                }
                                else
                                {
                                    LastCustomerID = sdr["LastBene"].ToString();
                                }
                                
                            }
                        }
                        else
                        {
                            LastCustomerID = CID + "00";
                        }

                    }
                }
            }
            CLastCustomerID = Int32.Parse(LastCustomerID);
            return CLastCustomerID;

        }

        protected void performBeneAdd(String CID, String LastName, String FirstName, String AddLine1, String AddLine2, String Suburb, String Postcode, String Country, String CountryofBirth, String DOB, String PlaceOfBirth, String Nationality, String Relationship, String AKA, String IDDetails, String State, String CNumber, String Email)
        {
            int LastBeneID = FindLastBeneficiaryID(CID);
            LastBeneID = LastBeneID + 1;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SQLStatement = "INSERT INTO dbo.Beneficiaries (BeneficiaryID, CustomerID, BeneficiaryName, BeneficiaryLastName, BeneficiaryFirstNames, AddressLine1, AddressLine2, Suburb, Postcode, Country, CountryOfBirth, DOB, PlaceofBirth, Nationality, Relationship, AKA, NationalityCardID, State, Active, TelHome, EmailAddress) VALUES (@BeneficiaryID, @CustomerID, @BeneficiaryName, @BeneficiaryLastName, @BeneficiaryFirstNames, @AddressLine1, @AddressLine2, @Suburb, @Postcode, @Country, @CountryOfBirth, @DOB, @PlaceofBirth, @Nationality, @Relationship, @AKA, @NationalityCardID, @State, 'Y', @TelHome, @EmailAddress)";

            using (SqlCommand cmd = new SqlCommand(SQLStatement))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@BeneficiaryID", SqlDbType.Int, 10).Value = LastBeneID;
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 10).Value = CID;
                cmd.Parameters.Add("@BeneficiaryName", SqlDbType.NVarChar, 100).Value = FirstName + ' ' + LastName;
                cmd.Parameters.Add("@BeneficiaryLastName", SqlDbType.NVarChar, 500).Value = LastName;
                cmd.Parameters.Add("@BeneficiaryFirstNames", SqlDbType.NVarChar, 500).Value = FirstName;
                cmd.Parameters.Add("@AddressLine1", SqlDbType.NVarChar, 500).Value = AddLine1;
                cmd.Parameters.Add("@AddressLine2", SqlDbType.NVarChar, 500).Value = AddLine2;
                cmd.Parameters.AddWithValue("@Suburb", Suburb == "-- SELECT --" ? (object) DBNull.Value : Suburb);
                cmd.Parameters.Add("@Postcode", SqlDbType.NVarChar, 10).Value = Postcode;
                cmd.Parameters.Add("@Country", SqlDbType.NVarChar, 100).Value = Country;
                cmd.Parameters.AddWithValue("@CountryOfBirth", CountryofBirth == "--Select--" ? (object) DBNull.Value : CountryofBirth);
                if (DOB == "")
                {
                    cmd.Parameters.Add("@DOB", SqlDbType.Date, 100).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@DOB", SqlDbType.Date, 100).Value = Convert.ToDateTime(DOB);  
                }
                cmd.Parameters.Add("@PlaceofBirth", SqlDbType.NVarChar, 100).Value = PlaceOfBirth;
                cmd.Parameters.Add("@Nationality", SqlDbType.NVarChar, 100).Value = Nationality;
                cmd.Parameters.Add("@Relationship", SqlDbType.NVarChar, 100).Value = Relationship;
                cmd.Parameters.Add("@AKA", SqlDbType.NVarChar, 100).Value = AKA;
                cmd.Parameters.AddWithValue("@NationalityCardID", IDDetails == "--Select--" ? (object) DBNull.Value : IDDetails);
                cmd.Parameters.AddWithValue("@State", State == "-- SELECT --" ? (object) DBNull.Value : State);
                cmd.Parameters.Add("@TelHome", SqlDbType.NVarChar, 100).Value = CNumber;
                cmd.Parameters.Add("@EmailAddress", SqlDbType.NVarChar, 100).Value = Email;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}