﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading.Tasks;
using KASI_Extend_.classes;
using Kapruka.Enterprise;

namespace KASI_Extend_.Processor
{
    public partial class AddTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String agentProcessor = String.Empty;
            Agent AGT = new Agent();
            Customer Cust = new Customer();

            String saveCustID = Request.QueryString["CID"].ToString();

            if (Session["TypeOfLogin"].ToString() == "Agent")
            {
                agentProcessor = Session["AgentID"].ToString();
            }
            else
            {
                Customer custm = new Customer();
                agentProcessor = custm.getOwnerAgentIDFromCustomerID(Request.QueryString["CID"].ToString());
            }

            String CommType = getCommissionTypeForAgent(agentProcessor);

            float AgentFeeCommission = 0;
            float AgentFlatFee = 0;
            float AgentFXGainCommish = 0;
            float AgentMoney = 0;
            float MasterMoney = 0;

            if (CommType == "AUDGain")
            {
                AgentFeeCommission = float.Parse(AGT.getAgentFeeCommission(agentProcessor));
                AgentFlatFee = float.Parse(AGT.getAgentFlatFee(agentProcessor));
                AgentFXGainCommish = float.Parse(AGT.getAgentCommissionGainPerc(agentProcessor));
                AgentMoney = getAgentCommission(AgentFlatFee, AgentFeeCommission, Request.QueryString["SC"].ToString());
                MasterMoney = getMasterCommission(AgentMoney, Request.QueryString["SC"].ToString());
            }
            else
            {
                // TO BE AMENDED WHEN CALCULATING EXCHANGE GAIN 
                AgentFeeCommission = float.Parse(AGT.getAgentFeeCommission(agentProcessor));
                AgentFlatFee = float.Parse(AGT.getAgentFlatFee(agentProcessor));
                AgentMoney = getAgentCommission(AgentFlatFee, AgentFeeCommission, Request.QueryString["SC"].ToString());
                MasterMoney = getMasterCommission(AgentMoney, Request.QueryString["SC"].ToString());
            }

            classes.Banking BKN = new classes.Banking();

            String RoutingBank = BKN.getRoutingBankFromBankName(Request.QueryString["Bank"].ToString());

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SqlStmt = "INSERT INTO dbo.Transactions (CustomerID, CustomerLastName, BeneficiaryID, BeneficiaryName, AccountID, Rate, DollarAmount, ServiceCharge, RemittedAmount, RemittedCurrency, ProcessTransaction, InsertedIntoBankFile, InsertedIntoAustracFile, InsertedIntoAustracFileByMaster, CreatedBy, CreatedDateTime, Status, Bank, BankBranch, Purpose, SourceOfFunds, ComplianceReason, TransactionAgentOwnership, TransactionType, AgentID, Notes, MasterMoney, AgentMoney, AddToMasterFile, CountryID, MTransferFee, MAdditionalTransPerc, MCommishGainPerc, RoutingBank, MTransferFeeTotal, MCommishGainPercTotal) VALUES (@CustomerID, @CustomerLastName, @BeneficiaryID, @BeneficiaryName, @AccountID, @Rate, @DollarAmount, @ServiceCharge, @RemittedAmount, @RemittedCurrency, @ProcessTransaction, @InsertedIntoBankFile, @InsertedIntoAustracFile, @InsertedIntoAustracFileByMaster, @CreatedBy, CURRENT_TIMESTAMP, @Status, @Bank, @BankBranch, @Purpose, @SourceOfFunds, @ComplianceReason, @TransactionAgentOwnership, 'DEBIT', @AgentID, @Notes, @MasterMoney, @AgentMoney, @AddToMasterFile, @CountryID, @MTransferFee, @MAdditionalTransPerc, @MCommishGainPerc, @RoutingBank, @MTransferFeeTotal, @MCommishGainPercTotal)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 100).Value = Request.QueryString["CID"].ToString();
                cmd.Parameters.Add("@CustomerLastName", SqlDbType.NVarChar, 250).Value = Cust.getCustomerLastName(saveCustID);
                cmd.Parameters.Add("@BeneficiaryID", SqlDbType.Int, 100).Value = Request.QueryString["BID"].ToString();
                cmd.Parameters.Add("@BeneficiaryName", SqlDbType.NVarChar, 250).Value = Request.QueryString["BN"].ToString();
                cmd.Parameters.Add("@AccountID", SqlDbType.Int, 100).Value = Request.QueryString["AID"].ToString();
                cmd.Parameters.Add("@Rate", SqlDbType.NVarChar, 100).Value = Request.QueryString["Rate"].ToString();
                cmd.Parameters.Add("@DollarAmount", SqlDbType.NVarChar, 100).Value = Request.QueryString["DM"].ToString();
                cmd.Parameters.Add("@ServiceCharge", SqlDbType.NVarChar, 100).Value = Request.QueryString["SC"].ToString();
                cmd.Parameters.Add("@RemittedAmount", SqlDbType.NVarChar, 100).Value = Request.QueryString["RA"].ToString();
                cmd.Parameters.Add("@RemittedCurrency", SqlDbType.NVarChar, 100).Value = Request.QueryString["CUR"].ToString().Trim();
                cmd.Parameters.Add("@ProcessTransaction", SqlDbType.NVarChar, 100).Value = Request.QueryString["PT"].ToString();
                cmd.Parameters.Add("@InsertedIntoBankFile", SqlDbType.NVarChar, 100).Value = "N";
                cmd.Parameters.Add("@InsertedIntoAustracFile", SqlDbType.NVarChar, 100).Value = "N";
                cmd.Parameters.Add("@InsertedIntoAustracFileByMaster", SqlDbType.NVarChar, 100).Value = "N";
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"].ToString();
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 250).Value = Request.QueryString["ST"].ToString();
                cmd.Parameters.Add("@Bank", SqlDbType.NVarChar, 100).Value = Request.QueryString["Bank"].ToString();
                cmd.Parameters.Add("@BankBranch", SqlDbType.NVarChar, 250).Value = Request.QueryString["Branch"].ToString();
                cmd.Parameters.Add("@Purpose", SqlDbType.NVarChar, 50).Value = Request.QueryString["Purpose"].ToString();
                cmd.Parameters.Add("@SourceOfFunds", SqlDbType.NVarChar, 50).Value = Request.QueryString["Source"].ToString();
                cmd.Parameters.Add("@ComplianceReason", SqlDbType.NVarChar, 500).Value = Request.QueryString["CR"].ToString();
                cmd.Parameters.Add("@TransactionAgentOwnership", SqlDbType.Int, 3).Value = Session["LoggedID"].ToString();
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 4).Value = Request.QueryString["CountryID"].ToString();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 10).Value = Int32.Parse(agentProcessor);
                if (string.IsNullOrWhiteSpace(Request.QueryString["Notes"]))
                {
                    cmd.Parameters.Add("@Notes", SqlDbType.NVarChar, 1000).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Notes", SqlDbType.NVarChar, 1000).Value = Request.QueryString["Notes"].ToString();

                }
                cmd.Parameters.Add("@MasterMoney", SqlDbType.Money, 20).Value = MasterMoney;
                cmd.Parameters.Add("@AgentMoney", SqlDbType.Money, 20).Value = AgentMoney;
                cmd.Parameters.Add("@AddToMasterFile", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@MTransferFee", SqlDbType.Money).Value = AgentFlatFee;
                cmd.Parameters.Add("@MAdditionalTransPerc", SqlDbType.Float).Value = AgentFeeCommission;
                cmd.Parameters.Add("@MCommishGainPerc", SqlDbType.Float).Value = AgentFXGainCommish;

                
                if (AgentFeeCommission == 0)
                {
                    cmd.Parameters.Add("@MTransferFeeTotal", SqlDbType.Money).Value = AgentFlatFee;
                }
                else
                {
                    var CalcFee = (float.Parse(Request.QueryString["SC"].ToString()) * AgentFeeCommission) / 100;
                    if (CalcFee < AgentFlatFee)
                    {
                        cmd.Parameters.Add("@MTransferFeeTotal", SqlDbType.Money).Value = AgentFlatFee;
                    }
                    else
                    {
                        cmd.Parameters.Add("@MTransferFeeTotal", SqlDbType.Money).Value = CalcFee;
                    }
                }
                
                
                //cmd.Parameters.Add("@MAdditionalTransPercTotal", SqlDbType.Money).Value = (float.Parse(Request.QueryString["SC"].ToString()) * float.Parse(AgentFeeCommission.ToString())) / 100;
                if (CommType == "AUDGain")
                {
                    cmd.Parameters.Add("@MCommishGainPercTotal", SqlDbType.Money).Value = (float.Parse(Request.QueryString["DM"].ToString()) * float.Parse(AgentFXGainCommish.ToString())) / 100;
                }
                else
                {
                    cmd.Parameters.Add("@MCommishGainPercTotal", SqlDbType.Money).Value = 0;
                }
                

                cmd.Parameters.Add("@RoutingBank", SqlDbType.NVarChar).Value = RoutingBank;
                cmd.ExecuteNonQuery();


                conn.Close();
            
            }

            Customer cust = new Customer();

            float CustBalance = float.Parse(cust.getCustomerAccountBalance(Request.QueryString["CID"].ToString()).ToString());
            float SendingAmount = float.Parse(Request.QueryString["DM"].ToString()) + float.Parse(Request.QueryString["SC"].ToString());

            float RemAmount = CustBalance - SendingAmount;

            SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string sqlStmt = "UPDATE dbo.Customers SET AccountBalance=@AccountBalance WHERE CustomerID=@CustomerID";

            using (SqlCommand cmd = new SqlCommand(sqlStmt))
            {
                cmd.Connection = conn2;
                conn2.Open();
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 100).Value = Request.QueryString["CID"].ToString();
                cmd.Parameters.Add("@AccountBalance", SqlDbType.NVarChar, 250).Value = RemAmount;
                cmd.ExecuteNonQuery();
                conn2.Close();
            
            }

            //TransactionService tServ = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            //var TransID = tServ.GetAll(x => x.CustomerID == Int32.Parse(Request.QueryString["CID"].ToString()) && x.AgentID == Int32.Parse(Session["AgentID"].ToString()), , "").SingleOrDefault();

            string sqlFindTransactID = "SELECT TOP 1 TransactionID FROM dbo.Transactions WHERE CustomerID = " + Request.QueryString["CID"].ToString() + " AND AgentID = " + Session["AgentID"].ToString() + " ORDER BY CreatedDateTime DESC";
            String TransactID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlFindTransactID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TransactID = sdr["TransactionID"].ToString();
                    }

                }

                conn.Close();
            }

            //classes.EmailCustomer ECust = new classes.EmailCustomer();
            //ECust.SendEmailWhenSaved(TransactID);

            classes.Transactions TRANS = new classes.Transactions();
            TRANS.insertAuditRecord(TransactID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "ADD", "Transaction " + TransactID + " created");

            String AgentID = String.Empty;
            if (Session["TypeOfLogin"].ToString() == "Agent")
            {
                AgentID = Session["AgentID"].ToString();
            }
            else
            {
                AgentID = cust.getOwnerAgentIDFromCustomerID(Request.QueryString["CID"].ToString());
            }

            String SqlStmt2 = "INSERT INTO dbo.CreditTransactions (CustomerID, CreditAmount, DepositMethod, MasterBankAccountID, Type, CreditConfirmed, TransactionID, CreatedDateTime, CreatedBy, AgentID) VALUES (@CustomerID, @CreditAmount, @DepositMethod, 0, 'DEBIT', 'Y', @TransactionID, CURRENT_TIMESTAMP, @CreatedBy, @AgentID)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt2))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 20).Value = Request.QueryString["CID"].ToString();
                cmd.Parameters.Add("@CreditAmount", SqlDbType.Money, 20).Value = Request.QueryString["DM"].ToString();
                cmd.Parameters.Add("@DepositMethod", SqlDbType.NVarChar, 100).Value = "Transaction Debit";
                cmd.Parameters.Add("@TransactionID", SqlDbType.Int, 20).Value = TransactID;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 500).Value = Session["LoggedPersonName"].ToString() + "(" + Session["LoggedUserFullName"].ToString() + ")";
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 20).Value = AgentID;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "INSERT INTO dbo.CreditTransactions(CustomerID, CreditAmount, DepositMethod, MasterBankAccountID, Type, CreditConfirmed, TransactionID, CreatedDateTime, CreatedBy, AgentID) VALUES (@CustomerID, @CreditAmount, @DepositMethod, 0, 'DEBIT', 'Y', @TransactionID, CURRENT_TIMESTAMP, @CreatedBy, @AgentID)";
                conn.Open();
                cmd.Parameters.AddWithValue("@CustomerID", Request.QueryString["CID"].ToString());
                cmd.Parameters.AddWithValue("@CreditAmount", Request.QueryString["SC"].ToString());
                cmd.Parameters.AddWithValue("@DepositMethod", "Agent Service Fee");
                cmd.Parameters.AddWithValue("@TransactionID", TransactID);
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 500).Value = Session["LoggedPersonName"].ToString() + "(" + Session["LoggedUserFullName"].ToString() + ")";
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 20).Value = AgentID;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            SettingsService SetServ = new SettingsService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var SendTheEmail = SetServ.GetAll(x => x.SettingName == "AgentEmailActive", null, "").SingleOrDefault().SettingsVariable;
            var theCustID = Int32.Parse(Request.QueryString["CID"].ToString());
            
            CustomerService CustServ = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var CustSendEmail = CustServ.GetAll(x => x.CustomerID == theCustID, null, "").SingleOrDefault().SendEmailNotification;

            if (SendTheEmail == "Y")
            {
                //if (CustSendEmail == true)
                //{
                    EmailCustomer CEmail = new EmailCustomer();
                    CEmail.SendEmailWhenSaved(TransactID);
                //}
                
            }

                

            Response.Write("Transaction successfully completed|" + TransactID);
        }

        protected String getCommissionTypeForAgent(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String CommType = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AgentCommissionStructure FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        CommType = sdr["AgentCommissionStructure"].ToString();
                    }
                }
                conn.Close();
            }

            return CommType;
        }

        protected float getAgentCommission(float AFF, float AFC, String ServiceCharge)
        {
            float total = 0;

            var firstcalc = float.Parse(ServiceCharge) - AFF;
            var perc = (firstcalc * AFC) / 100;
            total = firstcalc - perc;

            return total;
        }

        protected float getMasterCommission(float AgentTotal, String ServiceCharge)
        {
            float Total = 0;

            Total = float.Parse(ServiceCharge) - AgentTotal;

            return Total;
        }

    }
}