﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Enterprise;
using Kapruka.Repository;
using System.Threading.Tasks;

namespace KASI_Extend_.Processor
{
    public partial class UpdateTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Rates rts = new classes.Rates();
            classes.Customer cust = new classes.Customer();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            String TID = Request.QueryString["TID"].ToString();
            String BID = Request.QueryString["BID"].ToString();
            String BName = Request.QueryString["BName"].ToString();
            String AID = Request.QueryString["AID"].ToString();
            String DAmount = Request.QueryString["DAmount"].ToString();
            String SCharge = Request.QueryString["SCharge"].ToString();
            String AVFunds = Request.QueryString["avfunds"].ToString();
            String OverseasAmount = Request.QueryString["OSAmount"].ToString();

            String strRate = rts.getRate();
            float Rate = float.Parse(strRate);

            float TodaySend = float.Parse(DAmount) * Rate;

            classes.BeneficiaryAccounts bAcc = new classes.BeneficiaryAccounts();

            String BankName = bAcc.getBeneficiaryBankName(BID);
            String BankBranch = bAcc.getBeneficiaryBankBranch(BID);

            String SqlStmt = "UPDATE dbo.Transactions SET BeneficiaryID=@BeneficiaryID, BeneficiaryName=@BeneficiaryName, AccountID=@AccountID, DollarAmount=@DollarAmount, ServiceCharge=@ServiceCharge, RemittedAmount=@RemittedAmount, Bank=@Bank, BankBranch=@BankBranch WHERE TransactionID = " + TID; 

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@BeneficiaryID", SqlDbType.Int).Value = BID;
                cmd.Parameters.Add("@BeneficiaryName", SqlDbType.NVarChar).Value = BName;
                cmd.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = AID;
                cmd.Parameters.Add("@DollarAmount", SqlDbType.Money).Value = DAmount;
                cmd.Parameters.Add("@ServiceCharge", SqlDbType.Money).Value = SCharge;
                cmd.Parameters.Add("@RemittedAmount", SqlDbType.Money).Value = float.Parse(OverseasAmount);
                cmd.Parameters.Add("@Bank", SqlDbType.NVarChar).Value = BankName;
                cmd.Parameters.Add("@BankBranch", SqlDbType.NVarChar).Value = BankBranch;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String CID = cust.getCustomerIDFromBeneficiaryID(BID);
            updateCustomerAccountBalance(CID, AVFunds);
            classes.Transactions TRANS = new classes.Transactions();
            TRANS.insertAuditRecord(TID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + TID + " has been edited");
            UpdateTransactionStatus(int.Parse(TID));
        }
        private void UpdateTransactionStatus(int tid)
        {
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            TransactionService service = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            string masMonthlyAudLimit = setSer.GetAll(x => x.SettingName == "MonthlyAUDLimit", null, "").SingleOrDefault().SettingsVariable;
            string masYearlyAudLimit = setSer.GetAll(x => x.SettingName == "YearlyAUDLimit", null, "").SingleOrDefault().SettingsVariable;
            string masMonthlyTrLimit = setSer.GetAll(x => x.SettingName == "MonthlyTransLimit", null, "").SingleOrDefault().SettingsVariable;
            string masYearlyTrLimit = setSer.GetAll(x => x.SettingName == "YearlyTransLimit", null, "").SingleOrDefault().SettingsVariable;
            var item = service.GetAll(x => x.TransactionID == tid, null, "").SingleOrDefault();//is checking status PROCESSING or COMPLIANCE CHECK?
            if (item != null)
            {
                string loggedUserId = "0";
                KASI_Extend_.classes.Transactions tr = new KASI_Extend_.classes.Transactions();
                Customer cust = custSer.GetAll(x => x.CustomerID == item.CustomerID.Value, null, "").SingleOrDefault();
                Agent agent = agentSer.GetAll(y => y.AgentID == item.AgentID, null, "").SingleOrDefault();
                bool isAnalyseMasterMonthlyDollarLimit = false;
                bool isAnalyseMasterYearlyDollarLimit = false;
                bool isAnalyseMasterMonthlyNbtransLimit = false;
                bool isAnalyseMasterYearlyNbtransLimit = false;

                bool isAnalyseMonthlyDollarLimit = AnalyseMonthlyDollarLimit(item.CustomerID.Value, service, agent.AgentID, masMonthlyAudLimit, out isAnalyseMasterMonthlyDollarLimit);
                bool isAnalyseMonthlyNbTransLimit = AnalyseMonthlyNbTransLimit(item.CustomerID.Value, service, agent.AgentID, masMonthlyTrLimit, out isAnalyseMasterMonthlyNbtransLimit);
                bool isAnalysYearlyDollarLimit = AnalyseYearlyDollarLimit(service, agent.AgentID, masYearlyAudLimit, out isAnalyseMasterYearlyDollarLimit,item.CustomerID.Value);
                bool isAnalyseyearlyNbTransLimit = AnalyseYearlyNbTransLimit(service, agent.AgentID, masYearlyTrLimit, out isAnalyseMasterYearlyNbtransLimit, item.CustomerID.Value);
                bool isAnalyseDocumentExpiry = AnalyseDocumentExpiry(item.CustomerID.Value);
                //bool isAnalyseMonthlyVariation = AnalyseMonthlyVariation(item.CustomerID.Value, service);


                if (cust.RiskLevel == 4)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Customer Risk level is 4", service);
                    tr.LogFailedTransactions(tid, "Customer Risk level is 4", loggedUserId);
                }
                if (isAnalyseMonthlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Monthly AUD Limit Exceeded", loggedUserId);
                }
                if (isAnalyseMonthlyNbTransLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly Number of Transaction Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Monthly Number of Transaction Limit Exceeded", loggedUserId);
                }
                if (isAnalyseDocumentExpiry)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Primary Document(s) Expired", service);
                    ChangeRiskLevel(cust, 4, custSer);
                    tr.LogFailedTransactions(tid, "Primary Document(s) Expired", loggedUserId);
                }
                //if (isAnalyseMonthlyVariation)
                //{
                //    ChangeTransactionStatus(item, "REVIEW", "Monthly Variation Exceeded", service);
                //    ChangeRiskLevel(cust, 3, custSer);
                //    tr.LogFailedTransactions(tid, "Monthly Variation Exceeded", loggedUserId);
                //}
                if (isAnalysYearlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Yearly AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Yearly AUD Limit Exceeded", loggedUserId);
                }
                if (isAnalyseyearlyNbTransLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Yearly Number of Transaction Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Yearly Number of Transaction Limit Exceeded", loggedUserId);
                }
                if (isAnalyseMasterMonthlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly Master AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    UpdateTransactionTable(item, service);
                    tr.LogFailedTransactions(tid, "Monthly Master AUD Limit Exceeded", loggedUserId);
                }
                if (isAnalyseMasterMonthlyNbtransLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly Master number of Transactions Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    UpdateTransactionTable(item, service);
                    tr.LogFailedTransactions(tid, "Monthly Master number of Transactions Limit Exceeded", loggedUserId);
                }
                if (isAnalyseMasterYearlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Yearly Master AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    UpdateTransactionTable(item, service);
                    tr.LogFailedTransactions(tid, "Yearly Master AUD Limit Exceeded", loggedUserId);
                }
                if (isAnalyseMasterYearlyNbtransLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Yearly Master number of Transactions Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    UpdateTransactionTable(item, service);
                    tr.LogFailedTransactions(tid, "Yearly Master number of Transactions Limit Exceeded", loggedUserId);
                }
                if (cust.RiskLevel != 4 && !isAnalyseyearlyNbTransLimit && !isAnalysYearlyDollarLimit && !isAnalyseDocumentExpiry && !isAnalyseMonthlyNbTransLimit && !isAnalyseMonthlyDollarLimit && !isAnalyseMasterMonthlyDollarLimit && !isAnalyseMasterMonthlyNbtransLimit && !isAnalyseMasterYearlyDollarLimit && !isAnalyseMasterYearlyNbtransLimit)
                {
                    ChangeTransactionStatus(item, "PENDING", "", service);
                }


            }
        }
        private void UpdateTransactionTable(Transaction item, TransactionService service)
        {
            item.ReviewByMasterCompany = true;
            service.Update(item);
        }
        private void updateCustomerAccountBalance(String CID, String AVFunds)
        {
            classes.Customer cust = new classes.Customer();

            String custBalance = cust.getCustomerAccountBalance(CID);
            float newBalance = 0;

            newBalance = float.Parse(custBalance) + float.Parse(AVFunds);

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);
            String SqlStmt = "UPDATE dbo.Customers SET AccountBalance = " + newBalance + " WHERE CustomerID = " + CID;

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

        }

        public static void ChangeRiskLevel(Customer customer, int riskLevel, CustomerService service)
        {
            customer.RiskLevel = riskLevel;

            service.Update(customer);
        }

        public async Task<int> AnalyseTransaction(String TransactionID)
        {
            int x = await Task.FromResult(0);
            return x;

            // If all of the below method are not true, then put the transaction [dbo].[Transactions].[Status] into 'PENDING'
        }
        public void ChangeTransactionStatus(Transaction trans, string status, string reason, TransactionService service)
        {
            String desctext = String.Empty;
            trans.Status = status;
            if (!string.IsNullOrEmpty(reason))
            {
                trans.ComplianceReason = reason;
                desctext = "Transaction " + trans.TransactionID + " escalated to Compliance due to: " + reason;
            }
            else
            {
                desctext = "Transaction " + trans.TransactionID + " set to PENDING";
            }
            service.Update(trans);
            classes.Transactions TRANS = new classes.Transactions();
            TRANS.insertAuditRecord(trans.TransactionID.ToString(), Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", desctext);
        }
        public bool AnalyseMonthlyDollarLimit(int custId, TransactionService ser, int agentId, string masCheck, out bool ismasCheck)
        {
            ismasCheck = false;
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            var customer = custSer.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault();
            var agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            decimal monthLimit = 0;
            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));
            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID==custId && x.Status != "CANCELLED", null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);
            if (decimal.Parse(masCheck) < totAmount)
            {
                ismasCheck = true;

            }
            if (agent.MonthlyAUDLimit.HasValue)
            {
                if (agent.MonthlyAUDLimit.Value < totAmount)
                {

                    return true;

                }
                else
                {


                    return false;

                }
            }
            decimal spendingPower = 0;


            if (customer.SpendingPower.HasValue)
            {
                spendingPower = customer.SpendingPower.Value;
            }


            if (spendingPower < totAmount)
            {

                return true;
            }
            else
            {
                return false;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer has a monthly limit set in [dbo].[Customers].[SpendingPower].
            // If combined Transaction AUD Total [dbo].[Transactions].[DollarAmount] for this month exceed above SpendingPower, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly AUD Limit Exceeded'
        }
        public bool AnalyseYearlyDollarLimit(TransactionService ser, int agentId, string masCheck, out bool ismasCheck,int custId)
        {
            ismasCheck = false;
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));

            var agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            decimal monthLimit = 0;
            DateTime yearFirstDate = Convert.ToDateTime("01/01/" + DateTime.Now.ToString("yyyy"));
            var trList = ser.GetAll(x => x.CreatedDateTime >= yearFirstDate && x.CustomerID==custId && x.Status != "CANCELLED", null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);
            if (decimal.Parse(masCheck) < totAmount)
            {
                ismasCheck = true;
            }
            if (agent.YearlyAUDsLimit.HasValue)
            {
                if (agent.YearlyAUDsLimit.Value < totAmount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

            // CID --> CustomerID
            // TID --> TransactionID

            // Customer has a monthly limit set in [dbo].[Customers].[SpendingPower].
            // If combined Transaction AUD Total [dbo].[Transactions].[DollarAmount] for this month exceed above SpendingPower, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly AUD Limit Exceeded'
        }

        public bool AnalyseMonthlyNbTransLimit(int custId, TransactionService ser, int agentId, string masCheck, out bool ismasCheck)
        {
            AgentService agSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            ismasCheck = false;
            var agent = agSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            decimal monthlyLimit = 0;
            if (agent.MonthlyTransLimit.HasValue)
            {
                monthlyLimit = agent.MonthlyTransLimit.Value;
            }

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID == custId && x.Status != "CANCELLED", null, "").ToList();
            int cnt = trList.Count();
            if (cnt > int.Parse(masCheck))
            {
                ismasCheck = true;
            }
            if (monthlyLimit > 0)
            {
                if (cnt > monthlyLimit)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

            // CID --> CustomerID
            // TID --> TransactionID

            // Global setting in [dbo].[Settings].[SettingName] = MonthlyTransLimit gives the maximum amount of transactions per month before it gets flagged
            // If COUNT(*) Transactions for that customer for this month exceed above MonthlyTransLimit, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Number of Transaction Limit Exceeded'
        }

        public bool AnalyseYearlyNbTransLimit(TransactionService ser, int agentId, string masCheck, out bool ismasCheck,int custId)
        {
            AgentService agSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            ismasCheck = false;
            var agent = agSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            decimal yearlyLimit = 0;
            if (agent.YearlyTransLimit.HasValue)
            {
                yearlyLimit = agent.YearlyTransLimit.Value;
            }

            DateTime monthFirstDate = Convert.ToDateTime("01/01/" + DateTime.Now.ToString("yyyy"));

            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID==custId && x.Status != "CANCELLED", null, "").ToList();
            int cnt = trList.Count();
            if (cnt > int.Parse(masCheck))
            {
                ismasCheck = true;
            }
            if (yearlyLimit > 0)
            {
                if (cnt > yearlyLimit)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

            // CID --> CustomerID
            // TID --> TransactionID

            // Global setting in [dbo].[Settings].[SettingName] = MonthlyTransLimit gives the maximum amount of transactions per month before it gets flagged
            // If COUNT(*) Transactions for that customer for this month exceed above MonthlyTransLimit, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Number of Transaction Limit Exceeded'
        }


        public bool AnalyseDocumentExpiry(int custId)
        {
            CustomerDocumentService ser = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            var docList = ser.GetDocumentList(custId).ToList();
            if (docList.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer Primary Documents are in [dbo].[CustomerDocuments].[TypeOfDocument] = 'PRIMARY'
            // There can be multiple primary documents. If ALL primary documents have expired, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Primary Document(s) Expired'
        }

        public bool AnalyseMonthlyVariation(int custId, TransactionService trSer)
        {
            SettingsService ser = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            int varMonths = int.Parse(ser.GetAll(x => x.SettingName == "MonthlyVariationMonths", null, "").SingleOrDefault().SettingsVariable);
            decimal varValue = Convert.ToDecimal(ser.GetAll(x => x.SettingName == "MonthlyVariation", null, "").SingleOrDefault().SettingsVariable);

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trthismonthList = trSer.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID == custId, null, "").ToList();
            decimal totAmount = trthismonthList.Sum(x => x.DollarAmount.Value);
            int thisMonth = DateTime.Now.Month;
            int thisYear = DateTime.Now.Year;

            for (int i = 1; i <= varMonths; i++)
            {
                thisMonth = thisMonth - i;
                if (thisMonth == 0)
                {
                    thisMonth = 12;
                    thisYear = thisYear - 1;
                }
                DateTime fromDate = Convert.ToDateTime("01/" + thisMonth.ToString("00") + "/" + thisYear);
                DateTime toDate = fromDate.AddMonths(1).AddDays(-1);
                var trList = trSer.GetAll(x => x.CustomerID == custId && x.CreatedDateTime < toDate && x.CreatedDateTime > fromDate, null, "").ToList();
                decimal totTrVal = trList.Sum(x => x.DollarAmount.Value);
                if (Math.Abs(totAmount - totTrVal) > 1000)
                {
                    return true;
                }
            }

            return false;
            // CID --> CustomerID
            // TID --> TransactionID

            // Look at the total for the last x Months ([dbo].[Settings].[SettingName] = MonthlyVariationMonths) 
            //of transactions in Dollar Amount.
            // The allowed variation is in [dbo].[Settings].[SettingName] = 'MonthlyVariation'
            // If the variation between the total for each month is greater than the MonthlyVariation, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW' 
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Variation Exceeded'
        }
    }
}