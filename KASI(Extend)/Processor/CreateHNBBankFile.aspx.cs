﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class CreateHNBBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions transClass = new classes.Transactions();
            String BankCode = Request.QueryString["BankCode"].ToString();
            String delimiter = "|";
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;

            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts bAccount = new BeneficiaryAccounts();
            String RAmount = String.Empty;
            String BeneID = String.Empty;
            String filename = "HNB - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            String now = String.Empty;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, Rate, AuthorisationDateTime FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode;
            String outputString = String.Empty;
            String HNBAccountNumber = "054010065371";
            String CFullName = String.Empty;
            int CFullNameLength = 0;
            String empty = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString = outputString + sdr["TransactionID"].ToString().PadRight(30);
                            outputString = outputString + HNBAccountNumber.PadRight(20);
                            RAmount = sdr["RemittedAmount"].ToString().PadRight(18);
                            outputString = outputString + String.Format("{0:N2}", RAmount).Replace(",", ".");
                            outputString = outputString + sdr["RemittedCurrency"].ToString().PadRight(3);
                            outputString = outputString + "B";

                            CFullName = cust.getCustomerFullName(sdr["CustomerID"].ToString());
                            CFullNameLength = CFullName.Length;

                            if (CFullNameLength > 55)
                            {
                                CFullName = CFullName.Substring(0, 54);
                            }
                            else
                            {
                                CFullName = CFullName.PadRight(55);
                            }

                            outputString = outputString + CFullName;

                            String CEmailAddress = cust.getCustomerEmailAddress1(sdr["CustomerID"].ToString());
                            int CEmailAddressLength = CEmailAddress.Length;

                            if (CEmailAddressLength > 30)
                            {
                                CEmailAddress = CEmailAddress.Substring(0, 29);
                            }
                            else
                            {
                                CEmailAddress = CEmailAddress.PadRight(30);
                            }
                            outputString = outputString + CEmailAddress;

                            String CMobile = cust.getCustomerMobile(sdr["CustomerID"].ToString());
                            int CMobileLength = CMobile.Length;

                            if (CMobileLength > 20)
                            {
                                CMobile = CMobile.Substring(0, 19);
                            }
                            else
                            {
                                CMobile = CMobile.PadRight(20);
                            }
                            outputString = outputString + CMobile;
                            outputString = outputString + empty.PadRight(60);

                            String BeneName = sdr["BeneficiaryName"].ToString();
                            int BeneNameLength = BeneName.Length;

                            if (BeneNameLength > 55)
                            {
                                BeneName = BeneName.Substring(0, 54);
                            }
                            else
                            {
                                BeneName = BeneName.PadRight(55);
                            }

                            outputString = outputString + BeneName;

                            String BeneAddress = bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString());
                            int BeneAddressLength = BeneAddress.Length;

                            if (BeneAddressLength > 100)
                            {
                                BeneAddress = BeneAddress.Substring(0, 99);
                            }
                            else
                            {
                                BeneAddress = BeneAddress.PadRight(100);
                            }

                            outputString = outputString + BeneAddress;

                            String BenePhone = bene.getBeneficiaryPhone(sdr["BeneficiaryID"].ToString());
                            int BenePhoneLength = BenePhone.Length;

                            if (BenePhoneLength > 20)
                            {
                                BenePhone = BenePhone.Substring(0, 19);
                            }
                            else
                            {
                                BenePhone = BenePhone.PadRight(20);
                            }

                            outputString = outputString + BenePhone;

                            String BeneNIC = bene.getBeneficiaryNIC(sdr["BeneficiaryID"].ToString());
                            int BeneNICLength = BeneNIC.Length;

                            if (BeneNICLength > 30)
                            {
                                BeneNIC = BeneNIC.Substring(0, 29);
                            }
                            else
                            {
                                BeneNIC = BeneNIC.PadRight(30);
                            }

                            outputString = outputString + BeneNIC;

                            String BeneEmail = bene.getBeneficiaryEmail(sdr["BeneficiaryID"].ToString());
                            int BeneEmailLength = BeneEmail.Length;

                            if (BeneEmailLength > 30)
                            {
                                BeneEmail = BeneEmail.Substring(0, 29);
                            }
                            else
                            {
                                BeneEmail = BeneEmail.PadRight(30);
                            }

                            outputString = outputString + BeneEmail;

                            String BeneMobile = bene.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString());
                            int BeneMobileLength = BeneMobile.Length;

                            if (BeneMobileLength > 20)
                            {
                                BeneMobile = BeneMobile.Substring(0, 19);
                            }
                            else
                            {
                                BeneMobile = BeneMobile.PadRight(20);
                            }

                            outputString = outputString + BeneMobile;

                            String BeneAccountNumber = bAccount.getBeneficiaryAccountNumber(sdr["BeneficiaryID"].ToString());
                            int BeneANumberLength = BeneAccountNumber.Length;

                            if (BeneANumberLength > 20)
                            {
                                BeneAccountNumber = BeneAccountNumber.Substring(0, 19);
                            }
                            else
                            {
                                BeneAccountNumber = BeneAccountNumber.PadRight(20);
                            }

                            outputString = outputString + BeneAccountNumber;

                            outputString = outputString + bAccount.getBeneficiaryBankCodeByBID(sdr["BeneficiaryID"].ToString());

                            outputString = outputString + bAccount.getBeneficiaryBranchCodeByBID(sdr["BeneficiaryID"].ToString()).PadRight(3);
                            outputString = outputString + empty.PadRight(40);
                            outputString = outputString + empty.PadRight(7);
                            outputString = outputString + sdr["RemittedAmount"].ToString().PadRight(20);
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + "2222";
                            //outputString = outputString + sdr["AuthorisationDateTime"].ToString().Substring(0, 9).Replace("-", "");
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + "1111";
                            outputString = outputString + "\r\n";
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                        transClass.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into Hatton National Bank Bank File");
                    }
                    Response.Write("HNB File Successfully Created^" + filename);
                }
                else
                {
                    Response.Write("HNB File Could Not Be Created^noreport");
                }
            }

            //Response.Write(outputString);
        }
    }
}