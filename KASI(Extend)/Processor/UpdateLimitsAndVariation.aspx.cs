﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class UpdateLimitsAndVariation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String ConnString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            updateMonthlyAUDLimit(ConnString);
            updateYearlyAUDLimit(ConnString);
            updateMonthlyTransferLimit(ConnString);
            updateYearlyTransferLimit(ConnString);
            updateMonthlyVariation(ConnString);
            updateMonthlyVariationMonths(ConnString);
        }

        protected void updateMonthlyAUDLimit(String CS)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = CS;

            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable=@TheValue WHERE SettingName='MonthlyAUDLimit'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@TheValue", Request.QueryString["MAL"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void updateYearlyAUDLimit(String CS)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = CS;

            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable=@TheValue WHERE SettingName='YearlyAUDLimit'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@TheValue", Request.QueryString["YAL"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void updateMonthlyTransferLimit(String CS)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = CS;

            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable=@TheValue WHERE SettingName='MonthlyTransLimit'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@TheValue", Request.QueryString["MTL"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void updateYearlyTransferLimit(String CS)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = CS;

            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable=@TheValue WHERE SettingName='YearlyTransLimit'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@TheValue", Request.QueryString["YTL"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void updateMonthlyVariation(String CS)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = CS;

            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable=@TheValue WHERE SettingName='MonthlyVariation'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@TheValue", Request.QueryString["MV"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void updateMonthlyVariationMonths(String CS)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = CS;

            String SQLStmt = "UPDATE dbo.Settings SET SettingsVariable=@TheValue WHERE SettingName='MonthlyVariationMonths'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@TheValue", Request.QueryString["MVM"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}