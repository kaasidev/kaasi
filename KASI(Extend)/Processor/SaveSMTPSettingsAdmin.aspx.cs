﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class SaveSMTPSettingsAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt1 = "UPDATE dbo.Settings SET SettingsVariable=@SMTPServerName WHERE SettingName = 'SMTPServerName'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt1))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SMTPServerName", SqlDbType.NVarChar, 200).Value = Request.QueryString["Name"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String SQLStmt2 = "UPDATE dbo.Settings SET SettingsVariable=@SMTPPort WHERE SettingName = 'SMTPPort'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt2))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SMTPPort", SqlDbType.NVarChar, 200).Value = Request.QueryString["Port"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }


            String SQLStmt3 = "UPDATE dbo.Settings SET SettingsVariable=@SMTPSSL WHERE SettingName = 'SSLEnabled'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt3))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SMTPSSL", SqlDbType.NVarChar, 200).Value = Request.QueryString["SSL"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String SQLStmt4 = "UPDATE dbo.Settings SET SettingsVariable=@SMTPUsername WHERE SettingName = 'SMTPUsername'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt4))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SMTPUsername", SqlDbType.NVarChar, 200).Value = Request.QueryString["UName"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String SQLStmt5 = "UPDATE dbo.Settings SET SettingsVariable=@SMTPPassword WHERE SettingName = 'SMTPPassword'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt5))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SMTPPassword", SqlDbType.NVarChar, 200).Value = Request.QueryString["PWord"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String SQLStmt6 = "UPDATE dbo.Settings SET SettingsVariable=@SMTPEmail WHERE SettingName = 'SMTPEmailAddress'";

            using (SqlCommand cmd = new SqlCommand(SQLStmt6))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@SMTPEmail", SqlDbType.NVarChar, 200).Value = Request.QueryString["Email"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            Response.Write("Updated");
        }
    }
}