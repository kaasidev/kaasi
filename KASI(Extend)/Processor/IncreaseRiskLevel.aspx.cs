﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class IncreaseRiskLevel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String RiskCode = Request.QueryString["RID"].ToString();

            String Reason = String.Empty;

            if (RiskCode == "1")
            {
                Reason = "TRANSLIMIT";
            }

            Customer cust = new Customer();
            int currentRisk = Int32.Parse(cust.getCustomerRiskLevel(CID));

            int NewRisk  = currentRisk + 1;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            String SQLStmt = "UPDATE dbo.Customers SET RiskLevel = " + NewRisk + ", RiskReason = '" + Reason + "' WHERE CustomerID = " + CID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}