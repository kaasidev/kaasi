﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class updateForeignAccountCurrency : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            String output = String.Empty;
            try
            {
                String FBAccID = Request.QueryString["FBAccID"].ToString();
                String cid = Request.QueryString["cid"].ToString();
                String bid = Request.QueryString["bid"].ToString();
                String acc = Request.QueryString["acc"].ToString();
                String curids = Request.QueryString["curids"].ToString();

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "update AgentForeignBankSettings set CountryID='" + cid + "', BankID='" + bid + "', CurrencyID='" + curids + "', ForeignBankAccountNumber='" + acc + "' FROM dbo.AgentForeignBankSettings WHERE AgentForeignBankSettingsID = " + FBAccID;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                output = "1";
            }
            catch (Exception ex)
            {
                output = "0^" + ex.ToString();
            }

            Response.Write(output);
        }
    }
}