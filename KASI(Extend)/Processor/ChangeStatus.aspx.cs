﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class ChangeStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = Request.QueryString["TID"].ToString();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            String SqlStmt = "UPDATE dbo.Transactions SET Status = 'PENDING' WHERE TransactionID = " + TID;

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            Response.Write("OK");
        }
    }
}