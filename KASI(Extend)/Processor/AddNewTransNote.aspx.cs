﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend_.Processor
{
    public partial class AddNewTransNote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = "INSERT INTO dbo.CustomerNotes (CustomerID, TransactionID, Notes, CreatedBy, CreatedByAgent, CreatedDateTime) VALUES (@CustomerID, @TransactionID, @Notes, @CreatedBy, @CreatedByAgent, CURRENT_TIMESTAMP)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@CustomerID", Request.QueryString["CID"].ToString());
                cmd.Parameters.AddWithValue("@TransactionID", Request.QueryString["TID"]);
                cmd.Parameters.AddWithValue("@Notes", Request.QueryString["Notes"]);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["LoggedPersonName"]);
                cmd.Parameters.AddWithValue("@CreatedByAgent", Session["LoggedUserFullName"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            classes.Transactions TRANS = new classes.Transactions();
            TRANS.insertAuditRecord(Request.QueryString["TID"].ToString(), Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "ADD", "A new note has been added");


            

            if (Session["AgentID"] == null)
            {
                /******* Find AgentID ************/
                int CustID = Int32.Parse(Request.QueryString["CID"].ToString());
                CustomerService CustServ = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
                var CustDetails = CustServ.GetAll(x => x.CustomerID == CustID, null, "").SingleOrDefault().AgentID;

                String sendNotifToAffiliate = String.Empty;
                sendNotifToAffiliate = Request.QueryString["SendNotif"].ToString();

                if (sendNotifToAffiliate == "true")
                {
                    AgentNotificationService AGServ = new AgentNotificationService(new UnitOfWorks(new KaprukaEntities()));

                    Random rnd = new Random();
                    int GlobalID = rnd.Next(999999999);

                    AgentNotification AGNot = new AgentNotification();
                    AGNot.GlobalNotificiationID = GlobalID;
                    AGNot.Type = "TRX";
                    AGNot.RecordID = Int32.Parse(Request.QueryString["TID"].ToString());
                    AGNot.Description = "RNP has added a new note for transaction " + Request.QueryString["TID"].ToString();
                    AGNot.CreatedDateTime = DateTime.Now;
                    AGNot.AgentID = Int32.Parse(CustDetails.ToString());

                    AGServ.Add(AGNot);
                }
            }
            

            Response.Write("OK");
        }
    }
}