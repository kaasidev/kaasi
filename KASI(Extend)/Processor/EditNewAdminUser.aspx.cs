﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class EditNewAdminUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = "UPDATE dbo.Logins SET FirstName=@FirstName,LastName=@LastName,FullName=@FullName,Username=@Username,Email=@Email, AmendedBy=@AmendedBy, AmendedDateTime=CURRENT_TIMESTAMP WHERE LoginID=" + Request.QueryString["UID"].ToString();

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["FName"].ToString();
                cmd.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["LName"].ToString();
                cmd.Parameters.Add(("@FullName"), SqlDbType.NVarChar, 250).Value =
                    Request.QueryString["FName"].ToString() + ' ' + Request.QueryString["LName"].ToString();
                cmd.Parameters.Add(("@Username"), SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["UName"].ToString();
              
                cmd.Parameters.Add(("@Email"), SqlDbType.NVarChar, 100).Value = Request.QueryString["Email"].ToString();
                cmd.Parameters.Add("@AmendedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"];
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}