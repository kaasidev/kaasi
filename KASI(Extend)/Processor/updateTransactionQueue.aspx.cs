﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend_.Processor
{
    public partial class updateTransactionQueue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions trans = new classes.Transactions();
            trans.updateTranStatus(Request.QueryString["TID"].ToString(), Request.QueryString["NewStatus"].ToString(), Request.QueryString["VR"].ToString());

            TransAuditLogService TALServ = new TransAuditLogService(new UnitOfWorks(new KaprukaEntities()));
            
            TransAuditLog transAudit = new TransAuditLog();
            transAudit.TransactionID = Int32.Parse(Request.QueryString["TID"].ToString());
            transAudit.Username = Session["LoggedPersonName"].ToString();
            transAudit.AgentName = Session["LoggedCompany"].ToString();
            transAudit.ActionType = "EDIT";
            transAudit.Description = "Transaction has been approved. This transaction is now set to " + Request.QueryString["NewStatus"].ToString();
            transAudit.AuditDateTime = DateTime.Now;

            TALServ.Add(transAudit);

            int TrID = Int32.Parse(Request.QueryString["TID"].ToString());

            TransactionService TRServ = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var TRDetails = TRServ.GetAll(x => x.TransactionID == TrID, null, "").SingleOrDefault().AgentID;

            AgentNotificationService ANServ = new AgentNotificationService(new UnitOfWorks(new KaprukaEntities()));

            Random rnd = new Random();
            int GlobalID = rnd.Next(999999999);

            AgentNotification AGNot = new AgentNotification();
            AGNot.AgentID = Int32.Parse(TRDetails.ToString());
            AGNot.GlobalNotificiationID = GlobalID;
            AGNot.Type = "TRX";
            AGNot.RecordID = Int32.Parse(Request.QueryString["TID"].ToString());
            AGNot.Description = "RNP has approved transaction " + Request.QueryString["TID"].ToString();
            AGNot.CreatedDateTime = DateTime.Now;

            ANServ.Add(AGNot);

        }

    }
}