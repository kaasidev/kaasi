﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.IO;

namespace KASI_Extend_.Processor
{
    public partial class CreatePBBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          string res=createFileBody();
            Response.Write(res);
        }

        protected string createFileBody()
        {
            classes.Transactions transClass = new classes.Transactions();
            String BankCode = Request.QueryString["BankCode"].ToString();
            String strDelimiter = ",";
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            CultureInfo ci = new CultureInfo("en-GB");

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyymmdd");
            int agentCode = 8336;
            int numOfRem = 3;
            String cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            StringBuilder sbH = new StringBuilder();
            StringBuilder sb = new StringBuilder();


            // sb.Append("02" + strDelimiter);
            //sb.Append("ACCOUNT_NO." + strDelimiter);
            //sb.Append("" + strDelimiter);
            //sb.Append("CURRENCY" + strDelimiter);
            //sb.Append("REM_TYPE" + strDelimiter);
            //sb.Append("CHG_FROM" + strDelimiter);
            //sb.Append("CUST_NAME" + strDelimiter);
            //sb.Append("DETAILS" + strDelimiter);
            //sb.Append("BENE_NAME" + strDelimiter);
            //sb.Append("BENE_ADDRESS" + strDelimiter);
            //sb.Append("BENE_TEL" + strDelimiter);
            //sb.Append("BENE_ID_NO" + strDelimiter);
            //sb.Append("BENE_ACCNO" + strDelimiter);
            //sb.Append("BENE_BANK_CODE" + strDelimiter);
            //sb.Append("BENE_BRANCH_CODE" + strDelimiter);
            //sb.Append("BANK_TO_BANK_INFO" + strDelimiter);
            sb.Append("\r\n");
            int index = 1;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = cs;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select J.* from(select T.TransactionID,T.RemittedCurrency,T.RemittedAmount,Convert(varchar(10),T.CreatedDateTime,103) as CreatedDateTime,T.BeneficiaryID,T.RoutingBank, " +
                                      "  T.CustomerLastName,B.AccountNumber,B.Currency,B.PaymentMethod,F.ForeignBankAccountNumber, "+
                                      "  BN.BeneficiaryName,B.BankName,B.BranchName,concat(BN.AddressLine1, ' ', BN.AddressLine2, ' ', BN.Suburb, ' ', BN.State, ' ', BN.Postcode, ' ', BN.Country) as BeneficiaryAddress1, "+
                                      "  Concat(C.AddressLine1, ' ', C.AddressLine2, ' ', C.Suburb, ' ', C.State, ' ', C.Postcode, ' ', C.Country) as CustomerAddress,C.FullName,BN.Mobile as BeneficiaryMobile,C.Mobile as CustomerMobile " +
                                      "            from Transactions as T "+
                                     "   inner join BeneficiaryPaymentMethods as B on B.AccountID = T.AccountID "+
                                     "   inner join AgentForeignBankSettings as F on F.AgentID = T.AgentID "+
                                     "   inner join Beneficiaries as BN on BN.BeneficiaryID = T.BeneficiaryID "+
                                     "   inner join Customers as C on C.CustomerID = T.CustomerID "+
                                     "    where Status = 'AWAITING TRANSFER' and RoutingBank = "+ BankCode + " "+
                                        " ) as J";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                           
                            while (sdr.Read())
                            {
                                sb.Append("02" + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["ForeignBankAccountNumber"].ToString(),19,"s") + strDelimiter);
                                sb.Append("1030008822" + strDelimiter);
                                sb.Append(FillEmptyFields(index.ToString(),6,"i") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["RemittedCurrency"].ToString(),20,"s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["TransactionID"].ToString(),20,"i") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["RemittedCurrency"].ToString(), 4, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["RemittedAmount"].ToString(), 19, "i") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["PaymentMethod"].ToString(), 4, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(Convert.ToDateTime(sdr["CreatedDateTime"].ToString(),ci).ToString("dd/MM/yyyy"), 10, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["BeneficiaryID"].ToString(), 20, "i") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["BeneficiaryName"].ToString(), 40, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["AccountNumber"].ToString(), 19, "i") + strDelimiter);
                                //bank code not found
                                sb.Append(FillEmptyFields(sdr["BankName"].ToString(), 30, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["BranchName"].ToString(), 30, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["BeneficiaryAddress1"].ToString(), 110, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["BeneficiaryMobile"].ToString(), 15, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["CustomerLastName"].ToString(), 40, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["CustomerAddress"].ToString(), 110, "s") + strDelimiter);
                                sb.Append(FillEmptyFields(sdr["CustomerMobile"].ToString(),15, "s") + strDelimiter);



                                sb.Append("\r\n");
                                //TID.Add(sdr["TransactionID"].ToString());

                                index = index + 1;
                            }

                        }
                    }
                    conn.Close();
                }
            }

           
            String filename = agentCode.ToString() + DateTime.Now.ToString("ddmmyyyy") + "dddR.txt";
            sbH.Append("01" + strDelimiter);
            sbH.Append(agentCode.ToString("0000000") + strDelimiter);//agentCode
            sbH.Append(agentCode.ToString() + DateTime.Now.ToString("ddmmyyyy") + "dddR" + strDelimiter);//batchnumber
            sbH.Append(DateTime.Now.ToString("dd/mm/yyyy") + strDelimiter);//date
            sbH.Append(index.ToString("000000") + strDelimiter);
            sbH.Append("\r\n");

            sbH.Append(sb);

            string res = "";

            try
            {
                string filePath = Server.MapPath("/BankFileCreation") + "\\" + filename;
                StreamWriter file = new StreamWriter(filePath);
                file.WriteLine(sb.ToString());
                file.Close();
                filehasbeencomplete = true;
            }
            catch (Exception ex)
            {
                filehasbeencomplete = false;

            }

            if (filehasbeencomplete == true)
            {
                foreach (var Transaction in TID)
                {
                    transClass.updateTransBankFileInsertion(Transaction);
                    transClass.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into Commercial Bank Bank File");
                }

                res="PPB File Successfully Created^" + filename;
            }
            else
            {
                res = "PPB File Could Not Be Created^nofile";
            }

            return res;
        }

        public string FillEmptyFields(string input,int size,string type)
        {
            int inpSize = input.Length;
            int sub = size - inpSize;
            int fillCount = 0;
            string finStr = "";
            if((size-inpSize)>0)
            {
                for (int i = 0; i < size - inpSize; i++)
                {
                    if(type=="i")
                    {
                        finStr = finStr + "0";
                    }
                    else
                    {
                        finStr = finStr + " ";
                    }
                    
                }

                finStr = finStr + input;
            }
            else if((size - inpSize) == 0)
            {
                finStr= input;
            }
            else
            {
                int substrVal = inpSize - size;
                finStr = input.Remove(size, substrVal);
            }

            return finStr;
        }
    }
}