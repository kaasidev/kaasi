﻿using Kapruka.Enterprise;
using KASI_Extend_.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class GetAgentDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AgentEditModel model = new AgentEditModel();
            int aid = int.Parse(Request.QueryString["id"].ToString());
            AgentService ser = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            var agent = ser.GetAll(x => x.AgentID == aid, null, "").SingleOrDefault();
            model.AgentABN = agent.AgentABN;
            model.AgentACN = agent.AgentACN;
            model.AgentAddress1 = agent.AgentAddress1;
            model.AgentAddress2 = agent.AgentAddress2;
            model.AgentCity = agent.AgentCity;
            model.AgentCountries = GetAgentAssigedCountries(aid.ToString());
            model.AgentEmail = agent.AgentEmail;
            model.AgentFax = agent.AgentFax;
            model.AgentName = agent.AgentName;
            model.AgentPhone = agent.AgentPhone;
            model.AgentPostcode = agent.AgentPostcode;
            model.AgentState = agent.AgentState;
            model.AgentFeeFlat = agent.AgentFeeFlat.Value.ToString();
            model.AgentFeeCommission = agent.AgentFeeCommission.Value.ToString();
            model.AgentCommissionStructure = agent.AgentCommissionStructure;
            model.AgentTransGainCommission = agent.AgentTransGainCommission.Value.ToString();
            model.AgentAustracNumber = agent.AustracNumber;
            if(agent.AustracRegDate.HasValue)
            {
                model.AgentAustracRegDate = agent.AustracRegDate.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                model.AgentAustracRegDate = "";
            }
            if(agent.AustracRenewalDate.HasValue)
            {
                model.AgentAustracRenewalDate = agent.AustracRenewalDate.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                model.AgentAustracRenewalDate = "";
            }
            
            model.BankIds = GetAgentAssigedBanks(aid.ToString());



            string jsonObj = JsonConvert.SerializeObject(model);
            Response.Write(jsonObj);

        }

        private String GetAgentAssigedCountries(String agentId)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String CountryID = String.Empty;
            List<string> lst=new List<string>();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT CountryID FROM dbo.AgentAssignedCountries WHERE AgentID = '" + agentId + "'";
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        lst.Add(sdr["CountryID"].ToString());
                      
                    }
                }
                conn.Close();
            }

            string countryIdList = "";
            string[] cAr = CountryID.Split(',');
            for (int i = 0; i < lst.Count; i++)
			{
                string countryName = FindCountryNameByID(lst[i]);
                countryIdList = countryIdList + countryName + ",";
			}
            countryIdList = countryIdList.Replace(", ","");
            return countryIdList;
        }

        private String GetAgentAssigedBanks(String agentId)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
          
            string isList = "";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT MasterBankAccountID FROM dbo.AgentAssignedBanks WHERE AgentID = '" + agentId + "'";
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        isList = isList + sdr["MasterBankAccountID"].ToString() + ",";

                    }
                }
                conn.Close();
            }

         
            return isList;
        }

        private String FindCountryIDByName(String Country)
        {
            if (!string.IsNullOrEmpty(Country))
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                String CountryID = String.Empty;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT CountryID FROM dbo.Countries WHERE CountryName = '" + Country + "'";
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CountryID = sdr["CountryID"].ToString();
                        }
                    }
                    conn.Close();
                }
                return CountryID;
            }
            else
            {
                return "";
            }
            
        }

        private String FindCountryNameByID(String Country)
        {
            if (!string.IsNullOrEmpty(Country))
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                String CountryName = String.Empty;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT CountryName FROM dbo.Countries WHERE CountryID = '" + Country + "'";
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CountryName = sdr["CountryName"].ToString();
                        }
                    }
                    conn.Close();
                }
                return CountryName;
            }
            else
            {
                return "";
            }

        }
    }
}