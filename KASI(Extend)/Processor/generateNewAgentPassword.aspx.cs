﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class generateNewAgentPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String pword = Membership.GeneratePassword(10, 3);
            String LID = Request.QueryString["LID"];

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "UPDATE dbo.Logins SET Password=@Password WHERE LoginID = " + LID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@Password", pword);
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            EmailCustomer CS = new EmailCustomer();
            CS.SendAgentEmailEdit(LID);
        }
    }
}