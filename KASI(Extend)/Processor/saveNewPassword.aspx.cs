﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class saveNewPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String NewPAss = Request.QueryString["NP"].ToString();
            String result = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "UPDATE dbo.Logins SET Password=@Password WHERE LoginID=@LoginID";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@Password", SqlDbType.NVarChar, 50).Value = NewPAss;
                cmd.Parameters.Add("@LoginID", SqlDbType.NVarChar, 10).Value = Session["LoggedUserID"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            Response.Write("OK");
        }
    }
}