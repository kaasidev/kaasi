﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.Processor
{
    public partial class deleteAgentNotification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int GlobalID = Int32.Parse(Request.QueryString["NID"].ToString());
            try
            {
                AgentNotificationService AGServ = new AgentNotificationService(new UnitOfWorks(new KaprukaEntities()));
                var AGDetails = AGServ.GetAll(x => x.AgentNotificationID == GlobalID, null, "").SingleOrDefault();

                AGServ.Delete(AGDetails);

                Response.Write("OK");
            }
            catch (Exception ex)
            {
                Response.Write("An error occured. Please contact your system administrator");
            }
            
        }
    }
}