﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;

namespace KASI_Extend_.Processor
{
    public partial class AddNewAdminUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String pword = Membership.GeneratePassword(10, 3);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = "INSERT INTO dbo.Logins (FirstName, LastName, FullName, LoginGroup, Active, Username, Password, Email, LoginType, Activated) VALUES (@FirstName, @LastName, @FullName, 1, 'Y', @Username, @Password, @Email, @LoginType, 0)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["FName"].ToString();
                cmd.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["LName"].ToString();
                cmd.Parameters.Add(("@FullName"), SqlDbType.NVarChar, 250).Value =
                    Request.QueryString["FName"].ToString() + ' ' + Request.QueryString["LName"].ToString();
                cmd.Parameters.Add(("@Username"), SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["UName"].ToString();
                cmd.Parameters.Add(("@Password"), SqlDbType.NVarChar, 100).Value = pword;
                cmd.Parameters.Add(("@Email"), SqlDbType.NVarChar, 100).Value = Request.QueryString["Email"].ToString();
                cmd.Parameters.Add(("@LoginType"), SqlDbType.NVarChar, 100).Value =
                    Request.QueryString["AccountType"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}