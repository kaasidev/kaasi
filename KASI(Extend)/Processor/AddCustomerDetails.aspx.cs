﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Kapruka.Service;
using Kapruka.Domain;

namespace KASI_Extend_.Processor
{
    public partial class AddCustomerDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Customer cs = new Customer();

            String LastCustomerID = cs.getLastCustomerID();



            int LastID = Int32.Parse(LastCustomerID);
            LastID = LastID + 1;
            var dob = Request.Form["dob"];
            var firstName = Request.Form["firstnames"];
            var lastName = Request.Form["lastname"];
            var email = Request.Form["email1"];
            var result = addCustomer(LastID, lastName, firstName, dob,
                Request.Form["unitno"], Request.Form["streetno"], Request.Form["streetname"], Request.Form["streettype"],
                Request.Form["suburb"], Request.Form["state"],
                Request.Form["postcode"], Request.Form["homephone"], Request.Form["workphone"], Request.Form["mobile"],
                Request.Form["email1"], Request.Form["email2"], Request.Form["countryofbirth"], Request.Form["placeofbirth"],
                Request.Form["nationality"], Request.Form["aka"], Request.Form["hidagentid"], Request.Form["title"]);

            if (result)
            {
                var objKyced = new CustomerKyced();
                objKyced.dob = dob;
                objKyced.bfn = firstName;
                objKyced.bln = lastName;
                objKyced.man = LastCustomerID.ToString();

                //var customerService = new CustomerHandlerService();
                //var recordId = customerService.GetRecordIdBYCustomerId(LastID);
                //var customerApiMessage = "Webapi successfully called.";
                //var customerApiResult = new CustomerHandlerService().CreateKycedCustomer(objKyced, LastID, recordId);
                //if (customerApiResult == false)
                //    customerApiMessage = "An error has occured while calling Webapi";
                Response.Write("CustomerID: " + LastID + " - This customer has been successfully created.");
            }
            else
            {
                Response.Write("An error occured trying to create this customer. Please contact your system administrator.");
            }
        }



        public bool addCustomer(int LastID, String lastname, String firstnames, String dob, String unitno, String streetno, String streetname, String streetype, String suburb, String state, String postcode, String homephone, String workphone, String mobile, String email1, String email2, String CountryOfBirth, String PlaceOfBirth, String Nationality, String AKA, String AID, string title)
        {
            String FullName = firstnames + ' ' + lastname;
            var customerType = 1;
            var hasRecordInKyce = (int)CustomerINKYCE.No;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SqlStmt = @"SET DATEFORMAT dmy INSERT INTO dbo.Customers (CustomerID, FirstName, LastName, UnitNo,
                     StreetNo, StreetName, StreetType, Suburb, State, Postcode, Mobile, TelHome, TelWork, DOB, 
                     EmailAddress, EmailAddress2, FullName, RiskLevel, AccountBalance, RiskManagement, RiskReason, 
                     CountryOfBirth, PlaceOfBirth, Nationality, AKA, AgentID, SpendingPower, CustomerType,HasKYC,IsIndividual,Title, SendEmailNotification) VALUES 
                    (@CustomerID, @FirstName, @LastName, @UnitNo, @StreetNo, @StreetName, @StreetType, @Suburb,
                    @State, @Postcode, @Mobile, @TelHome, @TelWork, @DOB, @EmailAddress, @EmailAddress2, 
                   @FullName, '4', '0.00', 'Automatic', 'NOID', @CountryOfBirth, @PlaceOfBirth, @Nationality, 
                  @AKA, @AgentID, @SpendingPower, @CustomerType,@HasKYC,@IsIndividual,@Title,1)";
            try
            {
                using (SqlCommand cmd = new SqlCommand(SqlStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 6).Value = LastID;
                    cmd.Parameters.Add(new SqlParameter("@LastName", String.IsNullOrEmpty(firstnames) ? (object)DBNull.Value : lastname));
                    cmd.Parameters.Add(new SqlParameter("@FirstName", String.IsNullOrEmpty(lastname) ? (object)DBNull.Value : firstnames));
                    cmd.Parameters.Add(new SqlParameter("@UnitNo", String.IsNullOrEmpty(unitno) ? (object)DBNull.Value : unitno));
                    cmd.Parameters.Add(new SqlParameter("@StreetNo", String.IsNullOrEmpty(streetno) ? (object)DBNull.Value : streetno));
                    cmd.Parameters.Add(new SqlParameter("@StreetName", String.IsNullOrEmpty(streetname) ? (object)DBNull.Value : streetname));
                    cmd.Parameters.Add(new SqlParameter("@StreetType", String.IsNullOrEmpty(streetype) ? (object)DBNull.Value : streetype));
                    cmd.Parameters.Add(new SqlParameter("@Suburb", String.IsNullOrEmpty(suburb) ? (object)DBNull.Value : suburb));
                    cmd.Parameters.Add(new SqlParameter("@State", String.IsNullOrEmpty(state) ? (object)DBNull.Value : state.ToUpper()));
                    cmd.Parameters.Add(new SqlParameter("@Postcode", String.IsNullOrEmpty(postcode) ? (object)DBNull.Value : postcode));
                    cmd.Parameters.Add(new SqlParameter("@TelHome", String.IsNullOrEmpty(homephone) ? (object)DBNull.Value : homephone));
                    cmd.Parameters.Add(new SqlParameter("@TelWork", String.IsNullOrEmpty(workphone) ? (object)DBNull.Value : workphone));
                    cmd.Parameters.Add(new SqlParameter("@Mobile", String.IsNullOrEmpty(mobile) ? (object)DBNull.Value : mobile));
                    cmd.Parameters.Add(new SqlParameter("@DOB", String.IsNullOrEmpty(dob) ? (object)DBNull.Value : dob));
                    cmd.Parameters.Add(new SqlParameter("@EmailAddress", String.IsNullOrEmpty(email1) ? (object)DBNull.Value : email1));
                    cmd.Parameters.Add(new SqlParameter("@EmailAddress2", String.IsNullOrEmpty(email2) ? (object)DBNull.Value : email2));
                    cmd.Parameters.Add(new SqlParameter("@FullName", String.IsNullOrEmpty(FullName) ? (object)DBNull.Value : FullName));
                    cmd.Parameters.Add(new SqlParameter("@CountryOfBirth", String.IsNullOrEmpty(CountryOfBirth) ? (object)DBNull.Value : CountryOfBirth));
                    cmd.Parameters.Add(new SqlParameter("@PlaceOfBirth", String.IsNullOrEmpty(PlaceOfBirth) ? (object)DBNull.Value : PlaceOfBirth));
                    cmd.Parameters.Add(new SqlParameter("@Nationality", String.IsNullOrEmpty(Nationality) ? (object)DBNull.Value : Nationality));
                    cmd.Parameters.Add(new SqlParameter("@AKA", String.IsNullOrEmpty(AKA) ? (object)DBNull.Value : AKA));
                    cmd.Parameters.Add(new SqlParameter("@AgentID", String.IsNullOrEmpty(AID) ? (object)DBNull.Value : AID));
                    cmd.Parameters.Add(new SqlParameter("@SpendingPower", getDefaultMonthlyLimit()));
                    cmd.Parameters.Add(new SqlParameter("@CustomerType", customerType));
                    cmd.Parameters.Add(new SqlParameter("@HasKYC", hasRecordInKyce));
                    cmd.Parameters.Add(new SqlParameter("@Title", title));
                    cmd.Parameters.Add(new SqlParameter("@IsIndividual", "1"));
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", Session["LoggedUserFullName"].ToString()));
                    cmd.ExecuteNonQuery();


                    conn.Close();
                }

                classes.Customer CUST = new classes.Customer();
                CUST.insertIntoAuditLog(LastID.ToString(), Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "ADD", "Customer " + LastID + " has been added");
            }
            catch (Exception ex)
            {

                conn.Close();
                return false;
            }


            return true;

        }

        protected float getDefaultMonthlyLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float limit = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyAUDLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        limit = float.Parse(sdr["SettingsVariable"].ToString());
                    }
                }

                conn.Close();
            }

            return limit;
        }
    }
}