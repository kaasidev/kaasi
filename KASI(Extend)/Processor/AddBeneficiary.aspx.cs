﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace KASI_Extend_.Processor
{
    public partial class AddBeneficiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String theSelectedID = Request.QueryString["CID"].ToString();

            performBeneAdd(theSelectedID, Request.QueryString["BeneLastName"].ToString(), Request.QueryString["BeneFirstNames"].ToString(), Request.QueryString["AdLine1"].ToString(), Request.QueryString["AdLine2"].ToString(), Request.QueryString["Suburb"].ToString(), Request.QueryString["Postcode"].ToString(), Request.QueryString["CountryID"].ToString(), Request.QueryString["Country"].ToString(), Request.QueryString["COB"].ToString(), Request.QueryString["DOB"].ToString(), Request.QueryString["POB"].ToString(), Request.QueryString["National"].ToString(), Request.QueryString["Relation"].ToString(), Request.QueryString["AKA"].ToString(), Request.QueryString["IDDetails"].ToString(), Request.QueryString["State"].ToString());

        }

        protected int FindLastBeneficiaryID(String CID)
        {

            int LastCustomerID = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT MAX(BeneficiaryID) FROM dbo.Beneficiaries WHERE CustomerID = " + CID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                LastCustomerID = Int32.Parse(sdr["BeneficiaryID"].ToString());
                            }
                        }
                        else
                        {
                            LastCustomerID = Int32.Parse(CID + "00");
                        }
                        
                    }
                }
            }

            return LastCustomerID;

        }

        protected void performBeneAdd(String CID, String LastName, String FirstNames, String AddLine1, String AddLine2, String Suburb, String Postcode, String CountryID,  String Country, String COB, String DOB, String PlaceofBirth, String Nationality, String Relationship, String AKA, String IDDetails, String State)
        {
            int LastBeneID = FindLastBeneficiaryID(CID);
            LastBeneID = LastBeneID + 1;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SQLStatement = "INSERT INTO dbo.Beneficiaries (BeneficiaryID, CustomerID, BeneficiaryLastName, BeneficiaryFirstNames,  AddressLine1, AddressLine2, Suburb, Postcode, CountryID, Country, CountryofBirth, DOB, PlaceofBirth, Nationality, Relationship, AKA, NationalityCardID, State, CreatedBy, CreatedDateTime) VALUES (@BeneficiaryID, @CustomerID, @BeneficiaryLastName, @BeneficiaryFirstNames, @AddressLine1, @AddressLine2, @Suburb, @Postcode, @CountryID, @Country, @CountryofBirth, @DOB, @PlaceofBirth, @Nationality, @Relationship, @AKA, @NationalityCardID, @State, @CreatedBy, CURRENT_TIMESTAMP)";

            using (SqlCommand cmd = new SqlCommand(SQLStatement))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@BeneficiaryID", SqlDbType.Int, 10).Value = LastBeneID;
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 10).Value = CID;
                cmd.Parameters.Add("@BeneficiaryName", SqlDbType.NVarChar, 500).Value = FirstNames + ' ' + LastName;
                cmd.Parameters.Add("@BeneficiaryLastName", SqlDbType.NVarChar, 250).Value = LastName;
                cmd.Parameters.Add("@BeneficiaryFirstNames", SqlDbType.NVarChar, 500).Value = FirstNames;
                cmd.Parameters.AddWithValue("@CountryofBirth", COB == "--Select--" ? (object) DBNull.Value : COB);
                cmd.Parameters.AddWithValue("@DOB", DOB==null ? (object) DBNull.Value : DOB);
                cmd.Parameters.AddWithValue("@PlaceofBirth", PlaceofBirth==null ? (object) DBNull.Value : PlaceofBirth);
                cmd.Parameters.AddWithValue("@Nationality", Nationality == "--Select--" ? (object) DBNull.Value : Nationality);
                cmd.Parameters.Add("@Relationship", SqlDbType.NVarChar, 100).Value = Relationship;
                cmd.Parameters.Add("@AKA", SqlDbType.NVarChar, 500).Value = AKA;
                cmd.Parameters.Add("@NationalityCardID", SqlDbType.NVarChar, 50).Value = IDDetails;
                cmd.Parameters.Add("@AddressLine1", SqlDbType.NVarChar, 500).Value = AddLine1;
                cmd.Parameters.Add("@AddressLine2", SqlDbType.NVarChar, 500).Value = AddLine2;
                cmd.Parameters.Add("@Suburb", SqlDbType.NVarChar, 150).Value = Suburb;
                cmd.Parameters.Add("@Postcode", SqlDbType.NVarChar, 10).Value = Postcode;
                cmd.Parameters.AddWithValue("@Country", Country == "-- SELECT --" ? (object) DBNull.Value : Country);
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 100).Value = CountryID;
                cmd.Parameters.Add("@State", SqlDbType.NVarChar, 100).Value = State;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"].ToString();

                cmd.ExecuteNonQuery();
                conn.Close();

                classes.Customer CUST = new classes.Customer();
                CUST.insertIntoAuditLog(CID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "ADD", "Beneficiary " + FirstNames + ' ' + LastName + " has been added");
            }
        }

        public String AddNewBeneficiary(String CID, String BeneName, String AddLine1, String AddLine2, String Suburb, String Postcode, String Country)
        {
            int LastBeneID = FindLastBeneficiaryID(CID);
            LastBeneID = LastBeneID + 1;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SQLStatement = "INSERT INTO dbo.Beneficiaries (BeneficiaryID, CustomerID, BeneficiaryName, AddressLine1, AddressLine2, Suburb, Postcode, Country) VALUES (@BeneficiaryID, @CustomerID, @BeneficiaryName, @AddressLine1, @AddressLine2, @Suburb, @Postcode, @Country)";

            using (SqlCommand cmd = new SqlCommand(SQLStatement))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@BeneficiaryID", SqlDbType.Int, 10).Value = LastBeneID;
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 10).Value = CID;
                cmd.Parameters.Add("@BeneficiaryName", SqlDbType.NVarChar, 500).Value = BeneName;
                cmd.Parameters.Add("@AddressLine1", SqlDbType.NVarChar, 500).Value = AddLine1;
                cmd.Parameters.Add("@AddressLine2", SqlDbType.NVarChar, 500).Value = AddLine2;
                cmd.Parameters.Add("@Suburb", SqlDbType.NVarChar, 150).Value = Suburb;
                cmd.Parameters.Add("@Postcode", SqlDbType.NVarChar, 10).Value = Postcode;
                cmd.Parameters.Add("@Country", SqlDbType.NVarChar, 100).Value = Country;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            return "Successful";
        }
    }
}