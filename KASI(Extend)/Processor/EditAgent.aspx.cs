﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Security;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class EditAgent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String AgentID = Request.QueryString["AgId"].ToString();
            String AgentName = Request.QueryString["AgName"].ToString();
            String AddLine1 = Request.QueryString["AgAddLine1"].ToString();
            String AddLine2 = Request.QueryString["AgAddLine2"].ToString();
            String Suburb = Request.QueryString["AgSuburb"].ToString();
            String State = Request.QueryString["AgState"].ToString();
            String Postcode = Request.QueryString["AgPostcode"].ToString();
            String Phone = Request.QueryString["AgPhone"].ToString();
            String Fax = Request.QueryString["AgFax"].ToString();
            String Email = Request.QueryString["AgEmail"].ToString();
            String ABN = Request.QueryString["AgABN"].ToString();
            String ACN = Request.QueryString["AgACN"].ToString();
            String FlatFee = Request.QueryString["AgFlatFee"].ToString();
            String AddPerc = Request.QueryString["AgAddPerc"].ToString();
            String CommStruc = Request.QueryString["AgCommStruc"].ToString();
            String CommPerc = Request.QueryString["AgCommPerc"].ToString();
            String Countries = Request.QueryString["AgCountries"].ToString();
            String AusRegNum = Request.QueryString["AgAusNum"].ToString();
            String AusRegDate = Request.QueryString["AgAusDate"].ToString();
            String AusExpDate = Request.QueryString["AgAusExpDate"].ToString();
            String BankAccounts = Request.QueryString["AgBanks"].ToString();

            String MAUDLimit = Request.QueryString["MAUDLimit"].ToString();
            String YAUDLimit = Request.QueryString["YAUDLimit"].ToString();
            String MNbLimit = Request.QueryString["MNbLimit"].ToString();
            String YNbLimit = Request.QueryString["YNbLimit"].ToString();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"SET DATEFORMAT dmy UPDATE dbo.Agents SET AgentName=@AgentName,AgentAddress1=@AgentAddress1,
                    AgentAddress2=@AgentAddress2,AgentCity=@AgentCity,AgentState=@AgentState,AgentPostcode=@AgentPostcode,AgentPhone=@AgentPhone,
                    AgentFax=@AgentFax,AgentCommissionStructure=@AgentCommissionStructure,AgentFeeCommission=@AgentFeeCommission,
                    AgentFeeFlat=@AgentFeeFlat,AgentTransGainCommission=@AgentTransGainCommission,AgentABN=@AgentABN,AgentACN=@AgentACN,
                    AgentEmail=@AgentEmail,AustracNumber=@AustracNumber,AustracRegDate=@AustracRegDate,AustracRenewalDate=@AustracRenewalDate, 
                    AlteredBy=@AlteredBy, AlteredDateTime=CURRENT_TIMESTAMP, MonthlyAUDLimit=@MonthlyAUDLimit, YearlyAUDsLimit=@YearlyAUDsLimit, 
                    MonthlyTransLimit=@MonthlyTransLimit, YearlyTransLimit=@YearlyTransLimit WHERE AgentID=" + AgentID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentName", SqlDbType.NVarChar, 100).Value = AgentName;
                cmd.Parameters.Add("@AgentAddress1", SqlDbType.NVarChar, 150).Value = AddLine1;
                cmd.Parameters.Add("@AgentAddress2", SqlDbType.NVarChar, 150).Value = AddLine2;
                cmd.Parameters.Add("@AgentCity", SqlDbType.NVarChar, 100).Value = Suburb;
                cmd.Parameters.Add("@AgentState", SqlDbType.NVarChar, 100).Value = State;
                cmd.Parameters.Add("@AgentPostcode", SqlDbType.NVarChar, 5).Value = Postcode;
                cmd.Parameters.Add("@AgentPhone", SqlDbType.NVarChar, 15).Value = Phone;
                cmd.Parameters.Add("@AgentFax", SqlDbType.NVarChar, 15).Value = Fax;
                cmd.Parameters.Add("@AgentCommissionStructure", SqlDbType.NVarChar, 10).Value = CommStruc;
                cmd.Parameters.Add("@AgentFeeCommission", SqlDbType.Float).Value = AddPerc;
                cmd.Parameters.Add("@AgentFeeFlat", SqlDbType.Money).Value = FlatFee;
                cmd.Parameters.Add("@AgentTransGainCommission", SqlDbType.Float).Value = CommPerc;               
                cmd.Parameters.Add("@AgentABN", SqlDbType.NVarChar, 50).Value = ABN;
                cmd.Parameters.Add("@AgentACN", SqlDbType.NVarChar, 50).Value = ACN;
                cmd.Parameters.Add("@AgentEmail", SqlDbType.NVarChar, 50).Value = Email;
                cmd.Parameters.Add("@AustracNumber", SqlDbType.NVarChar, 50).Value = AusRegNum;
                cmd.Parameters.Add("@MonthlyAUDLimit", SqlDbType.Money).Value = MAUDLimit;
                cmd.Parameters.Add("@YearlyAUDsLimit", SqlDbType.Money).Value = YAUDLimit;
                cmd.Parameters.Add("@MonthlyTransLimit", SqlDbType.Int).Value = MNbLimit;
                cmd.Parameters.Add("@YearlyTransLimit", SqlDbType.Int).Value = YNbLimit;
                if(!String.IsNullOrEmpty(AusRegDate))
                {
                    DateTime ARD = Convert.ToDateTime(AusRegDate);
                    cmd.Parameters.Add("@AustracRegDate", SqlDbType.NVarChar, 50).Value = AusRegDate;
                }
                else
                {
                    cmd.Parameters.Add("@AustracRegDate", SqlDbType.NVarChar, 50).Value = DBNull.Value;
                }
                
                if(!String.IsNullOrEmpty(AusExpDate))
                {
                    DateTime AED = Convert.ToDateTime(AusExpDate);
                    cmd.Parameters.Add("@AustracRenewalDate", SqlDbType.NVarChar, 50).Value = AusExpDate;
                }
                else
                {
                    cmd.Parameters.Add("@AustracRenewalDate", SqlDbType.NVarChar, 50).Value = DBNull.Value;
                }
                
                cmd.Parameters.Add("@AlteredBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"];
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            AssignedAgentCountries(AgentID, Countries);
            AssignedAgentBankAccounts(AgentID, BankAccounts);
            Response.Write("OK");
        }

        private void AssignedAgentBankAccounts(string AID, string BankAccounts)
        {
            DeleteBankToAgent(AID);
            string[] bankArr = BankAccounts.Split(',');

            foreach (var bank in bankArr)
            {
                if (!string.IsNullOrEmpty(bank))
                {
                    SaveBankToAgent(AID, bank);
                }
            }
        }

        private String CreateAgentLoginID(String AgentName, String MUName, String MUEmail)
        {
            Agent agt = new Agent();
            String AID = agt.getAgentIDByName(AgentName);
            String pword = Membership.GeneratePassword(10, 3);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "SET DATEFORMAT dmy INSERT INTO dbo.Logins (FullName, LoginGroup, AdminOverrideCode, CreatedDateTime, CreatedBy, Active, Username, Password, Email, AgentID, LoginType, Activated) VALUES (@FullName, @LoginGroup, @AdminOverrideCode, CURRENT_TIMESTAMP, @CreatedBy, 'Y', @Username, @Password, @Email, @AgentID, 'AGENT', '0')";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@FullName", SqlDbType.NVarChar, 100).Value = MUName;
                cmd.Parameters.Add("@LoginGroup", SqlDbType.Int, 1).Value = 1;
                cmd.Parameters.Add("@AdminOverrideCode", SqlDbType.Int, 4).Value = 1234;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"].ToString();
                cmd.Parameters.Add("@Username", SqlDbType.NVarChar, 100).Value = MUEmail;
                cmd.Parameters.Add("@Password", SqlDbType.NVarChar, 20).Value = pword;
                cmd.Parameters.Add("@Email", SqlDbType.NVarChar, 100).Value = MUEmail;
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 15).Value = AID;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            return AID;
        }

        private void AssignedAgentCountries(String AID, String CountryList)
        {
            Generic gnr = new Generic();

            List<String> Countries = new List<String>();
            Countries = gnr.ConvertStringToList(CountryList, ',');
            MaintainAgentCountries(AID,Countries);
            //DeleteCountryToAgent(AID);
            //DeleteCurrenciesToAgent(AID);
            //foreach (var Country in Countries)
            //{
            //    if (!string.IsNullOrEmpty(Country))
            //    {

            //        saveCountryToAgent(AID, Country);
            //        AssignCurrenciesToAgent(AID, Country);
            //    }
            //}
        }

        private void MaintainAgentCountries(string AID, List<string> Countries)
        {
            List<string> agentCList = GetAgentCountryList(AID);
            List<string> newlyAddedList = new List<string>();
            List<string> deletedList = new List<string>();
            foreach (var item in agentCList)
            {
                if (!Countries.Exists(x => x.EndsWith(item)))
                {
                    deletedList.Add(item);
                    DeleteCountryToAgent(AID, item);


                }
             
            }
            foreach (var item in Countries)//newly added countries
            {
                if (!agentCList.Exists(x => x.EndsWith(item)))
                {
                   // newlyAddedList.Add(item);
                    saveCountryToAgent(AID, item);
                    AssignCurrenciesToAgent(AID, item);

                }

            }
           
        }

        private List<string> GetAgentCountryList(string AID)
        {
            List<string> lst = new List<string>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String CountryID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "select C.CountryID,CountryName from AgentAssignedCountries as A inner join Countries as C on A.CountryID=C.CountryID where A.AgentID=" + AID;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        lst.Add(sdr["CountryName"].ToString());
                    }
                }
                conn.Close();
            }

            return lst;
        }

        private String FindCountryIDByName(String Country)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String CountryID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT CountryID FROM dbo.Countries WHERE CountryName = '" + Country + "'";
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        CountryID = sdr["CountryID"].ToString();
                    }
                }
                conn.Close();
            }

            return CountryID;
        }

        private void saveCountryToAgent(String AID, String Country)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "INSERT INTO dbo.AgentAssignedCountries (AgentID, CountryID) VALUES (@AgentID, @CountryID)";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 3).Value = AID;
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 3).Value = FindCountryIDByName(Country);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void DeleteCountryToAgent(String AID, String Country)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "DELETE FROM dbo.AgentAssignedCountries Where  AgentID=AgentID and CountryID=@CountryID";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 3).Value = AID;
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 3).Value = FindCountryIDByName(Country);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

     

        private void SaveBankToAgent(String AID, String bankAccId)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "INSERT INTO dbo.AgentAssignedBanks (AgentID, MasterBankAccountID) VALUES (@AgentID, @BankAccountID)";

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 3).Value = AID;
                cmd.Parameters.Add("@BankAccountID", SqlDbType.Int, 3).Value = bankAccId;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void DeleteBankToAgent(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "DELETE FROM dbo.AgentAssignedBanks WHERE AgentID="+AID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
               
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void DeleteCurrenciesFromAgent(String AID, String Country)
        {
            classes.Countries Countries = new Countries();
            classes.Currencies curr = new Currencies();
            String CountryID = Countries.getCountryIDByName(Country);
            List<String> Currencies = new List<String>();
            Currencies = curr.getCountryCurrencies(CountryID);
            String MAUDLimit = curr.getMonthlyAUDLimit();
            String YAUDLimit = curr.getYearlyAUDLimit();

            foreach (var Currency in Currencies)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLStmt = "DELETE * FROM dbo.AgentCurrencies WHERE AgentID = " + AID + " AND CountryID = " + CountryID;

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }


        private void AssignCurrenciesToAgent(String AID, String Country)
        {
            classes.Countries Countries = new Countries();
            classes.Currencies curr = new Currencies();
            String CountryID = Countries.getCountryIDByName(Country);
            List<String> Currencies = new List<String>();
            Currencies = curr.getCountryCurrencies(CountryID);
            String MAUDLimit = curr.getMonthlyAUDLimit();
            String YAUDLimit = curr.getYearlyAUDLimit();

            foreach (var Currency in Currencies)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLStmt = "INSERT INTO dbo.AgentCurrencies (AgentID, CurrencyID, CurrencyMonthlyLimit, CurrencyYearlyLimit, AgentSetMonthlyLimit, AgentSetYearlyLimit) VALUES (@AgentID, @CurrencyID, @CurrencyMonthlyLimit, @CurrencyYearlyLimit, @AgentSetMonthlyLimit, @AgentSetYearlyLimit)";

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                    cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = Currency;
                    cmd.Parameters.Add("@CurrencyMonthlyLimit", SqlDbType.Money, 20).Value = MAUDLimit;
                    cmd.Parameters.Add("@CurrencyYearlyLimit", SqlDbType.Money, 20).Value = YAUDLimit;
                    cmd.Parameters.Add("@AgentSetMonthlyLimit", SqlDbType.Money, 20).Value = MAUDLimit;
                    cmd.Parameters.Add("@AgentSetYearlyLimit", SqlDbType.Money, 20).Value = YAUDLimit;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                AssignCurrencyMargins(AID, Currency);
                AssignCurrencyRate(AID, Currency);
                AssignTransactionFeeTiers(AID,Currency,CountryID);
            }

        }
        private void DeleteCurrenciesToAgent(String AID, String Country)
        {
            classes.Countries Countries = new Countries();
            classes.Currencies curr = new Currencies();
            String CountryID = Countries.getCountryIDByName(Country);
            List<String> Currencies = new List<String>();
            Currencies = curr.getCountryCurrencies(CountryID);
           

            foreach (var Currency in Currencies)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLStmt = "DELETE FROM dbo.AgentCurrencies where AgentID=@AgentID and CurrencyID=@CurrencyID";

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                    cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = Currency;
                    
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                DeleteCurrencyMargins(AID, Currency);
                DeleteCurrencyRate(AID, Currency);
                DeleteTransactionFeeTiers(AID, Currency, CountryID);
            }

        }

        private void DeleteCurrenciesToAgent(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "DELETE FROM dbo.AgentCurrencies WHERE AgentID="+AID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
               
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void AssignCurrencyMargins(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"INSERT INTO dbo.AgentCurrencyMargins (AgentID, CurrencyID, UpDown, Margin) VALUES (@AgentID, @CurrencyID, @UpDown, @Margin)";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CID;
                cmd.Parameters.Add("@UpDown", SqlDbType.NVarChar, 20).Value = "Down";
                cmd.Parameters.Add("@Margin", SqlDbType.Money, 20).Value = 0.00;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void DeleteCurrencyMargins(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"DELETE FROM dbo.AgentCurrencyMargins Where AgentID=@AgentID and CurrencyID=@CurrencyID";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CID;
               
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void AssignCurrencyRate(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"SET DATEFORMAT dmy INSERT INTO dbo.AgentCurrencyRates (AgentID, CurrencyID, AssignedRate, AssignedDate, CreatedDateTime) VALUES (@AgentID, @CurrencyID, @AssignedRate, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CID;
                cmd.Parameters.Add("@AssignedRate", SqlDbType.Money, 20).Value = 0.00;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        private void DeleteCurrencyRate(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"DELETE FROM dbo.AgentCurrencyRates Where AgentID=@AgentID and CurrencyID=@CurrencyID";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CID;
               
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void AssignTransactionFeeTiers(String AID, String CurrencyID, String CountryID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"INSERT INTO dbo.AgentTransactionFeeTiers (AgentID, CountryID, CurrencyID, Tier1From, Tier1To, Tier1Amount, Tier2From, Tier2To, Tier2Amount, Tier3From, Tier3To, Tier3Amount) 
                            VALUES (@AgentID, @CountryID, @CurrencyID, @Tier1From, @Tier1To, @Tier1Amount, @Tier2From, @Tier2To, @Tier2Amount, @Tier3From, @Tier3To, @Tier3Amount)";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CurrencyID;
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 20).Value = CountryID;
                cmd.Parameters.Add("@Tier1From", SqlDbType.Money, 5).Value = 1.00;
                cmd.Parameters.Add("@Tier1To", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier1Amount", SqlDbType.Money, 20).Value = 0.00;
                cmd.Parameters.Add("@Tier2From", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier2To", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier2Amount", SqlDbType.Money, 20).Value = 0.00;
                cmd.Parameters.Add("@Tier3From", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier3To", SqlDbType.Money, 5).Value = 0.00;
                cmd.Parameters.Add("@Tier3Amount", SqlDbType.Money, 20).Value = 0.00;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        private void DeleteTransactionFeeTiers(String AID, String CurrencyID, String CountryID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = @"DELETE FROM dbo.AgentTransactionFeeTiers where AgentID=@AgentID and  CountryID=@CountryID and CurrencyID=@CurrencyID";
            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 5).Value = AID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 5).Value = CurrencyID;
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 20).Value = CountryID;
                
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}