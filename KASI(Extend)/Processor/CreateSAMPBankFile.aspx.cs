﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class CreateSAMPBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions transClass = new classes.Transactions();
            String BankCode = Request.QueryString["BankCode"].ToString();
            String delimiter = "|";
            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts bAccount = new BeneficiaryAccounts();
            float RAmount = 0;
            String BeneID = String.Empty;
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filename = "SMP - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            String now = String.Empty;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode;
            String outputString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString = outputString + sdr["TransactionID"].ToString() + delimiter;
                            outputString = outputString + now + delimiter;
                            outputString = outputString + sdr["RemittedCurrency"].ToString() + delimiter;
                            RAmount = float.Parse(sdr["RemittedAmount"].ToString());
                            outputString = outputString + String.Format("{0:N2}", RAmount).Replace(",", "") + delimiter;
                            outputString = outputString + cust.getCustomerFullName(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + sdr["CustomerID"].ToString() + delimiter;
                            outputString = outputString + cust.getCustomerMobile(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + cust.getCustomerFullAddress(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + sdr["BeneficiaryName"].ToString() + delimiter;

                            BeneID = bene.getBeneficiaryNIC(sdr["BeneficiaryID"].ToString());
                            if (BeneID == "")
                            {
                                BeneID = bene.getBeneficiaryPassport(sdr["BeneficiaryID"].ToString());

                                if (BeneID == "")
                                {
                                    BeneID = "";
                                }
                            }

                            outputString = outputString + BeneID + delimiter;
                            outputString = outputString + bene.getBeneficiaryPhone(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryBranchCode(sdr["TransactionID"].ToString()) + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryBranchName(sdr["TransactionID"].ToString()) + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryBankCode(sdr["TransactionID"].ToString()) + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryBankNameOnTID(sdr["TransactionID"].ToString()) + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()) + delimiter;

                            String SendingBank = bAccount.getBeneficiaryBankCode(sdr["TransactionID"].ToString());

                            if (SendingBank != "7278")
                            {
                                outputString = outputString + "SLI" + Environment.NewLine;
                            }
                            else
                            {
                                outputString = outputString + "SBA" + Environment.NewLine;
                            }
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                        transClass.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into Sampath Bank Bank File");
                    }
                    Response.Write("Sampath File Successfully Created^"+filename);
                }
                else
                {
                    Response.Write("Sampath File Could Not Be Created^");
                }
            }

            //Response.Write(outputString);
        }
    }
}