﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class sendPasswordResetDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String Email = Request.QueryString["Email"].ToString();
            

            classes.EmailCustomer EMC = new classes.EmailCustomer();
            String result = EMC.SendPasswordToCustomer(Email);

            Response.Write(result);
        }
    }
}