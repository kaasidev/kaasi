﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class UpdateTransferFees : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CountryID = Request.QueryString["CURID"].ToString();
            String CurrencyID = Request.QueryString["COUTID"].ToString();

            String Tier1Amount = Request.QueryString["T1A"].ToString();
            String Tier1To = Request.QueryString["T1T"].ToString();

            String Tier2Amount = Request.QueryString["T2A"].ToString();
            String Tier2To = Request.QueryString["T2T"].ToString();

            String Tier3Amount = Request.QueryString["T3A"].ToString();

            String increment = "0.01";

            float FTier2From = float.Parse(Tier1To) + float.Parse(increment);
            float FTier3From = float.Parse(Tier2To) + float.Parse(increment);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = @"UPDATE dbo.AgentTransactionFeeTiers SET Tier1To = @Tier1To, Tier1Amount = @Tier1Amount, Tier2From = @Tier2From
                , Tier2To = @Tier2To, Tier2Amount = @Tier2Amount, Tier3From = @Tier3From, Tier3Amount = @Tier3Amount 
                WHERE AgentID = " + Session["AgentID"].ToString() + " AND CurrencyID = " + CurrencyID + " AND CountryID = " + CountryID;

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@Tier1To", SqlDbType.Money).Value = Tier1To;
                cmd.Parameters.Add("@Tier1Amount", SqlDbType.Money).Value = Tier1Amount;
                cmd.Parameters.Add("@Tier2From", SqlDbType.Money).Value = FTier2From;
                cmd.Parameters.Add("@Tier2To", SqlDbType.Money).Value = Tier2To;
                cmd.Parameters.Add("@Tier2Amount", SqlDbType.Money).Value = Tier2Amount;
                cmd.Parameters.Add("@Tier3From", SqlDbType.Money).Value = FTier3From;
                cmd.Parameters.Add("@Tier3Amount", SqlDbType.Money).Value = Tier3Amount;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}