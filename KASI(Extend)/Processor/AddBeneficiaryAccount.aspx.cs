﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;


namespace KASI_Extend_.Processor
{
    public partial class AddBeneficiaryAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String BID = Request.QueryString["BID"].ToString();
            int LastID = LastAccountID(BID);
            LastID = LastID + 1;

            performAccountAdd(LastID, Request.QueryString["CID"].ToString(), BID, Request.QueryString["BankID"].ToString(), Request.QueryString["BankName"].ToString(), Request.QueryString["BranchID"].ToString(), Request.QueryString["BranchName"].ToString(), Request.QueryString["AccNum"].ToString(), Request.QueryString["AccName"].ToString(), Request.QueryString["CountryID"].ToString(), Request.QueryString["CurrencyID"].ToString(), Request.QueryString["CurrencyName"].ToString());

        }

        protected int LastAccountID(String BID)
        {
            int LastAccID = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT MAX(AccountID) AS ACCID FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                if (sdr["ACCID"].ToString() == "")
                                {
                                    LastAccID = Int32.Parse(BID + "00");
                                }
                                else
                                {
                                    LastAccID = Int32.Parse(sdr["ACCID"].ToString());
                                }
                                
                            }
                        }
                        else
                        {
                            LastAccID = Int32.Parse(BID + "00");
                        }
                        
                    }
                }
                conn.Close();
            }

            return LastAccID;
        }

        protected void performAccountAdd(int LastID, String CustID, String BeneID, String BankID, String BankName, String BranchID, String BranchName, String AccNum, String AccName, String CountryID, String CurrencyID, String CurrencyName)
        {
            classes.Countries CTR = new classes.Countries();
            //String CID = CTR.getCountryMasterCurrencyByID(CountryID);

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SQLStatement = "INSERT INTO dbo.BeneficiaryPaymentMethods (AccountID, CustomerID, BeneficiaryID, BankID, BankName, BranchID, BranchName, AccountNumber, PaymentMethod, Currency, NRFC, AccountType, NRFCByKap, AcctStatus, AccountName, CountryID, CurrencyID, CreatedBy, CreatedDateTime) VALUES (@AccountID, @CustomerID, @BeneficiaryID, @BankID, @BankName, @BranchID, @BranchName, @AccountNumber, 'Credited to the acc.', @Currency, 'NO', @AccountType, '0', 'Y', @AccountName, @CountryID, @CurrencyID, @CreatedBy, CURRENT_TIMESTAMP)";

            using (SqlCommand cmd = new SqlCommand(SQLStatement))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@AccountID", SqlDbType.NVarChar, 10).Value = LastID;
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 10).Value = CustID;
                cmd.Parameters.Add("@BeneficiaryID", SqlDbType.NVarChar, 10).Value = BeneID;
                cmd.Parameters.Add("@BankID", SqlDbType.NVarChar, 10).Value = BankID;
                cmd.Parameters.Add("@BankName", SqlDbType.NVarChar, 100).Value = BankName;
                cmd.Parameters.Add("@BranchID", SqlDbType.NVarChar, 10).Value = BranchID;
                cmd.Parameters.Add("@BranchName", SqlDbType.NVarChar, 100).Value = BranchName;
                cmd.Parameters.Add("@AccountNumber", SqlDbType.NVarChar, 30).Value = AccNum;
                cmd.Parameters.Add("@AccountName", SqlDbType.NVarChar).Value = AccName;
                cmd.Parameters.Add("@AccountType", SqlDbType.NVarChar, 10).Value = CurrencyName;
                cmd.Parameters.Add("@Currency", SqlDbType.NVarChar, 10).Value = CurrencyName;
                cmd.Parameters.Add("@CountryID", SqlDbType.Int, 10).Value = CountryID;
                cmd.Parameters.Add("@CurrencyID", SqlDbType.Int, 10).Value = CurrencyID;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUserFullName"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            classes.Beneficiary BENE = new classes.Beneficiary();
            String BeneName = String.Empty;
            BeneName = BENE.getBeneficiaryName(BeneID);

            classes.Customer CUST = new classes.Customer();
            CUST.insertIntoAuditLog(CustID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "ADD", "Account Number " + AccNum + " has been added to Beneficiary " + BeneName);
        }
    }
}