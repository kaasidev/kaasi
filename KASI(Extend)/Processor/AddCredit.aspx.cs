﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace KASI_Extend_.Processor
{
    public partial class AddCredit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AddCreditToCustomer(Request.QueryString["CID"].ToString(), Request.QueryString["CreditAmount"].ToString(), Request.QueryString["CreditDescID"].ToString(), Request.QueryString["CreditDesc"].ToString());
        }

        public String AddCreditToCustomer(String CID, String DollarAmount, String DepositMethodID, String Description)
        {
            String CreditConfirmedFlag = String.Empty;


                CreditConfirmedFlag = "Y";

                String AgentPro = String.Empty;

                if (Session["TypeOfLogin"].ToString() == "Agent")
                {
                    AgentPro = Session["AgentID"].ToString();
                }
                else
                {
                    AgentPro = "0";
                }

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SqlStmt = "INSERT INTO dbo.CreditTransactions (CustomerID, CreditAmount, MasterBankAccountID, DepositMethod, Type, CreditConfirmed, CreatedDateTime, CreatedBy, AgentID) VALUES (@CustomerID, @CreditAmount, @DepositMethodID, @DepositMethod, 'CREDIT', @CreditConfirmed, CURRENT_TIMESTAMP, '" + Session["LoggedUserFullName"].ToString() + "', @AgentID)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@CustomerID", SqlDbType.NVarChar, 100).Value = CID;
                cmd.Parameters.Add("@CreditAmount", SqlDbType.NVarChar, 100).Value = DollarAmount;
                cmd.Parameters.Add("@DepositMethod", SqlDbType.NVarChar, 100).Value = Description;
                cmd.Parameters.Add("@DepositMethodID", SqlDbType.Int, 20).Value = DepositMethodID;
                cmd.Parameters.Add("@CreditConfirmed", SqlDbType.NVarChar, 10).Value = CreditConfirmedFlag;
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 10).Value = Int32.Parse(AgentPro);
                //cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedUser"].ToString();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            classes.Customer CUST = new classes.Customer();
            CUST.insertIntoAuditLog(CID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "ADD", "$" + DollarAmount + " has been added to customer " + CID);

            float currentBalance = 0;
            float unclearedBalance = 0;

            float AddedCurrentBalance = 0;
            float AddedUnclearedBalance = 0;

            #region RetrieveCurrentBalance
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AccountBalance, UnclearedFunds FROM dbo.Customers WHERE CustomerID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["AccountBalance"].Equals(DBNull.Value))
                        {
                            currentBalance = 0;
                        }
                        else
                        {
                            currentBalance = float.Parse(sdr["AccountBalance"].ToString());
                        }

                        if (sdr["UnclearedFunds"].Equals(DBNull.Value))
                        {
                            unclearedBalance = 0;
                        }
                        else
                        {
                            unclearedBalance = float.Parse(sdr["UnclearedFunds"].ToString());
                        }
                        
                    }
                }

                conn.Close();

            }
            #endregion

            AddedCurrentBalance = currentBalance + float.Parse(DollarAmount);
            AddedUnclearedBalance = unclearedBalance + float.Parse(DollarAmount);

            #region updateCustomerBalance
            string sqlStmt2 = "UPDATE dbo.Customers SET AccountBalance = @CurrentBalance, UnclearedFunds = @UnclearedFunds WHERE CustomerID = @CustomerID";

            using (SqlCommand cmd3 = new SqlCommand(sqlStmt2))
            {
                cmd3.Connection = conn;
                conn.Open();

                cmd3.Parameters.Add("@CurrentBalance", SqlDbType.NVarChar, 100).Value = AddedCurrentBalance;
                cmd3.Parameters.Add("@UnclearedFunds", SqlDbType.NVarChar, 100).Value = unclearedBalance;

                cmd3.Parameters.Add("@CustomerID", SqlDbType.NVarChar, 100).Value = CID;
                cmd3.ExecuteNonQuery();
                conn.Close();
            }

            return "OK";
        }

            #endregion

        
    }
}