﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend.Processor
{
    public partial class addNewInTransNote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InwardTransNoteService ITNServ = new InwardTransNoteService(new UnitOfWorks(new KaprukaEntities()));

            InwardTransNote ITN = new InwardTransNote();
            ITN.CustomerID = Int32.Parse(Request.QueryString["CID"].ToString());
            ITN.TransactionID = Int32.Parse(Request.QueryString["TID"].ToString());
            ITN.Notes = Request.QueryString["Notes"];
            ITN.CreatedBy = Session["LoggedPersonName"].ToString();
            ITN.CreatedByAgent = Session["LoggedUserFullName"].ToString();
            ITN.CreatedDateTime = DateTime.Now;

            ITNServ.Add(ITN);

            InTransAuditLogServicecs ITALServ = new InTransAuditLogServicecs(new UnitOfWorks(new KaprukaEntities()));

            InTransAuditLog ITAL = new InTransAuditLog();
            ITAL.TransactionID = Int32.Parse(Request.QueryString["TID"].ToString());
            ITAL.AuditDateTime = DateTime.Now;
            ITAL.Username = Session["LoggedPersonName"].ToString();
            ITAL.AgentName = Session["LoggedCompany"].ToString();
            ITAL.ActionType = "ADD";
            ITAL.Description = "has added a new note";

            ITALServ.Add(ITAL);

            Response.Write("OK");
        }
    }
}