﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class UpdateMasterCompanyRateMargins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                classes.Currencies Curr = new Currencies();

                string valueList = Request.QueryString["curr"].ToString();
                string[] arr = valueList.Split(',');

                foreach (var item in arr)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] subArr = item.Split('^');
                        string value = subArr[0];
                        string currencyId = subArr[1];

                        string CurrencyID = Curr.getCurrencyIDByCurrencyCode(currencyId);

                        SqlConnection conn = new SqlConnection();
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                        string StmtSql = "INSERT INTO dbo.MasterCompanyBuyRates (CurrencyID, BuyingRate, CreatedBy, CreatedDateTime) VALUES ('" + CurrencyID + "', " + subArr[0] + ", '" + Session["LoggedUserFullName"].ToString() + "', CURRENT_TIMESTAMP)";
                        using (SqlCommand cmd = new SqlCommand(StmtSql))
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                }

                string secondValueList = Request.QueryString["curr2"].ToString();
                string[] arr2 = secondValueList.Split(',');

                foreach (var item in arr2)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] subArr = item.Split('^');
                        string value = subArr[0];
                        string currencyId = subArr[1];

                        string CurrencyID = Curr.getCurrencyIDByCurrencyCode(currencyId);

                        SqlConnection conn = new SqlConnection();
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                        string StmtSql = "UPDATE dbo.MasterCompanyRateMargins SET Margin=" + subArr[0] + ", CreatedBy = '" + Session["LoggedUserFullName"].ToString() + "', CreatedDateTime = CURRENT_TIMESTAMP WHERE CurrencyID = " + CurrencyID;
                        using (SqlCommand cmd = new SqlCommand(StmtSql))
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                }

                UpdateAgentGivenRates();
                Response.Write("1");
            }

            catch (Exception ex)
            {
                Response.Write("0^" + ex.ToString());
            }
            
        }

        protected void UpdateAgentGivenRates()
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "startLoadingDialog", "openLoadingDIV();", true);
            classes.Agent AGT = new classes.Agent();
            classes.Currencies CURR = new classes.Currencies();
            List<String> EXGainAgentList = new List<String>();
            List<String> AgentCurrencyList = new List<String>();
            EXGainAgentList = AGT.getAgentsOnExGainCommission();
            float AssigningRate = 0;

            foreach (var Agent in EXGainAgentList)
            {
                AgentCurrencyList = AGT.getAgentCurrencyList(Agent);

                foreach (var Currency in AgentCurrencyList)
                {
                    AssigningRate = CURR.getAgentAssignedRateForCurrencyFromMasterRate(Currency);
                    AGT.UpdateAgentAssignedRateOnAgentIDandCurrencyID(Agent, Currency, AssigningRate);
                }

            }
            ScriptManager.RegisterStartupScript(this, GetType(), "stopLoadingDIV", "closeLoadingDIV();", true);
        }
    }
}