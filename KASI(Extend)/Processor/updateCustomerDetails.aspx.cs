﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class updateCustomerDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

                String FullName = Request.QueryString["firstnames"].ToString() + ' ' + Request.QueryString["lastname"].ToString();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

                string SqlStmt = "UPDATE dbo.Customers SET FirstName=@FirstName, LastName=@LastName, UnitNo=@UnitNo, StreetNo=@StreetNo, StreetName=@StreetName, StreetType=@StreetType, Suburb=@Suburb, State=@State, Postcode=@Postcode, TelHome=@TelHome, TelWork=@TelWork, Mobile=@Mobile, EmailAddress=@EmailAddress, EmailAddress2=@EmailAddress2, Occupation=@Occupation, FullName = @FullName, CountryOfBirth=@CountryOfBirth, DOB=@DOB, PlaceOfBirth=@PlaceOfBirth, Nationality=@Nationality, AKA=@AKA, Country=@Country, AccountPassword=@AccountPassword,  Notes=@Notes,Title=@Title  WHERE CustomerID = @CID";

                using (SqlCommand cmd = new SqlCommand(SqlStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@CID", SqlDbType.Int).Value = Request.QueryString["CID"].ToString();
                    cmd.Parameters.Add(new SqlParameter("@LastName", String.IsNullOrEmpty(Request.QueryString["lastname"].ToString()) ? (object)DBNull.Value : Request.QueryString["lastname"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@FirstName", String.IsNullOrEmpty(Request.QueryString["firstnames"].ToString()) ? (object)DBNull.Value : Request.QueryString["firstnames"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@UnitNo", String.IsNullOrEmpty(Request.QueryString["UnitNo"].ToString()) ? (object)DBNull.Value : Request.QueryString["UnitNo"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@StreetNo", String.IsNullOrEmpty(Request.QueryString["StreetNo"].ToString()) ? (object)DBNull.Value : Request.QueryString["StreetNo"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@StreetName", String.IsNullOrEmpty(Request.QueryString["StreetName"].ToString()) ? (object)DBNull.Value : Request.QueryString["StreetName"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@StreetType", String.IsNullOrEmpty(Request.QueryString["StreetType"].ToString()) ? (object)DBNull.Value : Request.QueryString["StreetType"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Suburb", String.IsNullOrEmpty(Request.QueryString["suburb"].ToString()) ? (object)DBNull.Value : Request.QueryString["suburb"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@State", String.IsNullOrEmpty(Request.QueryString["state"].ToString()) ? (object)DBNull.Value : Request.QueryString["state"].ToString().ToUpper()));
                    cmd.Parameters.Add(new SqlParameter("@Postcode", String.IsNullOrEmpty(Request.QueryString["postcode"].ToString()) ? (object)DBNull.Value : Request.QueryString["postcode"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@TelHome", String.IsNullOrEmpty(Request.QueryString["homephone"].ToString()) ? (object)DBNull.Value : Request.QueryString["homephone"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@TelWork", String.IsNullOrEmpty(Request.QueryString["workphone"].ToString()) ? (object)DBNull.Value : Request.QueryString["workphone"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Mobile", String.IsNullOrEmpty(Request.QueryString["mobile"].ToString()) ? (object)DBNull.Value : Request.QueryString["mobile"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@DOB", String.IsNullOrEmpty(Request.QueryString["dob"].ToString()) ? (object)DBNull.Value : Request.QueryString["dob"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@EmailAddress", String.IsNullOrEmpty(Request.QueryString["email1"].ToString()) ? (object)DBNull.Value : Request.QueryString["email1"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@EmailAddress2", String.IsNullOrEmpty(Request.QueryString["email2"].ToString()) ? (object)DBNull.Value : Request.QueryString["email2"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@FullName", String.IsNullOrEmpty(FullName) ? (object)DBNull.Value : FullName));
                    cmd.Parameters.Add(new SqlParameter("@CountryOfBirth", String.IsNullOrEmpty(Request.QueryString["COB"].ToString()) ? (object)DBNull.Value : Request.QueryString["COB"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@PlaceOfBirth", String.IsNullOrEmpty(Request.QueryString["POB"].ToString()) ? (object)DBNull.Value : Request.QueryString["POB"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Nationality", String.IsNullOrEmpty(Request.QueryString["National"].ToString()) ? (object)DBNull.Value : Request.QueryString["National"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@AKA", String.IsNullOrEmpty(Request.QueryString["AKA"].ToString()) ? (object)DBNull.Value : Request.QueryString["AKA"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Country", String.IsNullOrEmpty(Request.QueryString["Country"].ToString()) ? (object)DBNull.Value : Request.QueryString["Country"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Occupation", String.IsNullOrEmpty(Request.QueryString["Occupation"].ToString()) ? (object)DBNull.Value : Request.QueryString["Occupation"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Notes", String.IsNullOrEmpty(Request.QueryString["Notes"].ToString()) ? (object)DBNull.Value : Request.QueryString["Notes"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@AccountPassword", String.IsNullOrEmpty(Request.QueryString["password"].ToString()) ? (object)DBNull.Value : Request.QueryString["password"].ToString()));
                    cmd.Parameters.Add(new SqlParameter("@Title", String.IsNullOrEmpty(Request.QueryString["title"].ToString()) ? (object)DBNull.Value : Request.QueryString["title"].ToString()));
                    cmd.ExecuteNonQuery();


                    conn.Close();
                }


            

            
        }

    }
}