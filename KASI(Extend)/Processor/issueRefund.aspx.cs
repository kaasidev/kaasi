﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class issueRefund : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String RefundAmt = Request.QueryString["Amt"].ToString();
            String RefundMethod = Request.QueryString["Method"].ToString();

            classes.Customer CUST = new classes.Customer();


            float currentBalance = float.Parse(CUST.getCustomerAccountBalance(CID));
            float RefundTotal = currentBalance - float.Parse(RefundAmt);
            float AmtRefunded = float.Parse(RefundAmt);

            AmtRefunded = AmtRefunded * -1;

            // Update the DEBIT into the CreditDebit
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = @"INSERT INTO dbo.CreditTransactions (CustomerID, CreditAmount, DepositMethod, Type, 
                CreditConfirmed, CreatedDatetime, CreatedBy, AgentID) VALUES (@CustomerID, @CreditAmount, @DepositMethod, @Type, 
                @CreditConfirmed, CURRENT_TIMESTAMP, @CreatedBy, @AgentID)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@CustomerID", CID);
                cmd.Parameters.AddWithValue("@CreditAmount", AmtRefunded);
                cmd.Parameters.AddWithValue("@Type", "DEBIT");
                cmd.Parameters.AddWithValue("@CreditConfirmed", 'Y');
                cmd.Parameters.AddWithValue("@DepositMethod", "Refund to " + RefundMethod);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["LoggedPersonName"] + "(" + Session["LoggedUserFullName"].ToString() + ")");
                cmd.Parameters.AddWithValue("@AgentID", Session["AgentID"]);
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            CUST.insertIntoAuditLog(CID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "A refund of $" + AmtRefunded + " has been issued to the customer");

            // Update the Customer Balance

            String SqlStmt2 = "UPDATE dbo.Customers SET AccountBalance=@AccountBalance WHERE CustomerID = " + CID;

            using (SqlCommand cmd = new SqlCommand(SqlStmt2))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@AccountBalance", RefundTotal);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}