﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class sendBankDetailsToCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String CustomMessage = Request.QueryString["CMessage"].ToString();
            String CC = Request.QueryString["CC"].ToString();

            classes.EmailCustomer ECS = new classes.EmailCustomer();

            String SendResult = ECS.SendAccountDetailstoCustomer(CID, CustomMessage, CC);

            Response.Write(SendResult);
        }
    }
}