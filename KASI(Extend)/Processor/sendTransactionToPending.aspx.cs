﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class sendTransactionToPending : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = Request.QueryString["TID"].ToString();
            String ReviewReason = Request.QueryString["RR"].ToString();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "UPDATE dbo.Transactions SET Status = 'PENDING', Notes = @Notes  WHERE TransactionID = " + TID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@Notes", SqlDbType.NVarChar, 500).Value = ReviewReason;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            Response.Write("OK");
        }
    }
}