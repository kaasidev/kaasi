﻿using Kapruka.Domain;
using Kapruka.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class checkKYCAgent : System.Web.UI.Page
    {
        String AgentID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            AgentID = Request.QueryString["AgId"].ToString();
            var ageId = Convert.ToInt32(AgentID);
            var cService = new CustomerHandlerService();
            var agent = cService.GetAgentById(ageId);
            var reCallResult = new ResultKycCall();
            reCallResult = CallMerchant(ageId, cService, agent, reCallResult);
            Response.Write(reCallResult.Message);

        }

        private ResultKycCall CallMerchant(int customerId, CustomerHandlerService cService,
        AgentKYC customerForKyc, ResultKycCall resulCall)
        {
            var merchant = new MerchantKyced();
            merchant.profile = "FLEXEWALLET";
            if (customerForKyc.AgentName != null)
                merchant.dba = customerForKyc.AgentName;
            else
                merchant.dba = null;

            merchant.amn = customerForKyc.AgentName;

            merchant.website = null;
            if (customerForKyc.AgentCity != null)
                merchant.ac = customerForKyc.AgentCity;
            else
                merchant.ac = null;
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    merchant.aco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    merchant.aco = "AU";
            }
            else
                merchant.aco = "AU";
            if (customerForKyc.AgentName != null)
                merchant.afn = customerForKyc.AgentName;
            else
                merchant.afn = null;
            if (customerForKyc.AgentName != null)
                merchant.aln = customerForKyc.AgentName;
            else
                merchant.aln = null;
            if (customerForKyc.AgentPhone != null)
                merchant.aph = customerForKyc.AgentPhone;
            else
                merchant.aph = null;
            if (customerForKyc.AgentPostcode != null)
                merchant.az = customerForKyc.AgentPostcode;
            else
                merchant.az = null;

            if (customerForKyc.AgentState != null)
                merchant.@as = customerForKyc.AgentState;
            else
                merchant.@as = null;
            if (customerForKyc.AgentAddress1 != null)
                merchant.asn = customerForKyc.AgentAddress1;
            else
                merchant.asn = null;
            if (customerForKyc.AgentABN != null)
                merchant.ataxId = customerForKyc.AgentABN;
            else
                merchant.ataxId = null;

            merchant.bin = null;

            var runBy = "";
            if (Session["usernameLogged"] != null)
                runBy = Session["usernameLogged"].ToString();
            resulCall = cService.CreateKycedAgent(merchant, customerId, runBy);
            return resulCall;
        }
    }
}