﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;

namespace KASI_Extend.Processor
{
    public partial class UpdateAffiliateSMTPSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AgentService AGTServ = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            String SMTPServer = Request.QueryString["Name"].ToString().Trim();
            var Port = Request.QueryString["Port"].ToString().Trim();
            var Email = Request.QueryString["Email"].ToString().Trim();
            var SSL = Request.QueryString["SSL"].ToString();
            var Username = Request.QueryString["UName"].ToString().Trim();
            var Password = Request.QueryString["PWord"].ToString().Trim();

            var AgentID = Int32.Parse(Session["AgentID"].ToString());


            try
            {
                var SetObj = AGTServ.GetAll(x => x.AgentID == AgentID, null, "").SingleOrDefault();
                SetObj.SMTPServerName = SMTPServer;
                SetObj.SMTPPort = Port;
                SetObj.SSLEnabled = SSL;
                SetObj.SMTPUsername = Username;
                SetObj.SMTPassword = Password;
                AGTServ.Update(SetObj);

                Response.Write("OK");
            }
            catch (Exception ex)
            {
                Response.Write("ERROR");
            }
            

            
        }
    }
}