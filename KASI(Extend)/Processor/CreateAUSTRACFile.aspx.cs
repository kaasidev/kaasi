﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace KASI_Extend_.Processor
{
    public partial class CreateAUSTRACFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode iftiNode = doc.CreateElement("ifti-eList");
            XmlAttribute iftiNodeAttr1 = doc.CreateAttribute("versionMajor");
            iftiNodeAttr1.Value = "1";
            XmlAttribute iftiNodeAttr2 = doc.CreateAttribute("versionMinor");
            iftiNodeAttr2.Value = "2";
            XmlAttribute iftiNodeAtt3 = doc.CreateAttribute("xsi", "schemaLocation", " ");
            iftiNodeAtt3.Value = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2IFTI-E-1-2.xsd";
            XmlAttribute iftiNoteAttr4 = doc.CreateAttribute("xmlns");
            iftiNoteAttr4.Value = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2";
            XmlAttribute iftiNoteAttr5 = doc.CreateAttribute("xmlns", "xsi", "http://www.w3.org/2000/xmlns/");
            iftiNoteAttr5.Value = "http://www.w3.org/2001/XMLSchema-instance";
            iftiNode.Attributes.Append(iftiNodeAttr1);
            iftiNode.Attributes.Append(iftiNodeAttr2);
            //iftiNode.Attributes.Append(iftiNodeAtt3);
            iftiNode.Attributes.Append(iftiNoteAttr4);
            //iftiNode.Attributes.Append(iftiNoteAttr5);

            XmlNode ReNumber = doc.CreateElement("reNumber");
            ReNumber.AppendChild(doc.CreateTextNode("0001983"));

            iftiNode.AppendChild(ReNumber);
            doc.AppendChild(iftiNode);



            doc.Save(@"C:\TestFiles\AustracReport.xml");
        }
    }
}