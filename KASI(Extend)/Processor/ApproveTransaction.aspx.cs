﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class ApproveTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TransID = String.Empty;
            String RoutingBank = String.Empty;
            TransID = Request.QueryString["TransID"].ToString();
            RoutingBank = Request.QueryString["RBank"].ToString();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            String sqlStmt = "UPDATE dbo.Transactions SET RoutingBank=@RoutingBank, ProcessTransaction='Y', InsertedIntoBankFile='N', InsertedIntoAustracFile='N', FundsInBank='Y', Status='AWAITING TRANSFER', ApprovalDate = CURRENT_TIMESTAMP, ApprovedBy = @ApprovedBy WHERE TransactionID=@TransactionID";

            using (SqlCommand cmd = new SqlCommand(sqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@TransactionID", SqlDbType.Int, 100).Value = TransID;
                cmd.Parameters.Add("@RoutingBank", SqlDbType.Int, 20).Value = RoutingBank;
                cmd.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedPersonName"].ToString() + "(" + Session["LoggedUserFullName"] + ")";
                cmd.ExecuteNonQuery();
            }
            conn.Close();

            classes.Transactions TRANS = new classes.Transactions();
            TRANS.insertAuditRecord(TransID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + TransID + " has been approved");

            EmailCustomer CEmail = new EmailCustomer();
            //CEmail.SendEmailWhenApproved(TransID);

        }
    }
}