﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class AddCustomerBusiness : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                CustomerService ser = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                Customer customer = new Customer();
                string BBusinessName = Request.QueryString["BBusinessName"];
                string BABN = Request.QueryString["BABN"];
                string BTradingName = Request.QueryString["BTradingName"];
                string BAddressLine1 = Request.QueryString["BAddressLine1"];
                string BAddressLine2 = Request.QueryString["BAddressLine2"];
                string BSuburb = Request.QueryString["BSuburb"];
                string BState = Request.QueryString["BState"];
                string BPostcode = Request.QueryString["BPostcode"];
                string BPostalAddressLine1 = Request.QueryString["BPostalAddressLine1"];
                string BPostalAddressLine2 = Request.QueryString["BPostalAddressLine2"];
                string BPostalSuburb = Request.QueryString["BPostalSuburb"];
                string BPostalState = Request.QueryString["BPostalState"];
                string BPostalPostcode = Request.QueryString["BPostalPostcode"];
                string BLastName = Request.QueryString["BLastName"];
                string BDOB = Request.QueryString["BDOB"];
                string BFirstNames = Request.QueryString["BFirstNames"];
                string BHomePhone = Request.QueryString["BHomePhone"];
                string BPhoneWork = Request.QueryString["BPhoneWork"];
                string BMobilePhone = Request.QueryString["BMobilePhone"];
                string BEmail1 = Request.QueryString["BEmail1"];
                string BEmail2 = Request.QueryString["BEmail2"];
                string agentId = Request.QueryString["BAgent"];

                customer.BusinessName = BBusinessName;
                customer.ABN = BABN;
                customer.TradingName = BTradingName;
                customer.AddressLine1 = BAddressLine1;
                customer.AddressLine2 = BAddressLine2;
                customer.Suburb = BSuburb;
                customer.State = BState;
                customer.Postcode = BPostcode;
                customer.PostalAddressAddressLine2 = BPostalAddressLine2;
                customer.PostalAddressSuburb = BPostalSuburb;
                customer.PostalAddressState = BPostalState;
                customer.PostalAddressPostcode = BPostalPostcode;
                customer.LastName = BLastName;
                customer.DOB = BDOB;
                customer.FirstName = BFirstNames;
                customer.TelHome = BHomePhone;
                customer.TelWork = BPhoneWork;
                customer.Mobile = BMobilePhone;
                customer.EmailAddress = BEmail1;
                customer.EmailAddress2 = BEmail2;
                customer.AgentID = int.Parse(agentId);
                customer.IsIndividual = false;

                ser.Add(customer);
                Response.Write("1^Sucessfully Added");
            }
            catch (Exception ex)
            {

                Response.Write("2^"+ex.ToString());
            }

        }
    }
}