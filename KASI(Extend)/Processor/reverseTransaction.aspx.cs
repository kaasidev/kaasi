﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Kapruka.Enterprise;

namespace KASI_Extend_.Processor
{
    public partial class reverseTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = Request.QueryString["TID"].ToString();
            String reason = Request.QueryString["reason"].ToString();
            String CancelFee = Request.QueryString["CancelFee"].ToString();
            classes.Transactions trans = new classes.Transactions();
            classes.Customer cust = new classes.Customer();
            EmailCustomer eCustomer = new EmailCustomer();

            String CID = trans.getCustomerIDFromTransactionID(TID);
            String CAccBalance = cust.getCustomerAccountBalance(CID);

            float DollarAmount = trans.getTransactionSendingAmount(TID);
            float ServiceCharge = trans.getTransactionServiceCharge(TID);
            float fAccBalance = float.Parse(CAccBalance);
            float fCancelFee = float.Parse(CancelFee);

            float refundTotal = (DollarAmount + ServiceCharge) - fCancelFee;
            float TransTotal = (DollarAmount + ServiceCharge);

            float negCancelFee = fCancelFee * -1;

            //float TotalRefund = fAccBalance + DollarAmount + ServiceCharge;
            //float BaseRefund = DollarAmount + ServiceCharge;

            cust.updateCustomerBalance(CID, refundTotal);
            cust.AddCreditDebitForCustomer(CID, TransTotal, "Transaction Reversal", "CREDIT", TID, "0");
            cust.AddCreditDebitForCustomer(CID, negCancelFee, "Cancellation Fee", "DEBIT", TID, "0");

            TransactionService TransServ = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            int theTID = Int32.Parse(TID);
            var TransDetails = TransServ.GetAll(x => x.TransactionID == theTID, null, "").SingleOrDefault();

            TransDetails.MTransferFeeTotal = 0;
            TransDetails.MAdditionalTransPercTotal = 0;
            TransDetails.MCommishGainPercTotal = 0;
            TransDetails.CancellationFee = decimal.Parse(CancelFee);
            TransServ.Update(TransDetails);


            trans.updateTranStatus(TID, "CANCELLED", reason);
            trans.insertAuditRecord(TID, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + TID + " has been been voided");
            //eCustomer.SendEmailWhenCancelled(TID);
        }
    }
}