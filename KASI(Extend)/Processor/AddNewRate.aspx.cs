﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class AddNewRate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String ANZIMTUSD = String.Empty;
            String ANZIMTLKR = String.Empty;
            String XPRESS = String.Empty;
            String WUNION = String.Empty;
            String MGRAM = String.Empty;
            String CBSLTT = String.Empty;
            String BOCTT = String.Empty;
            String KASIRATE = String.Empty;

            String WebSwitch = String.Empty;
            String fWebSwitch = String.Empty;
            String WebMessage = String.Empty;

            float fANZIMTUSD = 0;
            float fANZIMTLKR = 0;
            float fXPRESS = 0;
            float fWUNION = 0;
            float fMGRAM = 0;
            float fCBSLTT = 0;
            float fBOCTT = 0;
            float fKASIRATE = 0;

            ANZIMTUSD = Request.QueryString["ANZIMTUSD"].ToString();
            ANZIMTLKR = Request.QueryString["ANZIMTLKR"].ToString();
            XPRESS = Request.QueryString["XPRESS"].ToString();
            WUNION = Request.QueryString["WUNION"].ToString();
            MGRAM = Request.QueryString["MGRAM"].ToString();
            CBSLTT = Request.QueryString["CBSLTT"].ToString();
            BOCTT = Request.QueryString["BOCTT"].ToString();
            KASIRATE = Request.QueryString["KEXCHRATE"].ToString();
            WebSwitch = Request.QueryString["WebSwitch"].ToString();
            WebMessage = Request.QueryString["WebMessage"].ToString();

            fANZIMTUSD = float.Parse(ANZIMTUSD);
            fANZIMTLKR = float.Parse(ANZIMTLKR);
            fXPRESS = float.Parse(XPRESS);
            fWUNION = float.Parse(WUNION);
            fMGRAM = float.Parse(MGRAM);
            fCBSLTT = float.Parse(CBSLTT);
            fBOCTT = float.Parse(BOCTT);
            fKASIRATE = float.Parse(KASIRATE);


             SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

             String SqlStmt = "INSERT INTO dbo.Rates (ANZIMTUSD, ANZIMTLKR, XPRESS, MGRAM, CBSLTT, BOCTT, Rate, RateDate, CreatedBy, CreatedDateTime, WUNION) VALUES (@ANZIMTUSD, @ANZIMTLKR, @XPRESS, @MGRAM, @CBSLTT, @BOCTT, @Rate, CURRENT_TIMESTAMP, @CreatedBy, CURRENT_TIMESTAMP, @WUNION)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@ANZIMTUSD", SqlDbType.Money, 10).Value = fANZIMTUSD;
                cmd.Parameters.Add("@ANZIMTLKR", SqlDbType.Money, 10).Value = fANZIMTLKR;
                cmd.Parameters.Add("@XPRESS", SqlDbType.Money, 10).Value = fXPRESS;
                cmd.Parameters.Add("@MGRAM", SqlDbType.Money, 10).Value = fMGRAM;
                cmd.Parameters.Add("@CBSLTT", SqlDbType.Money, 10).Value = fCBSLTT;
                cmd.Parameters.Add("@BOCTT", SqlDbType.Money, 10).Value = fBOCTT;
                cmd.Parameters.Add("@Rate", SqlDbType.Money, 10).Value = fKASIRATE;
                cmd.Parameters.Add("@WUNION", SqlDbType.Money, 10).Value = fWUNION;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = "SYSTEM";
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String SqlStmt2 = "UPDATE dbo.Settings SET SettingsVariable = @WebMessage WHERE SettingName = 'WebsiteMessage'";
            using (SqlCommand cmd = new SqlCommand(SqlStmt2))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@WebMessage", SqlDbType.NVarChar, 10).Value = WebMessage;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            if (WebSwitch == "true")
            {
                fWebSwitch = "ON";
            }
            else
            {
                fWebSwitch = "OFF";
            }

            String SqlStmt3 = "UPDATE dbo.Settings SET SettingsVariable = @WebSwitch WHERE SettingName = 'WebsiteMessageSwitch'";
            using (SqlCommand cmd = new SqlCommand(SqlStmt3))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@WebSwitch", SqlDbType.NVarChar, 10).Value = fWebSwitch;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            Response.Write("OK");
        }
    }
}