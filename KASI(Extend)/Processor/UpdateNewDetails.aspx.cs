﻿using Kapruka.Enterprise;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Processor
{
    public partial class UpdateNewDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string regNumber = Request.QueryString["reg"];
                string newDate = Request.QueryString["newd"];
                string regDate = Request.QueryString["dat"];
                int aid = int.Parse(Request.QueryString["aid"].ToString());

                //AgentService ser = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                //var agent = ser.GetAll(x => x.AgentID == aid, null, "").SingleOrDefault();

                //agent.AustracNumber = regNumber;
                //agent.AustracRegDate = Convert.ToDateTime(regDate);
                //agent.AustracRenewalDate = Convert.ToDateTime(newDate);

                //ser.Update(agent);
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String strQuery = "SET DATEFORMAT dmy update Agents set AustracNumber='" + regNumber + "',AustracRegDate=convert(date,'" + regDate + "'),AustracRenewalDate=convert(date,'" + newDate + "') where AgentID="+aid;

                using (SqlCommand cmd = new SqlCommand(strQuery))
                {
                    cmd.Connection = conn;
                    conn.Open();

                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Write('1');
            }
            catch (Exception ex)
            {

                Response.Write("0^"+ex.ToString());
            }

        }
    }
}