﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class CreateNSBBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions transClass = new classes.Transactions();
            String delimiter = "|";
            String outputString = String.Empty;

            String BankCode = Request.QueryString["BankCode"].ToString();
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filename = "NSB - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts beneAcc = new BeneficiaryAccounts();
            classes.Transactions trans = new classes.Transactions();

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyyMMdd");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString = outputString + sdr["TransactionID"].ToString() + delimiter;  // Transaction ID
                            outputString = outputString + sdr["RemittedCurrency"].ToString() + delimiter; // Remitted Currency
                            outputString = outputString + disbursalMethod(sdr["AccountID"].ToString()) + delimiter; // Disbursal Mode
                            String remAmount = String.Format("{0:N2}", float.Parse(sdr["RemittedAmount"].ToString()));
                            outputString = outputString + remAmount.ToString().Replace(".", "").Replace(",", "") + delimiter; //Transaction Amount
                            outputString = outputString + now + delimiter; // Today's date in DD/MM/YYYY
                            outputString = outputString + (bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString()).Length > 60 ? bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString()).Substring(0, 60) : bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString())) + delimiter; // Beneficiary's name
                            outputString = outputString + bene.getBeneficiaryNIC(sdr["BeneficiaryID"].ToString()) + delimiter; // Beneficiary NIC
                            outputString = outputString + beneAcc.getBeneficiaryAccountBankCodeBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter; // Beneficiary Bank Code
                            outputString = outputString + beneAcc.getBeneficiaryAccountBankNameBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter; // Beneficiary Bank Name
                            outputString = outputString + (beneAcc.getBeneficiaryAccountBranchCodeBasedOnAccountID(sdr["AccountID"].ToString()).Length > 3 ? beneAcc.getBeneficiaryAccountBranchCodeBasedOnAccountID(sdr["AccountID"].ToString()).Substring(0, 3) : beneAcc.getBeneficiaryAccountBranchCodeBasedOnAccountID(sdr["AccountID"].ToString())) + delimiter; // Beneficiary Branch Code
                            outputString = outputString + beneAcc.getBeneficiaryAccountBranchNameBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter; // Beneficiary Branch Name
                            outputString = outputString + beneAcc.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()) + delimiter; // Account Number
                            outputString = outputString + (bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()).Length > 200 ? bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()).Substring(0,200) : bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString())) + delimiter; // Beneficiary Address 1
                            outputString = outputString + delimiter; // Beneficiary Address 2
                            outputString = outputString + delimiter; // Beneficiary Address 3
                            outputString = outputString + "SRI LANKA" + delimiter; // Beneficiary Country
                            outputString = outputString + bene.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString()) + delimiter; // Beneficiary Telephone Number
                            outputString = outputString + (cust.getCustomerFullName(sdr["CustomerID"].ToString()).Length > 60 ? cust.getCustomerFullName(sdr["CustomerID"].ToString()).Substring(0, 60) : cust.getCustomerFullName(sdr["CustomerID"].ToString())) + delimiter; // Customer's Name
                            outputString = outputString + cust.getCustomerUnitNo(sdr["CustomerID"].ToString()) + " " + cust.getCustomerStreetNo(sdr["CustomerID"].ToString()) + " " + cust.getCustomerStreetName(sdr["CustomerID"].ToString()) + delimiter; // Customer's Address Line 1
                            outputString = outputString + cust.getCustomerState(sdr["CustomerID"].ToString()) + " " + cust.getCustomerPostcode(sdr["CustomerID"].ToString()) + delimiter; // Customer Address Line 2
                            outputString = outputString + cust.getCustomerCountry(sdr["CustomerID"].ToString()) + delimiter; // Customer Country
                            outputString = outputString + cust.getCustomerMobile(sdr["CustomerID"].ToString()) + delimiter; // Customer Contact Number
                            outputString = outputString + delimiter; // Sender's Message
                            outputString = outputString + delimiter; // Agent's remarks
                            outputString = outputString + trans.getTransactionPurpose(sdr["TransactionID"].ToString()) + delimiter; // Purpose of Transaction
                            outputString = outputString + trans.getTransactionSourceOfFunds(sdr["TransactionID"].ToString()) + delimiter; // Source of Funds
                            outputString = outputString + "FILE" + Environment.NewLine;
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                        transClass.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into NSB Bank File");
                    }
                    Response.Write("NSB File Successfully Created^" + filename);
                }
                else
                {
                    Response.Write("NSB File Could Not Be Created^");
                }

            }
        }

        protected String disbursalMethod(String AID)
        {
            String method = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT PaymentMethod FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        method = sdr["PaymentMethod"].ToString();
                    }
                }
                conn.Close();
            }

            if (method == "Credited to the acc.")
            {
                method = "A/C TRANSFER";
            }
            else
            {
                method = "DIRECT";
            }

            return method;
        }
    }
}