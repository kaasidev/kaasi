﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class CreateBOCBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions transClass = new classes.Transactions();
            String BankCode = Request.QueryString["BankCode"].ToString();
            String AgentID = Request.QueryString["AID"];
            String delimiter = Request.QueryString["Del"].ToString();
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filepath = String.Empty;
            String filename = "BOC - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            filepath = HttpContext.Current.Server.MapPath(@"\BankFileCreation\" + filename);
            //Uri nagivateUri = new Uri(System.Web.HttpContext.Current.Request.Url, filepath);

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyyMMdd");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AgentID;
            String outputString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString = outputString +  sdr["TransactionID"].ToString() + delimiter;
                            outputString = outputString + "8191173" + delimiter;
                            String remAmount = String.Format("{0:N2}",float.Parse(sdr["RemittedAmount"].ToString()));
                            outputString = outputString + remAmount.ToString().Replace(".", "").Replace(",",  "") + delimiter;
                            outputString = outputString + sdr["RemittedCurrency"].ToString() + delimiter;
                            outputString = outputString + now + delimiter;
                            outputString = outputString + getCustomerFullName(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + sdr["BeneficiaryName"].ToString() + delimiter;
                            outputString = outputString + getBeneficiaryAddress(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + getBeneficiaryAddress(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + getBeneficiaryAccountNumber(sdr["AccountID"].ToString()) + delimiter;
                            outputString = outputString + BankCode + delimiter;
                            outputString = outputString + getBankBranch(sdr["AccountID"].ToString()) + delimiter;
                            outputString = outputString + " " + delimiter;
                            outputString = outputString + getBeneficiaryMobileNumber(sdr["BeneficiaryID"].ToString()) + Environment.NewLine;
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }
                    
                catch (Exception ex)
                {
                    filehasbeencomplete = false;
                    
                }

                if(filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                        transClass.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into Bank Of Ceylon Bank File");
                    }
                    Response.Write("BOC File Created^" + filename);
                }
                else
                {
                    Response.Write("BOC File Could Not Be Created^");
                }
                
            }

            
        }


        protected String getCustomerFullName(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String SQLQuery = "SELECT FullName FROM dbo.Customers WHERE CustomerID = " + CID;

            String returnString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = SQLQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        returnString = sdr["FullName"].ToString();
                    }
                    
                }
                conn.Close();
            }

            int Length = returnString.Length;
            if (Length > 40)
            {
                returnString = returnString.Substring(0, 39);
            }

            return returnString;
        }

        protected String getBeneficiaryAddress(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String SQLQuery = "SELECT AddressLine1 + ' ' + AddressLine2 + ' ' + Suburb + ' ' + State + ' ' + Postcode + ' ' + Country AS FullAddress FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;

            String returnString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = SQLQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        returnString = sdr["FullAddress"].ToString();
                    }

                }
                conn.Close();
            }

            int Length = returnString.Length;
            if (Length > 120)
            {
                returnString = returnString.Substring(0, 119);
            }

            returnString = returnString.Replace(",", "");
            returnString = returnString.Replace("-- SELECT --", "");
            return returnString;
        }

        protected String getBeneficiaryID(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String SQLQuery = "SELECT NationalityCardID FROM dbo.Beneficiaries WHERE BenenficiaryID = " + BID;

            String returnString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = SQLQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            returnString = sdr["NationalityCardID"].ToString();
                        }
                    }
                    else
                    {
                        returnString = "";
                    }
                    

                }
                conn.Close();
            }

            int Length = returnString.Length;
            if (Length > 20)
            {
                returnString = returnString.Substring(0, 19);
            }

            return returnString;
        }

        protected String getBeneficiaryAccountNumber(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String SQLQuery = "SELECT AccountNumber FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;

            String returnString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = SQLQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            returnString = sdr["AccountNumber"].ToString().Replace(" ", "");
                        }
                    }
                    else
                    {
                        returnString = "";
                    }


                }
                conn.Close();
            }

            int Length = returnString.Length;
            if (Length > 20)
            {
                returnString = returnString.Substring(0, 19);
            }

            return returnString;
        }

        protected String getBankBranch(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String SQLQuery = "SELECT BranchID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;

            String returnString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = SQLQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            returnString = sdr["BranchID"].ToString().Replace(" ", "");
                        }
                    }
                    else
                    {
                        returnString = "Unknown Branch";
                    }


                }
                conn.Close();
            }

            return returnString;
        }

        protected String getBeneficiaryMobileNumber(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String SQLQuery = "SELECT Mobile FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;

            String returnString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = SQLQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            returnString = sdr["Mobile"].ToString().Replace(" ", "");;
                        }
                    }
                    else
                    {
                        returnString = "";
                    }


                }
                conn.Close();
            }

            int Length = returnString.Length;
            if (Length > 20)
            {
                returnString = returnString.Substring(0, 19);
            }

            return returnString;
        }
    }
}