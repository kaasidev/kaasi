﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend.Processor
{
    public partial class deleteNotification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String NotifID = Request.QueryString["NID"].ToString();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "DELETE FROM dbo.Notifications WHERE NotificationID = " + NotifID;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                Response.Write("OK");
            }
            catch (Exception ex)
            {
                Response.Write("ERROR");
            }
            
        }
    }
}