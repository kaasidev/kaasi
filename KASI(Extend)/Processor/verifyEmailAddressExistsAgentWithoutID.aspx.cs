﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class verifyEmailAddressExistsAgentWithoutID : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String EmailAddy = Request.QueryString["Email"].ToString();
            String AID = Request.QueryString["AID"].ToString();
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT LoginID FROM dbo.Logins WHERE Username = '" + EmailAddy + "' AND AgentID <> '" + AID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        output = "true";
                    }
                    else
                    {
                        output = "false";
                    }
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}