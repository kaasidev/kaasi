﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class UpdateCompanyVerified : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CompanyVerified = Request.QueryString["CVerified"].ToString();
            String CID = Request.QueryString["CID"].ToString();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);


                String SQLStmt = "UPDATE dbo.Customers SET CompanyVerified = @CompanyVerified, CompanyVerifiedBy = @CompanyVerifiedBy, CompanyVerifiedDateTime = CURRENT_TIMESTAMP WHERE CustomerID = " + CID;

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@CompanyVerified", SqlDbType.NVarChar, 50).Value = CompanyVerified;
                    cmd.Parameters.Add("@CompanyVerifiedBy", SqlDbType.NVarChar, 150).Value = "SYSTEM";
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }


            Response.Write("Successfully updated");
        }
    }
}