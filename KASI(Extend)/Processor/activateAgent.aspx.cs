﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class activateAgent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String AID = Request.QueryString["AID"].ToString();

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLStmt = "UPDATE dbo.Logins SET Active = 'Y' WHERE AgentID = " + AID;

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                String SQLStmt2 = "UPDATE dbo.Agents SET Status = 'Active' WHERE AgentID = " + AID;

                using (SqlCommand cmd = new SqlCommand(SQLStmt2))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                Response.Write("Agent successfully activated.");
            }
            catch (Exception ex)
            {
                Response.Write("An error has occured. Please contact your system administrator");
            }
        }
    }
}