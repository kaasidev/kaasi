﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend_.Processor
{
    public partial class updateCustomerKYCManually : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String Outcome = Request.QueryString["Outcome"].ToString();
            String KYCNotes = Request.QueryString["Notes"].ToString();
            String SQLStmt = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            try
            {
                if (Outcome == "SUCCESS")
                {
                    SQLStmt = @"UPDATE dbo.Customers SET resultCode = '0', resultDescription = 'SUCCESS', manualKYCNotes=@manualKYCNotes, typeOfCheck='MANUAL' WHERE CustomerID = " + CID;
                    classes.Customer cust = new classes.Customer();
                    cust.updateRiskLevel(CID, "1");
                }
                else
                {
                    SQLStmt = @"UPDATE dbo.Customers SET resultCode = '1', resultDescription = 'FAILED', manualKYCNotes=@manualKYCNotes, typeOfCheck='MANUAL' WHERE CustomerID = " + CID;
                }

                using (SqlCommand cmd = new SqlCommand(SQLStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@manualKYCNotes", KYCNotes);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                CustAuditLogService CALServ = new CustAuditLogService(new UnitOfWorks(new KaprukaEntities()));

                CustAuditLog newLog = new CustAuditLog();
                newLog.ActionType = "EDIT";
                newLog.AgentName = Session["LoggedCompany"].ToString();
                newLog.CustomerID = Int32.Parse(CID);
                newLog.Username = Session["LoggedPersonName"].ToString();
                if (Outcome == "SUCCESS")
                {
                    newLog.Description = "has manually approved this customer's KYC";
                }
                else
                {
                    newLog.Description = "has manually denied this customer's KYC";
                }
                newLog.AuditDateTime = DateTime.Now;

                CALServ.Add(newLog);



                    CustomerService CustServ = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
                    int intCID = Int32.Parse(CID);
                    var CustDetails = CustServ.GetAll(x => x.CustomerID == intCID, null, "").SingleOrDefault().AgentID;

                    AgentNotificationService AGServ = new AgentNotificationService(new UnitOfWorks(new KaprukaEntities()));

                    Random rnd = new Random();
                    int GlobalID = rnd.Next(999999999);

                    AgentNotification AGNot = new AgentNotification();
                    AGNot.GlobalNotificiationID = GlobalID;
                    AGNot.Type = "CUS";
                    AGNot.RecordID = Int32.Parse(CID);
                    if (Outcome == "SUCCESS")
                    {
                        AGNot.Description = "Customer " + CID + "'s KYC has been manually approved";
                    }
                    else
                    {
                        AGNot.Description = "Customer " + CID + "'s KYC has been manually declined";

                    }
                    AGNot.CreatedDateTime = DateTime.Now;
                    AGNot.AgentID = Int32.Parse(CustDetails.ToString());

                    AGServ.Add(AGNot);

             

                Response.Write("OK");
            }
            catch (Exception ex)
            {
                Response.Write("Something went wrong. Please consult your system administrator");
            }
        }
    }
}