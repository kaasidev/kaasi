﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend.Processor
{
    public partial class ResendTransactionEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EmailCustomer ECust = new EmailCustomer();

            String TID = Request.QueryString["TID"].ToString();

            ECust.SendEmailWhenSaved(TID);

            Response.Write("OK");
        }
    }
}