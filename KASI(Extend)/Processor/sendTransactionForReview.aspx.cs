﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.Processor
{
    public partial class sendTransactionForReview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = Request.QueryString["TID"].ToString();
            String ReviewReason = Request.QueryString["RR"].ToString();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SQLStmt = "UPDATE dbo.Transactions SET Status = 'REVIEW', Notes = @Notes  WHERE TransactionID = " + TID;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@Notes", SqlDbType.NVarChar, 500).Value = ReviewReason;
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String SQLStmt2 = @"INSERT INTO dbo.TransactionReviewReasons (TransactionID, ReviewReason, CreatedDateTime, CreatedBy) 
                        VALUES (" + TID + ", '" + ReviewReason + "', CURRENT_TIMESTAMP, '" + Session["LoggedPersonName"].ToString() + " " +
                        "(" + Session["LoggedUserFullName"].ToString() + ")')";

            using (SqlCommand cmd2 = new SqlCommand(SQLStmt2))
            {
                cmd2.Connection = conn;
                conn.Open();
                cmd2.ExecuteNonQuery();
                conn.Close();
            }

                Response.Write("OK");
        }
    }
}