﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.Processor
{
    public partial class CreateSEYBankFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String BankCode = Request.QueryString["BankCode"].ToString();
                DateTime LKRDT = DateTime.Now;
                String formattedDateTime = LKRDT.ToString();
                String UploadDate = String.Empty;
                var charsToRemove = new string[] { "/", ",", ":", "-", "PM", " ", "AM" };
                String filename = "SEY - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".HDL";
                foreach (var c in charsToRemove)
                {
                    formattedDateTime = formattedDateTime.Replace(c, "");
                }

                Random generator = new Random();
                String uniquenumber = generator.Next(100, 999).ToString();
                UploadDate = formattedDateTime.Substring(0, 8);
                formattedDateTime = formattedDateTime + uniquenumber;

                BuilkLKRDetailFile(formattedDateTime, UploadDate, BankCode);
                BuildLKRHeaderFile(formattedDateTime, BankCode, UploadDate, filename);
                Response.Write("Seylan File Successfully Created^" + filename);
            }
            catch (Exception ex)
            {
                Response.Write("Seylan Filecould not be Created^" + ex);
            }

        }

        protected void BuildLKRHeaderFile(String BatchNumber, String BankCode, String UploadDate, string filename)
        {
            String outputString = String.Empty;

            outputString = outputString + BatchNumber.PadRight(15);
            outputString = outputString + "AU02";
            float TAmount = TotalLKRAmount(BankCode);
            outputString = outputString + string.Format("{0:N2}", TAmount).Replace(",", "").PadRight(15);
            outputString = outputString + TotalLKRTXN(BankCode).PadRight(5);
            outputString = outputString + " ".PadRight(15);
            outputString = outputString + UploadDate.PadRight(7);
            outputString = outputString + "KAPR";
            outputString = outputString + "LKR";

            try
            {
                System.IO.File.WriteAllText(filename,
                    outputString);

            }
            catch (Exception ex)
            {
                
            }


        }

        protected void BuilkLKRDetailFile(String BatchNumber, String UploadDate, String BankCode)
        {
            classes.Transactions transClass = new classes.Transactions();
            
            String delimiter = "|";
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;

            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts bAccount = new BeneficiaryAccounts();
            String RAmount = String.Empty;
            String BeneID = String.Empty;

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, Rate, AuthorisationDateTime FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RemittedCurrency = 'LKR' AND RoutingBank = " + BankCode;
            String outputString = String.Empty;
            String CFullName = String.Empty;
            int CFullNameLength = 0;
            String empty = String.Empty;

            String InputBranch = "";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString = outputString + "    ";
                            outputString = outputString + BatchNumber.PadRight(15);
                            outputString = outputString + "AU02";
                            String beneName = bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString());
                            if (beneName.Length > 30)
                            {
                                outputString = outputString + beneName.Substring(0, 30);
                            }
                            else
                            {
                                outputString = outputString + beneName.PadRight(30);
                            }
                            
                            String BankID =
                                bAccount.getBeneficiaryAccountBankCodeBasedOnAccountID(sdr["AccountID"].ToString());

                            String BranchName =
                                bAccount.getBeneficiaryAccountBranchNameBasedOnAccountID(sdr["AccountID"].ToString());
                            outputString = outputString + BankID.PadRight(4);
                            outputString = outputString +
                                           bAccount.getBeneficiaryAccountBranchCodeBasedOnAccountID(sdr["AccountID"]
                                               .ToString()).PadRight(3);
                            outputString = outputString +
                                           bAccount.getBeneficicaryAccountNumberBasedOnAccountID(sdr["AccountID"]
                                               .ToString()).PadRight(15);
                            outputString = outputString + sdr["TransactionID"].ToString();

                            String TypeOfRemittance = ConfirmWhichTypeOfRemittance(BankID, BranchName);
                            outputString = outputString + TypeOfRemittance;
                            outputString = outputString + sdr["RemittedAmount"].ToString().PadRight(15);
                            outputString = outputString + " ".PadRight(15); // Charge Amount
                            outputString = outputString + " ".PadRight(7); // Value Date
                            outputString = outputString + sdr["TransactionID"].ToString().PadRight(25); // Reference
                            outputString = outputString + " ".PadRight(15); // Beneficiary ID
                            outputString = outputString + UploadDate.PadRight(7); // Upload Date
                            outputString = outputString + "KAPR"; // Upload User
                            outputString = outputString + sdr["RemittedCurrency"].ToString()
                                               .PadRight(3); // Currency
                            outputString = outputString + "9999"; //PIN
                            outputString = outputString + cust.getCustomerMobile(sdr["CustomerID"].ToString())
                                               .PadRight(20); // Remitter Tel
                            outputString = outputString + bene.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString())
                                               .PadRight(20); // Beneficiary Tel
                            outputString = outputString + " ".PadRight(50);
                            outputString = outputString + bene.getBeneficiaryEmail(sdr["BeneficiaryID"].ToString())
                                               .PadRight(75); // Beneficiary Email
                            outputString = outputString + sdr["CustomerID"].ToString().PadRight(15); // Remitted IT
                            outputString = outputString + " ".PadRight(75);
                            outputString = outputString + "\r\n";



                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                
                }
                conn.Close();

                String filename = "SEY - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".DTL";

                //Response.AppendHeader("content-disposition", "attachment;filename=" + filename);
                //Response.ContentType = "application/octet-stream";
                //Response.Write(outputString);
                
                //Response.End();
                try
                {
                    System.IO.File.WriteAllText(@"C:\TestFiles\" + filename, outputString);

                    


                    filehasbeencomplete = true;
                }
                    
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                        transClass.insertAuditRecord(Transaction, Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into Seylan Bank Bank File");
                    }
                    Response.Write("SEY File Successfully Created");
                }
                else
                {
                    Response.Write("SEY File Could Not Be Created");
                }
            }

            //Response.Write(outputString);
        }

        protected void BuildAUDHeaderFile()
        {
            
        }

        protected void BuildAUDDetailFile()
        {
            
        }

        protected String ConfirmWhichTypeOfRemittance(String BankID, String BranchName)
        {
            String returnValue = "";

            bool found = BranchName.Contains("Advise");

            if (found == true)
            {
                returnValue = "03";
            }
            else
            {
                if (BankID == "7287")
                {
                    returnValue = "01";
                }
                else
                {
                    returnValue = "04";
                }
            }

            return returnValue;
        }

        protected float TotalLKRAmount(String BankCode)
        {
            float TotalAmount = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT SUM(RemittedAmount) AS TotalRemittance FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RemittedCurrency = 'LKR' AND RoutingBank = " + BankCode;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["TotalRemittance"].ToString() == "0")
                        {
                            TotalAmount = 0;
                        }
                        else
                        {
                            TotalAmount = float.Parse(sdr["TotalRemittance"].ToString());
                        }
                        
                    }
                }

                conn.Close();
            }

            return TotalAmount;
        }

        protected String TotalLKRTXN(String BankCode)
        {
            String TotalAmount = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT COUNT(*) AS TotalRemittance FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RemittedCurrency = 'LKR' AND RoutingBank = " + BankCode;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TotalAmount = sdr["TotalRemittance"].ToString();
                    }
                }

                conn.Close();
            }

            return TotalAmount;
        }
    }
}