﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;


namespace KASI_Extend_.Settings
{
    public partial class AddNewBranch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "";
            LoadBanks();
            HyperLink1.NavigateUrl = "../welcome.aspx";
        }

        private void LoadBanks()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT * FROM dbo.Banks";
            String FirstBeneficiary = String.Empty;


            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    DropDownList1.Items.Add(new ListItem("--SELECT--", "NOSELECT"));
                    while (sdr.Read())
                    {
                        var items = new ListItem
                        {
                            Text = sdr["BankName"].ToString(),
                            Value = sdr["BankCode"].ToString()
                        };

                        DropDownList1.Items.Add(items);
                        //ddl_List_Beneficiaries.Attributes["onchange"] = "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")";
                        /*li = new HtmlGenericControl("li");
                        list_beneficiaries.Controls.Add(li);

                        HtmlGenericControl divAnchor = new HtmlGenericControl("div");
                        divAnchor.Attributes.Add("class", "beneficiary_bullet");

                        HtmlGenericControl imgAnchor = new HtmlGenericControl("img");
                        imgAnchor.Attributes.Add("src", "images/ben-avatar.png");
                        imgAnchor.Attributes.Add("width", "16px");

                        divAnchor.Controls.Add(imgAnchor);
                        li.Controls.Add(divAnchor);

                        HtmlGenericControl divAnchor2 = new HtmlGenericControl("div");
                        divAnchor2.InnerText = sdr["BeneficiaryName"].ToString();*/


                        //divAnchor2.Attributes.Add("onclick", "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")");

                        // li.Controls.Add(divAnchor2);


                        /*li.Attributes.Add("onclick", "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")");
                        li.Attributes.Add("class", "beneficiary_link");
                        li.InnerText = sdr["BeneficiaryName"].ToString();

                        list_beneficiaries.Controls.Add(li);
                        */

                    }

                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String AlreadyPresent = String.Empty;
            AlreadyPresent = checkBranchDoesNotExist(DropDownList1.SelectedValue, TextBox1.Text);

            if (AlreadyPresent == "N")
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

                string SqlStmt = "INSERT INTO dbo.BankBranches (BankCode, BranchCode, BranchName, CreatedBy, CreatedDateTime, City) VALUES (@BankCode, @BranchCode, @BranchName, @CreatedBy, CURRENT_TIMESTAMP, @City)";

                using (SqlCommand cmd = new SqlCommand(SqlStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.Parameters.Add("@BankCode", SqlDbType.Int, 6).Value = DropDownList1.SelectedValue;
                    cmd.Parameters.Add("@BranchCode", SqlDbType.Int, 6).Value = TextBox2.Text;
                    cmd.Parameters.Add("@BranchName", SqlDbType.NVarChar, 200).Value = TextBox1.Text;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = Session["LoggedInName"].ToString();
                    cmd.Parameters.Add("@City", SqlDbType.NVarChar, 100).Value = TextBox1.Text;
                    cmd.ExecuteNonQuery();
                }

                conn.Close();

                Label1.Text = TextBox1.Text + " has been added succcessfully.";
            }
            else
            {
                Label1.Text = "This Branch Code Already Exists for this bank in the system";
            }
            
        }

        private String checkBranchDoesNotExist(String BankCode, String BranchName)
        {
            String Exists = String.Empty;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT * FROM dbo.BankBranches WHERE BankCode = " + BankCode + " AND BranchName = '" + BranchName + "'";
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        Exists = "Y";
                    }
                    else
                    {
                        Exists = "N";
                    }
                }
                conn.Close();
            }

            return Exists;
        }
    }
}