﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewBranch.aspx.cs" Inherits="KASI_Extend_.Settings.AddNewBranch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Select Bank
        <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
        <p></p>
        Branch Name
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <p></p>
        Branch Code: <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <p>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </p>
        <asp:Button ID="Button1" runat="server" Text="SAVE" OnClick="Button1_Click" />
        <p></p>
        <asp:HyperLink ID="HyperLink1" runat="server">Go Back To Home</asp:HyperLink>
    </div>
    </form>
</body>
</html>
