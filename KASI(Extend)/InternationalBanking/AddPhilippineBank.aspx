﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPhilippineBank.aspx.cs" Inherits="KASI_Extend.InternationalBanking.AddPhilippineBank" %>
<head>
    <title></title>
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>
    <link href="css/jquery-ui.min.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="../css/kasi_main.css" />

    <style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 20px;

    }

    /* Hide default HTML checkbox */
    .switch input {display:none;}

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #DCDCDC;
        -webkit-transition: .4s;
        transition: .4s;
        }

    .slider:before {
        position: absolute;
        content: "";
        height: 12px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

    input:checked + .slider {
        background-color: #76D17F;
        
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
}

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 0px;
    }

    .slider.round:before {
        border-radius: 0%;
    }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#DDL_BankList').empty();
            $('#DDL_BankList').append('<option value="0">-- SELECT -- </option>');
            $('#TR_BankInformation').hide();
            $('#TR_ToggleSwitch').hide();

            $.ajax({
                url: 'JQDataFetch/getAllBanks.aspx',
                async: false,
                data: {
                    CID: '8',
                },
                success: function (response) {
                    var Countries = response.split('~');
                    Countries.sort();
                    $.each(Countries, function (item) {
                        var datasplit = Countries[item].split('|');
                        $('#DDL_BankList').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                    });
                }
            });

            $.ajax({
                url: 'JQDataFetch/getCountryAssignedCurrencies.aspx',
                async: false,
                data: {
                    CID: '8',
                },
                success: function (response) {
                    $('#NewAcc_TradingCurrency').empty();
                    var Currencies = response.split('~');
                    Currencies.sort();
                    $.each(Currencies, function (item) {
                        var datasplit = Currencies[item].split('|');
                        $("#NewAcc_TradingCurrency").append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                    });

                }
            })

            if ($('#AccountID').val() != "")
            {
                $('#TR_BankInformation').show();
                $('#btn_AddAcc').hide();
                $('#TR_ToggleSwitch').show();
                $('#btn_EditAccount').show();
                $.ajax({
                    url: 'JQDataFetch/getBeneficiaryAccountEdit.aspx',
                    async: false,
                    data: {
                        AccID: $('#AccountID').val()
                    },
                    success: function (response) {
                        var data = response.split('|');
                        $('#DDL_BankList').val(data[0]);

                        $('#DDL_BranchList').empty();
                        $('#DDL_BranchList').append('<option value="0">-- SELECT -- </option>');
                        $('#DIV_BankCode').text("");
                        $('#DIV_BranchCode').text("");
                        $.ajax({
                            url: 'JQDataFetch/getAllBankBranches.aspx',
                            async: false,
                            data: {
                                BID: $('#DDL_BankList').val(),
                            },
                            success: function (response) {
                                var Branches = response.split('~');
                                Branches.sort();
                                $.each(Branches, function (item) {
                                    var datasplit = Branches[item].split('|');
                                    $('#DDL_BranchList').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                                });
                                $('#DDL_BranchList').val(data[1]);
                                if (data[2] != "")
                                {
                                    $('#NewAcc_accountname').val(data[2]);
                                }
                                else
                                {
                                    $('#NewAcc_accountname').val(data[6]);
                                }
                                
                                $('#NewAcc_accountnumber').val(data[3]);
                                $('#NewAcc_TradingCurrency').val(data[4]);

                                
                                if (data[5] == "Y")
                                {
                                    $('#CHK_Active').attr('checked', true);
                                }
                                else
                                {
                                    $('#CHK_Active').attr('checked', false);
                                }


                                $('#DIV_BankCode').text($('#DDL_BankList').val());
                                $('#DIV_BranchCode').text($('#DDL_BranchList').val());
                            }
                        });
                    }
                });

               
            }
            else
            {
                $('#btn_AddAcc').show();
                $('#btn_EditAccount').hide();
                $('#NewAcc_accountname').val($('#<%=BeneficiaryName.ClientID%>').val());
            }


            $('#DDL_BankList').change(function () {
                if (!$('#DDL_BankList').val() == "0")
                {
                    $('#DDL_BranchList').empty();
                    $('#DDL_BranchList').append('<option value="0">-- SELECT -- </option>');
                    $('#DIV_BankCode').text("");
                    $('#DIV_BranchCode').text("");
                    $.ajax({
                        url: 'JQDataFetch/getAllBankBranches.aspx',
                        async: false,
                        data: {
                            BID: $('#DDL_BankList').val(),
                        },
                        success: function (response) {
                            var Branches = response.split('~');
                            Branches.sort();
                            $.each(Branches, function (item) {
                                var datasplit = Branches[item].split('|');
                                $('#DDL_BranchList').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                            });
                        }
                    });
                }
                
            });

            $('#btn_AddAcc').click(function () {

                var chkfield3 = $('#DDL_BankList').valid();
                var chkfield1 = $('#NewAcc_accountname').valid();
                var chkfield2 = $('#NewAcc_accountnumber').valid();
                var chkfield4 = $('#DDL_BranchList').valid();

                if (chkfield1 == true && chkfield2 == true && chkfield3 == true && chkfield4 == true)
                {
                    if ($('#DDL_BankList').val() != "0" || $('#DDL_BranchList').val() != "0")
                    {
                        $.ajax({
                            url: 'Processor/AddBeneficiaryAccount.aspx',
                            data: {
                                BID: $('#SelectedBeneficiaryID').val(),
                                CID: $('#CustomerID').val(),
                                BankID: $('#DDL_BankList :selected').val(),
                                BankName: $('#DDL_BankList :selected').text(),
                                BranchID: $('#DDL_BranchList :selected').val(),
                                BranchName: $('#DDL_BranchList :selected').text(),
                                AccNum: $('#NewAcc_accountnumber').val(),
                                AccName: $('#NewAcc_accountname').val(),
                                CountryID: '2',
                                CurrencyID: $('#NewAcc_TradingCurrency :selected').val(),
                                CurrencyName: $('#NewAcc_TradingCurrency :selected').text()
                            },
                            success: function (response) {
                                alert('Beneficiary Account Added successfully');
                                window.parent.closeAddAccount();
                                window.parent.loadBeneficiaryAccounts($('#SelectedBeneficiaryID').val());
                            }
                        });
                    }
                    else
                    {
                        alert('Please ensure that you have selected a bank and branch');
                    }
                    
                }
                
            });

            $('#btn_EditAccount').click(function () {

                var theStatus = "";

                alert($('#CHK_Active').is(':checked'));
                if ($('#CHK_Active').is(':checked'))
                {
                    theStatus = "Y";
                }
                else
                {
                    theStatus = "N";
                }

                var Check1 = $('#DDL_BankList').valid();
                var Check2 = $('#DDL_BranchList').valid();
                var Check3 = $('#NewAcc_accountname').valid();
                var Check4 = $('#NewAcc_accountnumber').valid();

                if (Check1 == true && Check2 == true && Check3 == true && Check4 == true)
                {
                    $.ajax({
                        url: 'Processor/EditBeneficiaryAccount.aspx',
                        data: {
                            AID: $('#AccountID').val(),
                            BankID: $('#DDL_BankList :selected').val(),
                            BankName: $('#DDL_BankList :selected').text(),
                            BranchID: $('#DDL_BranchList :selected').val(),
                            BranchName: $('#DDL_BranchList :selected').text(),
                            AccNum: $('#NewAcc_accountnumber').val(),
                            AccName: $('#NewAcc_accountname').val(),
                            CurrencyName: $('#NewAcc_TradingCurrency :selected').text(),
                            CountryID: '2',
                            CurrencyID: $('#NewAcc_TradingCurrency :selected').val(),
                            CID: $('#CustomerID').val(),
                            ActStat: theStatus
                        },
                        success: function (response) {
                            alert('Beneficiary Account Successfully Edited');
                            window.parent.closeAddAccount();
                            window.parent.loadBeneficiaryAccounts($('#SelectedBeneficiaryID').val());
                        }
                    })
                }
                
            })

            $('#DDL_BranchList').change(function () {
                $('#TR_BankInformation').show();
                $('#DIV_BankCode').text($('#DDL_BankList').val());
                $('#DIV_BranchCode').text($('#DDL_BranchList').val());
            });

            
        })
    </script>
    <style type="text/css">

        .auto-style1 {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 20px;
            left: 0px;
            top: 3px;
        }

    </style>
</head>


<!-- ************************* BUTTON 10 ADD NEW ACCOUNT MODAL POPUP START ************************* --> 
<body>
	<div id="AddNewAccount"> 
        <form id="form1" runat="server">
            <div class="add_account_form">
                <table class="tbl_width_600">
                    <tr><td>&nbsp;</td></tr>
                    <tr class="wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                    <tr class="wiz_bg_EFEFEF">
                        <td>
                            <table class="tbl_width_560">
                                <tr hidden="hidden"><td><input type="text" id="SelectedBeneficiaryID" runat="server" /> <input type="text" id="AgentID" runat="server" /> <input type="text" id="AccountID" runat="server" /><input type="text" id="CustomerID" runat="server" /><input type="text" id="BeneficiaryName" runat="server" /></td></tr>
                                <tr>
					                <td>
						                <div id="DIV_GenericBank">
							                <table class="tbl_width_560">
                                                <tr>
									                <td class="aa_label_font">BANK NAME *</td>
									                <td class="aa_label_font">&nbsp;</td>
									                <td class="aa_label_font">BRANCH NAME *</td>
								                </tr>
								                <tr>
									                <td style="width:270px;"><div><select id="DDL_BankList" style="width:100%;" required="required"></select></div></td>
									                <td style="width:20px;"></td>
									                <td style="width:270px;"><div><select id="DDL_BranchList" style="width:100%;" required="required"></select></div></td>
								                </tr>
							                </table>
						                </div>
                                    </td>
				                </tr>
                                
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr id="TR_AccountInfo">
					                <td>
					                    <table class="tbl_width_560">
						                    <tr>
							                    <td class="aa_label_font">ACCOUNT NAME *</td>
							                    <td class="aa_label_font">&nbsp;</td>
							                    <td class="aa_label_font">ACCOUNT NUMBER *</td>
                                                <td class="aa_label_font">&nbsp;</td>
							                    <td class="aa_label_font">CURRENCY *</td>
						                    </tr>
						                    <tr>
							                    <td style="width:270px;"><div><input class="aa_input" id="NewAcc_accountname" name="NewAcc_accountname" style="width:100%;" type="text" value="" required="required" /></div></td>
							                    <td style="width:20px;"></td>
							                    <td style="width:190px;"><div><input class="aa_input" id="NewAcc_accountnumber" name="NewAcc_accountnumber" style="width:100%;" type="text"  value="" required="required"  /></div></td>
						                        <td style="width:10px;"></td>
                                                <td style="vertical-align:top;"><select id="NewAcc_TradingCurrency" required="required"></select></td>
                                            </tr>
					                    </table>
					                </td>
				                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>

                                <tr id="TR_BankInformation">
                                    <td>
                                        <table  class="tbl_width_560">
                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_520">
                                                        <tr>
                                                		    <td>
                                                                <div><table><tr class="spacer10"><td>&nbsp;</td></tr></table></div>
						                                        <div id="DIV-Add-Beneficiary-Account-01" class="ag_add_benacc_01">
							                                        <table style="width:100%;">
								                                        <tr>
									                                        <td style="width:25%;" class="ag_add_benacc_02">Bank Code:</td>
									                                        <td style="width:75%;" class="ag_add_benacc_03"><div id="DIV_BankCode"></div></td>
								                                        </tr>
							                                        </table>
						                                        </div>
						                                        <div id="DIV-Add-Beneficiary-Account-02" class="ag_add_benacc_01">
							                                        <table style="width:100%;">
								                                        <tr>
									                                        <td style="width:25%;" class="ag_add_benacc_02">Branch Code:</td>
									                                        <td style="width:75%;" class="ag_add_benacc_03"><div id="DIV_BranchCode"></div></td>
								                                        </tr>
							                                        </table>
						                                        </div>
                                                                <div id="DIV-Add-Beneficiary-Account-04" class="ag_add_benacc_01">
							                                        <table style="width:100%;">
								                                        <tr>
									                                        <td style="width:25%;" class="ag_add_benacc_02">SWIFT:</td>
									                                        <td style="width:75%;" class="ag_add_benacc_03"><div id="DIV_SWIFT"></div></td>
								                                        </tr>
							                                        </table>
						                                        </div>
						                                        
                                                                <div><table><tr class="spacer10"><td>&nbsp;</td></tr></table></div>
					                                        </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="height:20px;"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="wiz_bg_EFEFEF" id="TR_ToggleSwitch">
                        <td>
                            <table class="tbl_width_560">
                                
                                <tr>
                                    <td><label class="auto-style1"><input type="checkbox" id="CHK_Active" checked="checked"><span class="slider round"></span></label></td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="spacer10"><td>
                        
                        </td></tr>
                    <tr>
					    <td>
					        <table class="tbl_width_600">
						        <tr>
								    <td style="text-align: right;"><input class="aa_btn_green" id="btn_AddAcc" type="button" value="Add Account"/><input class="aa_btn_red" id="btn_EditAccount" type="button" value="Update"/></td>
						        </tr>
						    </table>
				        </td>
				    </tr>
                    <tr class="spacer10"><td>&nbsp;</td></tr>

			        </table>
			
				

		        </div>
        </form>
	</div>
<!-- ************************* BUTTON 10 ADD NEW ACCOUNT MODAL POPUP END ************************* --> 
    </body>
