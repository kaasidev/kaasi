﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;


namespace KASI_Extend.InternationalBanking
{
    public partial class AddSriLankaBank : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (!IsPostBack)
            {
                SelectedBeneficiaryID.Value = Request.QueryString["BID"].ToString();
                AgentID.Value = Session["AgentID"].ToString();

                if (!String.IsNullOrEmpty(Request.QueryString["AccID"]))
                {
                    AccountID.Value = Request.QueryString["AccID"].ToString();
                }
                CustomerID.Value = Request.QueryString["CID"].ToString();

                Beneficiary Bene = new Beneficiary();
                BeneficiaryName.Value = Bene.getBeneficiaryName(Request.QueryString["BID"].ToString());
            }
            
        }


    }
}