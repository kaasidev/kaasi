﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kasi-main.Master" AutoEventWireup="true" CodeBehind="DashboardW.aspx.cs" Inherits="KASI_Extend_.DashboardW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.ui.widget.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.iframe-transport.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery.fileupload.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/moment.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/numeric-comma.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/moment.min.js"></script>
	<script src="js/toastr.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jszip.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.print.min.js"></script>


    <!-- CSS FILES -->
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/toastr.min.css" rel="stylesheet" />
	<link href="css/datatables.min.css" rel="stylesheet" />
    <!-- <link href="css/dataTables.custom.dashboard.css" rel="stylesheet" /> -->
	<link href="css/select2.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/kasi_main.css" />
    <link rel="stylesheet" type="text/css" href="css/buttons.dataTables.min.css" />

    <!-- FONTS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<!-- <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" /> -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" /> -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" />
	
    <style>
        .kyc_info_red { /* THIS IS A COMMENT BY SHEHAN */
            background-color: #fcdbd9;
        }
        .kyc_info_green {
            background-color: #76D17F;
        }
        .msg_heading_font_ok {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 1.5em;
            color: #666;
            font-weight: normal;
            padding-bottom: 10px;
            text-align: center;
        }
        .msg_contents_font_ok {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 0.7em;
            color: #999;
            font-weight: normal;
            padding-bottom: 10px;
            text-align: center;
        }

        /*SLIDER CHECKBOX*/
        
        .switch {
            position: relative;
            display: inline-block;
            width: 10px;
            height: 10px;
        }
        /* Hide default HTML checkbox */
        .switch input {
            display:none !important;
        }
        /* The slider */
        .cu_slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #DCDCDC;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .cu_slider:before {
            position: absolute;
            content: "";
            height: 12px;
            width: 18px;
            left: 2px;
            bottom: 2px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .cu_slider:focus {
            outline: none !important;
            -webkit-user-select: none !important;
            -moz-user-select: none !important;
            -ms-user-select: none !important;
            user-select: none !important;
        }
        .chk_label {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 16px;
            left: 0px;
            top: 0px;
        }
        input:checked + .cu_slider {
            background-color: #76D17F;
        }
        input:focus + .cu_slider {
            box-shadow: 0 0 0px #F00;
        }

    input:checked + .cu_slider:before {
        -webkit-transform: translateX(18px);
        -ms-transform: translateX(18px);
        transform: translateX(18px);
    }


    


    </style>


    <script src="js/select2.js"></script>

	<script type="text/javascript">

        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            }
            return val;
        }

        function displayReviewNotes(sTransID) {
            //alert(sCustID);
            $.ajax({
                url: 'JQDataFetch/getCustomerReviewNotes.aspx',
                async: false,
                data: {
                    TID: sTransID,
                },
                success: function (response) {

                    if (response != "NORECORDS") {
                        var initialsplit = response.split('~');
                        $('#DIV_Notes').empty();
                        $.each(initialsplit, function (item) {
                            var datasplit = initialsplit[item].split('|');

                            $('#DIV_Notes').append('<table class="tbl_width_540"><tr><td class="trn_notes_popup_heading">' + datasplit[2] + ' - ' + datasplit[0] + ' - ' + datasplit[3] + ' (' + datasplit[4] + ')' + '</td></tr><tr><td class="trn_notes_popup_details">' + datasplit[1] + '</td></tr><tr class="spacer10"><td>&nbsp;</td></tr></table>');
                        });
                    }
                    else {
                        $('#DIV_Notes').empty();
                    }
                }
            })
        }

        function closeWindow()
        {
            alert('This session has timed out. Please login again.');
            window.top.close();
        }

        function closeAddAccount()
        {
            AddNewBankAccount.dialog('close');
        }

        function progress() {
            size = size + 1;
            if (size > 299) {
                clearTimeout(id);
            }
            document.getElementById("divProgress").style.width = size + "pt";
            document.getElementById("<%=lblPercentage.ClientID %>").firstChild.data = parseInt(size / 3) + "%";
        }

        $(document).ready(function () {
            $.fn.dataTable.moment('dd/mm/yyyy');
            $(".datePickerReport").datepicker({ dateFormat: 'dd/mm/yy' });

            $(".datePickerMonthReport").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '2000:2045'
            });

            $(".datePickerDOB").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2045'
            });
            
            $('#TD_001').removeClass();
            $('#TD_001').addClass("dsh_tab_active");
            $('#DIV_002').hide();
            $('#DIV_003').hide();
            $('#DIV_004').hide();
            

			$.ajaxSetup({
			    beforeSend: function () {
			       
					$(".loading").show();
				},
				complete: function () {
					$(".loading").hide();
				}
			});
            loadcustomer();
			//$("[id$=bus_streettype]").val();
           
            $("#bus_countryofbirth").change(function () {
                $("#bcountry").val($("#bus_countryofbirth :selected").text());
            });
            $("#bus_nationality").change(function () {
                $("#bnationality").val($("#bus_nationality :selected").text());
            });
            $('#ppbus_state').mask('AAA');
            $('#ppbus_state').css('text-transform', 'uppercase');
            $('#bus_state').mask('AAA');
            $('#bus_state').css('text-transform', 'uppercase');
            $('#BPostalState').mask('AAA');
            $('#BPostalState').css('text-transform', 'uppercase');
            $('#BPostalPostcode').mask('0000');
            $('#ppbus_postcode').mask('0000');

            $('#bus_postcode').mask('0000');

            $('#pbus_phonemobile').mask('0000 000 000');
            $('#pbus_phonework').mask('(00) 0000 0000');
            $('#pbus_phonehome').mask('(00) 0000 0000');

            $('#DIV_002').hide();
            $('#DIV_003').hide();
            $('#DIV_004').hide();

            // TABS
            $('#TD_001').click(function () {
                $('#DIV_001').show();
                $('#DIV_002').hide();
                $('#DIV_003').hide();
                $('#DIV_004').hide();
                $('#TD_001').removeClass();
                $('#TD_001').addClass("dsh_tab_active");
                $('#TD_002').removeClass();
                $('#TD_002').addClass("dsh_tab_inactive");
                $('#TD_003').removeClass();
                $('#TD_003').addClass("dsh_tab_inactive");
                $('#TD_004').removeClass();
                $('#TD_004').addClass("dsh_tab_inactive");
            });
            $('#TD_002').click(function () {
                $('#DIV_001').hide();
                $('#DIV_002').show();
                $('#DIV_003').hide();
                $('#DIV_004').hide();
                $('#TD_001').removeClass();
                $('#TD_001').addClass("dsh_tab_inactive");
                $('#TD_002').removeClass();
                $('#TD_002').addClass("dsh_tab_active");
                $('#TD_003').removeClass();
                $('#TD_003').addClass("dsh_tab_inactive");
                $('#TD_004').removeClass();
                $('#TD_004').addClass("dsh_tab_inactive");
            });


        });

        


		function showerror(msg) {
			toastr.options = {
				closeButton: true,
				progressBar: true,
				showMethod: 'slideDown',
				timeOut: 6000
			};
			toastr.error(msg, 'Error');
		   
		}
		function showsuccess(msg, body) {
			toastr.options = {
				closeButton: true,
				progressBar: true,
				showMethod: 'slideDown',
				timeOut: 6000
			};
			toastr.success(msg, body);
		  
        }


        function submitTransaction()
        {
              // alert($("#<%=hidden_CountryID.ClientID%>").val());
            sendingTotal = $('#SM_div_lkrvalue').text();
            $.ajax({
                url: 'Processor/AddTransaction.aspx',
                beforeSend: function () {
                    ShowSendMoney.dialog('close');
                    LoadingDIV.dialog('open');
                },
                data: {
                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                            CLN: $('#lastname').val(),
                            BID: $('#<%=hidden_selectedBeneficiary.ClientID%>').val(),
                            BN: $('#div_BeneficiaryName').text(),
                            AID: $('#<%=txt_hidden_AccountID.ClientID%>').val(),
                            Rate: $('#SM_exchangerate').val(),
                            DM: $('#SM_amounttobesent').val(),
                            SC: $('#SM_servicecharge').val(),
                            RA: sendingTotal,
                            PT: 'N',
                            CUR: $("#hidden_currencycode").val(),
                            ST: 'PENDING',
                            AB: $('#txt_remainingaccountbalance').val(),
                            Bank: $('#sendingBank').val(),
                            Branch: $('#sendingBankBranch').val(),
                            Purpose: $('#SM_Purpose').val(),
                            Source: $('#SM_SourceOfFunds').val(),
                            CR: $('#hidden_compliancereason').val(),
                            Notes: $('#TXT_Add_TRN_Notes').val(),
                            CountryID: $("#<%=hidden_CountryID.ClientID%>").val(),
                        },
                        complete: function () {
                            LoadingDIV.dialog('close');
                        },
                        success: function (response) {
                            var datasplit = response.split('|');
                            showsuccess('', datasplit[0]);
                            setTimeout(function () {
                                $.ajax({
                                    url: 'FillPDF.aspx',
                                    aynsc: false,
                                    data: {
                                        TID: datasplit[1],
                                    },
                                    success: function (data) {
                                        //alert('PDF File now available');
                                    },

                                });
                                // location.reload();
                                ShowSendMoney.dialog('close');
                            }, 1000);
                            LoadCreditDebitTable();
                            LoadTransactionTable();
                            ChangeTrasactionStatus(datasplit[1]);
                            location.reload();
                        }
                    });
        }


        function loadcustomer() {
            $.ajax({
                url: 'JQDataFetch/getcustomerdetails.aspx?id=<%=Request.QueryString["custid"] %>',

                success: function (res) {
                    var response = $.parseJSON(res);
                    $("#hidCustId").val(response.CustomerID);
                    if (response.IsIndividual == false) {
                        $("#isbusi").val('busi');
                        $("#BBusinessName").val(response.BusinessName);
                        $("#BABN").val(response.ABN);
                        $("#BTradingName").val(response.TradingName);
                        $("#BBContact").val(response.BusinessContact );
                        $("#BBEmail").val(response.BusinessEmail);
                        $("#BBWeb").val(response.BusinessWebSite);
                        $("#BAddressLine1").val(response.AddressLine1);
                        $("#BPostalAddressLine2").val(response.PostalAddressAddressLine2);
                        $("#pos_unitno").val(response.PostalAddressUnitNo);
                        $("#pos_streetno").val(response.PostalAddressStreetNo);
                        $("#pos_streetname").val(response.PostalAddressStreetName);
                        $("#pos_streettype").val(response.PostalAddressStreetType);
                        $("#BPostalSuburb").val(response.PostalAddressSuburb);

                        $("BPostalState").val(response.PostalAddressState);

                        $("BPostalPostcode").val(response.PostalAddressPostcode);
                        $("pos_country").val(response.PostalAddressCountry);

                        $("bus_unitno").val(response.PrincipleUNitNo);
                        $("bus_streetno").val(response.PrincipleStreetNo);
                        $("bus_streetname").val(response.PrincipleStreetName);
                        $("bus_streettype").val(response.PrincipleStreetType);
                        $("bus_suburb").val(response.PrincipleStreetSuburb);
                        $("bus_state").val(response.PrincipleStreetState);

                        $("bus_postcode").val(response.PrincipleStreetPosalCode);
                        $("bus_country").val(response.PrincipleCountry);
                    }
                    else {
                        $("#isbusi").val('indi');
                        $("#TD_EditBusiness_001").hide();
                        $('#TD_EditBusiness_002').removeClass('wiz_tab_inactive_right');
                        $('#TD_EditBusiness_002').addClass('wiz_tab_active');
                        $("#divBusiness").hide();
                        $("#tblIndi").show();
                        
                        
                    }
                    $("#pbus_unitno").val(response.UnitNo);
                    $("#pbus_streetno").val(response.StreetNo);
                    $("#pbus_streetname").val(response.StreetName);
                    $("#pbus_streettype").val(response.StreetType);
                    $("#pbus_addressline2").val(response.AddressLine2);
                    $("#ppbus_suburb").val(response.Suburb);
                    $("#ppbus_state").val(response.State);

                    $("#ppbus_postcode").val(response.Postcode);
                    $("#pbus_country").val(response.Country);
                    $("#bus_lastname").val(response.LastName);
                    $("#bus_title").val(response.Title);
                    $("#bus_givenNames").val(response.FirstName);
                    $("#bus_dob").val(response.DOB);
                    $("#bus_placeofbirth").val(response.PlaceOfBirth);
                    $("#bus_nationality").val(response.Nationality);
                    $("#bus_aka").val(response.AKA);
                    $("#bus_occupation").val(response.Occupation);
                    $("#pbus_phonework").val(response.TelWork);
                    $("#pbus_phonemobile").val(response.Mobile);
                    $("#pbus_phonehome").val(response.TelHome);
                    $("#pbus_email1").val(response.EmailAddress);
                    $("#pbus_email2").val(response.EmailAddress2);
                    $("#edit_notes").val(response.Notes);
                    $("#edit_password").val(response.AccountPassword);
                    
                    if ((response.AccountPassword == "" || response.AccountPassword == null) && (response.Notes == "" || response.Notes == null)) {
                        $("[id$=NotesAreaTR]").hide();
                    }
                    else {
                        if (response.AccountPassword == "" || response.AccountPassword == null)
                        {
                            $("#trpassword").hide();
                        }
                        else {
                            $("#trpassword").show();
                            $("#showpassword").html(response.AccountPassword);

                        }
                        if (response.Notes == "" || response.Notes == null) {
                            $("#trnotes").hide();
                        }
                        else {
                            $("#trnotes").show();
                            $("#shownotes").html(response.Notes);
                        }
                        $("[id$=NotesAreaTR]").show();
                    }
                  
                    $("#bus_countryofbirth option").filter(function () {
                        return this.text == response.CountryOfBirth;
                    }).attr('selected', true);
                    $("#bus_nationality option").filter(function () {
                        return this.text == response.Nationality;
                    }).attr('selected', true);
                    $("#bcountry").val(response.CountryOfBirth);
                    $("#bnationality").val(response.Nationality);
                    
                }
            });
        }

        



		function loadtitlecustomer() {
		    $.ajax({
		        url: 'JQDataFetch/getCustomerTitle.aspx',
		       
		        success: function (response) {
		            if (response = '') {
		                $("#title").val("0");
		            }
		            else {
		                $("#title").val(response);
		            }
		        }
		    });
		}

		function EditBusiness() {
            var typ = $("#isbusi").val();
         //   $("#isbusi").val("busi");
            if (isValidForm(typ)) {
                var formData = $("#formEditCust").serialize();
                // console.log(formData);
                $.ajax({
                    url: 'Processor/AddNewBusiness.aspx',
                    type: 'post',
                    data: formData,
                    success: function (data) {
                        showsuccess(data);
                        location.reload();
                    },
                    error: function (xhr, err) {
                        showerror("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                        showerror("responseText: " + xhr.responseText);
                    }

                });
            }
            else {
                showerror("Please fill all fields mark with astrick(*)");
            }

        }
        function isValidForm(type) {
            if (type == "indi") {
                if ($('#formEditCust').valid() && $('#bus_lastname').val() != '' && $('#bus_title').val() != '' && $('#bus_firstnames').val() != '' && $('#bus_countryofbirth').val() != ''
                    && $('#bus_dob').val() != '' && $('#bus_placeofbirth').val() != '' && $('#bus_nationality').val() != '') {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                if ($('#formEditCust').valid() && $("#bus_lastname").val() != ''
                    && $("#bus_title").val() != '0' && $("#bus_givenNames").val() != '' && $("#bus_countryofbirth").val() != '0'
                    && $("#bus_dob").val() != '' && $("#bus_placeofbirth").val() != '' && $("#bus_nationality").val() != '0'
                    && $("#pbus_streetno").val() != '' && $("#pbus_streetname").val() != '' && $("#pbus_streettype").val() != '0' && $("#ppbus_suburb").val() != ''
                    && $("#ppbus_state").val() != '' && $("#ppbus_postcode").val() != '' && $("#pbus_phonemobile").val() != '' && $("#pbus_email1").val() != '' && $('#bus_lastname').val() != '' && $('#bus_title').val() != '' && $('#bus_firstnames').val() != '' && $('#bus_countryofbirth').val() != ''
                    && $('#bus_dob').val() != '' && $('#bus_placeofbirth').val() != '' && $('#bus_nationality').val() != '') {

                    return true;
                }
                else {
                    return false;
                }
            }
        }
		function openSendMoney(BID, BankName, BankBranch, CurrencyType) {
            //alert(CurrencyType);
		    $('#<%=txt_hidden_AccountID.ClientID%>').val(BID);

		    // Fill the CountryID and CurrencyID hidden fields.
		    $.ajax({
		        url: 'JQDataFetch/getBeneficiaryAccountCurrencyandCountry.aspx',
		        data: {
                    AID: BID,
		        },
		        success: function (response) {
		            dsplit = response.split('|');
		            $("#<%=hidden_CountryID.ClientID%>").val(dsplit[0]);
		            $("#<%=hidden_CurrencyID.ClientID%>").val(dsplit[1]);
		        }
		    })
		    
			if($('#<%=hidden_txt_accountbalance.ClientID%>').val() != 0)
			{
			    $("#hidden_currencycode").val(CurrencyType);
			    $("#divCurrencySending").text(CurrencyType);
			    $('#TR_SM_NegativeSend').hide();
			    $('#TR_SM_ExceedSend').hide();
				$('#SM_btn_Confirm').hide();
				$('#SM_btn_Cancel').hide();
				var todayrateint = 0;
				$('#sendingBank').val(BankName);
				$('#sendingBankBranch').val(BankBranch);               
				ShowSendMoney.dialog('open');
				$('#SM_btn_Send').show();
				$('#SM_btn_Confirm').attr("disabled", false);
				$('#SM_servicecharge').attr("readonly", true);
				$('#SM_exchangerate').attr("readonly", true);
				var AccBalance = $('#<%=hidden_txt_accountbalance.ClientID%>').val()
				$('#SPN_AddTR_AvailableFunds').text('$' + parseFloat(AccBalance, 10).toFixed(2));
				//$('#SM_availablefunds').val($('#<%=hidden_txt_accountbalance.ClientID%>').val());

			    var amtServiceCharge = calculateServiceCharge($('#<%=hidden_txt_accountbalance.ClientID%>').val());
			    if (amtServiceCharge == "Undefined")
			    {
                    alert('You have not setup Transaction Fees for this currency. Please complete this before remitting funds.')
			    }
				$('#SM_servicecharge').val(parseFloat(amtServiceCharge).toFixed(2));

				var amtToSend = parseFloat(AccBalance) - parseFloat($('#SM_servicecharge').val());
				$('#SM_amounttobesent').val(parseFloat(amtToSend).toFixed(2));
				$('#SM_div_sending').text("$ " + parseFloat(AccBalance).toFixed(2));

				var disExRate = getRateForCountryCurrency();
				$('#SM_exchangerate').val(parseFloat(disExRate).toFixed(2));
				$('#SM_div_todayrate').text(parseFloat(disExRate).toFixed(2));

				var totalSend = calculateSendingTotal(amtToSend, disExRate);
				$('#SM_div_lkrvalue').text(parseFloat(totalSend).toFixed(2));
				$('#txt_hidden_foreignamount').val(parseFloat(totalSend).toFixed(2));

				$('#hidden_enoughfunds').val("Y");
				
				return false;
			}
			else
			{
				showerror('Please reload customer credit before proceeding with transaction');
				$('#hidden_enoughfunds').val("N");
			}    
		}

		function calculateSendingTotal(amount, exrate)
		{
			var total = parseFloat(amount) * parseFloat(exrate);
			return total;
		}

		function getRateForCountryCurrency()
		{
			var setRate = 0;
			$.ajax({
				url: 'JQDataFetch/getExchangeRateForAgent.aspx',
				async: false,
				data: {
					AID: $('#<%=hidden_AgentID.ClientID%>').val(),
					AccID: $('#<%=txt_hidden_AccountID.ClientID%>').val()
				},
                success: function (response) {
                    //alert(response);
					setRate = response;
				}
			})

			return setRate;
		}

		function updateIDPicture()
		{
			$('#<%=Image1.ClientID%>').attr("src", "images/id-card.png");
			$('#<%=Image1.ClientID%>').attr("title", "Customer Identification Completed");
			$('#<%=hidden_hasID.ClientID%>').val('Y');
		}

		function onCompletionReload()
		{
		    var url = window.location.href;
		 
		    window.location.href = url;
		    window.location.reload();
		   
		}

		function onCompletionReloadDocTable()
		{
			FileMainTable.ajax.reload(null, false);
		}

		function highlightRiskLevel()
		{
			$.ajax({
				url: 'JQDataFetch/getCustomerRiskLevel.aspx',
				data: {
					CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
				},
				success: function(data) {
					$('#<%=hidden_risklevel.ClientID%>').val(data);
					if (data == 1)
					{
						$('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
						$('#<%=td_risk2.ClientID%>').attr("class", "risk_neutral");
						$('#<%=td_risk3.ClientID%>').attr("class", "risk_neutral");
						$('#<%=td_risk4.ClientID%>').attr("class", "risk_neutral");
					}
					else if (data == 2)
					{
						$('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
						$('#<%=td_risk2.ClientID%>').attr("class", "risk_2");
						$('#<%=td_risk3.ClientID%>').attr("class", "risk_neutral");
						$('#<%=td_risk4.ClientID%>').attr("class", "risk_neutral");
					}
					else if (data == 3)
					{
						$('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
						$('#<%=td_risk2.ClientID%>').attr("class", "risk_2");
						$('#<%=td_risk3.ClientID%>').attr("class", "risk_3");
						$('#<%=td_risk4.ClientID%>').attr("class", "risk_neutral");
					}
					else
					{
						$('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
						$('#<%=td_risk2.ClientID%>').attr("class", "risk_2");
						$('#<%=td_risk3.ClientID%>').attr("class", "risk_3");
						$('#<%=td_risk4.ClientID%>').attr("class", "risk_4");
					}
				}
			})
		}

        function populateBeneficaryDetails(BID) {
			$('#DIV_BeneficiaryDetails').show();
			$('#tr_ben_info').show();
			$('#<%=hidden_selectedBeneficiary.ClientID%>').val(BID);
			$.ajax({
                url: 'JQDataFetch/getCountriesForAgents.aspx',
				async: false,
				success: function(response) {
					$('#EditBene_CountryDDL').empty();
					$('#EditBene_stateprovince').empty();
					$('#EditBene_suburbdistrict').empty();
					var data = response.split('~');
					data.sort();
					$('#EditBene_CountryDDL').append('<option value="">-- SELECT --</option>');
					$.each(data, function(item)
					{
						var datasplit = data[item].split('|');
						$('#EditBene_CountryDDL').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
					});
				}
			})
			$.ajax({
				url: 'JQDataFetch/getBeneficiaryDetails.aspx',
				async: false,
				data: {
					BeneID: BID,
				},
				beforeSend: function () {
					LoaderWait.dialog('open')
				},
				complete: function () {
					LoaderWait.dialog('close')
				},
                success: function (data) {

					var splitdata = data.split('|');
					$('#EditBene_lastname').val(splitdata[1]);
                    $('#EditBene_firstnames').val(splitdata[2]);
                    $('#div_BeneficiaryName').text(splitdata[18]);
					$('#EditBene_addressline1').val(splitdata[3]);
					$('#EditBene_addressline2').val(splitdata[4]);
					$('#EditBene_postzipcode').val(splitdata[7]);
					$('#EditBene_CountryDDL option:contains('+ splitdata[8] +')').attr('selected', 'selected');

					$('#EditBene_stateprovince').empty();
					$('#EditBene_suburbdistrict').empty();
					$.ajax({
						url: 'JQDataFetch/getCountryProvinces.aspx',
						async: false,
						data: {
							CID: $('#EditBene_CountryDDL option:selected').val(),
						},
						success: function(data) {
							var firstdata = data.split('~');
							firstdata.sort();
							$('#EditBene_stateprovince').append('<option value="">-- SELECT --</option>');
                            $.each(firstdata, function (item) {
                                var datasplit = firstdata[item].split('|');
                                if (datasplit[0] == splitdata[6])
                                {
                                    $('#EditBene_stateprovince').append('<option selected value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                                }
                                else
                                {
                                    $('#EditBene_stateprovince').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                                }
								
								
							});
						}
					})

                    //alert(splitdata[6]);
					//$('#EditBene_stateprovince option:contains("' + splitdata[6] + '")').prop('selected', true);

					$('#EditBene_suburbdistrict').empty();
					$.ajax({
						url: 'JQDataFetch/getProvinceDistricts.aspx',
						async: false,
						data: {
							CID: $('#EditBene_stateprovince :selected').val(),
						},
						success: function(data) {
							var firstdata = data.split('~');
							firstdata.sort();
							$('#EditBene_suburbdistrict').append('<option value="">-- SELECT --</option>');
							$.each(firstdata, function(item){
                                var datasplit = firstdata[item].split('|');
                                if (datasplit[0] == splitdata[5])
                                {
                                    $('#EditBene_suburbdistrict').append('<option selected value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                                }
                                else {
                                    $('#EditBene_suburbdistrict').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                                }
								
							});
						}
					})

					//$('#EditBene_suburbdistrict option:contains("' + splitdata[5] + '")').prop('selected', true);

					$('#EditBene_contactnumber').val(splitdata[9]);
					$('#EditBene_email').val(splitdata[10]);
					$('#EditBene_iddetails').val(splitdata[11]);
					$('#EditBene_relationship').val(splitdata[12]);
					if (splitdata[13] != "")
					{
					    $('#EditBene_countryofbirth option:contains("' + splitdata[13] + '")').prop('selected', true);
					}
					
					$('#EditBene_dob').val(splitdata[14].substring(0,10));
					$('#Editbene_placeofbirth').val(splitdata[15]);
					if (splitdata[16] != "")
					{
					    $('#EditBene_nationality option:contains("' + splitdata[16] + '")').prop('selected', true);
					}
					
					$('#EditBene_aka').val(splitdata[17]);
                    
					$('#div_BeneficiaryAddress').text(splitdata[3] + ' ' + splitdata[4] + ' ' + splitdata[8] + ' ' + splitdata[6] + ' ' + splitdata[5]);
					$('#div_BeneficiaryNationalityCardID').text(splitdata[11]);
					$("#div_BeneficiaryTelHome").text(splitdata[9]);
					$('#div_BeneficiaryRelationship').text(splitdata[12]);
					$('#div_BeneficiaryEmailAddress').text(splitdata[10]);
					$("#div_BeneficiaryAKA").text(splitdata[17]);
					$("#div_BeneficiaryRelation").text(splitdata[12]);

					loadBeneficiaryAccounts(BID);
					disableOverCount(BID);
					disableHomeDelivery(BID);

				}
			});
		}

		function disableHomeDelivery(BID)
		{
			$.ajax({
				url: 'JQDataFetch/checkBeneficiaryForHomeDelivery.aspx',
				data: {
					BeneID: BID,
				},
				success: function(response)
				{
					if (response == "NO")
					{
						$('#btn_HomeDelivery').removeClass("btn_blue");
						$('#btn_HomeDelivery').addClass("btn_disabled");
						$('#btn_HomeDelivery').prop('onclick', null).off('click');
					}
					else
					{

					}
				}
			});
		}

		function disableOverCount(BID)
		{
			$.ajax({
				url: 'JQDataFetch/checkBeneficiaryHasID.aspx',
				data: {
					BeneID: BID,
				},
				success: function(response) {
					if (response == "NO")
					{
						$('#btn_OverTheCounter').removeClass("btn_blue");
						$('#btn_OverTheCounter').addClass("btn_disabled");
						$('#btn_OverTheCounter').prop('onclick', null).off('click');
					}
					else (response == "YES")
					{
						//$('#btn_OverTheCounter').removeClass("btn_disabled");
						//$('#btn_OverTheCounter').addClass("btn_blue");
						//$('#btn_OverTheCounter').prop('onclick', null).off('click');
					}
					
				}
			});

		}

		function openMsgDIV(CurrentDialogName, Message, ClickAction)
		{
			$('#msgtext').text(Message);
			if (ClickAction == '1')
			{
				$('#ok_button_dialog').attr('onClick', 'onCompletionReload()');
			}
			else if (ClickAction = '2')
			{
				$('#ok_button_dialog').attr('onClick', 'onCompletionReloadDocTable()');
			}
			MsgDIV.dialog('open');
		}

		function loadBeneficiaryAccounts(BID)
		{
			$.ajax({
				url: 'JQDataFetch/getBeneficiaryAccounts.aspx',
				data: {
					BeneID: BID,
				},
                async: false,
				success: function (response) {
                    
					$('#BeneAccountTable > tbody').empty();
					if (response != 'NORESULTS')
					{
                        var maindata = response.split('~');
                        //alert(maindata[0]);
						$.each(maindata, function(index, item)
						{
						    var spdata = maindata[index].split('|');

                            if (spdata[5] == "Y")
                            {
                                $('#BeneAccountTable').append('<tr><td class="tbl_contents_ben_fnt_01" style="background-color:#FFFFFF;">' + spdata[1] + '</td><td class="tbl_contents_ben_fnt_02" style="background-color:#FFFFFF;">' + spdata[2] + '</td><td class="tbl_contents_ben_fnt_02" style="background-color:#FFFFFF;">' + spdata[3] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:40px;">' + spdata[4] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:40px;">' + spdata[5] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:60px;"><button class="btn_red_nomargin_sml" type="button" id="btn_BenEdit" onclick="openBeneficiaryAccountEdit(\'' + spdata[1] + '\',\'' + spdata[2] + '\',\'' + spdata[3] + '\',\'' + spdata[0] + '\',\'' + spdata[6] + '\',\'' + spdata[7] + '\',\'' + spdata[8] + '\');">EDIT</button></td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:60px;"><button type="button" class="btn_blue_nomargin_sml" id="btn_BenSend" onclick="openSendMoney(\'' + spdata[0] + '\',\'' + spdata[1] + '\',\'' + spdata[2] + '\',\'' + spdata[4] + '\')">SEND</button></td></tr>');
                            }
                            else
                            {
                                $('#BeneAccountTable').append('<tr><td class="tbl_contents_ben_fnt_01" style="background-color:#FFFFFF;">' + spdata[1] + '</td><td class="tbl_contents_ben_fnt_02" style="background-color:#FFFFFF;">' + spdata[2] + '</td><td class="tbl_contents_ben_fnt_02" style="background-color:#FFFFFF;">' + spdata[3] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:40px;">' + spdata[4] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:40px;">' + spdata[5] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:60px;"><button class="btn_red_nomargin_sml" type="button" id="btn_BenEdit" onclick="openBeneficiaryAccountEdit(\'' + spdata[1] + '\',\'' + spdata[2] + '\',\'' + spdata[3] + '\',\'' + spdata[0] + '\',\'' + spdata[6] + '\',\'' + spdata[7] + '\',\'' + spdata[8] + '\');">EDIT</button></td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:60px;"><button type="button" disabled="disabled" class="btn_grey_nomargin_sml" id="btn_BenSend">SEND</button></td></tr>');
                            }
							
						});
					}
					
					
				}
			});
		}

		function openBeneficiaryAccountEdit(BankName, BranchName, AccountNumber, AccountID, CountryID, AccountName, CurrencyID)
        {
            
            $('#DIV_AddFrameLoader').load('InternationalBanking/AddSriLankaBank.aspx?BID=' + $('#<%=ddl_List_Beneficiaries.ClientID%>').val() + '&AccID=' + AccountID + '&CID=' + $('#<%=txt_hidden_CustomerID.ClientID%>').val());            
            AddNewBankAccount.dialog('open');
            //$('#DIV_AddNewBankAccount').parent().appendTo($("form:first"));
		}

		

		function calculateAmountToBeSent(amt, CT)
		{
			var avfund = amt;
			if (CT == 'LKR')
			{
				if (avfund <= 1000)
				{
					var amtsent = avfund - 10;
					return amtsent;
				}
				else
				{
					var amtsent = avfund - 15;
					return amtsent;
				}
			}
			else 
			{
				var amtsent = avfund - 25;
				return amtsent;
			}
			
		}

		function calculateServiceCharge(amount)
		{
			var agentID = $('#<%=hidden_AgentID.ClientID%>').val();
			var AccountID = $('#<%=txt_hidden_AccountID.ClientID%>').val();
			var ServiceCharge = 0;

			$.ajax({
				url :'JQDataFetch/getAgentServiceCharge.aspx',
				async: false,
				data: {
					ATBR: amount,
					AID: agentID,
					AccID: AccountID
				},
				success: function(response) {
					ServiceCharge = response;
				}
			});

			return ServiceCharge;

		}

		function updateWarningPanel(CID)
		{

		}
		

		function LoadTransactionTable() {
		    $('#example').DataTable().destroy();
                table = $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
		            ajax: {
		                url: 'JQDataFetch/getCustomerPastRecords.aspx',
		                data: {
		                    custid: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                }
		            },
                    "columnDefs": [
                        {
                            "targets": -1,
                            "data": null,
                            "defaultContent": "<a>Open</a>"
                        },
                        { className: "align__left", "targets": [0, 1, 2, 8] },
                        { className: "align__right", "targets": [3, 4, 5, 6, 7, 9] },
		            ],
		            "order": [[ 0, "desc" ]]
                });
		    $('#example tbody').on('click', 'a', function () {
		        var data = table.row($(this).parents('tr')).data();
		        openViewTransaction(data[0], data[8].toUpperCase(), data[4], data[3], data[7]);
		      //  alert(data[0] + "'s salary is: " + data[8]);
		    });
        }

	    function openViewTransaction(TID, sStatus, sDollarAmount, sRate, sReceiveAmount) {
	        //debugger;
	        //alert(TID);
          
            //$('#txt_workingTransID').val(TID);
            $("#DIV_CompReview_Main").show();
            $("#DIV_CompReview_Limits").hide();
            $("#DIV_CompReview_Notes").hide();
            $("#DIV_CompReview_Documents").hide();
            $("#DIV_CompReview_Audit").hide();
            $('#Review_TD_001').removeClass();
            $('#Review_TD_001').addClass("wiz_tab_active");
            $('#Review_TD_002').removeClass();
            $('#Review_TD_002').addClass("wiz_tab_inactive_right");
            $('#Review_TD_003').removeClass();
            $('#Review_TD_003').addClass("wiz_tab_inactive_right");
            $('#Review_TD_004').removeClass();
            $('#Review_TD_004').addClass("wiz_tab_inactive_right");
            $('#Review_TD_005').removeClass();
            $('#Review_TD_005').addClass("wiz_tab_inactive_right");

            $('#REVW_ApprovePIN').hide();

                if (sStatus == "MASTER REVIEW") {
                    $("#REVW_btn_Submit").hide();
                    $("#REVW_btn_Approve").hide();
                    $('#TR_Confirmation').hide();
                    $('#APPR_RoutingBank').hide();
                    $('#LST_ReviewReasons').show();
                    $('#SPN_Status').text("RNP REVIEW PENDING");
                }
                else if (sStatus == "PENDING") {
                    $('#TR_Confirmation').show();
                    $("#REVW_btn_Submit").hide();
                    $('#APPR_RoutingBank').show();
                    $('#LST_ReviewReasons').hide();
                    $('#SPN_Status').text("PENDING");
                }
                else if (sStatus == "REVIEW") {
                    $("#REVW_btn_Submit").show();
                    $("#REVW_btn_Approve").hide();
                    $('#TR_Confirmation').hide();
                    $('#APPR_RoutingBank').hide();
                    $('#LST_ReviewReasons').show();
                    $("#SPN_Status").text("AFFILIATE REVIEW PENDING");
                }
                else {
                    $("#REVW_btn_Submit").hide();
                    $("#REVW_btn_Approve").hide();
                    $('#TR_Confirmation').hide();
                    $('#APPR_RoutingBank').hide();
                    $('#LST_ReviewReasons').hide();
                    $('#SPN_Status').text(sStatus);
                }

                $('#REVW_DisplayTransID').text(TID);

                var AMonthlyAUD;
                var AYearlyAUD;
                var AMonthlyNb;
                var AYearlyNb;

                var CMonthlyAUD;
                var CYearlyAUD;
                var CMonthlyNb;
                var CYearlyNb;

                $.ajax({
                    url: 'JQDataFetch/getAgentTransLimit.aspx',
                    async: false,
                    success: function (response) {
                        var datasplit = response.split('|');
                        AMonthlyAUD = datasplit[0];
                        AYearlyAUD = datasplit[1];
                        AMonthlyNb = datasplit[2];
                        AYearlyNb = datasplit[3];
                    }
                });

                $.ajax({
                    url: 'JQDataFetch/getCustomerTotals.aspx',
                    async: false,
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    },
                    success: function (data) {
                        var datasplit = data.split('|');
                        CMonthlyAUD = datasplit[0];
                        CYearlyAUD = datasplit[1];
                        CMonthlyNb = datasplit[2];
                        CYearlyNb = datasplit[3];
                    }
                });


                $("#Last30daysDollar").text(commaSeparateNumber(parseFloat(CMonthlyAUD).toFixed(2)));
                $('#ALast30daysDollar').text(commaSeparateNumber(parseFloat(AMonthlyAUD).toFixed(2)));

                $("#LastYearDollar").text(commaSeparateNumber(parseFloat(CYearlyAUD).toFixed(2)));
                $("#ALastYearDollar").text(commaSeparateNumber(parseFloat(AYearlyAUD).toFixed(2)));

                $("#Last30daysNb").text(CMonthlyNb);
                $('#ALast30daysNb').text(AMonthlyNb);

                $('#LastYearNb').text(CYearlyNb);
                $('#ALastYearNb').text(AYearlyNb);

                $('#REWV_TransID').text(TID);

                if (sStatus == "REVIEW" || sStatus == "MASTER REVIEW") {
                    $.ajax({
                        url: 'JQDataFetch/getTransactionReviewReasons.aspx',
                        async: false,
                        data: {
                            TID: TID,
                        },
                        success: function (data) {
                            $("#LST_ReviewReasons").empty();
                            $("#TBL_ReviewReasons").empty();
                            //$("#TBL_ReviewReasons").append('<tr><td>&nbsp;</td></tr>');
                            $("#TBL_ReviewReasons").append('<tr><td class="auto-style1">ERROR!</td></tr>');
                            $("#TBL_ReviewReasons").append('<tr><td class="error_font" style="height:15px;">This Transaction needs to be reviewed by a Compliance Officer for the following reason/s.</td></tr>');
                            if (data != "NORECORDS") {

                                var splitdata = data.split('|');
                                $.each(splitdata, function (key, value) {
                                    $('#TBL_ReviewReasons').append('<tr><td class="error_font" style="height:15px;">* ' + value + "</td></tr>");
                                    $('#LST_ReviewReasons').append('<li><a>' + value + '</a></li>');

                                });
                                $('#TBL_ReviewReasons').append('<tr class="spacer10"><td>&nbsp;</td></tr>');
                            }

                        }
                    })
                }


                //$('#txt_workingDialog').val(sOrigin);
                $('#REVW_TransID').val(TID);
                $('#REVW_CustID').val($('#<%=txt_hidden_CustomerID.ClientID%>').val());
                //alert($('#REVW_CustID').val());

                $.ajax({
                    url: 'JQDataFetch/getTransactionType.aspx',
                    data: {
                        TID: TID,
                    },
                    success: function (TransType) {
                        $("#REVW_TransType").text(TransType);
                    }
                })

                //var sCustID = $('td', this).eq(2).text();
                $('#REVW_CustomerID').html($('#<%=txt_hidden_CustomerID.ClientID%>').val());

                $.ajax({
                    url: 'JQDataFetch/getCustomerDetailsForTransView.aspx',
                    async: false,
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    },
                    success: function (data) {
                        var splitdata = data.split('|');
                        $('#REVW_CustName').text(splitdata[0]);
                        $("#REVW_CustAddress").text(splitdata[1]);
                        $("#REVW_CustMobile").text(splitdata[2]);
                        $("#REVW_Occupation").text(splitdata[4]);
                    },

                });

                var sBeneID = "";
                $.ajax({
                    url: 'JQDataFetch/getBeneficiaryIDFromTransaction.aspx',
                    async: false,
                    data: {
                        TID: TID
                    },
                    success: function (response) {
                        sBeneID = response;
                    }
                })

                var sBeneName = '';
                $.ajax({
                    url: 'JQDataFetch/getBeneficiaryDetailsForTransView.aspx',
                    async: false,
                    data: {
                        BID: sBeneID,
                        TID: TID
                    },
                    success: function (response) {
                        //alert(response);
                        //sBeneName = response;
                        var sdata = response.split('|');
                        $('#REVW_BeneName').text(sdata[0]);
                        $("#REVW_BeneAddress").text(sdata[1]);
                        $("#REVW_BeneMobile").text(sdata[2]);
                        $('#REVW_BeneRelationship').text(sdata[9]);
                        $("#REVW_OriginalBank").text(sdata[10]);
                    }
                })



                $('#REVW_DollarAmount').text(commaSeparateNumber(sDollarAmount));


                $('#REVW_Rate').text(sRate);

                var sRsAmount = '';
                $.ajax({
                    url: 'JQDataFetch/getRemittedAmountFromTransaction.aspx',
                    async: false,
                    data: {
                        TID: TID
                    },
                    success: function (response) {
                        //alert(response);
                        data = response.split('|');
                        $('#REVW_CountryID').val(data[8]);
                        $("#REVW_ServiceFee").text(parseFloat(data[1]).toFixed(2));
                        $("#txt_ServiceFee").val(parseFloat(data[1]).toFixed(2));
                        $("#REVW_AUDAmount").text(parseFloat(data[2]).toFixed(2));
                        sRsAmount = parseFloat(data[2]).toFixed(2) + ' AUD';
                        $("#REWV_DateTime").text(data[9]);
                        $("#REVW_RsAmount").text(commaSeparateNumber(sReceiveAmount) + ' ' + data[3])
                        $("#REVW_Purpose").text(data[4]);
                        $("#REVW_SourceOfFunds").text(data[5]);
                        $("#REVW_Notes").text(data[6]);
                        $('#REVW_CreatedDateTime').text(data[10]);
                    }
                })
                //$('#REVW_RsAmount').text(sReceiveAmount);


                var BBank = '';
                $.ajax({
                    url: 'JQDataFetch/getBankBranchFromTransaction.aspx',
                    async: false,
                    data: {
                        TID: TID
                    },
                    success: function (response) {
                        BBank = response;
                    }
                })
                //$('#REVW_OriginalBank').text(OBank + " / " + BBank);

                //$.ajax({
                //    url: 'JQDataFetch/getRoutingBankList.aspx',
                //    async: false,
                //    data: {
                //        CID: $('#REVW_CountryID').val(),
                //    },
                //    success: function (data) {
                //        var Banks = data.split('~');
                //        Banks.sort();
                //        $('#REVW_RoutingBankddl').empty();
                //        $('#REVW_RoutingBankddl').append('<option value="">-- SELECT -- </option>');
                //        $.each(Banks, function (item) {
                //            var datasplit = Banks[item].split('|');
                //            $('#REVW_RoutingBankddl').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                //        });
                //        //$('#RoutingBankddl').attr('disabled', true);
                //        $('#REVW_btn_OverideRoutingBank').show();
                //    }
                //});

                //$.ajax({
                //    url: 'JQDataFetch/getDefaultRoutingBank.aspx',
                //    async: false,
                //    data: {
                //        TID: TID,
                //    },
                //    success: function (data) {
                //        var splitdata = data.split('|');
                //        $('#REVW_RoutingBankddl').val(splitdata[0]);

                //        if (splitdata[1] == 'NOROUTING') {
                //            $('#REVW_TR_RoutingInfo').hide();
                //        }
                //        else {
                //            $('#REVW_TR_RoutingInfo').show();
                //            $('#REVW_SPN_ShowRoutingPath').text(splitdata[2]);
                //        }
                //    },
                //    error: function (xhr, err) {
                //        alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                //        alert("responseText: " + xhr.responseText);
                //    }
                //})




                displayReviewNotes(TID);

                ReviewDocsTable.destroy();

                ReviewDocsTable = $('#REWV_Docs').DataTable({
                    ajax: {
                        url: 'JQDataFetch/getCustomerOtherFiles.aspx',
                        async: false,
                        data: {
                            CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                            TID: TID,
                        }
                    }
                });

                ReviewAuditTable.destroy();

                ReviewAuditTable = $("#TBL_REWV_Audit").DataTable({
                    ajax: {
                        url: 'JQDataFetch/getTransAudit.aspx',
                        async: false,
                        data: {
                            TID: TID,
                        }
                    }
                });

                //alert(sTransID);
                ViewTransactionDialog.dialog('open');
        }

	    function ChangeTrasactionStatus(TID) {
	        // alert('status changing');
	        var value = $('#SM_div_sending').html().replace('$', '').replace(',', '');
	        var balance = $("[id$=DIV_BankAccountBalance]").html().replace('$', '').replace(',','');
	        var remTotalval = $("[id$=DIV_MonthlyRemittanceTotal]").html().replace('$', '').replace(',', '');
	        var totLastYrval = $("[id$=DIV_YearlyRemittanceTotal]").html().replace('$', '').replace(',', '');
	        var serCg = $("#SM_servicecharge").val().replace('$', '').replace(',', '');
	        $.ajax({
	            url: 'JQDataFetch/ChangeTrasactionStatus.aspx',
	            async: false,
	            data: {
	                tid: TID,
	                bal: balance,
	                monthTotal: remTotalval,
	                totalLastYr: totLastYrval,
	                val: value,
	                ser: serCg
	            },
	            success: function (response) {
	               
	                var arr = response.split('|');
	                $("[id$=DIV_BankAccountBalance]").html(arr[0]);
	                $("[id$=DIV_MonthlyRemittanceTotal]").html(arr[1]);
	                $("[id$=DIV_YearlyRemittanceTotal]").html(arr[2]);
	                var totArr = $("[id$=DIV_Totals]").html().split('/');
	                var tothtml = (Number(totArr[0]) + 1) + "/" + (Number(totArr[1]) + 1);
	                $("[id$=DIV_Totals]").html(tothtml);
                    $("[id$=ddl_List_Beneficiaries]").val('NOSELECT');
                    location.reload();
	            }
	        });
	    }
	    function LoadCreditDebitTable() {
	        $('#DebitCreditTbl').DataTable().destroy();
              $('#DebitCreditTbl').DataTable({
		            ajax: {
		                url: 'JQDataFetch/getCustomerDebitsCredits.aspx',
		                data: {
		                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                }
		            },
		            "order": [[0, "desc"]],
		            "columnDefs": [
                       { className: "align__left", "targets": [0, 1, 2] },
                       { className: "align__right", "targets": [3] },
                       
		            ],
		        });
	    }
		    $(document).ready(function () {

		        $('#expirydatetxt').mask('99/99/9999');
		        $('#overide_total').hide();
		        $('#TR_BankInformation').hide();
		        $('#TR_AccountInfo').hide();
		        $('#DIV_TRN_Add_Error_Funds').hide();
		        $('#DIV_TRN_Errors').hide();
		        $('#DIV_TRN_Add_Error_Purpose').hide();
		        $('#DIV_TRN_Add_Error_Source').hide();
		        $('#TR_PromoCode').hide();
		        $('#DIV_DocumentsOther').hide();
		        $('#DIV_EditCustomerNotesPW').hide();
                $('#TR_refunderrormsg').hide();


		        // Documents Tabs Functions
		        $('#TD_Documents_001').click(function () {
                    $('#DIV_DocumentsID').show();
                    $('#DIV_EditCustomerDetails').hide();
		            $('#DIV_DocumentsOther').hide();
		            $('#TD_Documents_001').removeClass();
		            $('#TD_Documents_001').addClass("wiz_tab_active");
		            $('#TD_Documents_002').removeClass();
		            $('#TD_Documents_002').addClass("wiz_tab_inactive_right");
		        })
		        $('#TD_Documents_002').click(function () {
		            $('#DIV_DocumentsID').hide();
		            $('#DIV_DocumentsOther').show();
		            $('#TD_Documents_002').removeClass();
		            $('#TD_Documents_002').addClass("wiz_tab_active");
		            $('#TD_Documents_001').removeClass();
		            $('#TD_Documents_001').addClass("wiz_tab_inactive_left");
		        })

		        // EditCustomer Tabs Functions
		        $('#TD_EditCustomer_001').click(function () {
		            $('#DIV_EditCustomerDetails').show();
		            $('#DIV_EditCustomerNotesPW').hide();
		            $('#TD_EditCustomer_001').removeClass();
		            $('#TD_EditCustomer_001').addClass("wiz_tab_active");
		            $('#TD_EditCustomer_002').removeClass();
		            $('#TD_EditCustomer_002').addClass("wiz_tab_inactive_right");
		        })
		        $('#TD_EditCustomer_002').click(function () {
		            $('#DIV_EditCustomerDetails').hide();
		            $('#DIV_EditCustomerNotesPW').show();
		            $('#TD_EditCustomer_001').removeClass();
		            $('#TD_EditCustomer_001').addClass("wiz_tab_inactive_left");
		            $('#TD_EditCustomer_002').removeClass();
		            $('#TD_EditCustomer_002').addClass("wiz_tab_active");
		        })
		        $('#TD_EditCustomer_003').click(function () {
		            //$('#DIV_EditCustomerDetails').hide();
		            //$('#DIV_EditCustomerNotesPW').show();
                    $('#TD_EditBusiness_001').removeClass();
                    $('#TD_EditBusiness_001').addClass("wiz_tab_inactive_right");
		            $('#TD_EditBusiness_002').removeClass();
		            $('#TD_EditBusiness_002').addClass("wiz_tab_inactive_left");
		            $('#TD_EditCustomer_003').removeClass();
		            $('#TD_EditCustomer_003').addClass("wiz_tab_active");
		        })
                $('#TD_EditBusiness_002').click(function () {
                    //$('#DIV_EditCustomerDetails').hide();
                    //$('#DIV_EditCustomerNotesPW').show();
                    $('#TD_EditBusiness_001').removeClass();
                    $('#TD_EditBusiness_001').addClass("wiz_tab_inactive_right");
                    $('#TD_EditCustomer_003').removeClass();
                    $('#TD_EditCustomer_003').addClass("wiz_tab_inactive_right");
                    $('#TD_EditBusiness_002').removeClass();
                    $('#TD_EditBusiness_002').addClass("wiz_tab_active");
                });
                $('#TD_EditBusiness_001').click(function () {
                    //$('#DIV_EditCustomerDetails').hide();
                    //$('#DIV_EditCustomerNotesPW').show();
                    $('#TD_EditBusiness_001').removeClass();
                    $('#TD_EditBusiness_001').addClass("wiz_tab_active");
                    $('#TD_EditCustomer_003').removeClass();
                    $('#TD_EditCustomer_003').addClass("wiz_tab_inactive_right");
                    $('#TD_EditBusiness_002').removeClass();
                    $('#TD_EditBusiness_002').addClass("wiz_tab_inactive_right");
                });

                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $("#REVW_ApprovePIN").hide();

                $("#Review_TD_001").click(function () {
                    $("#DIV_CompReview_Main").show();
                    $("#DIV_CompReview_Limits").hide();
                    $("#DIV_CompReview_Notes").hide();
                    $("#DIV_CompReview_Documents").hide();
                    $("#DIV_CompReview_Audit").hide();
                    $('#Review_TD_001').removeClass();
                    $('#Review_TD_001').addClass("wiz_tab_active");
                    $('#Review_TD_002').removeClass();
                    $('#Review_TD_002').addClass("wiz_tab_inactive_right");
                    $('#Review_TD_003').removeClass();
                    $('#Review_TD_003').addClass("wiz_tab_inactive_right");
                    $('#Review_TD_004').removeClass();
                    $('#Review_TD_004').addClass("wiz_tab_inactive_right");
                    $('#Review_TD_005').removeClass();
                    $('#Review_TD_005').addClass("wiz_tab_inactive_right");
                });

                // LIMITS
                $("#Review_TD_002").click(function () {
                    $("#DIV_CompReview_Main").hide();
                    $("#DIV_CompReview_Limits").show();
                    $("#DIV_CompReview_Notes").hide();
                    $("#DIV_CompReview_Documents").hide();
                    $("#DIV_CompReview_Audit").hide();
                    $('#Review_TD_001').removeClass();
                    $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_002').removeClass();
                    $('#Review_TD_002').addClass("wiz_tab_active");
                    $('#Review_TD_003').removeClass();
                    $('#Review_TD_003').addClass("wiz_tab_inactive_right");
                    $('#Review_TD_004').removeClass();
                    $('#Review_TD_004').addClass("wiz_tab_inactive_right");
                    $('#Review_TD_005').removeClass();
                    $('#Review_TD_005').addClass("wiz_tab_inactive_right");
                });

                // NOTES
                $("#Review_TD_003").click(function () {
                    $("#DIV_CompReview_Main").hide();
                    $("#DIV_CompReview_Limits").hide();
                    $("#DIV_CompReview_Notes").show();
                    $("#DIV_CompReview_Documents").hide();
                    $("#DIV_CompReview_Audit").hide();
                    $('#Review_TD_001').removeClass();
                    $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_002').removeClass();
                    $('#Review_TD_002').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_003').removeClass();
                    $('#Review_TD_003').addClass("wiz_tab_active");
                    $('#Review_TD_004').removeClass();
                    $('#Review_TD_004').addClass("wiz_tab_inactive_right");
                    $('#Review_TD_005').removeClass();
                    $('#Review_TD_005').addClass("wiz_tab_inactive_right");
                });

                // DOCUMENTS
                $("#Review_TD_004").click(function () {
                    $("#DIV_CompReview_Main").hide();
                    $("#DIV_CompReview_Limits").hide();
                    $("#DIV_CompReview_Notes").hide();
                    $("#DIV_CompReview_Audit").hide();
                    $("#DIV_CompReview_Documents").show();
                    $('#Review_TD_001').removeClass();
                    $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_002').removeClass();
                    $('#Review_TD_002').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_003').removeClass();
                    $('#Review_TD_003').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_004').removeClass();
                    $('#Review_TD_004').addClass("wiz_tab_active");
                    $('#Review_TD_005').removeClass();
                    $('#Review_TD_005').addClass("wiz_tab_inactive_right");
                });

                $('#Review_TD_005').click(function () {
                    $("#DIV_CompReview_Main").hide();
                    $("#DIV_CompReview_Limits").hide();
                    $("#DIV_CompReview_Notes").hide();
                    $("#DIV_CompReview_Documents").hide();
                    $("#DIV_CompReview_Audit").show();
                    $('#Review_TD_001').removeClass();
                    $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_002').removeClass();
                    $('#Review_TD_002').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_003').removeClass();
                    $('#Review_TD_003').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_004').removeClass();
                    $('#Review_TD_004').addClass("wiz_tab_inactive_left");
                    $('#Review_TD_005').removeClass();
                    $('#Review_TD_005').addClass("wiz_tab_active");
                });

                $('#CALC_BTN').click(function () {
                    CalculatorDIV.dialog('open');
                });

                $('#SEL_Currency').change(function () {
                    $.ajax({
                        url: 'JQDataFetch/getRateForCurrency.aspx',
                        data: {
                            CID: $('#SEL_Currency :selected').val(),
                        },
                        success: function (response) {
                            var splitres = response.split('|');
                            $('#txt_Rate').val(splitres[0]);
                            $('#SPN_CurrencyCode').text(splitres[1]);
                            $('#txt_FValue').val('');
                            $('#txt_AUDValue').val('');
                        }
                    })
                });

                $("#txt_AUDValue").keyup(function () {
                    var AUDVal = parseFloat($("#txt_AUDValue").val());
                    var rate = parseFloat($('#txt_Rate').val());
                    var sendRate = AUDVal * rate;
                    $('#txt_FValue').val(sendRate.toFixed(2));
                });

                $('#txt_FValue').keyup(function () {
                    var FValue = parseFloat($('#txt_FValue').val());
                    var rate = parseFloat($('#txt_Rate').val());
                    var sendRate = FValue / rate;
                    $('#txt_AUDValue').val(sendRate.toFixed(2));
                })

                ReviewDocsTable = $('#REWV_Docs').DataTable({
                    "columnDefs": [
                        //{ className: "ali_right", "targets": [0, 1] },
                        { "width": "145px", "targets": 1 }, // TXN REF
                    ],
                });

                ReviewAuditTable = $('#TBL_REWV_Audit').DataTable({

                })

                $("#btn_SaveNewNote").click(function () {
                    if ($('#TXT_Add_TRN_Notes').val() != "") {
                        $.ajax({
                            url: 'Processor/AddNewTransNote.aspx',
                            data: {
                                TID: $("#REVW_TransID").val(),
                                CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                                Notes: $('#TXT_Add_TRN_Notes').val(),
                            },
                            success: function (response) {
                                alert('Notes added successfully');
                                $("#TXT_Add_TRN_Notes").val('');
                                $('#DIV_Notes').empty();
                                displayReviewNotes($("#REVW_TransID").val());
                            }
                        })
                    }
                });

                $('#UploadFile3').change(function () {
                    var filename = $(this).val().split('\\').pop();
                    $('#otherdoctext3').text(filename);
                });

                $('#UploadFile3').fileupload({
                    url: 'DataHandlers/UploadOtherFiles.ashx?upload=start',
                    autoUpload: false,
                    replaceFileInput: false,
                    add: function (e, data) {
                        $('#btn_UploadOtherFiles3').off('click').on('click', function () {
                            if ($('#docdescription3').val() == "") {
                                alert('Please provide a description to upload this file');
                            }
                            else {
                                data.submit();
                            }

                        });
                    },
                    success: function (response, status) {
                        alert('Upload successful');
                        //showsuccess('', 'File Uploaded Successfully');
                        //openMsgDIV('None', 'File has been successfully uploaded', '2');
                        ReviewDocsTable.ajax.reload(null, false);
                        $('#docdescription').val('');
                    }
                });

                $('#btn_refundcomplete').click(function () {

                    if ($('#txt_RefundAmount').val() == "" || parseFloat($('#txt_RefundAmount').val()) < 0)
                    {
                        $('#TR_refunderrormsg').show();
                        $('#refunderrormsg').text('Please enter a valid amount to proceed.');
                    }
                    else
                    {
                        if ($('#txt_RefundDescription :selected').val() == "")
                        {
                            $('#TR_refunderrormsg').show();
                            $('#refunderrormsg').text('Please select a deposit method to proceed.');
                        }
                        else
                        {
                            if (parseFloat($('#<%=hidden_txt_accountbalance.ClientID%>').val()) < parseFloat($('#txt_RefundAmount').val()))
                            {
                                $('#TR_refunderrormsg').show();
                                $('#refunderrormsg').text('Refund amount cannot be greater than the funds available');
                            }
                            else
                            {
                               
                                    $.ajax({
                                        url: 'Processor/issueRefund.aspx',
                                        async: false,
                                        data: {
                                            CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                                        Amt: $('#txt_RefundAmount').val(),
                                        Method: $('#txt_RefundDescription :selected').val(),
                                        },
                                        success: function () {
                                            alert('Refund has been successfully processed');
                                            location.reload();
                                        }
                                    })

                            }
                            
                        }
                        
                    }
                    
                });


		        var todayrate = 0;

		        $('#UploadFile2').fileupload({
		            url: 'DataHandlers/UploadOtherFiles.ashx?upload=start',
		            autoUpload: false,
		            replaceFileInput: false,
		            add: function(e, data) {
		                $('#btn_UploadOtherFiles').off('click').on('click', function () {
		                    if ($('#docdescription').val() == "")
		                    {
		                        showerror('Please provide a description to upload this file');
		                    }
		                    else
		                    {
		                        data.submit();
		                    }
						
		                });
		            },
		            success: function(response,status) {
		                showsuccess('','File Uploaded Successfully');
		                //openMsgDIV('None', 'File has been successfully uploaded', '2');
		                FileOtherTable.ajax.reload(null, false);
		                $('#docdescription').val('');
		            }
		        });

		        $('#UploadFile2').bind('fileuploadsubmit', function(e, data) {
		            data.formData = {
		                CustID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                DocDesc: $('#docdescription').val(),
		            };
		            if (!data.formData.CustID) {
		                return false;
		            }
		        });

		        $('#UploadFile1').fileupload({
		            url: 'DataHandlers/FileUploadHandler.ashx?upload=start',
		            autoUpload: false,
		            replaceFileInput:false,
		            add: function(e, data) {
		                $('#btn_UploadFile').off('click').on('click', function () {

		                    var ccc = 1024 * 1024 * 4;

		                    if ((document.getElementById("UploadFile1").files[0].size) > ccc) {
		                        alert("Maximum file size is 4MB.");
		                        return false;
		                    }

		                    if ($('#documentnumbertext').val() != "" && $('#issuingauthoritytxt').val() != "" && $('#expirydatetxt').val() != "" && $('#typeofdocumentddl :selected').val() != "")
		                    {
		                        $('#documentnumbertxt').removeClass('err_redborder');
		                        $('#issuingauthoritytxt').removeClass('err_redborder');
		                        $('#expirydatetxt').removeClass('err_redborder');
                                $('#typeofdocumentddl').removeClass('err_redborder');
                                document.getElementById("divProgress").style.display = "block";
                                document.getElementById("divUpload").style.display = "block";
                                id = setInterval("progress()", 20);
		                        data.submit();
		                    }
		                    else
		                    {
		                        if ($('#documentnumbertxt').val() == "") {
		                            $('#documentnumbertxt').addClass('err_redborder');
		                        }
		                        else
		                        {
		                            $('#documentnumbertxt').removeClass('err_redborder');
		                        }

		                        if ($('#issuingauthoritytxt').val() == "") {
		                            $('#issuingauthoritytxt').addClass('err_redborder');
		                        }
		                        else
		                        {
		                            $('#issuingauthoritytxt').removeClass('err_redborder');
		                        }

		                        if($('#expirydatetxt').val() == "") {
		                            $('#expirydatetxt').addClass('err_redborder');
		                        }
		                        else
		                        {
		                            $('#expirydatetxt').removeClass('err_redborder');
		                        }

		                        if($('#typeofdocumentddl :selected').val() == "")
		                        {
		                            $('#typeofdocumentddl').addClass('err_redborder');
		                        }
		                        else
		                        {
		                            $('#typeofdocumentddl').removeClass('err_redborder');
		                        }
		                        showerror('Please make sure that you have entered all the required information for the document.')
		                    }
						
		                });
					
		            },
                    success: function (reponse, status) {
                        document.getElementById("divProgress").style.display = "none";
                        document.getElementById("divUpload").style.display = "none";
		                showsuccess('','File Uploaded Successfully');
		                FileMainTable.ajax.reload(null, false);
		                //openMsgDIV('None', 'File Uploaded Successfully', '2');
		                highlightRiskLevel();   
		                updateIDPicture();
		                $('#typeofdocumentddl').find('option:first').attr('selected', 'selected');
		                $('#UploadFile1').val('');
		                $('#selectedfilename').text('');
		                $('#documentnumbertxt').val('');
		                $('#issuingauthoritytxt').val('');
		                $('#expirydatetxt').val('');
		                $('#ContentMainSection_ErrorMessageArea').hide();
		            },
		            error: function(error) {
		                showerror(error);
		            },
		        });

		        $('#UploadFile1').bind('fileuploadsubmit', function(e, data) {
		            data.formData = {
		                CustID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                DocNum: $('#documentnumbertxt').val(),
		                IssueAuth: $('#issuingauthoritytxt').val(),
		                ExpDate: $('#expirydatetxt').val(),
		                HDNPts: $('#hdnpoints').val(),
		                TypeOfDoc: $('#typeofdocumentddl :selected').text(),
		                TODID: $('#typeofdocumentddl').val(),
		            };
		            if (!data.formData.CustID) {
		                return false;
		            }
		        });

		        $('#txt_AdminCode').focusout(function(){
		            $.ajax({
		                url: 'JQDataFetch/verifyAdminOverrideCode.aspx',
		                data: {
		                    UID: $('#hidden_LoggedUserID').val(),
		                    ORCode: $('#txt_AdminCode').val(),
		                },
		                success: function(response) {
		                    if (response == 'Y')
		                    {
		                        showsuccess('','Admin Override Confirmed');
		                        $('#SM_btn_Confirm').attr("disabled", false);
		                        $('#hidden_goingStatus').val('PENDING');
		                        $('#hidden_adminoverride').val('Y');
		                    }
		                    else
		                    {
		                        showerror('Incorrect Admin Override. Try again or escalate to compliance team.')
		                        $('#hidden_goingStatus').val('REVIEW');
		                    }
		                }
		            })
		        });

		        $.ajax({
		            url: 'JQDataFetch/gettodayrate.aspx',
		            success: function(data) {
		                $('#div_todayrate').text(data);
		                todayrate = data;
		                $('#HD_div_todayrate').text(data);
		                $('#OTC_div_todayrate').text(data);
		                $('#SM_div_todayrate').text(data);
		                $('#<%=txt_hidden_Rate.ClientID%>').val(data);
		            }
		        });

		        $('#SM_exchangerate').change(function() {
		            var newtotal = $('#SM_amounttobesent').val() * $('#SM_exchangerate').val();
		            $('#SM_div_lkrvalue').text(parseFloat(newtotal).toFixed(2));
		        });

		        $.ajax({
		            url: 'JQDataFetch/getAgentCurrencies.aspx',
		            async: false,
		            data: {
		                AID: $('#<%=hidden_AgentID.ClientID%>').val(),
		            },
		            success: function(data) {
		                var Currencies = data.split('~');
		                Currencies.sort();
		                $.each(Currencies, function(item) {
		                    var datasplit = Currencies[item].split('|');

                            $('#Add_Account_Type').append('<option value="' + datasplit[1] + '">' + datasplit[0] + ' - ' + datasplit[1] + '</option>');
                            $('#SEL_Currency').append('<option value="' + datasplit[2] + '">' + datasplit[0] + ' - ' + datasplit[1] + '</option>');
		                })
		            }
		        })

		        $('#EditBene_CountryDDL').change(function() {
		            $('#EditBene_stateprovince').empty();
		            $('#EditBene_suburbdistrict').empty();
		            $.ajax({
		                url: 'JQDataFetch/getCountryProvinces.aspx',
		                async: false,
		                data: {
		                    CID: $('#EditBene_CountryDDL').val(),
		                },
		                success: function(data) {
		                    var firstdata = data.split('~');
		                    firstdata.sort();
		                    $('#EditBene_stateprovince').append('<option value="">-- SELECT --</option>');
		                    $.each(firstdata, function(item) {
		                        var datasplit = firstdata[item].split('|');
		                        $('#EditBene_stateprovince').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            })
		        });

		        $('#EditBene_stateprovince').change(function() {
		            $('#EditBene_suburbdistrict').empty();
		            $.ajax({
		                url: 'JQDataFetch/getProvinceDistricts.aspx',
		                async: false,
		                data: {
		                    CID: $('#EditBene_stateprovince').val(),
		                },
		                success: function(data) {
		                    var firstdata = data.split('~');
		                    firstdata.sort();
		                    $('#EditBene_suburbdistrict').append('<option value="">-- SELECT --</option>');
		                    $.each(firstdata, function(item){
		                        var datasplit = firstdata[item].split('|');
		                        $('#EditBene_suburbdistrict').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            })
		        })

		        $('#ddl_Banks').change(function() {
		            $.ajax({
		                url: 'JQDataFetch/getAllBankBranches.aspx',
		                data: {
		                    BID: $('#ddl_Banks').val(),
		                },
		                success: function(data) {
		                    var Branches = data.split('~');
		                    Branches.sort();
		                    $('#Edit_ddl_Branches').empty();
		                    $.each(Branches, function(item) {
		                        var datasplit = Branches[item].split('|');
						   
		                        $('#Edit_ddl_Branches').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            });
		        });

		        <%--			$('#<%=dob.ClientID%>').mask('00/00/0000');
			$('#<%=state.ClientID%>').mask('AAA');
			$('#<%=postcode.ClientID%>').mask('0000');
			$('#<%=phonehome.ClientID%>').mask('(00) 0000 0000');
			$('#<%=phonework.ClientID%>').mask('(00) 0000 0000');
			$('#<%=phonemobile.ClientID%>').mask('0000 000 000');--%>

		        $('#edit_bene_homephone').mask('000 0 000000');
		        $('#edit_bene_workphone').mask('000 0 000000');
		        $('#edit_bene_mobile').mask('000 0000000');

		        $('#DIV_BeneficiaryDetails').hide();
		        $('#tr_ben_info').hide();
		        $('#tblIndi').hide();

		        LoaderWait = $('#LoadingDiv').dialog({
		            autoOpen: false,
		            modal: true,
		            width: 710,
                });

                CalculatorDIV = $('#DIV_Calculator').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 710,
                })

                AddNewBankAccount = $('#DIV_AddNewBankAccount').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 640,
                    title: 'Bank Account Details',
                })

                DuplicateTransDialog = $('#DIV_DuplicateTransaction').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 640,
                    title: 'Duplicate Transcation Warning',
                })

                AddNewBankAccountCountrySelector = $('#DIV_AddNewBankAccountCountrySelector').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 480,
                    title: 'Select Country',
                })

                RefundDialog = $('#RefundDIV').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 480,
                    title: 'Refund',
                })

                SendCustDialog = $('#DIV_SendCustEmail').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 640,
                    title: 'Send Bank Details',
                });

                ViewTransactionDialog = $("#ViewTransactionDIV").dialog({
                    modal: true,
                    autoOpen: false,
                    width: 640,
                    title: 'View Transaction Details',
                });

		        MsgDIV = $('#DIV_Message_OK').dialog({
		            autoOpen: false,
		            modal: true,
		            width: 320,
		        });

		        MsgDIV.siblings('.ui-dialog-titlebar').remove();

		        DocumentsDialog = $('#DIV_Documents').dialog({
		            autoOpen: false,
		            modal: true,
		            width: 640,
		            title: 'Documents',
                });

                $('#btn_Refund').click(function () {
                    var availableBalance = $('#<%=DIV_BankAccountBalance.ClientID%>').text().replace('$', '');
                    if (parseFloat(availableBalance) > 0)
                    {
                        RefundDialog.dialog('open');
                    }
                    else
                    {
                        alert('Insufficient Funds in Account to issue refund');
                    }
                    
                })

		        $('#<%=ddl_List_Beneficiaries.ClientID%>').change(function() {
				
		            if ($('#<%=ddl_List_Beneficiaries.ClientID%> option:selected').val() == 'NOSELECT')
		            {
		                $('#tr_ben_info').hide();
		            }
		            else
		            {
		                populateBeneficaryDetails($('#<%=ddl_List_Beneficiaries.ClientID%>').val());
		            }
				
		        });
		        LoadTransactionTable();
		        $('#SM_amounttobesent').change(function() {
		            $('#hidden_sendamountoverride').val('Y');
		            var amtServiceCharge = calculateServiceCharge($('#SM_amounttobesent').val());
		            var checkTotal = parseFloat($('#SM_amounttobesent').val()) + parseFloat(amtServiceCharge);
		            if (parseFloat(checkTotal) > parseFloat($('#SPN_AddTR_AvailableFunds').text().substring(1)))
		            {
		                $('#DIV_TRN_Add_Error_Funds').show();
		                $('#DIV_TRN_Errors').show();
		                $('#hidden_enoughfunds').val('N');
		            }
		            else
		            {
		                $('#DIV_TRN_Add_Error_Funds').hide();
		                $('#DIV_TRN_Errors').hide();
		                $('#SM_amounttobesent').val(parseFloat($('#SM_amounttobesent').val()).toFixed(2));
		                var amtServiceCharge = calculateServiceCharge($('#SM_amounttobesent').val());
		                $('#SM_servicecharge').val(parseFloat(amtServiceCharge).toFixed(2)); 

		                var totalSend = calculateSendingTotal($('#SM_amounttobesent').val(), $('#SM_exchangerate').val());
		                $('#SM_div_lkrvalue').text(parseFloat(totalSend).toFixed(2));
		                $('#txt_hidden_foreignamount').val(parseFloat(totalSend).toFixed(2));

				  
		                $('#SM_div_sending').text("$ " + parseFloat(checkTotal).toFixed(2));
		                $('#hidden_enoughfunds').val('Y');
		            }

				
		        });

		        $('#BTN_PromoCode').click(function() {
		            var OpenedVal = $('#hidden_openedpromo').val();
		            if (OpenedVal == "")
		            {
		                $('#TR_PromoCode').show();
		                $('#SM_PromoCode').val('');
		                $('#hidden_openedpromo').val("Y");
		            }
		            else
		            {
		                $('#TR_PromoCode').hide();
		                $('#hidden_openedpromo').val("");
		            }
				
		        });


		        //populateBeneficaryDetails($('#<%=txt_hidden_FirstBeneficiaryID.ClientID%>').val());

                $('#<%=Image23.ClientID%>').click(function () {
		            $.ajax({
		                url: 'Processor/sendBankDetailsToCustomer.aspx',
		                async: false,
		                beforeSend: function () {
		                    LoaderWait.dialog('open')
		                },
		                complete: function () {
		                    LoaderWait.dialog('close')
		                },
		                data: {
		                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                    CMessage: $('#EMAIL_CustomMessage').val(),
		                    CC: $('#emailcc').val(),
		                },
                    
		                success: function (result) {
		                    alert(result);
		                }
		            })
		        })

		        $('#<%=btn_SendBankDetails.ClientID%>').click(function () {

		            $.ajax({
		                url: 'JQDataFetch/checkCustomerHasEmailAddress.aspx',
		                async: false,
		                data: {
		                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                },
		                success: function (response)
		                {
		                    if (response == "HasEmail")
		                    {
		                        $("#EMAIL_FirstLine").text('Hello, ' + $('#<%=hidden_CustomerFullName.ClientID%>').val());
		                        $.ajax({
		                            url: 'JQDataFetch/getAgentAssignedLocalBankDetails.aspx',
		                            async: false,
		                            success: function (response) {
		                                var data = response.split('|');
		                                $.each(data, function (item) {
		                                    $('#EMAIL_BankDetails').html(data[0] + "</br>");
		                                });
		                            }
		                        });


		                        $.ajax({
		                            url: 'JQDataFetch/getAgentDetailsForEmail.aspx',
		                            async: false,
		                            data: {
		                                CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                            },
		                            success: function (response) {
		                                var data = response.split('|');
		                                $('#EMAIL_SendingAgentName').text(data[0]);
		                                $('#EMAIL_SendingAgentAddress').text(data[1]);
		                                $('#EMAIL_SendingAgentPhone').text(data[2]);
		                                $('#EMAIL_Subject').text(data[0] + " Bank Account Details");
		                                $('#EMAIL_addy').text(data[3]);
		                            }
		                        })
		                        SendCustDialog.dialog('open');
		                    }
		                    else
		                    {
		                        alert('This customer does not have any email address. Please enter a valid email in the customer profile.');
		                    }
		                }
		            })


		       
		        });

		        $('#SM_btn_Send').click(function() {

		            $('#DIV_TRN_Errors').hide();
		            $('#DIV_TRN_Add_Error_Purpose').hide();
		            $('#DIV_TRN_Add_Error_Source').hide();
		            $("#DIV_TRN_Add_Error_Funds").hide();

				

		            if ($('#SM_Purpose :selected').val() != "NOSELECT" && $('#SM_SourceOfFunds :selected').val() != "NOSELECT" && $('#hidden_enoughfunds').val() == "Y")
                    {
                        var AUDMLimit = $('#<%=txt_AUDMLimit.ClientID%>').val();
                        var AUDYLimit = $('#<%=txt_AUDYLimit.ClientID%>').val();
                        var NbMLimit = $('#<%=txt_NbMLimit.ClientID%>').val();
                        var NbYLimit = $('#<%=txt_NbYLimit.ClientID%>').val();

                        //alert(AUDMLimit + "|" + AUDYLimit + "|" + NbMLimit + "|" + NbYLimit);

                        var AUDMCurrentSent = $("#<%=DIV_MonthlyRemittanceTotal.ClientID%>").text().replace('$', '').replace(',', '');
                        var AUDYCurrentSent = $("#<%=DIV_YearlyRemittanceTotal.ClientID%>").text().replace('$', '').replace(',', '');
                        var AllNbSent = $("#<%=DIV_Totals.ClientID%>").text().split('/');
                        var NbMSent = AllNbSent[0];
                        var NbYSent = AllNbSent[1];

                        //alert(AUDMCurrentSent + "|" + AUDYCurrentSent + "|" + AllNbSent + "|" + NbMSent + "|" + NbYSent)

                        var finalAUDMTotal = parseFloat($('#SM_amounttobesent').val()) + parseFloat(AUDMCurrentSent);
                        var finalAUDYTotal = parseFloat($('#SM_amounttobesent').val()) + parseFloat(AUDYCurrentSent);
                        var finalNbMTotal = parseFloat(NbMSent) + 1;
                        var finalNbYTotal = parseFloat(NbYSent) + 1;

                        //alert(finalAUDMTotal + "|" + finalAUDYTotal + "|" + finalNbMTotal + "|" + finalNbYTotal);

		                var SPower = parseFloat($('#<%=hidden_SpendingPower.ClientID%>').val());
		                var AlreadySentThisMonth = parseFloat($("#<%=DIV_MonthlyRemittanceTotal.ClientID%>").text().replace('$', ''));
		                var CalcTotalForThisMonth = AlreadySentThisMonth + parseFloat($('#SM_amounttobesent').val());
		                $("#hidden_goingStatus").val('PENDING');
                    

                        if (finalAUDMTotal >= parseFloat(AUDMLimit) || finalAUDYTotal >= parseFloat(AUDYLimit) || finalNbMTotal >= parseFloat(NbMLimit) || finalNbYTotal >= parseFloat(NbYLimit))
		                {
		                    $("#TR_SM_ExceedSend").show();
		                    $("#hidden_goingStatus").val('REVIEW');
		                }

		                $('#SM_btn_Confirm').show();
		                $('#SM_btn_Cancel').show();
		                $('#SM_btn_Send').hide();
		                $('#SM_amounttobesent').attr("readonly", true);
		                $('#SM_servicecharge').attr("readonly", true); 

		                var availfunds = parseFloat($('#<%=hidden_txt_accountbalance.ClientID%>').val());
		                var sendfunds = parseFloat($('#SM_amounttobesent').val());
		                var sendcharges = parseFloat($('#SM_servicecharge').val());
				
		                var hassent = $('#<%=hidden_CurrentSentAmount.ClientID%>').val();
		                var totalsending = parseFloat(hassent) + parseFloat(sendfunds);
		                var kycdaysleft = $('#<%=hidden_KYCExpiryDays.ClientID%>').val();

		                var totalmthtrans = $('#<%=hidden_NbTrans.ClientID%>').val();
		                var thisTransSend = parseInt(totalmthtrans) + 1;

		                var hKYC = $('#<%=hidden_hasKYC.ClientID%>').val();
		                var RiskLevelTest = $('#<%=hidden_risklevel.ClientID%>').val();

		                var totalsend = sendfunds + sendcharges;

		            }
		            else
		            {
					
		                if ($('#SM_Purpose :selected').val() == "NOSELECT")
		                {
		                    $('#DIV_TRN_Errors').show();
		                    $('#DIV_TRN_Add_Error_Purpose').show();
		                }

		                if ($('#SM_SourceOfFunds :selected').val() == "NOSELECT")
		                {
		                    $('#DIV_TRN_Errors').show();
		                    $('#DIV_TRN_Add_Error_Source').show();
		                }

		                if ($('#hidden_enoughfunds').val() == "N")
		                {
		                    $('#DIV_TRN_Errors').show();
		                    $("#DIV_TRN_Add_Error_Funds").show();
		                }
		            }
   
		        });

		        $('#SM_btn_override').click(function() {
		            $('#SM_servicecharge').attr("readonly", false);
		        });

		        $('#SM_btn_overrideexchangerate').click(function() {
		            $("#SM_exchangerate").attr("readonly", false);
		        });


		        $('#UploadFile1').change(function() {
		            var filename = $(this).val().split('\\').pop();
		            $('#selectedfilename').text(filename);
		        });

		        $('#UploadFile2').change(function() {
		            var filename = $(this).val().split('\\').pop();
		            $('#otherdoctext').text(filename);
		        });

                $('#SM_btn_Confirm').click(function () {
                    $.ajax({
                        url: 'JQDataFetch/checkDuplicateTransaction.aspx',
                        async: false,
                        data: {
                            CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                            BID: $('#<%=hidden_selectedBeneficiary.ClientID%>').val(),
                            AID: $('#<%=txt_hidden_AccountID.ClientID%>').val(),
                            AMT: $('#SM_amounttobesent').val(),
                        },
                        success: function(response) {
                            if (response == "YES")
                            {
                                DuplicateTransDialog.dialog('open');
                            }
                            else
                            {
                                submitTransaction();
                            }
                        }
                    })
                });

                $('#btn_DuplicateNO').click(function () {
                    DuplicateTransDialog.dialog('close');
                });

                $("#btn_DuplicateYES").click(function () {
                    DuplicateTransDialog.dialog('close');
                    submitTransaction();
                })

		        $('#SM_btn_Cancel').click(function() {
		            $('#TR_SM_NegativeSend').hide();
		            $('#SM_btn_Confirm').hide();
		            $('#SM_btn_Cancel').hide();
		            $('#SM_btn_Send').show();
		            $('#SM_amounttobesent').attr("readonly", false);
                    $('#SM_servicecharge').attr("readonly", false);
                    ShowSendMoney.dialog('close');
		        });

		        $('#SM_btn_overidetotal').click(function() {
		            $('#overide_total').show();
		            $('#overide_total').val($('#SM_div_lkrvalue').text());
		        });

		   
		    

		        var FileMainTable = $('#FileUploadMainTbl').DataTable({
		            ajax: {
		                url: 'JQDataFetch/getCustomerMainFiles.aspx',
		                data: {
		                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                }
		            }
		        });

		        var FileOtherTable = $('#FileUploadOtherTbl').DataTable({
		            ajax: {
		                url: 'JQDataFetch/getCustomerOtherFiles.aspx',
		                data: {
		                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                }
		            }
		        });

		        LoadCreditDebitTable();

		        EditCustomerDialog = $('#EditCustomerDiv').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		            title: "Edit Customer",
		        });

		        AddBeneficiaryDialog = $('#AddBeneficiaryDiv').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		            title: "Add New Beneficiary",
		        });

		        EditBeneficiaryDialog = $('#EditBeneficiaryDiv').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		            title: 'Edit Beneficiary',
		        });

		        AddCreditDialog = $('#AddCreditDiv').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 480,
		            title: 'Add Credit',
		        })

		        ShowDebitCreditDialog = $('#ShowCreditDebitsDIV').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 680,
		            title: 'VIEW ACCOUNT',
		        });

		        ShowOverTheCounter = $('#OverTheCounter').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		            title: 'Over The Counter Transaction',
		        });
			

		        ShowEditAccount = $('#EditAccount').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 680,
		            title: 'EDIT BENEFICIARY ACCOUNT',
		        });

		        ShowAddNewAccount = $("#AddNewAccount").dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		            title: 'ADD BENEFICIARY ACCOUNT',
		        });

		        ShowHomeDelivery = $('#HomeDelivery').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		            title: 'Home Delivery',
		        });

		        ShowCompanyVerified = $('#DIV_Modal_CompanyVerified').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		        });

		        ShowCompanyVerified.siblings('.ui-dialog-titlebar').remove();

		        $('#OpenModalCVerified').click(function() {
		            ShowCompanyVerified.dialog('open');
		        });

		        ShowSendMoney = $('#SendMoneyDIV').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 480,
		            title: 'Transfer Money',
		        });

		        ShowKYC = $('#DIV_Modal_KYC').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 640,
		            title: 'Know Your Customer (KYC) Additional Information',
		        });

		        LoadingDIV = $('#DIV_Loading').dialog({
		            modal: true,
		            autoOpen: false,
		            width: 120,
		            dialogClass: 'dialog_transparent_background',
		        });

		        LoadingDIV.siblings('.ui-dialog-titlebar').remove();

		        $('#btn_AddKYC').click(function() {
			   
		        });

		        $('#AddBene_CountryDDL').change(function() {
		            $('#AddBene_stateprovince').empty();
		            $('#AddBene_suburbdistrict').empty();
		            $.ajax({
		                url: 'JQDataFetch/getCountryProvinces.aspx',
		                data: {
		                    CID: $('#AddBene_CountryDDL').val(),
		                },
		                success: function(data) {
		                    var firstdata = data.split('~');
		                    firstdata.sort();
		                    $('#AddBene_stateprovince').append('<option value="">-- SELECT --</option>');
		                    $.each(firstdata, function(item) {
		                        var datasplit = firstdata[item].split('|');
		                        $('#AddBene_stateprovince').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            })
		        });

		        $('#AddBene_stateprovince').change(function() {
		            $('#AddBene_suburbdistrict').empty();
		            $.ajax({
		                url: 'JQDataFetch/getProvinceDistricts.aspx',
		                data: {
		                    CID: $('#AddBene_stateprovince').val(),
		                },
		                success: function(data) {
		                    var firstdata = data.split('~');
		                    firstdata.sort();
		                    $('#AddBene_suburbdistrict').append('<option value="">-- SELECT --</option>');
		                    $.each(firstdata, function(item){
		                        var datasplit = firstdata[item].split('|');
		                        $('#AddBene_suburbdistrict').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            })
		        });

		        $('#BTN_DocumentsClick').click(function() {

		            $('#documentnumbertxt').val('');
		            $('#issuingauthoritytxt').val('');
		            $('#expirydatetxt').val('');
		            $('#hdnpoints').val('');


		            $.ajax({
		                url: 'JQDataFetch/getDocumentTypes.aspx',
		                async: false,
		                success: function(data) {
		                    $('#typeofdocumentddl').empty();
		                    $('#typeofdocumentddl').append('<option value="">-- SELECT -- </option>');
		                    var Docs = data.split('~');
		                    Docs.sort();
		                    $.each(Docs, function (item) {
		                        var datasplit = Docs[item].split('|');
		                        $('#typeofdocumentddl').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            });
		            DocumentsDialog.dialog('open');
		        });

		        $('#typeofdocumentddl').change(function () {
		            $.ajax ({
		                url: 'JQDataFetch/getDocumentPoints.aspx',
		                data: {
		                    DID: $('#typeofdocumentddl').val(),
		                },
		                success: function(response) {
		                    $('#hdnpoints').val(response);
		                }
		            });
		        });

		        $('#btn_kyc_add_Submit').click(function() {
		            $.ajax({
		                url: 'Processor/performKYCUpdate.aspx',
		                beforeSend: function() {
		                    ShowKYC.dialog('close');
		                    LoadingDIV.dialog('open');
		                },
		                data: {
		                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                    MS: $('#maritalstatus').val(),
		                    FD: $('#findep').val(),
		                    AURes: $('#AURes').val(),
		                    RS: $('#ResStat').val(),
		                    ES: $('#EmpStat').val(),
		                    OC: $('#occupation').val(),
		                    SL: $('#salary').val(),
		                    FR: $('#frequency').val(),
		                    OI: $('#otherincome').val(),
		                    CM: $('#comments').val(),
		                },
		                complete: function() {
		                    LoadingDIV.dialog('close');
		                },
		                success: function(response) {
		                    showsuccess('','KYC Updated successfully');
		                    $('#<%=hidden_hasKYC.ClientID%>').val('true');
		                    $('#<%=hidden_KYCExpiryDays.ClientID%>').val('365');
		                    highlightRiskLevel();
		                    ShowKYC.dialog('close');
		                }
		            });
		        });

		        $('#btn_HomeDelivery').click(function() {
		            ShowHomeDelivery.dialog('open');
		            var amountToBeSent = calculateAmountToBeSent($('#<%=hidden_txt_accountbalance.ClientID%>').val());
		            $('#HD_availablefunds').val($('#<%=hidden_txt_accountbalance.ClientID%>').val());
		            $('#HD_amounttobesent').val(amountToBeSent);
		            var ServiceCharge = calculateServiceCharge(amountToBeSent);
		            $('#HD_servicecharge').val(ServiceCharge);
		            $('#HD_div_sending').text(amountToBeSent);

		            var totalsend = amountToBeSent * todayrate;
		            $('#HD_div_lkrvalue').text(totalsend);
		        })

                $('#btn_AddAccount').click(function () {
                    $('#DDL_Banks_CountryList').empty();
                    $.ajax({
                        url: 'JQDataFetch/getAgentAssignedCountries.aspx',
                        async: false,
                        data: {
                            CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                        },
                        success: function (data) {
                            var Countries = data.split('~');
                            Countries.sort();
                            $('#DDL_Banks_CountryList').append('<option value="">-- SELECT --</option>');
                            $.each(Countries, function (item) {
                                var datasplit = Countries[item].split('|');
                                $('#DDL_Banks_CountryList').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                            });
                        }
                    })
                    //$('#DIV_AddFrameLoader').load('InternationalBanking/AddSriLankaBank.aspx');
                    AddNewBankAccountCountrySelector.dialog('open');
		        });

                $('#BTN_Go').click(function () {
                    if ($('#DDL_Banks_CountryList').val() == 2) {
                        $('#DIV_AddFrameLoader').load('InternationalBanking/AddSriLankaBank.aspx?BID=' + $('#<%=ddl_List_Beneficiaries.ClientID%>').val() + '&CID=' + $('#<%=txt_hidden_CustomerID.ClientID%>').val());
                        AddNewBankAccountCountrySelector.dialog('close');
                        AddNewBankAccount.dialog('open');
                        $('#DIV_AddNewBankAccount').parent().appendTo($("form:first"));
                    }
                    else if ($('#DDL_Banks_CountryList').val() == 8) {
                        $('#DIV_AddFrameLoader').load('InternationalBanking/AddPhilippineBank.aspx?BID=' + $('#<%=ddl_List_Beneficiaries.ClientID%>').val() + '&CID=' + $('#<%=txt_hidden_CustomerID.ClientID%>').val());
                        AddNewBankAccountCountrySelector.dialog('close');
                        AddNewBankAccount.dialog('open');
                        $('#DIV_AddNewBankAccount').parent().appendTo($("form:first"));
                    }
                    else if ($('#DDL_Banks_CountryList').val() == 4) {
                        $('#DIV_AddFrameLoader').load('InternationalBanking/AddIndonesiaBank.aspx?BID=' + $('#<%=ddl_List_Beneficiaries.ClientID%>').val() + '&CID=' + $('#<%=txt_hidden_CustomerID.ClientID%>').val());
                        AddNewBankAccountCountrySelector.dialog('close');
                        AddNewBankAccount.dialog('open');
                        $('#DIV_AddNewBankAccount').parent().appendTo($("form:first"));
                    }
                    
		        });

		        $('#Add_ddl_Banks').change(function() {
		            $.ajax({
		                url: 'JQDataFetch/getAllBankBranches.aspx',
		                data: {
		                    BID: $('#Add_ddl_Banks').val(),
		                },
		                success: function(data) {
		                    var Branches = data.split('~');
		                    Branches.sort();
		                    $('#Add_ddl_Branches').empty();
		                    $('#Add_ddl_Branches').append('<option value="">-- SELECT --</option>');
		                    $.each(Branches, function(item) {
		                        var datasplit = Branches[item].split('|');
						   
		                        $('#Add_ddl_Branches').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            });
		        });

		        $('#Add_ddl_Banks_India').change(function() {
		            $.ajax({
		                url: 'JQDataFetch/getAllBankBranches.aspx',
		                data: {
		                    BID: $('#Add_ddl_Banks_India').val(),
		                },
		                success: function(data) {
		                    var Branches = data.split('~');
		                    Branches.sort();
		                    $('#Add_ddl_Branches_India').empty();
		                    $('#Add_ddl_Branches_India').append('<option value="">-- SELECT --</option>');
		                    $.each(Branches, function(item) {
		                        var datasplit = Branches[item].split('|');
						   
		                        $('#Add_ddl_Branches_India').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            });
		        });

		        $("#Add_ddl_Branches_India").change(function() {
		            if ($('#Add_ddl_Branches_India :selected').val() != "")
		            {
		                $('#DIV_BankCode').text($('#Add_ddl_Banks_India :selected').val());
		                $('#DIV_BranchCode').text($('#Add_ddl_Branches_India :selected').val());
		                $.ajax({
		                    url: 'JQDataFetch/getBankBranchGeneralDetails.aspx',
		                    async: false,
		                    data: {
		                        BankID: $('#Add_ddl_Banks_India :selected').val(),
		                        BranchID: $('#Add_ddl_Branches_India :selected').val(),
		                    },
		                    success: function(data)
		                    {
		                        $('#DIV_BranchAddress').text(data);
		                    }

		                })
		            }
		        });

		        $('#Add_ddl_Branches').change(function() {
		            $('#DIV_BankCode').text($('#Add_ddl_Banks :selected').val());
		            $('#DIV_BranchCode').text($('#Add_ddl_Branches :selected').val());
		            $.ajax({
		                url: 'JQDataFetch/getBankBranchGeneralDetails.aspx',
		                async: false,
		                data: {
		                    BankID: $('#Add_ddl_Banks :selected').val(),
		                    BranchID: $('#Add_ddl_Branches :selected').val(),
		                },
		                success: function(data)
		                {
		                    $('#DIV_BranchAddress').text(data);
		                }

		            })
		        });

		        $('#btn_editnewaccountcomplete').click(function() {
		            var formEditValid=$("#editaccountform").valid();
		            if(formEditValid)
		            {
		                //alert($('#<%=txt_hidden_AccountID.ClientID%>').val());
		                $.ajax({
		                    url: 'Processor/EditBeneficiaryAccount.aspx',
                            data: {
                                CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                        AID: $('#<%=txt_hidden_AccountID.ClientID%>').val(),
		                        BankID: $('#Edit_ddl_Banks').val(),
		                        BankName: $('#Edit_ddl_Banks :selected').text(),
		                        BranchID: $('#Edit_ddl_Branches').val(),
		                        BranchName: $('#Edit_ddl_Branches :selected').text(), 
		                        AccNum: $('#Edit_accountnumber').val(),
		                        AccName: $('#Edit_accountname').val(),
		                        CountryID: $('#DDL_Edit_Banks_CountryList :selected').val(),
		                        CurrencyID: $('#Edit_TradingCurrency :selected').val(),
		                        CurrencyName: $('#Edit_TradingCurrency :selected').text(),
		                    },
		                    success: function() {
		                        showsuccess('','Account has been successfully edited');
		                        ShowEditAccount.dialog('close');
		                        loadBeneficiaryAccounts($('#<%=hidden_selectedBeneficiary.ClientID%>').val());
		                    },
		                    error: function(xhr,err){
		                        showerror("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
		                        showerror("responseText: "+xhr.responseText);	
		                    }
		                })
		            }
		            else
		            {
		                showerror("Please select all required fields");	
		            }
		        })

		        $('#btn_addnewaccountcomplete').click(function() {
		            var formValid=$("#addnewaccountform").valid();
			  
		            if(formValid){
		                $.ajax({
		                    url: 'Processor/AddBeneficiaryAccount.aspx',
		                    data: {
		                        BID: $('#<%=hidden_selectedBeneficiary.ClientID%>').val(),
		                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                        BankID: $('#Add_ddl_Banks').val(),
		                        BankName: $('#Add_ddl_Banks :selected').text(),
		                        BranchID: $('#Add_ddl_Branches').val(),
		                        BranchName: $('#Add_ddl_Branches :selected').text(), 
		                        AccNum: $('#NewAcc_accountnumber').val(),
		                        AccName: $('#NewAcc_accountname').val(),
		                        CountryID: $('#DDL_Banks_CountryList :selected').val(),
		                        CurrencyID: $('#NewAcc_TradingCurrency :selected').val(),
		                        CurrencyName: $('#NewAcc_TradingCurrency :selected').text(),
		                    },
		                    success: function() {
		                        showsuccess('','Account has been successfully added');
		                        ShowAddNewAccount.dialog('close');
		                        loadBeneficiaryAccounts($('#<%=hidden_selectedBeneficiary.ClientID%>').val());
		                    },
		                    error: function(xhr,err){
		                        showerror("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
		                        showerror("responseText: "+xhr.responseText);	
		                    }
		                })
		            }
		            else
		            {
		                showerror('Please select required fields');
		            }
			 
		        });

		        $('#btn_AddBeneficiary').click(function() {
		            $.ajax({
		                url: 'JQDataFetch/getCountriesForAgents.aspx',
		                success: function(response) {
		                    $('#AddBene_CountryDDL').empty();
		                    $('#AddBene_stateprovince').empty();
		                    $('#AddBene_suburbdistrict').empty();
		                    var data = response.split('~');
		                    data.sort();
		                    $('#AddBene_CountryDDL').append('<option value="">-- SELECT --</option>');
		                    $.each(data, function(item)
		                    {
		                        var datasplit = data[item].split('|');
		                        $('#AddBene_CountryDDL').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
		                    });
		                }
		            })
		            AddBeneficiaryDialog.dialog('open');
		        });

		        $('#btn_AddCredit').click(function() {
		            $('#TR_addcrediterrormsg').hide();
		            AddCreditDialog.dialog('open');
		        })

		        $('#btn_EditCustomer').click(function () {
		            EditCustomerDialog.dialog('open');
		        });

		        $('#btn_AddBeneficiarySubmit').click(function() {
		            var isValidForm=$("#beneficiaryaddform").valid();
			   
		            if(isValidForm)
		            {
		                $.ajax ({
		                    url: 'Processor/AddNewBeneficiary.aspx',
		                    data: {
		                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                        BeneName: $('#AddBene_FullName').val(),
		                        BeneLastName: $('#AddBene_lastname').val(),
		                        BeneFirstNames: $('#AddBene_firstnames').val(),
		                        AdLine1: $('#AddBene_addressline1').val(),
		                        AdLine2: $('#AddBene_addressline2').val(),
		                        Suburb: $('#AddBene_suburbdistrict :selected').text(),
		                        Postcode: $('#AddBene_postzipcode').val(),
		                        State: $('#AddBene_stateprovince :selected').text(),
		                        Country: $('#AddBene_CountryDDL :selected').text(),
		                        CountryID: $('#AddBene_CountryDDL :selected').val(),
		                        COB: $('#AddBene_countryofbirth :selected').text(),
		                        DOB: $('#AddBene_dob').val(),
		                        POB: $('#Addbene_placeofbirth').val(),
		                        National: $('#AddBene_nationality :selected').text(),
		                        Relation: $('#AddBene_relationship').val(),
		                        AKA: $('#AddBene_aka').val(),
		                        IDDetails: $('#AddBene_iddetails').val(),
		                        CNumber: $('#AddBene_contactnumber').val(),
		                        Email: $('#AddBene_email').val(),
		                    },
		                    success: function(response) {
		                        showsuccess('', 'Beneficiary Successfully Added');
		                        setTimeout(function () {
		                            location.reload();
		                        }, 2000);
							
		                    },
		                    error: function(xhr,err){
		                        showerror("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
		                        showerror("responseText: "+xhr.responseText);	
		                    }
		                });
		            }else
		            {
		                showerror('Please select required fields');
		            }
			   
		        });

		        $('#<%=chk_CompanyVerified.ClientID%>').change(function() {
		            $.ajax ({
		                url: 'Processor/UpdateCompanyVerified.aspx',
		                data: {
		                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                    CVerified: $('#<%=chk_CompanyVerified.ClientID%>').is(':checked'),
		                },
		                success: function(response) {
		                    showsuccess('','Successfully updated');
		                    setTimeout(function () {
		                        location.reload();
		                    }, 2000);
		                }
		            });
		        });

		        $('#btn_Submit').click(function() {
		            // $('#customereditform').submit();
		            var isValid=$("#customereditform").valid();
		            if(isValid)
		            {
		                $.ajax ({
		                    url: 'Processor/updateCustomerDetails.aspx',
		                    data: {
		                        CID: $('#txt_hidden_CustomerID').val(),
		                        lastname: $('#lastname').val(),
		                        firstnames: $('#firstnames').val(),
		                        COB: $('#countryofbirth :selected').text(),
		                        dob: $('#dob').val(),
		                        POB: $('#placeofbirth').val(),
		                        National: $('#nationality :selected').text(),
		                        AKA: $('#aka').val(),
		                        Occupation: $('#Cust_occupation').val(),
		                        UnitNo: $('#unitno').val(),
		                        StreetNo: $('#streetno').val(),
		                        StreetName: $('#streetname').val(),
		                        StreetType: $('#streettype :selected').text(),
		                        suburb: $('#suburb').val(),
		                        state: $('#state').val(),
		                        postcode: $('#postcode').val(),
		                        Country: $('#country').val(),
		                        homephone: $('#phonehome').val(),
		                        workphone: $('#phonework').val(),
		                        mobile: $('#phonemobile').val(),
		                        email1: $('#email1').val(),
		                        email2: $('#email2').val(),
		                        notes: $('#edit_notes').val(), //new changes
		                        password: $('#edit_password').val(),
		                        title: $('#title').val()
		                    },
		                    success: function(data) {
		                        showsuccess('','Customer has been edited successfully');
		                        EditCustomerDialog.dialog('close');
		                        setTimeout(function () {
		                            location.reload();
		                        }, 2000);
		                    }, 
		                    error: function(xhr,err){
		                        showerror("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
		                        showerror("responseText: "+xhr.responseText);	
		                    }

		                })
		            }
		            else
		            {
		                showerror("Please enter all required fields");	

		            }
		        })
				

		        $('#btn_edit_Submit').click(function() {
		            var isFormValid=$("#beneficiaryeditform").valid();
		            if(isFormValid){
		                // $('#beneficiaryeditform').submit();
		                $.ajax ({
		                    url: 'Processor/EditBeneficiary.aspx',
                            data: {
                                CID: $("#<%=txt_hidden_CustomerID.ClientID%>").val(),
		                        BID: $('#<%=hidden_selectedBeneficiary.ClientID%>').val(),
		                        BeneFirstName: $('#EditBene_firstnames').val(),
		                        BeneLastName: $('#EditBene_lastname').val(),
		                        AddressLine1: $('#EditBene_addressline1').val(),
		                        AddressLine2: $('#EditBene_addressline2').val(),
		                        Suburb: $('#EditBene_suburbdistrict :selected').text(),
		                        State: $('#EditBene_stateprovince :selected').text(),
		                        Postcode: $('#EditBene_postzipcode').val(),
		                        Country: $('#EditBene_CountryDDL :selected').text(),
		                        CountryID: $('#EditBene_CountryDDL :selected').val(),
		                        ContactNumber: $('#EditBene_contactnumber').val(),
		                        EmailAddress: $('#EditBene_email').val(),
		                        Relationship: $('#EditBene_relationship').val(),
		                        COB: $("#EditBene_countryofbirth :selected").text(),
		                        DOB: $('#EditBene_dob').val(),
		                        POB: $('#Editbene_placeofbirth').val(),
		                        National: $('#EditBene_nationality :selected').text(),
		                        AKA: $('#EditBene_aka').val(),
		                        ID: $('#EditBene_iddetails').val(),
		                    },
		                    success: function(data) {
		                        showsuccess('','Beneficiary has been edited successfully');
		                        EditCustomerDialog.dialog('close');
		                        setTimeout(function () {
		                            location.reload();
		                        }, 2000);
		                    }, 
		                    error: function(xhr,err){
		                        showerror("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
		                        showerror("responseText: "+xhr.responseText);	
		                    }

		                });
		            }else
		            {
		                showerror('Please select required fields');
		            }
		        });

		        $('#btn_EditBeneficiary').click(function() {
		            EditBeneficiaryDialog.dialog('open');
		        })

		        $('#btn_ViewDebitCredit').click(function() {
		            ShowDebitCreditDialog.dialog('open');
		        })

		        $('#btn_OverTheCounter').click(function() {
		            ShowOverTheCounter.dialog('open');
		            $('#OTC_availablefunds').val($('#<%=hidden_txt_accountbalance.ClientID%>').val());
		            $('#OTC_DIV_AvailableFunds').text('$'+parseFloat($('#<%=hidden_txt_accountbalance.ClientID%>').val()).toFixed(2));
		            var amountToBeSent = calculateAmountToBeSent($('#<%=hidden_txt_accountbalance.ClientID%>').val());
		            $('#OTC_amounttobesent').val(amountToBeSent);
		            var ServiceCharge = calculateServiceCharge(amountToBeSent);
		            $('#OTC_servicecharge').val(ServiceCharge);
		            $('#OTC_div_sending').text(amountToBeSent);

		            var totalsend = amountToBeSent * todayrate;
		            $('#OTC_div_lkrvalue').text(totalsend);

		        });

		        $('#CHK_ConfirmInfo').change(function() {
		            if (this.checked)
		            {
		                $('#btn_kyc_add_Submit').removeClass('btn_disabled');
		                $('#btn_kyc_add_Submit').addClass('btn_red');
		                $('#btn_kyc_add_Submit').attr("disabled", false);
		            }
		            else
		            {
		                $('#btn_kyc_add_Submit').removeClass('btn_red');
		                $('#btn_kyc_add_Submit').addClass('btn_disabled');
		                $('#btn_kyc_add_Submit').attr("disabled", true);
		            }
		        });

		  

		        $('#OTC_amounttobesent').change(function() {
		            //var amountToBeSent = calculateAmountToBeSent($('#OTC_amounttobesent').val());
		            //$('#OTC_amounttobesent').val(amountToBeSent);
		            var ServiceCharge = calculateServiceCharge($('#OTC_amounttobesent').val());
		            $('#OTC_servicecharge').val(ServiceCharge);
		            $('#OTC_div_sending').text($('#OTC_amounttobesent').val());
		            var totalsend = $('#OTC_amounttobesent').val() * todayrate;
		            $('#OTC_div_lkrvalue').text(totalsend);
		        });

			

		        $('#SM_servicecharge').change(function () {
		            if ($('#hidden_sendamountoverride').val() == 'N')
		            {
		                var totalsend = ($('#<%=hidden_txt_accountbalance.ClientID%>').val() - $('#SM_servicecharge').val()) * $('#SM_exchangerate').val();
		                var newsendingtotal = $('#<%=hidden_txt_accountbalance.ClientID%>').val() - $('#SM_servicecharge').val();
		                $('#SM_amounttobesent').val(parseFloat(newsendingtotal).toFixed(2));
		                //showsuccess('',totalsend);
		                $('#SM_div_lkrvalue').text(parseFloat(totalsend).toFixed(2));
		            }
		            else
		            {
		                //alert('Inside');
		                var totalsend = $('#SM_amounttobesent').val() * $('#SM_exchangerate').val();
		                //var newsendingtotal = $('#SM_amounttobesent').val() - $('#SM_servicecharge').val();
		                //$('#SM_amounttobesent').val(parseFloat(newsendingtotal).toFixed(2));
		                //showsuccess('',totalsend);
		                $('#SM_div_lkrvalue').text(parseFloat(totalsend).toFixed(2));
		                $('#SM_div_sending').text($('#SM_amounttobesent').val());
		            }
			    
		        })

		        $('#HD_amounttobesent').change(function() {
		            var ServiceCharge = calculateServiceCharge($('#HD_amounttobesent').val());
		            $('#HD_servicecharge').val(parseFloat(ServiceCharge).toFixed(2));
		            $('#HD_div_sending').text(parseFloat($('#HD_amounttobesent').val()).toFixed(2));
		            var totalsend = $('#HD_amounttobesent').val() * todayrate;
		            $('#HD_div_lkrvalue').text(parseFloat(totalsend).toFixed(2));
		        });

		        $('#<%=Image25.ClientID%>').click(function () {
		            $('#tblIndi').show();
		            $('#divBusiness').hide();
		            $('#DIV_EditCustomerNotesPW').hide();
		        });

		        $('#<%=Image21.ClientID%>').click(function () {
		            $('#blIndi').hide();
		            $('#divBusiness').show();
		            $('#DIV_EditCustomerNotesPW').hide();
		        });

		        $('#<%=Image12.ClientID%>').click(function () {
		            $('#tblIndi').hide();
		            $('#divBusiness').hide();
		            $('#DIV_EditCustomerNotesPW').show();
		        });

		        $('#btn_addcreditcomplete').click(function() {
                    if ($('#txt_CreditAmount').val() == "" || parseFloat($('#txt_CreditAmount').val()) < 0)
		            {
		                $('#TR_addcrediterrormsg').show();
		                $('#addcrediterrormsg').text('Please enter a valid amount to proceed.');
		            }
		            else
		            {
		                if ($('#txt_CreditDescription :selected').val() == "")
                        {
                            $('#TR_addcrediterrormsg').show();
                            $('#addcrediterrormsg').text('Please select a deposit method to proceed.');
		                }
		                else
		                {
		                    $.ajax({
		                        url: 'Processor/AddCredit.aspx',
		                        beforeSend: function() {
		                            AddCreditDialog.dialog('close');
		                            LoadingDIV.dialog('open');
		                        },
		                        data: {
		                            CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
		                            CreditAmount: $('#txt_CreditAmount').val(),
		                            CreditDescID: $('#txt_CreditDescription').val(),
		                            CreditDesc: $('#txt_CreditDescription :selected').text(),
		                        },
		                        complete: function() {
		                            LoadingDIV.dialog('close');
		                        },
		                        success: function(data) {
		                            $('#TR_addcrediterrormsg').show();
		                            $('#addcrediterrormsg').text('Please wait. Processing account credit.');
		                            openMsgDIV('None', 'Account has been successfully credited', '1');
		                            //alert('Account has been credited successfully');
		                            //location.reload();
		                        }, 
		                        error: function(xhr,err){
		                            showerror("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
		                            showerror("responseText: "+xhr.responseText);	
		                        }

		                    });
		                }
					
		            }
				
		        });

		        $.ajax({
		            url: 'JQDataFetch/getDepositMethods.aspx',
		            success: function(data) {
		                var results = data.split('~');
		                $('#txt_CreditDescription').append('<option value="">-- SELECT --</option>');
		                $.each(results, function(item) {
		                    var finalsplit = results[item].split('|');
						
		                    $('#txt_CreditDescription').append('<option value="' + finalsplit[0] + '">' + finalsplit[1] + '</option>');
						
		                });
		            }
		        });

		  
			
			
		    });
		    function Runkyc() {
		        $.ajax({
		            url: 'JQDataFetch/runkyc.aspx?custid=<%=Request.QueryString["custid"] %>',
		            success: function (data) {
		                var results = data.split(':');
		                if (results[0] = 'Info') {
		                    showsuccess(data);
		                }
		                else {
		                    showerror(data);
		                }
		            }
		        });
		    }
	   
		
	</script>
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="hidCustType" />
      


<div class="container_btns">
	<table class="tbl_width_1200">
		<tr>
			<td class="tbl_width_280">
				<table class="tbl_width_280">
					<tr>
						<td class="stats_risk_heading">RISK PROFILE</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="risk_1" id="td_risk1" runat="server">1</td>
						<td class="risk_2" id="td_risk2" runat="server">2</td>
						<td class="risk_neutral" id="td_risk3" runat="server">3</td>
						<td class="risk_neutral" id="td_risk4" runat="server">4</td>
					</tr>
				</table>
			</td>
			<td>
				<table class="tbl_width_920">
					<!-- MAIN TABLE STARTS HERE -->
					<tr>
						<td>
							<table class="tbl_width_920">
								<tr style="height:15px;">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td class="stats_02">TOTAL LAST 30 DAYS AUD</td>
									<td class="stats_02">TOTAL LAST YEAR AUD</td>
									<td class="stats_02">TRANSACTIONS 30 DAYS/YEAR</td>
								</tr>
								<tr style="height:25px;">
									<td>&nbsp;</td>
									<td id="CALC_BTN">CALC</td>
									<td>&nbsp;</td>
									<td class="stats_03">
										<div id="DIV_MonthlyRemittanceTotal" runat="server">$ 2,000.00</div>
									</td>
									<td class="stats_03">
										<div id="DIV_YearlyRemittanceTotal" runat="server">$ 50,000.00</div>
									</td>
									<td class="stats_03">
										<div id="DIV_Totals" runat="server">10/127</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
				<!-- MAIN TABLE ENDS HERE -->
			</td>
		</tr>
	</table>
</div>



<div class="container_one" style="background-color:#efefef;">
			<table class="tbl_width_1200" >
				<tr>
					<td style="width:280px; vertical-align:top;">
						<table class="tbl_width_280">
														
							<!-- tbl comes here *** -->
							<tr><td>
							<table class="tbl_width_280 background_FFFFFF">

							<tr style="height:25px; background-color: #76D17F;"><td>&nbsp;</td></tr>
							<tr style="height:150px; background-color: #76D17F;">
								<td>
									<table class="tbl_width_150">
										<tr>
											<td><div class="imgWrapper"><img width="150" height="150" id="img_CustomerPic" runat="server" /></div></td>
										</tr>
									</table>
									

								</td>
									

							</tr>
							
                            <tr style="background-color: #76D17F;" class="cus_divider_solid">
								<td class="cus_name">
                                    <div id="div_cust_full_name" runat="server"></div>
									<asp:TextBox ID="hidden_txt_accountbalance" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_hidden_CustomerID" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_hidden_FirstBeneficiaryID" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_hidden_AccountID" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_hidden_Rate" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="hidden_SpendingPower" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="hidden_CustomerFullName" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_AUDMLimit" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_AUDYLimit" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_NbMLimit" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_NbYLimit" runat="server"></asp:TextBox>
								</td>
							</tr>
                                <tr id="trnotes" runat="server">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td>
                                                <table class="tbl_width_240">
                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td class="dsh_cus_notes"><span id="shownotes" runat="server"></span></td>
                                                    </tr>
                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>



                                
                            </tr>
							<!-- ICONS -->
                                <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table class="tbl_width_280 dsh_KYC_tab_h">
                                            <tr style="height:45px;">
                                                <td style="width:69px; vertical-align:middle;" class="dsh_tab_active" id="TD_001">
                                                    <table style="width:100%;">
                                                        <tr><td style="height:20px; text-align:center; "><asp:Image ID="Image1" runat="server" ImageUrl="~/images/id-card-gry.png" /></td></tr>
                                                        <!-- <tr><td>KYC CHECK</td></tr> -->
                                                    </table>
                                                </td>
                                                <td style="width:2px; background-color:#EFEFEF;" class="normal_font">&nbsp;</td>
                                                <td style="width:68px; vertical-align:middle;" class="dsh_tab_inactive" id="TD_002">
                                                    <table style="width:100%;">
                                                        <tr><td style="height:20px; text-align:center; "><asp:Image ID="Image26" runat="server" ImageUrl="~/images/cu_settings.png" Height="24px" /></td></tr>
                                                        <!-- <tr><td>KYC CHECK</td></tr> -->
                                                    </table>
                                                </td>
                                                <td style="width:2px; background-color:#EFEFEF;" class="normal_font">&nbsp;</td>
                                                <td style="width:68px;" class="dsh_tab_inactive" id="TD_003">&nbsp;</td>
                                                <td style="width:2px; background-color:#EFEFEF;" class="normal_font">&nbsp;</td>
                                                <td style="width:69px;" class="dsh_tab_inactive" id="TD_004">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            <tr>
                                    <td>
                                        <div id="DIV_001">
                                            <table style="width:100%; background-color: #FFF;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>
                                                        <table class="tbl_width_280"><tr><td>&nbsp;</td><td class="dsh_KYC_close">x</td></tr></table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="tbl_width_240">
                                                            <tr>
                                                                <td>
                                                                    <div class="dsh_KYC_success" id="dsh_KYC_Success" runat="server">KYC CHECK COMPLETED !</div>
                                                                    <span class="dsh_KYC_fail" id="dsh_KYC_Fail" runat="server">KYC CHECK FAILED !</span>
                                                                    <div id="dsh_KYC_NotRun" runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td class="dsh_KYC_none">WARNING !</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="dsh_KYC_none_font">KYC Check has not been completed for this customer.</td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_240">
                                                                        <tr>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_DateLabel" runat="server" style="width:50px;">Date:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_DateValue" runat="server" style="width:190px;">2</td></tr></table></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_ByLabel" runat="server">By:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_ByValue" runat="server" style="width:190px;">2</td></tr></table></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_RefLabel" runat="server" style="width:50px;">Ref:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_RefValue" runat="server" style="width:190px;">2</td></tr></table></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_InfoLabel" runat="server">Info:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_InfoValue" runat="server">2</td></tr></table></td>
                                                                        </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="spacer5"><td>&nbsp;</td></tr>
                                                                        <tr><td class="dsh_KYC_none_font">
                                                                            <asp:Label ID="lbl_KYC_ManualKYCComments" runat="server" Text="Label"></asp:Label></td></tr>


                                                                        
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr><td>&nbsp;</td></tr>

                                                            <tr id="TR_DisplayKYCErrors" runat="server">
                                                                <td>
                                                                    <table class="tbl_width_240">
                                                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                        <tr>
                                                                            <td class="kyc_info_red" id="TD_KYCResultDisplay" runat="server">
                                                                                <asp:Label ID="LBL_KYCErrors" runat="server" Text="Label"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr><td>&nbsp;</td></tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <tr id="TR_RUNKYC" runat="server">
                                                                <td>
                                                                    <table class="tbl_width_240">
                                                                        <tr>
                                                                            <td style="display:inline; float:left;">
                                                                                <asp:Button ID="BTN_CheckKYC" runat="server" Text="Run KYC Check" OnClientClick="return callLoadingDiv();" CssClass="aa_btn_green" OnClick="BTN_CheckKYC_Click" />
                                                                               
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                             
                                                             <tr id="TR_ConfirmRUNKYC" runat="server">
                                                                <td>
                                                                    <table class="tbl_width_240" >
                                                                        <tr class="spacer10" style="background-color:#FCF8E1"><td>&nbsp;</td></tr>
                                                                        <tr><td class="dsh_kyc_msg">Are you sure you want to rerun the KYC Check for this Customer?</td></tr>
                                                                        <tr class="spacer10" style="background-color:#FCF8E1"><td>&nbsp;</td></tr>
                                                                        <tr><td style="background-color:#FCF8E1; padding-left:10px;"><asp:button ID="confirm_YES_RunKYC" runat="server" text="Yes"   OnClientClick="return callLoadingDiv();"
                                                                            CssClass="aa_btn_green_slim" style="margin-bottom:0px !important;" OnClick="confirm_YES_RunKYC_Click" />&nbsp;<asp:button ID="confirm_NO_RunKYC" runat="server" text="No" 
                                                                                CssClass="aa_btn_red_slim" style="margin-bottom:0px !important;" OnClick="confirm_NO_RunKYC_Click" /></td></tr>
                                                                        <tr class="spacer10" style="background-color:#FCF8E1"><td>&nbsp;</td></tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr><td>&nbsp;</td></tr>
                                                            
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="DIV_002">
                                            <table style="width:100%;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>
                                                        <table class="tbl_width_240">
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_240">
                                                                        <tr>
                                                                            <td class="normal_font" style="vertical-align:middle;">Notification emails</td>
                                                                            <td style="text-align:right;"><label class="chk_label"><input class="switch" type="checkbox" id="CHK_Active" checked="checked"/><span class="cu_slider"></span></label></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr><td>&nbsp;</td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="DIV_003" style="display:none;">
                                            <table style="width:100%;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="DIV_004" style="display:none;">
                                            <table style="width:100%;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>
                                                        <table class="tbl_width_240">
                                                            <tr><td>fdsfdf</td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                            <tr><td class="spacer10" style="background-color:#EFEFEF;">&nbsp;</td></tr>

                            <tr>
                                <td class="dsh_cust_details_heading">Customer Details<asp:HiddenField ID="hidden_KYCRunBefore" runat="server"></asp:HiddenField></td>
                                 
                            </tr>

                            <tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h">
												<asp:Image ID="Image24" runat="server" ImageUrl="~/images/c_icon_01.png" /></td>
											<td class="cus_c"><div id="div_customertype" runat="server">TYPE</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h">
												<asp:Image ID="Image5" runat="server" ImageUrl="~/images/c_icon_01.png" /></td>
											<td class="cus_c"><div id="div_CustomerFullAddress" runat="server">ADDRESS</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/c_icon_07.png" />
											</td>
											<td class="cus_c"><div id="div_CustomerDOB" runat="server">DOB</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h"><asp:Image ID="Image9" runat="server" ImageUrl="~/images/c_icon_04.png" />
											</td>
											<td class="cus_c"><div id="div_CustomerMobile" runat="server">MOBILE</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h">
												<asp:Image ID="Image8" runat="server" ImageUrl="~/images/c_icon_03.png" /></td>
											<td class="cus_c"><div id="div_CustomerHomeNumber" runat="server">HOME</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h">
												<asp:Image ID="Image10" runat="server" ImageUrl="~/images/c_icon_06.png" /></td>
											<td class="cus_c"><div id="div_CustomerWorkNumber" runat="server">WORK</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h"><asp:Image ID="Image6" runat="server" ImageUrl="~/images/c_icon_02.png" />
											</td>
											<td class="cus_c"><div id="div_CustomerEmail1" runat="server">EMAIL1</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h"><asp:Image ID="Image7" runat="server" ImageUrl="~/images/c_icon_08.png" /></td>
											<td class="cus_c"><div id="div_CustomerEmail2" runat="server">EMAIL2</div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr style="background-color:#D6DFDE;"><td>&nbsp;</td></tr>
							<tr><td style="padding-left:20px; background-color:#D6DFDE;">
                                <button class="aa_btn_red" id="btn_EditCustomer" onclick="return false"><span>Edit</span></button>&nbsp;&nbsp;<button class="aa_btn_green" id="BTN_DocumentsClick" onclick="return false"><span>Documents</span></button></td></tr>
							<tr style="background-color:#D6DFDE;"><td>&nbsp;</td></tr>



							<!--tbl finishes here ***-->
								</table></td></tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr class="background_FFFFFF">
                                <td>
                                    <table class="tbl_width_240">
                                        <tr class="spacer15"><td>&nbsp;</td></tr>
                                        <tr><td class="normal_font">Send your bank account details to <asp:Label ID="lbl_custname" runat="server" Text=""></asp:Label></td></tr>
                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                        <tr><td><asp:Button ID="btn_SendBankDetails" runat="server" Text="Send Bank Details" CssClass="aa_btn_green" onClientClick="return false"/></td></tr>
                                        <tr class="spacer15"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>

						</table>
					</td>
					<td style="width:20px;">&nbsp;</td>
					<td style="width:900px; vertical-align:top;"> 
						<table> <!-- MAIN 900px TABLE START -->
							<tr>
								<td>
<!-- ************************* NOTES/PASSWORD PROTECTION DETAILS ************************* -->
	<div id="overallDIV" runat="server">
		<table class="tbl_width_900"> 
			<div id="notespassdiv" runat="server">
				
				<tr id="ErrorMessageArea" runat="server">
					<td>
						<table class="tbl_width_900">
							<tr>
								<td class="alert_01_red">
									<table class="tbl_width_860">
										<tr>
											<td style="width:60px;"><asp:Image ID="Image4" runat="server" ImageUrl="~/images/bell.png" />
											</td>
											<td style="width:800px; vertical-align:top;" class="alert_01_red_message" id="errormessagedisplay" runat="server"></td>
										</tr>
										<tr>
											<td style="height:20px;"></td>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td></tr>
						</table>
					</td>
				</tr>

				<tr id="MessageArea" runat="server">
					<td>
						<table class="tbl_width_900">
							<tr>
								<td class="alert_01_green">
									<table class="tbl_width_860">
										<tr>
											<td style="width:60px;"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/idea.png" />
											</td>
											<td style="width:800px; vertical-align:top;" class="alert_01_green_message">Error comes here</td>
										</tr>
										<tr>
											<td style="height:20px;"></td>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td></tr>
						</table>
					</td>
				</tr>

			<tr class="background_FFFFFF" id="NotesAreaTR" runat="server">
				<td>
                    <table class="tbl_width_900">
                        <tr id="trpassword">
                            <td>
                                <table class="tbl_width_900">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_860">
					                            <tr><td style="height:15px;">&nbsp;</td></tr>
					                            <tr  >
						                            <td>
							                            <table class="tbl_width_860">
                                                            <tr>
						                                        <td class="customer_password_fnt">Please note this customer account is password protected. Do not proceed further without verifying the password:</td>
						                                        <td class="ali_right"><span class="customer_password_bold_fnt" id="showpassword"></span></td>
							                                </tr>
							                            </table>
						                            </td>
					                            </tr>
					                
					                            <tr><td style="height:15px;">&nbsp;</td></tr>
				                            </table>
                                        </td>
                                    </tr>
                                    <tr ><td class="spacer10 wiz_bg_EFEFEF">&nbsp;</td></tr>
                                </table>

                                
                            </td>
                        </tr>
                        
                    </table>				    
			</td></tr>
				
			</div>
			

			<tr class="background_FFFFFF"> <!-- Account Balance and Adding/Viewing Transactions -->
				<td>
					<table class="tbl_width_860">
						<tr><td style="height:20px;">&nbsp;</td></tr>
						<tr><td class="label_headings">CUSTOMER ACCOUNT BALANCE</td></tr>
						<tr>
							<td><table class="tbl_width_860">
								<tr>
									<td class="cust_acc_bal"><div id="DIV_BankAccountBalance" runat="server">
									                         </div></td>
									<td style="text-align:right;"><button class="btn_green_nomargin" id="btn_ViewDebitCredit" onclick="return false"><span>View Account</span></button>&nbsp;&nbsp;<button class="btn_green_nomargin" id="btn_Refund" onclick="return false">Refund</button>&nbsp;&nbsp;<button class="btn_green_nomargin" id="btn_AddCredit" onclick="return false"><span>Add Credit</span></button></td>
								</tr>
								</table>
								
							</td></tr>
						<tr><td style="height:20px;">&nbsp;</td></tr>
					</table>
				</td>
			</tr>

			<tr>
				<td class="spacer10">&nbsp;</td>
			</tr>

			<tr class="background_FFFFFF"> <!-- SELECT BEN STARTS -->
				<td>
					<table class="tbl_width_900">
					<tr>
						<td style="height:15px;">&nbsp;</td>
					</tr>                    

					<tr>
						<td>
							<table class="tbl_width_860">
								<tr>
									<td class="label_headings">SELECT BENEFICIARY</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><asp:DropDownList ID="ddl_List_Beneficiaries" runat="server"></asp:DropDownList></td>
									<td style="text-align:right;"><button class="btn_green_nomargin" id="btn_AddBeneficiary" onclick="return false">Add New Beneficiary</button></td>

								</tr>
							</table>
						</td>
						
					</tr>
					<tr>
						<td style="height:20px;">&nbsp;</td>
					</tr>
					<tr id="tr_ben_info">
						<td>
							<div>
								<table class="tbl_width_860"> <!-- BEN DETAILS TABLE START -->
									<tr>
										<td>
											<table class="tbl_width_860">
												<tr>
													<td style="background-color:#F4F2F2; height:125px; width:840px; margin: 0 auto;">
														<table class="tbl_width_800">
															<tr><td>&nbsp;</td></tr>
															<tr><td>
																<table class="tbl_width_820">
																	<tr>
																		<td><div class="dsh_ben_name" id="div_BeneficiaryName">&nbsp;</div>
																		</td>
																		<td class="ali_right" style="width:150px;">&nbsp;</td>
																	</tr>
																</table></td></tr>
															
															
															<tr class="spacer5"><td>&nbsp;</td></tr> <!-- SPACER 5px -->
															
															<tr>
																<td>
																	<table class="tbl_width_820">
																		<tr>
																			<td style="width:820px; background-color:#F8F8F8;">
																				<table style="width:800px; margin: 0 auto;">
																					<tr>
																						<td class="bene_001" style="width:775px;"><div id="div_BeneficiaryAddress" title="Beneficiary Address">hehehe</div></td>
																						<td style="width:25px; text-align:right;"><i class="material-icons_4">home</i></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>

															<tr class="spacer5"><td>&nbsp;</td></tr> <!-- SPACER 5px -->

															<tr>
																<td>
																	<table class="tbl_width_820">
																		<tr>
																			<td style="width:280px; background-color:#F8F8F8;">
																				<table style="width:260px; margin: 0 auto;">
																					<tr>
																						<td class="bene_001" style="width:235px;"><div id="div_BeneficiaryNationalityCardID" title="Identification ID"></div></td>
																						<td style="width:25px; text-align:right;"><i class="material-icons_4">chrome_reader_mode</i></td>
																					</tr>
																				</table> 
																			</td>
																			<td style="width:5px;">&nbsp;</td>
																			<td style="width:175px; background-color:#F8F8F8;">
																				<table style="width:155px; margin: 0 auto;">
																					<tr>
																						<td class="bene_001" style="width:130px;"><div id="div_BeneficiaryTelHome" title="Contact Number">&nbsp;</div></td>
																						<td style="width:25px; text-align:right;"><i class="material-icons_4">phone</i></td>
																					</tr>
																				</table>
																			</td>
																			<td style="width:5px;">&nbsp;</td>
																			<td style="width:175px; background-color:#F8F8F8;">
																				<table style="width:155px; margin: 0 auto;">
																					<tr>
																						<td class="bene_001" style="width:130px;"><div id="div_BeneficiaryAKA" title="Also Known As"></div></td>
																						<td style="width:25px; text-align:right;"><i class="material-icons_4">recent_actors</i></td>
																					</tr>
																				</table>
																			</td>
																			<td style="width:5px;">&nbsp;</td>
																			<td style="width:175px; background-color:#F8F8F8;">
																				<table style="width:155px; margin: 0 auto;">
																					<tr>
																						<td class="bene_001" style="width:130px;"><div id="div_BeneficiaryRelation" title="Relationship to Customer"></div></td>
																						<td style="width:25px; text-align:right;"><i class="material-icons_4">recent_actors</i></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>


															<tr class="spacer5"><td>&nbsp;</td></tr>

															<tr>
																<td>
																	<table class="tbl_width_820">
																		<tr>
																			<td style="width:280px; background-color:#F8F8F8; height:30px;">
																				<table style="width:260px; margin: 0 auto;">
																					<tr>
																						<td class="bene_001" style="width:235px;"><div id="div_BeneficiaryRelationship" title="Beneficiary Relationship with Sender"></div></td>
																						<td style="width:25px; text-align:right;"><i class="material-icons_4">recent_actors</i></td>
																					</tr>
																				</table>
																			</td>
																			<td style="width:5px;">&nbsp;</td>
																			<td style="width:535px; background-color:#F8F8F8;">
																				<table style="width:515px; margin: 0 auto;">
																					<tr>
																						<td class="bene_001" style="width:490px;"><div id="div_BeneficiaryEmailAddress" title="Email Address"></div></td>
																						<td style="width:25px; text-align:right;"><i class="material-icons_4">mail</i></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>

															<tr><td class="spacer10">&nbsp;</td></tr>
															<tr><td>
																	<table class="tbl_width_820" id="BeneAccountTable">
																		<thead>
																			<td class="tbl_heading_ben_fnt_01">BANK</td>
																			<td class="tbl_heading_ben_fnt">BRANCH</td>
																			<td class="tbl_heading_ben_fnt">ACCOUNT NO</td>
																			<td class="tbl_heading_ben_fnt_02" style="width:40px;">TYPE</td>
																			<td class="tbl_heading_ben_fnt_02" style="width:40px;">ACT?</td>
																			<td class="tbl_heading_ben_fnt_02" style="width:60px;">EDIT</td>
																			<td class="tbl_heading_ben_fnt_03" style="width:60px;">SEND</td>
																		</thead>
																	</table>
																</td>
															</tr>
															<tr class="spacer10"><td>&nbsp;</td></tr>
															<tr>
																<td>
																<table class="tbl_width_820">
																	<tr>
																		<td><button class="aa_btn_red" id="btn_EditBeneficiary" onclick="return false"><span>Edit Beneficiary</span></button>&nbsp;&nbsp;<button class="aa_btn_green" id="btn_AddAccount" onclick="return false" hidden="hidden">Add New Account</button></td>
																		<td class="ali_right">&nbsp;</td>
																	</tr>
																</table>
																</td>
															</tr>
															<tr><td style="height:20px;">&nbsp;</td></tr>
														</table>
														
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style="height:20px;">&nbsp;</td>
									</tr>

									<tr >
										<td hidden="hidden">
											<!-- DO NOT DELETE -->
											<input type="text" id="hidden_selectedBeneficiary" name="hidden_selectedBeneficiary" runat="server" />&nbsp;
											<input type="text" id="hidden_hasKYC" name="hidden_hasKYC" runat="server" />
											<input type="text" id="hidden_CurrentSentAmount" name="hidden_CurrentSentAmount" runat="server" />
											<input type="text" id="hidden_KYCExpiryDays" name="hidden_KYCExpiryDays" runat="server" />
											<input type="text" id="hidden_NbTrans" name="hidden_NbTrans" runat="server" />
											<input type="text" id="sendingBank" name="sendingBank" />
											<input type="text" id="sendingBankBranch" name="sendingBankBranch" />
											<input type="text" id="hidden_hasID" name="hidden_hasID" runat="server" />
											<input type="text" id="hidden_FundsAvailable" name="hidden_FundsAvailable" runat="server" />
											<input type="text" id="hidden_AgentID" name="hidden_AgentID" runat="server" />
                                            <input type="text" id="hidden_CountryID" name="hidden_CountryID" runat="server" />
                                            <input type="text" id="hidden_CurrencyID" name="hidden_CurrencyID" runat="server" />
											<!-- END DO NOT DELETE -->

										</td>
									</tr>
								</table> <!-- BEN DETAILS TABLE END -->
							</div>
						</td>
					</tr>
				</table> <!-- SELECT BEN ENDS -->
				</td>
			</tr>

			<tr>
				<td class="spacer10">&nbsp;</td>
			</tr>

			<tr>
				<td>

	<!-- ************************* PAST TRANSACTIONS ************************* -->
	<div class="container_nobg_pad_tb no_padding_btm no_padding_top" id="Div1" runat="server">
		<table class="tbl_width_900 background_FFFFFF"> 
			<tr hidden="hidden"><td><input type="text" id="hidden_risklevel" name="hidden_risklevel" runat="server" /></td></tr>

            <tr><td class="dsh_cust_details_heading">ALL Past Transactions</td></tr>
            <tr style="height:20px;"><td>&nbsp;</td></tr>
            
            <tr>
                <td>
				    <table class="tbl_width_860">
					    <tr>
                            <td>
                                <div id="div-acc-pastrecords" class="ui-accordion"> <!-- ACCORDIAN DIV STARTS HERE -->
                                    <div>
										<table id="example" class="display nowrap" cellspacing="0" width="100%">

												<thead>
													<tr class="tbl__full__width">
														<th class="tbl__head__left">TRN ID</th>
														<th class="tbl__head__left">DATE</th>
														<th class="tbl__head__left">BENEFICIARY</th>
														<th class="tbl__head__left">RATE</th>
														<th class="tbl__head__numbers">AMOUNT</th>
														<th class="tbl__head__numbers">CHARGE</th>
														<th class="tbl__head__numbers">CUR</th>
														<th class="tbl__head__numbers">AMOUNT</th>
                                                        <th class="tbl__head__left">STATUS</th>
														<th>MORE INFO</th>
													</tr>
												</thead>

												<tbody>
		 
												</tbody>
										</table>
									</div>
									
								</div> <!-- ACCORDIAN DIV ENDS HERE -->

						</td></tr>
					<tr>
						<td style="height:20px;">&nbsp;</td>
					</tr>
				</table>

			</td></tr>
		</table>
	</div>
	<!-- ************************* PAST TRANSACTIONS ************************* -->

				</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
			</tr>

		</table>
	</div>
	<!-- ************************* NOTES/PASSWORD PROTECTION DETAILS ************************* -->
								</td>
							</tr>
						</table> <!-- MAIN 900px TABLE END -->
					</td>
				</tr>
				</table>
	</div>










<!-- this section needs to be taken out and put into the new table format -->


	

<!-- ************************* LOADING ************************* -->
<div id="LoadingDiv">
  <p>&nbsp;</p>
  <p>Please wait until beneficiary details are loaded</p>
</div>




<!-- ************************* BUTTON 02 VIEW ACCOUNT MODAL POPUP START ************************* --> 
	<div id="ShowCreditDebitsDIV">
		<div class="add_credit_form">
			
			<table class="tbl_width_640">
				
				<tr style="height:20px;"><td>&nbsp;</td></tr>

				<tr>
					<td>
					<table class="tbl_width_640">
						<tr>
							<td>
								<table id="DebitCreditTbl">
										<thead>
											<tr class="tbl__full__width">
                                                <th class="tbl__head__left" style="width:10%;">REF #</th>
												<th class="tbl__head__left" style="width:10%;">DATE/TIME</th>
												<th class="tbl__head__left" style="width:10%;">DESCRIPTION</th>
												<th class="tbl__head__numbers" style="width:10%;">DEBITS</th>
												<th class="tbl__head__numbers" style="width:70%;">CREDITS</th>
											</tr>
										</thead>
										<tbody></tbody>
								</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>

		</div>
	</div> 
<!-- ************************* BUTTON 02 VIEW ACCOUNT MODAL POPUP END ************************* --> 


<!-- ************************* BUTTON 03 ADD CREDIT MODAL POPUP START ************************* --> 
	<div id="AddCreditDiv"> 
		<div class="add_credit_form">
			
				<table class="tbl_width_440">
				
                <tr><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

				<tr id="TR_addcrediterrormsg">
					<td class="wiz_bg_EFEFEF">
						<table class="tbl_width_400">
                            <tr style="background-color:#fcdbd9;">
								<td>
									<table class="tbl_width_380">
										<tr>
											<td class="normal_font" style="color:#EB585C; padding: 10px 0px;"><span id="addcrediterrormsg">&nbsp;</span></td>
										</tr>
                                        
									</table>
								</td>
							</tr>
                            <tr><td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					<table class="tbl_width_400">
						<tr>
							<td class="aa_label_font">AUD AMOUNT </td>
							<td class="aa_label_font">&nbsp;</td>
							<td class="aa_label_font">DEPOSIT METHOD</td>
						</tr>
						<tr>
							<td style="width:100px;"><div><input class="aa_input" id="txt_CreditAmount" style="width:100%;" type="number" value="" min="0" /></div></td>
							<td style="width:20px;">&nbsp;</td>
							<td style="width:320px; vertical-align:top;"><div><select id="txt_CreditDescription" style="width:100%;"></select></div></td>
						</tr>
					</table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

				<tr><td style="text-align: right;"><input class="aa_btn_green" id="btn_addcreditcomplete" type="button" value="Add Credit"/></td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				</table>

			</div>
		</div>
<!-- ************************* BUTTON 03 ADD CREDIT MODAL POPUP END ************************* --> 





<!-- ************************* BUTTON 04 ADD NEW BENEFICIARY MODAL POPUP START ************************* --> 
	
<!-- ************************* BUTTON 04 ADD NEW BENEFICIARY MODAL POPUP END ************************* --> 





<!-- ************************* BUTTON 07 SEND MONEY MODAL POPUP START ************************* --> 
	<div id="SendMoneyDIV"> 
		<div class="send_money_form">
			<form method="get" id="sendmoneyform">
				
				<table class="tbl_width_440">
				    <tr><td><span id="SM_showdollaramount"></span></td></tr>

				    <tr><td>&nbsp;</td></tr>

				    <tr>
					    <td class="err_modal_general_green">
						    <table class="tbl_width_420">
							    <tr>
								    <td>
									    <table class="tbl_width_420">
										    <tr>
											    <td style="width:35px; vertical-align:middle;"><asp:Image ID="Image14" runat="server" ImageUrl="~/images/wallet.png" /></td>
											    <td class="ag_transfer_labels" style="width:285px; vertical-align:middle;">AVAILABLE FUNDS</td>
											    <td style="width:100px; vertical-align:middle;" class="ag_transfer_dollars"><span id="SPN_AddTR_AvailableFunds">$ 2,000.00</span></td>
										    </tr>
									    </table>
								    </td>
							    </tr>
						    </table>
					    </td>
				    </tr>
	
				<tr>
					<td>
						<div id="DIV_TRN_Errors">
							<table class="tbl_width_440">
								<tr class="spacer10"><td>&nbsp;</td></tr>
								<tr>
									<td class="err_modal_general">
										<table class="tbl_width_420">
											<tr class="spacer10"><td>&nbsp;</td></tr>
											<tr>
												<td>
													<div id="DIV_TRN_Add_Error_Funds">
														<table class="tbl_width_420">
															
															<tr>
																<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image13" runat="server" ImageUrl="~/images/err.png" /></td>
																<td style="width:385px; vertical-align:middle;">Insufficient funds to complete transaction.</td>
															</tr>
															<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
														</table>
													</div>
													<div id="DIV_TRN_Add_Error_Purpose">
														<table class="tbl_width_420">
															<tr>
																<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image16" runat="server" ImageUrl="~/images/err.png" /></td>
																<td style="width:385px; vertical-align:middle;">Please select the purpsose of the transaction to complete transaction.</td>
															</tr>
															<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
														</table>
													</div>
													<div id="DIV_TRN_Add_Error_Source">
														<table class="tbl_width_420">
															<tr>
																<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image17" runat="server" ImageUrl="~/images/err.png" /></td>
																<td style="width:385px; vertical-align:middle;">Please select the source of funds to complete transaction.</td>
															</tr>
															<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
														</table>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
							<tr>
								<td class="ag_transfer_labels_02" style="width:340px;">FUNDS TO BE SENT</td>
								<td><div class="ali_right" style="width:100px;"><input class="aa_input ali_right background_FFFFFF" id="SM_amounttobesent" style="width:100%;" type="text" value=""  /></div></td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr> 
								<tr>
									<td class="ag_transfer_labels_02" style="width:220px;">SERVICE CHARGES</td>
									<td>
										<table>
											<tr>
												<td><input class="aa_btn_green_outline" id="BTN_PromoCode" type="button" style="width:50px;" value="Promo" /></td>
												<td style="width:10px;">&nbsp;</td>
												<td><input class="aa_btn_red_outline" id="SM_btn_override" type="button" style="width:50px;" value="O/ride" /></td>
												<td style="width:10px;">&nbsp;</td>
												<td style="width:100px;"><div><input class="aa_input ali_right" id="SM_servicecharge" style="width:100%;" type="text" value="" /></div></td>
											</tr>
										</table>
									</td>
								</tr>
						</table>
					</td>
				</tr>

				<tr id="TR_PromoCode">
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr> 
								<tr>
									<td class="ag_transfer_labels_02" style="width:220px;">PROMO CODE</td>
									<td>
										<table>
											<tr>
												<td style="width:220px;"><div><input class="aa_input ali_right" id="SM_PromoCode" style="width:100%;" type="text" value="" /></div></td>
											</tr>
										</table>
									</td>
								</tr>
						</table>
					</td>
				</tr>


				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td></tr> 
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">EXCHANGE RATE</td>
								<td>
									<table class="tbl_width_220">
										<tr>
											<td style="width:50px;">&nbsp;</td>
											<td style="width:10px;">&nbsp;</td>
											<td class="ali_right"><input class="aa_btn_red_outline" id="SM_btn_overrideexchangerate" style="width:50px;" type="button" value="O/ride" /></td>
											<td style="width:10px;">&nbsp;</td>
											<td style="width:100px;"><div><input class="aa_input ali_right" id="SM_exchangerate" style="width:100%;" type="text" value="" /></div></td>
											</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">PURPOSE OF TRANSACTION</td>
								<td style="width:220px; text-align:right;">
									<div class="login__input">
										<select id="SM_Purpose" style="width:220px;">
											<option value="NOSELECT">-- SELECT --</option>
											<option value="Family Support">Family Support</option>
                                            <option value="Personal Savings">Personal Savings</option>
											<option value="Personal Travels & Tours">Personal Travels & Tours</option>
											<option value="Repayment of Loans">Repayment of Loans</option>
											<option value="Gift">Gift</option>
											<option value="Special Occasion">Special Occasion</option>
											<option value="Medical Expenses">Medical Expenses</option>
											<option value="Investments">Investments</option>
											<option value="Goods Purchased">Goods Purchased</option>
											<option value="Other">Other</option>
										</select>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">SOURCE OF FUNDS</td>
								<td style="width:220px; text-align:right;">
									<div class="login__input">
										<select id="SM_SourceOfFunds" style="width:220px;">
											<option value="NOSELECT">-- SELECT --</option>
											<option value="Personal Savings">Personal Savings</option>
											<option value="Business Activities">Business Activities</option>
											<option value="Loan">Loan</option>
											<option value="Family Money">Family Money</option>
                                            <option value="Other">Other</option>
										</select>
									</div>
								</td>
						</tr>
					</table>
					</td>
				</tr>

				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">NOTES</td>
								<td style="width:220px; text-align:right;"><div><textarea name="TXT_Add_TRN_Notes" style="width:220px; height:55px; resize:none; line-height:16px !important; padding-top:8px;" class="aa_input"></textarea></div></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td><span id="SM_span_senderror"></span></td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr><td><input type="text" id="txt_hidden_foreignamount" name="txt_hidden_foreignamount" hidden="hidden" /><input type="text" id="txt_remainingaccountbalance" name="txt_remainingaccountbalance" hidden="hidden" /><input type="text" id="txt_bankname" name="txt_bankname" hidden="hidden" /><input type="text" id="hidden_adminoverride" name="hidden_adminoverride" value="N" hidden="hidden" /><input type="text" id="hidden_goingStatus" name="hidden_goingStatus" value="PENDING" hidden="hidden"  /><input type="text" id="hidden_compliancereason" name="hidden_compliancereason" value="" hidden="hidden"  /><input type="text" id="hidden_enoughfunds" name="hidden_enoughfunds" value="" hidden="hidden"   /><input type="text" id="hidden_openedpromo" name="hidden_openedpromo" value="" hidden="hidden"  /><input type="text" id="hidden_currencycode" name="hidden_currencycode" value="" hidden="hidden"  /><input type="text" id="hidden_sendamountoverride" name="hidden_sendamountoverride" value="N" hidden="hidden"  /></td></tr>

				<tr>
					<td class="err_modal_general_green">
						<table class="tbl_width_420">
							<tr>
								<td>
									<table class="tbl_width_420">
										<tr>
											<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image15" runat="server" ImageUrl="~/images/paid.png" /></td>
											<td class="ag_transfer_labels" style="width:285px; vertical-align:middle;">TOTAL AUD (Funds to be sent + Service Charges</td>
											<td style="width:100px; vertical-align:middle;" class="ag_transfer_dollars"><div id="SM_div_sending"></div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table class="tbl_width_420">
										<tr>
											<td style="width:35px; vertical-align:middle;">&nbsp;</td>
											<td class="ag_transfer_labels" style="width:285px; vertical-align:middle;">TOTAL RECEIVE IN <span id="divCurrencySending">LKR&nbsp;</span></td>
											<td style="width:100px; vertical-align:middle;" class="ag_transfer_dollars"><div id="SM_div_lkrvalue"></div></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr><td>&nbsp;</td></tr>

				<tr id="TR_SM_NegativeSend"> <!-- TR to show only when sending money with a negative account balance -->
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="err_modal_general">
									<table class="tbl_width_440">
										<tr>
											<td id="SendWarningMessage">WARNING! This customer do not have sufficient funds for this transaction. If you would like to proceed please provide the following details.</td>
										</tr>
                                        
										<tr><td>&nbsp;</td></tr>
										<tr>
											<td>
											<table class="tbl_width_440">
												<tr id="WarningSection1TR">
													<td class="label__font">Admin code</td>
													<td class="label__font">&nbsp;</td>
													<td class="label__font">Reason/details</td>
												</tr>
												<tr id="WarningSection2TR">
													<td style="width:88px;"><div class="login__input"><input class="national_id_class modal__input__font" id="txt_AdminCode" style="width:100%;" type="password" value="" /></div></td>
													<td style="width:10px;">&nbsp;</td>
													<td style="width:462px;"><div class="login__input"><input class="relationship_class modal__input__font" id="txt_OverrideReason" style="width:100%;" type="text"  value=""  /></div></td>
												</tr>
											</table>
											</td>
										</tr>
									</table>
								</td>
								
							</tr>

							<tr>
								<td>&nbsp;</td>
							</tr>

						</table>
					</td>
				</tr>


                    <tr id="TR_SM_ExceedSend"> <!-- TR to show only when transaction exceeds monthly limit -->
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="err_modal_general">
									<table class="tbl_width_440">
										<tr>
											<td id="limitexceeded">WARNING! This transaction will exceed the allocated monthly limit for this customer. This transaction will be escalated to compliance for additional documentation.</td>
										</tr>
                                        
									</table>
								</td>
								
							</tr>

							<tr>
								<td>&nbsp;</td>
							</tr>

						</table>
					</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td><input class="btn_blue" id="SM_btn_Send" type="button" value="SEND"/> <input class="btn_red" id="SM_btn_Confirm" type="button" value="CONFIRM"/> <input class="btn_grey" id="SM_btn_Cancel" type="button" value="CANCEL" /></td>
							</tr>
						</table>
					</td>
				</tr>

			</table> 
			</form>
		</div>
	</div>
<!-- ************************* BUTTON 07 SEND MONEY MODAL POPUP END ************************* --> 







	<div id="DIV_Documents">
		<table class="tbl_width_600">

            <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_Documents_001"><asp:Image ID="Image19" runat="server" ImageUrl="~/images/id-card3.png" title="ID DOCUMENTS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_Documents_002"><asp:Image ID="Image20" runat="server" ImageUrl="~/images/folder2.png" title="OTHER DOCUMENTS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>
                
                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <!-- TAB01 - ID DOCUMENTS -->
                        <div id="DIV_DocumentsID">
                            <table class="tbl_width_560">
			                    <tr class="wiz_bg_EFEFEF">
				                    <td>
					                    <form id="form_DocUpload" method="get">
					                        <table class="tbl_width_560">
						                        
                                                <tr>
                                                    <td class="aa_label_font">TYPE OF IDENTITY DOCUMENT *</td>
						                        </tr>
						                        
                                                <tr>
                                                    <td><div class="login__input"><select id="typeofdocumentddl" style="width:100%;" required="required"></select></div></td>
						                        </tr>
						                        
                                                <tr><td class="spacer10">&nbsp;</td></tr>

						                        <tr>
							                        <td>
								                        <table>
									                        <tr>
										                        <td class="aa_label_font">DOC NUMBER *</td>
										                        <td class="aa_label_font">&nbsp;</td>
										                        <td class="aa_label_font">ISSUING AUTHORITY *</td>
										                        <td class="aa_label_font">&nbsp;</td>
										                        <td class="aa_label_font">EXPIRY DATE *</td>
										                        <td class="aa_label_font">&nbsp;</td>
										                        <td class="aa_label_font">&nbsp;</td>
									                        </tr>
									                        <tr>
										                        <td style="width:127px;"><div><input class="aa_input" id="documentnumbertxt" style="width:100%;" type="text" value="" required="required" /></div></td>
										                        <td style="width:18px;">&nbsp;</td>
										                        <td style="width:126px;"><div><input class="aa_input" id="issuingauthoritytxt" style="width:100%;" type="text" value="" required="required"/></div></td>
										                        <td style="width:18px;">&nbsp;</td>
										                        <td style="width:126px;"><div><input class="aa_input datePickerMonthReport" id="expirydatetxt" style="width:100%;" type="text" value="" readonly="readonly"  required="required" /></div></td>
										                        <td style="width:18px;"><input id="hdnpoints" type="text" name="hdnpoints" value="" hidden="hidden" /></td>
										                        <td style="width:127px; text-align:right; vertical-align:top;"><input type="file" name="UploadFile1" id="UploadFile1" class="btn_green_upload" style="display: none;" /><input type="button" class="aa_btn_green" value="Browse..." onclick="document.getElementById('UploadFile1').click();" /></td>
									                        </tr>
								                        </table>
							                        </td>
						                        </tr>

                                                <tr><td class="spacer10">&nbsp;</td></tr>

						                        <tr>
							                        <td class="trn_notes_popup_details" style="font-size:0.7em; color:#999;">Selected file name: <span style="color:#666;" id="selectedfilename"></span></td>
						                        </tr>
						                        
                                                <tr><td class="spacer10">&nbsp;</td></tr>

                                                <tr>
							                        <td><input class="aa_btn_red" id="btn_UploadFile" type="button" value="Upload File"/></td>
						                        </tr>
                                                <tr>
							                        <td>

                                                        <div id="divUpload" style="display: none">
                                                            <div style="width: 300pt; text-align: center;">
                                                                Uploading...</div>
                                                            <div style="width: 300pt; height: 20px; border: solid 1pt gray">
                                                                <div id="divProgress" style="width: 1pt; height: 20px; background-color: orange;
                                                                    display: none">
                                                                </div>
                                                            </div>
                                                            <div style="width: 300pt; text-align: center;">
                                                                <asp:Label ID="lblPercentage" runat="server" Text="Label"></asp:Label></div>
                                                            <br />
                                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                        </div>
							                        </td>
						                        </tr>
							
                                                <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                                <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

					                        </table>
						                </form>
				                    </td>
			                    </tr>

			                    <tr>
				                    <td>
					                    <table id="FileUploadMainTbl" class="tbl_width_560">
						                    <thead>
							                    <td>Doc type</td>
							                    <td>Number</td>
							                    <td>Authority</td>
							                    <td>Points</td>
							                    <td>Expiry</td>
							                    <td>View</td>
						                    </thead>
					                    </table>

				                    </td>
			                    </tr>

			                    <tr><td>&nbsp;</td></tr>

                                <tr><td>&nbsp;</td></tr>                                

                            </table>
                        </div>
                    </td>
                </tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <!-- TAB02 - OTHER DOCUMENTS -->
                        <div id="DIV_DocumentsOther">
                            <table class="tbl_width_560">
			                    <tr>
				                    <td>
					                    <form id="doc_UploadOtherDocs" method="get">
						                    <table class="tbl_width_560">
							                    
                                                <tr>
								                    <td>
									                    <table>
										                    <tr>
											                    <td class="aa_label_font">DOCUMENT DESCRIPTION *</td>
											                    <td class="aa_label_font">&nbsp;</td>
											                    <td class="aa_label_font">&nbsp;</td>
										                    </tr>
										                    <tr>
											                    <td style="width:415px;"><div><input class="aa_input" id="docdescription" name="docdescription" style="width:100%;" type="text" value="" /></div></td>
											                    <td style="width:18px;">&nbsp;</td>
											                    <td style="width:127px; text-align:right;"><input type="file" name="UploadFile2" id="UploadFile2" class="btn_green_upload" style="display: none;" /><input type="button" class="aa_btn_green" value="Browse..." onclick="document.getElementById('UploadFile2').click();" /></td>
										                    </tr>
									                    </table>
								                    </td>
							                    </tr>

                                                <tr><td class="spacer10">&nbsp;</td></tr>

							                    <tr>
								                    <td class="trn_notes_popup_details" style="font-size:0.7em; color:#999;">Selected file name: <span id="otherdoctext" style="color:#666;">Please browse for a file...</span></td>
							                    </tr>

							                    <tr><td class="spacer10">&nbsp;</td></tr>

							                    <tr>
								                    <td><input class="aa_btn_red" id="btn_UploadOtherFiles" type="button" value="Upload File"/></td>
							                    </tr>

							                    <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                                <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

						                    </table>
					                    </form>

				                    </td>
			                    </tr>

			<tr>
				<td>
					<table id="FileUploadOtherTbl" class="tbl_width_560">
						<thead>
							<td>Doc Name</td>
							<td>View</td>
						</thead>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td></tr>
                            </table>

                        </div>
                    </td>
                </tr>
            <tr class="spacer10"><td>&nbsp;</td></tr>

                        
                        
			
		</table>
	</div>

	<div id="DIV_Modal_CompanyVerified">
								<table style="width:600px;">
							<tr>
								<td>&nbsp;</td>
							</tr>

							<tr><td>
								<table class="tbl_width_560">
									<tr>
										<td>
											<input id="chk_CompanyVerified" type="checkbox" runat="server" /></td>
										<td>KASI verified customer. Verified by [name] on [date]</td>
									</tr>
								</table>
								</td></tr>
							<tr><td>&nbsp;</td></tr>
						</table>
	</div>


	<div id="DIV_Modal_KYC"> <!-- KYC STARTS HERE -->
		<div class="edit__form__main">
			<form method="get" id="kycaddform">
			<table class="style__width_600">
				
				<tr>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td class="label__font">Marital status</td>
								<td class="label__font">&nbsp;</td>
								<td class="label__font">Number of financial dependants</td>
							</tr>
							<tr>
								<td class="style__width_290"><div class="login__input">
									<select id="maritalstatus" style="width:290px;">
										<option value="">--SELECT--</option>
										<option value="Single">Single</option>
										<option value="Married">Married</option>
										<option value="Defacto">Defacto</option>
										<option value="Separated">Separated</option>
										<option value="Divorced">Divorced</option>
										<option value="Windowed">Windowed</option>
									</select>
									</div></td>
								<td class="style__width_20">&nbsp;</td>
								<td class="style__width_290"><div class="login__input"><select id="findep" style="width:290px;">
									<option value="">--SELECT--</option>
									<option value="1-One">1-One</option>
									<option value="2-Two">2-Two</option>
									<option value="3-Three">3-Three</option>
									<option value="4-Four">4-Four</option>
									<option value="5-Five">5-Five</option>
									<option value="6-Six">6-Six</option>
									<option value="7-Seven">7-Seven</option>
									<option value="8-Eight">8-Eight</option>
									<option value="9-Nine or more">9-Nine or more</option>
								</select></div></td>
							</tr>
						</table>
					</td>
				</tr>

			   <tr>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td class="label__font">Are you a permanent Australian resident?</td>
								<td class="label__font">&nbsp;</td>
								<td class="label__font">Residential status</td>
							</tr>
							<tr>
								<td class="style__width_290"><div class="login__input"><select id="AURes" style="width:290px;">
									<option value="">--SELECT--</option>
									<option value="YES">YES</option>
									<option value="NO">NO</option>
								 </select></div></td>
								<td class="style__width_20">&nbsp;</td>
								<td class="style__width_290"><div class="login__input"><select id="ResStat" style="width:290px;">
									<option value="">--SELECT--</option>
									<option value="Renting">Renting</option>
									<option value="Own home (no mortgage)">Own home (no mortgage)</option>
									<option value="Own home (mortgage)">Own home (mortgage)</option>
									<option value="Parent/Relative">Parent/Relative</option>
									<option value="Boarding">Boarding</option>
									<option value="Employer Supplied">Employer Supplied</option>
								</select></div></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10">
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td class="btm__line">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td class="label__font">Employment status</td>
								<td class="label__font">&nbsp;</td>
								<td class="label__font">Occupation/Job title</td>
							</tr>
							<tr>
								<td class="style__width_290"><div class="login__input"><select id="EmpStat" style="width:290px;">
									<option value="">--SELECT--</option>
									<option value="Full-time">Full-time</option>
									<option value="Part-time">Part-time</option>
									<option value="Self Employed">Self-Employed</option>
									<option value="Retired">Retired</option>
									<option value="Casual/Contractor">Causal/Contractor</option>
									<option value="Home Duties">Home Duties</option>
									<option value="Student">Student</option>
									<option value="Unemployed">Unemployed</option>
								 </select></div></td>
								<td class="style__width_20">&nbsp;</td>
								<td class="style__width_290"><div class="login__input"><input class="email2_class modal__input__font" style="width:100%;" type="text" value="" id="occupation"  /></div></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td class="spacer5">&nbsp;</td>
				</tr>

				<tr>
					<td>
						<table class="style__width_600">
							<tr>
								<td class="label__font">Salary (after tax) Net</td>
								<td class="label__font">&nbsp;</td>
								<td class="label__font">Frequency</td>
								<td class="label__font">&nbsp;</td>
								<td class="label__font">Do you have other income/s</td>
							</tr>
							<tr>
								<td class="style__width_135"><div class="login__input"><input class="suburb_class modal__input__font"  style="width:100%;" type="text" id="salary" value="" /></div></td>
								<td class="style__width_20">&nbsp;</td>
								<td class="style__width_135"><div class="login__input"><select id="frequency" style="width:135px;">
									<option value="">--SELECT--</option>
									<option value="Weekly">Weekly</option>
									<option value="Fortnightly">Fortnightly</option>
									<option value="Monthly">Monhtly</option>
									<option value="Yearly">Yearly</option>
								 </select></div></td>
								<td class="style__width_20">&nbsp;</td>
								<td class="style__width_290"><div class="login__input"><select id="otherincome" style="width:290px;" name="D1">
									<option value="">--SELECT--</option>
									<option value="Second job salary">Second job salary</option>
									<option value="Overtime">Overtime</option>
									<option value="Government Benefits">Government Benefits</option>
									<option value="Other">Other</option>
								</select></div></td>
							</tr>

						</table>
					</td>
				</tr>

				<tr class="spacer5">
					<td>
						&nbsp;
					</td>
				</tr>

				<tr>
					<td class="label__font">Comments</td>
				</tr>

				<tr>
					<td>
						<textarea id="comments" cols="20" rows="2" style="width:600px; resize:none;"></textarea>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>

				<tr style="background-color:#F4F2F2;">
					<td>
						<table class="tbl_width_560">
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td style="width:50px;"><input id="CHK_ConfirmInfo" type="checkbox" class="chk_confirm" /></td>
								<td style="width:510px;"class="fnt_chkbox">I confirm that the above information was given by customer <span id="SPN_ID" runat="server">ID</span> <span id="SPN_NAME" runat="server">NAME</span></td>
							</tr>
							<tr><td>&nbsp;</td></tr>
						</table>
						
					</td>
				</tr>
				
				<tr>
					<td>
						<table class="tbl_width_560"><tr><td id="TD_ExpInfo">&nbsp;</td></tr></table>
					</td>
				</tr>
				
				<tr style="height:20px;"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table>
							<tr>
								<td><input class="btn_disabled" id="btn_kyc_add_Submit" type="button" value="SAVE" disabled="disabled"/></td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
			
				</form>

		</div> <!-- KYC FORM END HERE -->
	</div>

	<div id="DIV_Loading"> <!-- Loading DIV -->
		<table class="tbl_width_120">
			<tr>
				<td style="text-align:center;">
					<asp:Image ID="Image3" runat="server" ImageUrl="~/images/loading.gif" Height="60px" Width="60px" />
				</td>
			</tr>
			<tr><td class="fnt_pleasewait">Please wait...</td></tr>
		</table>
	</div>

	<div id="DIV_Message_OK"> <!-- Message OK DIV -->
		<table class="tbl_width_280">
			<tr><td style="height:20px;">&nbsp;</td></tr>
            <tr><td style="text-align:center;"><asp:Image ID="OK_Tick" runat="server" ImageUrl="~/images/ok.png" title="COMPANY DETAILS" /></td></tr>
            <tr><td style="height:20px;">&nbsp;</td></tr>
            <tr><td class="msg_heading_font_ok">Success</td></tr>
			<tr>
				<td class="msg_contents_font_ok">
					<span id="msgtext">OK</span>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td style="text-align:center;"><button class="aa_btn_green" id="ok_button_dialog"><span>OK</span></button></td></tr>
            <tr><td>&nbsp;</td></tr>
		</table>
	</div>
    
    
    <div id="DIV_SendCustEmail">
        <table class="tbl_width_600">
            <tr><td>&nbsp;</td></tr>
            <tr class="wiz_bg_EFEFEF" style="border:1px solid #EFEFEF; height:20px;"><td>&nbsp;</td></tr>
            <tr class="wiz_bg_EFEFEF" style="border:1px solid #EFEFEF;">
                <td>
                    <table class="tbl_width_560">
                        <tr>
                            <td style="width:60px; text-align:center;" class="email_send_btn">
                                <table class="tbl_width_60">
                                    <tr><td style="text-align:center;"><asp:Image ID="Image23" runat="server" ImageUrl="~/images/send.png" /></td></tr>
                                    <tr><td>Send</td></tr>
                                </table>
                            </td>
                            <td style="width:10px;">&nbsp;</td>
                            <td style="vertical-align:top; width:80px;">
                                <table>
                                    <tr><td style="width:80px;" class="email_send_to_label">To...</td></tr>
                                    <tr class="spacer5"><td style="width:80px;">&nbsp;</td></tr>
                                    <tr><td style="width:80px;" class="email_send_to_label">CC...</td></tr>
                                    <tr class="spacer5"><td style="width:80px;" >&nbsp;</td></tr>
                                    <tr><td style="width:80px;" class="email_send_to_label">Subject</td></tr>
                                </table>
                            </td>
                            <td style="width:10px;">&nbsp;</td>
                            <td style="width:400px; vertical-align:top;">
                                <table style="vertical-align:top;">
                                    <tr><td style="width:400px;" class="email_send_to" id="EMAIL_addy">sajith.jayaratne@hotmail.com</td></tr>
                                    <tr class="spacer5"><td style="width:400px;">&nbsp;</td></tr>
                                    <tr><td style="width:400px;"><input class="aa_input email_send_cc" style="width:100%;" type="text" value="" id="emailcc"  /></td></tr>
                                    <tr class="spacer5"><td style="width:400px;">&nbsp;</td></tr>
                                    <tr><td class="email_send_to" style="width:400px;" id="EMAIL_Subject">Kapruka Pty Ltd Bank Account Details</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
            <tr style="border:1px solid #EFEFEF;">
                <td>
                    <table class="tbl_width_560">
                        <tr><td>&nbsp;</td></tr>
                        <tr><td class="email_send_font" id="EMAIL_FirstLine">Hello &lt;customer name&gt;,</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td class="email_send_font">Please find below our bank account details for your remittance needs.</td></tr>
                        <tr class="spacer5"><td>&nbsp;</td></tr>
                        <tr><td class="email_send_font" id="EMAIL_BankDetails">BSB: 063-987&nbsp;&nbsp; Account No: 1234567890 &nbsp;&nbsp; Name: Kapruka FW Account &nbsp;&nbsp; Bank: NAB</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr><td><textarea name="TXT_Add_TRN_Notes" id="EMAIL_CustomMessage" class="aa_input email_send_custom_msg" placeholder="Add a custom message here..."></textarea></td></tr>
                                </table>
                            </td>
                        </tr>

                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td class="email_send_font">Should you have any queries, or require further information, please do not hesitate to contact us.</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td class="email_send_font" id="EMAIL_SendingAgentName">Kapruka Pty Ltd</td></tr>
                        <tr><td class="email_send_font" id="EMAIL_SendingAgentAddress">U1/1321-1324 Centre Road, Clayton South VIC 3169</td></tr>
                        <tr><td class="email_send_font" id="EMAIL_SendingAgentPhone">03 4934 5678</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>

                    </table>
                </td>
            </tr>
            <tr class="spacer10"><td>&nbsp;</td></tr>
        </table>
    </div>
    
    
        </form>
    <!-- ************************* BUTTON 01 EDIT CUSTOMER MODAL POPUP START ************************* -->    
	<div id="EditCustomerDiv">
		<div class="edit__form__main">
			<form id="formEditCust">
			<table class="tbl_width_600">
				
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_EditBusiness_001"><asp:Image ID="Image21" runat="server" ImageUrl="~/images/company.png" title="COMPANY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_EditBusiness_002"><asp:Image ID="Image25" runat="server" ImageUrl="~/images/user.png" title="AUTHORIZED CONTACT" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_EditCustomer_003"><asp:Image ID="Image12" runat="server" ImageUrl="~/images/paper.png" title="NOTES & PASSWORD" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_EditCustomerDetails">
                                
                                     <input type="hidden" id="bcountry" name="bcountry" value="" />
                <input type="hidden" id="bnationality" name="bnationality" value="" />
                                    <input type="hidden" id="hidCustId" name="hidCustId" />
                                    <input type="hidden" id="isbusi" name="isbusi" />
                                    <input type="hidden" id="type" name="type" value="update" />
            <table class="tbl_width_560"  id="divBusiness" >
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">FULL BUSINESS NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ABN/ACN *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_393"><div><input class="aa_input" id="BBusinessName" name="BBusinessName" required style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;"></td>
                                                <td class="style__width_186"><div><input class="aa_input" id="BABN" name="BABN" style="width:100%;" required type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">TRADING NAME (IF APPLICABLE)</td>
                                               
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="BTradingName" name="BTradingName" style="width:100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">Busi. Contct</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">Busi. Email</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">Busi. Web site</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BBContact" name="BBContact" style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BBEmail" name="BBEmail" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BBWeb" name="BBWeb" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font_heading">PRINCIPLE PLACE OF BUSINESS (PO BOX IS NOT ACCEPTABLE)</td>
                                            </tr>
                                            <tr hidden="hidden">
                                                <td><div><input class="aa_input" id="BAddressLine1" name="BAddressLine1" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">UNIT NO</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NO *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET TYPE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_50"><div><input class="aa_input" id="bus_unitno" name="bus_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td class="style__width_10">&nbsp;</td> 
                                                <td class="style__width_75"><div><input class="aa_input" required id="bus_streetno" name="bus_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_290"><div><input class="aa_input" required id="bus_streetname" name="bus_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                                        <select id="bus_streettype" name="bus_streettype" required style="width:100%;">
                                                            <option value="0">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_600"><div><input class="aa_input" id="BAddressLine2" name="BAddressLine2" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input required class="aa_input" id="bus_suburb" name="bus_suburb" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input required class="aa_input" id="bus_state" name="bus_state" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input required class="aa_input" id="bus_postcode" name="bus_postcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BSuburb" name="BSuburb" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BState" name="BState" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostcode" name="BPostcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="bus_country" name="bus_country" style="width:100%;" type="text" value="Australia"  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                 <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font_heading">POSTAL ADDRESS (IF DIFFERENT FROM ABOVE)</td>
                                            </tr>
                                            <tr hidden="hidden">
                                                <td><div><input class="aa_input" id="BPostalAddressLine1" name="BPostalAddressLine1" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">UNIT NO</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NO *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET TYPE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_50"><div><input class="aa_input" id="pos_unitno" name="pos_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td class="style__width_10">&nbsp;</td>
                                                <td class="style__width_75"><div><input required class="aa_input" id="pos_streetno" name="pos_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_290"><div><input required class="aa_input" id="pos_streetname" name="pos_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                                        <select id="pos_streettype" name="pos_streettype" required style="width:100%;">
                                                             <option value="0">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">POSTAL ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="BPostalAddressLine2" name="BPostalAddressLine2" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BPostalSuburb" name="BPostalSuburb" style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostalState" name="BPostalState" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostalPostcode" name="BPostalPostcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="pos_country" name="pos_country" style="width:100%;" type="text" value="Australia"  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>
                                            
                            </table>
          
                           <table class="tbl_width_560" id="tblIndi">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">

                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="aa_label_font">LAST NAME *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">TITLE *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">GIVEN NAME/S *</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style__width_186"><div><input class="aa_input" required id="bus_lastname" name="bus_lastname" style="width:100%;" type="text" value="" /></div></td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td style="width:83px;">
                                                                <div>
                                                                    <select id="bus_title" name="bus_title" required style="width:100%;">
                                                                        <option value="0">--Select--</option>
	                                                                    <option value="Mr">Mr</option>
	                                                                    <option value="Mrs">Mrs</option>
	                                                                    <option value="Miss">Miss</option>
	                                                                    <option value="Ms">Ms</option>
	                                                                    <option value="Dr">Dr</option>
	                                                                    <option value="Other">Other</option>
                                                                    </select>                                   
                                                                </div>
                                                            </td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td style="width:290px;"><div><input class="aa_input" required id="bus_givenNames" name="bus_givenNames" style="width:100%;" type="text"  value=""  /></div></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="aa_label_font">COUNTRY OF BIRTH *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">DATE OF BIRTH (DOB) *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">PLACE OF BIRTH *</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style__width_186">
                                                                <div>
                                                                    <select id="bus_countryofbirth" name="bus_countryofbirth" required style="width:100%;">
	                                                                    <option selected="selected" value="0">--Select--</option>
	                                                                    <option value="1">Afghanistan</option>
	                                                                    <option value="2">Albania</option>
	                                                                    <option value="3">Algeria</option>
	                                                                    <option value="4">Andorra</option>
	                                                                    <option value="5">Angola</option>
	                                                                    <option value="6">Antigua and Barbuda</option>
	                                                                    <option value="7">Argentina</option>
	                                                                    <option value="8">Armenia</option>
	                                                                    <option value="9">Australia</option>
	                                                                    <option value="10">Austria</option>
	                                                                    <option value="11">Azerbaijan</option>
	                                                                    <option value="12">Bahamas</option>
	                                                                    <option value="13">Bahrain</option>
	                                                                    <option value="14">Bangladesh</option>
	                                                                    <option value="15">Barbados</option>
	                                                                    <option value="16">Belarus</option>
	                                                                    <option value="17">Belgium</option>
	                                                                    <option value="18">Belize</option>
	                                                                    <option value="19">Benin</option>
	                                                                    <option value="20">Bhutan</option>
	                                                                    <option value="21">Bolivia</option>
	                                                                    <option value="22">Bosnia and Herzegovina</option>
	                                                                    <option value="23">Botswana</option>
	                                                                    <option value="24">Brazil</option>
	                                                                    <option value="25">Brunei</option>
	                                                                    <option value="26">Bulgaria</option>
	                                                                    <option value="27">Burkina Faso</option>
	                                                                    <option value="28">Burundi</option>
	                                                                    <option value="29">Cambodia</option>
	                                                                    <option value="30">Cameroon</option>
	                                                                    <option value="31">Canada</option>
	                                                                    <option value="32">Cape Verde</option>
	                                                                    <option value="33">Central African Republic</option>
	                                                                    <option value="34">Chad</option>
	                                                                    <option value="35">Chile</option>
	                                                                    <option value="36">China</option>
	                                                                    <option value="37">Colombia</option>
	                                                                    <option value="38">Comoros</option>
	                                                                    <option value="39">Congo (Kinshasa)</option>
	                                                                    <option value="40">Congo (Brazzaville)</option>
	                                                                    <option value="41">Costa Rica</option>
	                                                                    <option value="42">Cote d Ivoire (Ivory Coast)</option>
	                                                                    <option value="43">Croatia</option>
	                                                                    <option value="44">Cuba</option>
	                                                                    <option value="45">Cyprus</option>
	                                                                    <option value="46">Czech Republic</option>
	                                                                    <option value="47">Denmark</option>
	                                                                    <option value="48">Djibouti</option>
	                                                                    <option value="49">Dominica</option>
	                                                                    <option value="50">Dominican Republic</option>
	                                                                    <option value="51">Ecuador</option>
	                                                                    <option value="52">Egypt</option>
	                                                                    <option value="53">El Salvador</option>
	                                                                    <option value="54">Equatorial Guinea</option>
	                                                                    <option value="55">Eritrea</option>
	                                                                    <option value="56">Estonia</option>
	                                                                    <option value="57">Ethiopia</option>
	                                                                    <option value="58">Fiji</option>
	                                                                    <option value="59">Finland</option>
	                                                                    <option value="60">France</option>
	                                                                    <option value="61">Gabon</option>
	                                                                    <option value="62">Gambia</option>
	                                                                    <option value="63">Georgia</option>
	                                                                    <option value="64">Germany</option>
	                                                                    <option value="65">Ghana</option>
	                                                                    <option value="66">Greece</option>
	                                                                    <option value="67">Grenada</option>
	                                                                    <option value="68">Guatemala</option>
	                                                                    <option value="69">Guinea</option>
	                                                                    <option value="70">Guinea-Bissau</option>
	                                                                    <option value="71">Guyana</option>
	                                                                    <option value="72">Haiti</option>
	                                                                    <option value="73">Honduras</option>
	                                                                    <option value="74">Hungary</option>
	                                                                    <option value="75">Iceland</option>
	                                                                    <option value="76">India</option>
	                                                                    <option value="77">Indonesia</option>
	                                                                    <option value="78">Iran</option>
	                                                                    <option value="79">Iraq</option>
	                                                                    <option value="80">Ireland</option>
	                                                                    <option value="81">Israel</option>
	                                                                    <option value="82">Italy</option>
	                                                                    <option value="83">Jamaica</option>
	                                                                    <option value="84">Japan</option>
	                                                                    <option value="85">Jordan</option>
	                                                                    <option value="86">Kazakhstan</option>
	                                                                    <option value="87">Kenya</option>
	                                                                    <option value="88">Kiribati</option>
	                                                                    <option value="89">Korea (North)</option>
	                                                                    <option value="90">Korea (South)</option>
	                                                                    <option value="91">Kuwait</option>
	                                                                    <option value="92">Kyrgyzstan</option>
	                                                                    <option value="93">Laos</option>
	                                                                    <option value="94">Latvia</option>
	                                                                    <option value="95">Lebanon</option>
	                                                                    <option value="96">Lesotho</option>
	                                                                    <option value="97">Liberia</option>
	                                                                    <option value="98">Libya</option>
	                                                                    <option value="99">Liechtenstein</option>
	                                                                    <option value="100">Lithuania</option>
	                                                                    <option value="101">Luxembourg</option>
	                                                                    <option value="102">Macedonia</option>
	                                                                    <option value="103">Madagascar</option>
	                                                                    <option value="104">Malawi</option>
	                                                                    <option value="105">Malaysia</option>
	                                                                    <option value="106">Maldives</option>
	                                                                    <option value="107">Mali</option>
	                                                                    <option value="108">Malta</option>
	                                                                    <option value="109">Marshall Islands</option>
	                                                                    <option value="110">Mauritania</option>
	                                                                    <option value="111">Mauritius</option>
	                                                                    <option value="112">Mexico</option>
	                                                                    <option value="113">Micronesia</option>
	                                                                    <option value="114">Moldova</option>
	                                                                    <option value="115">Monaco</option>
	                                                                    <option value="116">Mongolia</option>
	                                                                    <option value="117">Montenegro</option>
	                                                                    <option value="118">Morocco</option>
	                                                                    <option value="119">Mozambique</option>
	                                                                    <option value="120">Myanmar (Burma)</option>
	                                                                    <option value="121">Namibia</option>
	                                                                    <option value="122">Nauru</option>
	                                                                    <option value="123">Nepal</option>
	                                                                    <option value="124">Netherlands</option>
	                                                                    <option value="125">New Zealand</option>
	                                                                    <option value="126">Nicaragua</option>
	                                                                    <option value="127">Niger</option>
	                                                                    <option value="128">Nigeria</option>
	                                                                    <option value="129">Norway</option>
	                                                                    <option value="130">Oman</option>
	                                                                    <option value="131">Pakistan</option>
	                                                                    <option value="132">Palau</option>
	                                                                    <option value="133">Panama</option>
	                                                                    <option value="134">Papua New Guinea</option>
	                                                                    <option value="135">Paraguay</option>
	                                                                    <option value="136">Peru</option>
	                                                                    <option value="137">Philippines</option>
	                                                                    <option value="138">Poland</option>
	                                                                    <option value="139">Portugal</option>
	                                                                    <option value="140">Qatar</option>
	                                                                    <option value="141">Romania</option>
	                                                                    <option value="142">Russia</option>
	                                                                    <option value="143">Rwanda</option>
	                                                                    <option value="144">Saint Kitts and Nevis</option>
	                                                                    <option value="145">Saint Lucia</option>
	                                                                    <option value="147">Samoa</option>
	                                                                    <option value="148">San Marino</option>
	                                                                    <option value="149">Sao Tome and Principe</option>
	                                                                    <option value="150">Saudi Arabia</option>
	                                                                    <option value="151">Senegal</option>
	                                                                    <option value="152">Serbia</option>
	                                                                    <option value="153">Seychelles</option>
	                                                                    <option value="154">Sierra Leone</option>
	                                                                    <option value="155">Singapore</option>
	                                                                    <option value="156">Slovakia</option>
	                                                                    <option value="157">Slovenia</option>
	                                                                    <option value="158">Solomon Islands</option>
	                                                                    <option value="159">Somalia</option>
	                                                                    <option value="160">South Africa</option>
	                                                                    <option value="161">Spain</option>
	                                                                    <option value="162">Sri Lanka</option>
	                                                                    <option value="163">Sudan</option>
	                                                                    <option value="164">Suriname</option>
	                                                                    <option value="165">Swaziland</option>
	                                                                    <option value="166">Sweden</option>
	                                                                    <option value="167">Switzerland</option>
	                                                                    <option value="168">Syria</option>
	                                                                    <option value="169">Tajikistan</option>
	                                                                    <option value="170">Tanzania</option>
	                                                                    <option value="171">Thailand</option>
	                                                                    <option value="172">Timor-Leste (East Timor)</option>
	                                                                    <option value="173">Togo</option>
	                                                                    <option value="174">Tonga</option>
	                                                                    <option value="175">Trinidad and Tobago</option>
	                                                                    <option value="176">Tunisia</option>
	                                                                    <option value="177">Turkey</option>
	                                                                    <option value="178">Turkmenistan</option>
	                                                                    <option value="179">Tuvalu</option>
	                                                                    <option value="180">Uganda</option>
	                                                                    <option value="181">Ukraine</option>
	                                                                    <option value="182">United Arab Emirates</option>
	                                                                    <option value="183">United Kingdom</option>
	                                                                    <option value="184">United States</option>
	                                                                    <option value="185">Uruguay</option>
	                                                                    <option value="186">Uzbekistan</option>
	                                                                    <option value="187">Vanuatu</option>
	                                                                    <option value="188">Vatican City</option>
	                                                                    <option value="189">Venezuela</option>
	                                                                    <option value="190">Vietnam</option>
	                                                                    <option value="191">Yemen</option>
	                                                                    <option value="192">Zambia</option>
	                                                                    <option value="193">Zimbabwe</option>
	                                                                    <option value="194">Abkhazia</option>
	                                                                    <option value="195">Taiwan</option>
	                                                                    <option value="197">Northern Cyprus</option>
	                                                                    <option value="198">Pridnestrovie (Transnistria)</option>
	                                                                    <option value="200">South Ossetia</option>
	                                                                    <option value="202">Christmas Island</option>
	                                                                    <option value="203">Cocos (Keeling) Islands</option>
	                                                                    <option value="206">Norfolk Island</option>
	                                                                    <option value="207">New Caledonia</option>
	                                                                    <option value="208">French Polynesia</option>
	                                                                    <option value="209">Mayotte</option>
	                                                                    <option value="210">Saint Barthelemy</option>
	                                                                    <option value="211">Saint Martin</option>
	                                                                    <option value="212">Saint Pierre and Miquelon</option>
	                                                                    <option value="213">Wallis and Futuna</option>
	                                                                    <option value="216">Bouvet Island</option>
	                                                                    <option value="217">Cook Islands</option>
	                                                                    <option value="218">Niue</option>
	                                                                    <option value="219">Tokelau</option>
	                                                                    <option value="220">Guernsey</option>
	                                                                    <option value="221">Isle of Man</option>
	                                                                    <option value="222">Jersey</option>
	                                                                    <option value="223">Anguilla</option>
	                                                                    <option value="224">Bermuda</option>
	                                                                    <option value="227">British Virgin Islands</option>
	                                                                    <option value="228">Cayman Islands</option>
	                                                                    <option value="230">Gibraltar</option>
	                                                                    <option value="231">Montserrat</option>
	                                                                    <option value="232">Pitcairn Islands</option>
	                                                                    <option value="233">Saint Helena</option>
	                                                                    <option value="235">Turks and Caicos Islands</option>
	                                                                    <option value="236">Northern Mariana Islands</option>
	                                                                    <option value="237">Puerto Rico</option>
	                                                                    <option value="238">American Samoa</option>
	                                                                    <option value="240">Guam</option>
	                                                                    <option value="248">U.S. Virgin Islands</option>
	                                                                    <option value="250">Hong Kong</option>
	                                                                    <option value="251">Macau</option>
	                                                                    <option value="252">Faroe Islands</option>
	                                                                    <option value="253">Greenland</option>
	                                                                    <option value="254">French Guiana</option>
	                                                                    <option value="255">Guadeloupe</option>
	                                                                    <option value="256">Martinique</option>
	                                                                    <option value="257">Reunion</option>
	                                                                    <option value="258">Aland Islands</option>
	                                                                    <option value="259">Aruba</option>
	                                                                    <option value="260">Netherlands Antilles</option>
	                                                                    <option value="261">Svalbard</option>
	                                                                    <option value="264">Antarctica Territories</option>
	                                                                    <option value="265">Kosovo</option>
	                                                                    <option value="266">Palestinian Territories</option>
	                                                                    <option value="267">Western Sahara</option>
	                                                                    <option value="273">Sint Maarten</option>
	                                                                    <option value="274">South Sudan</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td class="style__width_186"><div><input required class="aa_input datePickerDOB" id="bus_dob" name="bus_dob" placeholder="DD/MM/YYYY" style="width:100%;" type="text" readonly="readonly" value="" /></div></td>
                                                            <td style="width:18px;"class="style__width_21">&nbsp;</td>
                                                            <td class="style__width_186"><div><input required class="aa_input" id="bus_placeofbirth" name="bus_placeofbirth" style="width:100%;" type="text" value=""  /></div></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                    <table class="tbl_width_560">
                        <tr>
                            <td class="aa_label_font">NATIONALITY *</td>
                            <td class="aa_label_font">&nbsp;</td>
                            <td class="aa_label_font">ALSO KNOWN AS (AKA)</td>
                        </tr>
                        <tr>
                            <td class="style__width_186">
                                <div>
                                    <select id="bus_nationality" name="bus_nationality" required style="width:100%;">
	                                    <option selected="selected" value="0">--Select--</option>
	                                    <option value="1">Afghani</option>
	                                    <option value="2">Albanian</option>
	                                    <option value="3">Algerian</option>
	                                    <option value="4">American</option>
	                                    <option value="5">Andorran</option>
	                                    <option value="6">Angolan</option>
	                                    <option value="202">Anguillan</option>
	                                    <option value="203">Antarctic</option>
	                                    <option value="7">Antiguans</option>
	                                    <option value="8">Argentinean</option>
	                                    <option value="9">Armenian</option>
	                                    <option value="204">Arubian</option>
	                                    <option value="10">Australian</option>
	                                    <option value="11">Austrian</option>
	                                    <option value="12">Azerbaijani</option>
	                                    <option value="208">Bahameese</option>
	                                    <option value="13">Bahamian</option>
	                                    <option value="14">Bahraini</option>
	                                    <option value="15">Bangladeshi</option>
	                                    <option value="16">Barbadian</option>
	                                    <option value="17">Barbudans</option>
	                                    <option value="206">Barthlemois</option>
	                                    <option value="19">Belarusian</option>
	                                    <option value="20">Belgian</option>
	                                    <option value="21">Belizean</option>
	                                    <option value="22">Beninese</option>
	                                    <option value="207">Bermudan</option>
	                                    <option value="23">Bhutanese</option>
	                                    <option value="24">Bolivian</option>
	                                    <option value="25">Bosnian</option>
	                                    <option value="26">Brazilian</option>
	                                    <option value="27">British</option>
	                                    <option value="28">Bruneian</option>
	                                    <option value="29">Bulgarian</option>
	                                    <option value="30">Burkinabe</option>
	                                    <option value="31">Burmese</option>
	                                    <option value="32">Burundian</option>
	                                    <option value="33">Cambodian</option>
	                                    <option value="34">Cameroonian</option>
	                                    <option value="35">Canadian</option>
	                                    <option value="36">Cape Verdean</option>
	                                    <option value="224">Caymanian</option>
	                                    <option value="37">Central African</option>
	                                    <option value="38">Chadian</option>
	                                    <option value="39">Chilean</option>
	                                    <option value="40">Chinese</option>
	                                    <option value="212">Christmas Islander</option>
	                                    <option value="209">Cocossian</option>
	                                    <option value="41">Colombian</option>
	                                    <option value="42">Comoran</option>
	                                    <option value="43">Congolese</option>
	                                    <option value="210">Cook Islander</option>
	                                    <option value="45">Costa Rican</option>
	                                    <option value="46">Croatian</option>
	                                    <option value="47">Cuban</option>
	                                    <option value="211">Curaaoan</option>
	                                    <option value="48">Cypriot</option>
	                                    <option value="49">Czech</option>
	                                    <option value="50">Danish</option>
	                                    <option value="51">Djiboutian</option>
	                                    <option value="52">Dominican</option>
	                                    <option value="54">Dutch</option>
	                                    <option value="56">Dutchwoman</option>
	                                    <option value="58">Ecuadorean</option>
	                                    <option value="59">Egyptian</option>
	                                    <option value="60">Emirian</option>
	                                    <option value="61">Equatorial Guinean</option>
	                                    <option value="62">Eritrean</option>
	                                    <option value="63">Estonian</option>
	                                    <option value="64">Ethiopian</option>
	                                    <option value="214">Falkland Islander</option>
	                                    <option value="215">Faroese</option>
	                                    <option value="65">Fijian</option>
	                                    <option value="66">Filipino</option>
	                                    <option value="67">Finnish</option>
	                                    <option value="68">French</option>
	                                    <option value="216">French Guianese</option>
	                                    <option value="233">French Polynesian</option>
	                                    <option value="69">Gabonese</option>
	                                    <option value="70">Gambian</option>
	                                    <option value="71">Georgian</option>
	                                    <option value="72">German</option>
	                                    <option value="73">Ghanaian</option>
	                                    <option value="217">Gibralterian</option>
	                                    <option value="74">Greek</option>
	                                    <option value="218">Greenlander</option>
	                                    <option value="75">Grenadian</option>
	                                    <option value="219">Guadeloupean</option>
	                                    <option value="220">Guamanian</option>
	                                    <option value="76">Guatemalan</option>
	                                    <option value="77">Guinean</option>
	                                    <option value="79">Guyanese</option>
	                                    <option value="80">Haitian</option>
	                                    <option value="81">Herzegovinian</option>
	                                    <option value="82">Honduran</option>
	                                    <option value="221">Hong Konger</option>
	                                    <option value="83">Hungarian</option>
	                                    <option value="84">I-Kiribati</option>
	                                    <option value="85">Icelander</option>
	                                    <option value="86">Indian</option>
	                                    <option value="87">Indonesian</option>
	                                    <option value="88">Iranian</option>
	                                    <option value="89">Iraqi</option>
	                                    <option value="91">Irish</option>
	                                    <option value="92">Israeli</option>
	                                    <option value="93">Italian</option>
	                                    <option value="94">Ivorian</option>
	                                    <option value="95">Jamaican</option>
	                                    <option value="96">Japanese</option>
	                                    <option value="97">Jordanian</option>
	                                    <option value="98">Kazakhstani</option>
	                                    <option value="99">Kenyan</option>
	                                    <option value="100">Kittian and Nevisian</option>
	                                    <option value="101">Kuwaiti</option>
	                                    <option value="102">Kyrgyz</option>
	                                    <option value="223">Kyrgyzstani</option>
	                                    <option value="103">Laotian</option>
	                                    <option value="104">Latvian</option>
	                                    <option value="105">Lebanese</option>
	                                    <option value="106">Liberian</option>
	                                    <option value="107">Libyan</option>
	                                    <option value="108">Liechtensteiner</option>
	                                    <option value="109">Lithuanian</option>
	                                    <option value="110">Luxembourger</option>
	                                    <option value="226">Macanese</option>
	                                    <option value="111">Macedonian</option>
	                                    <option value="246">Mahoran</option>
	                                    <option value="112">Malagasy</option>
	                                    <option value="113">Malawian</option>
	                                    <option value="114">Malaysian</option>
	                                    <option value="115">Maldivan</option>
	                                    <option value="116">Malian</option>
	                                    <option value="117">Maltese</option>
	                                    <option value="222">Manx</option>
	                                    <option value="118">Marshallese</option>
	                                    <option value="228">Martinican</option>
	                                    <option value="119">Mauritanian</option>
	                                    <option value="120">Mauritian</option>
	                                    <option value="121">Mexican</option>
	                                    <option value="122">Micronesian</option>
	                                    <option value="123">Moldovan</option>
	                                    <option value="124">Monacan</option>
	                                    <option value="125">Mongolian</option>
	                                    <option value="225">Montenegrin</option>
	                                    <option value="229">Montserratian</option>
	                                    <option value="126">Moroccan</option>
	                                    <option value="127">Mosotho</option>
	                                    <option value="18">Motswana</option>
	                                    <option value="128">Motswana</option>
	                                    <option value="129">Mozambican</option>
	                                    <option value="130">Namibian</option>
	                                    <option value="131">Nauruan</option>
	                                    <option value="132">Nepalese</option>
	                                    <option value="230">New Caledonian</option>
	                                    <option value="134">New Zealander</option>
	                                    <option value="135">Ni-Vanuatu</option>
	                                    <option value="136">Nicaraguan</option>
	                                    <option value="137">Nigerian</option>
	                                    <option value="138">Nigerien</option>
	                                    <option value="232">Niuean</option>
	                                    <option value="231">Norfolk Islander</option>
	                                    <option value="139">North Korean</option>
	                                    <option value="140">Northern Irish</option>
	                                    <option value="227">Northern Mariana Islander</option>
	                                    <option value="141">Norwegian</option>
	                                    <option value="142">Omani</option>
	                                    <option value="143">Pakistani</option>
	                                    <option value="144">Palauan</option>
	                                    <option value="237">Palestinian</option>
	                                    <option value="145">Panamanian</option>
	                                    <option value="146">Papua New Guinean</option>
	                                    <option value="147">Paraguayan</option>
	                                    <option value="148">Peruvian</option>
	                                    <option value="235">Pitcairn Islander</option>
	                                    <option value="149">Polish</option>
	                                    <option value="150">Portuguese</option>
	                                    <option value="236">Puerto Rican</option>
	                                    <option value="151">Qatari</option>
	                                    <option value="152">Romanian</option>
	                                    <option value="153">Russian</option>
	                                    <option value="154">Rwandan</option>
	                                    <option value="238">Saint Helenian</option>
	                                    <option value="155">Saint Lucian</option>
	                                    <option value="242">Saint Vincentian</option>
	                                    <option value="234">Saint-Pierrais</option>
	                                    <option value="156">Salvadoran</option>
	                                    <option value="157">Samoan</option>
	                                    <option value="158">San Marinese</option>
	                                    <option value="159">Sao Tomean</option>
	                                    <option value="160">Saudi</option>
	                                    <option value="161">Scottish</option>
	                                    <option value="162">Senegalese</option>
	                                    <option value="163">Serbian</option>
	                                    <option value="164">Seychellois</option>
	                                    <option value="165">Sierra Leonean</option>
	                                    <option value="166">Singaporean</option>
	                                    <option value="167">Slovakian</option>
	                                    <option value="168">Slovenian</option>
	                                    <option value="169">Solomon Islander</option>
	                                    <option value="170">Somali</option>
	                                    <option value="171">South African</option>
	                                    <option value="172">South Korean</option>
	                                    <option value="173">Spanish</option>
	                                    <option value="174">Sri Lankan</option>
	                                    <option value="175">Sudanese</option>
	                                    <option value="176">Surinamer</option>
	                                    <option value="177">Swazi</option>
	                                    <option value="178">Swedish</option>
	                                    <option value="179">Swiss</option>
	                                    <option value="180">Syrian</option>
	                                    <option value="181">Taiwanese</option>
	                                    <option value="182">Tajik</option>
	                                    <option value="183">Tanzanian</option>
	                                    <option value="184">Thai</option>
	                                    <option value="247">Tibetan </option>
	                                    <option value="57">Timorese</option>
	                                    <option value="185">Togolese</option>
	                                    <option value="240">Tokelauan</option>
	                                    <option value="186">Tongan</option>
	                                    <option value="187">Trinidadian or Tobagonian</option>
	                                    <option value="188">Tunisian</option>
	                                    <option value="189">Turkish</option>
	                                    <option value="241">Turkmen</option>
	                                    <option value="239">Turks and Caicos Islander</option>
	                                    <option value="190">Tuvaluan</option>
	                                    <option value="191">Ugandan</option>
	                                    <option value="192">Ukrainian</option>
	                                    <option value="193">Uruguayan</option>
	                                    <option value="194">Uzbekistani</option>
	                                    <option value="195">Venezuelan</option>
	                                    <option value="196">Vietnamese</option>
	                                    <option value="243">Virgin Islander-UK</option>
	                                    <option value="244">Virgin Islander-US</option>
	                                    <option value="245">Wallisian</option>
	                                    <option value="198">Welsh</option>
	                                    <option value="213">Western Saharan</option>
	                                    <option value="199">Yemenese</option>
	                                    <option value="200">Zambian</option>
	                                    <option value="201">Zimbabwean</option>
	                                    <option value="205">landic</option>
                                    </select>
                                </div>
                            </td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="style__width_393"><div><input class="aa_input" id="bus_aka" name="bus_aka" style="width:100%;" type="text"  value="" /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">OCCUPATION</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="bus_occupation" name="bus_occupation" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                    <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                    <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>

                <tr style="background-color:#EFEFEF;" hidden="hidden">
                    <td>
                    <table class="tbl_width_560" >
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td class="aa_label_font">CURRENT ADDRESS (Type address here to auto fill)</td>
                        </tr>
                        <tr hidden="hidden">
                            <td class="style__width_600"><div><input class="aa_input" id="bus_full_address" name="bus_full_address" style="width:100%;" type="text"  value="" <!--onFocus="geolocate()-->"></input></div></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                    </td>
                </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">UNIT NO</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET NO *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET NAME *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET TYPE *</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_50"><div><input class="aa_input" id="pbus_unitno" name="pbus_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                            <td class="style__width_10">&nbsp;</td>
                                            <td class="style__width_75"><div><input class="aa_input" required id="pbus_streetno" name="pbus_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" required id="pbus_streetname" name="pbus_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135">
                                                <div>
                                        
                                                    <select id="pbus_streettype" name="pbus_streettype" required style="width:100%;">
                                                         <option value="0">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr hidden="hidden">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">ADDRESS LINE 1</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">ADDRESS LINE 2</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_route" name="pbus_route" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_addressline2" name="pbus_addressline2" style="width:100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">SUBURB *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STATE *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">POSTCODE *</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input required class="aa_input" id="ppbus_suburb" name="ppbus_suburb" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input required class="aa_input" id="ppbus_state" name="ppbus_state" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input required class="aa_input" id="ppbus_postcode" name="ppbus_postcode" style="width:100%;" type="text" value=""  /></div></td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">COUNTRY</td>
                                        </tr>
                                        <tr>
                                            <td><div><input class="aa_input" id="pbus_country" name="pbus_country" style="width:100%;" type="text" value="Australia"  /></div></td>
                                        </tr>
                                        <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                        <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">HOME PHONE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">WORK PHONE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">MOBILE PHONE *</td>
                                        </tr>
                                        <tr>
                                            <td style="width:175px;"><input class="aa_input" id="pbus_phonehome" name="pbus_phonehome" style="width:100%;" type="text"/></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:174px;"><input class="aa_input" id="pbus_phonework" name="pbus_phonework" style="width:100%;" type="text"/></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:175px;"><input required class="aa_input" id="pbus_phonemobile" name="pbus_phonemobile" style="width:100%;" type="text"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">EMAIL ADDRESS 1 *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">EMAIL ADDRESS 2</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input required class="aa_input" id="pbus_email1" name="pbus_email1" style="width:100%;" type="email" value="" /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_email2" name="pbus_email2" style="width:100%;" type="email" value=""  /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr style="height:20px;"><td class="spacer10">&nbsp;</td></tr>

                        </table>
                    </td>
                </tr>
                                </table>
                                   
                            </div>
                    </td>
                </tr>

                <!-- NOTES & PW DIV -->
                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_EditCustomerNotesPW">
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">

                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_520">
                                                        <tr><td>&nbsp;</td></tr>
                                                        <tr><td class="trn_notes_popup_heading">Notes</td></tr>
                                                        <tr><td class="trn_notes_popup_details" style="font-size:0.65em; color:#999;">These notes will appear at the top of the Customer Profile Page. This will only be visible to the internal staff.</td></tr>
                                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                                        <tr><td class="trn_notes_popup_heading">Password</td></tr>
                                                        <tr><td class="trn_notes_popup_details" style="font-size:0.65em; color:#999;">You can assign a password for the customer Profile if requested by the customer. If there is a password this will appear at the top of the Customer Profile Page. </td></tr>
                                                        <tr><td>&nbsp;</td></tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr><td>&nbsp;</td></tr>

                                            <tr>
								                <td class="aa_label_font">NOTES</td>
							                </tr>
                                            <tr>
                                                <td><textarea name="TXT_Add_TRN_Notes" id="edit_notes"  style="width:100%; height:55px; resize:none; line-height:16px !important; padding-top:8px;" class="aa_input" rows="3" ></textarea></td>
                                            </tr>
						                </table>
                                    </td>
				                </tr>
                            				
                                <tr class="spacer10"><td>&nbsp;</td></tr>
				                
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
							                <tr>
								                <td class="aa_label_font">PASSWORD</td>
							                </tr>
							                <tr>
								                <td class="style__width_560"><div><input class="aa_input" name="passwordedit" id="edit_password" style="width:100%;" type="text" value="" /></div></td>

							                </tr>
						                </table>
                                    </td>
                                </tr>
                                <tr style="height:20px;" class="wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                            </table>
                        </div>
                    </td>
                </tr>
                
                

				<tr class="spacer10"><td>&nbsp;</td></tr>


				<tr>
					<td>
						<table class="tbl_width_600">
							<tr>
								<td style="text-align: right;"><input type="text" id="Text1" name="hidden_agentid" runat="server" hidden="hidden" />
                                    <input class="aa_btn_red" id="btn_Submit" type="button" value="Update" onclick="EditBusiness()" />
                                    <%--<asp:Button runat="server" Text="Update"  CssClass="aa-btn_red" OnClick="btnSubmit_Click" OnClientClick="return true;" ID="btnSubmit" />--%>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td class="spacer10">&nbsp;</td>
				</tr>

			</table>
		 </form>
            <div id="AddBeneficiaryDiv">
		<div class="edit__form__main">
			<form method="get" id="beneficiaryaddform">
			<table class="tbl_width_600">
				
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image18" runat="server" ImageUrl="~/images/beneficiary.png" title="BENEFICIARY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">LAST NAME *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">GIVEN NAME/S *</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="AddBene_lastname" name="lastname" style="width:100%;" type="text" value="" required="required" /></div></td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_firstnames" name="firstnames" style="width:100%;" type="text"  value="" required="required" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">COUNTRY OF BIRTH *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">DATE OF BIRTH (DOB)</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">PLACE OF BIRTH</td>
						    </tr>
						    <tr>
							    <td style="width:175px;">
								    <div>
									    <select id="AddBene_countryofbirth" style="width:100%;" name="D5">
										    <option selected="selected" value="">--Select--</option>
										    <option value="1">Afghanistan</option>
										    <option value="2">Albania</option>
										    <option value="3">Algeria</option>
										    <option value="4">Andorra</option>
										    <option value="5">Angola</option>
										    <option value="6">Antigua and Barbuda</option>
										    <option value="7">Argentina</option>
										    <option value="8">Armenia</option>
										    <option value="9">Australia</option>
										    <option value="10">Austria</option>
										    <option value="11">Azerbaijan</option>
										    <option value="12">Bahamas</option>
										    <option value="13">Bahrain</option>
										    <option value="14">Bangladesh</option>
										    <option value="15">Barbados</option>
										    <option value="16">Belarus</option>
										    <option value="17">Belgium</option>
										    <option value="18">Belize</option>
										    <option value="19">Benin</option>
										    <option value="20">Bhutan</option>
										    <option value="21">Bolivia</option>
										    <option value="22">Bosnia and Herzegovina</option>
										    <option value="23">Botswana</option>
										    <option value="24">Brazil</option>
										    <option value="25">Brunei</option>
										    <option value="26">Bulgaria</option>
										    <option value="27">Burkina Faso</option>
										    <option value="28">Burundi</option>
										    <option value="29">Cambodia</option>
										    <option value="30">Cameroon</option>
										    <option value="31">Canada</option>
										    <option value="32">Cape Verde</option>
										    <option value="33">Central African Republic</option>
										    <option value="34">Chad</option>
										    <option value="35">Chile</option>
										    <option value="36">China</option>
										    <option value="37">Colombia</option>
										    <option value="38">Comoros</option>
										    <option value="39">Congo (Kinshasa)</option>
										    <option value="40">Congo (Brazzaville)</option>
										    <option value="41">Costa Rica</option>
										    <option value="42">Cote d Ivoire (Ivory Coast)</option>
										    <option value="43">Croatia</option>
										    <option value="44">Cuba</option>
										    <option value="45">Cyprus</option>
										    <option value="46">Czech Republic</option>
										    <option value="47">Denmark</option>
										    <option value="48">Djibouti</option>
										    <option value="49">Dominica</option>
										    <option value="50">Dominican Republic</option>
										    <option value="51">Ecuador</option>
										    <option value="52">Egypt</option>
										    <option value="53">El Salvador</option>
										    <option value="54">Equatorial Guinea</option>
										    <option value="55">Eritrea</option>
										    <option value="56">Estonia</option>
										    <option value="57">Ethiopia</option>
										    <option value="58">Fiji</option>
										    <option value="59">Finland</option>
										    <option value="60">France</option>
										    <option value="61">Gabon</option>
										    <option value="62">Gambia</option>
										    <option value="63">Georgia</option>
										    <option value="64">Germany</option>
										    <option value="65">Ghana</option>
										    <option value="66">Greece</option>
										    <option value="67">Grenada</option>
										    <option value="68">Guatemala</option>
										    <option value="69">Guinea</option>
										    <option value="70">Guinea-Bissau</option>
										    <option value="71">Guyana</option>
										    <option value="72">Haiti</option>
										    <option value="73">Honduras</option>
										    <option value="74">Hungary</option>
										    <option value="75">Iceland</option>
										    <option value="76">India</option>
										    <option value="77">Indonesia</option>
										    <option value="78">Iran</option>
										    <option value="79">Iraq</option>
										    <option value="80">Ireland</option>
										    <option value="81">Israel</option>
										    <option value="82">Italy</option>
										    <option value="83">Jamaica</option>
										    <option value="84">Japan</option>
										    <option value="85">Jordan</option>
										    <option value="86">Kazakhstan</option>
										    <option value="87">Kenya</option>
										    <option value="88">Kiribati</option>
										    <option value="89">Korea (North)</option>
										    <option value="90">Korea (South)</option>
										    <option value="91">Kuwait</option>
										    <option value="92">Kyrgyzstan</option>
										    <option value="93">Laos</option>
										    <option value="94">Latvia</option>
										    <option value="95">Lebanon</option>
										    <option value="96">Lesotho</option>
										    <option value="97">Liberia</option>
										    <option value="98">Libya</option>
										    <option value="99">Liechtenstein</option>
										    <option value="100">Lithuania</option>
										    <option value="101">Luxembourg</option>
										    <option value="102">Macedonia</option>
										    <option value="103">Madagascar</option>
										    <option value="104">Malawi</option>
										    <option value="105">Malaysia</option>
										    <option value="106">Maldives</option>
										    <option value="107">Mali</option>
										    <option value="108">Malta</option>
										    <option value="109">Marshall Islands</option>
										    <option value="110">Mauritania</option>
										    <option value="111">Mauritius</option>
										    <option value="112">Mexico</option>
										    <option value="113">Micronesia</option>
										    <option value="114">Moldova</option>
										    <option value="115">Monaco</option>
										    <option value="116">Mongolia</option>
										    <option value="117">Montenegro</option>
										    <option value="118">Morocco</option>
										    <option value="119">Mozambique</option>
										    <option value="120">Myanmar (Burma)</option>
										    <option value="121">Namibia</option>
										    <option value="122">Nauru</option>
										    <option value="123">Nepal</option>
										    <option value="124">Netherlands</option>
										    <option value="125">New Zealand</option>
										    <option value="126">Nicaragua</option>
										    <option value="127">Niger</option>
										    <option value="128">Nigeria</option>
										    <option value="129">Norway</option>
										    <option value="130">Oman</option>
										    <option value="131">Pakistan</option>
										    <option value="132">Palau</option>
										    <option value="133">Panama</option>
										    <option value="134">Papua New Guinea</option>
										    <option value="135">Paraguay</option>
										    <option value="136">Peru</option>
										    <option value="137">Philippines</option>
										    <option value="138">Poland</option>
										    <option value="139">Portugal</option>
										    <option value="140">Qatar</option>
										    <option value="141">Romania</option>
										    <option value="142">Russia</option>
										    <option value="143">Rwanda</option>
										    <option value="144">Saint Kitts and Nevis</option>
										    <option value="145">Saint Lucia</option>
										    <option value="147">Samoa</option>
										    <option value="148">San Marino</option>
										    <option value="149">Sao Tome and Principe</option>
										    <option value="150">Saudi Arabia</option>
										    <option value="151">Senegal</option>
										    <option value="152">Serbia</option>
										    <option value="153">Seychelles</option>
										    <option value="154">Sierra Leone</option>
										    <option value="155">Singapore</option>
										    <option value="156">Slovakia</option>
										    <option value="157">Slovenia</option>
										    <option value="158">Solomon Islands</option>
										    <option value="159">Somalia</option>
										    <option value="160">South Africa</option>
										    <option value="161">Spain</option>
										    <option value="162">Sri Lanka</option>
										    <option value="163">Sudan</option>
										    <option value="164">Suriname</option>
										    <option value="165">Swaziland</option>
										    <option value="166">Sweden</option>
										    <option value="167">Switzerland</option>
										    <option value="168">Syria</option>
										    <option value="169">Tajikistan</option>
										    <option value="170">Tanzania</option>
										    <option value="171">Thailand</option>
										    <option value="172">Timor-Leste (East Timor)</option>
										    <option value="173">Togo</option>
										    <option value="174">Tonga</option>
										    <option value="175">Trinidad and Tobago</option>
										    <option value="176">Tunisia</option>
										    <option value="177">Turkey</option>
										    <option value="178">Turkmenistan</option>
										    <option value="179">Tuvalu</option>
										    <option value="180">Uganda</option>
										    <option value="181">Ukraine</option>
										    <option value="182">United Arab Emirates</option>
										    <option value="183">United Kingdom</option>
										    <option value="184">United States</option>
										    <option value="185">Uruguay</option>
										    <option value="186">Uzbekistan</option>
										    <option value="187">Vanuatu</option>
										    <option value="188">Vatican City</option>
										    <option value="189">Venezuela</option>
										    <option value="190">Vietnam</option>
										    <option value="191">Yemen</option>
										    <option value="192">Zambia</option>
										    <option value="193">Zimbabwe</option>
										    <option value="194">Abkhazia</option>
										    <option value="195">Taiwan</option>
										    <option value="197">Northern Cyprus</option>
										    <option value="198">Pridnestrovie (Transnistria)</option>
										    <option value="200">South Ossetia</option>
										    <option value="202">Christmas Island</option>
										    <option value="203">Cocos (Keeling) Islands</option>
										    <option value="206">Norfolk Island</option>
										    <option value="207">New Caledonia</option>
										    <option value="208">French Polynesia</option>
										    <option value="209">Mayotte</option>
										    <option value="210">Saint Barthelemy</option>
										    <option value="211">Saint Martin</option>
										    <option value="212">Saint Pierre and Miquelon</option>
										    <option value="213">Wallis and Futuna</option>
										    <option value="216">Bouvet Island</option>
										    <option value="217">Cook Islands</option>
										    <option value="218">Niue</option>
										    <option value="219">Tokelau</option>
										    <option value="220">Guernsey</option>
										    <option value="221">Isle of Man</option>
										    <option value="222">Jersey</option>
										    <option value="223">Anguilla</option>
										    <option value="224">Bermuda</option>
										    <option value="227">British Virgin Islands</option>
										    <option value="228">Cayman Islands</option>
										    <option value="230">Gibraltar</option>
										    <option value="231">Montserrat</option>
										    <option value="232">Pitcairn Islands</option>
										    <option value="233">Saint Helena</option>
										    <option value="235">Turks and Caicos Islands</option>
										    <option value="236">Northern Mariana Islands</option>
										    <option value="237">Puerto Rico</option>
										    <option value="238">American Samoa</option>
										    <option value="240">Guam</option>
										    <option value="248">U.S. Virgin Islands</option>
										    <option value="250">Hong Kong</option>
										    <option value="251">Macau</option>
										    <option value="252">Faroe Islands</option>
										    <option value="253">Greenland</option>
										    <option value="254">French Guiana</option>
										    <option value="255">Guadeloupe</option>
										    <option value="256">Martinique</option>
										    <option value="257">Reunion</option>
										    <option value="258">Aland Islands</option>
										    <option value="259">Aruba</option>
										    <option value="260">Netherlands Antilles</option>
										    <option value="261">Svalbard</option>
										    <option value="264">Antarctica Territories</option>
										    <option value="265">Kosovo</option>
										    <option value="266">Palestinian Territories</option>
										    <option value="267">Western Sahara</option>
										    <option value="273">Sint Maarten</option>
										    <option value="274">South Sudan</option>
									    </select>
								    </div>
							    </td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:174px;"><div><input class="aa_input datePickerDOB" id="AddBene_dob" name="dob" placeholder="dd/mm/yyyy" style="width:100%;" readonly="readonly" type="text" value="" /></div></td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:175px;"><div><input class="aa_input" id="Addbene_placeofbirth" name="placeofbirth" style="width:100%;" type="text" value="" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">NATIONALITY *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">ALSO KNOWN AS (AKA)</td>
						    </tr>
						    <tr>
							    <td style="width:175px;">
								    <div>
									    <select id="AddBene_nationality" required="required" style="width:100%;">
										    <option selected="selected" value="">--Select--</option>
										    <option value="1">Afghani</option>
										    <option value="2">Albanian</option>
										    <option value="3">Algerian</option>
										    <option value="4">American</option>
										    <option value="5">Andorran</option>
										    <option value="6">Angolan</option>
										    <option value="202">Anguillan</option>
										    <option value="203">Antarctic</option>
										    <option value="7">Antiguans</option>
										    <option value="8">Argentinean</option>
										    <option value="9">Armenian</option>
										    <option value="204">Arubian</option>
										    <option value="10">Australian</option>
										    <option value="11">Austrian</option>
										    <option value="12">Azerbaijani</option>
										    <option value="208">Bahameese</option>
										    <option value="13">Bahamian</option>
										    <option value="14">Bahraini</option>
										    <option value="15">Bangladeshi</option>
										    <option value="16">Barbadian</option>
										    <option value="17">Barbudans</option>
										    <option value="206">Barthlemois</option>
										    <option value="19">Belarusian</option>
										    <option value="20">Belgian</option>
										    <option value="21">Belizean</option>
										    <option value="22">Beninese</option>
										    <option value="207">Bermudan</option>
										    <option value="23">Bhutanese</option>
										    <option value="24">Bolivian</option>
										    <option value="25">Bosnian</option>
										    <option value="26">Brazilian</option>
										    <option value="27">British</option>
										    <option value="28">Bruneian</option>
										    <option value="29">Bulgarian</option>
										    <option value="30">Burkinabe</option>
										    <option value="31">Burmese</option>
										    <option value="32">Burundian</option>
										    <option value="33">Cambodian</option>
										    <option value="34">Cameroonian</option>
										    <option value="35">Canadian</option>
										    <option value="36">Cape Verdean</option>
										    <option value="224">Caymanian</option>
										    <option value="37">Central African</option>
										    <option value="38">Chadian</option>
										    <option value="39">Chilean</option>
										    <option value="40">Chinese</option>
										    <option value="212">Christmas Islander</option>
										    <option value="209">Cocossian</option>
										    <option value="41">Colombian</option>
										    <option value="42">Comoran</option>
										    <option value="43">Congolese</option>
										    <option value="210">Cook Islander</option>
										    <option value="45">Costa Rican</option>
										    <option value="46">Croatian</option>
										    <option value="47">Cuban</option>
										    <option value="211">Curaaoan</option>
										    <option value="48">Cypriot</option>
										    <option value="49">Czech</option>
										    <option value="50">Danish</option>
										    <option value="51">Djiboutian</option>
										    <option value="52">Dominican</option>
										    <option value="54">Dutch</option>
										    <option value="56">Dutchwoman</option>
										    <option value="58">Ecuadorean</option>
										    <option value="59">Egyptian</option>
										    <option value="60">Emirian</option>
										    <option value="61">Equatorial Guinean</option>
										    <option value="62">Eritrean</option>
										    <option value="63">Estonian</option>
										    <option value="64">Ethiopian</option>
										    <option value="214">Falkland Islander</option>
										    <option value="215">Faroese</option>
										    <option value="65">Fijian</option>
										    <option value="66">Filipino</option>
										    <option value="67">Finnish</option>
										    <option value="68">French</option>
										    <option value="216">French Guianese</option>
										    <option value="233">French Polynesian</option>
										    <option value="69">Gabonese</option>
										    <option value="70">Gambian</option>
										    <option value="71">Georgian</option>
										    <option value="72">German</option>
										    <option value="73">Ghanaian</option>
										    <option value="217">Gibralterian</option>
										    <option value="74">Greek</option>
										    <option value="218">Greenlander</option>
										    <option value="75">Grenadian</option>
										    <option value="219">Guadeloupean</option>
										    <option value="220">Guamanian</option>
										    <option value="76">Guatemalan</option>
										    <option value="77">Guinean</option>
										    <option value="79">Guyanese</option>
										    <option value="80">Haitian</option>
										    <option value="81">Herzegovinian</option>
										    <option value="82">Honduran</option>
										    <option value="221">Hong Konger</option>
										    <option value="83">Hungarian</option>
										    <option value="84">I-Kiribati</option>
										    <option value="85">Icelander</option>
										    <option value="86">Indian</option>
										    <option value="87">Indonesian</option>
										    <option value="88">Iranian</option>
										    <option value="89">Iraqi</option>
										    <option value="91">Irish</option>
										    <option value="92">Israeli</option>
										    <option value="93">Italian</option>
										    <option value="94">Ivorian</option>
										    <option value="95">Jamaican</option>
										    <option value="96">Japanese</option>
										    <option value="97">Jordanian</option>
										    <option value="98">Kazakhstani</option>
										    <option value="99">Kenyan</option>
										    <option value="100">Kittian and Nevisian</option>
										    <option value="101">Kuwaiti</option>
										    <option value="102">Kyrgyz</option>
										    <option value="223">Kyrgyzstani</option>
										    <option value="103">Laotian</option>
										    <option value="104">Latvian</option>
										    <option value="105">Lebanese</option>
										    <option value="106">Liberian</option>
										    <option value="107">Libyan</option>
										    <option value="108">Liechtensteiner</option>
										    <option value="109">Lithuanian</option>
										    <option value="110">Luxembourger</option>
										    <option value="226">Macanese</option>
										    <option value="111">Macedonian</option>
										    <option value="246">Mahoran</option>
										    <option value="112">Malagasy</option>
										    <option value="113">Malawian</option>
										    <option value="114">Malaysian</option>
										    <option value="115">Maldivan</option>
										    <option value="116">Malian</option>
										    <option value="117">Maltese</option>
										    <option value="222">Manx</option>
										    <option value="118">Marshallese</option>
										    <option value="228">Martinican</option>
										    <option value="119">Mauritanian</option>
										    <option value="120">Mauritian</option>
										    <option value="121">Mexican</option>
										    <option value="122">Micronesian</option>
										    <option value="123">Moldovan</option>
										    <option value="124">Monacan</option>
										    <option value="125">Mongolian</option>
										    <option value="225">Montenegrin</option>
										    <option value="229">Montserratian</option>
										    <option value="126">Moroccan</option>
										    <option value="127">Mosotho</option>
										    <option value="18">Motswana</option>
										    <option value="128">Motswana</option>
										    <option value="129">Mozambican</option>
										    <option value="130">Namibian</option>
										    <option value="131">Nauruan</option>
										    <option value="132">Nepalese</option>
										    <option value="230">New Caledonian</option>
										    <option value="134">New Zealander</option>
										    <option value="135">Ni-Vanuatu</option>
										    <option value="136">Nicaraguan</option>
										    <option value="137">Nigerian</option>
										    <option value="138">Nigerien</option>
										    <option value="232">Niuean</option>
										    <option value="231">Norfolk Islander</option>
										    <option value="139">North Korean</option>
										    <option value="140">Northern Irish</option>
										    <option value="227">Northern Mariana Islander</option>
										    <option value="141">Norwegian</option>
										    <option value="142">Omani</option>
										    <option value="143">Pakistani</option>
										    <option value="144">Palauan</option>
										    <option value="237">Palestinian</option>
										    <option value="145">Panamanian</option>
										    <option value="146">Papua New Guinean</option>
										    <option value="147">Paraguayan</option>
										    <option value="148">Peruvian</option>
										    <option value="235">Pitcairn Islander</option>
										    <option value="149">Polish</option>
										    <option value="150">Portuguese</option>
										    <option value="236">Puerto Rican</option>
										    <option value="151">Qatari</option>
										    <option value="152">Romanian</option>
										    <option value="153">Russian</option>
										    <option value="154">Rwandan</option>
										    <option value="238">Saint Helenian</option>
										    <option value="155">Saint Lucian</option>
										    <option value="242">Saint Vincentian</option>
										    <option value="234">Saint-Pierrais</option>
										    <option value="156">Salvadoran</option>
										    <option value="157">Samoan</option>
										    <option value="158">San Marinese</option>
										    <option value="159">Sao Tomean</option>
										    <option value="160">Saudi</option>
										    <option value="161">Scottish</option>
										    <option value="162">Senegalese</option>
										    <option value="163">Serbian</option>
										    <option value="164">Seychellois</option>
										    <option value="165">Sierra Leonean</option>
										    <option value="166">Singaporean</option>
										    <option value="167">Slovakian</option>
										    <option value="168">Slovenian</option>
										    <option value="169">Solomon Islander</option>
										    <option value="170">Somali</option>
										    <option value="171">South African</option>
										    <option value="172">South Korean</option>
										    <option value="173">Spanish</option>
										    <option value="174">Sri Lankan</option>
										    <option value="175">Sudanese</option>
										    <option value="176">Surinamer</option>
										    <option value="177">Swazi</option>
										    <option value="178">Swedish</option>
										    <option value="179">Swiss</option>
										    <option value="180">Syrian</option>
										    <option value="181">Taiwanese</option>
										    <option value="182">Tajik</option>
										    <option value="183">Tanzanian</option>
										    <option value="184">Thai</option>
										    <option value="247">Tibetan </option>
										    <option value="57">Timorese</option>
										    <option value="185">Togolese</option>
										    <option value="240">Tokelauan</option>
										    <option value="186">Tongan</option>
										    <option value="187">Trinidadian or Tobagonian</option>
										    <option value="188">Tunisian</option>
										    <option value="189">Turkish</option>
										    <option value="241">Turkmen</option>
										    <option value="239">Turks and Caicos Islander</option>
										    <option value="190">Tuvaluan</option>
										    <option value="191">Ugandan</option>
										    <option value="192">Ukrainian</option>
										    <option value="193">Uruguayan</option>
										    <option value="194">Uzbekistani</option>
										    <option value="195">Venezuelan</option>
										    <option value="196">Vietnamese</option>
										    <option value="243">Virgin Islander-UK</option>
										    <option value="244">Virgin Islander-US</option>
										    <option value="245">Wallisian</option>
										    <option value="198">Welsh</option>
										    <option value="213">Western Saharan</option>
										    <option value="199">Yemenese</option>
										    <option value="200">Zambian</option>
										    <option value="201">Zimbabwean</option>
										    <option value="205">landic</option>
									    </select>
								    </div>
							    </td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_aka" name="aka" style="width:100%;" type="text"  value="" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">ID DETAILS</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">RELATIONSHIP WITH SENDER *</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="AddBene_iddetails" name="iddetails" style="width:100%;" type="text" value=""  /></div></td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_relationship" name="relationshipwithsender" style="width:100%;" type="text"  value="" required="required"  /></div></td>
						    </tr>
                            <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
					    </table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">CONTACT NUMBER</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">EMAIL ADDRESS</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="AddBene_contactnumber" name="contactnumber" style="width:100%;" type="text" value="" /></div></td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_email" name="emailaddress" style="width:100%;" type="text"  value=""  /></div></td>
						    </tr>
                            <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
					    </table>
					</td>
				</tr>


				<tr class="wiz_bg_EFEFEF">
					<td>
						<table class="tbl_width_560">
							<tr>
								<td class="aa_label_font">COUNTRY*</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">STATE/PROVINCE*</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">SUBURB/DISTRICT *</td>
							</tr>
							<tr>
								<td style="width:175px;"><select id="AddBene_CountryDDL" name="D2" style="width:100%;"></select></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:174px;"><select id="AddBene_stateprovince" name="D3" style="width:100%;"><option></option></select></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:175px;"><select id="AddBene_suburbdistrict" name="D4" style="width:100%;"><option></option></select></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
						<table class="tbl_width_560">
							<tr>
								<td class="aa_label_font">ADDRESS LINE 1 *</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">ADDRESS LINE 2</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">POST CODE</td>
							</tr>
							<tr>
								<td style="width:175px;"><input class="aa_input" id="AddBene_addressline1" name="country" style="width:100%;" type="text" required="required"/></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:174px;"><input class="aa_input" id="AddBene_addressline2" name="stateprovince" style="width:100%;" type="text"/></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:175px;"><input class="aa_input" id="AddBene_postzipcode" name="suburbdistrict" style="width:100%;" type="text"/></td>
							</tr>
						</table>
					</td>
				</tr>

                <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_600">
							<tr>
								<td style="text-align: right;"><input class="aa_btn_red" id="btn_AddBeneficiarySubmit" type="button" value="Add Beneficiary"/></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

			</table>
			
				</form>

		</div>
	</div> 
            <!-- REFUND POPUP START ---->
        <div id="RefundDIV"> 
		<div class="add_credit_form">
			<form method="get" id="refundform">
			
				<table class="tbl_width_440">
				
                <tr><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

				<tr id="TR_refunderrormsg">
					<td class="wiz_bg_EFEFEF">
						<table class="tbl_width_400">
                            <tr style="background-color:#fcdbd9;">
								<td>
									<table class="tbl_width_380">
										<tr>
											<td class="normal_font" style="color:#EB585C; padding: 10px 0px;"><span id="refunderrormsg">&nbsp;</span></td>
										</tr>
                                        
									</table>
								</td>
							</tr>
                            <tr><td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					<table class="tbl_width_400">
						<tr>
							<td class="aa_label_font">AUD AMOUNT </td>
							<td class="aa_label_font">&nbsp;</td>
							<td class="aa_label_font">REFUND METHOD</td>
						</tr>
						<tr>
							<td style="width:100px;"><div><input class="aa_input" id="txt_RefundAmount" style="width:100%;" type="number" value="" /></div></td>
							<td style="width:20px;">&nbsp;</td>
							<td style="width:320px; vertical-align:top;"><div><select id="txt_RefundDescription" style="width:100%;" required="required">
                                <option value="">-- SELECT --</option>
                                <option value="Bank Account">Bank Account</option>
							                                                   </select></div></td>
						</tr>
					</table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

				<tr><td style="text-align: right;"><input class="aa_btn_red" id="btn_refundcomplete" type="button" value="Refund"/></td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				</table>
				</form>
			</div>
		</div>

<!-- REFUND POPUP END -->
		</div>
	</div>
<!-- ************************* BUTTON 01 EDIT CUSTOMER MODAL POPUP END ************************* --> 
    <!-- ************************* BUTTON 05 EDIT BENEFICIARY MODAL POPUP START ************************* --> 
		<div id="EditBeneficiaryDiv">
		<div class="edit__form__main">
			<form method="get" id="beneficiaryeditform">
			<table class="tbl_width_600">
				
				<tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image22" runat="server" ImageUrl="~/images/beneficiary.png" title="BENEFICIARY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">LAST NAME *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">GIVEN NAME/S *</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="EditBene_lastname" name="lastname" style="width:100%;" type="text" value="" required="required" /></div></td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="EditBene_firstnames" name="firstnames" style="width:100%;" type="text"  value=""  required="required" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">COUNTRY OF BIRTH</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">DATE OF BIRTH (DOB)</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">PLACE OF BIRTH</td>
						    </tr>
						    <tr>
							    <td style="width:175px;">
								    <div>
									    <select id="EditBene_countryofbirth" style="width:100%;">
										    <option selected="selected" value="">--Select--</option>
										    <option value="1">Afghanistan</option>
										    <option value="2">Albania</option>
										    <option value="3">Algeria</option>
										    <option value="4">Andorra</option>
										    <option value="5">Angola</option>
										    <option value="6">Antigua and Barbuda</option>
										    <option value="7">Argentina</option>
										    <option value="8">Armenia</option>
										    <option value="9">Australia</option>
										    <option value="10">Austria</option>
										    <option value="11">Azerbaijan</option>
										    <option value="12">Bahamas</option>
										    <option value="13">Bahrain</option>
										    <option value="14">Bangladesh</option>
										    <option value="15">Barbados</option>
										    <option value="16">Belarus</option>
										    <option value="17">Belgium</option>
										    <option value="18">Belize</option>
										    <option value="19">Benin</option>
										    <option value="20">Bhutan</option>
										    <option value="21">Bolivia</option>
										    <option value="22">Bosnia and Herzegovina</option>
										    <option value="23">Botswana</option>
										    <option value="24">Brazil</option>
										    <option value="25">Brunei</option>
										    <option value="26">Bulgaria</option>
										    <option value="27">Burkina Faso</option>
										    <option value="28">Burundi</option>
										    <option value="29">Cambodia</option>
										    <option value="30">Cameroon</option>
										    <option value="31">Canada</option>
										    <option value="32">Cape Verde</option>
										    <option value="33">Central African Republic</option>
										    <option value="34">Chad</option>
										    <option value="35">Chile</option>
										    <option value="36">China</option>
										    <option value="37">Colombia</option>
										    <option value="38">Comoros</option>
										    <option value="39">Congo (Kinshasa)</option>
										    <option value="40">Congo (Brazzaville)</option>
										    <option value="41">Costa Rica</option>
										    <option value="42">Cote d Ivoire (Ivory Coast)</option>
										    <option value="43">Croatia</option>
										    <option value="44">Cuba</option>
										    <option value="45">Cyprus</option>
										    <option value="46">Czech Republic</option>
										    <option value="47">Denmark</option>
										    <option value="48">Djibouti</option>
										    <option value="49">Dominica</option>
										    <option value="50">Dominican Republic</option>
										    <option value="51">Ecuador</option>
										    <option value="52">Egypt</option>
										    <option value="53">El Salvador</option>
										    <option value="54">Equatorial Guinea</option>
										    <option value="55">Eritrea</option>
										    <option value="56">Estonia</option>
										    <option value="57">Ethiopia</option>
										    <option value="58">Fiji</option>
										    <option value="59">Finland</option>
										    <option value="60">France</option>
										    <option value="61">Gabon</option>
										    <option value="62">Gambia</option>
										    <option value="63">Georgia</option>
										    <option value="64">Germany</option>
										    <option value="65">Ghana</option>
										    <option value="66">Greece</option>
										    <option value="67">Grenada</option>
										    <option value="68">Guatemala</option>
										    <option value="69">Guinea</option>
										    <option value="70">Guinea-Bissau</option>
										    <option value="71">Guyana</option>
										    <option value="72">Haiti</option>
										    <option value="73">Honduras</option>
										    <option value="74">Hungary</option>
										    <option value="75">Iceland</option>
										    <option value="76">India</option>
										    <option value="77">Indonesia</option>
										    <option value="78">Iran</option>
										    <option value="79">Iraq</option>
										    <option value="80">Ireland</option>
										    <option value="81">Israel</option>
										    <option value="82">Italy</option>
										    <option value="83">Jamaica</option>
										    <option value="84">Japan</option>
										    <option value="85">Jordan</option>
										    <option value="86">Kazakhstan</option>
										    <option value="87">Kenya</option>
										    <option value="88">Kiribati</option>
										    <option value="89">Korea (North)</option>
										    <option value="90">Korea (South)</option>
										    <option value="91">Kuwait</option>
										    <option value="92">Kyrgyzstan</option>
										    <option value="93">Laos</option>
										    <option value="94">Latvia</option>
										    <option value="95">Lebanon</option>
										    <option value="96">Lesotho</option>
										    <option value="97">Liberia</option>
										    <option value="98">Libya</option>
										    <option value="99">Liechtenstein</option>
										    <option value="100">Lithuania</option>
										    <option value="101">Luxembourg</option>
										    <option value="102">Macedonia</option>
										    <option value="103">Madagascar</option>
										    <option value="104">Malawi</option>
										    <option value="105">Malaysia</option>
										    <option value="106">Maldives</option>
										    <option value="107">Mali</option>
										    <option value="108">Malta</option>
										    <option value="109">Marshall Islands</option>
										    <option value="110">Mauritania</option>
										    <option value="111">Mauritius</option>
										    <option value="112">Mexico</option>
										    <option value="113">Micronesia</option>
										    <option value="114">Moldova</option>
										    <option value="115">Monaco</option>
										    <option value="116">Mongolia</option>
										    <option value="117">Montenegro</option>
										    <option value="118">Morocco</option>
										    <option value="119">Mozambique</option>
										    <option value="120">Myanmar (Burma)</option>
										    <option value="121">Namibia</option>
										    <option value="122">Nauru</option>
										    <option value="123">Nepal</option>
										    <option value="124">Netherlands</option>
										    <option value="125">New Zealand</option>
										    <option value="126">Nicaragua</option>
										    <option value="127">Niger</option>
										    <option value="128">Nigeria</option>
										    <option value="129">Norway</option>
										    <option value="130">Oman</option>
										    <option value="131">Pakistan</option>
										    <option value="132">Palau</option>
										    <option value="133">Panama</option>
										    <option value="134">Papua New Guinea</option>
										    <option value="135">Paraguay</option>
										    <option value="136">Peru</option>
										    <option value="137">Philippines</option>
										    <option value="138">Poland</option>
										    <option value="139">Portugal</option>
										    <option value="140">Qatar</option>
										    <option value="141">Romania</option>
										    <option value="142">Russia</option>
										    <option value="143">Rwanda</option>
										    <option value="144">Saint Kitts and Nevis</option>
										    <option value="145">Saint Lucia</option>
										    <option value="147">Samoa</option>
										    <option value="148">San Marino</option>
										    <option value="149">Sao Tome and Principe</option>
										    <option value="150">Saudi Arabia</option>
										    <option value="151">Senegal</option>
										    <option value="152">Serbia</option>
										    <option value="153">Seychelles</option>
										    <option value="154">Sierra Leone</option>
										    <option value="155">Singapore</option>
										    <option value="156">Slovakia</option>
										    <option value="157">Slovenia</option>
										    <option value="158">Solomon Islands</option>
										    <option value="159">Somalia</option>
										    <option value="160">South Africa</option>
										    <option value="161">Spain</option>
										    <option value="162">Sri Lanka</option>
										    <option value="163">Sudan</option>
										    <option value="164">Suriname</option>
										    <option value="165">Swaziland</option>
										    <option value="166">Sweden</option>
										    <option value="167">Switzerland</option>
										    <option value="168">Syria</option>
										    <option value="169">Tajikistan</option>
										    <option value="170">Tanzania</option>
										    <option value="171">Thailand</option>
										    <option value="172">Timor-Leste (East Timor)</option>
										    <option value="173">Togo</option>
										    <option value="174">Tonga</option>
										    <option value="175">Trinidad and Tobago</option>
										    <option value="176">Tunisia</option>
										    <option value="177">Turkey</option>
										    <option value="178">Turkmenistan</option>
										    <option value="179">Tuvalu</option>
										    <option value="180">Uganda</option>
										    <option value="181">Ukraine</option>
										    <option value="182">United Arab Emirates</option>
										    <option value="183">United Kingdom</option>
										    <option value="184">United States</option>
										    <option value="185">Uruguay</option>
										    <option value="186">Uzbekistan</option>
										    <option value="187">Vanuatu</option>
										    <option value="188">Vatican City</option>
										    <option value="189">Venezuela</option>
										    <option value="190">Vietnam</option>
										    <option value="191">Yemen</option>
										    <option value="192">Zambia</option>
										    <option value="193">Zimbabwe</option>
										    <option value="194">Abkhazia</option>
										    <option value="195">Taiwan</option>
										    <option value="197">Northern Cyprus</option>
										    <option value="198">Pridnestrovie (Transnistria)</option>
										    <option value="200">South Ossetia</option>
										    <option value="202">Christmas Island</option>
										    <option value="203">Cocos (Keeling) Islands</option>
										    <option value="206">Norfolk Island</option>
										    <option value="207">New Caledonia</option>
										    <option value="208">French Polynesia</option>
										    <option value="209">Mayotte</option>
										    <option value="210">Saint Barthelemy</option>
										    <option value="211">Saint Martin</option>
										    <option value="212">Saint Pierre and Miquelon</option>
										    <option value="213">Wallis and Futuna</option>
										    <option value="216">Bouvet Island</option>
										    <option value="217">Cook Islands</option>
										    <option value="218">Niue</option>
										    <option value="219">Tokelau</option>
										    <option value="220">Guernsey</option>
										    <option value="221">Isle of Man</option>
										    <option value="222">Jersey</option>
										    <option value="223">Anguilla</option>
										    <option value="224">Bermuda</option>
										    <option value="227">British Virgin Islands</option>
										    <option value="228">Cayman Islands</option>
										    <option value="230">Gibraltar</option>
										    <option value="231">Montserrat</option>
										    <option value="232">Pitcairn Islands</option>
										    <option value="233">Saint Helena</option>
										    <option value="235">Turks and Caicos Islands</option>
										    <option value="236">Northern Mariana Islands</option>
										    <option value="237">Puerto Rico</option>
										    <option value="238">American Samoa</option>
										    <option value="240">Guam</option>
										    <option value="248">U.S. Virgin Islands</option>
										    <option value="250">Hong Kong</option>
										    <option value="251">Macau</option>
										    <option value="252">Faroe Islands</option>
										    <option value="253">Greenland</option>
										    <option value="254">French Guiana</option>
										    <option value="255">Guadeloupe</option>
										    <option value="256">Martinique</option>
										    <option value="257">Reunion</option>
										    <option value="258">Aland Islands</option>
										    <option value="259">Aruba</option>
										    <option value="260">Netherlands Antilles</option>
										    <option value="261">Svalbard</option>
										    <option value="264">Antarctica Territories</option>
										    <option value="265">Kosovo</option>
										    <option value="266">Palestinian Territories</option>
										    <option value="267">Western Sahara</option>
										    <option value="273">Sint Maarten</option>
										    <option value="274">South Sudan</option>
									    </select>
								    </div>
							    </td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:174px;"><div><input class="aa_input datePickerDOB" id="EditBene_dob" name="dob" placeholder="dd/mm/yyyy" style="width:100%;" type="text" readonly="readonly" value="" /></div></td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:175px;"><div><input class="aa_input" id="Editbene_placeofbirth" name="placeofbirth" style="width:100%;" type="text" value=""  /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">NATIONALITY *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">ALSO KNOWN AS (AKA)</td>
						    </tr>
						    <tr>
							    <td style="width:175px;">
								    <div>
									    <select id="EditBene_nationality" required="required" style="width:100%;">
										    <option selected="selected" value="">--Select--</option>
										    <option value="1">Afghani</option>
										    <option value="2">Albanian</option>
										    <option value="3">Algerian</option>
										    <option value="4">American</option>
										    <option value="5">Andorran</option>
										    <option value="6">Angolan</option>
										    <option value="202">Anguillan</option>
										    <option value="203">Antarctic</option>
										    <option value="7">Antiguans</option>
										    <option value="8">Argentinean</option>
										    <option value="9">Armenian</option>
										    <option value="204">Arubian</option>
										    <option value="10">Australian</option>
										    <option value="11">Austrian</option>
										    <option value="12">Azerbaijani</option>
										    <option value="208">Bahameese</option>
										    <option value="13">Bahamian</option>
										    <option value="14">Bahraini</option>
										    <option value="15">Bangladeshi</option>
										    <option value="16">Barbadian</option>
										    <option value="17">Barbudans</option>
										    <option value="206">Barthlemois</option>
										    <option value="19">Belarusian</option>
										    <option value="20">Belgian</option>
										    <option value="21">Belizean</option>
										    <option value="22">Beninese</option>
										    <option value="207">Bermudan</option>
										    <option value="23">Bhutanese</option>
										    <option value="24">Bolivian</option>
										    <option value="25">Bosnian</option>
										    <option value="26">Brazilian</option>
										    <option value="27">British</option>
										    <option value="28">Bruneian</option>
										    <option value="29">Bulgarian</option>
										    <option value="30">Burkinabe</option>
										    <option value="31">Burmese</option>
										    <option value="32">Burundian</option>
										    <option value="33">Cambodian</option>
										    <option value="34">Cameroonian</option>
										    <option value="35">Canadian</option>
										    <option value="36">Cape Verdean</option>
										    <option value="224">Caymanian</option>
										    <option value="37">Central African</option>
										    <option value="38">Chadian</option>
										    <option value="39">Chilean</option>
										    <option value="40">Chinese</option>
										    <option value="212">Christmas Islander</option>
										    <option value="209">Cocossian</option>
										    <option value="41">Colombian</option>
										    <option value="42">Comoran</option>
										    <option value="43">Congolese</option>
										    <option value="210">Cook Islander</option>
										    <option value="45">Costa Rican</option>
										    <option value="46">Croatian</option>
										    <option value="47">Cuban</option>
										    <option value="211">Curaaoan</option>
										    <option value="48">Cypriot</option>
										    <option value="49">Czech</option>
										    <option value="50">Danish</option>
										    <option value="51">Djiboutian</option>
										    <option value="52">Dominican</option>
										    <option value="54">Dutch</option>
										    <option value="56">Dutchwoman</option>
										    <option value="58">Ecuadorean</option>
										    <option value="59">Egyptian</option>
										    <option value="60">Emirian</option>
										    <option value="61">Equatorial Guinean</option>
										    <option value="62">Eritrean</option>
										    <option value="63">Estonian</option>
										    <option value="64">Ethiopian</option>
										    <option value="214">Falkland Islander</option>
										    <option value="215">Faroese</option>
										    <option value="65">Fijian</option>
										    <option value="66">Filipino</option>
										    <option value="67">Finnish</option>
										    <option value="68">French</option>
										    <option value="216">French Guianese</option>
										    <option value="233">French Polynesian</option>
										    <option value="69">Gabonese</option>
										    <option value="70">Gambian</option>
										    <option value="71">Georgian</option>
										    <option value="72">German</option>
										    <option value="73">Ghanaian</option>
										    <option value="217">Gibralterian</option>
										    <option value="74">Greek</option>
										    <option value="218">Greenlander</option>
										    <option value="75">Grenadian</option>
										    <option value="219">Guadeloupean</option>
										    <option value="220">Guamanian</option>
										    <option value="76">Guatemalan</option>
										    <option value="77">Guinean</option>
										    <option value="79">Guyanese</option>
										    <option value="80">Haitian</option>
										    <option value="81">Herzegovinian</option>
										    <option value="82">Honduran</option>
										    <option value="221">Hong Konger</option>
										    <option value="83">Hungarian</option>
										    <option value="84">I-Kiribati</option>
										    <option value="85">Icelander</option>
										    <option value="86">Indian</option>
										    <option value="87">Indonesian</option>
										    <option value="88">Iranian</option>
										    <option value="89">Iraqi</option>
										    <option value="91">Irish</option>
										    <option value="92">Israeli</option>
										    <option value="93">Italian</option>
										    <option value="94">Ivorian</option>
										    <option value="95">Jamaican</option>
										    <option value="96">Japanese</option>
										    <option value="97">Jordanian</option>
										    <option value="98">Kazakhstani</option>
										    <option value="99">Kenyan</option>
										    <option value="100">Kittian and Nevisian</option>
										    <option value="101">Kuwaiti</option>
										    <option value="102">Kyrgyz</option>
										    <option value="223">Kyrgyzstani</option>
										    <option value="103">Laotian</option>
										    <option value="104">Latvian</option>
										    <option value="105">Lebanese</option>
										    <option value="106">Liberian</option>
										    <option value="107">Libyan</option>
										    <option value="108">Liechtensteiner</option>
										    <option value="109">Lithuanian</option>
										    <option value="110">Luxembourger</option>
										    <option value="226">Macanese</option>
										    <option value="111">Macedonian</option>
										    <option value="246">Mahoran</option>
										    <option value="112">Malagasy</option>
										    <option value="113">Malawian</option>
										    <option value="114">Malaysian</option>
										    <option value="115">Maldivan</option>
										    <option value="116">Malian</option>
										    <option value="117">Maltese</option>
										    <option value="222">Manx</option>
										    <option value="118">Marshallese</option>
										    <option value="228">Martinican</option>
										    <option value="119">Mauritanian</option>
										    <option value="120">Mauritian</option>
										    <option value="121">Mexican</option>
										    <option value="122">Micronesian</option>
										    <option value="123">Moldovan</option>
										    <option value="124">Monacan</option>
										    <option value="125">Mongolian</option>
										    <option value="225">Montenegrin</option>
										    <option value="229">Montserratian</option>
										    <option value="126">Moroccan</option>
										    <option value="127">Mosotho</option>
										    <option value="18">Motswana</option>
										    <option value="128">Motswana</option>
										    <option value="129">Mozambican</option>
										    <option value="130">Namibian</option>
										    <option value="131">Nauruan</option>
										    <option value="132">Nepalese</option>
										    <option value="230">New Caledonian</option>
										    <option value="134">New Zealander</option>
										    <option value="135">Ni-Vanuatu</option>
										    <option value="136">Nicaraguan</option>
										    <option value="137">Nigerian</option>
										    <option value="138">Nigerien</option>
										    <option value="232">Niuean</option>
										    <option value="231">Norfolk Islander</option>
										    <option value="139">North Korean</option>
										    <option value="140">Northern Irish</option>
										    <option value="227">Northern Mariana Islander</option>
										    <option value="141">Norwegian</option>
										    <option value="142">Omani</option>
										    <option value="143">Pakistani</option>
										    <option value="144">Palauan</option>
										    <option value="237">Palestinian</option>
										    <option value="145">Panamanian</option>
										    <option value="146">Papua New Guinean</option>
										    <option value="147">Paraguayan</option>
										    <option value="148">Peruvian</option>
										    <option value="235">Pitcairn Islander</option>
										    <option value="149">Polish</option>
										    <option value="150">Portuguese</option>
										    <option value="236">Puerto Rican</option>
										    <option value="151">Qatari</option>
										    <option value="152">Romanian</option>
										    <option value="153">Russian</option>
										    <option value="154">Rwandan</option>
										    <option value="238">Saint Helenian</option>
										    <option value="155">Saint Lucian</option>
										    <option value="242">Saint Vincentian</option>
										    <option value="234">Saint-Pierrais</option>
										    <option value="156">Salvadoran</option>
										    <option value="157">Samoan</option>
										    <option value="158">San Marinese</option>
										    <option value="159">Sao Tomean</option>
										    <option value="160">Saudi</option>
										    <option value="161">Scottish</option>
										    <option value="162">Senegalese</option>
										    <option value="163">Serbian</option>
										    <option value="164">Seychellois</option>
										    <option value="165">Sierra Leonean</option>
										    <option value="166">Singaporean</option>
										    <option value="167">Slovakian</option>
										    <option value="168">Slovenian</option>
										    <option value="169">Solomon Islander</option>
										    <option value="170">Somali</option>
										    <option value="171">South African</option>
										    <option value="172">South Korean</option>
										    <option value="173">Spanish</option>
										    <option value="174">Sri Lankan</option>
										    <option value="175">Sudanese</option>
										    <option value="176">Surinamer</option>
										    <option value="177">Swazi</option>
										    <option value="178">Swedish</option>
										    <option value="179">Swiss</option>
										    <option value="180">Syrian</option>
										    <option value="181">Taiwanese</option>
										    <option value="182">Tajik</option>
										    <option value="183">Tanzanian</option>
										    <option value="184">Thai</option>
										    <option value="247">Tibetan </option>
										    <option value="57">Timorese</option>
										    <option value="185">Togolese</option>
										    <option value="240">Tokelauan</option>
										    <option value="186">Tongan</option>
										    <option value="187">Trinidadian or Tobagonian</option>
										    <option value="188">Tunisian</option>
										    <option value="189">Turkish</option>
										    <option value="241">Turkmen</option>
										    <option value="239">Turks and Caicos Islander</option>
										    <option value="190">Tuvaluan</option>
										    <option value="191">Ugandan</option>
										    <option value="192">Ukrainian</option>
										    <option value="193">Uruguayan</option>
										    <option value="194">Uzbekistani</option>
										    <option value="195">Venezuelan</option>
										    <option value="196">Vietnamese</option>
										    <option value="243">Virgin Islander-UK</option>
										    <option value="244">Virgin Islander-US</option>
										    <option value="245">Wallisian</option>
										    <option value="198">Welsh</option>
										    <option value="213">Western Saharan</option>
										    <option value="199">Yemenese</option>
										    <option value="200">Zambian</option>
										    <option value="201">Zimbabwean</option>
										    <option value="205">landic</option>
									    </select>
								    </div>
							    </td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="EditBene_aka" name="aka" style="width:100%;" type="text"  value="" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">ID DETAILS</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">RELATIONSHIP WITH SENDER *</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="EditBene_iddetails" name="iddetails" style="width:100%;" type="text" value="" /></div></td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" required="required" id="EditBene_relationship" name="relationshipwithsender" style="width:100%;" type="text"  value=""  /></div></td>
						    </tr>
                            <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
					    </table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
						<table class="tbl_width_560">
							<tr>
								<td class="aa_label_font">COUNTRY</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">STATE/PROVINCE</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">SUBURB/DISTRICT</td>
							</tr>
							<tr>
								<td style="width:175px;"><select id="EditBene_CountryDDL" style="width:100%;" name="D2"></select></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:174px;"><select id="EditBene_stateprovince" style="width:100%;" name="D3"><option></option></select></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:175px;"><select id="EditBene_suburbdistrict" style="width:100%;" name="D4"><option></option></select></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
						<table class="tbl_width_560">
							<tr>
								<td class="aa_label_font">ADDRESS LINE 1</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">ADDRESS LINE 2</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">POST CODE/ZIP CODE</td>
							</tr>
							<tr>
								<td style="width:175px;"><input class="aa_input" id="EditBene_addressline1" name="country" style="width:100%;" type="text" required="required"/></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:174px;"><input class="aa_input" id="EditBene_addressline2" name="stateprovince" style="width:100%;" type="text"/></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:175px;"><input class="aa_input" id="EditBene_postzipcode" name="suburbdistrict" style="width:100%;" type="text"/></td>
							</tr>
                            <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    
                            <tr>
							    <td class="aa_label_font">CONTACT NUMBER</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">EMAIL ADDRESS</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="EditBene_contactnumber" name="contactnumber" style="width:100%;" type="text" value="" /></div></td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="EditBene_email" name="emailaddress" style="width:100%;" type="text"  value=""  /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>
                
                <tr>
					<td>
						<table class="tbl_width_600">
							<tr>
								<td style="text-align: right;"><input class="aa_btn_red" id="btn_edit_Submit" type="button" value="Update"/></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>
			</table>
		</form>

		</div>
	</div> 
<!-- ************************* BUTTON 05 EDIT BENEFICIARY MODAL POPUP END ************************* --> 
    <!-- ************************* BUTTON 06 EDIT ACCOUNT MODAL POPUP START ************************* --> 
	<div id="EditAccount"> 
		<div class="edit_account_form">
			<form method="get" id="editaccountform">
			<table class="style__width_600">
				
				<tr><td>&nbsp;</td></tr>

				<tr>
					<td>
					<table>
						<tr>
							<td class="aa_label_font">COUNTRY *</td>
						</tr>
						<tr>
							<td class="style__width_600"><div>
								<select id="DDL_Edit_Banks_CountryList" name="DDL_Edit_Banks_CountryList" style="width:100%" required="required">
									<option value ="">-- SELECT --</option>
								</select></div></td>
						</tr>
					</table>
					</td>
				</tr>

				<tr><td>&nbsp;</td></tr>

				<tr>
					<td>
						<div id="DIV_Edit_GenericBank" hidden="hidden">
							<table>
						
								<tr>
									<td class="aa_label_font">BANK NAME *</td>
									<td class="aa_label_font">&nbsp;</td>
									<td class="aa_label_font">BRANCH NAME *</td>
								</tr>
						
								<tr>
									<td class="style__width_290"><div><select id="Edit_ddl_Banks" style="width:290px;" required="required"></select></div></td>
									<td class="style__width_20"></td>
									<td class="style__width_290"><div><select id="Edit_ddl_Branches" style="width:290px;" required="required"></select></div></td>
								</tr>
							
							</table>
						</div>

						<div id="DIV_Edit_IndiaBank" hidden="hidden">
							<table>
								<tr>
									<td class="aa_label_font">BANK NAME INDIA*</td>
									<td class="aa_label_font">&nbsp;</td>
									<td class="aa_label_font">BRANCH NAME INDIA*</td>
								</tr>
						
								<tr>
									<td class="style__width_290"><div><select id="Edit_ddl_Banks_India" style="width:290px;" required="required"></select></div></td>
									<td class="style__width_20"></td>
									<td class="style__width_290"><div><select id="Edit_ddl_Branches_India" style="width:290px;" required="required"></select></div></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>

				<tr><td>&nbsp;</td></tr>

				<tr id="TR_Edit_BankInformation">
					<td>
						<div id="DIV-Edit-Beneficiary-Account-01" class="ag_add_benacc_01">
							<table style="width:100%;">
								<tr>
									<td style="width:25%;" class="ag_add_benacc_02">Bank Code:</td>
									<td style="width:75%;" class="ag_add_benacc_03"><div id="DIV_Edit_BankCode"></div></td>
								</tr>
							</table>
						</div>
						<div id="DIV-Edit-Beneficiary-Account-02" class="ag_add_benacc_01">
							<table style="width:100%;">
								<tr>
									<td style="width:25%;" class="ag_add_benacc_02">Branch Code/IFSC:</td>
									<td style="width:75%;" class="ag_add_benacc_03"><div id="DIV_Edit_BranchCode"></div></td>
								</tr>
							</table>
						</div>
						<div id="DIV-Edit-Beneficiary-Account-03" class="ag_add_benacc_01">
							<table style="width:100%;">
								<tr>
									<td style="width:25%;" class="ag_add_benacc_02">MICR: </td>
									<td style="width:75%;" class="ag_add_benacc_03">&nbsp;</td>
								</tr>
							</table>
						</div>
						<div id="DIV-Edit-Beneficiary-Account-04" class="ag_add_benacc_01">
							<table style="width:100%;">
								<tr>
									<td style="width:25%;" class="ag_add_benacc_02">Contact:</td>
									<td style="width:75%;" class="ag_add_benacc_03">&nbsp;</td>
								</tr>
							</table>
						</div>
						<div id="DIV-Edit-Beneficiary-Account-05" class="ag_add_benacc_01">
							<table style="width:100%;">
								<tr>
									<td style="width:25%;" class="ag_add_benacc_02">Branch Address:</td>
									<td style="width:75%;" class="ag_add_benacc_03"><div id="DIV_Edit_BranchAddress"></div></td>
								</tr>
							</table>
						</div>

						
						
						
					</td>
				</tr>

				<tr style="height:25px;"><td>&nbsp;</td></tr>

				<tr id="TR_Edit_AccountInfo">
					<td>
					<table>
						<tr>
							<td class="aa_label_font">ACCOUNT NAME *</td>
							<td class="aa_label_font">&nbsp;</td>
							<td class="aa_label_font">ACCOUNT NUMBER *</td>
						</tr>
						<tr>
							<td class="style__width_393"><div><input class="aa_input" required="required" id="Edit_accountname" name="Edit_accountname" style="width:100%;" type="text" value="" /></div></td>
							<td class="style__width_21"></td>
							<td class="style__width_186"><div><input class="aa_input" required="required" id="Edit_accountnumber" name="Edit_accountnumber" style="width:100%;" type="text"  value=""  /></div></td>
						</tr>
					</table>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>
				<tr><td>
					<table>
						<tr>
							<td class="aa_label_font">TRADING CURRENCY *</td>
							<td class="aa_label_font">&nbsp;</td>
							<td class="aa_label_font">&nbsp;</td>
						</tr>
						<tr>
							<td class="style__width_393">
								<select id="Edit_TradingCurrency" required="required">
									<option value="">-- SELECT --</option>

								</select></td>
							<td class="style__width_21"></td>
							<td class="style__width_186">&nbsp;</td>
						</tr>
					</table>
					</td></tr>
				<tr class="spacer10"><td>&nbsp;</td></tr>
				<tr>
					<td>
						<table class="tbl_width_600">
							<tr>
								<td style="text-align: right;"><input class="aa_btn_red" id="btn_editnewaccountcomplete" type="button" value="Edit Account"/></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

			</table>
			
		</form>

		</div>
	</div>
<!-- ************************* BUTTON 06 EDIT ACCOUNT MODAL POPUP END ************************* --> 
   
    <div id="ViewTransactionDIV"> <!-- THIS IS THE APPROVAL POPUP DIALOG BOX -->
        <table class="tbl_width_600">
        
        <tr><td>&nbsp;</td></tr>
    
        <tr>
            <td>
                <table class="tbl_width_600">
                    <tr style="height:50px;">
                        <td style="width:50px;" class="wiz_tab_active" id="Review_TD_001"><asp:Image ID="Image27" runat="server" ImageUrl="~/images/paid.png" title="TRANSACTION" /></td>
                        <td style="width:50px;" class="wiz_tab_inactive_right" id="Review_TD_002"><asp:Image ID="Image28" runat="server" ImageUrl="~/images/profits.png" title="LIMITS" /></td>
                        <td style="width:50px;" class="wiz_tab_inactive_right" id="Review_TD_003"><asp:Image ID="Image29" runat="server" ImageUrl="~/images/paper.png" title="NOTES" /></td>
                        <td style="width:50px;" class="wiz_tab_inactive_right" id="Review_TD_004"><asp:Image ID="Image30" runat="server" ImageUrl="~/images/folder.png" title="DOCUMENTS" /></td>
                        <td style="width:50px;" class="wiz_tab_inactive_right" id="Review_TD_005"><asp:Image ID="Image31" runat="server" ImageUrl="~/images/audit.png" title="AUDIT" /></td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>    
    
        <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>


        
        <!--<tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr><td class="settings_headings">Transaction Details - <span id="REWV_TransID"></span>&nbsp;<span id="REWV_DateTime"></span></td></tr>
                    <tr class="spacer5"><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>-->
        
        <tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    

                    
                    




                    <tr hidden="hidden" style="background-color:#FBE2E2;">
                        <td>
                            <table class="tbl_width_520" id="TBL_ReviewReasons">
                            </table>
                        </td>
                    </tr>
                    <tr hidden="hidden" class="wiz_seperator_white spacer10"><td>&nbsp;</td></tr>


                    <tr>
                        <td>
                              <section class="container_tags">
    <ul class="tags" id="LST_ReviewReasons">

    </ul>
  </section>


                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr>
                        <td style="background-color:#FFF; height:35px;">
                            <table class="tbl_width_560">
                                <tr style="height:35px;">
                                    <td style="width:15px;">&nbsp;</td>
                                    <td class="txn_sml_headings">TXN REF: <span id="REVW_DisplayTransID" style="font-weight:600;"></span>&nbsp;-&nbsp;<span id="SPN_Status"></span></td>
                                    <td style="width:35px;" class="txn_btns_01">&nbsp;</td>
                                    <td style="width:35px;" class="txn_btns_01">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr class="wiz_bg_EFEFEF spacer10"><td>&nbsp;</td></tr>

        <tr> 
            <td>
                <div id="DIV_CompReview_Main"> <!-- MAIN TAB -->
                    <form method="get" id="reviewtransactionform">
                        <table class="tbl_width_600">

                            <tr hidden="hidden"><td><input id="REVW_TransID" name="REVW_TransID" type="text" /><input id="REVW_CustID" name="REVW_CustID" type="text" /></td></tr>

                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td style="width:275px;">
                                                <table style="width:100%;">
                                                    <tr class="background_FFFFFF"><td class="inside_headings" style="height:40px;">SEND AMOUNT</td></tr>
                                                    <tr class="background_FFFFFF"><td style="padding-left:15px;"><span id="REVW_DollarAmount" class="trn_amounts_gray"></span></td></tr>
                                                    <tr class="background_FFFFFF spacer10"><td>&nbsp;</td></tr>
                                                    <tr style="background-color:#F7F9FA; border-top:1px solid rgb(234, 234, 234);"><td class="txn_popup_footer_01">Transaction Fee: <span id="REVW_ServiceFee" class="txn_popup_fw600"></span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Total Paid: <span id="REVW_AUDAmount" class="txn_popup_fw600"></span></td></tr>
                                                </table>
                                            </td>
                                            <td style="width:10px;">&nbsp;</td>
                                            <td style="width:275px;">
                                                <table style="width:100%;">
                                                    <tr class="background_FFFFFF"><td class="inside_headings" style="height:40px;">RECEIVE AMOUNT</td></tr>
                                                    <tr class="background_FFFFFF"><td style="padding-left:15px;"><span id="REVW_RsAmount" class="trn_amounts_green"></span></td></tr>
                                                    <tr class="background_FFFFFF spacer10"><td>&nbsp;</td></tr>
                                                    <tr style="background-color:#F7F9FA; border-top:1px solid rgb(234, 234, 234);"><td class="txn_popup_footer_01">Exchange Rate: <span id="REVW_Rate" class="txn_popup_fw600"></span></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>




                            <tr class="wiz_bg_EFEFEF spacer10"><td>&nbsp;</td></tr>

                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td style="width:275px; background-color:#FFF;">
                                                <table class="tbl_width_245">
                                                    <tr class="background_FFFFFF"><td class="inside_headings" style="padding-left:0px; height:50px;">SENDER DETAILS</td></tr>
                                                    <tr><td class="txn_popup_h_02">CUSTOMER ID/NAME</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_CustomerID"></span>&nbsp;-&nbsp;<span id="REVW_CustName"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">ADDRESS</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_CustAddress"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">CONTACT NO</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_CustMobile"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">OCCUPATION</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_Occupation"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">PURPOSE OF TRANSACTION</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_Purpose"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">SOURCE OF FUNDS</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_SourceOfFunds"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">NOTES</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_Notes"></span></td></tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                </table>
                                            </td>
                                            <td style="width:10px;">&nbsp;</td>
                                            <td style="width:275px; background-color:#FFF;">
                                                <table class="tbl_width_245">
                                                    <tr class="background_FFFFFF"><td class="inside_headings" style="padding-left:0px; height:50px;">RECEIVER DETAILS</td></tr>
                                                    <tr><td class="txn_popup_h_02">NAME</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_BeneName"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">ADDRESS</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_BeneAddress"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">CONTACT NO</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_BeneMobile"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">RELATIONSHIP</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_BeneRelationship"></span></td></tr>
                                                    <tr><td class="txn_popup_h_02">DESTINATION BANK</td></tr>
                                                    <tr><td class="txn_popup_c_01"><span id="REVW_OriginalBank"></span></td></tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">
                                        

                                        <tr hidden="hidden" class="background_FFFFFF">
                                            <td>
                                                <table class="tbl_width_520">
                                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_520">
                                                                <tr><td class="trn_popup_heading">Receive Option</td><td class="trn_popup_details"><span id="REVW_TransType"></span></td></tr>
                                                                <tr><td class="trn_popup_heading">Destination</td><td class="trn_popup_details"><span id="REVW_Destination"></span></td></tr>
                                                                <tr><td class="trn_popup_heading">Fee</td><td class="trn_popup_details"><span id="REVW_Fee"></span></td></tr>                                                           
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                </table>

                                            </td>
                                        </tr>

                                        <tr><td class="spacer15">&nbsp;</td></tr>
                                        

                                       

                                    </table>
                                </td>
                            </tr>

                        <!-- <tr class="spacer5" id="REVW_TR_RoutingInfo_5px"><td>What is this?</td></tr> -->

                        </table>
                    </form>
                </div>

                <div id="DIV_CompReview_Notes"> <!-- NOTES TAB -->
                    <table class="tbl_width_600">
                        <tr class="wiz_bg_EFEFEF">
                            <td>
                                <table class="tbl_width_560">
                                    <tr><td class="aa_label_font">ADD A NEW NOTE</td></tr>
                                    <tr><td><textarea name="TXT_Add_TRN_Notes" id="TXT_Add_TRN_Notes" style="width:100%; height:55px; resize:none; line-height:16px !important; padding-top:8px;" class="aa_input"></textarea></td></tr>
                                    <tr class="spacer5"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr><td class="ali_right"><input class="aa_btn_green" id="btn_SaveNewNote" type="button" value="Add Note" /></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="wiz_seperator_white spacer10"><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>

                                </table>
                            </td>
                        </tr>
                        
                        <tr class="wiz_bg_EFEFEF">
                            <td>
                                <div id="DIV_Notes" style="overflow-y: scroll; height:300px; border-right:1px solid #DCDCDC;" class="tbl_width_560">
                              
                                </div>
                            </td>
                        </tr>
                        <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                </div>

                <div id="DIV_CompReview_Documents"> <!-- DOCUMENTS TAB -->
                    <table class="tbl_width_600">
                        <tr class="wiz_bg_EFEFEF">
                            <td>
                                <table class="tbl_width_560">

                                    <tr class="background_FFFFFF">
                                        <td>
                                            <table class="tbl_width_530">
                                                <tr><td class="inside_headings" style="padding-left:0px; height:50px;">SUPPORTING DOCUMENTS</td></tr>
                                                <tr><td class="trn_notes_popup_details" style="font-size:0.7em; color:#999;">Please upload any required supporting documents for this transaction below. This may include proof of funds, purpose of transfer, etc. If you need to update/edit any identification documents please do that under customer profile. </td></tr>
                                                <tr><td>&nbsp;</td></tr>
                                            </table>
                                        </td></tr>

                                    <tr><td>&nbsp;</td></tr>

                                    <tr>
                                        <td>
                                            <form id="doc_UploadOtherDocs3" method="get">
                                            <table class="tbl_width_560"> 
                                                <tr>
                                                    <td class="aa_label_font">DOCUMENT DESCRIPTION</td>
                                                    <td class="aa_label_font">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><div><input class="aa_input" id="docdescription3" name="docdescription" style="width:455px;" type="text" value="" /></div></td>
                                                    <td style="text-align:right;"><input type="file" name="UploadFile3" id="UploadFile3" style="display: none;" /><input type="button" class="aa_btn_green" value="Browse..." onclick="document.getElementById('UploadFile3').click();" /></td>
                                                </tr>
                                                <tr class="spacer5"><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                <tr>
                                                    <td class="trn_notes_popup_details" style="font-size:0.7em; color:#999;">Selected File: <span id="otherdoctext3" style="color:#666;">Please browse for a file...</span></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                <tr>
                                                    <td><input class="aa_btn_red" id="btn_UploadOtherFiles3" type="button" value="Upload File"/></td>
                                                    <td style="text-align:right;">&nbsp;</td>
                                                </tr>
                                            </table>
                                         </form>
                                        </td>
                                    </tr>
                                    
                                    <tr class="wiz_seperator_white spacer10"><td>&nbsp;</td></tr>
                                    
                                    <tr><td>&nbsp;</td></tr>

                                    <tr><td class="trn_notes_popup_heading">Supporting Documents</td></tr>
                                    
                                    <tr><td>&nbsp;</td></tr>

                                    <tr>
                                        <td>
                                            <table id="REWV_Docs">
                                                <thead>
                                                    <!-- <td>Date</td> INCLUDE THIS -->
                                                    <td>DOCUMENT DESCRIPTION</td>
                                                    <td>VIEW</td>
                                                </thead>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr class="wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="DIV_CompReview_Limits"> <!-- LIMITS TAB -->
                    <table class="tbl_width_600">
                        <tr class="wiz_bg_EFEFEF">
                            <td>
                                <table class="tbl_width_560">
                                    <tr class="background_FFFFFF" style="height:150px;">
                                        <td style="width:275px;">
                                            <table class="tbl_width_245">
                                                <tr class="background_FFFFFF"><td class="inside_headings" style="height:50px; padding-left:0px;">MONTHLY AUD LIMIT</td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;"><span id="Last30daysDollar"></span> / <span id="ALast30daysDollar"></span></td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                            </table>
                                        </td>
                                        <td style="width:10px;" class="wiz_bg_EFEFEF">&nbsp;</td>
                                        <td style="width:275px;">
                                            <table class="tbl_width_230">
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr>
                                                    <td class="widget_heading">LAST YEAR</td>
                                                </tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;"><span id="LastYearDollar"></span> / <span id="ALastYearDollar"></span></td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr class="background_FFFFFF" style="height:150px;">
                                        <td style="width:270px;">
                                            <table class="tbl_width_230">
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr>
                                                    <td class="widget_heading">LAST MONTH</td>
                                                </tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;"><span id="Last30daysNb"></span> / <span id="ALast30daysNb"></span></td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                            </table>
                                        </td>
                                        <td style="width:20px;" class="wiz_bg_EFEFEF">&nbsp;</td>
                                        <td style="width:270px;">
                                            <table class="tbl_width_230">
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr>
                                                    <td class="widget_heading">LAST YEAR</td>
                                                </tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;"><span id="LastYearNb"></span> / <span id="ALastYearNb"></span></td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                    
                </div>
                <div id="DIV_CompReview_Audit">
                        <!-- AUDIT TAB -->
                        <table class="tbl_width_600">
                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr class="background_FFFFFF">
                                            <td class="inside_headings" style="padding-left:15px; height: 50px;">TRANSACTION AUDIT TRAIL</td>
                                        </tr>
                                        <tr class="background_FFFFFF">
                                            <td>
                                                <table class="tbl_width_530">
                                                    <tr>
                                                        <td>
                                                            <table id="TBL_REWV_Audit">
                                                                <thead>
                                                                    <tr>
                                                                        <td>Record Date</td>
                                                                        <td>Audit Record</td>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="background_FFFFFF spacer15"><td>&nbsp;</td></tr>
                                        <tr><td class="txn_popup_c_01">Created Date: <span id="REVW_CreatedDateTime"></span>&nbsp;&nbsp;|&nbsp;&nbsp;Completed Date: 17/11/2017 02:31:05PM</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>
                        </table>


                    </div>
            </td>
        </tr>

        <tr class="spacer10" hidden="hidden"><td><input type="text" id="REVW_CountryID" hidden="hidden" /></td></tr>
        
        <tr>
            <td>
                <table class="tbl_width_600">
                    <tr>
                        <td style="text-align:right;"><input class="aa_btn_red" id="BTN_CloseReviewWindow" type="button" value="Close"/></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="spacer10"><td>&nbsp;</td></tr>

    </table>
    </div>

    <div id="DIV_AddNewBankAccount">
        <div id="DIV_AddFrameLoader">

        </div>
    </div>


    <div id="DIV_DuplicateTransaction">
        A similar transaction was entered today for this customer, beneficiary, account and amount. This could be a duplicate transaction. Do you want to proceed with this transaction?<br />
        <input id="btn_DuplicateYES" type="button" value="YES" />&nbsp;<input id="btn_DuplicateNO" type="button" value="NO" />
    </div>

    <div id="DIV_AddNewBankAccountCountrySelector">
        <form method="get" id="addnewbankaccountcountry">
            <table class="tbl_width_440">
                <tr><td>&nbsp;</td></tr>
                <tr>
					<td>
                        <table class="tbl_width_440 wiz_bg_EFEFEF">
                            <tr>
                                <td>
                                    <table class="tbl_width_400">
						                <tr style="height:20px;"><td>&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="width:330px;"><div><select id="DDL_Banks_CountryList" name="DDL_Banks_CountryList" style="width:100%" required="required"></select></div></td>
                                                        <td style="width:10px;">&nbsp;</td>
                                                        <td style="width:60px;"><input class="aa_btn_green" type="button" value="Next" id="BTN_Go" /></td>
                                                    </tr>
                                                </table>
                                            </td>
						                </tr>
                                        <tr style="height:20px;"><td>&nbsp;</td></tr>
					                </table>
                                </td>
                            </tr>
                        </table>
					</td>
				</tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
            </table>
        </form>
    </div>

    <div id="DIV_Calculator">
        <table>
            <tr>
                <td>Select Currency: <select id="SEL_Currency">
                        <option></option>

                    </select> @ <input id="txt_Rate" type="text" /></td>
            </tr> 
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>AUD: <input id="txt_AUDValue" type="text" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><span id="SPN_CurrencyCode"></span>: <input id="txt_FValue" type="text" /></td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">

        function callLoadingDiv() {
           // $("#buttonyesKychidden").val("Y");
            $("#DIV_Loading").show();
        }



    </script>
</asp:Content>
