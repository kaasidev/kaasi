﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentControl/Initial.Master" AutoEventWireup="true" CodeBehind="BankingAG.aspx.cs" Inherits="KASI_Extend_.BankingAG" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.fileDownload.js"></script>
    
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />
     <link href="css/toastr.min.css" rel="stylesheet" />


	<script src="js/toastr.min.js"></script>
    <script type="text/javascript">
        function Close() {
            $('[id$=SetDefaultBank]').hide();
            $('[id$=mask]').hide();
        }

        function clearDataTable() {
            MainDataTable.clear().draw();
        }

        function openDataTable() {
            $('#TR_TransTable').show();
            //TblData.destroy();
            var MainDataTable = $('#TBL_ShowTransaction').DataTable({
                columns: [
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'TransactionID' },
                    { 'data': 'CustomerID' },
                    { 'data': 'BeneficiaryName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'Rate' },
                    { 'data': 'RemittedAmount' },
                    { 'data': 'RemittedCurrency' },
                    { 'data': 'BankName' },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: "DataHandlers/AGENTAwaitingTransferDataHandler.ashx?BankID=" + $('#ContentMainSection_BankID').val() + "&CCode=" + $('[id$=CurrencyList]').val() + "&AgentID=" + $('#ContentMainSection_hidden_AgentID').val(),

            });

        }
        
        
        $(document).ready(function () {

            

            //TblData = $('#TBL_ShowTransaction').DataTable({});
            $('#TR_TransTable').hide();
          //  DialogDefaultBank = $('[id$=SetDefaultBank]').dialog({
          //      autoOpen: false,
          //      modal: true,
          //      width: 710,
          //  });

            $('#btn_setdefaultview').click(function () {
                DialogDefaultBank.dialog('open');
                event.preventDefault();
            });
           
        });
        function showerror(msg) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.error(msg, 'Error');

        }
        function showsuccess(msg, body) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.success(msg, body);

        }

        
    </script>

    <style type="text/css">
        .auto-style1 {
            font-family: 'Varela Round', sans-serif;
            font-size: 0.6rem;
            color: #919aa0;
            padding-bottom: 3px;
            text-align: left;
            height: 19px;
        }
        .auto-style2 {
            height: 24px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="Form1" runat="server">

<!-- TO BE DELETED ***

    <div class="container_btns">
        <table class="tbl_width_1200">
            <tr>
                <td class="all_main_page_headings">BANKING</td>
                <td style="text-align:right;">
                    
                   <%-- <input class="aa_btn_red" id="btn_setdefaultview" type="button" value="Set Default Bank View"/>--%></td>
            </tr>
        </table>
    </div>
*** UP TO HERE -->

    <div class="container_top_headings">
        <table class="tbl_width_1200">
            <tr style="height:70px; vertical-align:middle;">
                <td style="vertical-align:middle;"><span class="all_headings">Banking</span></td>
                <td style="vertical-align:middle; text-align:right;"><asp:Button CssClass="aa_btn_red" runat="server" OnClick="Unnamed_Click" Text="Set Default Bank View" /></td>
            </tr>
        </table>
    </div>


        <asp:HiddenField ID="hidden_AgentID" runat="server" />
        <asp:HiddenField ID="hidden_BankID" runat="server" />
        <asp:HiddenField ID="CreateBankFileName" runat="server" />
        
        
        
        <div hidden="hidden">
            <table class="tbl_width_1200">
                <tr>
                    <td style="width:720px;">&nbsp;</td>
                    <td style="width:480px; background-color:#FFF;">
                        <table class="tbl_width_440">
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_440">
                                        <tr>
                                            <td style="width:210px;">
                                                <div>
                                                    <select id="EDIT_SSLEnabled" runat="server" class="bank_select" style="width:100%;">
                                                        <option value="">--Select--</option>
	                                                    <option value="1">Yes</option>
	                                                    <option value="2">No</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:210px;">
                                                <div>
                                                    <select id="Select1" runat="server" class="bank_select" style="width:100%;">
                                                        <option value="">--Select--</option>
	                                                    <option value="1">Yes</option>
	                                                    <option value="2">No</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_440">
                                        <tr>
                                            <td style="width:210px;">
                                                <table>
                                                    <tr>
                                                        <td style="width:25px; vertical-align:middle;"><asp:Image ID="Image3" runat="server" ImageUrl="~/images/account.png" title="ACCOUNT" /></td>
                                                        <td style="width:185px;">
                                                            <div>
                                                                <select id="Select2" runat="server" class="bank_select" style="width:100%;">
                                                                    <option value="">--Select--</option>
	                                                                <option value="1">Yes</option>
	                                                                <option value="2">No</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:210px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>


        <div class="container_one no_padding_top_btm">
             
            <table class="tbl_width_1200">
                <tr style="height:20px;"><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td style="width:280px; vertical-align:top;">
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td style="background-color:#76D17F; height:250px; vertical-align:top;">
                                                <table class="tbl_width_240">
                                                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td class="bank_sum_heading">AWAITING TRANSFER SUMMARY</td>
                                                    </tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_240">
                                                                <tr>
                                                                    <td style="width:118px; background-color:#8fe096;">
                                                                        <table style="width:115px;">
                                                                            <tr><td class="height:20px;">&nbsp;</td></tr>
                                                                            <tr><td class="bank_sum_numbers">
                                                                                <asp:Label ID="lbl_TotalTXNS" runat="server" Text="Lbl"></asp:Label></td></tr>
                                                                            <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                            <tr><td class="bank_sum_label">TOTAL TXNS</td></tr>
                                                                            <tr><td class="height:20px;">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width:4px;">&nbsp;</td>
                                                                    <td style="width:118px; background-color:#8fe096;">
                                                                        <table style="width:115px;">
                                                                            <tr><td class="height:20px;">&nbsp;</td></tr>
                                                                            <tr><td class="bank_sum_numbers">
                                                                                <asp:Label ID="lbl_TotalAUD" runat="server" Text="Lbl"></asp:Label></td></tr>
                                                                            <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                            <tr><td class="bank_sum_label">TOTAL AUD</td></tr>
                                                                            <tr><td class="height:20px;">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr><td>&nbsp;</td></tr>

                                                    <tr>
                                                        <td class="auto-style2">
                                                            <table>
                                                                <tr><td>

                                                                    <asp:Table ID="tbl_AllBankStats" runat="server"></asp:Table>
                                                                    </td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        
                                        
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </table>
                                </td>

                                <td style="width:20px;">&nbsp;</td>

                                <td style="width:900px;">

                                    <div class="container_one no_padding_top_btm" id="div_bank01">
                                        <table class="tbl_width_900">
                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_860">
                                                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_860">
                                                                    <tr style="height:30px;">
                                                                        <td style="width:30px; background-color:#D2E9F8; text-align:center; vertical-align:middle; padding-top:4px;"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/planet-earth.png" title="COUNTRY" /></td>
                                                                        <td style="width:248px;"><asp:DropDownList ID="DDL_CountryList" class="bank_select_top_drops" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDL_CountryList_Change"></asp:DropDownList></td>
                                                                        <td style="width:18px;">&nbsp;</td>
                                                                        <td style="width:30px; background-color:#D2E9F8; text-align:center; vertical-align:middle; padding-top:4px;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/bank.png" title="BANK" /></td>
                                                                        <td style="width:248px;"><asp:DropDownList ID="DDL_BankList" class="bank_select_top_drops" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDL_BankList_Change"></asp:DropDownList></td>
                                                                        <td style="width:18px;">&nbsp;</td>
                                                                        <td style="width:30px; background-color:#D2E9F8; text-align:center; vertical-align:middle; padding-top:4px;"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/account.png" title="ACCOUNT" /></td>
                                                                        <td style="width:248px;"><asp:DropDownList ID="DDL_AccountList" class="bank_select_top_drops" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDL_AccountList_Change"></asp:DropDownList></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                                                        <tr style="height:20px;" hidden="hidden"><td><input type="text" id="BankID" runat="server" /><input type="text" id="CurrencyList" runat="server" /></td></tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr><td>&nbsp;</td></tr>

                                            <tr>
                                                <td>
                                                    <table style="width:100%;">
                                                        <tr style="height:20px; background-color:#FFF;"><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td class="bank_bank_headings" id="BankName" runat="server"></td>
                                                            <td class="bank_bank_headings" style="text-align:right !important; padding-right:20px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_860">
                                                        <tr>
                                                            <td style="width:307px; background-color:#FFFFFF;">
                                                                <table>
                                                                    <tr><td class="bank_aac_heading">Account No:</td></tr>
                                                                    <tr><td class="bank_aac_number"><asp:Label ID="LBL_AccountNumber" runat="server" ></asp:Label></td></tr>
                                                                    <tr><td><asp:Table ID="Tbl_Currencies" runat="server"></asp:Table></td></tr>
                                                                </table>
                                                                
                                                                
                                                            </td>
                                                            <td style="width:20px;">&nbsp;</td>
                                                            <td style="width:125px; background-color:#F8F8F8;">
                                                                <table class="tbl_width_125">
                                                                    <tr style="width:20px;"><td>&nbsp;</td></tr>
                                                                    <tr><td class="bank_txnttl_numbers"><asp:Label ID="lbl_AccTotalTrans" runat="server" ></asp:Label></td></tr>
                                                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                    <tr><td class="bank_txnttl_label">TOTAL TXNS</td></tr>
                                                                    <tr style="width:20px;"><td>&nbsp;</td></tr>
                                                                </table>
                                                            </td>
                                                            <td style="width:4px;">&nbsp;</td>
                                                            <td style="width:200px; background-color:#F8F8F8;">
                                                                <table class="tbl_width_125">
                                                                    <tr style="width:20px;"><td>&nbsp;</td></tr>
                                                                    <tr><td class="bank_txnttl_numbers"><asp:Label ID="lbl_AccTotalAUD" runat="server" ></asp:Label></td></tr>
                                                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                    <tr><td class="bank_txnttl_label">TOTAL AUD</td></tr>
                                                                    <tr style="width:20px;"><td>&nbsp;</td></tr>
                                                                </table>
                                                            </td>


                                                                
                                                            <td style="width:4px;">&nbsp</td>
                                                            <td style="width:200px; background-color:#8fe096;">
                                                                <table class="tbl_width_160">
                                                                    <tr><td>&nbsp;</td></tr>
                                                                    <tr><td class="bank_txnttl_numbers2"><asp:Table ID="tbl_IndiCurr" runat="server"></asp:Table></td></tr>
                                                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                    <tr><td class="bank_txnttl_label2">TOTAL SENDING</td></tr>
                                                                    <tr style="width:20px;"><td>&nbsp;</td></tr>
                                                                </table>


                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="background_FFFFFF" style="height:20px;"><td>&nbsp;</td></tr>

                                            <!--<tr><td>&nbsp;</td></tr>-->

                                            <tr>
                                                <td style="background-color:#F8F8F8;">
                                                    <table class="tbl_width_860">
                                                        <tr class="spacer10" style="background-color:#F8F8F8;"><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_860">
                                                                    <tr>
                                                                        <td class="bank_file_created">
                                                                            <asp:Label ID="lbl_Message" runat="server"></asp:Label> File last created: <asp:Label ID="lbl_LastFileCreation" runat="server" Text="Lbl"></asp:Label></td>
                                                                        <td style="text-align:right;"><asp:Button ID="btn_CreateBankFile" runat="server" class="aa_btn_green" Text="Create Bank File" OnClick="btn_CreateBankFile_Click1" /></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr style="height:20px;"><td>&nbsp;</td></tr>

                                            <tr >
                                                <td>
                                                    <div id="DIV_BankTransactionsTbl" class="background_FFFFFF">
                                                        <table class="tbl_width_860">
                                                            <tr><td style="height:20px;">&nbsp;</td></tr>
                                                            <tr>
                                                                <td><table id="TBL_ShowTransaction">
                                                                        <thead>
                                                                            <tr>
                                                                                <td>Transaction Date</td>
                                                                                <td>Transaction ID</td>
                                                                                <td>Customer ID</td>
                                                                                <td>Beneficiary Name</td>
                                                                                <td>Dollar Amount</td>
                                                                                <td>Rate</td>
                                                                                <td>Remitted Amount</td>
                                                                                <td>Currency</td>
                                                                                <td>Final Bank</td>
                                                                            </tr>
                                                                            
                                                                        </thead>
                                                                    </table></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                </td>
                                            </tr>

                                        </table>
                                    </div>



                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height:20px;"><td>&nbsp;</td></tr>
            </table>
        </div>

        
        <div class="ui-widget-overlay ui-front" style="display:none; background-color:#76D17F;" id="mask" runat="server"></div>

        <div id="SetDefaultBank" runat="server" class="serverModel">
            <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle"><span id="ui-id-6" class="ui-dialog-title">Set Default Values</span>
                <button onclick="Close()" style="float:right" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" title="Close">
                <span class="ui-button-icon-primary ui-icon ui-icon-closethick" style="background-image:url('images/ui-icons_444444_256x240.png')"></span>
                <span class="ui-button-text">Close</span></button>
            </div>
    
            <table class="tbl_width_600">

                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image21" runat="server" ImageUrl="~/images/bank.png" title="BANK DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <table class="tbl_width_560">
                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">COUNTRY</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">BANK</td>
                                        </tr>
                                        <tr>
                                            <td style="width:270px;"><div>
                                              
                                                <asp:DropDownList ID="DDL_Edit_CountryList" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="DDL_Edit_CountryList_Change"></asp:DropDownList></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:270px;"><div>
                                               <asp:DropDownList ID="DDL_Edit_BankList" AutoPostBack="true" runat="server"   OnSelectedIndexChanged="DDL_Edit_BankList_SelectedIndexChanged"></asp:DropDownList></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="auto-style1">ACCOUNT</td>
                                            <td class="auto-style1"></td>
                                            <td class="auto-style1"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:270px;"><div>
                                                <asp:DropDownList ID="DDL_Edit_AccountList" runat="server"></asp:DropDownList></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:270px;">
                                                <asp:Button ID="BTN_SaveSettings" runat="server" Text="SAVE" OnClick="BTN_SaveSettings_Click" />&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>




            </table>
        
            
        </div>


    </form>
</asp:Content>
