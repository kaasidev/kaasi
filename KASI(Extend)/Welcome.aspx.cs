﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using KASI_Extend_.classes;
using System.Web.Services;
using System.Web.Script.Services;
using Kapruka.Service;

namespace KASI_Extend_
{
    public partial class Welcome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SPN_TodayDate.InnerText = DateTime.Now.ToString("dd MMMM yyyy");

            classes.Logins LGS = new Logins();
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            Button1.Visible = true;
            if (!IsPostBack)
            {
                string query = "SELECT TOP 10 SUM(RemittedAmount) FROM dbo.Transactions WHERE Status IN ('AWAITING TRANSFER', 'SUCCESSFUL')";
                DataTable dt = GetData(query);
                string[] x = new string[dt.Rows.Count];
                int[] y = new int[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // x[i] = "Remitted Value";
                    // y[i] = Convert.ToInt32(dt.Rows[i][0]);
                }
                //TotalRemitValue.Series[0].Points.DataBindXY(x, y);
                //TotalRemitValue.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar;
                //TotalRemitValue.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                //TotalRemitValue.Legends[0].Enabled = true;

                
                buildExchangeRateTable();

                TotalTansSPN.InnerText = findTotalDailyTransactions().ToString();
                CompTransSPN.InnerText = findTotalDailyCompletedTransactions().ToString();
                PendTransSPN.InnerText = findPendingTransactions().ToString();
                BlockTransSPN.InnerText = findBlockedTransactions().ToString();
                SPN_LoggedUser.InnerText = LGS.getLoginFirstName(Session["LoggedUserID"].ToString());

            }
            CheckBankAccounts();
        }



        private static DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(query);
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            SqlConnection conn = new SqlConnection(constr);
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;
            sda.SelectCommand = cmd;

            sda.Fill(dt);
            conn.Close();
            return dt;
        }

        private void CheckBankAccounts()
        {
            List<String> DepositMethods = new List<String>();
            classes.MasterBankClass MBC = new MasterBankClass();
            float BankTotal = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BankAccountID FROM dbo.MasterCompanyBanks";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            DepositMethods.Add(sdr["BankAccountID"].ToString());
                        }
                    }
                    conn.Close();
                }
            }


            foreach (var method in DepositMethods)
            {
                using (SqlConnection conn2 = new SqlConnection())
                {
                    conn2.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "SELECT SUM(CreditAmount) AS CTotal FROM dbo.CreditTransactions WHERE MasterBankAccountID = '" + method + "' AND CAST(CreatedDateTime AS DATE) = CAST(getDate() AS DATE) AND Type = 'CREDIT'";
                        cmd.Connection = conn2;
                        conn2.Open();

                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            if (sdr.HasRows)
                            {
                                while (sdr.Read())
                                {
                                    if (sdr["CTotal"].ToString() == "")
                                    {
                                        BankTotal = 0;
                                    }
                                    else
                                    {
                                        BankTotal = float.Parse(sdr["CTotal"].ToString());
                                    }
                                }
                            }
                            else
                            {
                                BankTotal = 0;
                            }
                            conn2.Close();
                        }
                    }
                }

                TableRow row = new TableRow();
                TableCell cell = new TableCell();
                cell.Text = MBC.getMasterBankNameByAccountID(method);
                cell.CssClass = "welcome_daily_deposits_03";
                row.Cells.Add(cell);
                //TableCell cell2 = new TableCell();
                //cell2.Text = "&nbsp;";
                //row.Cells.Add(cell2);
                TableCell cell3 = new TableCell();
                cell3.Text = String.Format("{0:C2}", float.Parse(BankTotal.ToString()));
                cell3.CssClass = "welcome_daily_deposits_04";
                row.Cells.Add(cell3);
                BankMonies.Rows.Add(row);
            }

        }

        protected void RefreshAccounts_Click(object sender, EventArgs e)
        {
            TBL_ExchangeRates.Controls.Clear();
            buildExchangeRateTable();
        }


        private int findTotalDailyTransactions()
        {
            int DailyTotal = 0;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) AS THETOTAL FROM dbo.Transactions WHERE CONVERT(varchar(10), CreatedDateTime, 103) = CONVERT(varchar(10), GETDATE(), 103)";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                DailyTotal = Int32.Parse(sdr["THETOTAL"].ToString());
                            }
                        }
                    }
                }
                conn.Close();
            }

            return DailyTotal;
        }

        private int findTotalDailyCompletedTransactions()
        {
            int DailyTotal = 0;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) AS THETOTAL FROM dbo.Transactions WHERE Status = 'SUCCESSFUL' AND CONVERT(varchar(10), CreatedDateTime, 103) = CONVERT(varchar(10), GETDATE(), 103)";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                DailyTotal = Int32.Parse(sdr["THETOTAL"].ToString());
                            }
                        }
                    }
                }
                conn.Close();
            }

            return DailyTotal;
        }

        private int findPendingTransactions()
        {
            int DailyTotal = 0;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) AS THETOTAL FROM dbo.Transactions WHERE Status = 'PENDING'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                DailyTotal = Int32.Parse(sdr["THETOTAL"].ToString());
                            }
                        }
                    }
                }
                conn.Close();
            }

            return DailyTotal;
        }

        private int findBlockedTransactions()
        {
            int DailyTotal = 0;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) AS THETOTAL FROM dbo.Transactions WHERE Status = 'COMPLIANCE'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                DailyTotal = Int32.Parse(sdr["THETOTAL"].ToString());
                            }
                        }
                    }
                }
                conn.Close();
            }

            return DailyTotal;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String TID = "305129";

            classes.EmailCustomer EMCS = new classes.EmailCustomer();
            EMCS.SendEmailWhenSaved(TID);
        }

        protected void buildExchangeRateTable()
        {
            List<String> CurrencyIDs = new List<String>();
            classes.Currencies CUR = new classes.Currencies();
            classes.MasterCompany MST = new classes.MasterCompany();

            CurrencyIDs = CUR.getTop10CurrenciesForMasterCompany();

            foreach(var currency in CurrencyIDs)
            {
                TableRow trow = new TableRow();
                TableCell tcell1 = new TableCell();
                TableCell tcell2 = new TableCell();
                TableCell tcell3 = new TableCell();
                TableCell tcell4 = new TableCell();

                tcell1.Text = "<img src='" + CUR.getCurrencyFlag(currency) + "'/>";
                tcell1.Attributes.Add("class", "welcome_ex_rate_001");
                tcell2.Text = CUR.getCurrencyCodeFromID(currency);
                tcell2.Attributes.Add("class", "welcome_ex_rate_002");
                tcell3.Text = CUR.getCurrencyNameFromID(currency);
                tcell3.Attributes.Add("class", "welcome_ex_rate_003");
                tcell4.Text = String.Format("{0:N4}", float.Parse(MST.getLastCurrencyBuyRate(currency)));
                tcell4.Attributes.Add("class", "welcome_ex_rate_004");
                trow.Controls.Add(tcell1);
                trow.Controls.Add(tcell2);
                trow.Controls.Add(tcell3);
                trow.Controls.Add(tcell4);
                TBL_ExchangeRates.Controls.Add(trow);
            }

            var CurrencyIDs2 = CUR.getAllCurrenciesForMasterCompany();
            foreach (var currency in CurrencyIDs2)
            {
                TableRow trow = new TableRow();
                TableCell tcell1 = new TableCell();
                TableCell tcell2 = new TableCell();
                TableCell tcell3 = new TableCell();
                TableCell tcell4 = new TableCell();

                tcell1.Text = "<img src='" + CUR.getCurrencyFlag(currency) + "'/>";
                tcell1.Attributes.Add("class", "welcome_ex_rate_001");
                tcell2.Text = CUR.getCurrencyCodeFromID(currency);
                tcell2.Attributes.Add("class", "welcome_ex_rate_002");
                tcell3.Text = CUR.getCurrencyNameFromID(currency);
                tcell3.Attributes.Add("class", "welcome_ex_rate_003");
                tcell4.Text = String.Format("{0:N4}", float.Parse(MST.getLastCurrencyBuyRate(currency)));
                tcell4.Attributes.Add("class", "welcome_ex_rate_004");
                trow.Controls.Add(tcell1);
                trow.Controls.Add(tcell2);
                trow.Controls.Add(tcell3);
                trow.Controls.Add(tcell4);
                TBL_AllExchangeRates.Controls.Add(trow);
            }


        }

        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static CurrencyApiResult GetCurrencyRatesApi()
        {
            var obj = new CurrencyApiResult();
            try
            {
               // obj = new CurrencyHandlerService().GetCurrency();
            }
            catch
            {
                return obj;
            }

            return obj;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect(@"Inward\importIFTI.aspx");
        }
    }
}