﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using KASI_Extend_.classes;
using System.Globalization;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend_
{
    public partial class AgentW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {

                DateTime today = DateTime.Today;
                ag_date.InnerText = today.ToString("D", CultureInfo.CreateSpecificCulture("en-US"));
                DisplayAgentInfo();
                CheckBankAccounts();
                showAgentStatus(Session["AgentID"].ToString());
                
                // BuildEditRates(Session["AgentID"].ToString());
            }
            buildCurrencyDisplay(Session["AgentID"].ToString());
        }

        private static DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(query);
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            SqlConnection conn = new SqlConnection(constr);
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;
            sda.SelectCommand = cmd;

            sda.Fill(dt);
            conn.Close();
            return dt;
        }

        private void CheckBankAccounts()
        {
            List<String> DepositMethods = new List<String>();
            classes.MasterBankClass MBC = new MasterBankClass();
            float BankTotal = 0;


            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"SELECT MCB.BankAccountID FROM dbo.AgentAssignedBanks AAB INNER JOIN dbo.MasterCompanyBanks MCB
                                        ON AAB.MasterBankAccountID = MCB.BankAccountID WHERE AgentID = " + Session["AgentID"].ToString();
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            DepositMethods.Add(sdr["BankAccountID"].ToString());
                        }
                    }
                    conn.Close();
                }
            }


            foreach (var method in DepositMethods)
            {
                using (SqlConnection conn2 = new SqlConnection())
                {
                    conn2.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "SELECT SUM(CreditAmount) AS CTotal FROM dbo.CreditTransactions WHERE AgentID = " + Session["AgentID"].ToString() + " AND MasterBankAccountID = '" + method + "' AND CAST(CreatedDateTime AS DATE) = CAST(getDate() AS DATE) AND Type = 'CREDIT'";
                        cmd.Connection = conn2;
                        conn2.Open();

                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            if (sdr.HasRows)
                            {
                                while (sdr.Read())
                                {
                                    if (sdr["CTotal"].ToString() == "")
                                    {
                                        BankTotal = 0;
                                    }
                                    else
                                    {
                                        BankTotal = float.Parse(sdr["CTotal"].ToString());
                                    }
                                }
                            }
                            else
                            {
                                BankTotal = 0;
                            }
                            conn2.Close();
                        }
                    }
                }

                ADT.InnerText = String.Format("{0:C2}", float.Parse(BankTotal.ToString()));
                AName.InnerText = MBC.getMasterBankNameByAccountID(method);
                Anumber.InnerText = MBC.getMasterBankAccountNumberByAccountID(method);

                //TableRow row = new TableRow();
                //TableCell cell = new TableCell();
                //cell.Text = MBC.getMasterBankNameByAccountID(method);
                //cell.CssClass = "welcome_daily_deposits_03";
                //row.Cells.Add(cell);
                ////TableCell cell2 = new TableCell();
                ////cell2.Text = "&nbsp;";
                ////row.Cells.Add(cell2);
                //TableCell cell3 = new TableCell();
                //cell3.Text = String.Format("{0:C2}", float.Parse(BankTotal.ToString()));
                //cell3.CssClass = "welcome_daily_deposits_04";
                //row.Cells.Add(cell3);
                //BankMonies.Rows.Add(row);
            }

        }

        protected void RefreshAccounts_Click(object sender, EventArgs e)
        {
            CheckBankAccounts();
        }

        private void showAgentStatus(String AID)
        {
            classes.Agent agent = new classes.Agent();
            spannbtrans.InnerText = agent.getAgentNumberOfTransactionsDaily(AID);
            float PSend = float.Parse(agent.getAgentPendingTotal(AID));
            spanaudpendingamount.InnerText = String.Format("{0:C2}", PSend);
            float ASend = float.Parse(agent.getAgentSentTotalToday(AID));
            spanaudsentamount.InnerText = String.Format("{0:C2}", ASend);
            spannbpendtrans.InnerText = agent.getAgentNbPendingTrans(AID);
            spannbreviewtrans.InnerText = agent.getAgentnbReviewTrans(AID);
            spanaudreviewamount.InnerText = String.Format("{0:C2}", float.Parse(agent.getAgentReviewTotal(AID)));
            TXT_firstTimeLogin.Value = agent.getAgentFirstTimeLoginStatus(AID);

        }

        private void DisplayAgentInfo()
        {
            if (Session["AgentName"] != null)
                AgentNameDIV.InnerText = "Hello " + Session["AgentName"];
            if (Session["AgentID"] != null)
                hidden_agentid.Value = Session["AgentID"].ToString();
        }

        private void buildCurrencyDisplay(String AID)
        {
            List<Int32> AGTCurrencies = new List<Int32>();
            AGTCurrencies = AgentCurrencies(AID);
            Currencies CR = new Currencies();

            foreach (var Currency in AGTCurrencies)
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                TableCell cell3 = new TableCell();
                TableCell cell4 = new TableCell();
                TableCell cell5 = new TableCell();
                cell1.Text = CR.getCurrencyCodeFromID(Currency.ToString());
                String GivenRate = CR.getAgentCurrencyRateFromAIDandCID(AID, Currency.ToString());
                String GRCalc = String.Empty;
                if (GivenRate != "")
                {
                    cell2.Text = String.Format("{0:N4}", float.Parse(GivenRate));
                    GRCalc = GivenRate;
                }
                else
                {
                    cell2.Text = String.Format("{0:N4}", 0.00);
                    GRCalc = "0";
                }
                String OwnRate = CR.getAgentOwnRateFromAIDandCID(AID, Currency.ToString());
                String ORCalc = String.Empty;

                if (OwnRate != "")
                {
                    cell3.Text = String.Format("{0:N4}", float.Parse(OwnRate));
                    ORCalc = OwnRate;
                }
                else
                {
                    cell3.Text = String.Format("{0:N4}", 0.00);
                    ORCalc = "0";

                }

                cell4.Text = String.Format("{0:N4}", CR.getDifferenceBetweenSetandOwn(GRCalc, ORCalc));
                cell5.Text = String.Format("<img src='" + CR.getCurrencyFlag(Currency.ToString()) + "' />");
                cell5.CssClass = "ag_dash_currency_rows1";
                cell1.CssClass = "ag_dash_currency_rows1";
                cell2.CssClass = "ag_dash_currency_rows2";
                cell3.CssClass = "ag_dash_currency_rows2";
                cell4.CssClass = "ag_dash_currency_rows2";
                row.Cells.Add(cell5);
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                row.Cells.Add(cell3);
                row.Cells.Add(cell4);
                tbl_Currencies.Rows.Add(row);
            }

        }

        private List<Int32> AgentCurrencies(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<Int32> output = new List<Int32>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.AgentCurrencies WHERE AgentID = " + AID + " AND CurrencyID <> 10";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output.Add(Int32.Parse(sdr["CurrencyID"].ToString()));
                    }
                }

                conn.Close();
            }

            return output;
        }

        private void BuildEditRates(String AID)
        {
            classes.Agent AGT = new classes.Agent();
            List<String> AgentCurrencies = new List<String>();
            AgentCurrencies = AGT.getAgentCurrencies(AID);
            classes.Currencies CRC = new classes.Currencies();
            Generic GRC = new Generic();
            TextBox KRate, MyRate, Margin;
            HiddenField hidCurId;
            int counter = 0;


            foreach (var currency in AgentCurrencies)
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                TableCell cell3 = new TableCell();
                TableCell cell4 = new TableCell();
                TableCell cell5 = new TableCell();
                TableCell cell6 = new TableCell();
                TableCell cell7 = new TableCell();

                String CurrencyCode = CRC.getCurrencyCodeFromID(currency);
                cell1.Text = CurrencyCode;
                cell1.CssClass = "ad_set_rate_001";
                cell2.Text = "";

                String GivenRate = CRC.getAgentCurrencyRateFromAIDandCID(AID, currency);

                KRate = new TextBox();
                KRate.ID = "TXT_KaasiRate" + CurrencyCode;
                KRate.CssClass = "aa_input ad_set_rate_002";
                KRate.TextMode = TextBoxMode.SingleLine;
                KRate.Visible = true;
                KRate.Text = String.Format("{0:N2}", float.Parse(GivenRate));
                cell3.Controls.Add(KRate);

                hidCurId = new HiddenField();
                hidCurId.ID = "hidCurid";
                hidCurId.Value = currency;
                cell3.Controls.Add(hidCurId);

                cell4.Text = "";

                String VR = AGT.getAgentCurrencyVariation(AID, currency);
                String MR = AGT.getAgentCurrencyMargin(AID, currency);
                float ModRate = GRC.CalculateRate(float.Parse(GivenRate), float.Parse(MR), VR);

                MyRate = new TextBox();
                MyRate.ID = "TXT_MyRate" + CurrencyCode;
                MyRate.CssClass = "aa_input ad_set_rate_002";
                MyRate.TextMode = TextBoxMode.SingleLine;
                MyRate.Visible = true;
                MyRate.Text = String.Format("{0:N2}", ModRate.ToString());
                cell5.Controls.Add(MyRate);

                cell6.Text = "";

                Margin = new TextBox();
                Margin.ID = "TXT_Margin";
                Margin.CssClass = "aa_input";
                Margin.TextMode = TextBoxMode.SingleLine;
                Margin.Visible = true;
                Margin.Text = String.Format("{0:N2}", float.Parse(MR));
                cell7.Controls.Add(Margin);


                row.Cells.Add(cell1);
                row.Cells.Add(cell3);
                row.Cells.Add(cell4);
                row.Cells.Add(cell5);
                row.Cells.Add(cell6);
                row.Cells.Add(cell7);

                // TBL_SetExchngeRate.Rows.Add(row);

                counter++;
            }
        }

        protected void BTN_SetRates_Click(object sender, EventArgs e)
        {
            Control myControl = FindControl("TXT_Margin");
            if (myControl != null)
            {
                String val = "Y";
            }

            String Test = String.Empty;
            //foreach(Control ctl in TBL_SetExchngeRate.Controls)
            //{

            //    if((ctl as TextBox).ID == "TXT_MarginLKR")
            //    {

            //    }
            //}
        }

    }
}