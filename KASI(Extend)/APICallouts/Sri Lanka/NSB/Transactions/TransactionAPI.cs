﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.APICallouts.Sri_Lanka.NSB.Transactions
{
    public class TransactionAPI
    {
        public void processTransaction(String sTID)
        {
            var TID = Int32.Parse(sTID);

            TransactionService TransServ = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var TransDetails = TransServ.GetAll(x => x.TransactionID == TID, null, "").SingleOrDefault();

            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            var BPMDetails = BPMServ.GetAll(x => x.AccountID == TransDetails.AccountID.ToString(), null, "").SingleOrDefault();

            BeneficiaryService BeneServ = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            var BeneDetails = BeneServ.GetAll(x => x.BeneficiaryID == TransDetails.BeneficiaryID, null, "").SingleOrDefault();

            CustomerService CustServ = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            var CustDetails = CustServ.GetAll(x => x.CustomerID == TransDetails.CustomerID, null, "").SingleOrDefault();

            String sUserName = "";
            String sPassword = "";
            String sReferenceNo = TransDetails.TransactionID.ToString();
            String sTxnCountry = "AUD";
            String xTxnBankCode = BPMDetails.BankID;
            String sCurrency = TransDetails.RemittedCurrency;
            decimal dcTxnAmount = decimal.Parse(TransDetails.RemittedAmount.ToString());
            String xTxnDate = TransDetails.ApprovalDate.ToString();
            String sBenName = BeneDetails.BeneficiaryName;
            String sBenAccountNo = BPMDetails.AccountNumber;
            String sBenAddress = BeneDetails.AddressLine1 + " " + BeneDetails.State;
            String sBenPhone = BeneDetails.Mobile;
            String sSendName = CustDetails.FullName;
            String sSendAddress = CustDetails.StreetNo + " " + CustDetails.StreetName + " " + CustDetails.StreetType + " " + CustDetails.Suburb + " " + CustDetails.State + " " + CustDetails.Postcode;
            String sSendPhone = CustDetails.Mobile;
            String sSendOccupation = CustDetails.Occupation;
            String sSendMsg = "";
            String sRemarks = "";
            String sSourceOfIncome = TransDetails.SourceOfFunds;
            String sPurOfRemit = TransDetails.Purpose;

            //NSBWebAPI.ServiceSoapClient NSAPI = new NSBWebAPI.ServiceSoapClient();
            //var result = NSAPI.Deposit_Funds_To_Given_Account(sUserName, sPassword, sReferenceNo, sTxnCountry, xTxnBankCode, sCurrency, dcTxnAmount, xTxnDate, sBenName, sBenAccountNo, sBenAddress, sBenPhone, sSendName, sSendAddress, sSendPhone, sSendOccupation, sSendMsg, sRemarks, sSourceOfIncome, sPurOfRemit);
        }
        
    }
}