﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.APICallouts.Sri_Lanka.Sampath.Transactions
{
    public class TransactionAPI
    {
        public void processTransaction(String sTID)
        {
            int TID = Int32.Parse(sTID);

            TransactionService TransServ = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var TransDetails = TransServ.GetAll(x => x.TransactionID == TID, null, "").SingleOrDefault();

            CustomerService CustServ = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            var CustDetails = CustServ.GetAll(x => x.CustomerID == TransDetails.CustomerID, null, "").SingleOrDefault();

            BeneficiaryService BeneServ = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            var BeneDetails = BeneServ.GetAll(x => x.BeneficiaryID == TransDetails.BeneficiaryID, null, "").SingleOrDefault();

            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            var BPMDetails = BPMServ.GetAll(x => x.AccountID == TransDetails.AccountID.ToString(), null, "").SingleOrDefault();
            

            String[] trVals = new String[16];
            trVals[0] = TransDetails.TransactionID.ToString();
            trVals[1] = TransDetails.RemittedCurrency;
            trVals[2] = TransDetails.RemittedAmount.ToString();
            trVals[3] = CustDetails.FullName;
            trVals[4] = CustDetails.StreetNo + " " + CustDetails.StreetName + " " + CustDetails.StreetType + " " + CustDetails.Suburb + " " + CustDetails.Postcode;
            trVals[5] = CustDetails.CustomerID.ToString();
            trVals[6] = CustDetails.Mobile;
            trVals[7] = BeneDetails.BeneficiaryName;
            trVals[8] = BeneDetails.AddressLine1 + " " + BeneDetails.Suburb;
            trVals[9] = BeneDetails.NationalityCardID;
            trVals[10] = BeneDetails.Mobile;
            trVals[11] = BPMDetails.AccountNumber;
            trVals[12] = BPMDetails.BankID;
            trVals[13] = BPMDetails.BranchID.ToString();

            var RType = OwnRemittance(TransDetails.RoutingBank, BPMDetails.BankID, BPMDetails.BranchName, BPMDetails.AccountNumber);
            if (RType == "CASH")
            {
                trVals[14] = "POI";
            }
            else if (RType == "EQUAL")
            {
                trVals[14] = "SBA";
            }
            else
            {
                trVals[14] = "SLI";
            }
            trVals[15] = "";

            String user = "100330";
            String accessCode = "hoax";
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            string accessCodeEncoded = Convert.ToBase64String(enc.GetBytes(accessCode));
            String CompanyID = "2";

            //SampathWebService.ERMWSAgentClient SWAPI = new SampathWebService.ERMWSAgentClient();
            
            //var result = SWAPI.processTransaction(user, accessCodeEncoded, CompanyID, trVals);
        }

        private String OwnRemittance(String theBank, String OriginatingBank, String BranchName, String AccountNumber)
        {
            String output = String.Empty;

            if (theBank == OriginatingBank)
            {
                if (AccountNumber.ToUpper().Contains("ADVISE") || AccountNumber.ToUpper().Contains("ADVICE"))
                {
                    output = "CASH";
                }
                else
                {
                    output = "EQUAL";
                }

            }
            else
            {
                if (AccountNumber.ToUpper().Contains("ADVISE") || AccountNumber.ToUpper().Contains("ADVICE"))
                {
                    output = "CASH";
                }
                else
                {
                    output = "DIFFERENT";
                }
            }

            return output;
        }

    }
}