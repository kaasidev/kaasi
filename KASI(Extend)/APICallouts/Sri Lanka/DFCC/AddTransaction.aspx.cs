﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;

namespace KASI_Extend.APICallouts.Sri_Lanka.DFCC
{
    public partial class AddTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            callAPI();
        }

        public class DataObject
        {
            public string Name { get; set; }
        }

        public void callAPI()
        {
            string URL = "https://203.115.12.12/LMTAPIService/api/LMTServices/APIService/Uploadtransactions";
            string urlParameters = "?UserName=QA003Sabir&Password=Zxcv@12345";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var dataObjects = response.Content.ReadAsAsync<IEnumerable<DataObject>>().Result;
                foreach (var d in dataObjects)
                {
                    Console.WriteLine("{0}", d.Name);
                }
            }
        }
    }
}