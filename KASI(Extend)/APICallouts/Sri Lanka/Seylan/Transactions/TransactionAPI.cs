﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kapruka.Enterprise;
using Kapruka.Repository;
using System.Security.Cryptography;

namespace KASI_Extend.APICallouts.Sri_Lanka.Seylan.Transactions
{
    public class TransactionAPI
    {
        public void TransactionCallOut(String sTID)
        {
            int TID = Int32.Parse(sTID);

            TransactionService TransServ = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var TransDetails = TransServ.GetAll(x => x.TransactionID == TID, null, "").SingleOrDefault();

            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));

            var AccountID = TransDetails.AccountID.ToString();
            var BPMDetails = BPMServ.GetAll(x => x.AccountID == AccountID, null, "").SingleOrDefault();


            BeneficiaryService BeneServ = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            var BeneDetails = BeneServ.GetAll(x => x.BeneficiaryID == TransDetails.BeneficiaryID, null, "").SingleOrDefault();

            CustomerService CustServ = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            var CustDetails = CustServ.GetAll(x => x.CustomerID == TransDetails.CustomerID, null, "").SingleOrDefault();


            String exCode = "ABCD";
            String UserID = "ABCD";
            String Pword = "kapruka123";
            String SecurityCode = "a2xy2n8G";
            String TransType = String.Empty;
            String AccNum = String.Empty;
            var TransTypeOriginal = OwnRemittance(TransDetails.RoutingBank, BPMDetails.BankID, BPMDetails.BranchName, BPMDetails.AccountNumber);
            if (TransTypeOriginal == "CASH")
            {
                TransType = "FP";
                AccNum = TransDetails.TransactionID.ToString();
            }
            else
            {
                TransType = "FA";
                AccNum = BPMDetails.AccountNumber;
            }
            String Currency = TransDetails.RemittedCurrency;
            String Amount = String.Format("{0:N2}", float.Parse(TransDetails.RemittedAmount.ToString())).Replace(",", "").Replace(".","");
            String TransactionID = TransDetails.TransactionID.ToString();
            String BeneficiaryID = TransDetails.BeneficiaryID.ToString();
            String BeneficiaryName = TransDetails.BeneficiaryName;
            String BeneAddress = BeneDetails.AddressLine1;
            String BenePhone = BeneDetails.Mobile;
            String CustID = TransDetails.CustomerID.ToString();
            String CustName = CustDetails.FullName;
            String Comments = "";
            String strRef = exCode + "#" + "069000799837090" + "#" + TransDetails.RemittedAmount.ToString().Replace(",", "").Replace(".","");
            String strRef2 = exCode + "#" + "LKR";
            String HCode = HashValue(strRef, exCode, SecurityCode);
            String HCode2 = HashValue(strRef2, exCode, SecurityCode);
           
            SeylanAPIService.Service1Client SeyAPI = new SeylanAPIService.Service1Client();

            var result = SeyAPI.AccountBalance(exCode, "LKR", UserID, Pword, HCode2);
            var result2 = SeyAPI.TransactionInformation(exCode, UserID, Pword, TransType, AccNum, Currency, Amount, TransactionID, BeneficiaryID, BeneficiaryName, BeneAddress, BenePhone, CustID, CustName, Comments, HCode);
        }


        private String OwnRemittance(String theBank, String OriginatingBank, String BranchName, String AccountNumber)
        {
            String output = String.Empty;

            if (theBank == OriginatingBank)
            {
                if (AccountNumber.ToUpper().Contains("ADVISE") || AccountNumber.ToUpper().Contains("ADVICE"))
                {
                    output = "CASH";
                }
                else
                {
                    output = "EQUAL";
                }

            }
            else
            {
                if (AccountNumber.ToUpper().Contains("ADVISE") || AccountNumber.ToUpper().Contains("ADVICE"))
                {
                    output = "CASH";
                }
                else
                {
                    output = "DIFFERENT";
                }
            }

            return output;
        }

        private String HashValue(String strRef, String sExCode, String sSecCode)
        {
            String output = String.Empty;

            try
            {
                String FileWithKey = strRef + "|" + sSecCode + "|";
                byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(FileWithKey);
                byte[] hash;
                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    hash = sha1.ComputeHash(toEncodeAsBytes);
                }
                output = Convert.ToBase64String(hash);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return output;
        }

    }
}