﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;

namespace KASI_Extend_.Reports
{
    public partial class AgentAssignedRatesReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                DataTable agents = new DataTable();

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                    try
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter("SELECT AgentName, AgentID FROM dbo.Agents", conn);
                        adapter.Fill(agents);

                        DropDownList1.DataSource = agents;
                        DropDownList1.DataTextField = "AgentName";
                        DropDownList1.DataValueField = "AgentID";
                        DropDownList1.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    conn.Close();
                }
                DropDownList1.Items.Insert(0, new ListItem("-- SELECT --", ""));

            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(DateTime.Parse(FromDate.Text), DateTime.Parse(EndDate.Text), DropDownList1.SelectedValue.ToString());
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "AgentAssignedRatesReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text),
                new ReportParameter("EndDate", EndDate.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(DateTime fromDate, DateTime toDate, String SelectedAgent)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;

                SqlCommand cmd = new SqlCommand("REPORT_getAgentAssignedRates", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                    cmd.Parameters.Add("@AgentID", SqlDbType.Int).Value = Int32.Parse(SelectedAgent);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                   

            }

            return dt;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}