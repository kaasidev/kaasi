﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;

namespace KASI_Extend_.DataHandlers
{
    /// <summary>
    /// Summary description for AllNotificationsDataHandler
    /// </summary>
    public class AllNotificationsDataHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<Notifications> notifications = new List<Notifications>();
            int filteredCount = 0;

            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spGetAllNotifications", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDisplayLength = new SqlParameter()
                {
                    ParameterName = "@DisplayLength",
                    Value = displayLength
                };
                cmd.Parameters.Add(paramDisplayLength);

                SqlParameter paramDisplayStart = new SqlParameter()
                {
                    ParameterName = "@DisplayStart",
                    Value = displayStart
                };
                cmd.Parameters.Add(paramDisplayStart);

                SqlParameter paramSortCol = new SqlParameter()
                {
                    ParameterName = "@SortCol",
                    Value = sortCol
                };
                cmd.Parameters.Add(paramSortCol);

                SqlParameter paramSortDir = new SqlParameter()
                {
                    ParameterName = "@SortDir",
                    Value = sortDir
                };
                cmd.Parameters.Add(paramSortDir);

                SqlParameter paramSearchString = new SqlParameter()
                {
                    ParameterName = "@Search",
                    Value = search
                };
                cmd.Parameters.Add(paramSearchString);

                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    String ActionToPerform = string.Empty;
                    if (sdr["Type"].ToString() == "CUSTOMER")
                    {
                        ActionToPerform = "<a href='dashboard.aspx?custid=" + sdr["RecordID"].ToString() + "' target='_blank'><img src='../images/action.png'/></a>&nbsp;<img src='../images/action_x.png' onclick='deleteNotification(" + sdr["NotificationID"].ToString() + ")'/>";
                    }
                    else if (sdr["Type"].ToString() == "TRANSACTION")
                    {
                        ActionToPerform = "<a href='transactions.aspx?transid=" + sdr["RecordID"].ToString() + "'><img src='../images/action.png'/></a>&nbsp;<img src='../images/action_x.png' onclick='deleteNotification(" + sdr["NotificationID"].ToString() + ")'/>";
                    }
                    else if(sdr["Type"].ToString() == "INAGENT")
                    {
                        ActionToPerform = @"<a href='inward\inTransactions.aspx'><img src='../images/action.png'/></a>&nbsp;<img src='../images/action_x.png' onclick='deleteNotification(" + sdr["NotificationID"].ToString() + ")'/>";
                    }

                    Notifications nots = new Notifications();
                    nots.NotificationID = Int32.Parse(sdr["NotificationID"].ToString());
                    nots.Type = sdr["Type"].ToString();
                    nots.Description = sdr["Description"].ToString();
                    nots.CreatedDateTime = sdr["CreatedDateTime"].ToString();
                    nots.Action = ActionToPerform;
                    filteredCount = Convert.ToInt32(sdr["TotalCount"]);
                    notifications.Add(nots);
                }
                conn.Close();
            }

            var result = new
            {
                iTotalRecords = getNotificationsTotalCount(),
                iTotalDisplayRecords = filteredCount,
                aaData = notifications
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));
        }

    private int getNotificationsTotalCount()
    {
        int TotalTransactionCount = 0;
        string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(cs))
        {
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.Notifications", conn);
            conn.Open();

            TotalTransactionCount = (int)cmd.ExecuteScalar();
        }

        return TotalTransactionCount;
    }

    public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}