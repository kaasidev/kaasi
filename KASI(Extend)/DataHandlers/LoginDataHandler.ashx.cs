﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;

namespace KASI_Extend_.DataHandlers
{
    /// <summary>
    /// Summary description for LoginDataHandler
    /// </summary>
    public class LoginDataHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            List<Logins> FullLoginList = new List<Logins>();
            int filteredCount = 0;

            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spGetAllLogins", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDisplayLength = new SqlParameter()
                {
                    ParameterName = "@DisplayLength",
                    Value = displayLength
                };
                cmd.Parameters.Add(paramDisplayLength);

                SqlParameter paramDisplayStart = new SqlParameter()
                {
                    ParameterName = "@DisplayStart",
                    Value = displayStart
                };
                cmd.Parameters.Add(paramDisplayStart);

                SqlParameter paramSortCol = new SqlParameter()
                {
                    ParameterName = "@SortCol",
                    Value = sortCol
                };
                cmd.Parameters.Add(paramSortCol);

                SqlParameter paramSortDir = new SqlParameter()
                {
                    ParameterName = "@SortDir",
                    Value = sortDir
                };
                cmd.Parameters.Add(paramSortDir);

                SqlParameter paramSearchString = new SqlParameter()
                {
                    ParameterName = "@Search",
                    Value = search
                };
                cmd.Parameters.Add(paramSearchString);

                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Logins Login = new Logins();
                    filteredCount = Convert.ToInt32(sdr["TotalCount"]);
                    Login.LoginID = Convert.ToInt32(sdr["LoginID"]);
                    Login.FullName = sdr["FullName"].ToString();
                    Login.LoginType = sdr["LoginType"].ToString();
                    Login.AgentName = sdr["AgentName"].ToString();
                    Login.Email = sdr["Email"].ToString();
                    Login.Active = sdr["Active"].ToString();
                    Login.ViewEdit = "<a onclick='openUsersForEdit(" + sdr["LoginID"].ToString() + ")'>Edit</a>";
                    FullLoginList.Add(Login);
                }
            }

            var result = new
            {
                iTotalRecords = getTransactionTotalCount(),
                iTotalDisplayRecords = filteredCount,
                aaData = FullLoginList
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));
        }

        private int getTransactionTotalCount()
        {
            int TotalTransactionCount = 0;
            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.Logins", conn);
                conn.Open();

                TotalTransactionCount = (int)cmd.ExecuteScalar();
            }

            return TotalTransactionCount;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}