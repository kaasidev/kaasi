﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Service;

namespace KASI_Extend.DataHandlers
{
    /// <summary>
    /// Summary description for UploadIncomingFileDocs
    /// </summary>
    public class UploadIncomingFileDocs : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                //if (HttpContext.Current.Session["TypeOfLogin"] == null)
                //{
                //    context.Response.Redirect("../Login.aspx");
                //}
                String theCustomerID = context.Request.Form["CustID"].ToString();
                String DocDesc = context.Request.Form["DocDesc"].ToString();
                String pathrefer = context.Request.UrlReferrer.ToString();
                String Serverpath = HttpContext.Current.Server.MapPath("~" + "\\InCustomerOtherDocuments\\" + theCustomerID);
                String originalFileName = String.Empty;
                String TransactionID = context.Request.Form["TransID"];

                var postedFiles = context.Request.Files[0];

                String file = String.Empty;

                // If file is handled by IE
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                {
                    String[] files = postedFiles.FileName.Split(new char[] {
                            '\\'
                        });
                }
                else
                {
                    file = postedFiles.FileName;
                }

                if (!Directory.Exists(Serverpath))
                    Directory.CreateDirectory(Serverpath);

                String fileDirectory = Serverpath;
                if (context.Request.QueryString["filename"] != null)
                {
                    file = context.Request.QueryString["filename"];
                    if (File.Exists(fileDirectory + "\\" + file))
                    {
                        File.Delete(fileDirectory + "\\" + file);
                    }
                }

                originalFileName = file;

                String ext = Path.GetExtension(fileDirectory + "\\" + file);
                file = Guid.NewGuid() + ext;
                fileDirectory = Serverpath + "\\" + file;
                var tempPath = HttpContext.Current.Server.MapPath("~/temp/");
                if (!Directory.Exists(tempPath))
                    Directory.CreateDirectory(tempPath);


                postedFiles.SaveAs(fileDirectory);

                // string input = tempPath + originalFileName;

                // postedFiles.SaveAs(input);

                // Utilities.Encrypt(input, fileDirectory);

                //File.Delete(input);


                SaveInfoIntoDatabase(theCustomerID, originalFileName, file, DocDesc, TransactionID);





                context.Response.AddHeader("Vary", "Accept");
                try
                {
                    if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
                    {
                        context.Response.ContentType = "application/json";
                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                    }
                }
                catch
                {
                    context.Response.ContentType = "text/plain";
                }

                context.Response.Write("Success");

            }
            catch (Exception exp)
            {
                context.Response.Write(exp.Message);
            }
        }

        private void SaveInfoIntoDatabase(String theCustID, String FileName, String FileGUID, String DocDesc, String TransactionID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SqlStmt = "INSERT INTO dbo.InCustomerOtherDocuments (CustomerID, DocumentName, DocumentGUID, Description, TransactionID) VALUES (@CustomerID,@DocumentName, @DocumentGUID, @Description, @TransactionID)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 100).Value = theCustID;
                cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 500).Value = FileName;
                cmd.Parameters.Add("@DocumentGUID", SqlDbType.NVarChar, 500).Value = FileGUID;
                cmd.Parameters.Add("@Description", SqlDbType.NVarChar, 1500).Value = DocDesc;
                if (!String.IsNullOrEmpty(TransactionID))
                {
                    cmd.Parameters.Add("@TransactionID", SqlDbType.Int, 10).Value = Int32.Parse(TransactionID);
                }
                else
                {
                    cmd.Parameters.Add("@TransactionID", SqlDbType.Int, 10).Value = DBNull.Value;
                }

                cmd.ExecuteNonQuery();


                conn.Close();

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}