﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;

namespace KASI_Extend.DataHandlers
{
    /// <summary>
    /// Summary description for AGENTAwaitingTransferDataHandler
    /// </summary>
    public class AGENTAwaitingTransferDataHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];
            string agentID = context.Request.QueryString["AgentID"].ToString();
            string BankID = context.Request.QueryString["BankID"].ToString();
            string CurrencyCode = context.Request.QueryString["CCode"].ToString();

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            List<AllTransactions> listTransactions = new List<AllTransactions>();
            int filteredCount = 0;
         
            using (SqlConnection conn = new SqlConnection(cs))
            {
                conn.Open();
                string query = "exec spAGENTGetAllAwaitingTransactions @DisplayLength=" + displayLength + ",@DisplayStart=" + displayStart + ",@SortCol=" + sortCol + ",@SortDir=N'" + sortDir + "',@Search='" + search + "',@AgentID=" + agentID + ",@BankID=" + BankID + ",@CurrencyCode=" + CurrencyCode + " ";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandType = CommandType.Text;

                //SqlParameter paramDisplayLength = new SqlParameter()
                //{
                //    ParameterName = "@DisplayLength",
                //    Value = displayLength
                //};
                //cmd.Parameters.Add(paramDisplayLength);

                //SqlParameter paramDisplayStart = new SqlParameter()
                //{
                //    ParameterName = "@DisplayStart",
                //    Value = displayStart
                //};
                //cmd.Parameters.Add(paramDisplayStart);

                //SqlParameter paramSortCol = new SqlParameter()
                //{
                //    ParameterName = "@SortCol",
                //    Value = sortCol
                //};
                //cmd.Parameters.Add(paramSortCol);

                //SqlParameter paramSortDir = new SqlParameter()
                //{
                //    ParameterName = "@SortDir",
                //    Value = sortDir
                //};
                //cmd.Parameters.Add(paramSortDir);

                //SqlParameter paramSearchString = new SqlParameter()
                //{
                //    ParameterName = "@Search",
                //    Value = search
                //};
                //cmd.Parameters.Add(paramSearchString);

                //SqlParameter paramAgentID = new SqlParameter()
                //{
                //    ParameterName = "@AgentID",
                //    Value = agentID
                //};
                //cmd.Parameters.Add(paramAgentID);

                //SqlParameter paramBankID = new SqlParameter()
                //{
                //    ParameterName = "@BankID",
                //    Value = BankID
                //};
                //cmd.Parameters.Add(paramBankID);

                //SqlParameter paramCurrencyCode = new SqlParameter()
                //{
                //    ParameterName = "@CurrencyCode",
                //    Value = CurrencyCode
                //};
                //cmd.Parameters.Add(paramCurrencyCode);

                
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        AllTransactions transaction = new AllTransactions();
                        transaction.TransactionID = Convert.ToInt32(sdr["TransactionID"]);
                        filteredCount = Convert.ToInt32(sdr["TotalCount"]);
                        transaction.CreatedDateTime = Convert.ToDateTime(sdr["CreatedDateTime"].ToString()).ToString("dd/MM/yyyy");
                        transaction.CustomerID = sdr["CustomerID"].ToString();
                        transaction.BeneficiaryName = sdr["BeneficiaryName"].ToString();
                        transaction.RemittedCurrency = sdr["RemittedCurrency"].ToString();
                        float DAmount = float.Parse(sdr["DollarAmount"].ToString());
                        transaction.DollarAmount = String.Format("{0:N2}", DAmount);
                        float Rt = float.Parse(sdr["Rate"].ToString());
                        transaction.Rate = String.Format("{0:N2}", Rt);
                        float RAmount = float.Parse(sdr["RemittedAmount"].ToString());
                        transaction.RemittedAmount = String.Format("{0:N2}", RAmount);
                        transaction.BankName = sdr["BankName"].ToString();
                        listTransactions.Add(transaction);
                    }
                }
            }

            var result = new
            {
                iTotalRecords = getTransactionTotalCount(int.Parse(agentID),int.Parse(BankID),CurrencyCode),
                iTotalDisplayRecords = filteredCount,
                aaData = listTransactions
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));

        }

        private int getTransactionTotalCount(int agentId,int bankId,string currencyCode)
        {
            int TotalTransactionCount = 0;
            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' and AgentID="+ agentId + " and RoutingBank="+bankId+ " and RemittedCurrency IN ("+ currencyCode + ")", conn);
                conn.Open();

                TotalTransactionCount = (int)cmd.ExecuteScalar();
            }

            return TotalTransactionCount;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}