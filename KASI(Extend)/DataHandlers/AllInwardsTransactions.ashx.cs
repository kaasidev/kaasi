﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;


namespace KASI_Extend.DataHandlers
{
    /// <summary>
    /// Summary description for AllInwardsTransactions
    /// </summary>
    public class AllInwardsTransactions : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            List<TransactionINBound> listIFTI = new List<TransactionINBound>();
            int filteredCount = 0;

            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spGetAllInwardsTransactions", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDisplayLength = new SqlParameter()
                {
                    ParameterName = "@DisplayLength",
                    Value = displayLength
                };
                cmd.Parameters.Add(paramDisplayLength);

                SqlParameter paramDisplayStart = new SqlParameter()
                {
                    ParameterName = "@DisplayStart",
                    Value = displayStart
                };
                cmd.Parameters.Add(paramDisplayStart);

                SqlParameter paramSortCol = new SqlParameter()
                {
                    ParameterName = "@SortCol",
                    Value = sortCol
                };
                cmd.Parameters.Add(paramSortCol);

                SqlParameter paramSortDir = new SqlParameter()
                {
                    ParameterName = "@SortDir",
                    Value = sortDir
                };
                cmd.Parameters.Add(paramSortDir);

                SqlParameter paramSearchString = new SqlParameter()
                {
                    ParameterName = "@Search",
                    Value = search
                };
                cmd.Parameters.Add(paramSearchString);

                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    TransactionINBound IFTI = new TransactionINBound();
                    IFTI.KAASIID = Int32.Parse(sdr["ID"].ToString());
                    filteredCount = Convert.ToInt32(sdr["TotalCount"]);
                    IFTI.CreatedDateTime = sdr["CreatedDateTime"].ToString().Substring(0,10);
                    IFTI.AgentName = sdr["AgentName"].ToString();
                    IFTI.AltTransID = sdr["AlTransID"].ToString();
                    IFTI.BeneficiaryName = sdr["BeneficiaryName"].ToString();
                    IFTI.BankName = sdr["BankName"].ToString();
                    IFTI.DollarAmount = String.Format("{0:N2}", float.Parse(sdr["DollarAmount"].ToString()));
                    IFTI.Status = sdr["Status"].ToString();
                    IFTI.CustomerID = Int32.Parse(sdr["CustomerID"].ToString());
                    if (String.IsNullOrEmpty(sdr["resultCode"].ToString()) || sdr["resultCode"].ToString() == "-1")
                    {
                        IFTI.KYCStatus = @"<img src='images/KYC_FAILED.png'/>";
                    }
                    else
                    {
                        IFTI.KYCStatus = @"<img src='images/KYC_OK.png'/>";
                    }
                    
                    if (sdr["InboundBeneficiaryID"] != DBNull.Value)
                        IFTI.BeneficiaryID = Convert.ToInt32(sdr["InboundBeneficiaryID"].ToString());
                    listIFTI.Add(IFTI);
                }
            }

            var result = new
            {
                iTotalRecords = getTransactionTotalCount(),
                iTotalDisplayRecords = filteredCount,
                aaData = listIFTI
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));
        }

        private int getTransactionTotalCount()
        {
            int TotalTransactionCount = 0;
            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.InboundTransactions", conn);
                conn.Open();

                TotalTransactionCount = (int)cmd.ExecuteScalar();
            }

            return TotalTransactionCount;
        }

        protected class TransactionINBound
        {
            public int AgentID { get; set; }
            public int CustomerID { get; set; }
            public string CreatedBy { get; set; }
            public string Status { get; set; }
            public string Purpose { get; set; }
            public string CurrencyCode { get; set; }
            public String CreatedDateTime { get; set; }
            public String DollarAmount { get; set; }
            public int RowNumber { get; set; }
            public String AltTransID { get; set; }
            public String BeneficiaryName { get; set; }
            public String BankName { get; set; }
            public String AgentName { get; set; }
            public int KAASIID { get; set; }
            public int? BeneficiaryID { get; set; }
            public String KYCStatus { get; set; }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}