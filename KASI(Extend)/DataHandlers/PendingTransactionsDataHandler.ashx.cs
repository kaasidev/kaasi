﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;

namespace KASI_Extend_.DataHandlers
{
    /// <summary>
    /// Summary description for PendingTransactionsDataHandler
    /// </summary>
    public class PendingTransactionsDataHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            List<AllTransactions> listTransactions = new List<AllTransactions>();
            int filteredCount = 0;

            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spGetAllPendingTransactions", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDisplayLength = new SqlParameter()
                {
                    ParameterName = "@DisplayLength",
                    Value = displayLength
                };
                cmd.Parameters.Add(paramDisplayLength);

                SqlParameter paramDisplayStart = new SqlParameter()
                {
                    ParameterName = "@DisplayStart",
                    Value = displayStart
                };
                cmd.Parameters.Add(paramDisplayStart);

                SqlParameter paramSortCol = new SqlParameter()
                {
                    ParameterName = "@SortCol",
                    Value = sortCol
                };
                cmd.Parameters.Add(paramSortCol);

                SqlParameter paramSortDir = new SqlParameter()
                {
                    ParameterName = "@SortDir",
                    Value = sortDir
                };
                cmd.Parameters.Add(paramSortDir);

                SqlParameter paramSearchString = new SqlParameter()
                {
                    ParameterName = "@Search",
                    Value = search
                };
                cmd.Parameters.Add(paramSearchString);

                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    AllTransactions transaction = new AllTransactions();
                    transaction.TransactionID = Convert.ToInt32(sdr["TransactionID"]);
                    filteredCount = Convert.ToInt32(sdr["TotalCount"]);
                    transaction.CreatedDateTime = "<span style=\"display:none \">"+ Convert.ToDateTime(sdr["CreatedDateTime"].ToString()).ToString("yyyy/MM/dd") + "</span>"+sdr["CreatedDateTime"].ToString().Substring(0,10);
                    transaction.CustomerID = sdr["CustomerID"].ToString();
                    transaction.CustomerLastName = sdr["FullName"].ToString();
                    transaction.DOB = sdr["DOB"].ToString();
                    transaction.Mobile = sdr["Mobile"].ToString();
                    transaction.Suburb = sdr["Suburb"].ToString();
                    transaction.BeneficiaryName = sdr["BeneficiaryName"].ToString();
                    float DAmount = float.Parse(sdr["TotalCharge"].ToString());
                    transaction.DollarAmount = String.Format("{0:N2}", DAmount);
                    float Rt = float.Parse(sdr["Rate"].ToString());
                    transaction.Rate = String.Format("{0:N2}", Rt);
                    float RAmount = float.Parse(sdr["RemittedAmount"].ToString());
                    transaction.RemittedAmount = String.Format("{0:N2}", RAmount);
                    transaction.Status = sdr["Status"].ToString();
                    //transaction.DepositAccount = sdr["DepositMethod"].ToString();
                    transaction.BankName = sdr["Bank"].ToString();
                    transaction.BankBranch = sdr["BankBranch"].ToString();
                    transaction.AgentName = sdr["AgentName"].ToString();
                    transaction.CountryName = sdr["CountryName"].ToString();
                    transaction.ServiceCharge = String.Format("{0:N2}", float.Parse(sdr["ServiceCharge"].ToString()));
                    transaction.RemittedCurrency = sdr["RemittedCurrency"].ToString();
                    listTransactions.Add(transaction);
                }
            }

            var result = new
            {
                iTotalRecords = getTransactionTotalCount(),
                iTotalDisplayRecords = filteredCount,
                aaData = listTransactions
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));
        }

        private int getTransactionTotalCount()
        {
            int TotalTransactionCount = 0;
            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.Transactions", conn);
                conn.Open();

                TotalTransactionCount = (int)cmd.ExecuteScalar();
            }

            return TotalTransactionCount;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}