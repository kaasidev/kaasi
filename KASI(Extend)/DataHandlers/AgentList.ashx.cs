﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;

namespace KASI_Extend_.DataHandlers
{
    /// <summary>
    /// Summary description for AgentList
    /// </summary>
    public class AgentList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            List<AgentFullList> FullAgentList = new List<AgentFullList>();
            int filteredCount = 0;

            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spGetAllAgents", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDisplayLength = new SqlParameter()
                {
                    ParameterName = "@DisplayLength",
                    Value = displayLength
                };
                cmd.Parameters.Add(paramDisplayLength);

                SqlParameter paramDisplayStart = new SqlParameter()
                {
                    ParameterName = "@DisplayStart",
                    Value = displayStart
                };
                cmd.Parameters.Add(paramDisplayStart);

                SqlParameter paramSortCol = new SqlParameter()
                {
                    ParameterName = "@SortCol",
                    Value = sortCol
                };
                cmd.Parameters.Add(paramSortCol);

                SqlParameter paramSortDir = new SqlParameter()
                {
                    ParameterName = "@SortDir",
                    Value = sortDir
                };
                cmd.Parameters.Add(paramSortDir);

                SqlParameter paramSearchString = new SqlParameter()
                {
                    ParameterName = "@Search",
                    Value = search
                };
                cmd.Parameters.Add(paramSearchString);

                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    AgentFullList theAgent = new AgentFullList();
                    theAgent.AgentID = Convert.ToInt32(sdr["AgentID"]);
                    filteredCount = Convert.ToInt32(sdr["TotalCount"]);
                    theAgent.AgentName = sdr["AgentName"].ToString();
                    theAgent.AgentSuburb = sdr["AgentCity"].ToString();
                    theAgent.AgentState = sdr["AgentState"].ToString();
                    theAgent.AgentStatus = sdr["Status"].ToString();
                    theAgent.ViewEdit = "<a onclick='openAgentForEdit(" + sdr["AgentID"].ToString() + ")'>Edit</a>";
                    FullAgentList.Add(theAgent);
                }
            }

            var result = new
            {
                iTotalRecords = getTransactionTotalCount(),
                iTotalDisplayRecords = filteredCount,
                aaData = FullAgentList
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));
        }

        private int getTransactionTotalCount()
        {
            int TotalTransactionCount = 0;
            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.Agents", conn);
                conn.Open();

                TotalTransactionCount = (int)cmd.ExecuteScalar();
            }

            return TotalTransactionCount;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}