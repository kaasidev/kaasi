﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;


namespace KASI_Extend_.DataHandlers
{
    /// <summary>
    /// Summary description for AgentAllCustomersDataHandler
    /// </summary>
    public class AgentAllCustomersDataHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            List<Customer> listCustomers = new List<Customer>();
            int filteredCount = 0;


            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spAGENTGetAllCustomers", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDisplayLength = new SqlParameter()
                {
                    ParameterName = "@DisplayLength",
                    Value = displayLength
                };
                cmd.Parameters.Add(paramDisplayLength);

                SqlParameter paramDisplayStart = new SqlParameter()
                {
                    ParameterName = "@DisplayStart",
                    Value = displayStart
                };
                cmd.Parameters.Add(paramDisplayStart);

                SqlParameter paramSortCol = new SqlParameter()
                {
                    ParameterName = "@SortCol",
                    Value = sortCol
                };
                cmd.Parameters.Add(paramSortCol);

                SqlParameter paramSortDir = new SqlParameter()
                {
                    ParameterName = "@SortDir",
                    Value = sortDir
                };
                cmd.Parameters.Add(paramSortDir);

                SqlParameter paramSearchString = new SqlParameter()
                {
                    ParameterName = "@Search",
                    Value = search
                };
                cmd.Parameters.Add(paramSearchString);

                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Customer cust = new Customer();
                    cust.CustomerID = Convert.ToInt32(sdr["CustomerID"]);
                    filteredCount = Convert.ToInt32(sdr["TotalCount"]);
                    cust.AgentName = sdr["AgentName"].ToString();
                    cust.LastName = sdr["LastName"].ToString();
                    cust.FirstName = sdr["FirstName"].ToString();
                    cust.CountryOfBirth = sdr["CountryOfBirth"].ToString();
                    cust.Nationality = sdr["Nationality"].ToString();
                    cust.MinorAddress = sdr["CAddress"].ToString();
                    cust.Mobile = sdr["Mobile"].ToString();
                    cust.EmailAddress = sdr["EmailAddress"].ToString();
                    cust.openCust = "<a href='dashboard.aspx?custid=" + sdr["CustomerID"].ToString() + "' target='_blank'>OPEN</a>";
                    listCustomers.Add(cust);
                }
            }

            var result = new
            {
                iTotalRecords = getCustomerTotalCount(),
                iTotalDisplayRecords = filteredCount,
                aaData = listCustomers
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));
        }

        private int getCustomerTotalCount()
        {
            int TotalCustomerCount = 0;
            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.Customers", conn);
                conn.Open();

                TotalCustomerCount = (int)cmd.ExecuteScalar();
            }

            return TotalCustomerCount;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}