﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Kapruka.Service;

namespace KASI_Extend_.DataHandlers
{
    /// <summary>
    /// Summary description for FileUploadHandler
    /// </summary>
    public class FileUploadHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                //if (HttpContext.Current.Session["TypeOfLogin"] == null)
                //{
                //    context.Response.Redirect("../Login.aspx");
                //}
                if (context.Request.QueryString["upload"] != null)
                {
                    Customer cust = new Customer();

                    String theCustomerID = context.Request.Form["CustID"].ToString();
                    String DocNum = context.Request.Form["DocNum"].ToString();
                    String IssueAuth = context.Request.Form["IssueAuth"].ToString();
                    String ExpDate = context.Request.Form["ExpDate"].ToString();
                    String TDoc = context.Request.Form["TypeOfDoc"].ToString();
                    String TDocID = context.Request.Form["TODID"].ToString();
                    String AP = context.Request.Form["HDNPts"].ToString();
                    String pathrefer = context.Request.UrlReferrer.ToString();
                    String Serverpath = HttpContext.Current.Server.MapPath("~" + "\\CustomerDocuments\\" + theCustomerID);
                    String originalFileName = String.Empty;



                    var postedFiles = context.Request.Files[0];

                    String file = String.Empty;

                    // If file is handled by IE
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        String[] files = postedFiles.FileName.Split(new char[] {
                            '\\'
                        });
                    }
                    else
                    {
                        file = postedFiles.FileName;
                    }

                    if (!Directory.Exists(Serverpath))
                        Directory.CreateDirectory(Serverpath);

                    String fileDirectory = Serverpath;
                    if (context.Request.QueryString["filename"] != null)
                    {
                        file = context.Request.QueryString["filename"];
                        if (File.Exists(fileDirectory + "\\" + file))
                        {
                            File.Delete(fileDirectory + "\\" + file);
                        }
                    }

                    originalFileName = file;


                    String ext = Path.GetExtension(fileDirectory + "\\" + file);
                    file = Guid.NewGuid() + ext;

                    String hasPreviousDoc = cust.checkIfDocumnetHasBeenUploaded(theCustomerID, TDocID);

                    if (hasPreviousDoc == "Y")
                    {
                        AP = "0";
                    }
                    fileDirectory = Serverpath + "\\" + file;


                    String CurrentRiskLevel = cust.getCustomerRiskLevel(theCustomerID);
                    String CurrentRiskReason = cust.getRiskReason(theCustomerID);
                    String IDPoints = cust.getCustomerIDPointsTotal(theCustomerID);

                    if (IDPoints != "")
                    {
                        int TotalPoints = Int32.Parse(IDPoints);

                        if (TotalPoints >= 100)
                        {
                            if (CurrentRiskLevel == "4" && CurrentRiskReason == "NOID")
                            {
                                cust.SetRiskLevel(theCustomerID, "2", "NO KYC");
                            }
                        }
                    }


                    var tempPath = HttpContext.Current.Server.MapPath("~/temp/");
                    if (!Directory.Exists(tempPath))
                        Directory.CreateDirectory(tempPath);


                    postedFiles.SaveAs(fileDirectory);

                  //  string input = tempPath + originalFileName;


                    //postedFiles.SaveAs(input);
                  ///  Utilities.Encrypt(input, fileDirectory);

                  //  File.Delete(input);

                  //  string output = file;  // enable later

                    byte[] imageBytes = System.IO.File.ReadAllBytes(fileDirectory);
                    string base64String = Convert.ToBase64String(imageBytes);

                    SaveInfoIntoDatabase(theCustomerID, originalFileName, file, DocNum, IssueAuth, ExpDate, AP, TDoc, TDocID);

                    context.Response.AddHeader("Vary", "Accept");
                    try
                    {
                        if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
                        {
                            context.Response.ContentType = "application/json";
                        }
                        else
                        {
                            context.Response.ContentType = "text/plain";
                        }
                    }
                    catch
                    {
                        context.Response.ContentType = "text/plain";
                    }

                    context.Response.Write("Success");
                }


            }
            catch (Exception exp)
            {
                context.Response.Write(exp.Message);
            }
        }

        private void SaveInfoIntoDatabase(String theCustID, String FileName, 
            String FileGUID, String DocumentNumber, String IssuingAuthority, 
            String ExpiryDate, String AllocatedPoints, String TypeOfDoc, 
            String TDocID)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string SqlStmt = "INSERT INTO dbo.CustomerDocuments (CustomerID, DocumentName, DocumentGUID, DocumentNumber, IssuingAuthority, ExpiryDate, UploadDate, PointsAllocated, TypeOfDocument, TypeOfDocumentID) VALUES (@CustomerID,@DocumentName, @DocumentGUID, @DocumentNumber, @IssuingAuthority, @ExpiryDate, CURRENT_TIMESTAMP, @PointsAllocated, @TypeOfDocument, @TypeOfDocumentID)";

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 100).Value = theCustID;
                cmd.Parameters.Add("@DocumentName", SqlDbType.NVarChar, 500).Value = FileName;
                cmd.Parameters.Add("@DocumentGUID", SqlDbType.NVarChar, 500).Value = FileGUID;
                cmd.Parameters.Add("@DocumentNumber", SqlDbType.NVarChar, 150).Value = DocumentNumber;
                cmd.Parameters.Add("@IssuingAuthority", SqlDbType.NVarChar, 150).Value = IssuingAuthority;
                cmd.Parameters.Add("@ExpiryDate", SqlDbType.NVarChar, 20).Value = ExpiryDate;
                cmd.Parameters.Add("@PointsAllocated", SqlDbType.Int, 20).Value = AllocatedPoints;
                cmd.Parameters.Add("@TypeOfDocument", SqlDbType.NVarChar, 200).Value = TypeOfDoc;
                cmd.Parameters.Add("@TypeOfDocumentID", SqlDbType.Int, 100).Value = TDocID;
                
                cmd.ExecuteNonQuery();


                conn.Close();

            }

            checkandUpdateIfDocumentHasExpired(FileName, ExpiryDate, theCustID);
        }

        private void checkandUpdateIfDocumentHasExpired(String FileName, String ExpiryDate, String CID)
        {
            String HasDocsExpired = "N";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT * FROM dbo.CustomerDocuments WHERE CONVERT(datetime, '" + ExpiryDate + "', 103) < getdate() AND CustomerID = " + CID;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {

                        HasDocsExpired = "Y";

                    }
                    else
                    {
                        HasDocsExpired = "N";
                    }
                }
                conn.Close();
            }

            if (HasDocsExpired == "Y")
            {
                String SqlStmt = "UPDATE dbo.Customers SET RiskLevel = 3, RiskReason = 'EXPDOC', RiskMessage = 'Document " + FileName + " has expired. Please verify document before proceeding.' WHERE CustomerID = " + CID;

                using (SqlCommand cmd = new SqlCommand(SqlStmt))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}