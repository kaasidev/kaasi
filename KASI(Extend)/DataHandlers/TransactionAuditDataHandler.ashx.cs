﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;

namespace KASI_Extend_.DataHandlers
{
    /// <summary>
    /// Summary description for TransactionAuditDataHandler
    /// </summary>
    public class TransactionAuditDataHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int displayLength = int.Parse(context.Request["iDisplayLength"]);
            int displayStart = int.Parse(context.Request["iDisplayStart"]);
            int sortCol = int.Parse(context.Request["iSortCol_0"]);
            string sortDir = context.Request["sSortDir_0"];
            string search = context.Request["sSearch"];
            string TransID = context.Request["TID"];

            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<Audit> audits = new List<Audit>();
            int filteredCount = 0;

            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spGetAllNotifications", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDisplayLength = new SqlParameter()
                {
                    ParameterName = "@DisplayLength",
                    Value = displayLength
                };
                cmd.Parameters.Add(paramDisplayLength);

                SqlParameter paramDisplayStart = new SqlParameter()
                {
                    ParameterName = "@DisplayStart",
                    Value = displayStart
                };
                cmd.Parameters.Add(paramDisplayStart);

                SqlParameter paramSortCol = new SqlParameter()
                {
                    ParameterName = "@SortCol",
                    Value = sortCol
                };
                cmd.Parameters.Add(paramSortCol);

                SqlParameter paramSortDir = new SqlParameter()
                {
                    ParameterName = "@SortDir",
                    Value = sortDir
                };
                cmd.Parameters.Add(paramSortDir);

                SqlParameter paramSearchString = new SqlParameter()
                {
                    ParameterName = "@Search",
                    Value = search
                };
                cmd.Parameters.Add(paramSearchString);

                SqlParameter paramTransactionID = new SqlParameter()
                {
                    ParameterName = "@TransID",
                    Value = TransID
                };
                cmd.Parameters.Add(paramTransactionID);

                conn.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Audit theAud = new Audit();
                    theAud.AuditDateTime = DateTime.Parse(sdr["AuditDateTime"].ToString());
                    theAud.By = sdr["FullName"].ToString();
                    theAud.Description = sdr["Description"].ToString();
                    audits.Add(theAud);
                }
                conn.Close();
            }

            var result = new
            {
                iTotalRecords = getAuditCount(TransID),
                iTotalDisplayRecords = filteredCount,
                aaData = audits
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));

        }

        private int getAuditCount(String TransID)
        {
            int TotalTransactionCount = 0;
            string cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.TransAuditLog WHERE TransactionID = " + TransID, conn);
                conn.Open();

                TotalTransactionCount = (int)cmd.ExecuteScalar();
            }

            return TotalTransactionCount;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}