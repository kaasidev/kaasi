﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.APIValidations
{
    public partial class SAMPATH_ValidateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String BeneAccount = Request.QueryString["BeneAccount"].ToString();

            string accessCode = "hoax";
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            string accessCodeEncoded = Convert.ToBase64String(enc.GetBytes(accessCode));

            KASI_Extend.SampathWebAPI.ERMWSAgentClient client = new KASI_Extend.SampathWebAPI.ERMWSAgentClient();
            string[] result = new String[2];
            result = client.validateAccountNumber("100330", accessCodeEncoded, "2", BeneAccount);
            Response.Write(result[0].ToString() + " | " + result[1].ToString());
        }
    }
}