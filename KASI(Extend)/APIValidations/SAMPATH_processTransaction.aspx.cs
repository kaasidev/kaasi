﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.APIValidations
{
    public partial class SAMPATH_processTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            classes.Transactions trans = new classes.Transactions();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts beneAcc = new BeneficiaryAccounts();
            Customer cust = new Customer();

            String TID = Request.QueryString["TID"].ToString();
            String[] txnData = new String[16];
            String CustID = trans.getCustomerIDFromTransactionID(TID);
            String BeneID = trans.getBeneficiaryIDFomTransaction(TID);
            String AccID = trans.getBeneficiaryAccountIDFromTransaction(TID);
            String PayMethod = beneAcc.getBeneficiaryPaymentMethod(BeneID);

            txnData[0] = TID;
            txnData[1] = trans.getRemittedCurrency(TID);
            txnData[2] = trans.getTransactionSendingAmount(TID).ToString();
            txnData[3] = cust.getCustomerFullName(CustID);
            txnData[4] = cust.getCustomerFullAddress(CustID);
            txnData[5] = CustID;
            txnData[6] = cust.getCustomerMobile(CustID);
            txnData[7] = bene.getBeneficiaryName(BeneID);
            txnData[8] = bene.getBeneficiaryFullAddress(BeneID);
            txnData[9] = BeneID;
            txnData[10] = bene.getBeneficiaryMobile(BeneID);
            txnData[11] = beneAcc.getBeneficiaryAccountNumberOnTID(TID);
            txnData[12] = beneAcc.getBeneficiaryBankCode(TID);
            txnData[13] = beneAcc.getBeneficiaryBranchCode(TID);


            if (PayMethod == "Cash Over the Counter")
            {
                txnData[14] = "POI";
            }
            else
            {
                if (txnData[12] == "7278")
                {
                    txnData[14] = "SBA";
                }
                else
                {
                    txnData[14] = "SLI";
                }
            }
            txnData[15] = "";
            

            string accessCode = "hoax";
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            string accessCodeEncoded = Convert.ToBase64String(enc.GetBytes(accessCode));

            KASI_Extend.SampathWebAPI.ERMWSAgentClient client = new KASI_Extend.SampathWebAPI.ERMWSAgentClient();
            string[] result = new String[2];
            result = client.processTransaction("100330", accessCodeEncoded, "2", txnData);
            Response.Write(result[0].ToString() + " | " + result[1].ToString());
        }
    }
}