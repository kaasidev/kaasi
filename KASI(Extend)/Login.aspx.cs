﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Security;
using KASI_Extend_.classes;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend_
{
    public partial class Login : System.Web.UI.Page
    {
        //KASIStateManager sessManag = new KASIStateManager();

        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String AID = String.Empty;
            String LoginLevel = String.Empty;
            

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM dbo.Logins WHERE Username=@Username AND Password=@Password";
                    cmd.Parameters.AddWithValue("@Username", txt_uname.Value);
                    cmd.Parameters.AddWithValue("@Password", txt_pword.Value);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Session["usernameLogged"] = sdr["FullName"].ToString();
                              
                                if (sdr["LoginType"].ToString() == "MASTER")
                                {
                                    LogMasterUser(sdr["LoginID"].ToString(), sdr["LoginGroup"].ToString(), sdr["FullName"].ToString(), sdr["LoginID"].ToString());
                                }
                                else if (sdr["LoginType"].ToString() == "AGENT")
                                {
                                    LogAngetUser(sdr["AgentID"].ToString(), sdr["LoginID"].ToString());
                                }
                                else
                                {
                                    LogCustomerUser(sdr["CustomerID"].ToString());
                                }
                                
                            }
                       }
                        else
                       {
                            err_msg.InnerText = "Invalid username and/or password";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShakeError", "shakeErrorMsg()", true);
                            //   err_msg.InnerText = "Invalid username and/or password";
                        }

                                
                            
                    }
                }
                conn.Close();
            }
        }
            

        private String ConfirmAgentisActive(String AID)
        {
            String Confirmed = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM dbo.Agents WHERE AgentID = @AgentID";
                    cmd.Parameters.AddWithValue("@AgentID", AID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while(sdr.Read())
                            {
                                Confirmed = sdr["Status"].ToString();
                            }
                        }
                    }
                }
                conn.Close();
            }
            return Confirmed;
        }

        private void BuildSessions(String AID)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AgentName, AgentRevenue, AgentPendingTransactions, AgentApprovedTransactions FROM dbo.Agents WHERE AgentID = @AgentID";
                    cmd.Parameters.AddWithValue("@AgentID", AID);
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Session["AgentName"] = sdr["AgentName"].ToString();
                                Session["AgentRevenue"] = sdr["AgentRevenue"].ToString();
                                Session["AgentPending"] = sdr["AgentPendingTransactions"].ToString();
                                Session["AgentApproved"] = sdr["AgentApprovedTransactions"].ToString();
                                Session["LoggedID"] = AID;
                            }
                        }
                    }
                    conn.Close();
                }
            }
        }

        private void LogMasterUser(String UID, String LoginGroup, String FullName, String LID)
        {
            Logins LGT = new Logins();
            KASI_Extend_.classes.MasterCompany MC = new KASI_Extend_.classes.MasterCompany();
            Session["TypeOfLogin"] = "Master";
            Session["LoggedID"] = "0";
            Session["LoggedUserID"] = UID;
            Session["LoggedUserFullName"] = MC.MasterCompanyName();
            Session["LoggedCompany"] = MC.MasterCompanyName();
            Session["IsComplianceManager"] = LGT.isComplianceManager(LID);
            Session["CompliancePIN"] = LGT.getComplianceManagerPIN(LID);
            Session["LoggedPersonName"] = FullName;
            LogtoAudit(FullName, MC.MasterCompanyName(), "Master");
            Response.Redirect("Welcome.aspx");
        }

        private void LogAngetUser(String AID, String LID)
        {
            int theLID = Int32.Parse(LID);

            LoginService LGServ = new LoginService(new UnitOfWorks(new KaprukaEntities()));
            var LGDetails = LGServ.GetAll(x => x.LoginID == theLID, null, "").SingleOrDefault();


            KASI_Extend_.classes.Agent AGT = new KASI_Extend_.classes.Agent();
            Logins LGT = new Logins();
            String AgentIsActive = String.Empty;
            String LoggedAgentName = String.Empty;
            String AgentName = String.Empty;
            AgentName = AGT.getAgentNameByID(AID);
            LoggedAgentName = LGT.getFullNameFromID(LID);
            Session["LoggedUserFullName"] = AgentName;
            Session["TypeOfLogin"] = "Agent";
            Session["AgentID"] = AID;
            Session["LoggedUserID"] = LID;
            Session["LoggedCompany"] = AgentName;
            Session["LoggedPersonName"] = LoggedAgentName;
            Session["IsComplianceManager"] = LGT.isComplianceManager(LID);
            Session["CompliancePIN"] = LGT.getComplianceManagerPIN(LID);

            AgentIsActive = ConfirmAgentisActive(AID);
            if (AgentIsActive != "Active")
            {
                Session.Abandon();
                err_msg.InnerText = "There is a problem with your account. Please contact your master administrator.";
            }
            else
            {
                if (LGDetails.TransactionDirection == "O" || LGDetails.TransactionDirection == "B" || String.IsNullOrEmpty(LGDetails.TransactionDirection))
                {
                    BuildSessions(AID);
                    LogtoAudit(AgentName, AgentName, "Agent");
                    Response.Redirect("AgentW.aspx");
                }
                else
                {
                    int intAID = Int32.Parse(AID);
                    IncomingAgentService IAServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));
                    var IADetails = IAServ.GetAll(x => x.AgentID == intAID, null, "").SingleOrDefault();
                    Session["LoggedCompany"] = IADetails.AgentName;
                    LogtoAudit(AgentName, AgentName, "Agent");
                    Response.Redirect("AgentControl/Welcome.aspx");
                }
                
            }
        }

        private void LogCustomerUser(String CID)
        {
            Session["TypeOfLogin"] = "CUSTOMER";
            Session["LoggedInCustomerID"] = CID;
            Response.Redirect("Clients\\Dashboard.aspx");
        }

        private void LogtoAudit(String UserName, String AgentName, String LoginType)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            String IPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!String.IsNullOrEmpty(IPAddress))
            {
                string[] addresses = IPAddress.Split(',');
                if (addresses.Length != 0)
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = @"INSERT INTO dbo.LoginAudit (UserName, AgentName, LoginType, IPAddress, LoggedDateTime)
                            VALUES ('" + UserName + "', '" + AgentName + "', '" + LoginType + "', '" + addresses[0] + "', CURRENT_TIMESTAMP)";
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            else
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"INSERT INTO dbo.LoginAudit (UserName, AgentName, LoginType, IPAddress, LoggedDateTime)
                            VALUES ('" + UserName + "', '" + AgentName + "', '" + LoginType + "', '" + context.Request.ServerVariables["REMOTE_ADDR"] + "', CURRENT_TIMESTAMP)";
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }
}