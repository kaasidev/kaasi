﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getDocumentFullPath : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String DocID = Request.QueryString["CID"].ToString();
            String FullDocPath = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DocumentGUID FROM dbo.CustomerDocuments WHERE CustDocID = " + DocID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        FullDocPath = Server.MapPath(".") + sdr["DocumentGUID"].ToString();
                    }
                }
            }

        }
    }
}