﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getVariationLimits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Settings SETT = new classes.Settings();

            String MAUDLimit = SETT.getSystemMonthlyAUDLimit().Replace("$", "");
            String YAUDLimit = SETT.getSystemYearlyAUDLimit().Replace("$", "");
            String MTransLimit = SETT.getSystemMonthlyTransLimit().ToString();
            String YTransLimit = SETT.getSystemYearlyTransLimit().ToString();
            String MVariation = SETT.getSystemMonthlyVariationLimit().Replace("$", "");
            String MToCheck = SETT.getSystemMonthsOfVariation().ToString();

            Response.Write(MAUDLimit + "|" + YAUDLimit + "|" + MTransLimit + "|" + YTransLimit + "|" + MVariation + "|" + MToCheck);
        }
    }
}