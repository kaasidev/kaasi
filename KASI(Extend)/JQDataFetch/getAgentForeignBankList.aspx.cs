﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentForeignBankList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String FullReturnString = "{\"data\":[";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT AFBS.AgentForeignBankSettingsID, CTR.CountryName, BKS.BankName, AFBS.ForeignBankAccountNumber FROM dbo.AgentForeignBankSettings AFBS
                                JOIN dbo.Countries CTR ON AFBS.CountryID = CTR.CountryID JOIN dbo.Banks BKS ON AFBS.BankID = BKS.BankCode
                                WHERE AFBS.AgentID = " + Session["AgentID"].ToString();
            cmd.Connection = conn;

            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {

                        FullReturnString = FullReturnString + "[\"" + sdr["AgentForeignBankSettingsID"] + "\"," + "\"" + sdr["CountryName"] + "\"," + "\"" + sdr["BankName"] + "\"," + "\"" + sdr["ForeignBankAccountNumber"] + "\"," + "\"<a onclick='editFBAcc(" + sdr["AgentForeignBankSettingsID"] + ")'>VIEW</a>" + "\"],";

                    }
                    int Length = FullReturnString.Length;
                    FullReturnString = FullReturnString.Substring(0, (Length - 1));
                }

                FullReturnString = FullReturnString + "]}";

            }
            conn.Close();
            Response.Write(FullReturnString);
        }
    }
}