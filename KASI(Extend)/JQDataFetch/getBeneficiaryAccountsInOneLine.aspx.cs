﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getBeneficiaryAccountsInOneLine : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String BID = Request.QueryString["BeneID"].ToString();
            Response.Write(buildReturnString(BID));
        }

        public string buildReturnString(String BID)
        {
            String output = String.Empty;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT BankID, BranchID, BankName, BranchName, AccountNumber, AccountID FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID + " AND AcctStatus = 'Y'";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + sdr["BankName"].ToString() + " - " + sdr["BranchName"].ToString() + " - " + sdr["AccountNumber"].ToString() + "|";
                            output = output + sdr["AccountID"].ToString() + "|";
                            output = output + sdr["BankName"].ToString() + "|";
                            output = output + sdr["BranchName"].ToString() + "~";
                        }
                        int Length = output.Length;
                        if (Length != 0)
                        {
                            output = output.Substring(0, (Length - 1));
                        }
                    }
                    else
                    {
                        output = "NORESULTS";
                    }


                }




            }
            conn.Close();
            return output;
        }
    }
}