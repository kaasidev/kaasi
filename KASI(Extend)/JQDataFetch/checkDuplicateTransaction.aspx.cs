﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend.JQDataFetch
{
    public partial class checkDuplicateTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String BID = Request.QueryString["BID"].ToString();
            String AID = Request.QueryString["AID"].ToString();
            String Amount = Request.QueryString["AMT"].ToString();
            String output = "NO";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT * FROM dbo.Transactions WHERE CustomerID = " + CID + " AND BeneficiaryID = " + BID + "AND " +
                    "AccountID = " + AID + " AND DollarAmount = " + Amount + " AND CONVERT(date, CreatedDateTime) = CONVERT(date, GetDate())";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        output = "YES";
                    }
                    
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}