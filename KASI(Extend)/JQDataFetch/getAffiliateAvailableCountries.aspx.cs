﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend.JQDataFetch
{
    public partial class getAffiliateAvailableCountries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CountryID, CountryName FROM dbo.Countries WHERE ShowInAffiliate = 'True'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = output + "<option value='" + sdr["CountryID"].ToString() + "'>" + sdr["CountryName"] + "</option>|";
                    }
                }

                conn.Close();
                int Length = output.Length;
                output = output.Substring(0, (Length - 1));
            }

            Response.Write(output);
        }
    }
}