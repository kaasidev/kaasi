﻿using Kapruka.Domain;
using Kapruka.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class runkyc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var customerId = Convert.ToInt32(Request.QueryString["custid"].ToString());

           string res= CreateKYCEInfo(customerId);
            Response.Write(res);
        }
        private string CreateKYCEInfo(int customerId)
        {
            var cService = new CustomerHandlerService();
            var customerForKyc = cService.GetCustomerById(customerId);

            var resulCall = new ResultKycCall();
            var individual = (int)CustomerType.Individual;
            if (
                customerForKyc.CustomerType == individual)
            {
                var customerAllDocs = cService.GetCustomerAllDocs(customerId);
                var resultCallMe = ValidateCustomerDocs(customerAllDocs, customerId);

                if (string.IsNullOrEmpty(customerForKyc.FirstName))
                {
                    
                    return "Info: First name is mandatory";
                   

                }
                if (resultCallMe.Status == false)
                {
                   
                    return "Info: " + resultCallMe.Message;
                    
                }
                else
                    if (resultCallMe.RecordExist)
                {
                   
                    return "Info: " + resultCallMe.Message;
                  
                }
                else
                        if (resultCallMe.CanConnectApi)
                    resulCall = CallCustomerKcy(customerId, cService, customerForKyc, resulCall);



            }

            else
            {
                var merchantType = (int)CustomerType.Merchant;
                if (
              customerForKyc.CustomerType == merchantType)
                {
                    if (string.IsNullOrEmpty(customerForKyc.BusinessName))
                    {
                       
                        return "Info: Business legal name is mandatory";
                       

                    }

                    resulCall = CallMerchant(customerId, cService, customerForKyc, resulCall);
                }


            }

            if (resulCall.Status)
            {
               
                return "Info: This Merchant has successfully created in KYCE. " + resulCall.Message;
            }
            else
            {
               
                return "Warning: Failed " + resulCall.Message;
            }


        }
        private static ResultKycCall CallMerchant(int customerId, CustomerHandlerService cService,
          CustomerForKyc customerForKyc, ResultKycCall resulCall)
        {
            var merchant = new MerchantKyced();
            if (customerForKyc.TradingName != null)
                merchant.dba = customerForKyc.TradingName;
            else
                merchant.dba = "";

            merchant.amn = customerForKyc.BusinessName;

            merchant.website = "";
            if (customerForKyc.City != null)
                merchant.ac = customerForKyc.City;
            else
                merchant.ac = "";
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    merchant.aco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    merchant.aco = "AU";
            }
            else
                merchant.aco = "AU";
            if (customerForKyc.FirstName != null)
                merchant.afn = customerForKyc.FirstName;
            else
                merchant.afn = "";
            if (customerForKyc.LastName != null)
                merchant.aln = customerForKyc.LastName;
            else
                merchant.aln = "";
            if (customerForKyc.Mobile != null)
                merchant.aph = customerForKyc.Mobile;
            else
                merchant.aph = "";
            if (customerForKyc.Postcode != null)
                merchant.az = customerForKyc.Postcode;
            else
                merchant.az = "";

            if (customerForKyc.State != null)
                merchant.@as = customerForKyc.State;
            else
                merchant.@as = "";
            if (customerForKyc.StreetName != null)
                merchant.asn = customerForKyc.StreetName;
            else
                merchant.asn = "";
            if (customerForKyc.ABN != null)
                merchant.ataxId = customerForKyc.ABN;
            else
                merchant.ataxId = "";

            merchant.bin = "";
            if (customerForKyc.RiskLevel != null)
                merchant.bin = customerForKyc.RiskLevel;

            var runBy = "";
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            resulCall = cService.CreateKycedMerchant(merchant, customerId, customerForKyc.RecordID, runBy);
            return resulCall;
        }

        private ResultKycCall CallCustomerKcy(int customerId, CustomerHandlerService cService,
           CustomerForKyc customerForKyc,
           ResultKycCall resulCall)
        {

            var cusKyc = new CustomerKyced();
            cusKyc.profile = "FLEXEWALLET";
            if (customerForKyc.Title != null)
                cusKyc.title = customerForKyc.Title;
            else
                cusKyc.title = "";
            if (customerForKyc.FirstName != null)
                cusKyc.bfn = customerForKyc.FirstName;
            else
                cusKyc.bfn = "";

            if (customerForKyc.LastName != null)
                cusKyc.bln = customerForKyc.LastName;
            else
                cusKyc.bln = "";
            if (customerForKyc.DOB != null)
            {

                cusKyc.dob = ConvertDateFormat(customerForKyc.DOB);
            }
            else
                cusKyc.dob = "";
            if (customerForKyc.FirstName != null)
                cusKyc.man = customerForKyc.CustomerID.ToString();
            else
                cusKyc.man = "";
            if (customerForKyc.City != null)
                cusKyc.bc = customerForKyc.City;
            else
                cusKyc.bc = "";
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    cusKyc.bco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    cusKyc.bco = "AU";
            }
            else
                cusKyc.bco = "AU";

            if (customerForKyc.State != null)
                cusKyc.bs = customerForKyc.State;
            else
                cusKyc.bs = "";
            if (customerForKyc.StreetName != null)
                cusKyc.bsn = customerForKyc.StreetName;
            else
                cusKyc.bsn = "";
            if (customerForKyc.Postcode != null)
                cusKyc.bz = customerForKyc.Postcode;
            else
                cusKyc.bz = "";
            if (customerForKyc.Mobile != null)
                cusKyc.pw = customerForKyc.Mobile;
            else
                cusKyc.pw = "";
            if (customerForKyc.EmailAddress != null)
                cusKyc.tea = customerForKyc.EmailAddress;
            else
                cusKyc.tea = "";
            if (customerForKyc.FaceImage != null)
            {
                var image = Utilities.ByteArrayToImage(customerForKyc.FaceImage);
                if (image != null)
                    cusKyc.faceImage = Utilities.ImageToBase64(image, System.Drawing.Imaging.ImageFormat.Png);

            }
            cusKyc.faceImage = "";
            cusKyc.documentImageFront = "";
            //cusKyc.documentImangeBack = "";
            cusKyc.securityNumber = "";
            cusKyc.ip = "";
            //if (customerdoc != null)
            //{
            //    if (customerdoc.hasDriverLicence)
            //    {
            //        //var imageDriver = Utilities.ByteArrayToImage(customerdoc.DocumentDriverImage1);
            //        cusKyc.documentImageFront = customerdoc.DocumentDriverImage1;
            //    }
            //}
            var runBy = "";
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            resulCall = cService.CreateKycedCustomer(cusKyc, customerId, customerForKyc.RecordID, runBy);
            return resulCall;
        }
        private string ConvertDateFormat(string date)
        {
            var dateS = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
            return dateS;
        }
        protected class CustomerValidYear
        {
            public int customerId { get; set; }
            public bool status { get; set; }
        }
        private bool CheckDocSValidDate(List<CustomerDocs> docs)
        {
            var validDocFound = false;
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        validDocFound = true;
                        return validDocFound;
                    }
                }
            }

            return validDocFound;
        }
        private CustomerValidDoc ValidateCustomerDocs(List<CustomerDocs> docs, int customerId)
        {
            var obj = new CustomerValidDoc();

            if (docs.Count() == 0)  // check doc valid
            {
                obj.Message = "No valid documents found";
                return obj;
            }

            if (CheckDocSValidDate(docs) == false)  // check doc valid
            {
                obj.Message = "No valid documents found";
                return obj;
            }

            var serVic = new CustomerHandlerService();
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        var doNumber = item.DocumentNumber;
                        var checkDocNumberList = serVic.GetCustomerAllDocsByDocumentNumber(doNumber);
                        if (checkDocNumberList.Count() > 1)
                        {
                            var oneYearLessCheck = IsCustomerHasKycLessThanOneYear(checkDocNumberList);
                            if (oneYearLessCheck.status)
                            {
                                var cusInfo = serVic.GetCustomerById(oneYearLessCheck.customerId);
                                obj.Status = true;
                                obj.RecordExist = true;
                                obj.Message = " Result " + cusInfo.KCYResult + ". KYC checked date " + cusInfo.KYCCheckDate.Date.ToShortDateString();
                                return obj;
                            }

                            var oneYearCheck = IsCustomerHasKycMoreThanOneYear(checkDocNumberList);
                            if (oneYearCheck.status)
                            {
                                obj.Status = true;
                                obj.CanConnectApi = true;
                                return obj;
                            }


                        }
                        else
                        {
                            obj.Status = true;
                            obj.CanConnectApi = true;
                            return obj;
                        }
                    }

                }

            }

            return obj;

        }
        private CustomerValidYear IsCustomerHasKycMoreThanOneYear(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {

                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (customerKcCheck.KYCCheckDate != null || customerKcCheck.KYCCheckDate != DateTime.MinValue)
                {
                    var KYCCheckDateyear = customerKcCheck.KYCCheckDate.AddYears(1);
                    var currentDate = DateTime.Now;
                    if (currentDate > KYCCheckDateyear)
                    {
                        validDocFound.status = true;
                        validDocFound.customerId = customerforKyc.CustomerID;

                    }

                }
            }

            return validDocFound;
        }

        private CustomerValidYear IsCustomerHasKycLessThanOneYear(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {
                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (customerKcCheck.KYCCheckDate != null || customerKcCheck.KYCCheckDate != DateTime.MinValue)
                {
                    var KYCCheckDateyear = customerKcCheck.KYCCheckDate.AddYears(1);
                    var currentDate = DateTime.Now;
                    if (currentDate < KYCCheckDateyear)
                    {
                        validDocFound.status = true;
                        validDocFound.customerId = customerforKyc.CustomerID;

                    }

                }
            }

            return validDocFound;
        }
    }

   
}