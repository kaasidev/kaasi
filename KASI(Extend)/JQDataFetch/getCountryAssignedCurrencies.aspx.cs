﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCountryAssignedCurrencies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;
            String CountryID = Request.QueryString["CID"];
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID, CurrencyCode FROM dbo.Currencies WHERE CurrencyID IN (SELECT CurrencyID FROM dbo.CountryCurrencies WHERE CountryID = " + CountryID + ")";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = output + sdr["CurrencyCode"].ToString() + "|";
                        output = output + sdr["CurrencyID"].ToString() + "~";
                    }
                }
                conn.Close();
            }

            int Length = output.Length;
            output = output.Substring(0, (Length - 1));

            Response.Write(output);
        }
    }
}