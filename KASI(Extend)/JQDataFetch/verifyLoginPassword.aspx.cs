﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class verifyLoginPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String OldPass = Request.QueryString["OP"].ToString();
            String output = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM dbo.Logins WHERE LoginID=@LoginID AND Password=@Password";
                    conn.Open();
                    cmd.Parameters.Add("@LoginID", SqlDbType.NVarChar, 50).Value = Session["LoggedUserID"].ToString();
                    cmd.Parameters.Add("@Password", SqlDbType.NVarChar, 50).Value = OldPass;
                    

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            output = "PASS";
                        }
                        else
                        {
                            output = "FAIL";
                        }
                    }
                    conn.Close();
                }
            }

            Response.Write(output);
        }
    }
}