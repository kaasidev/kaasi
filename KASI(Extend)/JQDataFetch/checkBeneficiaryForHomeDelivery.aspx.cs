﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class checkBeneficiaryForHomeDelivery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String BeneID = Request.QueryString["BeneID"].ToString();
            Response.Write(checkHomeDelivery(BeneID));
        }

        protected String checkHomeDelivery(String BID)
        {
            String output = String.Empty;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT TelHome, TelWork, Mobile, AddressLine1, Suburb, NationalityCardID FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["TelHome"].Equals(DBNull.Value) && sdr["TelWork"].Equals(DBNull.Value) && sdr["Mobile"].Equals(DBNull.Value))
                        {
                            output = "NO";
                        }
                        else
                        {
                            if (sdr["AddressLine1"].Equals(DBNull.Value) && sdr["Suburb"].Equals(DBNull.Value))
                            {
                                output = "NO";
                            }
                            else
                            {
                                if (sdr["NationalityCardID"].Equals(DBNull.Value))
                                {
                                    output = "NO";
                                }
                                else
                                {
                                    output = "YES";
                                }
                            }
                        }
                    }

                }



            }
            conn.Close();
            return output;
        }
    }
}