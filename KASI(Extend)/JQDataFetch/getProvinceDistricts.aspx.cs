﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getProvinceDistricts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            if (!String.IsNullOrEmpty(CID))
            {
                String output = String.Empty;

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "SELECT DistrictID, DistrictName FROM dbo.CountryProvinceDistrict WHERE ProvinceID = " + CID;
                        cmd.Connection = conn;
                        conn.Open();

                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                output = output + sdr["DistrictName"].ToString() + "|" + sdr["DistrictID"].ToString() + "~";
                            }
                        }
                        conn.Close();
                    }

                    int Length = output.Length;
                    if (Length != 0)
                    {
                        output = output.Substring(0, (Length - 1));
                    }

                    Response.Write(output);
                }
                catch (Exception ex)
                {
                    conn.Close();
                }
                finally
                {
                    conn.Dispose();
                }

                
            }
            else
            {
                Response.Write("");
            }
        }
    }
}