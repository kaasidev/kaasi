﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getDepositMethods : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(loadDepositMethods());
        }

        protected String loadDepositMethods()
        {
            String output = String.Empty;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = String.Empty;

            if (Session["TypeOfLogin"].ToString() == "Agent")
            {
                strQuery = @"SELECT MCB.BankAccountID, MCB.AccountName FROM dbo.AgentAssignedBanks AAB INNER JOIN dbo.MasterCompanyBanks MCB
                           ON AAB.MasterBankAccountID = MCB.BankAccountID WHERE AgentID = " + Session["AgentID"].ToString();
            }
            else
            {
                strQuery = "SELECT * FROM dbo.MasterCompanyBanks";
            }
            

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = output + sdr["BankAccountID"].ToString() + "|";
                        output = output + sdr["AccountName"].ToString() + "~";
                    }

                }
                int Length = output.Length;
                output = output.Substring(0, (Length - 1));

            }
            conn.Close();
            return output;
        }
    }
}