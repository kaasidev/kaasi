﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerPastRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String requestedCID = Request.QueryString["custid"].ToString();
            String CID = requestedCID;
            Response.Write(returnString(CID));
        }

        public String returnString(String CID)
        {
            String FullReturnString = "{\"data\":[";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Session["TypeOfLogin"].ToString() == "Master")
            {
                cmd.CommandText = "spGetCustomerPastRecords";
            }
            else
            {
                cmd.CommandText = "spGetCustomerPastRecordsForAgent";
                cmd.Parameters.Add("@AgentID", SqlDbType.Int).Value = Session["LoggedID"].ToString();
                
            }
            
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        //if (sdr["RemittedCurrency"].ToString().Equals("LKR") || sdr["RemittedCurrency"].ToString().Equals("SLR"))
                        //   {
                        //       
                        //  }
                        // else if (sdr["RemittedCurrency"].ToString().Equals("AUD"))
                        //  {
                        //      FullReturnString = FullReturnString + "[\"" + sdr["TransactionID"] + "\"," + "\"" + sdr["CDT"] + "\"," + "\"" + sdr["BeneficiaryName"] + "\"," + "\"" + "Rs " + String.Format("{0:N2}", sdr["Rate"]) + "\"," + "\"" + String.Format("{0:C2}", sdr["DollarAmount"]) + "\"," + "\"" + String.Format("{0:C2}", sdr["ServiceCharge"]) + "\"," + "\"" + sdr["RemittedCurrency"] + "\"," + "\"" + "$ " + String.Format("{0:N2}", sdr["RemittedAmount"]) + "\"," + "\"" + "<a href='FillPDF.aspx?TID=" + sdr["TransactionID"] + "' target='_blank'>OPEN</a>" + "\"],";
                        //  }
                        string trId = sdr["Status"].ToString();
                        string dd = sdr["Rate"].ToString();
                        string gg= sdr["DollarAmount"].ToString();
                        string hh = sdr["ServiceCharge"].ToString();
                        string yy = sdr["RemittedAmount"].ToString();
                        string rate= String.Format("{0:N2}", sdr["Rate"]);
                        string dolAmnt = String.Format("{0:C2}", sdr["DollarAmount"]);
                        string serviceCharge = String.Format("{0:C2}", sdr["ServiceCharge"]);
                        string remAmnt = String.Format("{0:N2}", sdr["RemittedAmount"]);
                        FullReturnString = FullReturnString + "[\"" + sdr["TransactionID"] + "\"," + "\"" + sdr["CDT"] + "\"," + "\"" + sdr["BeneficiaryName"] + "\"," + "\"" + String.Format("{0:N2}", sdr["Rate"]) + "\"," + "\"" + String.Format("{0:C2}", sdr["DollarAmount"]) + "\"," + "\"" + String.Format("{0:C2}", sdr["ServiceCharge"]) + "\"," + "\"" + sdr["RemittedCurrency"] + "\"," + "\"" + String.Format("{0:N2}", sdr["RemittedAmount"]) + "\"," + "\"" + sdr["Status"] + "\"," + "\"" + "" + "\"],";

                    }
                    int Length = FullReturnString.Length;
                    FullReturnString = FullReturnString.Substring(0, (Length - 1));
                }

                FullReturnString = FullReturnString + "]}";

            }
            conn.Close();
            return FullReturnString;
        }
    }
}