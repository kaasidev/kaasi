﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getExchangeRateForAgent : System.Web.UI.Page
    {
        String Variation = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            String AID = Request.QueryString["AID"].ToString();
            String AccountID = Request.QueryString["AccID"].ToString();
            float finalRate = 0;
            String CurrencyID = String.Empty;

            int intAID = Int32.Parse(AID);
            
            float Margin = getMarginForAgentByCurrency(AID, AccountID);

            BeneficiaryAccounts BA = new BeneficiaryAccounts();
            CurrencyID = BA.getBeneficiaryAccountCurrencyID(AccountID);
            classes.Agent AGT = new classes.Agent();
            String CommStructure = AGT.getAgentCommissionStructure(AID);

            /****** Find currency code for the sending currency ********/
            int intCurrencyID = Int32.Parse(CurrencyID);
            CurrencyService CurServ = new CurrencyService(new UnitOfWorks(new KaprukaEntities()));
            var CurDetails = CurServ.GetAll(x => x.CurrencyID == intCurrencyID, null, "").SingleOrDefault().CurrencyCode;

            /******* Is the currency the local currency? *********/
            SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var SetCurDetails = SetServ.GetAll(x => x.SettingName == "LocalCurrency", null, "").SingleOrDefault().SettingsVariable;

            if (SetCurDetails == CurDetails)
            {
                finalRate = 1;
            }
            else
            {
                KaprukaEntities dbContext = new KaprukaEntities();
                var theRate = (from d in dbContext.AgentCurrencyRates
                               where d.AgentID == intAID && d.CurrencyID == intCurrencyID
                               select d).ToList().OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();

                if (theRate != null)
                {
                    if (CommStructure == "FXGain")
                    {
                        finalRate = float.Parse(theRate.AssignedRate.ToString()) - Margin;
                    }
                    else
                    {
                        finalRate = float.Parse(theRate.AssignedRate.ToString()) + Margin;
                    }
                }
                else
                {
                    finalRate = 0;
                }

            }

            Response.Write(finalRate);
        }

        private float getMarginForAgentByCurrency(String AID, String AccID)
        {
            BeneficiaryAccounts  BA = new BeneficiaryAccounts();
            String CurrencyID = BA.getBeneficiaryAccountCurrencyID(AccID);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            float finalMargin = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Margin, UpDown FROM dbo.AgentCurrencyMargins WHERE AgentID = " + AID + " AND CurrencyID = " + CurrencyID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        finalMargin = float.Parse(sdr["Margin"].ToString());
                        Variation = sdr["UpDown"].ToString();
                    }
                }

                conn.Close();
            }

            return finalMargin;
        }

    }
}