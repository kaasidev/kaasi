﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getDocumentTypes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLstmt = "SELECT CompliantDocumentID, DocumentName FROM dbo.CompliantDocumentsList";

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = SQLstmt;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            output = output + sdr["DocumentName"].ToString() + "|";
                            output = output + sdr["CompliantDocumentID"].ToString() + "~";
                        }
                    }
                    int Length = output.Length;
                    output = output.Substring(0, (Length - 1));
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}