﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace KASI_Extend_.JQDataFetch
{
    public partial class VerifyMobileDuplicate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String MobileNumber = String.Empty;
            MobileNumber = Request.QueryString["MOB"].ToString();
            String HasMatch = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT * FROM dbo.Customers WHERE Replace(Mobile, ' ','') = Replace('" + MobileNumber + "', ' ', '')";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        HasMatch = "Y";
                    }
                    else
                    {
                        HasMatch = "N";
                    }
                }
                conn.Close();
            }

            Response.Write(HasMatch);
        }
    }
}