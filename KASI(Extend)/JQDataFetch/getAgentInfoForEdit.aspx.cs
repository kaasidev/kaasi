﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentInfoForEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Agent AG = new Agent();
            String output = String.Empty;
            String AgentID = Session["AgentID"].ToString();

            output = AG.getAgentAddressLine1(AgentID) + "|" + AG.getAgentAddressLine2(AgentID) + "|" + AG.getAgentSuburb(AgentID) + "|" + AG.getAgentState(AgentID) + "|";
            output = output + AG.getAgentPostcode(AgentID) + "|" + AG.getAgentTelephoneNumber(AgentID) + "|" + AG.getAgentFaxNumber(AgentID) + "|";
            output = output + AG.getAgentEmailAddress(AgentID) + "|" + AG.getAgentABN(AgentID) + "|" + AG.getAgentACN(AgentID);


            Response.Write(output);

        }
    }
}