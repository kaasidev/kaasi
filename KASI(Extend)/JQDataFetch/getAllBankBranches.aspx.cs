﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAllBankBranches : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String BankID = Request.QueryString["BID"].ToString();
            Response.Write(getAllBankBranchesString(BankID));
        }

        protected String getAllBankBranchesString(String BID)
        {
            String output = String.Empty;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT BranchCode, BranchName FROM dbo.BankBranches WHERE BankCode = '" + BID + "'";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = output + sdr["BranchName"].ToString() + "|";
                        output = output + sdr["BranchCode"].ToString() + "~";
                    }

                }

                int Length = output.Length;
                output = output.Substring(0, (Length - 1));

            }
            conn.Close();
            return output;
        }
    }
}