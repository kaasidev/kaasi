﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getTradingCurencies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string kaasiRate = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;
            classes.MasterCompany MC = new MasterCompany();
            classes.Currencies Cur = new Currencies();
            Dictionary<string, string> MasterCurrencies = new Dictionary<string, string>();
            MasterCurrencies = MC.getMasterCurrencies();
            string currencyIds = "";
            string currencyBuys = "";
            // ADMIN EXCHANGE RATE SETTINGS ********
            string innerHTMLVal = "<table class=\"tbl_width_560\" id=\"tbl_ExchangeRate\"><tbody>" +
                                    "<tr>" +
                                      "<th class=\"exrate_headings_001\">Currency</th>" +
                                    //"<th class=\"style__width_20\">&nbsp;</th>" +
                                      "<th class=\"exrate_headings_002\">Bank Feed</th>" +
                                      //"<th class=\"style__width_20\">&nbsp;</th>" +
                                       "<th class=\"exrate_headings_003\">TransferTo Rate</th>" +
                                      "<th class=\"exrate_headings_004\">Your Margin</th>" +
                                      //"<th class=\"style__width_20\">&nbsp;</th>" +
                                      //"<th class=\"aa_label_font\">Kaasi Rate</th>" +
                                      //"<th class=\"style__width_20\">&nbsp;</th>" +
                                     
                                    "</tr>";

            foreach (KeyValuePair<string, string> entry in MasterCurrencies)
            {
                String CurrencyID = Cur.getCurrencyIDByCurrencyCode(entry.Key);
                String BuyRate = MC.getLastCurrencyBuyRate(CurrencyID);
                String Margin = MC.getLastMarginForCurrency(CurrencyID);
                var transfertoBuyRate = MC.getLastCurrencyTranferToBuyRate(CurrencyID);
                var transferToRate = "0";
                if (!string.IsNullOrEmpty(transfertoBuyRate))
                    transferToRate = transfertoBuyRate;
                double newRate = Convert.ToDouble(BuyRate) - Convert.ToDouble(Margin);
                innerHTMLVal += "<tr>";
                innerHTMLVal += "<td class=\"exrate_contents_001\">" + entry.Key + "</td>";
                //innerHTMLVal += "<td>&nbsp;</td>";
                innerHTMLVal += "<td><input type=\"number\"  class=\"aa_input exrate_contents_002\" id=\"curMargin_" + entry.Key + "\" value=\"" + String.Format("{0:N4}", float.Parse(BuyRate)).Replace(",", "") + "\" readonly='readonly'></td>";
                //innerHTMLVal += "<td class=\"style__width_20\">&nbsp;</td>";
                innerHTMLVal += "<td ><input type=\"number\" id=\"tdNewRate" + entry.Key + "\" class=\"aa_input exrate_contents_003\" value=\"" + String.Format("{0:N4}", float.Parse(transferToRate.ToString())) + "\" readonly='readonly'/></td>";
              
                innerHTMLVal += "<td><input type=\"number\"  class=\"aa_input exrate_contents_004\" onkeyup=\"ChangeCurrencyRate('" + entry.Key + "'," + kaasiRate + ")\" onblur=\"calcNewRate('" + entry.Key + "')\" id=\"curBuy_" + entry.Key + "\" value=\"" + String.Format("{0:N4}", float.Parse(Margin)) + "\"></td>";
                //innerHTMLVal += "<td class=\"style__width_20\">&nbsp;</td>";
                //innerHTMLVal += "<td ><input style=\"width:80px\" type=\"text\"  class=\"aa_input\" value=\"" + kaasiRate + "\" disabled /></td>";
                //innerHTMLVal += "<td class=\"style__width_20\">&nbsp;</td>";

                innerHTMLVal += "</tr>";
                currencyIds = currencyIds + "curMargin_" + entry.Key + "^";
                currencyBuys = currencyBuys + "curBuy_" + entry.Key + "^";
            }

            innerHTMLVal += "</tbody></table><input type=\"hidden\" id=\"hidCurrencyIdList\" value=\"" + currencyIds + "\" /><input type=\"hidden\" id=\"hidCurrencyBuyList\" value=\"" + currencyBuys + "\" />";

            Response.Write(innerHTMLVal);
        }

        protected void oldMethod()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT CurrencyCode FROM dbo.Currencies WHERE TradingCurrency ='Y'";
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            //output = output + sdr["CurrencyCode"].ToString() + "|";
                        }
                    }
                }
                //int Length = output.Length;
                //output = output.Substring(0, (Length - 1));
            }
        }
    }
}