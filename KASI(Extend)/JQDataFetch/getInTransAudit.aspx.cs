﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;

namespace KASI_Extend.JQDataFetch
{
    public partial class getInTransAudit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String transactionID = Request.QueryString["TID"];

            String FullReturnString = "{\"data\":[";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetInTransactionAudit";
            cmd.Parameters.Add("@TransID", SqlDbType.Int).Value = transactionID;


            cmd.Connection = conn;

            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        FullReturnString = FullReturnString + "[\"" + sdr["AuditDateTime"] + "\"," + "\"" + sdr["FullName"].ToString() + " - " + sdr["Description"].ToString() + "\"],";

                    }
                    int Length = FullReturnString.Length;
                    FullReturnString = FullReturnString.Substring(0, (Length - 1));
                }

                FullReturnString = FullReturnString + "]}";

            }
            conn.Close();

            Response.Write(FullReturnString);
        }
    }
}