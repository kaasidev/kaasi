﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentTransLimit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Agent AGT = new Agent();
            String AID = String.Empty;
            String theID = Request.QueryString["AgentID"];
            if (!String.IsNullOrEmpty(Request.QueryString["AgentID"]))
            {
                AID = Request.QueryString["AgentID"].ToString();
            }
            else
            {
                AID = Session["AgentID"].ToString();
            }
            
            

            Response.Write(AGT.getAgentMonthlyAUDLimit(AID) + "|" + AGT.getAgentYearlyAUDLimit(AID) + "|" + AGT.getAgentMonthlyTransLimit(AID) + "|" + AGT.getAgentYearlyTransLimit(AID));
        }
    }
}