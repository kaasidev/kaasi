﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class verifyAdminOverrideCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String UserID = Request.QueryString["UID"].ToString();
            String OverrideCode = Request.QueryString["ORCode"].ToString();
            String Confirmed = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM dbo.Logins WHERE LoginID = " + UserID + " AND AdminOverrideCode = " + OverrideCode;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            Confirmed = "Y";
                        }
                        else
                        {
                            Confirmed = "N";
                        }
                    }
                    conn.Close();
                }

                Response.Write(Confirmed);
            }
        }
    }
}