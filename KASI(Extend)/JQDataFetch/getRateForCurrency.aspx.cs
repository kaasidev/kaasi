﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend.JQDataFetch
{
    public partial class getRateForCurrency : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int CurrencyID = Int32.Parse(Request.QueryString["CID"].ToString());
            int loggedAgentID = Int32.Parse(Session["AgentID"].ToString());

            AgentCurrencyRatesService ACRServ = new AgentCurrencyRatesService(new UnitOfWorks(new KaprukaEntities()));
            var ACRDetails = ACRServ.GetAll(x => x.CurrencyID == CurrencyID && x.AgentID == loggedAgentID, null, "").SingleOrDefault();

            AgentCurrencyMarginService ACMServ = new AgentCurrencyMarginService(new UnitOfWorks(new KaprukaEntities()));
            var ACMDetails = ACMServ.GetAll(x => x.CurrencyID == CurrencyID && x.AgentID == loggedAgentID, null, "").SingleOrDefault();

            CurrencyService CurServ = new CurrencyService(new UnitOfWorks(new KaprukaEntities()));
            var CurDetails = CurServ.GetAll(x => x.CurrencyID == CurrencyID, null, "").SingleOrDefault();

            var totalRate = float.Parse(ACRDetails.AssignedRate.ToString()) + float.Parse(ACMDetails.Margin.ToString());

            Response.Write(String.Format("{0:N2}", float.Parse(totalRate.ToString())) + "|" + CurDetails.CurrencyCode);
        }
    }
}