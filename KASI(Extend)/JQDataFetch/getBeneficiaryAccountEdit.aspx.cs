﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend.JQDataFetch
{
    public partial class getBeneficiaryAccountEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String AccountID = Request.QueryString["AccID"].ToString();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT BankID, BranchID, AccountName, AccountNumber, CurrencyID, AcctStatus , BENE.BeneficiaryName
                            FROM dbo.BeneficiaryPaymentMethods BPM INNER JOIN dbo.Beneficiaries BENE ON BENE.BeneficiaryID = BPM.BeneficiaryID
                            WHERE AccountID = " + AccountID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BankID"].ToString() + "|" + sdr["BranchID"].ToString() + "|" + sdr["AccountName"].ToString() + "|" + sdr["AccountNumber"].ToString() + "|" + sdr["CurrencyID"].ToString() + "|" + sdr["AcctStatus"].ToString() + "|" + sdr["BeneficiaryName"].ToString();
                    }
                }

                conn.Close();
            }

            Response.Write(output);
        }
    }
}