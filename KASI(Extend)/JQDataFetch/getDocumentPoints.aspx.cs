﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getDocumentPoints : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String DID = Request.QueryString["DID"].ToString();
            String output = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLstmt = "SELECT DocumentPoints FROM dbo.CompliantDocumentsList WHERE CompliantDocumentID = " + DID;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = SQLstmt;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            output = sdr["DocumentPoints"].ToString();
                        }
                    }
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}