﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getMasterSMTPServerDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Settings SETT = new classes.Settings();

            String SMTPName = SETT.getSMTPServerName();
            String SMTPPort = SETT.getSMTPServerPort();
            String EmailAddress = SETT.getSMTPEmailAddress();
            String SSLEnabled = SETT.getSMTPSSLEnabled();
            String SMTPUsername = SETT.getSMTPUsername();
            String Password = SETT.getSMTPPassword();

            Response.Write(SMTPName + "|" + SMTPPort + "|" + EmailAddress + "|" + SSLEnabled + "|" + SMTPUsername + "|" + Password);
        }
    }
}