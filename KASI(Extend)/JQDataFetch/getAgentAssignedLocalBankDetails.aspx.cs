﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentAssignedLocalBankDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.MasterBankClass MBC = new classes.MasterBankClass();
            String output = String.Empty;
            List<String> AccountList = new List<String>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT MasterBankAccountID FROM dbo.AgentAssignedBanks WHERE AgentID = " + Session["AgentID"].ToString();
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        AccountList.Add(sdr["MasterBankAccountID"].ToString());
                    }
                    
                }
                conn.Close();
            }

            foreach(var Account in AccountList)
            {
                output += "BSB: " + MBC.getMasterBankBSBByAccountID(Account) + " Account No: " + MBC.getMasterBankAccountNumberbyAccountID(Account) + " Name: " + MBC.getMasterBankNameByAccountID(Account) + " Bank: " + MBC.getMasterBankByAccountID(Account) + "|";
            }

            int Length = output.Length;
            output = output.Substring(0, (Length - 1));

            Response.Write(output);
        }

    }
}