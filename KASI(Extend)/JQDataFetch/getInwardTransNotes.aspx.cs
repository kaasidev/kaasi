﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend.JQDataFetch
{
    public partial class getInwardTransNotes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = Request.QueryString["TID"].ToString();
            String output = String.Empty;

            if (!String.IsNullOrEmpty(TID))
            {
                int intTID = Int32.Parse(TID);
                InwardTransNoteService ITNServ = new InwardTransNoteService(new UnitOfWorks(new KaprukaEntities()));
                var ITNDetails = ITNServ.GetAll(x => x.TransactionID == intTID, null, "").ToList();

                if (ITNDetails.Count != 0)
                {
                    foreach (var ITN in ITNDetails)
                    {
                        output += ITN.TransactionID + "|" + ITN.Notes + "|" + ITN.CreatedDateTime + "|" + ITN.CreatedBy + "|" + ITN.CreatedByAgent + "~";
                    }

                    int Length = output.Length;
                    output = output.Substring(0, (Length - 1));
                }
                else
                {
                    output = "NORECORDS";
                }
                
                Response.Write(output);
            }
            else
            {
                Response.Write("NORECORDS.");
            }

            
        }
    }
}