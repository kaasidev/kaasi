﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace KASI_Extend_.JQDataFetch
{
    public partial class getAllBanks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(getAllBankNames());
        }

        protected String getAllBankNames()
        {
            String output = String.Empty;
            String AID = Request.QueryString["CID"].ToString();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = String.Empty;

            if (AID != "1")
            {
                strQuery = "SELECT BankCode, BankName FROM dbo.Banks WHERE CountryID = " + AID;
            }
            else
            {
                strQuery = "SELECT RecordID AS BankCode, BankName FROM dbo.Banks WHERE CountryID = " + AID;
            }
            

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = output + sdr["BankName"].ToString() + "|";
                        output = output + sdr["BankCode"].ToString() + "~";
                       
                    }

                }

                int Length = output.Length;
                if (Length != 0)
                {
                    output = output.Substring(0, (Length - 1));
                }
                

            }
            return output;
        }
    }
}