﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getMCSMTPDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Settings STS = new classes.Settings();

            String SMTPServerName = STS.getSMTPServerName();
            String SMTPPort = STS.getSMTPServerPort();
            String SSLEnabled = STS.getSMTPSSLEnabled();
            String Username = STS.getSMTPUsername();
            String Password = STS.getSMTPPassword();
            String EmailAddress = STS.getSMTPEmailAddress();


            Response.Write(SMTPServerName + "|" + SMTPPort + "|" + SSLEnabled + "|" + Username + "|" + Password + "|" + EmailAddress);
        }
    }
}