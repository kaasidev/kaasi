﻿using KASI_Extend_.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCurrencyRateTable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string agentId = Request.QueryString["agentId"];
            classes.Agent AGT = new classes.Agent();
            List<String> AgentCurrencies = new List<String>();
            AgentCurrencies = AGT.getAgentCurrencies(agentId);
            Generic GRC = new Generic();
            classes.Currencies CRC = new classes.Currencies();
            string currencyIds = "";
            string innerHtmlVal = "<table class=\"tbl_width_560\"><tbody> "+
                                  " <tr> "+
                                  "    <th class=\"exrate_headings_001\">Currency</th> " +
                                  "    <th class=\"exrate_headings_002\">KAASI Rate</th> " +
                                  //"    <th class=\"style__width_10\">&nbsp;</th> "+
                                  "    <th class=\"exrate_headings_004\">Margin</th> " +

                                  //"    <th class=\"style__width_10\">&nbsp;</th> "+
                                  "    <th class=\"exrate_headings_003\">New Rate</th> " +
                                  " </tr>";


            foreach (var currency in AgentCurrencies)
            {
                innerHtmlVal += "<tr>";
                String CurrencyCode = CRC.getCurrencyCodeFromID(currency);
                String GivenRate = CRC.getAgentCurrencyRateFromAIDandCID(agentId, currency);
                String VR = AGT.getAgentCurrencyVariation(agentId, currency);
                String MR = AGT.getAgentCurrencyMargin(agentId, currency);
                String FeeStructure = AGT.getAgentCommissionStructure(agentId);
                float ModRate = 0;
                if (FeeStructure == "FXGain")
                {
                    ModRate = float.Parse(GivenRate) - float.Parse(MR);
                }
                else
                {
                    if (!String.IsNullOrEmpty(GivenRate))
                    {
                        ModRate = float.Parse(GivenRate) + float.Parse(MR);
                    }
                    else
                    {
                        ModRate = 0;
                        GivenRate = "0";
                        MR = "0";
                    }
                    
                }
                
                //float ModRate = GRC.CalculateRate(float.Parse(GivenRate), float.Parse(MR), VR);
                innerHtmlVal += " <td class=\"exrate_contents_001\">" + CurrencyCode + "</td>" +
                                    "<td> " +
                                    "<input  type=\"text\" value=\"" + String.Format("{0:N4}", float.Parse(GivenRate)) + "\" id=\"curKAASIRate_" + currency + "\"  class=\"aa_input exrate_contents_002\" readonly='readonly'>" +
                                    "</td>" +
                                    //" <td></td> " +
                                    "<td><input  type=\"text\" value=\"" + String.Format("{0:N4}", float.Parse(MR)) + "\" onblur=\"calcNewRate(" + currency + ")\" id=\"curMargin_" + currency + "\" class=\"aa_input exrate_contents_004\"></td>" +
                                    //" <td></td> " +
                                    "<td><input  type=\"text\" value=\"" + String.Format("{0:N4}", ModRate) + "\" id=\"tdNewRate" + currency + "\" class=\"aa_input exrate_contents_003\" readonly='readonly'></td>";



                innerHtmlVal += "</tr>";
                currencyIds =currencyIds+"curMargin_"+currency+"^";
            }

            innerHtmlVal += "</tbody></table><input type=\"hidden\" id=\"hidCurrencyIdList\" value=\"" + currencyIds + "\" />";


            Response.Write(innerHtmlVal);

        }
    }
}