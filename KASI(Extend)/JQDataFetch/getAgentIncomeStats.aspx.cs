﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using System.Web.Script.Serialization;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentIncomeStats : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<String> AgentIDs = new List<String>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AgentID FROM dbo.Agents";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        AgentIDs.Add(sdr["AgentID"].ToString());
                    }
                }
                conn.Close();
            }

            List<IncomeStats> incomeStats = new List<IncomeStats>();

            foreach (var AgentID in AgentIDs)
            {
                var income = getTodaysTotalForAgent(AgentID);
                incomeStats.Add(new IncomeStats { AgentName = AgentID, AgentIncome = income });
            }

            string jsonString = new JavaScriptSerializer().Serialize(incomeStats);

            Response.Write(jsonString);
        }

        private float getTodaysTotalForAgent(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float output = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT SUM(CreditAmount) AS TotAmount FROM dbo.CreditTransactions WHERE AgentID = " + AID 
                    + " AND DAY(CreatedDateTime) = DAY(getDate()) AND MONTH(CreatedDateTime) = MONTH(getDate()) AND YEAR(CreatedDateTime) = YEAR(getDate())";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            if (sdr["TotAmount"].Equals(DBNull.Value))
                            {
                                output = 0;
                            }
                            else
                            {
                                output = float.Parse(sdr["TotAmount"].ToString());
                            }
                            
                        }
                    }
                    else
                    {
                        output = 0;
                    }
                }
                conn.Close();
            }

            return output;
        }

    }
}