﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.JQDataFetch
{
    public partial class getInwardTransactionCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var obj = new CustomerAndTransaction();
            obj.status = "1";
            var tranID = Convert.ToInt32(Request.QueryString["trID"].ToString());

            var cusTran = getCustomerTransaction(tranID);
            var json = new JavaScriptSerializer().Serialize(cusTran);

            Response.Write(json);

        }

        private CustomerAndTransaction getCustomerTransaction(int Id)
        {
            IncomingTransactionService ITServ = new IncomingTransactionService(new UnitOfWorks(new KaprukaEntities()));

            var getTransaList = ITServ.GetAll(x => x.ID == Id, null, "").ToList();
           

            InCustomerService cusService = new InCustomerService(new UnitOfWorks(new KaprukaEntities()));
            IncomingAgentService IAServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));

            var EntityList = new KaprukaEntities();
            
            var currentDate = DateTime.Now;
            var lastOneMonth = currentDate.Date.AddDays(-30);
            var lastOneYear = currentDate.Date.AddDays(-365);


            var obj = new CustomerAndTransaction();
            obj.status = "0";
            if ( getTransaList.Count() > 0)
            {
                
              var  getTransa = getTransaList[0];
                var BeneTransList = ITServ.GetAll(x => x.InboundBeneficiaryID == getTransa.InboundBeneficiaryID, null, "").ToList();
                var last30daysdaterange = (from thelist in BeneTransList
                                           where
                                                    thelist.CreatedDateTime >= lastOneMonth && thelist.CreatedDateTime < currentDate
                                           select thelist).ToList();
                var Last30daysAmount = last30daysdaterange.Sum(x => x.DollarAmount).ToString();



                if(last30daysdaterange.Count() > 0)
                {
                    obj.MonthlyAudAmount = String.Format("{0:N2}", float.Parse(Last30daysAmount));
                    obj.MonthlyTransaction = last30daysdaterange.Count().ToString();
                }



                var last365daysdaterange = (from thelist in BeneTransList
                                            where thelist.CreatedDateTime >= lastOneYear && thelist.CreatedDateTime < currentDate
                                            select thelist).ToList();

                var last365daysAmount = last365daysdaterange.Sum(x => x.DollarAmount).ToString();
                if (last365daysAmount.Count() > 0)
                {
                    obj.YearlyAudAmount = String.Format("{0:N2}", float.Parse(last365daysAmount));
                    obj.YealryTransaction = last365daysdaterange.Count().ToString();

                }


                obj.status = "1";
                obj.AccountNumber = getTransa.AccountNumber;
                obj.AgentID = getTransa.AgentID;
                obj.AlTransID = getTransa.AlTransID;
                obj.BankName = getTransa.BankName;
                obj.BeneficiaryName = getTransa.BeneficiaryName;
                obj.BSB = getTransa.BSB;
                obj.CurrencyCode = getTransa.CurrencyCode;
                obj.CustomerID = getTransa.CustomerID;
                obj.DollarAmount = String.Format("{0:N2}", float.Parse(getTransa.DollarAmount.ToString()));
                obj.RefID = getTransa.RefID;
                obj.Purpose = getTransa.Purpose;
                obj.MoneyRecDate = getTransa.CreatedDateTime.ToString().Substring(0, 10);

                var customer = cusService.GetAll(x => x.CustomerID == getTransa.CustomerID, null, "").ToList();
                var beneficiary = (from thelist in EntityList.InboundIFTIBeneficiaries
                                   where
                                      thelist.ID == getTransa.InboundBeneficiaryID
                                   select thelist).SingleOrDefault();


                var IADetails = IAServ.GetAll(x => x.AgentID == getTransa.AgentID, null, "").SingleOrDefault();
                obj.AffiliateName = IADetails.AgentName;
                obj.KAASIRefID = getTransa.ID.ToString() ;
                obj.BeneFullAddress = beneficiary.Address + " " + beneficiary.City + " " + beneficiary.State + " " + beneficiary.Postcode + " " + beneficiary.Country;
                if (customer != null && customer.Count() > 0)
                {
                    obj.CustomerName = customer[0].FullName;
                    obj.CustomerAddress = customer[0].AddressLine1;
                    obj.ContactNo = customer[0].Mobile;
                    if (beneficiary != null)
                    {
                        obj.KYCStatus = beneficiary.resultDescription;
                    }
                    else
                    {
                        obj.KYCStatus = "NOTDONE";
                    }
                    

                }

            }

           

            return obj;

        }


        public class CustomerAndTransaction
        {

            public string status { get; set; }
            public int AgentID { get; set; }
            public int CustomerID { get; set; }
            public Nullable<System.DateTime> CreatedDateTime { get; set; }
            public string Status { get; set; }
            public string CreatedBy { get; set; }
            public string Purpose { get; set; }
            public string DollarAmount { get; set; }
            public Nullable<int> RefID { get; set; }
            public string CurrencyCode { get; set; }
            public string AlTransID { get; set; }
            public string BeneficiaryName { get; set; }
            public string BankName { get; set; }
            public Nullable<bool> InsertedIntoABAFile { get; set; }
            public Nullable<System.DateTime> InsertedIntoABAFileDateTime { get; set; }
            public string InsertedIntoABAFileBy { get; set; }
            public Nullable<bool> InsertedIntoIFTIFile { get; set; }
            public Nullable<System.DateTime> InsertedIntoIFTIFileDateTime { get; set; }
            public string InsertedIntoIFTIFileBy { get; set; }
            public string BSB { get; set; }
            public string AccountNumber { get; set; }

            public string CustomerName { get; set; }
            public string CustomerAddress { get; set; }
            public string ContactNo { get; set; }
            public string MonthlyAudAmount { get; set; }
            public string YearlyAudAmount { get; set; }
            public string MonthlyTransaction { get; set; }
            public string YealryTransaction { get; set; }

            public string KYCStatus { get; set; }
            public string AffiliateName { get; set; }
            public string KAASIRefID { get; set; }
            public String BeneFullAddress { get; set; }
            public String MoneyRecDate { get; set; }

        }
    }
}