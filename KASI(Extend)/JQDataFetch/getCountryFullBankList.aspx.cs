﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCountryFullBankList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String CID = Request.QueryString["CID"].ToString();
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankName, BankCode FROM dbo.Banks WHERE RoutingBank = 'Y' AND CountryID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = output + sdr["BankName"].ToString() + "|" + sdr["BankCode"].ToString() + "~";
                    }
                }
                conn.Close();
            }

            int Length = output.Length;
            output = output.Substring(0, (Length - 1));

            Response.Write(output);
        }
    }
}