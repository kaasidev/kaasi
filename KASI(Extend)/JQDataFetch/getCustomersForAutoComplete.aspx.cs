﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomersForAutoComplete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String searchTerm = Request.QueryString["term"].ToString();
            Response.Write(getCustomers(searchTerm));
        }

        protected String getCustomers(String getText)
        {
            String strOutput = "[";

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 100 * FROM dbo.Customers WHERE FullName LIKE '%" + getText + "%' OR REPLACE(Mobile, ' ', '') LIKE '%" + getText + "%' OR DOB LIKE '%" + getText + "%' OR CustomerID LIKE '%" + getText + "%'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            strOutput = strOutput + "{\"id\": \"" + sdr["CustomerID"].ToString() + "\", \"value\": \"" + sdr["CustomerID"].ToString() + " - " + sdr["FullName"].ToString() + " - " + sdr["Suburb"].ToString().ToUpper() + " " + sdr["State"].ToString().ToUpper() + " - Mobile: " + sdr["Mobile"].ToString() + " - DOB: " + sdr["DOB"].ToString() + "\"},";
                        }

                    }

                    int length = strOutput.Length;
                    strOutput = strOutput.Substring(0, (length - 1));
                }

                strOutput = strOutput + "]";

            }
            
            return strOutput;
        }
    }
}