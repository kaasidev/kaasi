﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerTotals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            Customer CUST = new Customer();
            Response.Write(CUST.getMonthlyRemittanceTotal(CID) + "|" + CUST.getYearlyRemittancetotal(CID) + "|" + CUST.getSumMonthlyRemit(CID) + "|" + CUST.getSumYearlyRemit(CID));
        }
    }
}