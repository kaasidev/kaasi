﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerReviewNotes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            String TID = Request.QueryString["TID"].ToString();
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT * FROM dbo.CustomerNotes WHERE TransactionID = " + TID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + sdr["TransactionID"].ToString() + "|" + sdr["Notes"].ToString() + "|" + sdr["CreatedDateTime"].ToString() + "|" + sdr["CreatedBy"].ToString() + "|" + sdr["CreatedByAgent"].ToString() + "~";
                        }
                    }
                    else
                    {
                        output = "NORECORDS.";
                    }
                }
                conn.Close();
            }

            int Length = output.Length;
            output = output.Substring(0, (Length - 1));

            Response.Write(output);
        }
    }
}