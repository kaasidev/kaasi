﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getBeneficiaryDetails : System.Web.UI.Page
    {

        Beneficiary beneficiary = new Beneficiary();

        protected void Page_Load(object sender, EventArgs e)
        {
            String BID = Request.QueryString["BeneID"].ToString();
            Response.Write(buildReturnString(BID));
        }

        public string buildReturnString(String BID)
        {
            string fullReturnString = String.Empty;
            string fullBeneficiaryAddress = String.Empty;
            string BeneficiaryName = String.Empty;
            string BeneficiaryID = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetBeneficiaryDetails";
            cmd.Parameters.Add("@BeneficiaryID", SqlDbType.Int).Value = BID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    fullReturnString = fullReturnString + idr["BeneficiaryID"].ToString(); //0
                    fullReturnString = fullReturnString + "|" + idr["BeneficiaryLastName"].ToString();//1
                    fullReturnString = fullReturnString + "|" + idr["BeneficiaryFirstNames"].ToString();//2
                    fullReturnString = fullReturnString + "|" + idr["AddressLine1"].ToString();//3
                    fullReturnString = fullReturnString + "|" + idr["AddressLine2"].ToString();//4
                    fullReturnString = fullReturnString + "|" + idr["Suburb"].ToString();//5
                    fullReturnString = fullReturnString + "|" + idr["State"].ToString();//6
                    fullReturnString = fullReturnString + "|" + idr["Postcode"].ToString();//7
                    fullReturnString = fullReturnString + "|" + idr["Country"].ToString();//8
                    fullReturnString = fullReturnString + "|" + idr["TelHome"].ToString();//9
                    fullReturnString = fullReturnString + "|" + idr["EmailAddress"].ToString();//10
                    fullReturnString = fullReturnString + "|" + idr["NationalityCardID"].ToString();//11
                    fullReturnString = fullReturnString + "|" + idr["Relationship"].ToString();//12
                    fullReturnString = fullReturnString + "|" + idr["CountryOfBirth"].ToString();//13
                    fullReturnString = fullReturnString + "|" + idr["DOB"].ToString();//14
                    fullReturnString = fullReturnString + "|" + idr["PlaceofBirth"].ToString();//15
                    fullReturnString = fullReturnString + "|" + idr["Nationality"].ToString();//16
                    fullReturnString = fullReturnString + "|" + idr["AKA"].ToString();//17
                    fullReturnString = fullReturnString + "|" + idr["BeneficiaryName"].ToString();//18
                    fullReturnString = fullReturnString + "|" + idr["Mobile"].ToString();//19
                }
            }
            conn.Close();
            return fullReturnString;
        }
    }
}