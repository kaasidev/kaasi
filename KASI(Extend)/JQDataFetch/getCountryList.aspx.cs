﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCountryList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Countries> LCt = new List<Countries>();
            using (SqlConnection conn = new SqlConnection())
            {
                
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM dbo.Countries";
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Countries Ctr = new Countries();
                            Ctr.CountryID = sdr["CountryID"].ToString();
                            Ctr.CountryName = sdr["CountryName"].ToString();
                            LCt.Add(Ctr);
                        }
                    }
                    conn.Close();
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            Response.Write(js.Serialize(LCt));
        }
    }
}