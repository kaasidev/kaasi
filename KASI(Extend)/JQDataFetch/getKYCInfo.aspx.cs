﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getKYCInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            Customer cust = new Customer();
            String LastRep = cust.KYCLastUpdatePerson(CID);
            String AmendDate = cust.KYCLAstUpdateTime(CID);
            String ExpDate = cust.KYCExpiryDate(CID);

            Response.Write(LastRep + "|" + AmendDate + "|" + ExpDate);
        }
    }
}