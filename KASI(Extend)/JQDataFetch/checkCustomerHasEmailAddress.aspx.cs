﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class checkCustomerHasEmailAddress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Customer CUS = new Customer();

            String CID = Request.QueryString["CID"].ToString();

            String CustEmail1 = String.Empty;
            String CustEmail2 = String.Empty;

            String output = String.Empty;

            CustEmail1 = CUS.getCustomerEmailAddress1(CID);
            CustEmail2 = CUS.getCustomerEmailAddress2(CID);

            if (CustEmail1 == "" && CustEmail2 == "")
            {
                output = "NoEmail";
            }
            else
            {
                output = "HasEmail";
            }

            Response.Write(output);
        }
    }
}