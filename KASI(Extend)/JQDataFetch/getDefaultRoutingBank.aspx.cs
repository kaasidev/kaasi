﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getDefaultRoutingBank : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String BankCode = String.Empty;
            String OriginalBankCode = String.Empty;
            String CurrentBankCode = String.Empty;
            String FinalCode = String.Empty;
            String TransactionID = Request.QueryString["TID"].ToString();
            String BankPath = String.Empty;
            String CurrentBankName = String.Empty;
            classes.Transactions trans = new classes.Transactions();
            classes.Banking banks = new classes.Banking();

            OriginalBankCode = trans.getBankCodeForTransaction(TransactionID);
            BankCode = trans.getRoutingBankCodeForTransaction(TransactionID);
            CurrentBankCode = OriginalBankCode;

            if (OriginalBankCode == BankCode)
            {
                FinalCode = BankCode + "|NOROUTING|NOPATH";
            }
            else
            {
                BankPath = banks.getBankNameFromBankCode(CurrentBankCode) + " > ";
                while (CurrentBankCode != BankCode)
                {
                    CurrentBankCode = BankCode;
                    CurrentBankName = banks.getBankNameFromBankCode(CurrentBankCode);
                    BankCode = banks.getRoutingBank(CurrentBankCode);
                    FinalCode = BankCode;
                    BankPath = BankPath + CurrentBankName + " > ";
                }
                int Length = BankPath.Length;
                BankPath = BankPath.Substring(0, (Length - 2));
            }

            Response.Write(FinalCode + "|ROUTING|" + BankPath);
        }
    }
}