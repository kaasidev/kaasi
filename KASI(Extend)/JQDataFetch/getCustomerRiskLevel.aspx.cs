﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerRiskLevel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String RiskLevel = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT RiskLevel FROM dbo.Customers WHERE CustomerID = " + CID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            RiskLevel = sdr["RiskLevel"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            Response.Write(RiskLevel);
        }
    }
}