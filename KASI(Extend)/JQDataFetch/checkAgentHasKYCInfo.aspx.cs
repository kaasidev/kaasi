﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class checkAgentHasKYCInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String AID = Request.QueryString["AID"].ToString();
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT resultDescription, KYCCheckDate FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["resultDescription"] == DBNull.Value)
                        {
                            output = "NOKYC";
                        }
                        else
                        {
                            output = sdr["resultDescription"].ToString() + "|" + sdr["KYCCheckDate"].ToString();
                        }
                        
                    }
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}