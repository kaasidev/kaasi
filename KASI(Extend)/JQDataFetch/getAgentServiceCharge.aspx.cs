﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentServiceCharge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String AmtToBeRemitted = String.Empty;
            String AID = String.Empty;
            String AccountID = String.Empty;
            float ATBR = 0;
            String TransFee = "Undefined";

            String LookInTier1 = String.Empty;
            String LookInTier2 = String.Empty;
            String LookInTier3 = String.Empty;

            BeneficiaryAccounts BeneAcc = new BeneficiaryAccounts();

            AmtToBeRemitted = Request.QueryString["ATBR"].ToString();
            AID = Request.QueryString["AID"].ToString();
            AccountID = Request.QueryString["AccID"].ToString();

            String CID = String.Empty;
            CID = BeneAcc.getBeneficiaryCountryIDBasedOnAccount(AccountID);

            String CurrencyID = String.Empty;
            CurrencyID = BeneAcc.getBeneficiaryAccountCurrencyID(AccountID);

            if (AmtToBeRemitted != "")
            {
                ATBR = float.Parse(AmtToBeRemitted);
            }

            float Tier1From = 0;
            float Tier1To = 0;

            float Tier2From = 0;
            float Tier2To = 0;

            float Tier3From = 0;
            float Tier3To = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Tier1From, Tier1To, Tier2From, Tier2To, Tier3From, Tier3To, Tier1Amount, Tier2Amount, Tier3Amount FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + AID + " AND CountryID = " + CID + " AND CurrencyID = " + CurrencyID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["Tier1From"].ToString() != "")
                        {
                            Tier1From = float.Parse(sdr["Tier1From"].ToString());
                        }
                        else
                        {
                            Tier1From = 0;
                        }

                        if (sdr["Tier1To"].ToString() != "")
                        {
                            Tier1To = float.Parse(sdr["Tier1To"].ToString());
                        }
                        else
                        {
                            Tier1To = 0;
                        }

                        if (sdr["Tier2From"].ToString() != "")
                        {
                            Tier2From = float.Parse(sdr["Tier2From"].ToString());
                        }
                        else
                        {
                            Tier2From = 0;
                        }

                        if (sdr["Tier2To"].ToString() != "")
                        {
                            Tier2To = float.Parse(sdr["Tier2To"].ToString());
                        }
                        else
                        {
                            Tier2To = 0;
                        }

                        if (sdr["Tier3From"].ToString() != "")
                        {
                            Tier3From = float.Parse(sdr["Tier3From"].ToString());
                        }
                        else
                        {
                            Tier3From = 0;
                        }

                        if (sdr["Tier3To"].ToString() != "")
                        {
                            Tier3To = float.Parse(sdr["Tier3To"].ToString());
                        }
                        else
                        {
                            Tier3To = 0;
                        }


                        // Confirm which brackets to look into
                        if (Tier1From != 0 && Tier1To != 0)
                        {
                            LookInTier1 = "Y";
                        }
                        else
                        {
                            LookInTier1 = "N";
                        }


                        if (Tier2From != 0 && Tier2To != 0)
                        {
                            LookInTier2 = "Y";
                        }
                        else
                        {
                            LookInTier2 = "N";
                        }

                        if (Tier3From != 0 && Tier3To != 0)
                        {
                            LookInTier3 = "Y";
                        }
                        else
                        {
                            LookInTier3 = "N";
                        }


                        if (LookInTier1 == "Y")
                        {
                            if (ATBR >= Tier1From && ATBR <= Tier1To)
                            {
                                TransFee = sdr["Tier1Amount"].ToString();
                            }
                        }

                        if (LookInTier2 == "Y")
                        {
                            if (ATBR >= Tier2From && ATBR <= Tier2To)
                            {
                                TransFee = sdr["Tier2Amount"].ToString();
                            }
                        }

                        if (LookInTier3 == "Y")
                        {
                            if (ATBR >= Tier3From && ATBR <= Tier3To)
                            {
                                TransFee = sdr["Tier3Amount"].ToString();
                            }
                        }
                    }
                    
                }
            }
            conn.Close();
            Response.Write(TransFee);
        }
        
    }
}