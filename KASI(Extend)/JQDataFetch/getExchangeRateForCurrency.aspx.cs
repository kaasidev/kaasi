﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getExchangeRateForCurrency : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CurrencyCode = Request.QueryString["CID"].ToString();
            String Rate = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CurrencyExchangeRate FROM dbo.Currencies WHERE CurrencyCode = '" + CurrencyCode + "'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = sdr["CurrencyExchangeRate"].ToString();
                            }
                        }
                    }
                    conn.Close();
                } 
                
            }

            Response.Write(Rate);
        }
    }
}