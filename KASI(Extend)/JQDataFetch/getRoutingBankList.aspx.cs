﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getRoutingBankList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;
            String CID = String.Empty;
            CID = Request.QueryString["CID"].ToString();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                String SQLstmt = @"SELECT BankCode, BankName FROM dbo.Banks WHERE BankCode IN(SELECT BankID FROM dbo.AgentForeignBankSettings 
                        WHERE CountryID = " + CID + " AND AgentID = " + Session["AgentID"].ToString() + ")";

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = SQLstmt;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            output = output + sdr["BankName"].ToString() + "|";
                            output = output + sdr["BankCode"].ToString() + "~";
                        }
                    }
                    int Length = output.Length;
                    if (Length == 0)
                    {
                        output = "NOBANKS";
                    }
                    else
                    {
                        output = output.Substring(0, (Length - 1));
                    }
                    
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}