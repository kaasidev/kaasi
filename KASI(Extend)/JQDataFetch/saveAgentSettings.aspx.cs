﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class saveAgentSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string output = "";
            try
            {
                if (Session["AgentID"] != null)
                {
                    int agentId = int.Parse(Session["AgentID"].ToString());
                    //AgentService ser = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                    Agent ag = new Agent();
                    ag.AgentAddress1 = Request.QueryString["address1"];
                    ag.AgentAddress2 = Request.QueryString["address2"];
                    ag.AgentCity = Request.QueryString["suburb"];
                    ag.AgentPhone = Request.QueryString["phone"];
                    ag.AgentFax = Request.QueryString["fax"];
                    ag.AgentState = Request.QueryString["state"];
                    ag.AgentPostcode = Request.QueryString["postcode"];
                    ag.AgentEmail = Request.QueryString["email"];
                    ag.AgentABN = Request.QueryString["abn"];
                    ag.AgentACN = Request.QueryString["acn"];

                    //ser.Update(ag);
                    //output ="1^";

                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

                    String SqlStmt = "update Agents set AgentAddress1='" + ag.AgentAddress1 + "',AgentAddress2='" + ag.AgentAddress2 + "',AgentCity='" + ag.AgentCity + "',AgentPhone='" + ag.AgentPhone + "',AgentFax='"+ag.AgentFax+"',AgentState='"+ag.AgentState+"',AgentPostcode='"+ag.AgentPostcode+"',AgentEmail='"+ag.AgentEmail+"',AgentABN='"+ag.AgentABN+"',AgentACN='"+ag.AgentACN+"' where AgentID=" + agentId;

                    using (SqlCommand cmd = new SqlCommand(SqlStmt))
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        //cmd.Parameters.Add("@AgentAddress1", SqlDbType.VarChar, 100).Value = ag.AgentAddress1;
                        //cmd.Parameters.Add("@AgentAddress2", SqlDbType.VarChar, 100).Value = ag.AgentAddress2;
                        //cmd.Parameters.Add("@AgentCity", SqlDbType.VarChar, 50).Value = ag.AgentCity;
                        //cmd.Parameters.Add("@AgentPhone", SqlDbType.VarChar, 14).Value = ag.AgentPhone;
                        //cmd.Parameters.Add("@AgentFax", SqlDbType.VarChar, 14).Value = ag.AgentFax;
                        //cmd.Parameters.Add("@AgentState", SqlDbType.VarChar, 20).Value = ag.AgentState;
                        //cmd.Parameters.Add("@AgentPostcode", SqlDbType.VarChar, 10).Value = ag.AgentPostcode;
                        //cmd.Parameters.Add("@AgentEmail", SqlDbType.VarChar, 50).Value = ag.AgentEmail;
                        //cmd.Parameters.Add("@AgentABN", SqlDbType.VarChar, 100).Value = ag.AgentABN;
                        //cmd.Parameters.Add("@AgentACN", SqlDbType.VarChar, 100).Value = ag.AgentACN;
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                    output = "1^";


                }
                else
                {
                    output = "2^";
                }
            }
            catch (Exception ex)
            {
                output = "0^" + ex.ToString() ;
            }
            Response.Write(output);
        }
    }
}