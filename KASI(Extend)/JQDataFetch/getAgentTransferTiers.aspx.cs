﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentTransferTiers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CountryID = Request.QueryString["CTID"].ToString();
            String CurrencyID = Request.QueryString["CRID"].ToString();
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT * FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + Session["AgentID"].ToString() + " AND CountryID = " + CountryID + " AND CurrencyID = " + CurrencyID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = String.Format("{0:N2}", float.Parse(sdr["Tier1To"].ToString())) + "|" + String.Format("{0:N2}", float.Parse(sdr["Tier1Amount"].ToString())) + "|" + String.Format("{0:N2}", float.Parse(sdr["Tier2To"].ToString())) + "|" + String.Format("{0:N2}", float.Parse(sdr["Tier2Amount"].ToString())) + "|" + String.Format("{0:N2}", float.Parse(sdr["Tier3To"].ToString())) + "|" + String.Format("{0:N2}", float.Parse(sdr["Tier3Amount"].ToString())); 
                    }
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}