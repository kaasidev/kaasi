﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentBankingUploadInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;
            String AID = Request.QueryString["AID"].ToString();
            String BID = Request.QueryString["BID"].ToString();
            

            classes.Banking BKN = new classes.Banking();

            String BankName = BKN.getBankNameFromBankID(BID);
             
            output += BankName + "|";
            output += getTotalTransactionForBank(BankName, AID) + "|";
            output += getTotalAUDForBank(BankName, AID) + "|";
            output += getTotalForeignAmountForBank(BankName, AID) + "|";
            output += BKN.getBankUploadFileNameFromBankID(BID);

            Response.Write(output);

        }

        private String getTotalTransactionForBank(String BankName, String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT COUNT(*) AS TOTCount FROM dbo.Transactions WHERE Bank = '" + BankName + "' AND AgentID = " + AID + " AND Status = 'AWAITING TRANSFER'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TOTCount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        private String getTotalAUDForBank(String BankName, String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SUM(DollarAmount) AS TOTCount FROM dbo.Transactions WHERE Bank = '" + BankName + "' AND AgentID = " + AID + " AND Status = 'AWAITING TRANSFER'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TOTCount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        private String getTotalForeignAmountForBank (String BankName, String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SUM(RemittedAmount) AS TOTCount FROM dbo.Transactions WHERE Bank = '" + BankName + "' AND AgentID = " + AID + " AND Status = 'AWAITING TRANSFER'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TOTCount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }
    }
}