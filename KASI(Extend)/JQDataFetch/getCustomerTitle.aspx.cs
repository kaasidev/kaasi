﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerTitle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string agentId = Session["AgentID"].ToString();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "select Title from Customers where CustomerId="+agentId;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = sdr["Title"].ToString();
                        }
                    }
                    conn.Close();
                }
            }
            if (string.IsNullOrEmpty(output))
            {
                output = "";
            }
           
            Response.Write(output);
        }
    }
}