﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getTransactionBasicDetailsForEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions trans = new classes.Transactions();

            String TID = Request.QueryString["TID"].ToString();

            String output = String.Empty;
            output = trans.getTransactionDateTime(TID) + "|";
            output = output + trans.getCustomerIDFromTransactionID(TID) + "|";
            output = output + trans.getCustomerFullNameFromTransaction(TID) + "|";
            output = output + trans.getTransactionRate(TID) + "|";
            output = output + trans.getTransactionSendingAmount(TID) + "|";
            output = output + trans.getTransactionBeneficiary(TID) + "|";
            output = output + trans.getTransactionAccount(TID) + "|";
            output = output + trans.getReceivedAmountFromTransaction(TID);

            Response.Write(output);
        }
    }
}