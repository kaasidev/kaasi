﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCountryDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Currencies curr = new Currencies();
            classes.Countries CNT = new Countries();
            String CID = Request.QueryString["CID"].ToString();

            String MasterCurrencyCode = curr.getCountryMasterCurrencyCode(CID);
            String MasterCurrencyName = curr.getCountryMasterCurrencyName(CID);
            String AdditionalCodes = curr.getCountryAdditionalCurrencyCodes(CID);
            String CountryFlagFilePath = CNT.getCountryFlagFilePathLarge(CID);
            String CountryName = CNT.getCountryNameByID(CID);

            Response.Write(MasterCurrencyCode + "~" + MasterCurrencyName + "~" + AdditionalCodes + "~" + CountryFlagFilePath + "~" + CountryName);
        }
    }
}