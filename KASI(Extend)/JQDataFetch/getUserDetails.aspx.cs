﻿using KASI_Extend_.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getUserDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userId = Request.QueryString["id"].ToString();
            LoginUserModel model = new LoginUserModel();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String CountryID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT * FROM dbo.Logins WHERE LoginID =" + userId;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        model.Email = sdr["Email"].ToString();
                        model.FirstName = sdr["FirstName"].ToString();
                        model.LastName = sdr["LastName"].ToString();
                        model.LoginGroup = sdr["LoginGroup"].ToString();
                        model.LoginType = sdr["LoginType"].ToString();
                        model.Password = sdr["Password"].ToString();
                        model.UserName = sdr["UserName"].ToString();
                        model.AgentID = sdr["AgentID"].ToString();
                        model.CompMan = sdr["IsComplianceManager"].ToString();
                        model.CompManPIN = sdr["AdminOverrideCode"].ToString();
                    }
                }
                conn.Close();
            }
            string jsonArr = JsonConvert.SerializeObject(model);
            Response.Write(jsonArr);
        }
    }
}