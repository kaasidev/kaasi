﻿using KASI_Extend_.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class GetMasterBankDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string aid = Request.QueryString["id"].ToString();
            MasterBankModel model = new MasterBankModel();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String CountryID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT * FROM dbo.MasterCompanyBanks WHERE BankAccountID ="+aid;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string bsb = sdr["BSB"].ToString();
                        string[] arr = bsb.Split('-');
                        if (arr.Length == 1)
                        {
                            model.BSB = bsb;
                        }
                        else
                        {
                            model.BSB = arr[0];
                            model.BSB2 = arr[1];
                        }
                        model.AccountName = sdr["AccountName"].ToString();
                        model.AccountNumber = sdr["AccountNumber"].ToString();
                        model.BankName = sdr["BankName"].ToString();
                    
                    }
                }
                conn.Close();
            }
            string jsonArr = JsonConvert.SerializeObject(model);
            Response.Write(jsonArr);
        }
    }
}