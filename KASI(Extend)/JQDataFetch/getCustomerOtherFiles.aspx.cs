﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using Kapruka.Service;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerOtherFiles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String requestedCID = Request.QueryString["CID"].ToString();
            String transactionID = Request.QueryString["TID"];
            String CID = requestedCID;
            Response.Write(returnString(CID, transactionID));
        }

        public String returnString(String CID, String TID)
        {
            String FullReturnString = "{\"data\":[";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerOtherFiles";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            if (!String.IsNullOrEmpty(TID))
            {
                cmd.Parameters.Add("@TransactionID", SqlDbType.Int).Value = TID;
            }
            else
            {
                cmd.Parameters.Add("@TransactionID", SqlDbType.Int).Value = DBNull.Value;
            }

            cmd.Connection = conn;

            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        //var fileDecrypt = DecryptFile(CID,
                        // sdr["DocumentName"].ToString(), sdr["DocumentGUID"].ToString());
                        FullReturnString = FullReturnString + "[\"" + sdr["Description"] + "\","
                            + "\"" + "<a href='./CustomerOtherDocuments/" + CID + "/" + sdr["DocumentGUID"].ToString() + "' target='_blank'>VIEW</a>" + "\"],";

                    }
                    int Length = FullReturnString.Length;
                    FullReturnString = FullReturnString.Substring(0, (Length - 1));
                }

                FullReturnString = FullReturnString + "]}";

            }
            conn.Close();
            return FullReturnString;
        }


        private string DecryptFile(string cusID, string fileName, string fileGuIDName)
        {
            string fPath = "";
            var customerFilePath = HttpContext.Current.Server.MapPath("~/CustomerOtherDocuments/") + cusID + "\\" + fileGuIDName;
            var tempPath = HttpContext.Current.Server.MapPath("~/temp/");
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);
            var decFiLeName = fileName;
            fPath = tempPath + decFiLeName;
            if (File.Exists(fPath))
                return fPath;
            if (File.Exists(customerFilePath))
                Utilities.Decrypt(customerFilePath, fPath);

            return fPath;
        }
    }
}