﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAllAgents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;
            AgentService AGTServ = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            var AGTDetails = AGTServ.GetAll().ToList();

            if (AGTDetails != null)
            {
                foreach (var Agent in AGTDetails)
                {
                    output = output + Agent.AgentName + "|";
                    output = output + Agent.AgentID + "~";
                }

                int Length = output.Length;
                output = output.Substring(0, (Length - 1));

                Response.Write(output);
            }
            else
            {
                Response.Write("NOAGENTS");
            }
            

        }
            
        
    }
}