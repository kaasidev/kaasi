﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAdminViewTransactionDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions TRANS = new classes.Transactions();
            classes.Customer CUST = new classes.Customer();
            classes.Beneficiary BENE = new classes.Beneficiary();
            classes.BeneficiaryAccounts BENEACC = new classes.BeneficiaryAccounts();

            String TID = Request.QueryString["TID"].ToString();
            String CID = TRANS.getCustomerIDFromTransactionID(TID);
            String BID = TRANS.getTransactionBeneficiary(TID);
            String AccID = TRANS.getTransactionAccount(TID);

            String output = String.Empty;

            output += TRANS.getTransactionSendingAmount(TID) + "|";
            output += TRANS.getTransactionServiceCharge(TID) + "|";
            output += TRANS.getTransactionRate(TID) + "|";
            output += TRANS.getReceivedAmountFromTransaction(TID) + "|";
            output += TRANS.getRemittedCurrency(TID) + "|";
            output += CID + "|";
            output += CUST.getCustomerFullName(CID) + "|";
            output += CUST.getCustomerFullAddress(CID) + "|";
            output += CUST.getCustomerMobile(CID) + "|";
            output += CUST.getCustomerEmailAddress1(CID) + "|";
            output += TRANS.getTransactionPurpose(TID) + "|";
            output += TRANS.getTransactionSourceOfFunds(TID) + "|";
            output += TRANS.getTransactionNotes(TID) + "|";
            output += BENE.getBeneficiaryName(BID) + "|";
            output += BENE.getBeneficiaryFullAddress(BID) + "|";
            output += BENE.getBeneficiaryNIC(BID) + "|";
            output += BENE.getBeneficiaryRelationship(BID) + "|";
            output += BENEACC.getBeneficiaryBankName(AccID) + "|";
            output += BENEACC.getBeneficiaryBranchName(AccID) + "|";
            output += BENEACC.getBeneficiaryAccountName(AccID) + "|";
            output += BENEACC.getBeneficiaryAccountNumber(AccID) + "|";

            Response.Write(output);
        }
    }
}