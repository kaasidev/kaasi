﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace KASI_Extend_.JQDataFetch
{
    public partial class performTriangularVerification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String MobileNumber = String.Empty;
            String LastName = String.Empty;
            String DOB = String.Empty;

            MobileNumber = Request.QueryString["MOB"].ToString();
            LastName = Request.QueryString["LN"].ToString();
            DOB = Request.QueryString["DOB"].ToString();

            String HasMatch = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT TOP 1 CustomerID FROM dbo.Customers WHERE Replace(Mobile, ' ','') = Replace('" + MobileNumber + "', ' ', '') AND LastName = '" + LastName + "' AND DOB = '" + DOB + "' AND AgentID = " + Session["AgentID"].ToString();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        HasMatch = "Y";
                        while (sdr.Read())
                        {
                            HasMatch = HasMatch + "|" + sdr["CustomerID"].ToString();
                        }
                    }
                    else
                    {
                        HasMatch = "N|NOID";
                    }
                }
                conn.Close();
            }

            Response.Write(HasMatch);
        }
    }
}