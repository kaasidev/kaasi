﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getRemittedAmountFromTransasction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = Request.QueryString["TID"].ToString();
            classes.Transactions trans = new classes.Transactions();

            Response.Write(trans.getReceivedAmountFromTransaction(TID) + "|" + trans.getTransactionServiceCharge(TID) + "|" + trans.getTransactionTotalAUDAmount(TID) + "|" + trans.getRemittedCurrency(TID) + "|" + trans.getTransactionPurpose(TID) + "|" + trans.getTransactionSourceOfFunds(TID) + "|" + trans.getTransactionNotes(TID) + "|" + trans.getTransactionRate(TID) + "|" + trans.getTransactionCountryID(TID) + "|" + trans.getTransactionDateTime(TID) + "|" + trans.getTransactionDateTime(TID));
        }
    }
}