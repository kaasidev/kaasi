﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerKYC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;
            String CID = Request.QueryString["CID"].ToString();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM dbo.KYCInfo WHERE CustomerID = " + CID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                output = output + sdr["MaritalStatus"].ToString() + "|";
                                output = output + sdr["FinancialDependants"].ToString() + "|";
                                output = output + sdr["PermanentResident"].ToString() + "|";
                                output = output + sdr["ResidentialStatus"].ToString() + "|";
                                output = output + sdr["EmploymentStatus"].ToString() + "|";
                                output = output + sdr["Occupation"].ToString() + "|";
                                output = output + sdr["Salary"].ToString() + "|";
                                output = output + sdr["Frequency"].ToString() + "|";
                                output = output + sdr["OtherIncomes"].ToString() + "|";
                                output = output + sdr["Comments"].ToString() + "|";
                                output = output + sdr["CreatedBy"].ToString() + "|";
                                output = output + sdr["CreatedDateTime"].ToString() + "|";
                                output = output + sdr["AmendedBy"].ToString() + "|";
                                output = output + sdr["AmendedDateTime"].ToString();
                            }
                        }
                        else
                        {
                            output = "NOKYC";
                        }
                    }
                }
            }

            Response.Write(output);
        }
    }
}