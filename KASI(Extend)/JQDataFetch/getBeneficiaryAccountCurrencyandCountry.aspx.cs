﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getBeneficiaryAccountCurrencyandCountry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            String AID = Request.QueryString["AID"].ToString();
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CountryID, CurrencyID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CountryID"].ToString() + "|" + sdr["CurrencyID"].ToString();
                    }
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}