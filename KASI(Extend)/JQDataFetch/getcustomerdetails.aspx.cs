﻿using Kapruka.Enterprise;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getcustomerdetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Kapruka.Repository.Customer 
            try
            {
                int id = int.Parse(Request.QueryString["id"]);
                CustomerService ser = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                var cusList = ser.GetAll();
                var cust = (from cs in cusList where cs.CustomerID == id select cs).SingleOrDefault();
                string objt = JsonConvert.SerializeObject(cust);
                Response.Write(objt);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}