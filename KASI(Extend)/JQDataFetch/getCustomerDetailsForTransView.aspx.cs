﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerDetailsForTransView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String output = String.Empty;
            String CID = Request.QueryString["CID"].ToString();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT FullName, ISNULL(UnitNo,'') + ' ' + ISNULL(StreetNo, '') + ' ' + ISNULL(StreetName,'') + ' '
                                 + ISNULL(StreetType, '') + ', ' + ISNULL(AddressLine2, '') + ' ' + ISNULL(Suburb, '') + ' ' + ISNULL(State, '') + ' '
                                 + ISNULL(Postcode,'') + ' ' + ISNULL(Country, '') AS FullAddress, Mobile, EmailAddress, Occupation FROM dbo.Customers WHERE CustomerID =" + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        //          0                                   1                           2                               3                                       4
                        output = sdr["FullName"].ToString() + "|" + sdr["FullAddress"] + "|" + sdr["Mobile"].ToString() + "|" + sdr["EmailAddress"].ToString() + "|" + sdr["Occupation"].ToString();
                    }
                }
                conn.Close();
                
            }

            Response.Write(output);
        }
    }
}