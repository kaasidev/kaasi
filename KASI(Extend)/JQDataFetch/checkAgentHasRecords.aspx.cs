﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class checkAgentHasRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Agent AGT = new Agent();
            String AID = Request.QueryString["AID"].ToString();
            Boolean hasCustomer = AGT.checkAgentHasCustomers(AID);

            if (hasCustomer == true)
            {
                Response.Write("INACTIVE");
            }
            else
            {
                Response.Write("DELETE");
            }
        }
    }
}