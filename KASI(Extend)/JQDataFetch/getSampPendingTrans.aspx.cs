﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getSampPendingTrans : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT * FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = 7278";

            String returnString = "{\"data\":[";
            String bankName = "SAMPATH";
            String BankBranch = String.Empty;
            String AccountNumber = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            BankBranch = findBankBranch(sdr["AccountID"].ToString());
                            AccountNumber = findAccountNumber(sdr["AccountID"].ToString());
                            returnString = returnString + "[\"" + sdr["TransactionID"] + "\"," + "\"" + sdr["CreatedDateTime"] + "\"," + "\"" + sdr["CustomerID"] + "\"," + "\"" + sdr["CustomerLastName"] + "\"," + "\"" + sdr["BeneficiaryName"] + "\"," + "\"" + bankName + "\"," + "\"" + BankBranch + "\"," + "\"" + AccountNumber + "\"," + "\"" + sdr["RemittedAmount"] + "\"],";
                        }
                        int Length = returnString.Length;
                        returnString = returnString.Substring(0, (Length - 1));
                    }


                }

                returnString = returnString + "]}";
                conn.Close();
            }

            Response.Write(returnString);
        }

        protected String findBankBranch(String AccountID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT BranchName FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AccountID;
            String BankBranch = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankBranch = sdr["BranchName"].ToString();
                    }
                }
            }
            conn.Close();

            return BankBranch;

        }

        protected String findAccountNumber(String AccountID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String sqlQuery = "SELECT AccountNumber FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AccountID;
            String AccountNumber = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        AccountNumber = sdr["AccountNumber"].ToString();
                    }
                }
            }
            conn.Close();

            return AccountNumber;
        }
    }
}