﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerPrimaryDocumentForTransView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String output = String.Empty;
            String CID = Request.QueryString["CID"].ToString();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TypeOfDocument, DocumentNumber FROM dbo.CustomerDocuments WHERE CustomerID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = sdr["TypeOfDocument"].ToString() + "|" + sdr["DocumentNumber"].ToString();
                        }
                    }
                    else
                    {
                        output = "No Primary Documents";
                    }
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}