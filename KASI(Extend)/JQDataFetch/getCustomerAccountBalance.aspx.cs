﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerAccountBalance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Customer cust = new Customer();
            String CID = Request.QueryString["CID"].ToString();

            Response.Write(cust.getCustomerAccountBalance(CID));
        }
    }
}