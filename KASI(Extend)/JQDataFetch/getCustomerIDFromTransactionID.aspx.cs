﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerIDFromTransactionID : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Transactions trans = new classes.Transactions();

            String TID = Request.QueryString["TID"].ToString();

            Response.Write(trans.getCustomerIDFromTransactionID(TID));
        }
    }
}