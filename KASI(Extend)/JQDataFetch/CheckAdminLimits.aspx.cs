﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend_.JQDataFetch
{
    public partial class CheckAdminLimits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int tid = int.Parse(Request.QueryString["tid"].ToString());
            String hasPassedAdminCheck = "PASSED";
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            TransactionService service = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var item = service.GetAll(x => x.TransactionID == tid, null, "").SingleOrDefault();//is checking status PROCESSING or COMPLIANCE CHECK?
            if (item != null)
            {
                string loggedUserId = "0";
                KASI_Extend_.classes.Transactions tr = new KASI_Extend_.classes.Transactions();
                Customer cust = custSer.GetAll(x => x.CustomerID == item.CustomerID.Value, null, "").SingleOrDefault();


                bool isAnalyseMasterMonthlyDollarLimit = false;
                bool isAnalyseMasterYearlyDollarLimit = false;
                bool isAnalyseMasterMonthlyNbtransLimit = false;
                bool isAnalyseMasterYearlyNbtransLimit = false;
                

                bool isAnalyseMonthlyDollarLimit = AnalyseMonthlyDollarLimit(item.CustomerID.Value, service, out isAnalyseMasterMonthlyDollarLimit);
                bool isAnalysYearlyDollarLimit = AnalyseYearlyDollarLimit(service, out isAnalyseMasterYearlyDollarLimit, item.CustomerID.Value);
                bool isAnalyseMonthlyNbTransLimit = AnalyseMonthlyNbTransLimit(item.CustomerID.Value, service, out isAnalyseMasterMonthlyNbtransLimit);
                bool isAnalyseyearlyNbTransLimit = AnalyseYearlyNbTransLimit(service, out isAnalyseMasterYearlyNbtransLimit, item.CustomerID.Value);
                bool isAnalyseKYCPerformed = AnalyseKYCPerformed(item.CustomerID.Value, out isAnalyseKYCPerformed);


                if (isAnalyseMonthlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "MASTER REVIEW", "RNP Monthly AUD Limit Exceeded", service);
                    hasPassedAdminCheck = "FAILED";
                    tr.LogFailedTransactions(tid, "RNP Monthly AUD Limit Exceeded", loggedUserId);

                    Random rnd = new Random();
                    int GlobalID = rnd.Next(999999999); 

                    NotificationService Notserv = new NotificationService(new UnitOfWorks(new KaprukaEntities()));

                    Notification newNotif = new Notification();
                    newNotif.GlobalNotificiationID = GlobalID;
                    newNotif.Type = "TRANSACTION";
                    newNotif.RecordID = tid;
                    newNotif.Description = "Transaction " + tid + " has exceeded RNP Monthly AUD Limit.";
                    newNotif.CreatedDateTime = DateTime.Now;

                    Notserv.Add(newNotif);
                }

                if (isAnalysYearlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "MASTER REVIEW", "RNP Yearly AUD Limit Exceeded", service);
                    hasPassedAdminCheck = "FAILED";
                    tr.LogFailedTransactions(tid, "RNP Yearly AUD Limit Exceeded", loggedUserId);

                    Random rnd = new Random();
                    int GlobalID = rnd.Next(999999999);

                    NotificationService Notserv = new NotificationService(new UnitOfWorks(new KaprukaEntities()));

                    Notification newNotif = new Notification();
                    newNotif.GlobalNotificiationID = GlobalID;
                    newNotif.Type = "TRANSACTION";
                    newNotif.RecordID = tid;
                    newNotif.Description = "Transaction " + tid + " has exceeded RNP Yearly AUD Limit.";
                    newNotif.CreatedDateTime = DateTime.Now;

                    Notserv.Add(newNotif);
                }

                if (isAnalyseMonthlyNbTransLimit)
                {
                    ChangeTransactionStatus(item, "MASTER REVIEW", "RNP Monthly Number of Transaction Limit Exceeded", service);
                    hasPassedAdminCheck = "FAILED";
                    tr.LogFailedTransactions(tid, "RNP Monthly Number of Transaction Limit Exceeded", loggedUserId);

                    Random rnd = new Random();
                    int GlobalID = rnd.Next(999999999);

                    NotificationService Notserv = new NotificationService(new UnitOfWorks(new KaprukaEntities()));

                    Notification newNotif = new Notification();
                    newNotif.GlobalNotificiationID = GlobalID;
                    newNotif.Type = "TRANSACTION";
                    newNotif.RecordID = tid;
                    newNotif.Description = "Transaction " + tid + " has exceeded RNP Monthly Number of Transaction Limit.";
                    newNotif.CreatedDateTime = DateTime.Now;

                    Notserv.Add(newNotif);
                }

                if (isAnalyseKYCPerformed)
                {
                    ChangeTransactionStatus(item, "MASTER REVIEW", "KYC Not Performed/Failed", service);
                    hasPassedAdminCheck = "FAILED";
                    tr.LogFailedTransactions(tid, "Customer has either failed KYC or KYC has not been performed", loggedUserId);

                    Random rnd = new Random();
                    int GlobalID = rnd.Next(999999999);

                    NotificationService Notserv = new NotificationService(new UnitOfWorks(new KaprukaEntities()));

                    Notification newNotif = new Notification();
                    newNotif.GlobalNotificiationID = GlobalID;
                    newNotif.Type = "TRANSACTION";
                    newNotif.RecordID = tid;
                    newNotif.Description = "Transaction " + tid + ": Customer has either failed KYC or KYC has not been run";
                    newNotif.CreatedDateTime = DateTime.Now;

                    Notserv.Add(newNotif);
                }

                if (isAnalyseyearlyNbTransLimit)
                {
                    ChangeTransactionStatus(item, "MASTER REVIEW", "RNP Yearly Number of Transaction Limit Exceeded", service);
                    hasPassedAdminCheck = "FAILED";
                    tr.LogFailedTransactions(tid, "RNP Yearly Number of Transaction Limit Exceeded", loggedUserId);

                    Random rnd = new Random();
                    int GlobalID = rnd.Next(999999999);

                    NotificationService Notserv = new NotificationService(new UnitOfWorks(new KaprukaEntities()));

                    Notification newNotif = new Notification();
                    newNotif.GlobalNotificiationID = GlobalID;
                    newNotif.Type = "TRANSACTION";
                    newNotif.RecordID = tid;
                    newNotif.Description = "Transaction " + tid + " has exceeded RNP Yearly Number of Transaction Limit.";
                    newNotif.CreatedDateTime = DateTime.Now;

                    Notserv.Add(newNotif);
                }


            }


            Response.Write(hasPassedAdminCheck);
        }

        public bool AnalyseKYCPerformed(int custID, out bool ismasCheck)
        {
            ismasCheck = false;
            CustomerService CustServ = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            var CustDetails = CustServ.GetAll(x => x.CustomerID == custID, null, "").SingleOrDefault();

            if (CustDetails.KYCResult == DBNull.Value.ToString() || CustDetails.KYCResult == "-1")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool AnalyseMonthlyDollarLimit(int custId, TransactionService ser, out bool ismasCheck)
        {
            ismasCheck = false;
            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var MAudLimit = setSer.GetAll(x => x.SettingName == "MonthlyAUDLimit", null, "").SingleOrDefault().SettingsVariable;
            DateTime comparisonDate = DateTime.Now.Date.AddMonths(-1);
            //DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));
            var trList = ser.GetAll(x => x.CreatedDateTime >= comparisonDate && x.CustomerID == custId, null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);

            if (!String.IsNullOrEmpty(MAudLimit.ToString()))
            {
                if (float.Parse(MAudLimit.ToString()) < float.Parse(totAmount.ToString()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool AnalyseYearlyDollarLimit(TransactionService ser, out bool ismasCheck, int custid)
        {
            ismasCheck = false;
            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var YAudLimit = setSer.GetAll(x => x.SettingName == "YearlyAUDLimit", null, "").SingleOrDefault().SettingsVariable;
            //DateTime yearFirstDate = Convert.ToDateTime("01/01/" + DateTime.Now.ToString("yyyy"));
            DateTime comparisonDate = DateTime.Now.Date.AddYears(-1);
            var trList = ser.GetAll(x => x.CreatedDateTime >= comparisonDate && x.CustomerID == custid, null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);
            if (!String.IsNullOrEmpty(YAudLimit.ToString()))
            {
                if (float.Parse(YAudLimit.ToString()) < float.Parse(totAmount.ToString()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        public bool AnalyseMonthlyNbTransLimit(int custId, TransactionService ser, out bool ismasCheck)
        {
            ismasCheck = false;
            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var MNbLimit = setSer.GetAll(x => x.SettingName == "MonthlyTransLimit", null, "").SingleOrDefault().SettingsVariable;
            //DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));
            DateTime comparisonDate = DateTime.Now.Date.AddMonths(-1);
            var trList = ser.GetAll(x => x.CreatedDateTime >= comparisonDate && x.CustomerID == custId, null, "").ToList();
            int cnt = trList.Count();

            if (!String.IsNullOrEmpty(MNbLimit.ToString()))
            {
                if (Int32.Parse(cnt.ToString()) > Int32.Parse(MNbLimit))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        public bool AnalyseYearlyNbTransLimit(TransactionService ser, out bool ismasCheck, int custId)
        {
            ismasCheck = false;
            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            var YNbLimit = setSer.GetAll(x => x.SettingName == "YearlyTransLimit", null, "").SingleOrDefault().SettingsVariable;
            //DateTime monthFirstDate = Convert.ToDateTime("01/01/" + DateTime.Now.ToString("yyyy"));
            DateTime comparisonDate = DateTime.Now.Date.AddYears(-1);
            var trList = ser.GetAll(x => x.CreatedDateTime >= comparisonDate && x.CustomerID == custId, null, "").ToList();
            int cnt = trList.Count();
            if (!String.IsNullOrEmpty(YNbLimit.ToString()))
            {
                if (cnt > Int32.Parse(YNbLimit))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        public void ChangeTransactionStatus(Transaction trans, string status, string reason, TransactionService service)
        {
            String desctext = String.Empty;
            trans.Status = status;
            trans.ReviewByMasterCompany = true;
            if (!string.IsNullOrEmpty(reason))
            {
                trans.ComplianceReason = reason;
                desctext = "Transaction " + trans.TransactionID + " escalated to Master Compliance due to: " + reason;
            }
            else
            {
                desctext = "Transaction " + trans.TransactionID + " set to PENDING";
            }
            service.Update(trans);

            classes.Transactions TRANS = new classes.Transactions();
            TRANS.insertAuditRecord(trans.TransactionID.ToString(), Session["LoggedPersonName"].ToString(), Session["LoggedCompany"].ToString(), "EDIT", desctext);
        }

        
    }
}