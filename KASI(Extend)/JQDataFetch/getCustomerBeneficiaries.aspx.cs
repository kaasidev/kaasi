﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerBeneficiaries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String output = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BeneficiaryID, BeneficiaryName FROM dbo.Beneficiaries WHERE CustomerID = " + CID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                output = output + sdr["BeneficiaryName"].ToString() + "|";
                                output = output + sdr["BeneficiaryID"].ToString() + "~";
                            }
                            int Length = output.Length;
                            output = output.Substring(0, (Length - 1));
                        }
                        else
                        {
                            output = "NORESULTS";
                        }
                    }
                }
                conn.Close();
            }

            Response.Write(output);
        }
    }
}