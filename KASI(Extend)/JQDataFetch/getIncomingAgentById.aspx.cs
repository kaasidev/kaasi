﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend.JQDataFetch
{
    public partial class getIncomingAgentById : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
          //  obj.status = "1";
            var agetnId = Convert.ToInt32(Request.QueryString["agentId"].ToString());

             var cusTran = getAgentData(agetnId);
             var json = new JavaScriptSerializer().Serialize(cusTran);

            Response.Write(json);
        }


        private IncomingAgent getAgentData(int agentId)
        {
            var obj = new IncomingAgent();

            IncomingAgentService IAServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));

            obj = IAServ.GetAll().Where(x => x.AgentID == agentId).SingleOrDefault();

           

            return obj;
        }
    }
}