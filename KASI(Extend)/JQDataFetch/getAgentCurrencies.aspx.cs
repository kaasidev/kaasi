﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentCurrencies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String AID = Request.QueryString["AID"];

            if (String.IsNullOrEmpty(AID))
            {
                AID = Session["AgentID"].ToString();
            }

            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyCode, CurrencyName,CurrencyID FROM dbo.Currencies WHERE CurrencyID IN (SELECT CurrencyID FROM dbo.AgentCurrencies WHERE AgentID = " + AID + ")";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + sdr["CurrencyName"].ToString() + "|" + sdr["CurrencyCode"].ToString() + "|" + sdr["CurrencyID"].ToString() + "~";
                        }
                    }
                }
                conn.Close();
            }

            int Length = output.Length;
            output = output.Substring(0, (Length - 1));

            Response.Write(output);
        }
    }
}