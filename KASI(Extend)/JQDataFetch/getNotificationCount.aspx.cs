﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend.JQDataFetch
{
    public partial class getNotificationCount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Notifications notifs = new Notifications();
            String output = notifs.NbOfNotifications();

            Response.Write(output);
        }
    }
}