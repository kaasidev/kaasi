﻿using Kapruka.Enterprise;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class ChangeTrasactionStatus : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            int tid = int.Parse(Request.QueryString["tid"].ToString());
            string total = Request.QueryString["bal"].ToString();
            string remTotal = Request.QueryString["monthTotal"].ToString();
            string totalLastYear = Request.QueryString["totalLastYr"].ToString();
            string value = Request.QueryString["val"].ToString();
            string serviceCharge = Request.QueryString["ser"].ToString();
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            TransactionService service = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            SettingsService setSer = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            string masMonthlyAudLimit = setSer.GetAll(x => x.SettingName == "MonthlyAUDLimit", null, "").SingleOrDefault().SettingsVariable;
            string masYearlyAudLimit = setSer.GetAll(x => x.SettingName == "YearlyAUDLimit", null, "").SingleOrDefault().SettingsVariable;
            string masMonthlyTrLimit = setSer.GetAll(x => x.SettingName == "MonthlyTransLimit", null, "").SingleOrDefault().SettingsVariable;
            string masYearlyTrLimit = setSer.GetAll(x => x.SettingName == "YearlyTransLimit", null, "").SingleOrDefault().SettingsVariable;
            var item = service.GetAll(x => x.TransactionID == tid, null, "").SingleOrDefault();//is checking status PROCESSING or COMPLIANCE CHECK?
            if (item != null)
            {
                string loggedUserId = "0";
                KASI_Extend_.classes.Transactions tr = new KASI_Extend_.classes.Transactions();
                Customer cust = custSer.GetAll(x => x.CustomerID == item.CustomerID.Value, null, "").SingleOrDefault();
                Agent agent = agentSer.GetAll(y => y.AgentID == item.AgentID, null, "").SingleOrDefault();
                bool isAnalyseMasterMonthlyDollarLimit = false;
                bool isAnalyseMasterYearlyDollarLimit = false;
                bool isAnalyseMasterMonthlyNbtransLimit = false;
                bool isAnalyseMasterYearlyNbtransLimit = false;
                String isAnalyseKYC = String.Empty;

                bool isAnalyseMonthlyDollarLimit = AnalyseMonthlyDollarLimit(item.CustomerID.Value, service, agent.AgentID, masMonthlyAudLimit, out isAnalyseMasterMonthlyDollarLimit);
                bool isAnalyseMonthlyNbTransLimit = AnalyseMonthlyNbTransLimit(item.CustomerID.Value, service, agent.AgentID, masMonthlyTrLimit, out isAnalyseMasterMonthlyNbtransLimit);
                bool isAnalysYearlyDollarLimit = AnalyseYearlyDollarLimit(service, agent.AgentID, masYearlyAudLimit, out isAnalyseMasterYearlyDollarLimit, item.CustomerID.Value);
                bool isAnalyseyearlyNbTransLimit = AnalyseYearlyNbTransLimit(service, agent.AgentID, masYearlyTrLimit, out isAnalyseMasterYearlyNbtransLimit, item.CustomerID.Value);
                bool isAnalyseDocPresent = AnalyseCustDocumentPresent(item.CustomerID.Value);
                bool isAnalyseDocumentExpiry = AnalyseDocumentExpiry(item.CustomerID.Value, isAnalyseDocPresent);
                bool isAnalyseMonthlyVariation = AnalyseMonthlyVariation(item.CustomerID.Value, service);
                
                String isAnalyseKYCCheck = AnalyseKYCStatus(item.CustomerID.Value);


                if (cust.RiskLevel == 4)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Customer Risk level is 4", service);
                    tr.LogFailedTransactions(tid, "Customer Risk level is 4", loggedUserId);
                }
                if (isAnalyseMonthlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Monthly AUD Limit Exceeded", loggedUserId);
                }
                if (isAnalyseDocPresent)
                {
                    ChangeTransactionStatus(item, "REVIEW", "No Primary Document Found", service);
                    ChangeRiskLevel(cust, 4, custSer);
                    tr.LogFailedTransactions(tid, "No Primary Document Found", loggedUserId);
                }
                if (isAnalyseKYCCheck == "FAILED")
                {
                    ChangeTransactionStatus(item, "REVIEW", "KYC Check Failed", service);
                    ChangeRiskLevel(cust, 4, custSer);
                    tr.LogFailedTransactions(tid, "KYC Check Failed", loggedUserId);
                }
                if (isAnalyseKYCCheck == "NOTDONE")
                {
                    ChangeTransactionStatus(item, "REVIEW", "KYC Not Performed", service);
                    ChangeRiskLevel(cust, 4, custSer);
                    tr.LogFailedTransactions(tid, "KYC Not Performed", loggedUserId);
                }

                if (isAnalyseMonthlyNbTransLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Monthly Number of Transaction Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Monthly Number of Transaction Limit Exceeded", loggedUserId);
                }
                if (isAnalyseDocumentExpiry)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Primary Document(s) Expired", service);
                    ChangeRiskLevel(cust, 4, custSer);
                    tr.LogFailedTransactions(tid, "Primary Document(s) Expired", loggedUserId);
                }
                //   if (isAnalyseMonthlyVariation)
                //  {
                //        ChangeTransactionStatus(item, "REVIEW", "Monthly Variation Exceeded", service);
                //       ChangeRiskLevel(cust, 3, custSer);
                //       tr.LogFailedTransactions(tid, "Monthly Variation Exceeded", loggedUserId);
                //   }
                if (isAnalysYearlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Yearly AUD Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Yearly AUD Limit Exceeded", loggedUserId);
                }
                if (isAnalyseyearlyNbTransLimit)
                {
                    ChangeTransactionStatus(item, "REVIEW", "Yearly Number of Transaction Limit Exceeded", service);
                    ChangeRiskLevel(cust, 3, custSer);
                    tr.LogFailedTransactions(tid, "Yearly Number of Transaction Limit Exceeded", loggedUserId);
                }
                
                if (isAnalyseKYCCheck == "PASSED" && cust.RiskLevel != 4 && !isAnalyseyearlyNbTransLimit && !isAnalysYearlyDollarLimit && !isAnalyseDocumentExpiry && !isAnalyseMonthlyNbTransLimit && !isAnalyseMonthlyDollarLimit)
                {
                    ChangeTransactionStatus(item, "PENDING", "", service);
                }
                else
                {
                    classes.Notifications Notifs = new classes.Notifications();
                    if (isAnalyseKYCCheck == "FAILED")
                    {
                        Notifs.Type = "CUSTOMER";
                        Notifs.RecordID = Int32.Parse(cust.CustomerID.ToString());
                        Notifs.Description = "Customer " + cust.CustomerID.ToString() + " has failed KYC.";
                        //Notifs.AddNotification(Notifs);
                    }

                    if (isAnalyseKYCCheck != "NOTDONE")
                    {
                        classes.Notifications Notif2 = new classes.Notifications()
                        {
                            Type = "TRANSACTION",
                            RecordID = tid,
                            Description = "Transaction " + tid + " has exceeded RNP transaction limit(s)"
                        };
                        //Notif2.AddNotification(Notif2);
                    }
                    

                    
                }


            }

            float rem = float.Parse(total) - float.Parse(value);
            float monthtot = float.Parse(remTotal) + float.Parse(value) - float.Parse(serviceCharge);
            float yeartot = float.Parse(totalLastYear) + float.Parse(value) - float.Parse(serviceCharge);

            Response.Write(String.Format("{0:C2}", rem) + "|" + String.Format("{0:C2}", monthtot) + "|" + String.Format("{0:C2}", yeartot));
        }


        private void UpdateTransactionTable(Transaction item, TransactionService service)
        {
            item.ReviewByMasterCompany = true;
            service.Update(item);
        }

        public static void ChangeRiskLevel(Customer customer, int riskLevel, CustomerService service)
        {
            customer.RiskLevel = riskLevel;

            service.Update(customer);
        }

        public async Task<int> AnalyseTransaction(String TransactionID)
        {
            int x = await Task.FromResult(0);
            return x;

            // If all of the below method are not true, then put the transaction [dbo].[Transactions].[Status] into 'PENDING'
        }
        public void ChangeTransactionStatus(Transaction trans, string status, string reason, TransactionService service)
        {
            String desctext = String.Empty;
            trans.Status = status;
            if (!string.IsNullOrEmpty(reason))
            {
                trans.ComplianceReason = reason;
                desctext = "Transaction " + trans.TransactionID + " escalated to Compliance due to: " + reason;
            }
            else
            {
                desctext = "Transaction " + trans.TransactionID + " set to PENDING";
            }
            service.Update(trans);

            classes.Transactions TRANS = new classes.Transactions();
            TRANS.insertAuditRecord(trans.TransactionID.ToString(), "SYSTEM", "FLEXEWALLET", "EDIT", desctext);
        }

        public String AnalyseKYCStatus(int custid)
        {
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            var KYCStat = custSer.GetAll(x => x.CustomerID == custid, null, "").SingleOrDefault().resultDescription;

            if (!String.IsNullOrEmpty(KYCStat))
            {
                if (KYCStat.Contains("Success"))
                {
                    return "PASSED";
                }
                else if (KYCStat.Contains("Failed"))
                {
                    return "FAILED";
                }
                else
                {
                    return "NOTDONE";
                }
            }
            else
            {
                return "NOTDONE";
            }

        }

        public bool AnalyseMonthlyDollarLimit(int custId, TransactionService ser, int agentId, string masCheck, out bool ismasCheck)
        {
            ismasCheck = false;
            CustomerService custSer = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            var customer = custSer.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault();
            var agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));
            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID == custId && x.Status != "CANCELLED", null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);
            if (decimal.Parse(masCheck) < totAmount)
            {
                ismasCheck = true;

            }
            if (agent.MonthlyAUDLimit.HasValue)
            {
                if (agent.MonthlyAUDLimit.Value < totAmount)
                {

                    return true;

                }
                else
                {


                    return false;

                }
            }
            else
            {
                return false;
            }

            // CID --> CustomerID
            // TID --> TransactionID

            // Customer has a monthly limit set in [dbo].[Customers].[SpendingPower].
            // If combined Transaction AUD Total [dbo].[Transactions].[DollarAmount] for this month exceed above SpendingPower, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly AUD Limit Exceeded'
        }
        public bool AnalyseYearlyDollarLimit(TransactionService ser, int agentId, string masCheck, out bool ismasCheck, int custid)
        {
            ismasCheck = false;
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));

            var agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            DateTime yearFirstDate = Convert.ToDateTime("01/01/" + DateTime.Now.ToString("yyyy"));
            var trList = ser.GetAll(x => x.CreatedDateTime >= yearFirstDate && x.AgentID == agentId && x.CustomerID == custid && x.Status != "CANCELLED", null, "").ToList();
            decimal totAmount = trList.Sum(x => x.DollarAmount.Value);
            if (decimal.Parse(masCheck) < totAmount)
            {
                ismasCheck = true;
            }
            if (agent.YearlyAUDsLimit.HasValue)
            {
                if (agent.YearlyAUDsLimit.Value < totAmount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

            // CID --> CustomerID
            // TID --> TransactionID

            // Customer has a monthly limit set in [dbo].[Customers].[SpendingPower].
            // If combined Transaction AUD Total [dbo].[Transactions].[DollarAmount] for this month exceed above SpendingPower, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly AUD Limit Exceeded'
        }

        public bool AnalyseMonthlyNbTransLimit(int custId, TransactionService ser, int agentId, string masCheck, out bool ismasCheck)
        {
            AgentService agSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            ismasCheck = false;
            var agent = agSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            decimal monthlyLimit = 0;
            if (agent.MonthlyTransLimit.HasValue)
            {
                monthlyLimit = agent.MonthlyTransLimit.Value;
            }

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID == custId && x.Status != "CANCELLED", null, "").ToList();
            int cnt = trList.Count();
            if (cnt > int.Parse(masCheck))
            {
                ismasCheck = true;
            }
            if (monthlyLimit > 0)
            {
                if (cnt > monthlyLimit)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

            // CID --> CustomerID
            // TID --> TransactionID

            // Global setting in [dbo].[Settings].[SettingName] = MonthlyTransLimit gives the maximum amount of transactions per month before it gets flagged
            // If COUNT(*) Transactions for that customer for this month exceed above MonthlyTransLimit, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Number of Transaction Limit Exceeded'
        }

        public bool AnalyseYearlyNbTransLimit(TransactionService ser, int agentId, string masCheck, out bool ismasCheck, int custId)
        {
            AgentService agSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            ismasCheck = false;
            var agent = agSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            decimal yearlyLimit = 0;
            if (agent.YearlyTransLimit.HasValue)
            {
                yearlyLimit = agent.YearlyTransLimit.Value;
            }

            DateTime monthFirstDate = Convert.ToDateTime("01/01/" + DateTime.Now.ToString("yyyy"));

            var trList = ser.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID == custId && x.Status != "CANCELLED", null, "").ToList();
            int cnt = trList.Count();
            if (cnt > int.Parse(masCheck))
            {
                ismasCheck = true;
            }
            if (yearlyLimit > 0)
            {
                if (cnt > yearlyLimit)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

            // CID --> CustomerID
            // TID --> TransactionID

            // Global setting in [dbo].[Settings].[SettingName] = MonthlyTransLimit gives the maximum amount of transactions per month before it gets flagged
            // If COUNT(*) Transactions for that customer for this month exceed above MonthlyTransLimit, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Number of Transaction Limit Exceeded'
        }

        public bool AnalyseCustDocumentPresent(int custID)
        {
            CustomerDocumentService CustDocServ = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            var docList = CustDocServ.GetAll(x => x.CustomerID == custID, null, "").ToList();
            if (docList.Count <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AnalyseDocumentExpiry(int custId, bool NoPrimaryDocs)
        {
            if (!NoPrimaryDocs)
            {
                CustomerDocumentService ser = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
                var docList = ser.GetAll(x => x.CustomerID == custId, null, "").ToList();
                if (docList.Count > 0)
                {
                    DateTime today = DateTime.Now;
                    String latestDate = docList.Max(r => r.ExpiryDate);
                    DateTime docDate = Convert.ToDateTime(latestDate);

                    int result = DateTime.Compare(today, docDate);

                    if (result <= 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
            

            // CID --> CustomerID
            // TID --> TransactionID

            // Customer Primary Documents are in [dbo].[CustomerDocuments].[TypeOfDocument] = 'PRIMARY'
            // There can be multiple primary documents. If ALL primary documents have expired, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Primary Document(s) Expired'
        }

        public bool AnalyseMonthlyVariation(int custId, TransactionService trSer)
        {
            // Need to check if customer is older than MonthlyVariationMonths. If not then do not run this check
            // If the customer has not transacted for x months, then do not run this check

            SettingsService ser = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            CustomerService cust = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            TransactionService trans = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            int varMonths = int.Parse(ser.GetAll(x => x.SettingName == "MonthlyVariationMonths", null, "").SingleOrDefault().SettingsVariable);
            decimal varValue = Convert.ToDecimal(ser.GetAll(x => x.SettingName == "MonthlyVariation", null, "").SingleOrDefault().SettingsVariable);
            int varMonthsBeforeCheck = Int32.Parse(ser.GetAll(x => x.SettingName == "MonthlyVariationMonthCheck", null, "").SingleOrDefault().SettingsVariable);
            DateTime customerJoinDate = DateTime.Parse(cust.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault().CreatedDateTime.ToString());


            DateTime now = DateTime.Now;

            DateTime CJDPlusMonths = customerJoinDate.AddMonths(varMonthsBeforeCheck);

            DateTime monthFirstDate = Convert.ToDateTime("01/" + DateTime.Now.ToString("MM/yyyy"));

            var trthismonthList = trSer.GetAll(x => x.CreatedDateTime >= monthFirstDate && x.CustomerID == custId, null, "").ToList();
            decimal totAmount = trthismonthList.Sum(x => x.DollarAmount.Value);
            int thisMonth = DateTime.Now.Month;
            int thisYear = DateTime.Now.Year;

            if (CJDPlusMonths > now)
            {
                for (int i = 1; i <= varMonths; i++)
                {
                    thisMonth = thisMonth - i;
                    if (thisMonth == 0)
                    {
                        thisMonth = 12;
                        thisYear = thisYear - 1;
                    }
                    DateTime fromDate = Convert.ToDateTime("01/" + thisMonth.ToString("00") + "/" + thisYear);
                    DateTime toDate = fromDate.AddMonths(1).AddDays(-1);
                    var trList = trSer.GetAll(x => x.CustomerID == custId && x.CreatedDateTime < toDate && x.CreatedDateTime > fromDate, null, "").ToList();
                    decimal totTrVal = trList.Sum(x => x.DollarAmount.Value);
                    if (Math.Abs(totAmount - totTrVal) > 1000)
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {
                return false;
            }


            // CID --> CustomerID
            // TID --> TransactionID

            // Look at the total for the last x Months ([dbo].[Settings].[SettingName] = MonthlyVariationMonths) 
            //of transactions in Dollar Amount.
            // The allowed variation is in [dbo].[Settings].[SettingName] = 'MonthlyVariation'
            // If the variation between the total for each month is greater than the MonthlyVariation, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW' 
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Variation Exceeded'
        }
    }
}