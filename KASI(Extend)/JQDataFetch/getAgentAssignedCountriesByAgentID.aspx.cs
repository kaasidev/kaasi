﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentAssignedCountriesByAgentID : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            String AID = Request.QueryString["AID"];

            if (AID == null)
            {
                AID = Session["AgentID"].ToString();
            }

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SELECT CTR.CountryID, CTR.CountryName FROM dbo.AgentAssignedCountries AAC INNER JOIN dbo.Countries CTR ON AAC.CountryID = CTR.CountryID WHERE AAC.AgentID = " + AID;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + sdr["CountryName"].ToString() + "|" + sdr["CountryID"].ToString() + "~";
                        }
                    }
                    conn.Close();
                }
            }

            int Length = output.Length;
            if (Length != 0)
            {
                output = output.Substring(0, (Length - 1));
            }

            Response.Write(output);
        }
    }
}