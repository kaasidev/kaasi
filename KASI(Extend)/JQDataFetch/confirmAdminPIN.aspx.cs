﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend.JQDataFetch
{
    public partial class confirmAdminPIN : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int LID = Int32.Parse(Session["LoggedUserID"].ToString());
            String PIN = Request.QueryString["PIN"].ToString();

            LoginService LGServ = new LoginService(new UnitOfWorks(new KaprukaEntities()));
            var LGDetails = LGServ.GetAll(x => x.LoginID == LID, null, "").SingleOrDefault();

            if (PIN == LGDetails.AdminOverrideCode.ToString())
            {
                Response.Write("true");
            }
            else
            {
                Response.Write("false");
            }
        }
    }
}