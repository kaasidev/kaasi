﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentCurrenciesForTransferFeeEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            String output = String.Empty;
            classes.Currencies curr = new Currencies();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.AgentTransactionFeeTiers WHERE CountryID = " + CID + "AND AgentID = " + Session["AgentID"].ToString();
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        String CName = curr.getCurrencyNameFromID(sdr["CurrencyID"].ToString());
                        String CCode = curr.getCurrencyCodeFromID(sdr["CurrencyID"].ToString());

                        output = output +   CCode + " - " + CName +  "|" + sdr["CurrencyID"].ToString() + "~";
                    }
                }
                conn.Close();
            }
            int Length = output.Length;
            output = output.Substring(0, (Length - 1));
            Response.Write(output);
        }
    }
}