﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class updateCurrencyRate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string agentId = Request.QueryString["agentId"].ToString();
                string valueList = Request.QueryString["curr"].ToString();
                string[] arr = valueList.Split(',');
                foreach (var item in arr)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] subArr = item.Split('^');
                        string value = subArr[0];
                        string currencyId = subArr[1];



                        SqlConnection conn = new SqlConnection();
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                        string StmtSql = "update AgentCurrencyMargins set Margin=" + value + " where AgentID=" + agentId + " and CurrencyID=" + currencyId;
                        using (SqlCommand cmd = new SqlCommand(StmtSql))
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }

                        String StmtSql2 = "INSERT INTO dbo.AgentSetRates (AgentID, CurrencyID, SetRate, CreatedDateTime, CreatedBy) VALUES ('" + agentId + "', '" + currencyId + "', '" + value + "', CURRENT_TIMESTAMP, '" + Session["LoggedUserFullName"].ToString() + "')";
                        using (SqlCommand cmd = new SqlCommand(StmtSql2))
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }

                    }
                }

                Response.Write("Y");
            }
            catch (Exception ex)
            {
                Response.Write("N^"+ex.ToString());
            }
        }
    }
}