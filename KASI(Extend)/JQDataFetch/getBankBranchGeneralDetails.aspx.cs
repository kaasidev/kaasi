﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getBankBranchGeneralDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Banking BK = new classes.Banking();
            String BankCode = Request.QueryString["BankID"].ToString();
            String BranchCode = Request.QueryString["BranchID"].ToString();

            String BranchAddress = BK.getBranchAddressByID(BankCode, BranchCode);

            Response.Write(BranchAddress);
        }
    }
}