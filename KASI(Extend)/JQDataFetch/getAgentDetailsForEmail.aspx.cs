﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
namespace KASI_Extend_.JQDataFetch
{
    public partial class getAgentDetailsForEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;
            String CID = Request.QueryString["CID"].ToString();

            classes.Agent AGT = new classes.Agent();
            classes.Customer CUS = new classes.Customer();

            output += AGT.getAgentCompanyName(Session["AgentID"].ToString()) + "|" + AGT.getAgentFullAddress(Session["AgentID"].ToString()) + "|" + AGT.getAgentTelephoneNumber(Session["AgentID"].ToString()) + "|" + CUS.getCustomerEmailAddress1(CID);

            Response.Write(output);
        }
    }
}