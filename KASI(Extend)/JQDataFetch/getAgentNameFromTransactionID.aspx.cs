﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend.JQDataFetch
{
    public partial class getAgentNameFromTransactionID : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = Request.QueryString["TID"].ToString();
            KASI_Extend_.classes.Agent AGT = new KASI_Extend_.classes.Agent();
            Response.Write(AGT.getAgentNameFromTransactionID(TID));
        }
    }
}