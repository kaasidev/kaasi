﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getMasterTransLimits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Settings STS = new classes.Settings();

            Response.Write(STS.getSystemMonthlyAUDLimit().ToString().Replace("$","").Replace(",","") + "|" + STS.getSystemYearlyAUDLimit().ToString().Replace("$", "").Replace(",", "") + "|" + STS.getSystemMonthlyTransLimit().ToString().Replace("$", "").Replace(",", "") + "|" + STS.getSystemYearlyTransLimit().ToString().Replace("$", "").Replace(",", ""));
        }
    }
}