﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCustomerDebitsCredits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String CID = Request.QueryString["CID"].ToString();
            Response.Write(returnString(CID));
        }

        public String returnString(String CID)
        {
            String FullReturnString = "{\"data\":[";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCreditsDebits";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;

            if (Session["TypeOfLogin"].ToString() == "Agent")
            {
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 10).Value = Session["AgentID"].ToString();
            }
            else
            {
                cmd.Parameters.Add("@AgentID", SqlDbType.Int, 10).Value = 0;
            }
            
            cmd.Connection = conn;

            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        if (sdr["Type"].Equals("CREDIT"))
                        {
                            FullReturnString = FullReturnString + "[\"" + sdr["CreditID"].ToString() + "\"," + "\"" + sdr["CreatedDateTime"].ToString().Substring(0,10) + "\"," + "\"" + sdr["DepositMethod"].ToString() + "\"," + "\"" + "-" + "\"," + "\"" + String.Format("{0:C2}", float.Parse(sdr["CreditAmount"].ToString())) + "\"],";
                        }
                        else
                        {
                            FullReturnString = FullReturnString + "[\"" + sdr["CreditID"].ToString() + "\"," + "\"" + sdr["CreatedDateTime"].ToString().Substring(0, 10) + "\"," + "\"" + sdr["DepositMethod"].ToString() + "\"," + "\"" + String.Format("{0:C2}", float.Parse(sdr["CreditAmount"].ToString())) + "\"," + "\"" + "-" + "\"],";
                        }
                            

                    }
                    int Length = FullReturnString.Length;
                    FullReturnString = FullReturnString.Substring(0, (Length - 1));
                }

                FullReturnString = FullReturnString + "]}";

            }
            conn.Close();
            return FullReturnString;
        }
    }
}