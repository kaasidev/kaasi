﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;


namespace KASI_Extend_.JQDataFetch
{
    public partial class gettodayrate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(String.Format("{0:C2}", loadTodayRate()));
        }

        private String loadTodayRate()
        {
            String output = String.Empty;
            float OutputRate = 0;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT TOP 1 Rate FROM dbo.Rates ORDER BY RateDate DESC";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["Rate"].ToString();
                    }

                }

            }
            OutputRate = float.Parse(output);
            return String.Format("{0:N2}", OutputRate);
        }
    }
}