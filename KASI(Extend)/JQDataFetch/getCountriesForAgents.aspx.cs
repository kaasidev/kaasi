﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getCountriesForAgents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT CountryID, CountryName FROM dbo.Countries";
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                output = output + sdr["CountryName"].ToString() + "|" + sdr["CountryID"].ToString() + "~";
                            }
                        }
                        conn.Close();
                    }
                }

                int Length = output.Length;
                if (Length != 0)
                {
                    output = output.Substring(0, (Length - 1));
                }

                Response.Write(output);
            }
            catch(Exception ex)
            {
                conn.Close();
            }
            finally
            {
                conn.Dispose();
            }
            
        }
    }
}