﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getTransactionMajorDetailsForConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String TID = String.Empty;
            TID = Request.QueryString["TID"].ToString();

            classes.Transactions Trans = new classes.Transactions();
            String CID = Trans.getCustomerIDFromTransactionID(TID);
            String CustName = Trans.getCustomerFullNameFromTransaction(TID);
            String AmtSend = Trans.getTransactionSendingAmount(TID).ToString();
            String BeneName = Trans.getBeneficiaryNameFromBIDfromTID(TID);
            String Rate = Trans.getTransactionRate(TID).ToString();
            String AmtRece = Trans.getReceivedAmountFromTransaction(TID);
            String BankName = Trans.getBankNameFromTransaction(TID);
            String BranchName = Trans.getBankBranchFromTransaction(TID);
            String TransType = Trans.getTransactionType(TID);

            Response.Write(TID + "|" + CID + "|" + CustName + "|" + AmtSend + "|" + BeneName + "|" + Rate + "|" + AmtRece + "|" + BankName + "|" + BranchName + "|" + TransType);
        }
    }
}