﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getAllTransactions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(returnString());
        }

        public String returnString()
        {
            String FullReturnString = "{\"data\":[";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT TR.TransactionID, CT.LastName, CT.Suburb, CT.DOB, CT.Mobile, TR.DollarAmount, TR.CreatedDateTime, TR.BeneficiaryName, TR.Rate, TR.RemittedAmount, TR.Status FROM dbo.Transactions TR INNER JOIN dbo.Customers CT ON TR.CustomerID = CT.CustomerID ORDER BY TR.TransactionID DESC";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        FullReturnString = FullReturnString + "[\"" + sdr["TransactionID"].ToString() + "\"," + "\"" + sdr["CreatedDateTime"].ToString() + "\"," + "\"" + sdr["LastName"].ToString() + "\"," + "\"" + sdr["DOB"].ToString() + "\"," + "\"" + sdr["Mobile"].ToString() + "\"," + "\"" + sdr["Suburb"].ToString() + "\"," + "\"" + sdr["BeneficiaryName"].ToString() + "\"," + "\"" + sdr["DollarAmount"].ToString() + "\"," + "\"" + sdr["Rate"].ToString() + "\"," + "\"" + sdr["RemittedAmount"].ToString() + "\"," + "\"" + sdr["Status"].ToString() + "\"]," ;
                    }

                }
                int Length = FullReturnString.Length;
                FullReturnString = FullReturnString.Substring(0, (Length - 1));

            }
            FullReturnString = FullReturnString + "]}";
            conn.Close();
            return FullReturnString;
        }
    }
}