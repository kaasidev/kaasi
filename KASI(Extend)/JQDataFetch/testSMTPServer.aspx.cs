﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace KASI_Extend_.JQDataFetch
{
    public partial class testSMTPServer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String SMTPServer = Request.QueryString["SMTPServer"].ToString();
            String SMTPPort = Request.QueryString["SMTPPort"].ToString();
            String Username = Request.QueryString["UName"].ToString();
            String PWord = Request.QueryString["PWord"].ToString();
            String SSLRequired = Request.QueryString["SSL"].ToString();
            String SendEmail = Request.QueryString["SendEmail"].ToString();
            String ReceivingEmail = "shehanx@hotmail.com";

            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredentials = new NetworkCredential(Username, PWord);
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress(SendEmail, "KAASI");

            smtpClient.Host = SMTPServer;
            smtpClient.Port = Int32.Parse(SMTPPort);
            smtpClient.Timeout = 50000;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = basicCredentials;

            if (SSLRequired == "1")
            {
                smtpClient.EnableSsl = true;
            }
            else
            {
                smtpClient.EnableSsl = false;
            }

            message.From = fromAddress;
            message.Subject = "SMTP Server Test Email";
            message.IsBodyHtml = true;
            message.Body = "<h1>TEST SUCCESSFULL</h1>";
            message.To.Add(ReceivingEmail);

            try
            {
                smtpClient.Send(message);
                Response.Write("SMTP Server Settings Successful");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }

           
        }

            
    }
}