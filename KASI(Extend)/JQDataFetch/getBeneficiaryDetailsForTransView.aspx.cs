﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Kapruka.Enterprise;

namespace KASI_Extend_.JQDataFetch
{
    public partial class getBeneficiaryDetailsForTransView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String output = String.Empty;

            classes.Beneficiary Bene = new classes.Beneficiary();
            classes.BeneficiaryAccounts BeneAcc = new classes.BeneficiaryAccounts();
            classes.Transactions Trans = new classes.Transactions();

            String BID = Request.QueryString["BID"].ToString();
            String TID = Request.QueryString["TID"].ToString();

            String AccNum = Trans.getBeneficiaryAccountIDFromTransaction(TID);

            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var BPMDetails = BPMServ.GetAll(x => x.AccountID == AccNum, null, "").SingleOrDefault();

            //          1                                       2                                       3                                   4                                                   5                                                                       6                                   7                                           8                                           9                                                       10                                          11                                                                      12                                       
            output = Bene.getBeneficiaryName(BID) + "|" + Bene.getBeneficiaryFullAddress(BID) + "|" + Bene.getBeneficiaryMobile(BID) + "|" + BeneAcc.getBeneficiaryBankName(AccNum) + "|" + BeneAcc.getBeneficicaryAccountNumberBasedOnAccountID(AccNum) + "|" + Trans.getTransactionType(TID) + "|" + BeneAcc.getBeneficiaryBankBranch(BID) + "|" + BeneAcc.getBeneficiaryAccountName(BID) + "|" + BeneAcc.getBeneficiaryAccountCountryName(TID) + "|" + Bene.getBeneficiaryRelationship(BID) + "|" + BeneAcc.getBeneficiaryAccountBankNameBasedOnAccountID(AccNum) + "|" + BPMDetails.BranchName;

            Response.Write(output);
        }
    }
}