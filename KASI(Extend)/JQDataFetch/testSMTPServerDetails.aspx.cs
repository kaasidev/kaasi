﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Sockets;
using System.IO;

namespace KASI_Extend_.JQDataFetch
{
    public partial class testSMTPServerDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool valid = false;
            bool overalltest = false;
            String hostname = Request.QueryString["SMTPServer"].ToString();
            int port = int.Parse(Request.QueryString["Port"].ToString());

            try
            {
                TcpClient smtpTest = new TcpClient();
                smtpTest.Connect(hostname, port);
                if (smtpTest.Connected)
                {
                    NetworkStream ns = smtpTest.GetStream();
                    StreamReader sr = new StreamReader(ns);
                    if (sr.ReadLine().Contains("220"))
                    {
                        valid = true;
                    }
                    smtpTest.Close;
                }
            }

            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}