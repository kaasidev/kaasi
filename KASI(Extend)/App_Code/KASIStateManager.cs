﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.App_Code
{
    public class KASIStateManager
    {
        public static int LoggedInUserID
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return 0;
                return HttpContext.Current.Session["userid"] == null ? 0 : Convert.ToInt32(HttpContext.Current.Session["userid"]);
            }
            set { HttpContext.Current.Session["userid"] = value; }
        }

        public static string LoggedInUserName
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return "";
                return HttpContext.Current.Session["username"] == null ? "" : HttpContext.Current.Session["username"].ToString();
            }
            set { HttpContext.Current.Session["username"] = value; }
        }
    }
}