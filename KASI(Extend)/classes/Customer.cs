﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using Kapruka.Domain;

namespace KASI_Extend_.classes
{
    public class Customer
    {
        public int CustomerID { get; set; } 
        public String AgentName { get; set; }
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String CountryOfBirth { get; set; }
        public String Nationality { get; set; }
        public String MinorAddress { get; set; }
        public String Mobile { get; set; }
        public String EmailAddress { get; set; }
        public String openCust { get; set; }

        // ----------------------------------------------------- QUERIES -------------------------------------------------------

        // Returns customer's Full Name
        public string getCustomerFullName(String CID)
        {
            string CustomerFullName = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerFullName";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerFullName = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerFullName;

        }

        public String getCustomerNotes(String CID)
        {
            string CustomerNotes = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerNotes";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerNotes = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerNotes;

        }

        public String getCustomerPassword(String CID)
        {
            string CustomerPassword = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerPassword";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerPassword = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerPassword;
        }

        // Return customer's Occupation
        public String getCustomerOccupation(String CID)
        {
            string CustomerOccupation = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerOccupation";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerOccupation = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerOccupation;
        }

        // Return customer's First Name
        public String getCustomerFirstName(String CID)
        {
            string CustomerFirstName = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerFirstName";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerFirstName = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerFirstName;
        }

        // Return the Customer's Risk Level
        public String getCustomerRiskLevel(String CID)
        {
            string risklevel = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerRiskLevel";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    risklevel = idr[0].ToString();
                }
            }
            conn.Close();
            return risklevel;

        }

        // Return's the type of customer as individual or business
        public String getCustomerTypeNamed(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT IsIndividual FROM dbo.Customers WHERE CustomerID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["IsIndividual"].ToString();
                    }
                }
                conn.Close();
            }

            if (output.Equals("True"))
            {
                output = "INDIVIDUAL";
            }
            else
            {
                output = "BUSINESS";
            }

            return output;
        }

        public String getCustomerType(String CID)
        {
            string isIndi = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select IsIndividual from Customers where CustomerID=" + CID;
           
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    isIndi = idr[0].ToString();
                }
            }
            conn.Close();
            return isIndi;
        }
        // Return Customer's Last Name
        public String getCustomerLastName(String CID)
        {
            string CustomerLastName = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerLastName";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerLastName = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerLastName;
        }

        // Return customer's Picture File Path
        public string getCustomerPicPath(String CID)
        {
            string CustomerPicPath = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerPicPath";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    if (idr[0].Equals(DBNull.Value))
                    {
                        CustomerPicPath = "NA";
                    }
                    else
                    {
                        CustomerPicPath = idr[0].ToString();
                    }

                }
            }
            conn.Close();
            return CustomerPicPath;
        }

        // Return customer's Gender
        public string getCustomerGender(String CID)
        {
            string CustomerGender = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerGender";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    if (idr[0].Equals(DBNull.Value))
                    {
                        CustomerGender = "Unknown";
                    }
                    else if (idr[0].ToString().Equals("Male"))
                    {
                        CustomerGender = "Male";
                    }
                    else if (idr[0].ToString().Equals("Female"))
                    {
                        CustomerGender = "Female";
                    }
                    else
                    {
                        CustomerGender = "Unknown";
                    }
                }
            }
            conn.Close();
            return CustomerGender;
        }

        // Return customer's Date of Birth
        public string getCustomerDOB(String CID)
        {
            string CustomerDOB = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerDOB";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerDOB = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerDOB;
        }

        // Return customer's Email Address 1
        public string getCustomerEmailAddress1(String CID)
        {
            string CustomerEmail1 = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerEmail1";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerEmail1 = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerEmail1;
        }

        // Return customer's Email Address 2
        public string getCustomerEmailAddress2(String CID)
        {
            string CustomerEmail2 = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerEmail2";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerEmail2 = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerEmail2;
        }

        // Return customer's Mobile Number
        public string getCustomerMobile(String CID)
        {
            string CustomerMobile = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerMobile";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerMobile = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerMobile;
        }

        // Return customer's Home Number
        public string getCustomerHomeNumber(String CID)
        {
            string CustomerHomeNumber = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerHomeNumber";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerHomeNumber = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerHomeNumber;
        }

        // Return customer's Work Number
        public string getCustomerWorkNumber(String CID)
        {
            string CustomerWorkNumber = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerWorkNumber";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerWorkNumber = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerWorkNumber;
        }

        // Return customer's Account Balance
        public string getCustomerAccountBalance(String CID)
        {
            string CustomerAccountBalance = String.Empty;
            float CAB = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerAccountBalance";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerAccountBalance = idr[0].ToString();
                }
            }
            if (!string.IsNullOrEmpty(CustomerAccountBalance))
            {
                CAB = float.Parse(CustomerAccountBalance);
                CAB = (float)Math.Round(CAB, 2);
            }

            CustomerAccountBalance = CAB.ToString();
            conn.Close();
            return CustomerAccountBalance;
        }

        // Return customer's Address Line 1 
        public String getCustomerAddressLine1(String CID)
        {
            string CustomerAddressLine1 = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerAddressLine1";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerAddressLine1 = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerAddressLine1;

        }

        // Return Customer's Address Line 1 
        public String getCustomerAddressLine2(String CID)
        {
            string CustomerAddressLine2 = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerAddressLine2";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerAddressLine2 = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerAddressLine2;

        }

        // Return Customer's Suburb
        public String getCustomerSuburb(String CID)
        {
            string CustomerSuburb = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerSuburb";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerSuburb = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerSuburb;
        }

        // Return customer's State
        public String getCustomerState(String CID)
        {
            string CustomerState = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerState";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerState = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerState;
        }

        // Return customer's Postcode
        public String getCustomerPostcode(String CID)
        {
            string CustomerPostcode = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerPostcode";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    CustomerPostcode = idr[0].ToString();
                }
            }
            conn.Close();
            return CustomerPostcode;
        }

        // Return customer's Address in One Line
        public string getCustomerFullAddress(String CID)
        {
            string CustomerFullAddress = String.Empty;
            string UnitNo = String.Empty;
            string StreetNo = String.Empty;
            string StreetName = String.Empty;
            string StreetType = String.Empty;
            string cSuburb = String.Empty;
            string cState = String.Empty;
            string cPostcode = String.Empty;
            string Country = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerFullAddress";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    UnitNo = idr[0].ToString();
                    StreetNo = idr[1].ToString();
                    StreetName = idr[2].ToString();
                    StreetType = idr[3].ToString();
                    cSuburb = idr[4].ToString();
                    cState = idr[5].ToString();
                    cPostcode = idr[6].ToString();
                    Country = idr[7].ToString();
                }
            }

            if (!UnitNo.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + UnitNo;
            }

            if (!StreetNo.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + ' ' + StreetNo;
            }

            if (!StreetName.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + ' ' + StreetName;
            }

            if (!StreetType.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + ' ' + StreetType;
            }

            if (!cSuburb.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + ' ' + cSuburb;
            }

            if (!cState.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + ' ' + cState;
            }

            if (!cPostcode.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + ' ' + cPostcode;
            }

            if (!Country.Equals(DBNull.Value))
            {
                CustomerFullAddress = CustomerFullAddress + ' ' + Country;
            }

            conn.Close();
            return CustomerFullAddress;

        }

        // Return customer's Monthly Remittance in AUD
        public String getMonthlyRemittanceTotal(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(DollarAmount) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(MONTH, -1, getDate())";
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0.00";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        // Return's customer's Monthly Remittance in AUD based on Agent
        public String getMonthlyAgentRemittanceTotal(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(DollarAmount) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(Month, -1, getDate()) AND TransactionAgentOwnership = " + HttpContext.Current.Session["LoggedID"];
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0.00";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        // Return customer's Yearly Remittrance in AUD
        public String getYearlyRemittancetotal(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(DollarAmount) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(YEAR, -1, getDate())";
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0.00";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        // Return's Customer's Agent Yearly Remittance in AUD
        public String getAgentYearlyRemittanceTotal(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(DollarAmount) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(Year, -1, getDate()) AND TransactionAgentOwnership = " + HttpContext.Current.Session["LoggedID"];
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0.00";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        //Return customer's Monthly Number of Transactions
        public String getSumMonthlyRemit(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(TransactionID) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(MONTH, -1, getDate())";
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        // Return's Customer's Agent Monthly Number of Transactions
        public String getAgentSumMonthlyRemit(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(TransactionID) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(Month, -1, getDate()) AND TransactionAgentOwnership = " + HttpContext.Current.Session["LoggedID"];
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        //Return customer's Yearly Number of Transactions
        public String getSumYearlyRemit(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(TransactionID) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(YEAR, -1, getDate())";
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        // Return's Customer's Agnet Yearly Number of Transactions
        public String getAgentSumYearlyRemit(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(TransactionID) AS TOTALSUM FROM dbo.Transactions WHERE Status <> 'CANCELLED' AND CustomerID = " + CID + " AND CreatedDateTime >= DATEADD(Year, -1, getDate()) AND TransactionAgentOwnership = " + HttpContext.Current.Session["LoggedID"];
            String TotalAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TotalAmount = sdr["TOTALSUM"].ToString();
                        }
                    }
                }
                if (TotalAmount == "")
                {
                    TotalAmount = "0";
                }
            }
            conn.Close();
            return TotalAmount;
        }

        //Return customer's Company Verified Status
        public String getCompanyVerifiedStatus(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT CompanyVerified FROM dbo.Customers WHERE CustomerID = " + CID;
            String CompanyVerified = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            CompanyVerified = sdr["CompanyVerified"].ToString();
                        }
                    }

                    if (CompanyVerified == "")
                    {
                        CompanyVerified = "false";
                    }
                }

            }
            conn.Close();
            return CompanyVerified;
        }

        //Return If Customer has KYC
        public String getCustomerHasKYC(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT * FROM dbo.KYCInfo WHERE CustomerID = " + CID;
            String HasKYC = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        HasKYC = "true";
                    }
                    else
                    {
                        HasKYC = "false";
                    }
                }

            }
            conn.Close();
            return HasKYC;
        }

        //Return Last Customer ID Available
        public String getLastCustomerID()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT MAX(CustomerID) AS CID FROM dbo.Customers";
            String LastCustomerID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        LastCustomerID = sdr["CID"].ToString();
                    }
                }
                conn.Close();
            }

            return LastCustomerID;

        }

        //Return who updated the KYC Last
        public String KYCLastUpdatePerson(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AmendedBy FROM dbo.KYCInfo WHERE CustomerID = " + CID;
            String KYCUpdatePerson = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        KYCUpdatePerson = sdr["AmendedBy"].ToString();
                    }
                }
                conn.Close();
            }

            return KYCUpdatePerson;
        }

        //Return last update date of the KYC
        public String KYCLAstUpdateTime(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT CONVERT(date, AmendedDateTime) AS theDate FROM dbo.KYCInfo WHERE CustomerID = " + CID;
            String KYCLastUpdateDate = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        KYCLastUpdateDate = sdr["theDate"].ToString();
                    }
                }
                conn.Close();
            }

            return KYCLastUpdateDate;
        }

        //Return KYC Expiry Date
        public String KYCExpiryDate(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT CAST(ExpiryDate AS DATE) AS theDate FROM dbo.KYCInfo WHERE CustomerID = " + CID;
            String KYCExpiryDate = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        KYCExpiryDate = sdr["theDate"].ToString();
                    }
                }
                conn.Close();
            }

            return KYCExpiryDate;
        }

        //Return the date difference between today and KYC Expiry Date
        public String KYCExpiryDays(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT DATEDIFF(day, GETDATE(), ExpiryDate) AS theDate FROM dbo.KYCInfo WHERE CustomerID = " + CID;
            String KYCExpiryDate = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        KYCExpiryDate = sdr["theDate"].ToString();
                    }
                }
                conn.Close();
            }

            return KYCExpiryDate;
        }

        //Return the Customer's Risk Reason
        public String getRiskReason(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT RiskReason FROM dbo.Customers WHERE CustomerID = " + CID;
            String RiskReason = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        RiskReason = sdr["RiskReason"].ToString();
                    }
                }
                conn.Close();
            }

            return RiskReason;
        }

        //Return the Customer's ID Points Total
        public String getCustomerIDPointsTotal(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(PointsAllocated) AS TotalPoints FROM dbo.CustomerDocuments WHERE CustomerID = " + CID;
            String IDPointsTotal = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        IDPointsTotal = sdr["TotalPoints"].ToString();
                    }
                }
                conn.Close();
            }

            return IDPointsTotal;
        }

        //Returns if the Customer has previously uploaded similar document
        public String checkIfDocumnetHasBeenUploaded(String CID, String DID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT * FROM dbo.CustomerDocuments WHERE CustomerID = " + CID + " AND TypeOfDocumentID = " + DID;
            String HasDoc = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        HasDoc = "Y";
                    }
                    else
                    {
                        HasDoc = "N";
                    }
                }
                conn.Close();
            }

            return HasDoc;
        }

        // Return's the Customer's ID from the BeneficiaryID
        public String getCustomerIDFromBeneficiaryID(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT CustomerID FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
            String CustomerID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        CustomerID = sdr["CustomerID"].ToString();
                    }
                }
                conn.Close();
            }

            return CustomerID;
        }

        // Return's the Owner Agent ID from the CustomerID
        public String getOwnerAgentIDFromCustomerID(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentID FROM dbo.Customers WHERE CustomerID = " + CID;
            String AgentID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentID = sdr["AgentID"].ToString();
                    }
                }
                conn.Close();
            }

            return AgentID;
        }

        // Returns' the Customer's Place of Birth
        public String getCustomerPlaceOfBirth(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT PlaceOfBirth FROM dbo.Customers WHERE CustomerID = " + CID;
            String PlaceOFBirth = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        PlaceOFBirth = sdr["PlaceOfBirth"].ToString();
                    }
                }
                conn.Close();
            }

            return PlaceOFBirth;
        }

        // Return's the Customer AKA
        public String getCustomerAKA(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AKA FROM dbo.Customers WHERE CustomerID = " + CID;
            String AKA = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AKA = sdr["AKA"].ToString();
                    }
                }
                conn.Close();
            }

            return AKA;
        }

        // Return's the Customer UnitNo
        public String getCustomerUnitNo(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT UnitNo FROM dbo.Customers WHERE CustomerID = " + CID;
            String UnitNo = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        UnitNo = sdr["UnitNo"].ToString();
                    }
                }
                conn.Close();
            }

            return UnitNo;
        }

        // Return's the Customer's Street No
        public String getCustomerStreetNo(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT StreetNo FROM dbo.Customers WHERE CustomerID = " + CID;
            String StreetNo = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        StreetNo = sdr["StreetNo"].ToString();
                    }
                }
                conn.Close();
            }

            return StreetNo;
        }

        // Return's the Customer's Street Name
        public String getCustomerStreetName(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT StreetName FROM dbo.Customers WHERE CustomerID = " + CID;
            String StreetName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        StreetName = sdr["StreetName"].ToString();
                    }
                }
                conn.Close();
            }

            return StreetName;
        }

        // Return's the Customer's Street Type
        public String getCustomerStreetType(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT StreetType FROM dbo.Customers WHERE CustomerID = " + CID;
            String StreetType = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        StreetType = sdr["StreetType"].ToString();
                    }
                }
                conn.Close();
            }

            return StreetType;
        }

        // Return's the Customer Country Of Birth
        public String getCustomerCountryOfBirth(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT CountryOfBirth FROM dbo.Customers WHERE CustomerID = " + CID;
            String CountryOfBirth = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        CountryOfBirth = sdr["CountryOfBirth"].ToString();
                    }
                }
                conn.Close();
            }

            return CountryOfBirth;
        }

        // Return's the Customer Nationality
        public String getCustomerNationality(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Nationality FROM dbo.Customers WHERE CustomerID = " + CID;
            String Nationality = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Nationality = sdr["Nationality"].ToString();
                    }
                }
                conn.Close();
            }

            return Nationality;
        }

        // Return's the Customer Country
        public String getCustomerCountry(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Country FROM dbo.Customers WHERE CustomerID = " + CID;
            String Country = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Country = sdr["Country"].ToString();
                    }
                }
                conn.Close();
            }

            return Country;
        }

        // Return's the Customer's Spending Powert
        public String getCustomerSpendingPower(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SpendingPower FROM dbo.Customers WHERE CustomerID = " + CID;
            String SpendingPower = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        SpendingPower = sdr["SpendingPower"].ToString();
                    }
                }
                conn.Close();
            }

            return SpendingPower;
        }

        // Return's the Customer KYC Check (IdentityMind) Result Code
        public String getCustomerKYCResultCode(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT resultCode FROM dbo.Customers WHERE CustomerID = " + CID;
            String KYCResultCode = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        KYCResultCode = sdr["resultCode"].ToString();
                    }
                }
                conn.Close();
            }

            return KYCResultCode;
        }

        // Return's the Customer Owning Agent's Name based on Agent ID
        public String getCustomerOwningAgentName(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentName FROM dbo.Agents WHERE AgentID IN (SELECT AgentID FROM dbo.Customers WHERE CustomerID = " + CID + ")";
            String AgentName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentName = sdr["AgentName"].ToString();
                    }
                }
                conn.Close();
            }

            return AgentName;
        }

        // Return's the Count of document for Customer
        public String getCustomerDocumentCount(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(*) AS TotCount FROM dbo.CustomerDocuments WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["TotCount"].ToString() == "0")
                        {
                            output = "NODOCS";
                        }
                        else
                        {
                            output = "DOCSOK";
                        }
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's true/false if all customer documents have expired
        public Boolean checkIfCustomerDocumentsHaveAllExpired(String CID)
        {
            CultureInfo ci = new CultureInfo("en-GB");
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT MAX(CONVERT(datetime, ExpiryDate, 103)) AS ExpDate FROM dbo.CustomerDocuments WHERE CustomerID = " + CID;
            Boolean output = false;
            String theDateTime = String.Empty;
            String TodaysDate = DateTime.Now.ToString("dd/MM/yyyy");

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {

                        theDateTime = sdr["ExpDate"].ToString();
                    }
                }
                conn.Close();
            }

            DateTime t1 = Convert.ToDateTime(theDateTime,ci);
            DateTime t2 = Convert.ToDateTime(TodaysDate,ci);

            if (t2 > t1)
            {
                output = true;
            }
            else
            {
                output = false;
            }

            return output;
        }

        // Return's the Customer Record ID based on Customer ID
        public String getCustomerRecordIDBasedOnCustomerID(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT RecordID FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = sdr["RecordID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Customer KYC Result Description
        public String getCustomerKYCResultDescription(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT resultDescription FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = sdr["resultDescription"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Customer KYC Transaction Code
        public String getCustomerKYCResultTransactionID(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT TransactionID FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = sdr["TransactionID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the customer KYC Check Date
        public String getCustomerKYCCheckDateDate(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT convert(varchar(10),KYCCheckDate,101) as KYCCheckDate FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if(sdr["KYCCheckDate"] !=null)
                        {
                            output = sdr["KYCCheckDate"].ToString().Substring(0, 10);
                        }
                       
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Customer KYC Notes
        public String getCustomerKYCNotes(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT manualKYCNotes FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = sdr["manualKYCNotes"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the type of check for KYC for Customer
        public String getCustomerKYCCheckType(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT typeOfCheck FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = sdr["typeOfCheck"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's who ran the KYC
        public string getCustomerKYCRunBy(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT KYCBy FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = sdr["KYCBy"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's if Customer KYC has been run before
        public Boolean confirmCustomerKYCHasBeenDone(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            Boolean result = false;

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TransactionID FROM dbo.Customers WHERE CustomerID = " + CID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            if (!String.IsNullOrEmpty(sdr["TransactionID"].ToString()))
                            {
                                result = true;
                            }
                        }
                    }
                    conn.Close();
                }

                return result;
            }
            finally
            {
                conn.Dispose();
            }

        }

        // Return's the Business NAme
        public String getCustomerBusinessName(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT BusinessName FROM dbo.Customers WHERE CustomerID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = sdr["BusinessName"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's true/false if customer's document is about to expire in the next 30 days
        public Boolean isDocumentExpiringin30Days(ValidOldestDoc docList)
        {
            if (docList == null)
            {
                return false;
            }
            DateTime documentDate = docList.ExpiryDate;
            Boolean isExpiring = false;
            DateTime currentDate = DateTime.Now;

            DateTime next30days = currentDate.AddDays(60);

            if (documentDate > currentDate && documentDate <= next30days)
            {
                isExpiring = true;
            }

            return isExpiring;

        }

        // ------------------------------------------------------- UPDATES ------------------------------------------------------

        public void updateCustomerDetails(String CID, String lastname, String firstname, String dob, String occupation, String addressline1, String addressline2, String suburb, String state, String postcode, String homephone, String workphone, String mobile, String email1, String email2)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            using (SqlCommand cmd = new SqlCommand("spUpdateCustomerDetails", conn))
            {
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
                cmd.Parameters.Add("@lastname", SqlDbType.NVarChar).Value = lastname;
                cmd.Parameters.Add("@firstnames", SqlDbType.NVarChar).Value = firstname;
                cmd.Parameters.Add("@dob", SqlDbType.NVarChar).Value = dob;
                cmd.Parameters.Add("@occupation", SqlDbType.NVarChar).Value = occupation;
                cmd.Parameters.Add("@addressline1", SqlDbType.NVarChar).Value = addressline1;
                cmd.Parameters.Add("@addressline2", SqlDbType.NVarChar).Value = addressline2;
                cmd.Parameters.Add("@suburb", SqlDbType.NVarChar).Value = suburb;
                cmd.Parameters.Add("@state", SqlDbType.NVarChar).Value = state;
                cmd.Parameters.Add("@postcode", SqlDbType.NVarChar).Value = postcode;
                cmd.Parameters.Add("@homephone", SqlDbType.NVarChar).Value = homephone;
                cmd.Parameters.Add("@workphone", SqlDbType.NVarChar).Value = workphone;
                cmd.Parameters.Add("@mobile", SqlDbType.NVarChar).Value = mobile;
                cmd.Parameters.Add("@email1", SqlDbType.NVarChar).Value = email1;
                cmd.Parameters.Add("@email2", SqlDbType.NVarChar).Value = email2;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }

            conn.Close();
        }

        //Set the Risk Level of the customer

        public void SetRiskLevel(String CID, String RL, String RR)
        {
            SqlConnection conn = new SqlConnection();
            String SQLStmt = "UPDATE dbo.Customers SET RiskLevel = " + RL + ", RiskReason = '" + RR + "'  WHERE CustomerID = " + CID;
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand(SQLStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }


        }

        // Update the Balance of the Customer
        public void updateCustomerBalance(String CID, float Amount)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String StmtSql = "UPDATE dbo.Customers SET AccountBalance = " + Amount + " WHERE CustomerID = " + CID;

            using (SqlCommand cmd = new SqlCommand(StmtSql))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            
        }

        // Add Credit/Debit Note against Customer
        public void AddCreditDebitForCustomer(String CID, float Amount, String Description, String TypeOfUpdate, String TID, String MasterBAcc)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt2 = @"INSERT INTO dbo.CreditTransactions (CustomerID, CreditAmount, MasterBankAccountID, DepositMethod, Type, TransactionID, CreatedDateTime, CreatedBy, CreditConfirmed, AgentID) 
            VALUES (" + CID + ", " + Amount + ", '" + MasterBAcc + "', '" + Description + "', '" + TypeOfUpdate + "', " + TID + ", CURRENT_TIMESTAMP, '" + HttpContext.Current.Session["LoggedUserFullName"].ToString() + "','Y', " +
            "'" + HttpContext.Current.Session["AgentID"].ToString() + "')";

            using (SqlCommand cmd = new SqlCommand(SqlStmt2))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        

        // Update's the customer's KYC to expired
        public void ExpireKYC(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = "UPDATE dbo.Customers SET resultDescription='EXPIRED' WHERE CustomerID = " + CID;

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        // Insert into AuditLog
        public void insertIntoAuditLog(String CID, String Uname, String AName, String ActionType, String Description)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = @"INSERT INTO CustAuditLog (CustomerID, Username, AgentName, ActionType, Description, AuditDateTime) VALUES
                                    (@CustomerID, @Username, @AgentName, @ActionType, @Description, CURRENT_TIMESTAMP)";
                conn.Open();
                cmd.Parameters.AddWithValue("@CustomerID", CID);
                cmd.Parameters.AddWithValue("@Username", Uname);
                cmd.Parameters.AddWithValue("@AgentName", AName);
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.AddWithValue("@Description", Description);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void updateRiskLevel(String CID, String RiskLevel)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = @"UPDATE dbo.Customers SET RiskLevel = " + RiskLevel;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }





}