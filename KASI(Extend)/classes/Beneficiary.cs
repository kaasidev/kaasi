﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;

namespace KASI_Extend_.classes
{
    public class Beneficiary
    {
        // Return's The Beneficiary Name 
        public String getBeneficiaryName(String BID)
        {
            String BeneficiaryName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BeneficiaryName FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryName = sdr["BeneficiaryName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryName;
        }

        // Return's the Beneficiary Full Address
        public String getBeneficiaryAddressLine1(String BID)
        {
            String BeneficiaryAddress = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AddressLine1 FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryAddress = sdr["AddressLine1"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryAddress;
        }

        // Return's the Beneficiary City
        public String getBeneficiaryCity(String BID)
        {
            String BeneficiarySuburb = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT Suburb FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiarySuburb = sdr["Suburb"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiarySuburb;
        }

        // Return's the Beneficiary Home
        public String getBeneficiaryPhone(String BID)
        {
            String BeneficiaryPhone = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TelHome FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryPhone = sdr["TelHome"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryPhone;
        }

        // Return's the Beneficiary Email Address
        public String getBeneficiaryEmail(String BID)
        {
            String BeneficiaryEmail = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT EmailAddress FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryEmail = sdr["EmailAddress"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryEmail;
        }

        // Return's the Beneficairy National ID Card
        public String getBeneficiaryNIC(String BID)
        {
            String BeneficiaryNIC = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT NationalityCardID FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryNIC = sdr["NationalityCardID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryNIC;
        }

        // Return's the Beneficiary PassportID 
        public String getBeneficiaryPassport(String BID)
        {
            String BeneficiaryPassport = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT PassportID FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryPassport = sdr["PassportID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryPassport;
        }

        // Return's the Beneficiary Full Address
        public String getBeneficiaryFullAddress(String BID)
        {
            String BeneficiaryAddress = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AddressLine1 + ' ' + AddressLine2 + ' ' + Suburb + ' ' + State + ' ' + Postcode + ' ' + Country AS FullAddy FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryAddress = sdr["FullAddy"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            BeneficiaryAddress = BeneficiaryAddress.Replace(",", "");
            BeneficiaryAddress = BeneficiaryAddress.Replace("-- SELECT --", "");
            return BeneficiaryAddress;
        }

        // Return's the Beneficary Mobile Number
        public String getBeneficiaryMobile(String BID)
        {
            String BeneficiaryMobile = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT Mobile FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryMobile = sdr["Mobile"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryMobile;
        }

        // Return's the Beneficiary Relationship
        public String getBeneficiaryRelationship(String BID)
        {
            String output = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT Relationship FROM dbo.Beneficiaries WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            output = sdr["Relationship"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return output;
        }
    }
}