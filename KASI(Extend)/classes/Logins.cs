﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace KASI_Extend_.classes
{
    public class Logins
    {
        public int LoginID { get; set; }
        public String FullName { get; set; }
        public String LoginType { get; set; }
        public String AgentName { get; set; }
        public String Email { get; set; }
        public String Active { get; set; }
        public String ViewEdit { get; set; }

        public string getLoginIDFromEmailAddress(String Email)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT LoginID FROM dbo.Logins WHERE Email = '" + Email + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["LoginID"].ToString();
                    }
                }
                conn.Close();
            }
            return output;
        }

        public String getUsernameFromID(String LID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Username FROM dbo.Logins WHERE LoginID = " + LID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["Username"].ToString();
                    }
                }
                conn.Close();
            }
            return output;
        }

        public String getPasswordFromID(String LID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Password FROM dbo.Logins WHERE LoginID = " + LID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["Password"].ToString();
                    }
                }
                conn.Close();
            }
            return output;
        }

        public String getEmailFromID(String LID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Email FROM dbo.Logins WHERE LoginID = " + LID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["Email"].ToString();
                    }
                }
                conn.Close();
            }
            return output;
        }

        public String getFullNameFromID(String LID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT FullName FROM dbo.Logins WHERE LoginID = " + LID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["FullName"].ToString();
                    }
                }
                conn.Close();
            }
            return output;
        }

        // Return's the First Name From Login ID
        public String getLoginFirstName(String LID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT FirstName FROM dbo.Logins WHERE LoginID = " + LID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["FirstName"].ToString();
                    }
                }
                conn.Close();
            }
            return output;
        }

        // Return's the Login details from Email address
        public String getLoginDetailsFromEmailAddress(String Email)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT FullName, Password, Username FROM dbo.Logins WHERE Email = '" + Email + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = sdr["FullName"].ToString() + "|" + sdr["Password"].ToString() + "|" + sdr["Username"].ToString();
                        }
                    }
                    else
                    {
                        output = "NORECORDS";
                    }
                    
                }
                conn.Close();
            }
            return output;
        }

        // Return's if the Logged User is a Compliance Manager
        public Boolean isComplianceManager(String LID)
        {
            Boolean isCManager = false;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT IsComplianceManager FROM dbo.Logins WHERE LoginID = " + LID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["IsComplianceManager"].ToString();
                    }
                }
                conn.Close();
            }

            if (output == "True")
            {
                isCManager = true;
            }

            return isCManager;
        }

        // Return's the Compliance Manager's PIN
        public String getComplianceManagerPIN(String LID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AdminOverrideCode FROM dbo.Logins WHERE LoginID = " + LID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["AdminOverrideCode"].ToString();
                    }
                }
                conn.Close();
            }
            return output;
        }

    }

    
}