﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Mime;

namespace KASI_Extend_.classes
{
    public class EmailCustomer
    {
        public void SendEmailWhenSaved(String TID)
        {
            var fromAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "KAASI");
            var toAddress = new MailAddress("shehan.peiris@improvata.com.au", "Shehan Peiris");
            const string fromPassword = "1Mprovata#";
            String subject = "KAASI Notification - Transaction " + TID + " has been entered";

            String body = SavedEmailBody(TID);

           

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            try {
                var smtp = new SmtpClient
                {
                    Host = "mail.improvata.com.au",
                    Port = 25,
                    EnableSsl = false,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                String FileName = "notification-flat.png";
                LinkedResource LinkedImage = new LinkedResource(System.Web.HttpContext.Current.Server.MapPath("~//images//" + FileName), "image/png");
                LinkedImage.ContentId = "Notification";
                LinkedImage.ContentType = new ContentType(MediaTypeNames.Image.Jpeg);

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body + "<img src=cid:Notification>", null, "text/html");
                htmlView.LinkedResources.Add(LinkedImage);

                kaasiemail.AlternateViews.Add(avHtml);
                kaasiemail.AlternateViews.Add(htmlView);

                smtp.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendEmailWhenApproved(String TID)
        {
            var fromAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "KAASI");
            var toAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "Sajith Jayaratne");
            const string fromPassword = "1Mprovata#";
            String subject = "KAASI Notification - Transaction " + TID + " has been approved";

            String body = ApprovedEmailBody();

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            try {
                var smtp = new SmtpClient
                {
                    Host = "mail.improvata.com.au",
                    Port = 25,
                    EnableSsl = false,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtp.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendEmailWhenCancelled(String TID)
        {
            var fromAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "KAASI");
            var toAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "Sajith Jayaratne");
            const string fromPassword = "1Mprovata#";
            String subject = "KAASI Notification - Transaction " + TID + " has been cancelled";

            String body = CancelledEmailBody();

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            try
            {
                var smtp = new SmtpClient
                {
                    Host = "mail.improvata.com.au",
                    Port = 25,
                    EnableSsl = false,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtp.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendAgentEmailCreation(String AID)
        {
            var fromAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "KAASI");
            var toAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "Sajith Jayaratne");
            const string fromPassword = "1Mprovata#";
            String subject = "Welcome to KAASI - Your Login Details";

            String body = CancelledEmailBody();

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            try
            {
                var smtp = new SmtpClient
                {
                    Host = "mail.improvata.com.au",
                    Port = 25,
                    EnableSsl = false,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtp.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public String ApprovedEmailBody()
        {
            String output = String.Empty;

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + "<html><body>Your transaction has been successully approved and funds should be available in <COUNTRY> within the next 5 mins.</body></html>";
            
            return output;
        }

        public String SavedEmailBody(String TID)
        {
            String output = String.Empty;
            classes.Transactions Trans = new classes.Transactions();
            classes.Customer Cust = new classes.Customer();
            classes.DepositsCredits DC = new classes.DepositsCredits();

            String CustomerID = Trans.getCustomerIDFromTransactionID(TID);

            output = "<!DOCTYPE html PUBLIC ' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
            output = output + "<html xmlns='http://www.w3.org/1999/xhtml'>";
            output = output + "<head>";
            output = output + "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
            output = output + "<title>KAASI Notification - Transactions Successfully Created</title>";
            output = output + "<meta name='viewport' content='width=device-width, initial-scale=1.0'/>";
            output = output + "< link href = 'https://fonts.googleapis.com/css?family=Varela+Round' rel = 'stylesheet' >";
            output = output + "</head>";

            output = output + "<body style='margin: 0; padding: 0;'>";

            output = output + "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
            output = output + "<tr>";
            output = output + "<td>";

            output = output + "<table align='center' border='0' cellpadding='0' cellspacing='0' width='800' style='border-collapse: collapse;'>";

            output = output + "<tr>";
            output = output + "<td align='center' bgcolor='#00B545' style='padding: 40px 0 30px 0;'>";
            output = output + "<img src=cid:notificationlogo alt='KAASI Notification' width='300' height ='230' style='display: block;'";
            output = output + "</td>";
            output = output + "</tr>";

            output = output + "<tr>";
            output = output + "<td bgcolor='#ffffff' style='padding: 40px 30px 40px 30px;'>";

                output = output + "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";

                output = output + "<tr>";
                output = output + "<td style='color: #153643; font-family: Arial, sans-serif; font-size: 24px;'>";
                output = output + "Transaction " + TID + " Successfully Created";
                output = output + "</td>";
                output = output + "</tr>";

                output = output + "<tr>";
                output = output + "<td style='padding: 20px 0 30px 0;' color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;>";
                output = output + "Your transaction has been successfully created in KAASI. Once your funds have been confirmed, we will approve the transaction. You will get another notification confirming when the transaction has been approved. Should additional documentation be required, we will contact you directly before processsing the transaction.";
                output = output + "</td>";
                output = output + "</tr>";

                output = output + "<tr>";
                output = output + "<td>";

                    output = output + "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";

                    output = output + "<tr>";
                    output = output + "<td width='310' valign='top' color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;>";
                    output = output + "Customer Details<br/>";
                     // - Information about the Customer Table
                    output = output + "<table border='0' width='310' cellspacing='0' cellpadding='0'>";
                    
                    // Customer ID
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Cust ID:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + CustomerID;
                    output = output + "</td>";
                    output = output + "</tr>";
                    // End Customer ID

                    // Name
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Name:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + Cust.getCustomerFullName(CustomerID);
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Name

                    // Address
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Address:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + Cust.getCustomerFullName(CustomerID);
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Address

                    // Phone/Mobile
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Phone:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + Cust.getCustomerHomeNumber(CustomerID) + " / " + Cust.getCustomerMobile(CustomerID);
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Phone/Mobile

                    // Phone/Mobile
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Email:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + Cust.getCustomerEmailAddress1(CustomerID);
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Phone/Mobile
                    

                    output = output + "</table>";
                    // -- End Information about the Customer Table
                    output = output + "</td>";

                    output = output + "<td style='font-size: 0; line-height: 0;' width='20'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td width='310' valign='top' color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;>";
                    output = output + "Transaction Details<br/>";

                    output = output + "<table border='0' width='310' cellspacing='0' cellpadding='0'>";

                    // Date
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Date:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + Trans.getTransactionDateTime(TID);
                    output = output + "</td>";
                    output = output + "</tr>";
                    // End Date

                    // Amount Remitted
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Amount to Remit:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + String.Format("{0:C2}", float.Parse(Trans.getTransactionSendingAmount(TID).ToString()));
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Amount Remitted

                    // Service Charge
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Service Charge:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + String.Format("{0:C2}", float.Parse(Trans.getTransactionServiceCharge(TID).ToString()));
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Service Charge

                    // Total Paid
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Total Received:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + String.Format("{0:C2}", float.Parse(Trans.getTransactionTotalAUDAmount(TID).ToString())) ;
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Total PAid

                    // PAid INto
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Paid Into: </b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + DC.getDepositMethod(CustomerID);
                    output = output + "</td>";
                    output = output + "</tr>";
                    // PAid INto

                    // Exchange Rate 
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Exchange Rate: </b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + String.Format("{0:N2}", float.Parse(Trans.getTransactionRate(TID).ToString()));
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Exchange Rate 

                    // Total Sending Amount
                    output = output + "<tr>";
                    output = output + "<td>";
                    output = output + "<b>Depositing Amount:</b>";
                    output = output + "</td>";

                    output = output + "<td width='10'>";
                    output = output + "&nbsp;";
                    output = output + "</td>";

                    output = output + "<td>";
                    output = output + String.Format("{0:N2}", float.Parse(Trans.getReceivedAmountFromTransaction(TID).ToString()));
                    output = output + "</td>";
                    output = output + "</tr>";
                    // Total Sending Amont


                    output = output + "</table>";



                    output = output + "</td>";



                    output = output + "</tr>";
                    output = output + "</table>";

                output = output + "</td>";
                output = output + "</tr>";

                output = output + "<tr>";
                output = output + "<td>";
                output = output + "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";

                output = output + "<tr>";
                output = output + "<td style='padding: 0 30px 0 0;'>";

                output = output + "Beneficiary Details";

                output = output + "</td>";
                output = output + "</tr>";

                output = output + "</table>";
                output = output + "</td>";
                output = output + "</tr>";

                output = output + "</table>";

            output = output + "</td>";
            output = output + "</tr>";

                output = output + "<tr>";
                    output = output + "<td bgcolor='#DCDCDC' style='padding: 30px 30px 30px 30px;'>";
                        output = output + "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                            output = output + "<tr>";

                                output = output + "<td width='75%' style='color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;'>";
                                output = output + "&reg; Kapruka Pty Ltd <br/> Powered by FlexeWallet";
                                output = output + "</td>";
                                output = output + "<td align='right'>";

                                    output = output + "<table border='0' cellpadding='0' cellspacing='0'>";

                                        output = output + "<tr>";
                                        output = output + "<td>";
                                        output = output + "<a href='#'>";
                                        output = output + "Facebook Logo";
                                        output = output + "</a>";
                                        output = output + "</td>";

                                        output = output + "<td>";
                                        output = output + "<a href='#'>";
                                        output = output + "LinkedIn Logo";
                                        output = output + "</a>";
                                        output = output + "</td>";
                                        output = output + "</tr>";

                                   output = output + "</table>";

                                output = output + "</td>";
                            output = output + "</tr>";

                        output = output + "</table>";

                    output = output + "</td>";
                output = output + "</tr>";

            output = output + "</table>";



            output = output + "</td>";
            output = output + "</tr>";
            output = output + "</table>";

            output = output + "</body>";


            output = output + "</html>";

            


            return output;
        }

        public String CancelledEmailBody()
        {
            String output = String.Empty;

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + "<html><body>Your transaction has been cancelled. Your funds should be refunded within the next 3-5 business days.</body></html>";

            return output;
        }

        public String NewAgentEmailBody()
        {
            String output = String.Empty;

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + "<html><body>Thank you for signing up with KAASI.<br/></br>Please use the following URL to login to the system: https://wwww.kaasi.com.au<br/><br/>Your login details are as follows</body></html>";

            return output;
        }
    }
}