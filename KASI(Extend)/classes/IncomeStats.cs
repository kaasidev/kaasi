﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.classes
{
    public class IncomeStats
    {
        public String AgentName { get; set; }
        public float AgentIncome { get; set; }
    }
}