﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections.Generic;


namespace KASI_Extend_.classes
{
    public class MasterCompany
    {
        // Return's the Master Company Name
        public String MasterCompanyName()
        {
            String MasterCompName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompany'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompName = sdr["SettingsVariable"].ToString();
                        }
                    }
                }
                conn.Close();
            }

            return MasterCompName;
        }

        // Return's the Master Company's Address
        public String MasterCompanyFullAddress()
        {
            String MasterCompAddressLine1 = String.Empty;
            String MasterCompSuburb = String.Empty;
            String MasterCompState = String.Empty;
            String MasterCompPostcode = String.Empty;
            String MasterCompCountry = String.Empty;

            String MasterCompFullAddress = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyAddressLine1'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompAddressLine1 = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanySuburb'";
                    cmd.Connection = conn;

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompSuburb = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyState'";
                    cmd.Connection = conn;

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompState = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyPostcode'";
                    cmd.Connection = conn;

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompPostcode = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyCountry'";
                    cmd.Connection = conn;

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompCountry = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            MasterCompFullAddress = MasterCompAddressLine1 + ' ' + MasterCompSuburb + ' ' + MasterCompState + ' ' + MasterCompPostcode + ' ' + MasterCompCountry;

            return MasterCompFullAddress;
        }

        // Return's the MAster Company Address Line 1
        public String getMasterCompanyAddressLine1()
        {
            String MasterCompAddressLine1 = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyAddressLine1'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompAddressLine1 = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return MasterCompAddressLine1;

        }

        // Return's the Master Company Address Line 2
        public String getMasterCompanyAddressLine2()
        {
            String MasterCompAddressLine2 = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyAddressLine2'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompAddressLine2 = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return MasterCompAddressLine2;
        }

        // Return's the Master Company Suburb, State and Postcode Bundled Together
        public String getMasterCompanyAddressBundle()
        {
            String MasterCompSuburb = String.Empty;
            String MasterCompState = String.Empty;
            String MasterCompPostcode = String.Empty;
            String MasterCompCountry = String.Empty;
            String MasterCompFullAddress = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanySuburb'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompSuburb = sdr["SettingsVariable"].ToString();
                        };
                    };
                    conn.Close();
                };

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyState'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompState = sdr["SettingsVariable"].ToString();
                        };
                    };

                    conn.Close();
                };

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyPostcode'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompPostcode = sdr["SettingsVariable"].ToString();
                        };
                    };
                    conn.Close();
                };

            };

            MasterCompFullAddress = MasterCompSuburb + ' ' + MasterCompState + ' ' + MasterCompPostcode;

            return MasterCompFullAddress;
        }

        // Return's the Master Company Suburb
        public String getMasterCompanySuburb()
        {
            String MasterCompSuburb = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanySuburb'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompSuburb = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return MasterCompSuburb;
        }

        // Return's the Master Company State
        public String getMasterCompanyState()
        {
            String MasterCompState = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyState'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompState = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return MasterCompState;
        }

        // Return's the Master Company Postcode
        public String getMasterCompanyPostcode()
        {
            String MasterCompPostcode = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyPostcode'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompPostcode = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return MasterCompPostcode;
        }

        // Return's the Master Company Telephone/Fax Bundled together
        public String getMasterCompanyTeleFax()
        {
            String Telephone = String.Empty;
            String Fax = String.Empty;
            String FullText = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyPhone'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Telephone = sdr["SettingsVariable"].ToString();
                        };
                    };
                    conn.Close();
                };

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyFax'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Fax = sdr["SettingsVariable"].ToString();
                        };
                    };
                    conn.Close();
                };


            };

            FullText = Telephone + '/' + Fax;

            return FullText;
        }



        // Return's the Master Company Telephone Number
        public String getMasterCompanyTelephone()
        {
            String MasterCompTele = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyPhone'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompTele = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return MasterCompTele;
        }

        // Return's the Master Company Fax Number
        public String getMasterCompanyFax()
        {
            String MasterCompFax = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyFax'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            MasterCompFax = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return MasterCompFax;
        }

        // Return's the Master Company's Email Address
        public String getMasterCompanyEmail()
        {
            String Email = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyEmail'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Email = sdr["SettingsVariable"].ToString();
                        }
                    }
                }
                conn.Close();
            }

            return Email;
        }

        // Return's the Master Company's ABN number
        public String getMasterCompanyABN()
        {
            String ABN = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyABN'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ABN = sdr["SettingsVariable"].ToString();
                        }
                    }
                }
                conn.Close();
            }

            return ABN;
        }

        // Return's the Master Company's ACN number
        public String getMasterCompanyACN()
        {
            String ACN = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MasterCompanyACN'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ACN = sdr["SettingsVariable"].ToString();
                        }
                    }
                }
                conn.Close();
            }

            return ACN;
        }

        // Return's the active currencies for the master company
        public Dictionary<String, String> getMasterCurrencies()
        {
            Dictionary<String, String> Currencies = new Dictionary<String, String>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CurrencyCode, CurrencyName FROM dbo.Currencies WHERE CurrencyCode NOT IN ('AUD')";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Currencies.Add(sdr["CurrencyCode"].ToString(), sdr["CurrencyName"].ToString());
                        }
                    }
                }
                conn.Close();
            }

            return Currencies;
        }

        // Return's All the Master Company Information for Editing
        public String getAllMasterCompanyInfoForEditing()
        {
            String output = String.Empty;

            output = getMasterCompanyAddressLine1() + "|" + getMasterCompanyAddressLine2() + "|" + getMasterCompanySuburb() + "|" + getMasterCompanyState() + "|" + getMasterCompanyPostcode() + "|";
            output = output + getMasterCompanyTelephone() + "|" + getMasterCompanyFax() + "|" + getMasterCompanyEmail() + "|" + getMasterCompanyABN() + "|" + getMasterCompanyACN();

            return output;
        }

        // Return's the total Profit for this month
        public String getMasterCompanyProfitForThisMonth()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SUM(MasterMoney) AS TotalMoney FROM dbo.Transactions WHERE MONTH(CreatedDateTime) = MONTH(getDate()) AND YEAR(CreatedDateTime) = YEAR(getDate())";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Total = sdr["TotalMoney"].ToString();
                    }
                }
                conn.Close();
            }

            return Total;

        }

        // Return's the LastCompanyCurrencyBuyRate
        public String getLastCurrencyBuyRate(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String BuyRate = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 BuyingRate FROM dbo.MasterCompanyBuyRates WHERE CurrencyID = " + CID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            BuyRate = sdr["BuyingRate"].ToString();
                        }
                    }
                    else
                    {
                        BuyRate = "0";
                    }

                }
                conn.Close();
            }

            return BuyRate;
        }

        // Return's the LastCompanyCurrencyBuyRate
        public String getLastCurrencyTranferToBuyRate(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String BuyRate = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 TranfertoBuyRate FROM dbo.MasterCompanyBuyRates WHERE CurrencyID = " + CID + " ORDER BY TranfertoBuyRateModifiedDate DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            if (sdr["TranfertoBuyRate"] != DBNull.Value)
                                BuyRate = sdr["TranfertoBuyRate"].ToString();
                        }
                    }
                    else
                    {
                        BuyRate = "0";
                    }

                }
                conn.Close();
            }

            return BuyRate;
        }

        // Return's the Last Margin for Master Company For Currency
        public String getLastMarginForCurrency(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String Margin = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 Margin FROM dbo.MasterCompanyRateMargins WHERE CurrencyID = " + CID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            Margin = sdr["Margin"].ToString();
                        }
                    }
                    else
                    {
                        Margin = "0";
                    }

                }
                conn.Close();
            }

            return Margin;
        }

        public String getMasterCompanyAustracRegNum()
        {
            String AustractNum = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'AustracRegNum'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AustractNum = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return AustractNum;

        }

        public String getMasterCompanyAustracRegDate()
        {
            String AustractRegDate = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'AustracRegDate'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AustractRegDate = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return AustractRegDate;

        }
        public String getMasterCompanyAustracExpDate()
        {
            String AustractExpDate = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'AustracExpDate'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AustractExpDate = sdr["SettingsVariable"].ToString();
                        };
                    };
                };

                conn.Close();
            }

            return AustractExpDate;

        }
    }
}