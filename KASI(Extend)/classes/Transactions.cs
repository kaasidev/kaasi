﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Enterprise;


namespace KASI_Extend_.classes
{
    public class Transactions
    {

        // Return's the customer's monthly sent amount
        public String getCustomerMonthlySentAmount(String CID)
        {
            float SentAmount = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerMonthlySend";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    if (idr[0].Equals(DBNull.Value))
                    {
                        SentAmount = 0;
                    }
                    else
                    {
                        SentAmount = float.Parse(idr[0].ToString());
                    }
                    
                }
            }
            conn.Close();
            return String.Format("{0:C2}", SentAmount);

        }

        // Return's the customer amount remitted in the last year
        public String getCustomerYearlySentAmount(String CID)
        {
            float SentAmount = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerYearSend";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    if (idr[0].Equals(DBNull.Value))
                    {
                        SentAmount = 0;
                    }
                    else
                    {
                        SentAmount = float.Parse(idr[0].ToString());
                    }

                }
            }
            conn.Close();
            return String.Format("{0:C2}", SentAmount);
        }

        // Return's the customers number of transactions in the last year
        public String getCustomerTotalTransLastYear(String CID)
        {
            float SentAmount = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spGetCustomerNumberOfTransPerYear";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = CID;
            cmd.Connection = conn;

            using (IDataReader idr = cmd.ExecuteReader())
            {
                if (idr.Read())
                {
                    if (idr[0].Equals(DBNull.Value))
                    {
                        SentAmount = 0;
                    }
                    else
                    {
                        SentAmount = float.Parse(idr[0].ToString());
                    }

                }
            }
            conn.Close();
            return String.Format("{0:C2}", SentAmount);
        }

        public void LogFailedTransactions(int TID,string reason,string userId)
        {
         
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "INSERT INTO TransactionReviewReasons values("+TID+",'"+reason+"',getdate(),"+userId+")";
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }

        }
        // Return's the Customer's ID Based on the Transaction ID
        public String getCustomerIDFromTransactionID(String TID)
        {
            String CustomerID = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CustomerID FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CustomerID = sdr["CustomerID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return CustomerID;
        }

        // Return's the BankCode for the transaction
        public String getBankCodeForTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankCode = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankCode = sdr["BankID"].ToString();
                    }
                }
                conn.Close();
            }
            return BankCode;
        }

        // Return's the default bank selected for the transaction
        public String getRoutingBankCodeForTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankCode = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DefaultRoutingBank FROM dbo.Banks WHERE BankCode IN (SELECT BankID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + "))";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankCode = sdr["DefaultRoutingBank"].ToString();
                    }
                }
                conn.Close();
            }

            return BankCode;
        }

        // Return's the Bank Name for the Transaction
        public String getBankNameFromTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Bank FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankName = sdr["Bank"].ToString();
                    }
                }
                conn.Close();
            }

            return BankName; 
        }

        // Return's the Date Time for the transaction
        public String getTransactionDateTime(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String TDateTime = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CreatedDateTime FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TDateTime = sdr["CreatedDateTime"].ToString();
                    }
                }
                conn.Close();
            }

            return TDateTime;
        }

        // Return's the Customer's full name for the transaction
        public String getCustomerFullNameFromTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String FullName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT FullName FROM dbo.Customers WHERE CustomerID  = (SELECT CustomerID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        FullName = sdr["FullName"].ToString();
                    }
                }
                conn.Close();
            }

            return FullName;
        }

        // Return's the Rate the Transaction was entered at
        public float getTransactionRate(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float Rate = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Rate FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Rate = float.Parse(sdr["Rate"].ToString());
                    }
                }
                conn.Close();
            }

            return Rate;
        }

        // Return's the Sending Amount of the transaction
        public float getTransactionSendingAmount(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float SendingAmount = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DollarAmount FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        SendingAmount = float.Parse(sdr["DollarAmount"].ToString());
                    }
                }
                conn.Close();
            }

            return SendingAmount;
        }

        // Return's the total amount used for the transaction
        public float getTransactionTotalAUDAmount(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float TotalAmount = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DollarAmount + ServiceCharge AS TotalSend FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TotalAmount = float.Parse(sdr["TotalSend"].ToString());
                    }
                }
                conn.Close();
            }

            return TotalAmount;
        }

        // Return's the BeneficiaryID of the transaction
        public String getTransactionBeneficiary(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Beneficiary = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BeneficiaryID FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Beneficiary = sdr["BeneficiaryID"].ToString();
                    }
                }
                conn.Close();
            }

            return Beneficiary;
        }

        // Return's the AccountID of the transaction
        public String getTransactionAccount(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Account = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Account = sdr["AccountID"].ToString();
                    }
                }
                conn.Close();
            }

            return Account;
        }

        // Returns't the Transaction Type
        public String getTransactionType(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String TransType = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT PaymentMethod FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TransType = sdr["PaymentMethod"].ToString();
                    }
                }
                conn.Close();
            }

            return TransType;
        }

        // Return's the Transaction Notes
        public String getTransactionNotes(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Notes = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Notes FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Notes = sdr["Notes"].ToString();
                    }
                }
                conn.Close();
            }

            return Notes;
        }

        // Return's the Bank Branch From Transaction ID
        public String getBankBranchFromTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankBranch = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankBranch FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankBranch = sdr["BankBranch"].ToString();
                    }
                }
                conn.Close();
            }

            return BankBranch;
        }

        // Return's the Beneficiary Name From the Beneficiary ID From the Transaction ID
        public String getBeneficiaryNameFromBIDfromTID(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BeneficiaryName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText =
                    "SELECT BeneficiaryName FROM dbo.Beneficiaries WHERE BeneficiaryID IN (SELECT BeneficiaryID FROM dbo.Transactions WHERE TransactionID = " +
                    TID + ")";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BeneficiaryName = sdr["BeneficiaryName"].ToString();
                    }
                }
                conn.Close();
            }

            return BeneficiaryName;
        }

        // Return's the Beneficiary Name From Transaction ID
        public String getBeneficiaryNameFromTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BeneficiaryName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BeneficiaryName FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BeneficiaryName = sdr["BeneficiaryName"].ToString();
                    }
                }
                conn.Close();
            }

            return BeneficiaryName;
        }

        // Return's the Beneficiary ID From Transaction ID
        public String getBeneficiaryIDFomTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BeneficiaryID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BeneficiaryID FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BeneficiaryID = sdr["BeneficiaryID"].ToString();
                    }
                }
                conn.Close();
            }

            return BeneficiaryID;
        }

        // Return's the Beneficiafy Account ID From TransactionID
        public String getBeneficiaryAccountIDFromTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String AccountID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        AccountID = sdr["AccountID"].ToString();
                    }
                }
                conn.Close();
            }

            return AccountID;
        }

        // Return's the Receiving Amount From Transaction ID
        public String getReceivedAmountFromTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String RemittedAmount = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT RemittedAmount FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        RemittedAmount = sdr["RemittedAmount"].ToString();
                    }
                }
                conn.Close();
            }

            return RemittedAmount;
        }

        // Return's the Service Charge of the Transaction
        public float getTransactionServiceCharge(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float ServiceCharge = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT ServiceCharge FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ServiceCharge = float.Parse(sdr["ServiceCharge"].ToString());
                    }
                }
                conn.Close();
            }

            return ServiceCharge;
        }

        // Return's the Reason the transaction was escalated to Rewiew
        public String getTransactionReviewReason(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String ReviewReason = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT ComplianceReason FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ReviewReason = sdr["ComplianceReason"].ToString();
                    }
                }
                conn.Close();
            }

            return ReviewReason;
        }

        // Return's the Remitted Currency based on TransactionID
        public String getRemittedCurrency(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String RemittedCurrency = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT RemittedCurrency FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        RemittedCurrency = sdr["RemittedCurrency"].ToString();
                    }
                }
                conn.Close();
            }

            return RemittedCurrency;
        }

        // Return's the Purpose of the Transaction
        public String getTransactionPurpose(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Purpose = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT Purpose FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Purpose = sdr["Purpose"].ToString();
                    }
                }
                conn.Close();
            }

            return Purpose;
        }

        // Return's the Source of Funds for the Transction
        public String getTransactionSourceOfFunds(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Source = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SourceOfFunds FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Source = sdr["SourceOfFunds"].ToString();
                    }
                }
                conn.Close();
            }

            return Source;
        }

        // Return's the Total Transaction for Agent per Currency Type
        public String getTransactionTotalByAgentIDandCurrencyType(String AID, String Currency, String BankID)
        {
            classes.Currencies CUR = new classes.Currencies();
            classes.Banking BKN = new classes.Banking();

            String BankName = BKN.getBankNameFromBankID(BankID);

            String CurrencyCode = CUR.getCurrencyCodeFromID(Currency);
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT COUNT(*) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND RemittedCurrency = '" 
                    + CurrencyCode + "' AND Bank = '" + BankName + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            if (sdr["TotCount"].Equals(DBNull.Value))
                            {
                                output = "0";
                            }
                            else
                            {
                                output = sdr["TotCount"].ToString();
                            }
                        }
                    }
                    else
                    {
                        output = "0";
                    }
                    
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Total in AUD for Agent per Currency Type
        public String getTransactionAUDTotalByAgentIDandCurrencyType(String AID, String Currency, String BankID)
        {
            classes.Currencies CUR = new classes.Currencies();
            classes.Banking BKN = new classes.Banking();

            String BankName = BKN.getBankNameFromBankID(BankID);

            String CurrencyCode = CUR.getCurrencyCodeFromID(Currency);
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT SUM(DollarAmount) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND RemittedCurrency = '"
                    + CurrencyCode + "' AND Bank = '" + BankName + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            if (sdr["TotCount"].Equals(DBNull.Value))
                            {
                                output = "0";
                            }
                            else
                            {
                                output = sdr["TotCount"].ToString();
                            }
                        }
                    }
                    else
                    {
                        output = "0";
                    }

                }
                conn.Close();
            }

            return output;
        }

        // Return's the Total in Foreign Currency for Agent per Currency Type
        public String getTransactionFCTotalByAgentIDandCurrencyType(String AID, String Currency, String BankID)
        {
            classes.Currencies CUR = new classes.Currencies();
            classes.Banking BKN = new classes.Banking();

            String BankName = BKN.getBankNameFromBankID(BankID);

            String CurrencyCode = CUR.getCurrencyCodeFromID(Currency);
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT SUM(RemittedAmount) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND RemittedCurrency = '"
                    + CurrencyCode + "' AND Bank = '" + BankName + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            if (sdr["TotCount"].Equals(DBNull.Value))
                            {
                                output = "0";
                            }
                            else
                            {
                                output = sdr["TotCount"].ToString();
                            }
                        }
                    }
                    else
                    {
                        output = "0";
                    }

                }
                conn.Close();
            }

            return output;
        }

        // Return's the Country ID for the Transaction
        public String getTransactionCountryID(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CountryID FROM dbo.Transactions WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CountryID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's a List of Transactions for particular overseas bank with transactions status AWAITING TRANSFER
        public List<String> getAwaitingTransferTransactionsForParticularBank(String AID, String BID)
        {
            List<String> TransactionList = new List<String>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TransactionID FROM dbo.Transactions WHERE AgentID = " + AID + " AND RoutingBank = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TransactionList.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    
                }
            }

                return TransactionList;
        }

        // Return's the review reasons of the transaction
        public String getTransactionReviewReasons(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT ReviewReason FROM dbo.TransactionReviewReasons WHERE TransactionID = " + TID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output += sdr["ReviewReason"].ToString() + "|";
                        }
                    }
                    else
                    {
                        output = "NORECORDS";
                    }
                   
                }
                conn.Close();
            }

            if (output != "NORECORDS")
            {
                int Length = output.Length;
                output = output.Substring(0, (Length - 1));
            }
            

            return output;
        }

        // Return's the highest AUD Transaction Amount done
        public String getHighestAUDTransAmount()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT MAX(DollarAmount) AS HighAmount FROM dbo.Transactions";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["HighAmount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getAgentIDFromTransaction(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AgentID FROM dbo.Transactions";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["AgentID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }


        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // UPDATES QUERIES 
        // ---------------------------------------------------------------------------------------------------------------------------------------------------------------

        // Updates the status of the transaction
        public void updateTranStatus(String TID, String StatusName, String Reason)
        {
            TransactionService TransServ = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            int intTID = Int32.Parse(TID);
            var TransDetails = TransServ.GetAll(x => x.TransactionID == intTID, null, "").SingleOrDefault();

            TransDetails.Status = StatusName;
            TransDetails.VoidReason = Reason;
            TransDetails.CancelledBy = HttpContext.Current.Session["LoggedUserFullName"].ToString();
            TransDetails.CancelledDateTime = DateTime.Now;
            TransServ.Update(TransDetails);

            //SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            //String StmtSQL = "UPDATE dbo.Transactions SET Status = '" + StatusName + "', VoidReason = '" + Reason + "', CancelledBy = '" + HttpContext.Current.Session["LoggedUserFullName"].ToString() + "', CancelledDateTime = CURRENT_TIMESTAMP WHERE TransactionID = " + TID;

            //using (SqlCommand cmd = new SqlCommand(StmtSQL))
            //{
            //    cmd.Connection = conn;
            //    conn.Open();
            //    cmd.ExecuteNonQuery();
            //    conn.Close();
            //}
        }

        // Updates the Bank File Status and Date Time it was written to the file
        public void updateTransBankFileInsertion(String TID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String stmtSql = "UPDATE dbo.Transactions SET InsertedIntoBankFile = 'Y', InsertedIntoBankFileDateTime = CURRENT_TIMESTAMP, Status = 'SUCCESSFUL' WHERE TransactionID = @TransactionID";

            using (SqlCommand cmd = new SqlCommand(stmtSql))
            {
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@TransactionID", TID);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void insertAuditRecord(String TID, String Uname, String Aname, String ActionType, String Description)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = @"INSERT INTO dbo.TransAuditLog (TransactionID, Username, AgentName, ActionType, Description, AuditDateTime) VALUES
                                    (@TransactionID, @Username, @AgentName, @ActionType, @Description, CURRENT_TIMESTAMP)";
                conn.Open();
                cmd.Parameters.AddWithValue("@TransactionID", TID);
                cmd.Parameters.AddWithValue("@Username", Uname);
                cmd.Parameters.AddWithValue("@AgentName", Aname);
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.AddWithValue("@Description", Description);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

    }
}