﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.classes
{
    public class QueryCustomer
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal AccountBalance { get; set; }
    }
}