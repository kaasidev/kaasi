﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.classes
{
    public class TransClass
    {

        // Return's the Customer's ID Based on the Transaction ID
        public String getCustomerIDFromTransactionID(String TID)
        {
            String CustomerID = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CustomerID FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CustomerID = sdr["CustomerID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return CustomerID;
        }

        // Return's the Transaction Date
        public String getTransactionDate(String TID)
        {
            String TransactionDate = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CreatedDateTime FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            TransactionDate = sdr["CreatedDateTime"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return TransactionDate;
        }

        // Return's the Transaction Amount Being Sent in AUD
        public String getTransactionAmount(String TID)
        {
            String TransactionAmount = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT DollarAmount FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            TransactionAmount = sdr["DollarAmount"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return String.Format("{0:C2}", float.Parse(TransactionAmount));
        }

        // Return's the Transaction Service Charge
        public String getTransactionServiceCharge(String TID)
        {
            String ServiceCharge = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT ServiceCharge FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ServiceCharge = sdr["ServiceCharge"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return String.Format("{0:C2}", float.Parse(ServiceCharge));
        }

        // Return's the Transaction Total Amount in AUD
        public String getTransactionTotalAUDAmount(String TID)
        {
            float DAmount = 0;
            float SCharge = 0;
            float TotalAmount = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT DollarAmount, ServiceCharge FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            DAmount = float.Parse(sdr["DollarAmount"].ToString());
                            SCharge = float.Parse(sdr["ServiceCharge"].ToString());
                        }
                    }
                    conn.Close();
                }
            }

            TotalAmount = DAmount + SCharge;
            return String.Format("{0:C2}", TotalAmount);
        }

        // Return's the Transaction Exchange Rate
        public String getTransactionExchangeRate(String TID)
        {
            String ExchangeRate = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT Rate FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ExchangeRate = sdr["Rate"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return String.Format("{0:N2}", float.Parse(ExchangeRate));
        }

        // Return's the Transaction LKR Amount
        public String getTransactionLKRAmount(String TID)
        {
            String LKRAmount = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT RemittedAmount FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            LKRAmount = sdr["RemittedAmount"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return String.Format("{0:N2}", float.Parse(LKRAmount));
        }

        // Return's the Beneficiary ID Based on the Transaction ID
        public string getBeneficiaryIDFromTransactionID(String TID)
        {
            String BeneficiaryID = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BeneficiaryID FROM dbo.Transactions WHERE TransactionID = " + TID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BeneficiaryID = sdr["BeneficiaryID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BeneficiaryID;
        }
    }
}