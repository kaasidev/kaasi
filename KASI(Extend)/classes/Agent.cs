﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Enterprise;
using Kapruka.Repository;

namespace KASI_Extend_.classes
{
    public class Agent
    {

        // Return's the Agent's Number of Transactions Today
        public String getAgentNumberOfTransactionsDaily(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(*) AS TotalTrans FROM dbo.Transactions WHERE TransactionAgentOwnership = " + AID + " AND MONTH(CreatedDateTime) = MONTH(getdate()) AND YEAR(CreatedDateTime) = YEAR(getdate())";
            String TotalTans = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        TotalTans = sdr["TotalTrans"].ToString();
                    }
                }
                conn.Close();
            }

            return TotalTans;
        }

        // Return's the agent's pending amount to be remitted
        public String getAgentPendingTotal(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(DollarAmount) AS TotalTrans FROM dbo.Transactions WHERE TransactionAgentOwnership = " + AID + " AND Status = 'PENDING'";
            String TotalTans = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["TotalTrans"].Equals(DBNull.Value))
                        {
                            TotalTans = "0";
                        }
                        else
                        {
                            TotalTans = sdr["TotalTrans"].ToString();
                        }
                        
                    }
                }
                conn.Close();
            }

            return TotalTans;
        }

        // Return's the agent's review amount 
        public String getAgentReviewTotal(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(DollarAmount) AS TotalTrans FROM dbo.Transactions WHERE TransactionAgentOwnership = " + AID + " AND Status = 'REVIEW'";
            String TotalTans = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["TotalTrans"].Equals(DBNull.Value))
                        {
                            TotalTans = "0";
                        }
                        else
                        {
                            TotalTans = sdr["TotalTrans"].ToString();
                        }

                    }
                }
                conn.Close();
            }

            return TotalTans;
        }

        // Return's the agent's number of pending transactions
        public String getAgentNbPendingTrans(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(*) AS TotalTrans FROM dbo.Transactions WHERE TransactionAgentOwnership = " + AID + " AND Status = 'PENDING'";
            String TotalTans = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["TotalTrans"].Equals(DBNull.Value))
                        {
                            TotalTans = "0";
                        }
                        else
                        {
                            TotalTans = sdr["TotalTrans"].ToString();
                        }

                    }
                }
                conn.Close();
            }

            return TotalTans;
        }

        // Return's the Agent's number of Review transactions
        public String getAgentnbReviewTrans(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT COUNT(*) AS TotalTrans FROM dbo.Transactions WHERE TransactionAgentOwnership = " + AID + " AND Status = 'REVIEW'";
            String TotalTans = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["TotalTrans"].Equals(DBNull.Value))
                        {
                            TotalTans = "0";
                        }
                        else
                        {
                            TotalTans = sdr["TotalTrans"].ToString();
                        }

                    }
                }
                conn.Close();
            }

            return TotalTans;
        }

        // Return's the agent's sent amount today
        public String getAgentSentTotalToday(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT SUM(DollarAmount) AS TotalTrans FROM dbo.Transactions WHERE TransactionAgentOwnership = " + AID + " AND Status = 'SUCCESSFUL' AND CONVERT(nvarchar(10), ApprovalDate, 103) = CONVERT(nvarchar(10), getDate(), 103)";
            String TotalTans = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["TotalTrans"].Equals(DBNull.Value))
                        {
                            TotalTans = "0";
                        }
                        else
                        {
                            TotalTans = sdr["TotalTrans"].ToString();
                        }
                        
                    }
                }
                conn.Close();
            }

            return TotalTans;
        }

        // Return's the agent's name based on Agent ID
        public String getAgentNameByID(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentName FROM dbo.Agents WHERE AgentID = " + AID ;
            String AgentName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentName = sdr["AgentName"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentName;
        }

        // Return's the Agent ID based on the Agent NAme
        public String getAgentIDByName(String AgentName)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentID FROM dbo.Agents WHERE AgentName = '" + AgentName + "'";
            String AgentID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentID = sdr["AgentID"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentID;
        }

        // Return's the Agent's Company Name
        public String getAgentCompanyName(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentName FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentName = sdr["AgentName"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentName;
        }

        // Return's the Agent's Full Address
        public string getAgentFullAddress(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentAddress1, AgentAddress2, AgentCity, AgentState, AgentPostcode FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentAddress = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentAddress = sdr["AgentAddress1"].ToString() + " ";
                        if (sdr["AgentAddress2"].ToString() != "")
                        {
                            AgentAddress = AgentAddress + sdr["AgentAddress2"].ToString() + " " ;
                        }
                        AgentAddress = AgentAddress + sdr["AgentCity"].ToString() + " " + sdr["AgentState"].ToString() + " " + sdr["AgentPostcode"].ToString();


                    }
                }
                conn.Close();
            }

            return AgentAddress;
        }

        public Kapruka.Repository.Agent getAgentFullAddress(Kapruka.Repository.Agent agent,string AID)
        {
           
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentAddress1, AgentAddress2, AgentCity, AgentState, AgentPostcode FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentAddress = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        agent.AgentAddress1 = sdr["AgentAddress1"].ToString();
                        agent.AgentAddress2 = sdr["AgentAddress2"].ToString();
                        agent.AgentCity = sdr["AgentAddress2"].ToString();
                        agent.AgentState = sdr["AgentAddress2"].ToString();
                        AgentAddress = sdr["AgentState"].ToString();
                        agent.AgentPostcode = sdr["AgentPostcode"].ToString();
                    }
                }
                conn.Close();
            }

            return agent;
        }

        // Return's the Agent Telephone Number
        public String getAgentTelephoneNumber(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentPhone FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentPhone = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentPhone = sdr["AgentPhone"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentPhone;
        }

        // Return's the Agent Fax Number
        public String getAgentFaxNumber(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentFax FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentFax = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentFax = sdr["AgentFax"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentFax;
        }

        // Return's the Agent Email Address
        public String getAgentEmailAddress(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentEmail FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentEmail = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentEmail = sdr["AgentEmail"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentEmail;
        }

        // Return's the Agent ABN Number
        public String getAgentABN(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentABN FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentABN = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentABN = sdr["AgentABN"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentABN;
        }

        // Return's the Agent ACN Number
        public String getAgentACN(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentACN FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentACN = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentACN = sdr["AgentACN"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentACN;
        
        
        }

        // Return's Agent Austrac Registration Number
        public String getAgentAustracRegNumber(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AustracNumber FROM dbo.Agents WHERE AgentID = " + AID;
            String AustracNum = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AustracNum = sdr["AustracNumber"].ToString();

                    }
                }
                conn.Close();
            }

            return AustracNum;

        }

        // Return's Agent Austrac Registration Date
        public String getAgentAustracRegDate(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AustracRegDate FROM dbo.Agents WHERE AgentID = " + AID;
            String AustracRenDate = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AustracRenDate = sdr["AustracRegDate"].ToString();

                    }
                }
                conn.Close();
            }

            return AustracRenDate;
        }

        // Return's the Agent Austrac Registration Renewal Date
        public String getAgentAustracRenewalDate(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AustracRenewalDate FROM dbo.Agents WHERE AgentID = " + AID;
            String RenewalDate = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        RenewalDate = sdr["AustracRenewalDate"].ToString();

                    }
                }
                conn.Close();
            }

            return RenewalDate;
        }

        // Return's the Agent Address Line 1
        public String getAgentAddressLine1(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentAddress1 FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentAddress1 = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentAddress1 = sdr["AgentAddress1"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentAddress1;
        }

        // Return's the Agent Address Line 2
        public String getAgentAddressLine2(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentAddress2 FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentAddress2 = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentAddress2 = sdr["AgentAddress2"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentAddress2;
        }

        // Return's the Agent Suburb
        public String getAgentSuburb(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentCity FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentCity = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentCity = sdr["AgentCity"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentCity;
        }

        // Return's the Agent State
        public String getAgentState(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentState FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentState = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentState = sdr["AgentState"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentState;
        }

        // Return's the Agent Address Line 1
        public String getAgentPostcode(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentPostcode FROM dbo.Agents WHERE AgentID = " + AID;
            String AgentPostcode = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        AgentPostcode = sdr["AgentPostcode"].ToString();

                    }
                }
                conn.Close();
            }

            return AgentPostcode;
        }

        // Return's the Agent's Monthly AUD Limit
        public String getAgentMonthlyAUDLimit(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT MonthlyAUDLimit FROM dbo.Agents WHERE AgentID = " + AID;
            String Limit = "0";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Limit = sdr["MonthlyAUDLimit"].ToString();

                    }
                }
                conn.Close();
            }

            return Limit;
        }

        // Return's the Agent's Yearly AUD Limit
        public String getAgentYearlyAUDLimit(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT YearlyAUDsLimit FROM dbo.Agents WHERE AgentID = " + AID;
            String Limit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Limit = sdr["YearlyAUDsLimit"].ToString();

                    }
                }
                conn.Close();
            }

            return Limit;
        }

        // Return's the Agent's Monthly Trans Limit
        public String getAgentMonthlyTransLimit(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT MonthlyTransLimit FROM dbo.Agents WHERE AgentID = " + AID;
            String Limit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Limit = sdr["MonthlyTransLimit"].ToString();

                    }
                }
                conn.Close();
            }

            return Limit;
        }

        // Return's the Agent's Yearly Trans Limit
        public String getAgentYearlyTransLimit(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT YearlyTransLimit FROM dbo.Agents WHERE AgentID = " + AID;
            String Limit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Limit = sdr["YearlyTransLimit"].ToString();

                    }
                }
                conn.Close();
            }

            return Limit;
        }

        // Return's the Agent's Currencies
        public List<String> getAgentCurrencies(String AID)
        {
            List<String> ACurrencies = new List<String>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.AgentCurrencies WHERE AgentID = " + AID + " AND CurrencyID <> 10";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ACurrencies.Add(sdr["CurrencyID"].ToString());
                    }
                }
                conn.Close();
            }

            return ACurrencies;
        }

        // Return's the Agent's Assigned Countries as a List
        public List<String> getAgentAssignedCountries(String AID)
        {
            List<String> ACountries = new List<String>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CountryID FROM dbo.AgentAssignedCountries WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ACountries.Add(sdr["CountryID"].ToString());
                    }
                }
                conn.Close();
            }

            return ACountries;
        }

        // Return's the Agent's Margin for Currency
        public String getAgentCurrencyMargin(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Margin FROM dbo.AgentCurrencyMargins WHERE AgentID = " + AID + " AND CurrencyID = " + CID;
            String Margin = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Margin = sdr["Margin"].ToString();

                    }
                }
                conn.Close();
            }

            return Margin;
        }

        // Return's the Agent's Margin Variation
        public String getAgentCurrencyVariation(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT UpDown FROM dbo.AgentCurrencyMargins WHERE AgentID = " + AID + " AND CurrencyID = " + CID;
            String Variation = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        Variation = sdr["UpDown"].ToString();

                    }
                }
                conn.Close();
            }

            return Variation;
        }

        // Return's the Agent Fee Commission
        public String getAgentFeeCommission(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentFeeCommission FROM dbo.Agents WHERE AgentID = " + AID;
            String FeeCommission = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        FeeCommission = sdr["AgentFeeCommission"].ToString();

                    }
                }
                conn.Close();
            }

            return FeeCommission;
        }

        // Return's the Agent Flat Fee
        public String getAgentFlatFee(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentFeeFlat FROM dbo.Agents WHERE AgentID = " + AID;
            String FlatFee = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        FlatFee = sdr["AgentFeeFlat"].ToString();

                    }
                }
                conn.Close();
            }

            return FlatFee;
        }

        // Return's the Agent Commission Gain 
        public String getAgentCommissionGainPerc(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT AgentTransGainCommission FROM dbo.Agents WHERE AgentID = " + AID;
            String GainCommission = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        GainCommission = sdr["AgentTransGainCommission"].ToString();

                    }
                }
                conn.Close();
            }

            return GainCommission;
        }

        // Return's the Agent Monthly Commission
        public String getAllAgentsMoneyForThisMonth()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Total = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SUM(AgentMoney) AS TotalMoney FROM dbo.Transactions WHERE MONTH(CreatedDateTime) = MONTH(getDate()) AND YEAR(CreatedDateTime) = YEAR(getDate())";
                cmd.Connection = conn;
                conn.Open();
                
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Total = sdr["TotalMoney"].ToString();
                    }
                }
                conn.Close();
            }

            return Total;
        }

        // Return's the value to check if this is the first time agent logs in
        public String getAgentFirstTimeLoginStatus(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT FirstTimeLogin FROM dbo.Agents WHERE AgentID = " + AID;
            String FirstTimeLogin = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        FirstTimeLogin = sdr["FirstTimeLogin"].ToString();

                    }
                }
                conn.Close();
            }

            return FirstTimeLogin;
        }

        // Return's true/false if agent has customers
        public Boolean checkAgentHasCustomers(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            Boolean hasCustomer = false;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT * FROM dbo.Customers WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        hasCustomer = true;
                    }
                }
                conn.Close();
            }

            return hasCustomer;
        }

        // Return's the SMTP Server Name
        public String getSMTPServerName(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SMTPServerName FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SMTPServerName"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP Port
        public String getSMTPPort(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SMTPPort FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SMTPPort"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SSL Setting
        public String getSMTPSSLSetting(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SSLEnabled FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SSLEnabled"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP Username
        public String getSMTPUsername(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SMTPUsername FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SMTPUsername"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP Password
        public String getSMTPPassword(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SMTPassword FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SMTPassword"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP Email Address
        public String getSMTPEmailAddress(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SMTPEmailAddress FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SMTPEmailAddress"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getAgentDefaultForeignBankID(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankID FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND CountryID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BankID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent Default Foreign Bank Settings based on Agent ID
        public String getAgentDefaultForeignBankSettings(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CountryID, BankID, ForeignBankAccountNumber FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND DefaultBank = 'Y'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CountryID"].ToString() + "|" + sdr["BankID"].ToString() + "|" + sdr["ForeignBankAccountNumber"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the list of agent who are on Ex Gain 
        public List<String> getAgentsOnExGainCommission()
        {
            List<String> output = new List<String>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AgentID FROM dbo.Agents WHERE AgentCommissionStructure = 'FXGain'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output.Add(sdr["AgentID"].ToString());
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the list of agent assigned currency 
        public List<String> getAgentCurrencyList(String AID)
        {
            List<String> output = new List<String>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.AgentCurrencies WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output.Add(sdr["CurrencyID"].ToString());
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the agent's total awaiting transfer
        public String getAgentAwaitingTransTotal(String AID)
        {
            int intAID = Int32.Parse(AID);

            TransactionService TransServ = new TransactionService(new UnitOfWorks(new KaprukaEntities()));
            var TransDetails = TransServ.GetAll(x => x.AgentID == intAID && x.Status == "AWAITING TRANSFER", null, "").ToList();

            String output = TransDetails.Count.ToString();

            //SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            //String output = String.Empty;

            //using (SqlCommand cmd = new SqlCommand())
            //{
            //    cmd.CommandText = "SELECT COUNT(*) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND Status = 'AWAITING TRANSFER'";
            //    cmd.Connection = conn;
            //    conn.Open();

            //    using (SqlDataReader sdr = cmd.ExecuteReader())
            //    {
            //        while (sdr.Read())
            //        {
            //            output = sdr["TotCount"].ToString();
            //        }
            //    }
            //    conn.Close();
            //}

            return output;
        }

        // Return's the Agent's Total AUD Awaiting Transfer
        public String getAgentAUDAwaitingTransTotal(String AID)
        {

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SUM(DollarAmount) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND Status = 'AWAITING TRANSFER'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr.HasRows)
                        {
                            output = sdr["TotCount"].ToString();
                            if (String.IsNullOrEmpty(output))
                            {
                                output = "0";
                            }
                        }
                        else
                        {
                            output = "0";
                        }
                        
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent's Total Awaiting Transfer Based on Currency
        public String getAgentTransTotalBasedOnAgentIDAndCurrencyCode(String AID, String CID, String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT COUNT(*) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND " +
                    "Status = 'AWAITING TRANSFER' AND RemittedCurrency = '" + CID + "' AND RoutingBank = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TotCount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent's Total AUD Awaiting Transfer Based on Currency
        public String getAgentAUDTotalBasedOnAgentIDandCurrencyCode(String AID, String CID, String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT SUM(DollarAmount) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND " +
                    "Status = 'AWAITING TRANSFER' AND RemittedCurrency = '" + CID + "' AND RoutingBank = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TotCount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent's Total AUD Awaiting Transfer Based on Currency
        public String getAgentAUDTotalBasedOnAgentIDandCurrencyCodeInLocalCurrency(String AID, String CID, String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT SUM(RemittedAmount) AS TotCount FROM dbo.Transactions WHERE AgentID = " + AID + " AND " +
                    "Status = 'AWAITING TRANSFER' AND RemittedCurrency = '" + CID + "' AND RoutingBank = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TotCount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the last bank file created date
        public String getAgentLastBankFileCreatedDate(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT TOP 1 MAX(InsertedIntoBankFileDateTime) As TotCount FROM dbo.Transactions WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TotCount"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the commission structure of the agent
        public String getAgentCommissionStructure(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT AgentCommissionStructure FROM dbo.Agents WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["AgentCommissionStructure"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent Total Number of Customer (Irrespective of Status)
        public String getAgentTotalNumberOfCustomers(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT COUNT(*) AS TotalCust FROM dbo.Customers WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TotalCust"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Number of Customers for that Agent that have transacted in the last 12 months
        public String getAgentCustomersThatHaveTransactedInLast12Month(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT COUNT(DISTINCT CustomerID) AS TotalCustomer FROM dbo.Transactions 
                            WHERE CreatedDateTime >= DATEADD(month, -12, GETDATE()) AND AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["TotalCustomer"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Gets the Agent Name from the TransactionID
        public String getAgentNameFromTransactionID(String TID)
        {
            AgentService agtser = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            TransactionService trserv = new TransactionService(new UnitOfWorks(new KaprukaEntities()));

            int TransID = Int32.Parse(TID);

            var AgentID = trserv.GetAll(x => x.TransactionID == TransID, null, "").SingleOrDefault().AgentID;
            var AgentName = agtser.GetAll(x => x.AgentID == AgentID, null, "").SingleOrDefault().AgentName;

            return AgentName;
        }


        // Update's the Agent Assigned Rate
        public void UpdateAgentAssignedRateOnAgentIDandCurrencyID(String AID, String CID, float Rate)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt = "UPDATE dbo.AgentCurrencyRates SET AssignedRate=@AssignedRate WHERE AgentID = " + AID + " AND CurrencyID = " + CID;

            using (SqlCommand cmd = new SqlCommand(SqlStmt))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.AddWithValue("@AssignedRate", Rate);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}