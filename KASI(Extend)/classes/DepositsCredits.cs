﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.classes
{
    public class DepositsCredits
    {

        // Return's the DepositMethod used by Customer ID
        public String getDepositMethod(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String DepositMethod = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 DepositMethod FROM dbo.CreditTransactions WHERE CustomerID = " + CID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        DepositMethod = sdr["DepositMethod"].ToString();
                    }
                }
                conn.Close();
            }
            return DepositMethod;
        }

        public float getTotalDeposited()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float TotalDeposit = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SUM(CreditAmount) AS TotalCredit FROM dbo.CreditTransactions WHERE MONTH(CreatedDateTime) = MONTH(getDate()) AND YEAR(CreatedDateTime) = YEAR(getDate())";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TotalDeposit = float.Parse(sdr["TotalCredit"].ToString());
                    }
                }
                conn.Close();
            }
            return TotalDeposit;
        }
    }
}