﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Mime;
using KASI_Extend_.classes;
using Kapruka.Enterprise;

namespace KASI_Extend_.classes
{
    public class EmailCustomer
    {
        public void SendEmailWhenSaved(String TID)
        {
            TransactionService TransServ = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            AgentService AgtServ = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            int theTID = Int32.Parse(TID);
            var theTransDetails = TransServ.GetAll(x => x.TransactionID == theTID, null, "").SingleOrDefault();

            var TheCustID = theTransDetails.CustomerID;

            CustomerService CustServ = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var theCustEmail = CustServ.GetAll(x => x.CustomerID == TheCustID, null, "").SingleOrDefault();

            var toAddress = new MailAddress(theCustEmail.EmailAddress, theCustEmail.FullName);

            var AgentName = AgtServ.GetAll(x => x.AgentID == theTransDetails.AgentID, null, "").SingleOrDefault().AgentName;


            String subject = AgentName + " Notification - Transaction " + TID + " has been entered";

            String body = SavedEmailBody(TID);

            

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            var AgentID = getAgentIDFromTransactionID(TID);
            var AgentSMTP = getAgentSMTPSettings(AgentID);

            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredentials = new NetworkCredential(AgentSMTP.SMTPUsername, AgentSMTP.SMTPassword);
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress(AgentSMTP.AgentEmail, AgentSMTP.AgentName);


            try
            {
                smtpClient.Host = AgentSMTP.SMTPServerName;
                smtpClient.Port = Int32.Parse(AgentSMTP.SMTPPort);
                smtpClient.Timeout = 50000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = basicCredentials;

                if (AgentSMTP.SSLEnabled == "1")
                {
                    smtpClient.EnableSsl = true;
                }
                else
                {
                    smtpClient.EnableSsl = false;
                }

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;
                kaasiemail.CC.Add(fromAddress);
                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                //String FileName = "notification-flat.png";
                //LinkedResource LinkedImage = new LinkedResource(System.Web.HttpContext.Current.Server.MapPath("~//images//" + FileName), "image/png");
                //LinkedImage.ContentId = "Notification";
                //LinkedImage.ContentType = new ContentType(MediaTypeNames.Image.Jpeg);

                //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body + "<img src=cid:Notification>", null, "text/html");
                //htmlView.LinkedResources.Add(LinkedImage);

                kaasiemail.AlternateViews.Add(avHtml);
                //kaasiemail.AlternateViews.Add(htmlView);

                smtpClient.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendEmailWhenApproved(String TID)
        {
            var fromAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "KAASI");
            var toAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "Sajith Jayaratne");
            const string fromPassword = "1Mprovata#";
            String subject = "KAASI Notification - Transaction " + TID + " has been approved";

            String body = ApprovedEmailBody();

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            try
            {
                var smtp = new SmtpClient
                {
                    Host = "mail.improvata.com.au",
                    Port = 25,
                    EnableSsl = false,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtp.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendEmailWhenCancelled(String TID)
        {
            var fromAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "KAASI");
            var toAddress = new MailAddress("sajith.jayaratne@improvata.com.au", "Sajith Jayaratne");
            const string fromPassword = "1Mprovata#";
            String subject = "KAASI Notification - Transaction " + TID + " has been cancelled";

            String body = CancelledEmailBody();

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            try
            {
                var smtp = new SmtpClient
                {
                    Host = "mail.improvata.com.au",
                    Port = 25,
                    EnableSsl = false,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtp.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendAffiliateEmailCreation(String LID)
        {
            classes.Logins LGS = new Logins();
            String LoginID = LID;
            String sendToEmail = LGS.getEmailFromID(LoginID);
            String FullName = LGS.getFullNameFromID(LoginID);

            classes.Settings STS = new Settings();
            String MailServer = STS.getSMTPServerName();
            String EmailAddress = STS.getSMTPEmailAddress();
            String MailPort = STS.getSMTPServerPort();
            String SSLEnabled = STS.getSMTPSSLEnabled();
            String EmailPassword = STS.getSMTPPassword();

            Boolean SSLValue = false;

            if (SSLEnabled == "2")
            {
                SSLValue = false;
            }
            else
            {
                SSLValue = true;
            }

            //var fromAddress = new MailAddress(EmailAddress, );
            var toAddress = new MailAddress(sendToEmail, FullName);
            string fromPassword = EmailPassword;
            String subject = "Welcome to KAASI - Your Login Details";

            String body = NewAgentEmailBody(LoginID);

            SettingsService SetServ = new SettingsService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var SetSMTP = SetServ.GetAll(x => x.SettingName == "SMTPServerName", null, "").SingleOrDefault().SettingsVariable;
            var SetPort = SetServ.GetAll(x => x.SettingName == "SMTPPort", null, "").SingleOrDefault().SettingsVariable;
            var SetUsername = SetServ.GetAll(x => x.SettingName == "SMTPUsername", null, "").SingleOrDefault().SettingsVariable;
            var SetPassword = SetServ.GetAll(x => x.SettingName == "SMTPPassword", null, "").SingleOrDefault().SettingsVariable;
            var setFrom = SetServ.GetAll(x => x.SettingName == "SMTPEmailAddress", null, "").SingleOrDefault().SettingsVariable;
            var setSSL = SetServ.GetAll(x => x.SettingName == "SSLEnabled", null, "").SingleOrDefault().SettingsVariable;

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredentials = new NetworkCredential(SetUsername, SetPassword);
            MailAddress fromAddress = new MailAddress(setFrom, "KAASI Notifications");

            try
            {


                smtpClient.Host = SetSMTP;
                smtpClient.Port = Int32.Parse(SetPort);
                smtpClient.Timeout = 50000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = basicCredentials;

                if (setSSL == "1")
                {
                    smtpClient.EnableSsl = true;
                }
                else
                {
                    smtpClient.EnableSsl = false;
                }

                //var smtp = new SmtpClient
                //{
                //    Host = MailServer,
                //    Port = Int32.Parse(MailPort),
                //    EnableSsl = SSLValue,
                //    Timeout = 10000,
                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                //    UseDefaultCredentials = false,
                //    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                //};

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtpClient.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendAgentEmailCreation(String EmailID)
        {
            classes.Logins LGS = new Logins();
            String LoginID = LGS.getLoginIDFromEmailAddress(EmailID);
            String sendToEmail = LGS.getEmailFromID(LoginID);
            String FullName = LGS.getFullNameFromID(LoginID);

            classes.Settings STS = new Settings();
            String MailServer = STS.getSMTPServerName();
            String EmailAddress = STS.getSMTPEmailAddress();
            String MailPort = STS.getSMTPServerPort();
            String SSLEnabled = STS.getSMTPSSLEnabled();
            String EmailPassword = STS.getSMTPPassword();

            Boolean SSLValue = false;

            if (SSLEnabled == "2")
            {
                SSLValue = false;
            }
            else
            {
                SSLValue = true;
            }

            //var fromAddress = new MailAddress(EmailAddress, );
            var toAddress = new MailAddress(sendToEmail, FullName);
            string fromPassword = EmailPassword;
            String subject = "Welcome to KAASI - Your Login Details";

            String body = NewAgentEmailBody(LoginID);

            SettingsService SetServ = new SettingsService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var SetSMTP = SetServ.GetAll(x => x.SettingName == "SMTPServerName", null, "").SingleOrDefault().SettingsVariable;
            var SetPort = SetServ.GetAll(x => x.SettingName == "SMTPPort", null, "").SingleOrDefault().SettingsVariable;
            var SetUsername = SetServ.GetAll(x => x.SettingName == "SMTPUsername", null, "").SingleOrDefault().SettingsVariable;
            var SetPassword = SetServ.GetAll(x => x.SettingName == "SMTPPassword", null, "").SingleOrDefault().SettingsVariable;
            var setFrom = SetServ.GetAll(x => x.SettingName == "SMTPEmailAddress", null, "").SingleOrDefault().SettingsVariable;
            var setSSL = SetServ.GetAll(x => x.SettingName == "SSLEnabled", null, "").SingleOrDefault().SettingsVariable;

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredentials = new NetworkCredential(SetUsername, SetPassword);
            MailAddress fromAddress = new MailAddress(setFrom, "KAASI Notifications");

            try
            {


                smtpClient.Host = SetSMTP;
                smtpClient.Port = Int32.Parse(SetPort);
                smtpClient.Timeout = 50000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = basicCredentials;

                if (setSSL == "1")
                {
                    smtpClient.EnableSsl = true;
                }
                else
                {
                    smtpClient.EnableSsl = false;
                }

                //var smtp = new SmtpClient
                //{
                //    Host = MailServer,
                //    Port = Int32.Parse(MailPort),
                //    EnableSsl = SSLValue,
                //    Timeout = 10000,
                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                //    UseDefaultCredentials = false,
                //    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                //};

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtpClient.Send(kaasiemail);

            }
            catch (Exception ex)
            {
                String error = ex.Message.ToString();
            }
        }

        public void SendAgentEmailEdit(String LID)
        {
            classes.Logins LGS = new Logins();
            String sendToEmail = LGS.getEmailFromID(LID);
            String FullName = LGS.getFullNameFromID(LID);

            classes.Settings STS = new Settings();
            String MailServer = STS.getSMTPServerName();
            String EmailAddress = STS.getSMTPEmailAddress();
            String MailPort = STS.getSMTPServerPort();
            String SSLEnabled = STS.getSMTPSSLEnabled();
            String EmailPassword = STS.getSMTPPassword();

            Boolean SSLValue = false;

            if (SSLEnabled == "2")
            {
                SSLValue = false;
            }
            else
            {
                SSLValue = true;
            }

            //var fromAddress = new MailAddress(EmailAddress, "KAASI Notifications");
            var toAddress = new MailAddress(sendToEmail, FullName);
            string fromPassword = EmailPassword;
            String subject = "KAASI Notifications - Your new password";

            String body = AgentPasswordReset(LID);
            //SmtpClient smtpClient = new SmtpClient();

            //SettingsService SetServ = new

            //AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            SettingsService SetServ = new SettingsService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var SetSMTP = SetServ.GetAll(x => x.SettingName == "SMTPServerName", null, "").SingleOrDefault().SettingsVariable;
            var SetPort = SetServ.GetAll(x => x.SettingName == "SMTPPort", null, "").SingleOrDefault().SettingsVariable;
            var SetUsername = SetServ.GetAll(x => x.SettingName == "SMTPUsername", null, "").SingleOrDefault().SettingsVariable;
            var SetPassword = SetServ.GetAll(x => x.SettingName == "SMTPPassword", null, "").SingleOrDefault().SettingsVariable;
            var setFrom = SetServ.GetAll(x => x.SettingName == "SMTPEmailAddress", null, "").SingleOrDefault().SettingsVariable;
            var setSSL = SetServ.GetAll(x => x.SettingName == "SSLEnabled", null, "").SingleOrDefault().SettingsVariable;

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredentials = new NetworkCredential(SetUsername, SetPassword);
            MailAddress fromAddress = new MailAddress(setFrom, "KAASI Notifications");

            try
            {

                smtpClient.Host = SetSMTP;
                smtpClient.Port = Int32.Parse(SetPort);
                smtpClient.Timeout = 50000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = basicCredentials;

                if (setSSL == "1")
                {
                    smtpClient.EnableSsl = true;
                }
                else
                {
                    smtpClient.EnableSsl = false;
                }

                //var smtp = new SmtpClient
                //{
                //    Host = SetSMTP,
                //    Port = Int32.Parse(SetPort),
                //    EnableSsl = SSLValue,
                //    Timeout = 10000,
                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                //    UseDefaultCredentials = false,
                //    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                //};

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtpClient.Send(kaasiemail);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message.ToString());
            }
        }

        public String SendAccountDetailstoCustomer(String CID, String CustomMessage, String CC)
        {
            classes.Customer Cust = new classes.Customer();

            String CustomerName = Cust.getCustomerFirstName(CID);
            String CustomerEmail = Cust.getCustomerEmailAddress1(CID);

            classes.Settings STS = new Settings();
            String MailServer = STS.getSMTPServerName();
            String EmailAddress = STS.getSMTPEmailAddress();
            String MailPort = STS.getSMTPServerPort();
            String SSLEnabled = STS.getSMTPSSLEnabled();
            String EmailPassword = STS.getSMTPPassword();

            Boolean SSLValue = false;

            if (SSLEnabled == "2")
            {
                SSLValue = false;
            }
            else
            {
                SSLValue = true;
            }

            var fromAddress = new MailAddress(EmailAddress, "KAASI Notifications");
            var toAddress = new MailAddress(CustomerEmail, CustomerName);
            string fromPassword = EmailPassword;
            classes.Agent AGT = new classes.Agent();
            String AgentName = AGT.getAgentNameByID(HttpContext.Current.Session["AgentID"].ToString());
            String subject = AgentName + " Bank Account Details";

            String body = BankAccountEmail(CID, CustomMessage);

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            try
            {
                var smtp = new SmtpClient
                {
                    Host = MailServer,
                    Port = Int32.Parse(MailPort),
                    EnableSsl = SSLValue,
                    Timeout = 50000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                kaasiemail.Subject = subject;
                kaasiemail.IsBodyHtml = true;
                kaasiemail.Body = body;

                kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                    DeliveryNotificationOptions.OnFailure |
                    DeliveryNotificationOptions.Delay;

                kaasiemail.AlternateViews.Add(avHtml);

                smtp.Send(kaasiemail);

                return "Bank Details Have Been Successfully Sent To Customer";
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message.ToString());

                return (ex.Message.ToString());
            }




        }

        public String SendPasswordToCustomer(String Email)
        {
            classes.Logins LGS = new Logins();
            String fulldetails = LGS.getLoginDetailsFromEmailAddress(Email);
            String fullName = String.Empty;
            String Password = String.Empty;
            String Username = String.Empty;

            if (fulldetails != "NORECORDS")
            {
                var splitDetails = fulldetails.Split('|');
                fullName = splitDetails[0].ToString();
                Password = splitDetails[1].ToString();
                Username = splitDetails[2].ToString();

                classes.Settings STS = new Settings();
                String MailServer = STS.getSMTPServerName();
                String EmailAddress = STS.getSMTPEmailAddress();
                String MailPort = STS.getSMTPServerPort();
                String SSLEnabled = STS.getSMTPSSLEnabled();
                String EmailPassword = STS.getSMTPPassword();

                Boolean SSLValue = false;

                if (SSLEnabled == "2")
                {
                    SSLValue = false;
                }
                else
                {
                    SSLValue = true;
                }

                var fromAddress = new MailAddress(EmailAddress, "KAASI Notifications");
                var toAddress = new MailAddress(Email, fullName);
                string fromPassword = EmailPassword;
                String subject = "Your Login details to KAASI";
                String body = PasswordEmail(fullName, Password, Username);

                AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

                try
                {
                    var smtp = new SmtpClient
                    {
                        Host = MailServer,
                        Port = Int32.Parse(MailPort),
                        EnableSsl = SSLValue,
                        Timeout = 50000,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };

                    MailMessage kaasiemail = new MailMessage(fromAddress, toAddress);
                    kaasiemail.Subject = subject;
                    kaasiemail.IsBodyHtml = true;
                    kaasiemail.Body = body;

                    kaasiemail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess |
                        DeliveryNotificationOptions.OnFailure |
                        DeliveryNotificationOptions.Delay;

                    kaasiemail.AlternateViews.Add(avHtml);

                    smtp.Send(kaasiemail);

                    return "Login Details Have Been Successfully Sent To Customer";
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message.ToString());

                    return (ex.Message.ToString());
                }
            }

            return "No Valid Records";

        }

        public String ApprovedEmailBody()
        {
            String output = String.Empty;

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + "<html><body>Your transaction has been successully approved and funds should be available in <COUNTRY> within the next 5 mins.</body></html>";

            return output;
        }

        public String SavedEmailBody(String TID)
        {
            String output = String.Empty;

            int intTID = Int32.Parse(TID);

            TransactionService TransServ = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var TransDetails = TransServ.GetAll(x => x.TransactionID == intTID, null, "").SingleOrDefault();

            CustomerService CustServ = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var CustDetails = CustServ.GetAll(x => x.CustomerID == TransDetails.CustomerID, null, "").SingleOrDefault();

            beneficiaryPaymentMethodService BeneAccServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var BeneAccDetails = BeneAccServ.GetAll(x => x.AccountID == TransDetails.AccountID.ToString(), null, "").SingleOrDefault();

            AgentService AgtServ = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var AgtDetails = AgtServ.GetAll(x => x.AgentID == TransDetails.AgentID, null, "").SingleOrDefault();

            //String CustomerID = Trans.getCustomerIDFromTransactionID(TID);

            var TotalSending = TransDetails.DollarAmount + TransDetails.ServiceCharge;

            // cut code to be inserted here

            // new code by SHEHAN

            output = "<html>";

            output = output + "<head>";
            output = output + "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
            output = output + "<title>KAASI Notification - Transactions Successfully Created</title>";
            output = output + "<meta name='viewport' content='width=device-width, initial-scale=1.0'/>";
            output = output + "</head>";

            // Start
            output = output + "<body>";
            output = output + "<div style='text-align:center;'>";

            output = output + "<table style='width:600px; border:0; cellpadding:0; cellspacing:0;'>";

            output = output + "<tr style='background-color:#00B545; height:50px; margin-left:20px; margin-right:20px;'><td style='font-size:18px; font-weight:bold; color:#FFF; font-family:Verdana;'>" + AgtDetails.AgentName + " - Transaction Notification</td></tr>";

            output = output + "<tr style='background-color:#EFEFEF; margin-left:20px; margin-top:10px; margin-right:20px;'><td><span style='font-size:16px; font-weight:bold; color:#333; font-family:Palatino;'>Your Transaction " + TID + " Has Been Successfully Created.</span> <br> <span style='font-size:14px; color:#333; font-family:Palatino;'><br>Your transaction has been successfully created. Once your funds have been confirmed, we will approve the transaction. Should additional documentation be required, we will contact you directly before processing the transaction.</span><br><br>";

            output = output + "<table style='width:100%; border:0; cellpadding:0; cellspacing:0;'><td style='width:51%; font-size:14px; color:#000; font-family:Trebuchet MS; font-weight:bold;'>CUSTOMER</td><td style='width:49%; font-size:14px; color:#000; font-family:Trebuchet MS; font-weight:bold;'>TRANSACTION</td></tr></table>";

            // Info table
            output = output + "<table style='width:100%; border:10; cellpadding:0; cellspacing:0;'>";

            output = output + "<tr><td style='width:15%; font-size:12px; color:#333; font-weight:bold; font-family:Trebuchet MS;'>CUST ID:</td><td style='width:34%; font-size:12px; color:#666; font-family:Trebuchet MS; font-weight:bold;'>" + TransDetails.CustomerID + "</td><td style='width:2%;'>&nbsp;</td><td style='width:15%; font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold;'>BENEFICIARY:</td><td style='width:34%; font-size:12px; color:#666; font-family:Trebuchet MS;'>" + TransDetails.BeneficiaryName + "</td></tr>";
            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold;'>NAME:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + CustDetails.FullName + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold;'>DATE/TIME:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + TransDetails.CreatedDateTime + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>ADDRESS:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + CustDetails.UnitNo + " " + CustDetails.StreetNo + " " + CustDetails.StreetName + " - " + CustDetails.Suburb + " " + CustDetails.State + " " + CustDetails.Postcode + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>TO REMIT:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + String.Format("{0:C2}", TransDetails.DollarAmount) + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>MOBILE:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + CustDetails.Mobile + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>CHARGES:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + String.Format("{0:C2}", TransDetails.ServiceCharge) + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>&nbsp;</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + "&nbsp;" + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold;'>TOTAL:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + String.Format("{0:C2}", float.Parse(TotalSending.ToString())) + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>&nbsp;</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + "&nbsp;" + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>EX RATE:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + String.Format("{0:N2}", TransDetails.Rate) + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>&nbsp;</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + "&nbsp;" + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>TOT DEPOSIT:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + String.Format("{0:N2}", TransDetails.RemittedAmount) + " " + TransDetails.RemittedCurrency + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>&nbsp;</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + "&nbsp;" + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>BANK:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + BeneAccDetails.BankName + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>&nbsp;</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + "&nbsp;" + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>BRANCH:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + BeneAccDetails.BranchName + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>&nbsp;</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + "&nbsp;" + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>ACCOUNT No:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + BeneAccDetails.AccountNumber + "</td></tr>";

            output = output + "<tr><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>&nbsp;</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + "&nbsp;" + "</td><td>&nbsp;</td><td style='font-size:12px; color:#333; font-family:Trebuchet MS; font-weight:bold'>ACCOUNT Name:</td><td style='font-size:12px; color:#666; font-family:Trebuchet MS;'>" + BeneAccDetails.AccountName + "</td></tr>";

            output = output + "<tr><td>&nbsp;</td></tr>";
            output = output + "</table>";
            output = output + "</td></tr>";

            output = output + "<tr style='background-color:#EFEFEF; margin-left:20px; margin-top:10px; margin-right:20px;'><td><span style='font-size:16px; font-weight:bold; color:#333; font-family:Palatino;'>Further Information</span> <br> <span style='font-size:14px; color:#333; font-family:Palatino;'><br>Should you have any queries or issues with your transactions, please contact " + AgtDetails.AgentName + " on " + AgtDetails.AgentPhone + " or alternatively send an email to " + AgtDetails.AgentEmail + " </span><br><br><span style='font-size:14px; color:#333; font-family:Palatino;'>All transactions paid for by credit/debit card at the office will be processed by FLEXEWALLET PTY LTD and your card statement entry will include the word 'FLEXEWALLET' in the description.</span><br/><br/>";

            output = output + "</table>";

           

            output = output + "<table style='width:600px;'><tr><td style='text-align:left; font-size:12px; font-weight:normal; color:#666; font-family:Palatino;'><br>Powered by Flexewallet</td></tr></table>";

            // End
            output = output + "</div>";
            output = output + "</body>";
            output = output + "</html>";

            // new code by SHEHAN ENDS HERE

            return output;
        }

        public String CancelledEmailBody()
        {
            String output = String.Empty;

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + "<html><body>Your transaction has been cancelled. Your funds should be refunded within the next 3-5 business days.</body></html>";

            return output;
        }

        public String NewAccountPasswordBody(String LID)
        {
            String output = String.Empty;

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + @"<html><body>Your account with KAASI has been setup. <br/><br/> To login to the system, please proceed to http://remittance.flexewallet.com
                            <br/><br/>Your username: xxxxxx<br/>Your Password:xxxxx</body></html>";

            return output;
        }

        public String NewAgentEmailBody(String LID)
        {
            String output = String.Empty;
            classes.Logins LGS = new Logins();

            String Username = LGS.getUsernameFromID(LID);
            String Password = LGS.getPasswordFromID(LID);

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + @"<html><body>Your account with KAASI has been setup. <br/><br/> To login to the system, please proceed to http://remittance.flexewallet.com
                            <br/><br/>Your username: " + Username + " <br/>Your Password: " + Password + " </body></html>";

            return output;
        }

        public String AgentPasswordReset(String LID)
        {
            String output = String.Empty;
            classes.Logins LGS = new Logins();

            String Password = LGS.getPasswordFromID(LID);

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + @"<html><body>Your password has been reset <br/><br/> To login to the system, please proceed to http://remittance.flexewallet.com
                            <br/><br/>Your Password: " + Password + " </body></html>";

            return output;
        }

        public String BankAccountEmail(String CID, String CustomMessage)
        {
            String output = String.Empty;
            classes.Logins LGS = new Logins();
            classes.Banking BKN = new classes.Banking();
            classes.Customer CTS = new classes.Customer();
            classes.Agent AGT = new classes.Agent();
            classes.MasterBankClass MBC = new classes.MasterBankClass();

            //String Password = LGS.getPasswordFromID(LID);

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + @"<html><body>";
            output += "Hello " + CTS.getCustomerFullName(CID) + "<br/><br/>";
            output += "Please find below our bank account details for your remittance needs.<br/><br/>";
            List<String> AccountList = new List<String>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT MasterBankAccountID FROM dbo.AgentAssignedBanks WHERE AgentID = " + HttpContext.Current.Session["AgentID"].ToString();
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        AccountList.Add(sdr["MasterBankAccountID"].ToString());
                    }

                }
                conn.Close();
            }

            foreach (var Account in AccountList)
            {
                output += "BSB: " + MBC.getMasterBankBSBByAccountID(Account) + " Account No: " + MBC.getMasterBankAccountNumberbyAccountID(Account) + " Name: " + MBC.getMasterBankNameByAccountID(Account) + " Bank: " + MBC.getMasterBankByAccountID(Account) + "<br/>";
            }
            if (CustomMessage != "")
            {
                output += "<br/>" + CustomMessage + "<br/>";
            }

            output += @"<br/>Should you have any queries, or require further information, please do not hesitate to contact us.<br/><br/>";

            output += AGT.getAgentCompanyName(HttpContext.Current.Session["AgentID"].ToString()) + "<br/>" + AGT.getAgentFullAddress(HttpContext.Current.Session["AgentID"].ToString()) + "<br/>" + AGT.getAgentTelephoneNumber(HttpContext.Current.Session["AgentID"].ToString());
            output += "</body></html>";

            return output;
        }

        public String PasswordEmail(String Fullname, String Password, String Username)
        {
            String output = String.Empty;

            output = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2//EN'><html>";
            output = output + @"<html><body>";
            output += "Hello " + Fullname + "<br/><br/>";
            output += "Please find below your login details to KAASI.<br/><br/>";
            output += @"<br/>Username: " + Username + "<br/><br/>Password: " + Password + "<br/><br/>";
            output += "</body></html>";

            return output;
        }

        private int getAgentIDFromTransactionID(String TID)
        {
            TransactionService TransServ = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            int intTID = Int32.Parse(TID);

            var AgentID = TransServ.GetAll(x => x.TransactionID == intTID, null, "").SingleOrDefault().AgentID;

            return Int32.Parse(AgentID.ToString());
        }

        private Kapruka.Repository.Agent getAgentSMTPSettings(int AID)
        {
            AgentService AgtServ = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            var SMTPSettings = AgtServ.GetAll(x => x.AgentID == AID, null, "").SingleOrDefault();

            return SMTPSettings;
        }
    }
}