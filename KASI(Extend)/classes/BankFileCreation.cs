﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using System.Text;
using System.IO;
using System.Globalization;
using System.IO.Compression;
using Kapruka.Enterprise;
using System.Text.RegularExpressions;
using Kapruka.Enterprise;

namespace KASI_Extend_.classes
{
    public class BankFileCreation
    {

        public String createBOCBankFile(String CountryID, String BankCode, String AccountID)
        {
            classes.Transactions transClass = new classes.Transactions();
            Beneficiary BENE = new Beneficiary();
            BeneficiaryAccounts BENEACC = new BeneficiaryAccounts();
            classes.Banking BKN = new Banking();
            classes.Currencies CUR = new Currencies();
            //String BankCode = HttpContext.Current.Request.QueryString["BankCode"].ToString();
            String AgentID = HttpContext.Current.Session["AgentID"].ToString();
            String delimiter = ",";
            String CurrencyList = BKN.getCurrenciesLinkedToForeignBankAccount(HttpContext.Current.Session["AgentID"].ToString(), CountryID, BankCode, AccountID);
            String[] Currencies = CurrencyList.Split(',');
            String CurrencyCodeList = String.Empty;

            foreach (var Currency in Currencies)
            {
                CurrencyCodeList += "'" +  CUR.getCurrencyCodeFromID(Currency) + "',";
            }

            int Length = CurrencyCodeList.Length;
            CurrencyCodeList = CurrencyCodeList.Substring(0, (Length - 1));

            Customer CUS = new Customer();
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filepath = String.Empty;
            String filename = "BOC - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            filepath = HttpContext.Current.Server.MapPath(@"\BankFileCreation\" + filename);
            //Uri nagivateUri = new Uri(System.Web.HttpContext.Current.Request.Url, filepath);

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyyMMdd");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = @"SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID 
                            FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AgentID 
                            + " AND RemittedCurrency IN (" + CurrencyCodeList + ")";
            String outputString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString = outputString + sdr["TransactionID"].ToString() + delimiter;
                            outputString = outputString + AccountID + delimiter;
                            String remAmount = String.Format("{0:N2}", float.Parse(sdr["RemittedAmount"].ToString()));
                            outputString = outputString + remAmount.ToString().Replace(".", "").Replace(",", "") + delimiter;
                            outputString = outputString + sdr["RemittedCurrency"].ToString() + delimiter;
                            outputString = outputString + now + delimiter;
                            outputString = outputString + CUS.getCustomerFullName(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + sdr["BeneficiaryName"].ToString() + delimiter;
                            outputString = outputString + BENE.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + BENE.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + BENEACC.getBeneficicaryAccountNumberBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter;
                            outputString = outputString + BankCode + delimiter;
                            outputString = outputString + BENEACC.getBeneficiaryAccountBranchNameBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter;
                            outputString = outputString + " " + delimiter;
                            outputString = outputString + BENE.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString()) + Environment.NewLine;
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }

                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                    }
                    return ("File Created^" + filename);
                }
                else
                {
                    return ("BOC File Could Not Be Created^");
                }

            }
        }

        public String createCOMBankFile(String CountryID, String BankCode, String AccountID)
        {
            classes.Transactions transClass = new classes.Transactions();
            classes.Customer CUST = new Customer();
            classes.Beneficiary BENE = new Beneficiary();
            classes.BeneficiaryAccounts BENEACC = new BeneficiaryAccounts();
            classes.Currencies CUR = new Currencies();
            classes.Banking BKN = new Banking();
            //String BankCode = Request.QueryString["BankCode"].ToString();
            String AID = HttpContext.Current.Session["AgentID"].ToString();
            String strDelimiter = ",";
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filename = "COM - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv";
            String now = String.Empty;
            now = DateTime.Now.ToString("yyyyMMdd");


            String CurrencyList = BKN.getCurrenciesLinkedToForeignBankAccount(HttpContext.Current.Session["AgentID"].ToString(), CountryID, BankCode, AccountID);
            String[] Currencies = CurrencyList.Split(',');
            String CurrencyCodeList = String.Empty;

            foreach (var Currency in Currencies)
            {
                CurrencyCodeList += "'" + CUR.getCurrencyCodeFromID(Currency) + "',";
            }

            int Length = CurrencyCodeList.Length;
            CurrencyCodeList = CurrencyCodeList.Substring(0, (Length - 1));

            String cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            StringBuilder sb = new StringBuilder();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = cs;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, RoutingBank 
                                    FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AID 
                                    + " AND RemittedCurrency IN (" + CurrencyCodeList + ")";
                    cmd.Connection = conn;
                    conn.Open();

                    beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

                    
                    sb.Append("REF_NO." + strDelimiter + "ACCOUNT_NO." + strDelimiter + "AMOUNT" + strDelimiter + "CURRENCY" + strDelimiter + "REM_TYPE" + strDelimiter + "CHG_FROM" + strDelimiter + "CUST_NAME" + strDelimiter + "DETAILS" + strDelimiter + "BENE_NAME" + strDelimiter + "BENE_ADDRESS" + strDelimiter + "BENE_TEL" + strDelimiter + "BENE_ID_NO" + strDelimiter + "BENE_ACCNO" + strDelimiter + "BENE_BANK_CODE" + strDelimiter + "BENE_BRANCH_CODE" + strDelimiter + "BANK_TO_BANK_INFO\r\n");

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                sb.Append(sdr["TransactionID"].ToString() + strDelimiter);
                                sb.Append(AccountID + strDelimiter);
                                sb.Append(sdr["RemittedAmount"].ToString().Replace(",", "") + strDelimiter);
                                sb.Append(sdr["RemittedCurrency"].ToString() + strDelimiter);
                                String theAccountID = sdr["AccountID"].ToString();
                                var BPMDetails = BPMServ.GetAll(x => x.AccountID == theAccountID, null, "").SingleOrDefault();                                
                                String typeOfRemittance = OwnRemittance(sdr["RoutingBank"].ToString(), BPMDetails.BankID, BPMDetails.BranchName, BPMDetails.AccountNumber);
                                if (typeOfRemittance == "CASH")
                                {
                                    sb.Append("C" + strDelimiter);
                                }
                                else if (typeOfRemittance == "EQUAL")
                                {
                                    sb.Append("A" + strDelimiter);
                                }
                                else
                                {
                                    sb.Append("O" + strDelimiter);
                                }
                                
                                sb.Append("B" + strDelimiter);
                                sb.Append(CUST.getCustomerFullName(sdr["CustomerID"].ToString()) + strDelimiter);
                                sb.Append("" + strDelimiter);
                                sb.Append(BENE.getBeneficiaryName(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                int BeneAddyLength = BENE.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()).Length;
                                if (BeneAddyLength > 60)
                                {
                                    sb.Append(BENE.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()).Substring(0, 60) + strDelimiter);
                                }
                                else
                                {
                                    sb.Append(BENE.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                }
                                sb.Append(BENE.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                sb.Append(BENE.getBeneficiaryNIC(sdr["BeneficiaryID"].ToString()) + strDelimiter);
                                if (typeOfRemittance != "CASH")
                                {
                                    sb.Append(BENEACC.getBeneficicaryAccountNumberBasedOnAccountID(sdr["AccountID"].ToString()) + strDelimiter);
                                }
                                else
                                {
                                    sb.Append("" + strDelimiter);
                                }
                                
                                sb.Append("7056" + strDelimiter);
                                sb.Append("2" + strDelimiter);
                                sb.Append("" + strDelimiter);
                                sb.Append("\r\n");
                                TID.Add(sdr["TransactionID"].ToString());
                            }
                        }
                        else
                        {
                            sb.Append("NoRecords");
                        }
                    }
                    conn.Close();
                }
            }
            try
            {
                string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                StreamWriter file = new StreamWriter(filePath);
                file.WriteLine(sb.ToString());
                file.Close();
                filehasbeencomplete = true;
            }
            catch (Exception ex)
            {
                filehasbeencomplete = false;

            }

            if (filehasbeencomplete == true)
            {
                foreach (var Transaction in TID)
                {
                    transClass.updateTransBankFileInsertion(Transaction);
                }

                return ("File Created^" + filename);
            }
            else
            {
                return ("COM File Could Not Be Created^nofile");
            }
        }

        public String createNSBBankFile(String CountryID, String BankCode, String AccountID)
        {
            classes.Transactions transClass = new classes.Transactions();
            String delimiter = "|";
            String outputString = String.Empty;
            String AID = HttpContext.Current.Session["AgentID"].ToString();
            //String BankCode = Request.QueryString["BankCode"].ToString();
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filename = "NSB - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            classes.Currencies CUR = new Currencies();
            classes.Banking BKN = new Banking();
            BeneficiaryAccounts beneAcc = new BeneficiaryAccounts();
            classes.Transactions trans = new classes.Transactions();

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyyMMdd");

            String CurrencyList = BKN.getCurrenciesLinkedToForeignBankAccount(HttpContext.Current.Session["AgentID"].ToString(), CountryID, BankCode, AccountID);
            String[] Currencies = CurrencyList.Split(',');
            String CurrencyCodeList = String.Empty;

            foreach (var Currency in Currencies)
            {
                CurrencyCodeList += "'" + CUR.getCurrencyCodeFromID(Currency) + "',";
            }

            int Length = CurrencyCodeList.Length;
            CurrencyCodeList = CurrencyCodeList.Substring(0, (Length - 1));

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = @"SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, RoutingBank, Purpose, SourceOfFunds
                          FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AID
                          + " AND RemittedCurrency IN (" + CurrencyCodeList + ")";
            outputString = outputString + "currencyType|agentReference|disbursalMode|amount|valueDate|beneficiaryName|beneficiaryBankCode|beneficiaryBank|beneficiaryBankBranchCode|beneficiaryBankBranch|beneficiaryBankAccountNumber|senderName|senderCountry|purposeOfRemittance|source|remittanceType" + Environment.NewLine;

            BeneficiaryService BeneServ = new BeneficiaryService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            CustomerService CustServ = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));


            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString += sdr["RemittedCurrency"].ToString() + delimiter; // currencyType
                            outputString += sdr["TransactionID"].ToString() + delimiter; // Transaction ID
                            var theAccountID = sdr["AccountID"].ToString();
                            var BPMdetails = BPMServ.GetAll(x => x.AccountID == theAccountID, null, "").SingleOrDefault();
                            var whichAccount = OwnRemittance(sdr["RoutingBank"].ToString(), BPMdetails.BankID, BPMdetails.BranchName, BPMdetails.AccountNumber);
                            if (whichAccount == "CASH")
                            {
                                outputString += "DIRECT" + delimiter;
                            }
                            else
                            {
                                outputString += "A/C TRANSFER" + delimiter;
                            }
                            String remAmount = String.Format("{0:N0}", float.Parse(sdr["RemittedAmount"].ToString()));
                            outputString += remAmount.ToString().Replace(".", "").Replace(",", "") + delimiter; //Transaction Amount
                            DateTime today = DateTime.Now;
                            String justToday = today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            outputString += justToday + delimiter;
                            outputString += sdr["BeneficiaryName"].ToString() + delimiter;
                            var theBeneID = Int32.Parse(sdr["BeneficiaryID"].ToString());
                            var BeneDetails = BeneServ.GetAll(x => x.BeneficiaryID == theBeneID, null, "").SingleOrDefault();

                            outputString += BeneDetails.NationalityCardID + delimiter;
                            outputString += BPMdetails.BankID + delimiter;
                            outputString += BPMdetails.BankName + delimiter;
                            outputString += BPMdetails.BranchID.ToString().PadLeft(3, '0') + delimiter;
                            outputString += BPMdetails.BranchName + delimiter;
                            outputString += BPMdetails.AccountNumber + delimiter;
                            outputString += BeneDetails.AddressLine1 + delimiter;
                            outputString += BeneDetails.AddressLine2 + delimiter;
                            outputString += BeneDetails.Country + delimiter;
                            outputString += BeneDetails.Mobile + delimiter;

                            var theCustID = Int32.Parse(sdr["CustomerID"].ToString());
                            var CustDetails = CustServ.GetAll(x => x.CustomerID == theCustID, null, "").SingleOrDefault();

                            outputString += CustDetails.FullName + delimiter;
                            outputString += CustDetails.UnitNo + " " + CustDetails.StreetNo + " " + CustDetails.StreetName + delimiter;
                            outputString += CustDetails.Suburb + " " + CustDetails.State + " " + CustDetails.Postcode + " " + delimiter;
                            outputString += CustDetails.Country + delimiter;
                            outputString += CustDetails.Mobile + delimiter;
                            outputString += sdr["Purpose"].ToString() + delimiter;
                            outputString += "" + delimiter;
                            outputString += "" + delimiter;
                            outputString += sdr["SourceOfFunds"].ToString() + delimiter;
                            outputString += "FILE" + Environment.NewLine;

                            //outputString = outputString + sdr["TransactionID"].ToString() + delimiter;  // Transaction ID
                            //outputString = outputString + sdr["RemittedCurrency"].ToString() + delimiter; // Remitted Currency
                            //outputString = outputString + disbursalMethod(sdr["AccountID"].ToString()) + delimiter; // Disbursal Mode

                            //outputString = outputString + 
                            //outputString = outputString + now + delimiter; // Today's date in DD/MM/YYYY
                            //outputString = outputString + (bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString()).Length > 60 ? bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString()).Substring(0, 60) : bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString())) + delimiter; // Beneficiary's name
                            //outputString = outputString + bene.getBeneficiaryNIC(sdr["BeneficiaryID"].ToString()) + delimiter; // Beneficiary NIC
                            //outputString = outputString + beneAcc.getBeneficiaryAccountBankCodeBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter; // Beneficiary Bank Code
                            //outputString = outputString + beneAcc.getBeneficiaryAccountBankNameBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter; // Beneficiary Bank Name
                            //outputString = outputString + (beneAcc.getBeneficiaryAccountBranchCodeBasedOnAccountID(sdr["AccountID"].ToString()).Length > 3 ? beneAcc.getBeneficiaryAccountBranchCodeBasedOnAccountID(sdr["AccountID"].ToString()).Substring(0, 3) : beneAcc.getBeneficiaryAccountBranchCodeBasedOnAccountID(sdr["AccountID"].ToString())) + delimiter; // Beneficiary Branch Code
                            //outputString = outputString + beneAcc.getBeneficiaryAccountBranchNameBasedOnAccountID(sdr["AccountID"].ToString()) + delimiter; // Beneficiary Branch Name
                            //outputString = outputString + beneAcc.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()) + delimiter; // Account Number
                            //outputString = outputString + (bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()).Length > 200 ? bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()).Substring(0, 200) : bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString())) + delimiter; // Beneficiary Address 1
                            //outputString = outputString + delimiter; // Beneficiary Address 2
                            //outputString = outputString + delimiter; // Beneficiary Address 3
                            //outputString = outputString + "SRI LANKA" + delimiter; // Beneficiary Country
                            //outputString = outputString + bene.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString()) + delimiter; // Beneficiary Telephone Number
                            //outputString = outputString + (cust.getCustomerFullName(sdr["CustomerID"].ToString()).Length > 60 ? cust.getCustomerFullName(sdr["CustomerID"].ToString()).Substring(0, 60) : cust.getCustomerFullName(sdr["CustomerID"].ToString())) + delimiter; // Customer's Name
                            //outputString = outputString + cust.getCustomerUnitNo(sdr["CustomerID"].ToString()) + " " + cust.getCustomerStreetNo(sdr["CustomerID"].ToString()) + " " + cust.getCustomerStreetName(sdr["CustomerID"].ToString()) + delimiter; // Customer's Address Line 1
                            //outputString = outputString + cust.getCustomerState(sdr["CustomerID"].ToString()) + " " + cust.getCustomerPostcode(sdr["CustomerID"].ToString()) + delimiter; // Customer Address Line 2
                            //outputString = outputString + cust.getCustomerCountry(sdr["CustomerID"].ToString()) + delimiter; // Customer Country
                            //outputString = outputString + cust.getCustomerMobile(sdr["CustomerID"].ToString()) + delimiter; // Customer Contact Number
                            //outputString = outputString + delimiter; // Sender's Message
                            //outputString = outputString + delimiter; // Agent's remarks
                            //outputString = outputString + trans.getTransactionPurpose(sdr["TransactionID"].ToString()) + delimiter; // Purpose of Transaction
                            //outputString = outputString + trans.getTransactionSourceOfFunds(sdr["TransactionID"].ToString()) + delimiter; // Source of Funds
                            //outputString = outputString + "FILE" + Environment.NewLine;
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                    }
                    return ("File Created^" + filename);
                }
                else
                {
                    return ("BOC File Could Not Be Created^");
                }

            }
        }

        public String createSAMBankFile(String CountryID, String BankCode, String AccountID)
        {
            classes.Transactions transClass = new classes.Transactions();
            //String BankCode = Request.QueryString["BankCode"].ToString();
            String AID = HttpContext.Current.Session["AgentID"].ToString();
            String delimiter = "|";
            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts bAccount = new BeneficiaryAccounts();
            classes.Currencies CUR = new Currencies();
            classes.Banking BKN = new Banking();
            float RAmount = 0;
            String BeneID = String.Empty;
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            String filename = "SMP - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            String now = String.Empty;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            String CurrencyList = BKN.getCurrenciesLinkedToForeignBankAccount(HttpContext.Current.Session["AgentID"].ToString(), CountryID, BankCode, AccountID);
            String[] Currencies = CurrencyList.Split(',');
            String CurrencyCodeList = String.Empty;

            foreach (var Currency in Currencies)
            {
                CurrencyCodeList += "'" + CUR.getCurrencyCodeFromID(Currency) + "',";
            }

            int Length = CurrencyCodeList.Length;
            CurrencyCodeList = CurrencyCodeList.Substring(0, (Length - 1));

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = @"SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID 
                            FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AID
                            + " AND RemittedCurrency IN (" + CurrencyCodeList + ")";
            String outputString = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            outputString = outputString + sdr["TransactionID"].ToString() + delimiter;
                            outputString = outputString + now + delimiter;
                            outputString = outputString + sdr["RemittedCurrency"].ToString() + delimiter;
                            RAmount = float.Parse(sdr["RemittedAmount"].ToString());
                            outputString = outputString + String.Format("{0:N2}", RAmount).Replace(",", "") + delimiter;
                            outputString = outputString + cust.getCustomerFullName(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + sdr["CustomerID"].ToString() + delimiter;
                            outputString = outputString + cust.getCustomerMobile(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + cust.getCustomerFullAddress(sdr["CustomerID"].ToString()) + delimiter;
                            outputString = outputString + sdr["BeneficiaryName"].ToString() + delimiter;

                            BeneID = bene.getBeneficiaryNIC(sdr["BeneficiaryID"].ToString());
                            if (BeneID == "")
                            {
                                BeneID = bene.getBeneficiaryPassport(sdr["BeneficiaryID"].ToString());

                                if (BeneID == "")
                                {
                                    BeneID = "";
                                }
                            }

                            outputString = outputString + BeneID + delimiter;
                            outputString = outputString + bene.getBeneficiaryPhone(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString()) + delimiter;
                            outputString = outputString + delimiter;

                            var BranchID = getBranchIDFromBankCodeAndBranchName(bAccount.getBeneficiaryBankCode(sdr["TransactionID"].ToString()), bAccount.getBeneficiaryBranchName(sdr["TransactionID"].ToString()));

                            outputString = outputString + BranchID.PadLeft(3, '0') + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryBranchName(sdr["TransactionID"].ToString()) + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryBankCode(sdr["TransactionID"].ToString()) + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryBankNameOnTID(sdr["TransactionID"].ToString()) + delimiter;
                            outputString = outputString + bAccount.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()) + delimiter;

                            String SendingBank = bAccount.getBeneficiaryBankCode(sdr["TransactionID"].ToString());

                            if (SendingBank != "7278")
                            {
                                if (bAccount.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()).Contains("Advice") || bAccount.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()).Contains("Advise"))
                                {
                                    outputString += "POI" + Environment.NewLine;
                                }
                                else
                                {
                                    outputString = outputString + "SLI" + Environment.NewLine;
                                }
                                
                            }
                            else
                            {
                                if (bAccount.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()).Contains("Advice") || bAccount.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()).Contains("Advise"))
                                {
                                    outputString += "POI" + Environment.NewLine;
                                }
                                else
                                {
                                    outputString = outputString + "SBA" + Environment.NewLine;
                                }
                                    
                            }
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                    }
                    return ("File Created^" + filename);
                }
                else
                {
                    return ("Sampath File Could Not Be Created^");
                }
            }
        }

        public String createSEYBankFile(String CountryID, String BankCode, String AccountID)
        {
            try
            {
                int AgentID = Int32.Parse(HttpContext.Current.Session["AgentID"].ToString());
                int intCID = Int32.Parse(CountryID);

                AgentForeignBankSettingService AFBSServ = new AgentForeignBankSettingService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                var AGTDetails = AFBSServ.GetAll(x => x.AgentID == AgentID && x.CountryID == intCID && x.BankID == BankCode && x.ForeignBankAccountNumber == AccountID, null, "").SingleOrDefault();
                CurrencyService CurServ = new CurrencyService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                int intCurID = Int32.Parse(AGTDetails.CurrencyID);
                var CurDetails = CurServ.GetAll(x => x.CurrencyID == intCurID, null, "").SingleOrDefault();
                //String BankCode = Request.QueryString["BankCode"].ToString();
                DateTime LKRDT = DateTime.Now;
                String formattedDateTime = LKRDT.ToString("dd/MM/yy HH:mm:ss");
                String HeaderFormattedDateTime = LKRDT.ToString("ss:mm:HH yy/MM/dd");

                String UploadDate = String.Empty;
                var charsToRemove = new string[] { "/", ",", ":", "-", "PM", " ", "AM" };
                
                foreach (var c in charsToRemove)
                {
                    formattedDateTime = formattedDateTime.Replace(c, "");
                    HeaderFormattedDateTime = HeaderFormattedDateTime.Replace(c, "");
                }

                Random generator = new Random();
                String uniquenumber = generator.Next(100, 999).ToString();
                UploadDate = formattedDateTime.Substring(0, 7);
                string ReverseUploadDate = "1" + HeaderFormattedDateTime.Substring((HeaderFormattedDateTime.Length - 6), 6);
                formattedDateTime = formattedDateTime + uniquenumber;
                string folderName = "Sey";
                Array.ForEach(Directory.GetFiles(HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + folderName), File.Delete);
                string detFile= BuilkLKRDetailFile(formattedDateTime, UploadDate, BankCode, folderName, ReverseUploadDate, AGTDetails.BankFileUsername, AGTDetails.ExchangeHouseCode, AGTDetails.UploadPIN, CurDetails.CurrencyCode);
                

                var splitdetFile = detFile.Split('|');
                var Rtotal = splitdetFile[1];
                var nbtotal = splitdetFile[2];
                BuildLKRHeaderFile(formattedDateTime, BankCode, UploadDate, AccountID, folderName, Rtotal, nbtotal, AGTDetails.BankFileUsername, ReverseUploadDate, AGTDetails.ExchangeHouseCode, CurDetails.CurrencyCode);


                ZipFile.CreateFromDirectory(HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + folderName, HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + formattedDateTime + ".zip");
             
                return ("File Created^" + formattedDateTime + ".zip");
            }
            catch (Exception ex)
            {
                return ("Seylan Filecould not be Created^" + ex);
            }
        }
        
        public String createHNBBankFile(String CountryID, String BankCode, String AccountID)
        {
            classes.Transactions transClass = new classes.Transactions();
            //String BankCode = Request.QueryString["BankCode"].ToString();
            String delimiter = "|";
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            classes.Currencies CUR = new Currencies();
            classes.Banking BKN = new Banking();
            String AID = HttpContext.Current.Session["AgentID"].ToString();


            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts bAccount = new BeneficiaryAccounts();
            //String RAmount = String.Empty;
            String BeneID = String.Empty;
            String filename = "HNB - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            String now = String.Empty;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            String CurrencyList = BKN.getCurrenciesLinkedToForeignBankAccount(HttpContext.Current.Session["AgentID"].ToString(), CountryID, BankCode, AccountID);
            String[] Currencies = CurrencyList.Split(',');
            String CurrencyCodeList = String.Empty;

            foreach (var Currency in Currencies)
            {
                CurrencyCodeList += "'" + CUR.getCurrencyCodeFromID(Currency) + "',";
            }

            int Length = CurrencyCodeList.Length;
            CurrencyCodeList = CurrencyCodeList.Substring(0, (Length - 1));

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = @"SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, Rate, AuthorisationDateTime, RoutingBank
                        FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AID 
                        + " AND RemittedCurrency IN (" + CurrencyCodeList + ")";
            String outputString = String.Empty;
            String HNBAccountNumber = AccountID;
            String CFullName = String.Empty;
            int CFullNameLength = 0;
            String empty = String.Empty;

            TransactionService TransServ = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            var IntTransID = Int32.Parse(sdr["TransactionID"].ToString());
                            var TransDetails = TransServ.GetAll(x => x.TransactionID == IntTransID, null, "").SingleOrDefault();

                            var BPMDetails = BPMServ.GetAll(x => x.AccountID == TransDetails.AccountID.ToString(), null, "").SingleOrDefault();
                            outputString = outputString + sdr["TransactionID"].ToString().PadRight(30);
                            outputString = outputString + HNBAccountNumber.PadRight(20);
                            var RAmount = float.Parse(sdr["RemittedAmount"].ToString());
                            outputString = outputString + String.Format("{0:N2}",RAmount).Replace(",","").PadRight(20);
                            outputString = outputString + sdr["RemittedCurrency"].ToString().PadRight(3);
                            String result = OwnRemittance(sdr["RoutingBank"].ToString(), bAccount.getBeneficiaryBankCode(sdr["TransactionID"].ToString()), bAccount.getBeneficiaryBranchName(sdr["TransactionID"].ToString()), bAccount.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString()));
                            if (result == "EQUAL")
                            {
                                outputString = outputString + "A";
                            }
                            else if (result == "CASH")
                            {
                                outputString = outputString + "C";
                            }
                            else
                            {
                                outputString = outputString + "O";
                            }
                            outputString = outputString + "B";

                            CFullName = cust.getCustomerFullName(sdr["CustomerID"].ToString());
                            CFullNameLength = CFullName.Length;

                            if (CFullNameLength > 55)
                            {
                                CFullName = CFullName.Substring(0, 55);
                            }
                            else
                            {
                                CFullName = CFullName.PadRight(55);
                            }

                            outputString = outputString + CFullName;

                            String CEmailAddress = cust.getCustomerEmailAddress1(sdr["CustomerID"].ToString());
                            int CEmailAddressLength = CEmailAddress.Length;

                            if (CEmailAddressLength > 30)
                            {
                                CEmailAddress = CEmailAddress.Substring(0, 30);
                            }
                            else
                            {
                                CEmailAddress = CEmailAddress.PadRight(30);
                            }
                            outputString = outputString + CEmailAddress;

                            String CMobile = cust.getCustomerMobile(sdr["CustomerID"].ToString());
                            int CMobileLength = CMobile.Length;

                            if (CMobileLength > 20)
                            {
                                CMobile = CMobile.Substring(0, 20);
                            }
                            else
                            {
                                CMobile = CMobile.PadRight(20);
                            }
                            outputString = outputString + CMobile;
                            outputString = outputString + empty.PadRight(60);

                            String BeneName = sdr["BeneficiaryName"].ToString();
                            int BeneNameLength = BeneName.Length;

                            if (BeneNameLength > 55)
                            {
                                BeneName = BeneName.Substring(0, 55);
                            }
                            else
                            {
                                BeneName = BeneName.PadRight(55);
                            }

                            outputString = outputString + BeneName;

                            String BeneAddress = bene.getBeneficiaryFullAddress(sdr["BeneficiaryID"].ToString());
                            int BeneAddressLength = BeneAddress.Length;

                            if (BeneAddressLength > 100)
                            {
                                BeneAddress = BeneAddress.Substring(0, 100);
                            }
                            else
                            {
                                BeneAddress = BeneAddress.PadRight(100);
                            }

                            outputString = outputString + BeneAddress;

                            String BenePhone = bene.getBeneficiaryPhone(sdr["BeneficiaryID"].ToString());
                            int BenePhoneLength = BenePhone.Length;

                            if (BenePhoneLength > 20)
                            {
                                BenePhone = BenePhone.Substring(0, 20);
                            }
                            else
                            {
                                BenePhone = BenePhone.PadRight(20);
                            }

                            outputString = outputString + BenePhone;

                            String BeneNIC = bene.getBeneficiaryNIC(sdr["BeneficiaryID"].ToString());
                            int BeneNICLength = BeneNIC.Length;

                            if (BeneNICLength > 30)
                            {
                                BeneNIC = BeneNIC.Substring(0, 30);
                            }
                            else
                            {
                                BeneNIC = BeneNIC.PadRight(30);
                            }

                            outputString = outputString + BeneNIC;

                            String BeneEmail = bene.getBeneficiaryEmail(sdr["BeneficiaryID"].ToString());
                            int BeneEmailLength = BeneEmail.Length;

                            if (BeneEmailLength > 30)
                            {
                                BeneEmail = BeneEmail.Substring(0, 30);
                            }
                            else
                            {
                                BeneEmail = BeneEmail.PadRight(30);
                            }

                            outputString = outputString + BeneEmail;

                            String BeneMobile = bene.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString());
                            int BeneMobileLength = BeneMobile.Length;

                            if (BeneMobileLength > 20)
                            {
                                BeneMobile = BeneMobile.Substring(0, 20);
                            }
                            else
                            {
                                BeneMobile = BeneMobile.PadRight(20);
                            }

                            outputString = outputString + BeneMobile;

                            String BeneAccountNumber = BPMDetails.AccountNumber;
                            int BeneANumberLength = BeneAccountNumber.Length;

                            if (BeneANumberLength > 20)
                            {
                                BeneAccountNumber = BeneAccountNumber.Substring(0, 20);
                            }
                            else
                            {
                                BeneAccountNumber = BeneAccountNumber.PadRight(20);
                            }

                            outputString = outputString + BeneAccountNumber;

                            outputString = outputString + bAccount.getBeneficiaryBankCodeByBID(sdr["BeneficiaryID"].ToString()).PadRight(4);




                            //var BranchID = getBranchIDFromBankCodeAndBranchName(bAccount.getBeneficiaryBankCodeByBID(sdr["BeneficiaryID"].ToString()), TransDetails.BankBranch);
                            var BranchID = BPMDetails.BranchID.ToString();

                            outputString = outputString + BranchID.PadLeft(3, '0');
                            outputString = outputString + empty.PadRight(40);
                            outputString = outputString + empty.PadRight(7);
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + empty.PadRight(20);
                            outputString = outputString + "2222".PadRight(6);
                            //outputString = outputString + sdr["AuthorisationDateTime"].ToString().Substring(0, 9).Replace("-", "");
                            outputString = outputString + empty.PadRight(10);
                            outputString = outputString + empty.PadRight(10);
                            outputString = outputString + empty.PadRight(8);
                            outputString = outputString + "1111".PadRight(6);
                            outputString = outputString + "\r\n";
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }
                }
                conn.Close();
                try
                {
                    string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, outputString);
                    filehasbeencomplete = true;
                }
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                    }
                    return ("File Created^" + filename);
                }
                else
                {
                    return ("HNB File Could Not Be Created^noreport");
                }
            }
        }

        public String createPBBankFile(String CountryID, String BankCode, String AccountID)
        {
            classes.Transactions transClass = new classes.Transactions();
            //String BankCode = Request.QueryString["BankCode"].ToString();
            String strDelimiter = "";
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            CultureInfo ci = new CultureInfo("en-GB");
            classes.Banking BKN = new Banking();
            classes.Currencies CUR = new Currencies();
            String AID = HttpContext.Current.Session["AgentID"].ToString();

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyymmdd");
            int agentCode = 8386;
            //int numOfRem = 3;
            String cs = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            StringBuilder sbH = new StringBuilder();
            StringBuilder sb = new StringBuilder();

            String CurrencyList = BKN.getCurrenciesLinkedToForeignBankAccount(HttpContext.Current.Session["AgentID"].ToString(), CountryID, BankCode, AccountID);
            String[] Currencies = CurrencyList.Split(',');
            String CurrencyCodeList = String.Empty;

            foreach (var Currency in Currencies)
            {
                CurrencyCodeList += "'" + CUR.getCurrencyCodeFromID(Currency) + "',";
            }

            int Length = CurrencyCodeList.Length;
            CurrencyCodeList = CurrencyCodeList.Substring(0, (Length - 1));

            int TotTrans = 0;
            String filename = agentCode.ToString() + DateTime.Now.ToString("ddMMyyyy") + "001R.txt"; // Change 001 to file sequence
            //sb.Append("\r\n");
            int index = 1;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = cs;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, CreatedDateTime
                            FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AID
                            + " AND RemittedCurrency IN (" + CurrencyCodeList + ")";
                    cmd.Connection = conn;
                    conn.Open();

                    BeneficiaryService BeneServ = new BeneficiaryService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                    beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                    CustomerService CustServ = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {

                            while (sdr.Read())
                            {
                                sb.Append("\r\n");
                                sb.Append("02");
                                sb.Append("796100100240922".PadRight(19, ' '));
                                sb.Append(filename.Substring(0, filename.Length-4).PadRight(16, ' '));
                                sb.Append(index.ToString().PadLeft(6, '0'));
                                sb.Append(sdr["TransactionID"].ToString().PadLeft(20, '0'));
                                sb.Append(sdr["RemittedCurrency"].ToString().PadRight(4, ' '));
                                var theAmount = String.Format("{0:N2}", float.Parse(sdr["RemittedAmount"].ToString()));
                                sb.Append(theAmount.Replace(",", "").Replace(".", "").PadLeft(19, '0'));
                                sb.Append("0001");
                                String CDTParse1 = Convert.ToDateTime(sdr["CreatedDateTime"].ToString()).ToString().Substring(0, 10);
                                String CDTParse2 = CDTParse1.Trim();
                                String CDT = CDTParse2;
                                if (CDT.Length < 10)
                                {
                                    sb.Append(CDT.PadLeft(10, '0'));
                                }
                                else
                                {
                                    sb.Append(CDT);
                                }
                                
                                sb.Append("SHA");
                                var intBene = Int32.Parse(sdr["BeneficiaryID"].ToString());
                                var BeneDetails = BeneServ.GetAll(x => x.BeneficiaryID == intBene, null, "").SingleOrDefault();
                                if (String.IsNullOrEmpty(BeneDetails.NationalityCardID))
                                {
                                    sb.Append(" ".PadRight(20, ' '));
                                }
                                else
                                {
                                    sb.Append(BeneDetails.NationalityCardID.PadRight(20, ' '));
                                }
                                if (BeneDetails.BeneficiaryName.Length > 40)
                                {
                                    sb.Append(BeneDetails.BeneficiaryName.Substring(0, 40));
                                }
                                else
                                {
                                    sb.Append(BeneDetails.BeneficiaryName.PadRight(40, ' '));
                                }
                                

                                var BeneAccount = sdr["AccountID"].ToString();
                                var BPMDetails = BPMServ.GetAll(x => x.AccountID == BeneAccount, null, "").SingleOrDefault();

                                sb.Append(BPMDetails.AccountNumber.PadRight(19, ' '));
                                sb.Append(BPMDetails.BankID.PadRight(7, ' '));
                                sb.Append(BPMDetails.BankName.PadRight(30, ' '));
                                sb.Append(BPMDetails.BranchID.ToString().PadRight(5, ' '));
                                sb.Append(BPMDetails.BranchName.PadRight(30, ' '));
                                if (BeneDetails.AddressLine1.Length > 40)
                                {
                                    sb.Append(BeneDetails.AddressLine1.Substring(0,40));
                                }
                                else
                                {
                                    sb.Append(BeneDetails.AddressLine1.PadRight(40, ' '));
                                }
                                
                                if (String.IsNullOrEmpty(BeneDetails.AddressLine2))
                                {
                                    sb.Append(" ".PadRight(40, ' '));
                                }
                                else
                                {
                                    sb.Append(BeneDetails.AddressLine2.PadRight(40, ' '));
                                }

                                var BeneAddressLine3 = BeneDetails.State + ' ' + BeneDetails.Suburb + ' ' + BeneDetails.Country;

                                if (BeneAddressLine3.Length > 30)
                                {
                                    sb.Append(" ".PadRight(30, ' '));
                                }
                                else
                                {
                                    sb.Append(" ".PadRight(30, ' '));
                                }
                                
                                if (String.IsNullOrEmpty(BeneDetails.Mobile))
                                {
                                    sb.Append(" ".PadRight(15, ' '));
                                }
                                else
                                {
                                    sb.Append(BeneDetails.Mobile.PadRight(15, ' '));
                                }
                                
                                sb.Append(" ".PadRight(80, ' '));

                                var intCustID = Int32.Parse(sdr["CustomerID"].ToString());
                                var CustDetails = CustServ.GetAll(x => x.CustomerID == intCustID, null, "").SingleOrDefault();

                                if(CustDetails.FullName.Length > 40)
                                {
                                    sb.Append(CustDetails.FullName.Substring(0,40));
                                }
                                else
                                {
                                    sb.Append(CustDetails.FullName.PadRight(40, ' '));
                                }

                                var CustAddressLine1 = CustDetails.UnitNo + ' ' + CustDetails.StreetNo + ' ' + CustDetails.StreetName + ' ' + CustDetails.StreetType;

                                if (CustAddressLine1.Length > 40)
                                {
                                    sb.Append(CustAddressLine1.Substring(0,40));
                                }
                                else
                                {
                                    sb.Append(CustAddressLine1.PadRight(40, ' '));
                                }

                                var CustAddressLine2 = CustDetails.Suburb + ' ' + CustDetails.State + ' ' + CustDetails.Postcode;
                                if (CustAddressLine2.Length > 40)
                                {
                                    sb.Append(CustAddressLine2.Substring(0,40));
                                }
                                else
                                {
                                    sb.Append(CustAddressLine2.PadRight(40, ' '));
                                }
                                
                                sb.Append(CustDetails.Country.PadRight(30, ' '));
                                if (String.IsNullOrEmpty(CustDetails.Mobile))
                                {
                                    sb.Append(" ".PadRight(15, ' '));
                                }
                                else
                                {
                                    sb.Append(CustDetails.Mobile.PadRight(15, ' '));
                                }
                                
                                sb.Append(" ".PadRight(25, ' '));

                                //sb.Append(FillEmptyFields(sdr["RemittedCurrency"].ToString(), 20, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["TransactionID"].ToString(), 20, "i") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["RemittedCurrency"].ToString(), 4, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["RemittedAmount"].ToString(), 19, "i") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["PaymentMethod"].ToString(), 4, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(Convert.ToDateTime(sdr["CreatedDateTime"].ToString(), ci).ToString("dd/MM/yyyy"), 10, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["BeneficiaryID"].ToString(), 20, "i") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["BeneficiaryName"].ToString(), 40, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["AccountNumber"].ToString(), 19, "i") + strDelimiter);
                                ////bank code not found
                                //sb.Append(FillEmptyFields(sdr["BankName"].ToString(), 30, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["BranchName"].ToString(), 30, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["BeneficiaryAddress1"].ToString(), 110, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["BeneficiaryMobile"].ToString(), 15, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["CustomerLastName"].ToString(), 40, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["CustomerAddress"].ToString(), 110, "s") + strDelimiter);
                                //sb.Append(FillEmptyFields(sdr["CustomerMobile"].ToString(), 15, "s") + strDelimiter);



                                
                                TotTrans++;
                                TID.Add(sdr["TransactionID"].ToString());

                                index = index + 1;
                            }

                        }
                    }
                    conn.Close();
                }
            }
            
            // SB TRIM LAST CARRIAGE RETURN
            
            sbH.Append("01" + strDelimiter);
            sbH.Append(agentCode.ToString().PadRight(7, ' ') + strDelimiter);
            
            //sbH.Append(agentCode.ToString("0000000") + strDelimiter);//agentCode
            sbH.Append(filename.Substring(0, filename.Length-4) + strDelimiter);//batchnumber
            sbH.Append(DateTime.Now.ToString("dd/MM/yyyy") + strDelimiter);//date
            sbH.Append(TotTrans.ToString().PadLeft(6, '0'));
            //sbH.Append("\r\n");

            sbH.Append(sb);

            string res = "";

            try
            {
                string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                StreamWriter file = new StreamWriter(filePath);
                file.WriteLine(sbH.ToString());
                file.Close();
                filehasbeencomplete = true;
            }
            catch (Exception ex)
            {
                filehasbeencomplete = false;

            }

            if (filehasbeencomplete == true)
            {
                foreach (var Transaction in TID)
                {
                    transClass.updateTransBankFileInsertion(Transaction);
                    transClass.insertAuditRecord(Transaction, HttpContext.Current.Session["LoggedPersonName"].ToString(), HttpContext.Current.Session["LoggedCompany"].ToString(), "EDIT", "Transaction " + Transaction + " has been inserted into Commercial Bank Bank File");
                }

                res = "File Created^" + filename;
            }
            else
            {
                res = "PPB File Could Not Be Created^nofile";
            }

            return res;
        }

        public String createDFCCBankFile(String CountryID, String BankCode, String AccountID)
        {
            classes.Transactions TRANS = new classes.Transactions();
            classes.Customer CUST = new classes.Customer();
            classes.Beneficiary BENE = new classes.Beneficiary();
            classes.BeneficiaryAccounts BENEACC = new classes.BeneficiaryAccounts();
            classes.Banking BKN = new Banking();
            classes.Currencies CUR = new Currencies();
            Boolean filehasbeencomplete = false;
            String filename = "DFCC - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv";
            String AID = HttpContext.Current.Session["AgentID"].ToString();

            String CurrencyList = BKN.getCurrenciesLinkedToForeignBankAccount(HttpContext.Current.Session["AgentID"].ToString(), CountryID, BankCode, AccountID);
            String[] Currencies = CurrencyList.Split(',');
            String CurrencyCodeList = String.Empty;
            List<String> TID = new List<String>();

            String BID = BankCode;
            String output = String.Empty;
            String BankType = String.Empty;

            foreach (var Currency in Currencies)
            {
                CurrencyCodeList += "'" + CUR.getCurrencyCodeFromID(Currency) + "',";
            }

            int Length = CurrencyCodeList.Length;
            CurrencyCodeList = CurrencyCodeList.Substring(0, (Length - 1));

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = @"SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, Rate, AuthorisationDateTime 
                        FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RoutingBank = " + BankCode + " AND AgentID = " + AID
                        + " AND RemittedCurrency IN (" + CurrencyCodeList + ")";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            String CID = TRANS.getCustomerIDFromTransactionID(sdr["TransactionID"].ToString());
                            String BeneID = TRANS.getTransactionBeneficiary(sdr["TransactionID"].ToString());
                            output += sdr["TransactionID"].ToString() + "~"; // Transaction ID
                            output += CUST.getCustomerFullName(CID) + "~"; // Customer's Full Name
                            output += CUST.getCustomerMobile(CID) + "~"; // Customer's Mobile
                            output += TRANS.getTransactionPurpose(sdr["TransactionID"].ToString()) + "~"; // Transaction Purpose
                            var BeneName = BENE.getBeneficiaryName(BeneID);
                            var BenePass1 = Regex.Replace(BeneName, @"[\d-]", string.Empty);
                            output += Regex.Replace(BenePass1, @"[^\w\.@-]", string.Empty)  + "~"; // Beneficiary Full Name
                            output += BENE.getBeneficiaryNIC(BeneID) + "~"; // Beneficiary NIC
                            output += BENE.getBeneficiaryFullAddress(BeneID) + "~"; // Beneficiary Full Address
                            output += BENE.getBeneficiaryMobile(BeneID) + "~"; // Beneficiary Mobile
                            output += BENE.getBeneficiaryPhone(BeneID) + "~"; //Beneficiary Other Contact Number
                            output += "~"; // Beneficairy Notes about transaction
                            output += TRANS.getReceivedAmountFromTransaction(sdr["TransactionID"].ToString()) + "~"; // Remitted Amount in Local Currency
                            output += TRANS.getRemittedCurrency(sdr["TransactionID"].ToString()) + "~"; // Remitted Currency

                            String RBank = TRANS.getRoutingBankCodeForTransaction(sdr["TransactionID"].ToString());

                            if (RBank == "7454")
                            {
                                BankType = "2";
                            }
                            else
                            {
                                BankType = "3";
                            }

                            output += BankType + "~";
                            output += BENEACC.getBeneficiaryBankCodeByBID(BeneID) + "~";
                            output += BENEACC.getBeneficiaryBranchCodeByBID(BeneID).PadLeft(3,'0') + "~";
                            output += BENEACC.getBeneficiaryAccountNumberOnTID(sdr["TransactionID"].ToString());
                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        output = "NoRecords";
                    }
                }
                conn.Close();

                try
                {
                    string filePath = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\" + filename;
                    System.IO.File.WriteAllText(filePath, output);
                    filehasbeencomplete = true;
                }
                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        TRANS.updateTransBankFileInsertion(Transaction);
                    }
                    return ("File Created^" + filename);
                }
                else
                {
                    return ("DFCC File Could Not Be Created^noreport");
                }
            }
        }

        public String PBHeaderFile()
        {
            classes.Transactions transClass = new classes.Transactions();
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;
            classes.Currencies CUR = new Currencies();
            classes.Banking BKN = new Banking();
            String AID = HttpContext.Current.Session["AgentID"].ToString();
            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts bAccount = new BeneficiaryAccounts();
            String filename1 = "PB - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
            String now = String.Empty;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            return "ok";

        }

        protected String disbursalMethod(String AID)
        {
            String method = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT PaymentMethod FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        method = sdr["PaymentMethod"].ToString();
                    }
                }
                conn.Close();
            }

            if (method == "Credited to the acc.")
            {
                method = "A/C TRANSFER";
            }
            else
            {
                method = "DIRECT";
            }

            return method;
        }

        protected string BuildLKRHeaderFile(String BatchNumber, String BankCode, String UploadDate, String AccountID, string FolderName, String RTotal, String Nbtotal, String Username, String RUploadDate, String XHouseCode, String CCode)
        {
            String outputString = String.Empty;
            string createdFile = "";
            outputString += " ".PadRight(4);
            outputString = outputString + BatchNumber.PadRight(15);
            outputString = outputString + XHouseCode;
            float TAmount = TotalLKRAmount(BankCode, CCode);
            outputString = outputString + string.Format("{0:N2}", float.Parse(RTotal)).Replace(",", "").Replace(".","").PadLeft(15);
            outputString = outputString + Nbtotal.PadLeft(5, '0');
            outputString = outputString + " ".PadRight(15);
            outputString = outputString + RUploadDate.PadRight(7);
            outputString = outputString + Username;
            outputString = outputString + CCode;

            try
            {
                String filename = BatchNumber + ".HDR";
                //String filename = FolderName + ".DTL";
                createdFile = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\"+ FolderName+"\\" + filename;
                System.IO.File.WriteAllText(createdFile,
                    outputString);

            }
            catch (Exception ex)
            {

            }
            return createdFile;

        }

        protected string BuilkLKRDetailFile(String BatchNumber, String UploadDate, String BankCode, string folderName, String RUploadDate, String Username, String XCode, String PIN, String CCode)
        {
            classes.Transactions transClass = new classes.Transactions();
            string createdFile = "";
            String delimiter = "|";
           
            List<String> TID = new List<String>();
            Boolean filehasbeencomplete = false;

            Customer cust = new Customer();
            Beneficiary bene = new Beneficiary();
            BeneficiaryAccounts bAccount = new BeneficiaryAccounts();
            String RAmount = String.Empty;
            String BeneID = String.Empty;

            String now = String.Empty;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT TransactionID, RemittedAmount, RemittedCurrency, CustomerID, BeneficiaryName, BeneficiaryID, AccountID, Rate, AuthorisationDateTime FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RemittedCurrency = '" + CCode + "' AND RoutingBank = " + BankCode;
            String outputString = String.Empty;
            String CFullName = String.Empty;
            int CFullNameLength = 0;
            String empty = String.Empty;

            String InputBranch = "";
            float RunningTotal = 0;
            int nbTrans = 0;
            BeneficiaryService BeneServ = new BeneficiaryService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            String IntAID = sdr["AccountID"].ToString();
                            var BPMDetails = BPMServ.GetAll(x => x.AccountID == IntAID, null, "").SingleOrDefault();
                            var IntBeneID = Int32.Parse(sdr["BeneficiaryID"].ToString());
                            var BeneDetails = BeneServ.GetAll(x => x.BeneficiaryID == IntBeneID, null, "").SingleOrDefault();

                            outputString = outputString + "    ";
                            outputString = outputString + BatchNumber.PadRight(15);
                            outputString = outputString + XCode;
                            String beneName = bene.getBeneficiaryName(sdr["BeneficiaryID"].ToString());
                            if (beneName.Length > 30)
                            {
                                outputString = outputString + beneName.Substring(0, 30);
                            }
                            else
                            {
                                outputString = outputString + beneName.PadRight(30);
                            }

                            String BankID =
                                bAccount.getBeneficiaryAccountBankCodeBasedOnAccountID(sdr["AccountID"].ToString());

                            String BranchName =
                                bAccount.getBeneficiaryAccountBranchNameBasedOnAccountID(sdr["AccountID"].ToString());
                            outputString = outputString + BankID.PadRight(4);

                            var BranchID = BPMDetails.BranchID.ToString();

                            outputString = outputString +
                                           BranchID.TrimEnd().PadLeft(3, '0');
                                
                            outputString = outputString +
                                           bAccount.getBeneficicaryAccountNumberBasedOnAccountID(sdr["AccountID"]
                                               .ToString()).PadRight(15);
                            //outputString = outputString + sdr["TransactionID"].ToString();

                            String TypeOfRemittance = ConfirmWhichTypeOfRemittance(BankID, BranchName);
                            outputString = outputString + TypeOfRemittance;
                            outputString = outputString + String.Format("{0:N2}", float.Parse(sdr["RemittedAmount"].ToString())).Replace(".", "").Replace(",", "").PadLeft(15);
                            RunningTotal += float.Parse(sdr["RemittedAmount"].ToString());
                            nbTrans++;
                            outputString = outputString + " ".PadRight(15); // Charge Amount
                            outputString = outputString + " ".PadRight(7); // Value Date
                            outputString = outputString + sdr["TransactionID"].ToString().PadLeft(25); // Reference
                            outputString = outputString + BeneDetails.NationalityCardID.PadRight(15); // Beneficiary ID

                            var Parse1 = RUploadDate.Trim();
                            var Parse2 = "";
                            if (Parse1.Length < 7)
                            {
                                Parse2 = Parse1.PadLeft(7, '0');
                            }
                            else
                            {
                                Parse2 = Parse1.Substring(0, 7);
                            }
                            outputString = outputString + Parse2; // Upload Date
                            outputString = outputString + Username; // Upload User
                            outputString = outputString + CCode
                                               .PadLeft(3); // Currency
                            outputString = outputString + PIN; //PIN
                            outputString = outputString + cust.getCustomerMobile(sdr["CustomerID"].ToString())
                                               .PadLeft(20); // Remitter Tel
                            outputString = outputString + bene.getBeneficiaryMobile(sdr["BeneficiaryID"].ToString())
                                               .PadRight(20); // Beneficiary Tel
                            outputString = outputString + " ".PadRight(50);
                            outputString = outputString + bene.getBeneficiaryEmail(sdr["BeneficiaryID"].ToString())
                                               .PadLeft(75); // Beneficiary Email
                            outputString = outputString + sdr["CustomerID"].ToString().PadRight(15); // Remitted ID
                            outputString = outputString + " ".PadRight(95);
                            outputString = outputString + "\r\n";



                            TID.Add(sdr["TransactionID"].ToString());
                        }
                    }
                    else
                    {
                        outputString = "NoRecords";
                    }

                }
                conn.Close();
                //folderName = "SEY - " + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                String filename = BatchNumber + ".DTL";
               

                //Response.AppendHeader("content-disposition", "attachment;filename=" + filename);
                //Response.ContentType = "application/octet-stream";
                //Response.Write(outputString);

                //Response.End();
                try
                {
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("//BankFileCreation//") + folderName);
                    createdFile = HttpContext.Current.Server.MapPath("/BankFileCreation") + "\\"+ folderName + "\\" + filename;
                    System.IO.File.WriteAllText(createdFile, outputString);

                    filehasbeencomplete = true;
                }

                catch (Exception ex)
                {
                    filehasbeencomplete = false;

                }

                if (filehasbeencomplete == true)
                {
                    foreach (var Transaction in TID)
                    {
                        transClass.updateTransBankFileInsertion(Transaction);
                    }
                   // return ("SEY File Successfully Created");
                }
                else
                {
                    //return ("SEY File Could Not Be Created");
                }

            }

            return createdFile + "|" + RunningTotal + "|" + nbTrans ;

            //Response.Write(outputString);
        }

        private String OwnRemittance(String theBank, String OriginatingBank, String BranchName, String AccountNumber)
        {
            String output = String.Empty;

            if (theBank == OriginatingBank)
            {
                if (AccountNumber.ToUpper().Contains("ADVISE") || AccountNumber.ToUpper().Contains("ADVICE"))
                {
                    output = "CASH";
                }
                else
                {
                    output = "EQUAL";
                }
                
            }
            else
            {
                if (AccountNumber.ToUpper().Contains("ADVISE") || AccountNumber.ToUpper().Contains("ADVICE"))
                {
                    output = "CASH";
                }
                else
                {
                    output = "DIFFERENT";
                }
            }

            return output;
        }

        protected String ConfirmWhichTypeOfRemittance(String BankID, String BranchName)
        {
            String returnValue = "";

            bool found = BranchName.Contains("Advise") || BranchName.Contains("Advice");

            if (found == true)
            {
                returnValue = "03";
            }
            else
            {
                if (BankID == "7287")
                {
                    returnValue = "01";
                }
                else
                {
                    returnValue = "04";
                }
            }

            return returnValue;
        }

        protected float TotalLKRAmount(String BankCode, String CCode)
        {
            float TotalAmount = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT SUM(RemittedAmount) AS TotalRemittance FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RemittedCurrency = '" + CCode + "' AND RoutingBank = " + BankCode;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["TotalRemittance"].ToString() == "0" || sdr["TotalRemittance"].ToString() == "NULL" || sdr["TotalRemittance"].ToString() == "")
                        {
                            TotalAmount = 0;
                        }
                        else
                        {
                            string dds = sdr["TotalRemittance"].ToString();
                            TotalAmount = float.Parse(sdr["TotalRemittance"].ToString());
                        }

                    }
                }

                conn.Close();
            }

            return TotalAmount;
        }

        protected String TotalLKRTXN(String BankCode)
        {
            String TotalAmount = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String strSQL = "SELECT COUNT(*) AS TotalRemittance FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' AND RemittedCurrency = 'LKR' AND RoutingBank = " + BankCode;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TotalAmount = sdr["TotalRemittance"].ToString();
                    }
                }

                conn.Close();
            }

            return TotalAmount;
        }

        public string FillEmptyFields(string input, int size, string type)
        {
            int inpSize = input.Length;
            int sub = size - inpSize;
            int fillCount = 0;
            string finStr = "";
            if ((size - inpSize) > 0)
            {
                if (type == "i")
                {
                    for (int i = 0; i < size - inpSize; i++)
                    {
                        finStr = finStr + "0";
                    }
                    finStr = finStr + input;
                }
                else
                {
                    finStr = finStr + input;
                    for (int i = 0; i < size - inpSize; i++)
                    {
                        finStr = finStr + " ";
                    }

                }
  
            }
            else if ((size - inpSize) == 0)
            {
                finStr = input;
            }
            else
            {
                int substrVal = inpSize - size;
                finStr = input.Remove(size, substrVal);
            }

            return finStr;
        }

        public string getBranchIDFromBankCodeAndBranchName(String BankCode, String BranchName)
        {
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BranchCode FROM dbo.BankBranches WHERE BankCode = '" + BankCode + "' AND BranchName = '" + BranchName + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BranchCode"].ToString();
                    }
                }

                conn.Close();
            }

            return output;
        }

    }
}