﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;

namespace KASI_Extend_.classes
{
    public class BeneficiaryAccounts
    {
        // Return's Beneficiary Account number
        public String getBeneficiaryAccountNumber(String BID)
        {
            String AccountNumber = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AccountNumber FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AccountNumber = sdr["AccountNumber"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return AccountNumber;
        }

        // Return's Beneficiary Bank Name
        public String getBeneficiaryBankName(String BID)
        {
            String BankName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BankName FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BankName = sdr["BankName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BankName;
        }

        // Return's the Country Name based on the Beneficiary Account through TransactionID
        public String getBeneficiaryAccountCountryName(String TID)
        {
            String CountryName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"SELECT CountryName FROM dbo.Countries
                                WHERE CountryID IN (SELECT CountryID FROM dbo.BeneficiaryPaymentMethods 
                                WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE 
                                TransactionID = " + TID + "))";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CountryName = sdr["CountryName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return CountryName;
        }

        // Return's the Beneficiary Account Name based on the BeneficiaryID
        public String getBeneficiaryAccountName(String BID)
        {
            String AccountName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AccountName FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AccountName = sdr["AccountName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return AccountName;
        }

        // Return's the Beneficiary Bank Branch
        public String getBeneficiaryBankBranch(String BID)
        {
            String BranchName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BranchName FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BranchName = sdr["BranchName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BranchName;
        }

        // Return's the Beneficiary Account Type
        public String getBeneficairyAccountType(String BID)
        {
            String AccountType = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AccountType FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AccountType = sdr["AccountType"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return AccountType;
        }

        // Return's the Beneficiary Payment Method
        public String getBeneficiaryPaymentMethod(String BID)
        {
            String PaymentMethod = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT PaymentMethod FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            PaymentMethod = sdr["PaymentMethod"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return PaymentMethod;
        }

        // Return's the Beneficiary Bank Code Based on Beneficiary ID
        public String getBeneficiaryBankCodeByBID(String BID)
        {
            String BankCode = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BankID FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BankCode = sdr["BankID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BankCode;
        }

        // Retunr's the Beneficiary Branch Code Based on Beneficiary ID
        public String getBeneficiaryBranchCodeByBID(String BID)
        {
            String BranchCode = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BranchID FROM dbo.BeneficiaryPaymentMethods WHERE BeneficiaryID = " + BID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BranchCode = sdr["BranchID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BranchCode;
        }

        // Return's the Beneficiary Bank Branch Code based on the transaction ID
        public String getBeneficiaryBranchCode(String TID)
        {
            String BranchCode = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BranchID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BranchCode = sdr["BranchID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BranchCode;
        }

        // Return's the Beneficiary Bank Branch Name Based on the Transaction ID
        public String getBeneficiaryBranchName(String TID)
        {
            String BranchName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BranchName FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BranchName = sdr["BranchName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BranchName;
        }

        // Return's the Beneficiary Bank Code Based on the Transaction ID
        public String getBeneficiaryBankCode(String TID)
        {
            String BankCode = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BankID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BankCode = sdr["BankID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BankCode;
        }

        // Return's the Beneficiary Bank Name Based on the Transaction  ID
        public String getBeneficiaryBankNameOnTID(String TID)
        {
            String BankName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BankName FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BankName = sdr["BankName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BankName;
        }

        // Return's the Beneficiary Account Number Based on the Transaction ID
        public String getBeneficiaryAccountNumberOnTID(String TID)
        {
            String AccountNumber = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AccountNumber FROM dbo.BeneficiaryPaymentMethods WHERE AccountID IN (SELECT AccountID FROM dbo.Transactions WHERE TransactionID = " + TID + ")";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AccountNumber = sdr["AccountNumber"].ToString().Replace(" ", "");
                        }
                    }
                    conn.Close();
                }
            }

            return AccountNumber;
        }

        // Return's the CountryID of the Beneficiary Account
        public String getBeneficiaryCountryIDBasedOnAccount(String AID)
        {
            String CountryID = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CountryID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CountryID = sdr["CountryID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return CountryID;
        }

        // Return's the CurrencyID of the Beneficiary Account
        public String getBeneficiaryAccountCurrencyID(String AID)
        {
            String CurrencyID = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CurrencyID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            CurrencyID = sdr["CurrencyID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return CurrencyID;
        }

        // Return's the Bank Code of the Beneficiary Account by AccountID
        public String getBeneficiaryAccountBankCodeBasedOnAccountID(String AID)
        {
            String BankCode = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BankID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BankCode = sdr["BankID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BankCode;
        }

        // Return's the Branch Code of the Beneficiary Account by AccountID
        public String getBeneficiaryAccountBranchCodeBasedOnAccountID(String AID)
        {
            String BranchCode = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BranchID FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BranchCode = sdr["BranchID"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BranchCode;
        }

        // Return's the Account Number of the Beneficiary Based on AccountID
        public String getBeneficicaryAccountNumberBasedOnAccountID(String AID)
        {
            String AccountNumber = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AccountNumber FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            AccountNumber = sdr["AccountNumber"].ToString().Replace(" ", "");
                        }
                    }
                    conn.Close();
                }
            }

            return AccountNumber;
        }

        // Return's the Beneficiary Account Branch Name Based On Account ID
        public String getBeneficiaryAccountBranchNameBasedOnAccountID(String AID)
        {
            String BranchName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BranchName FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BranchName = sdr["BranchName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BranchName;
        }

        // Return's the Beneficiary Account Bank Name Based On Account ID
        public String getBeneficiaryAccountBankNameBasedOnAccountID(String AID)
        {
            String BankName = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT BankName FROM dbo.BeneficiaryPaymentMethods WHERE AccountID = " + AID;
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            BankName = sdr["BankName"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return BankName;
        }


    }
}