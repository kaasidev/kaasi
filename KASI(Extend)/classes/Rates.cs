﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;

namespace KASI_Extend_.classes
{
    public class Rates
    {

        //Return's the latest rate
        public String getRate()
        {
            String Rate = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 Rate FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Rate = sdr["Rate"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the ANZIMTUSD Rate
        public float getANZIMTUSD()
        {
            float Rate = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 ANZIMTUSD FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = float.Parse(sdr["ANZIMTUSD"].ToString());
                            }
                        }
                        else
                        {
                            Rate = 0;
                        }
                        
                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the ANZIMTLKR Rate
        public float getANZIMTLKR()
        {
            float Rate = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 ANZIMTLKR FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = float.Parse(sdr["ANZIMTLKR"].ToString());
                            }
                        }
                        else
                        {
                            Rate = 0;
                        }

                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the XPRESS Rate
        public float getXPRESS()
        {
            float Rate = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 XPRESS FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = float.Parse(sdr["XPRESS"].ToString());
                            }
                        }
                        else
                        {
                            Rate = 0;
                        }

                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the MGRAM Rate
        public float getMGRAM()
        {
            float Rate = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 MGRAM FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = float.Parse(sdr["MGRAM"].ToString());
                            }
                        }
                        else
                        {
                            Rate = 0;
                        }

                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the CBSLTT Rate
        public float getCBSLTT()
        {
            float Rate = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 CBSLTT FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = float.Parse(sdr["CBSLTT"].ToString());
                            }
                        }
                        else
                        {
                            Rate = 0;
                        }

                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the BOCTT Rate
        public float getBOCTT()
        {
            float Rate = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 BOCTT FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = float.Parse(sdr["BOCTT"].ToString());
                            }
                        }
                        else
                        {
                            Rate = 0;
                        }

                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the WUNION Rate
        public float getWUNION()
        {
            float Rate = 0;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT TOP 1 WUNION FROM dbo.Rates ORDER BY RateDate DESC";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                Rate = float.Parse(sdr["WUNION"].ToString());
                            }
                        }
                        else
                        {
                            Rate = 0;
                        }

                    }
                    conn.Close();
                }
            }

            return Rate;
        }

        // Return's the rate for the agent for the day
        public String getAgentRates(String AID)
        {
            String AgentID = String.Empty;

            String Rate = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT CurrencyCode, AssignedRate FROM dbo.AgentCurrencyRates ACR INNER JOIN dbo.Currencies CR ON CR.CurrencyID = ACR.CurrencyID WHERE AssignedDate = CONVERT(varchar(10), getDate(), 103) AND AgentID = " + HttpContext.Current.Session["AgentID"].ToString();
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Rate = sdr["CurrencyCode"].ToString() + "|" + sdr["AssignedRate"].ToString() + "~";
                        }
                    }
                    conn.Close();
                }
            }

            return Rate;
        }
    }
}