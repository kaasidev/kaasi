﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;

namespace KASI_Extend_.classes
{
    public class Settings
    {

        // Return's if the WebSwitch is ON
        public String getWebSwitch()
        {
            String Switch = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'WebsiteMessageSwitch'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Switch = sdr["SettingsVariable"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return Switch;
        }

        // Return's the Web Message
        public String getWebMessage()
        {
            String Message = String.Empty;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'WebsiteMessage'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Message = sdr["SettingsVariable"].ToString();
                        }
                    }
                    conn.Close();
                }
            }

            return Message;
        }

        // Return's the Allocated Monthly AUD Limit
        public String getSystemMonthlyAUDLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float Limit = 0;
            String returnLimit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyAUDLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Limit = float.Parse(sdr["SettingsVariable"].ToString());
                    }
                }
                conn.Close();
            }

            return String.Format("{0:C2}", Limit);
        }

        // Return's the Allocated Yearly AUD Limit
        public String getSystemYearlyAUDLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float Limit = 0;
            String returnLimit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'YearlyAUDLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Limit = float.Parse(sdr["SettingsVariable"].ToString());
                    }
                }
                conn.Close();
            }

            return String.Format("{0:C2}", Limit);
        }

        // Return's the Allocated Monthly Trans Limit
        public Int32 getSystemMonthlyTransLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            Int32 Limit = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyTransLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Limit = Int32.Parse(sdr["SettingsVariable"].ToString());
                    }
                }
                conn.Close();
            }

            return Limit;
        }

        // Return's the Allocated Yearly Trans Limit
        public Int32 getSystemYearlyTransLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            Int32 Limit = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'YearlyTransLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Limit = Int32.Parse(sdr["SettingsVariable"].ToString());
                    }
                }
                conn.Close();
            }

            return Limit;
        }

        // Return's the Monthly AUD Variation Limit
        public String getSystemMonthlyVariationLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            float Limit = 0;
            String returnLimit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyVariation'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Limit = float.Parse(sdr["SettingsVariable"].ToString());
                    }
                }
                conn.Close();
            }

            return String.Format("{0:C2}", Limit);
        }

        // Return's the Number of Months it should check the variation
        public Int32 getSystemMonthsOfVariation()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            Int32 Limit = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyVariationMonths'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Limit = Int32.Parse(sdr["SettingsVariable"].ToString());
                    }
                }
                conn.Close();
            }

            return Limit;
        }

        // Return's the IFTI File Location
        public String getSystemIFTIFileLocation()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String location = String.Empty;
            String returnLimit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'IFITFileLocation'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        location = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return location;
        }

        // Return's the SMR File Location
        public String getSystemSMRFileLocation()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String location = String.Empty;
            String returnLimit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'SMRFileLocation'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        location = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return location;
        }

        // Return's the SMR File Location
        public String getSystemTTRFileLocation()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String location = String.Empty;
            String returnLimit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'TTRFileLocation'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        location = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return location;
        }

        // Return's the STMP Server Name of Master Company
        public String getSMTPServerName()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String SMTPServerName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'SMTPServerName'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        SMTPServerName = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return SMTPServerName;
        }

        // Return's the SMTP Server Port of Master Company
        public String getSMTPServerPort()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'SMTPPort'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP SSL Enabled status of Master Company
        public String getSMTPSSLEnabled()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'SSLEnabled'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP Username status of Master Company
        public String getSMTPUsername()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'SMTPUsername'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP Password status of Master Company
        public String getSMTPPassword()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'SMTPPassword'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the SMTP Password status of Master Company
        public String getSMTPEmailAddress()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'SMTPEmailAddress'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Nb Months Before Running the Monthly Variation Check
        public String getSystemNbMonthBeforeCheck()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String Limit = String.Empty;
            String returnLimit = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyVariationMonthCheck'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        Limit = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return Limit;
        }
    }
}