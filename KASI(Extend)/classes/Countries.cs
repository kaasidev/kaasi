﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.classes
{
    public class Countries
    {
        public String CountryID { get; set; }
        public String CountryName { get; set; }
        public String MasterCurrencyCode { get; set; }
        public String MasterCurrencyName { get; set; }
        public float MaximumAmount { get; set; }
        public String ViewEdit { get; set; }

        public String getCountryNameByID(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT CountryName FROM dbo.Countries WHERE CountryID = " + CID;
            String CountryName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        CountryName = sdr["CountryName"].ToString();
                    }
                }
                conn.Close();
            }

            return CountryName;
        }

        public String getCountryMasterCurrencyByID(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT MasterCurrencyCode FROM dbo.Countries WHERE CountryID = " + CID;
            String CurrencyCode = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        CurrencyCode = sdr["MasterCurrencyCode"].ToString();
                    }
                }
                conn.Close();
            }

            return CurrencyCode;
        }

        public String getCountryIDByName(String Country)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT CountryID FROM dbo.Countries WHERE CountryName = '" + Country + "'";
            String CountryID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        CountryID = sdr["CountryID"].ToString();
                    }
                }
                conn.Close();
            }

            return CountryID;
        }

        // Return's the Country's Flag File Path
        public String getCountryFlagFilePathLarge(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT ImageFileLocationLarge FROM dbo.Countries WHERE CountryID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["ImageFileLocationLarge"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }
    }
}