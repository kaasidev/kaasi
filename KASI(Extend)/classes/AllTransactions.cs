﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.classes
{
    public class AllTransactions
    {
        public int TransactionID { get; set; }
        public String CreatedDateTime { get; set; }
        public String CustomerID { get; set; }
        public String CustomerLastName { get; set; }
        public String DOB { get; set; }
        public String Mobile { get; set; }
        public String Suburb { get; set; }
        public String BeneficiaryName { get; set; }
        public String DollarAmount { get; set; }
        public String Rate { get; set; }
        public String RemittedAmount { get; set; }
        public String Status { get; set; }
        public String theCustomerID { get; set; }
        public String BankName { get; set; }
        public String DepositAccount { get; set; }
        public String BankBranch { get; set; }
        public String AgentName { get; set; }
        public String CountryName { get; set; }
        public String ServiceCharge { get; set; }
        public String RemittedCurrency { get; set; }

    }
}