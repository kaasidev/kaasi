﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace KASI_Extend_.classes
{
    public class ThinkTank
    {
        public async Task<int> AnalyseTransaction(String TransactionID)
        {
            int x = await Task.FromResult(0);
            return x;

            // If all of the below method are not true, then put the transaction [dbo].[Transactions].[Status] into 'PENDING'
        }

        protected void AnalyseMonthlyDollarLimit(String CID, String TID)
        {
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer has a monthly limit set in [dbo].[Customers].[SpendingPower].
            // If combined Transaction AUD Total [dbo].[Transactions].[DollarAmount] for this month exceed above SpendingPower, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly AUD Limit Exceeded'
        }

        protected void AnalyseMonthlyNbTransLimit(String CID, String TID)
        {
            // CID --> CustomerID
            // TID --> TransactionID

            // Global setting in [dbo].[Settings].[SettingName] = MonthlyTransLimit gives the maximum amount of transactions per month before it gets flagged
            // If COUNT(*) Transactions for that customer for this month exceed above MonthlyTransLimit, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Number of Transaction Limit Exceeded'
        }

        protected void AnalyseDocumentExpiry(String CID, String TID)
        {
            // CID --> CustomerID
            // TID --> TransactionID

            // Customer Primary Documents are in [dbo].[CustomerDocuments].[TypeOfDocument] = 'PRIMARY'
            // There can be multiple primary documents. If ALL primary documents have expired, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW'
            // Set [dbo].[Transactions].[ComplianceReason] to 'Primary Document(s) Expired'
        }

        protected void AnalyseMonthlyVariation(String CID, String TID)
        {
            // CID --> CustomerID
            // TID --> TransactionID

            // Look at the total for the last x Months ([dbo].[Settings].[SettingName] = MonthlyVariationMonths) of transactions in Dollar Amount.
            // The allowed variation is in [dbo].[Settings].[SettingName] = 'MonthlyVariation'
            // If the variation between the total for each month is greater than the MonthlyVariation, change transaction Status [dbo].[Transactions].[Status] to 'REVIEW' 
            // Set [dbo].[Transactions].[ComplianceReason] to 'Monthly Variation Exceeded'
        }

    }
}