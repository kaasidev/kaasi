﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace KASI_Extend_.classes
{
    public class MasterBankClass
    {
        public int BankAccountID { get; set; }
        public String BankName { get; set; }
        public String BSB { get; set; }
        public String AccountNumber { get; set; }
        public String AccountName { get; set; }
        public String Active { get; set; }
        public String ViewEdit { get; set; }
        
        // Return's the Master Bank Account Name based on Master Bank AccountID
        public String getMasterBankNameByAccountID(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;
            
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AccountName FROM dbo.MasterCompanyBanks WHERE BankAccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["AccountName"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getMasterBankAccountNumberByAccountID(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BSB, AccountNumber FROM dbo.MasterCompanyBanks WHERE BankAccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BSB"].ToString() + " " + sdr["AccountNumber"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Master Bank Account BSB based on Master Bank AccountID
        public String getMasterBankBSBByAccountID(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BSB FROM dbo.MasterCompanyBanks WHERE BankAccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BSB"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the MAster Bank Account Number based on Master Bank AccountID
        public String getMasterBankAccountNumberbyAccountID(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AccountNumber FROM dbo.MasterCompanyBanks WHERE BankAccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["AccountNumber"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Master Bank Name based on Master Bank AccountID
        public String getMasterBankByAccountID(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankName FROM dbo.MasterCompanyBanks WHERE BankAccountID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BankName"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }
    }
}