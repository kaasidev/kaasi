﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.classes
{
    public class AgentFullList
    {
        public int AgentID { get; set; }
        public String AgentName { get; set; }
        public String AgentSuburb { get; set; }
        public String AgentState { get; set; }
        public String AgentStatus { get; set; }
        public String ViewEdit { get; set; }
    }
}