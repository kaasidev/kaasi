﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend_.classes
{
    public class Banking
    {

        public String getRoutingBank(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankCode = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DefaultRoutingBank FROM dbo.Banks WHERE BankCode = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankCode = sdr["DefaultRoutingBank"].ToString();
                    }

                }
                conn.Close();
            }

            return BankCode;
        }

        public String getRoutingBankFromBankName(String BankName)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DefaultRoutingBank FROM dbo.Banks WHERE BankName = '" + BankName + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["DefaultRoutingBank"].ToString();
                    }

                }
                conn.Close();
            }

            return output;
        }

        public String getBankNameFromBankCode(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankName FROM dbo.Banks WHERE BankCode = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankName = sdr["BankName"].ToString();
                    }
                }
                conn.Close();
            }
            return BankName;
        }

        public String getLastDepositMethod(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String DepositMethodName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DepositMethodName FROM dbo.DepositMethods WHERE DepositMethodID IN (SELECT TOP 1 DepositMethodID FROM dbo.CreditTransactions WHERE CustomerID = " + CID + " ORDER BY CreatedDateTime DESC)";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        DepositMethodName = sdr["DepositMethodName"].ToString();
                    }
                }
                conn.Close();
            }
            return DepositMethodName;
        }

        public String getBranchAddressByID(String BankID, String BranchID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankName = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT City FROM dbo.BankBranches WHERE BankCode = '" + BankID + "' AND BranchCode = '" + BranchID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankName = sdr["City"].ToString();
                    }
                }
                conn.Close();
            }
            return BankName;
        }

        public List<String> getForeignBankListForAgentByCountryID(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<String> BankID = new List<String>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT DISTINCT BankID FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND CountryID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankID.Add(sdr["BankID"].ToString());
                    }
                }
                conn.Close();
            }

            return BankID;
        }

        public List<String> getForeignBankListAccountsForAgentByCountryID(String AID, String CID, String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<String> FBA = new List<String>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT ForeignBankAccountNumber FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND CountryID = " + CID + " AND BankID = " + BID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        FBA.Add(sdr["ForeignBankAccountNumber"].ToString());
                    }
                }
                conn.Close();
            }

            return FBA;
        }

        public String getBankNameFromBankID(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String BankName = String.Empty; 

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankName FROM dbo.Banks WHERE BankCode = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BankName = sdr["BankName"].ToString();
                    }
                }
                conn.Close();
            }

            return BankName;
        }

        public String getBankUploadFileNameFromBankID(String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT UploadFileName FROM dbo.Banks WHERE BankCode = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["UploadFileName"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }
        
        public String getAgentFBSettingIDFromAgentIDCountryIDandAccountID(String AID, String CID, String BID, String FBAN)
        {
            int intAID = Int32.Parse(AID);
            int intCID = Int32.Parse(CID);
            String output = String.Empty;

            AgentForeignBankSettingService AFBSServ = new AgentForeignBankSettingService(new UnitOfWorks(new KaprukaEntities()));
            var AFBSDetails = AFBSServ.GetAll(x => x.AgentID == intAID && x.CountryID == intCID && x.ForeignBankAccountNumber == FBAN && x.BankID == BID, null, "").SingleOrDefault();

            output = AFBSDetails.AgentForeignBankSettingsID.ToString();

            return output;
        }

        public String getCurrenciesLinkedToForeignBankAccount(String AID, String CID, String BID, String AccID)
        {
            String output = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND BankID = '" + BID + "' AND CountryID = " + CID + " AND ForeignBankAccountNumber = " + AccID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CurrencyID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        public String confirmBankViewDefaultExist(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT AgentForeignBankSettingsID FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND DefaultBank = 'Y'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        output = "true";
                    }
                    else
                    {
                        output = "false";
                    }
                }
                conn.Close();
            }

            return output;
        }
    }
}