﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace KASI_Extend.classes
{
    public class AgentNotification
    {
        public int AgentNotificationID { get; set; }
        public int GlobalNotificationID { get; set; }
        public int LoginID { get; set; }
        public String Type { get; set; }
        public int RecordID { get; set; }
        public String Description { get; set; }
        public String CreatedDateTime { get; set; }
        public String Action { get; set; }
    }
}