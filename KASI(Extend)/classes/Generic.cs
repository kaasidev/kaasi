﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.classes
{
    public class Generic
    {
        public List<String> ConvertStringToList(String input, Char delimiter)
        {
            List<String> output = new List<String>();

            output = input.Split(delimiter).ToList();

            return output;
        }

        public float CalculateRate(float GivenRate, float Margin, String UpDown)
        {

            float Total = 0;

            if (UpDown == "Down")
            {
                Total = GivenRate - Margin;
            }
            else
            {
                Total = GivenRate + Margin;
            }

            return Total;
        }

    }
}