﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.classes
{
    public class Audit
    {
        public DateTime AuditDateTime { get; set; }
        public String By { get; set; }
        public String Description { get; set; }
    }
}