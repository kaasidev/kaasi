﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_.classes
{
    public class TransactionFeeTiers
    {
        // Return's The Tier 1 Bracket
        public String getTier1Bracket(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Tier1From, Tier1To FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + AID + " AND CountryID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["Tier1From"].ToString() != "" && sdr["Tier1To"].ToString() != "")
                        {
                            output = String.Format("{0:C2}", float.Parse(sdr["Tier1From"].ToString())) + " - " + String.Format("{0:C2}", float.Parse(sdr["Tier1To"].ToString()));
                        }
                        else
                        {
                            output = "Tier 1";
                        }
                        

                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getTier2Bracket(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Tier2From, Tier2To FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + AID + " AND CountryID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["Tier2From"].ToString() != "" && sdr["Tier2To"].ToString() != "")
                        {
                            output = String.Format("{0:C2}", float.Parse(sdr["Tier2From"].ToString())) + " - " + String.Format("{0:C2}", float.Parse(sdr["Tier2To"].ToString()));
                        }
                        else
                        {
                            output = "Tier 2";
                        }
                        

                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getTier3Bracket(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Tier3From, Tier3To FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + AID + " AND CountryID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["Tier3From"].ToString() != "" && sdr["Tier3To"].ToString() != "")
                        {
                            output = String.Format("{0:C2}", float.Parse(sdr["Tier3From"].ToString())) + " - " + String.Format("{0:C2}", float.Parse(sdr["Tier3To"].ToString()));
                        }
                        else
                        {
                            output = "Tier 3";
                        }
                        

                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getCountryMaximum(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT MaximumAllowedAmount FROM dbo.Countries WHERE CountryID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        output = String.Format("{0:C2}", float.Parse(sdr["MaximumAllowedAmount"].ToString()));

                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getTier1AgentFee(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Tier1From, Tier1To, Tier1Amount FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + AID + " AND CountryID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["Tier1From"].ToString() != "" && sdr["Tier1To"].ToString() != "")
                        {
                            output = String.Format("{0:C2}", float.Parse(sdr["Tier1Amount"].ToString()));
                        }
                        else
                        {
                            output = "N/A";
                        }


                    }
                }
                conn.Close();
            }

            return output;
        }
        
        public String getTier2AgentFee(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Tier2From, Tier2To, Tier2Amount FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + AID + " AND CountryID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["Tier2From"].ToString() != "" && sdr["Tier2To"].ToString() != "")
                        {
                            output = String.Format("{0:C2}", float.Parse(sdr["Tier2Amount"].ToString()));
                        }
                        else
                        {
                            output = "N/A";
                        }


                    }
                }
                conn.Close();
            }

            return output;
        }

        public String getTier3AgentFee(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT Tier3From, Tier3To, Tier3Amount FROM dbo.AgentTransactionFeeTiers WHERE AgentID = " + AID + " AND CountryID = " + CID;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                    while (sdr.Read())
                    {
                        if (sdr["Tier3From"].ToString() != "" && sdr["Tier3To"].ToString() != "")
                        {
                            output = String.Format("{0:C2}", float.Parse(sdr["Tier3Amount"].ToString()));
                        }
                        else
                        {
                            output = "N/A";
                        }


                    }
                }
                conn.Close();
            }

            return output;
        }
    }
}