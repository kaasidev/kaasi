﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using KASI_Extend_.classes;
using System.Globalization;

namespace KASI_Extend_.classes
{
    public class Currencies
    {

        // Return's the Currency Code based on ID
        public String getCurrencyCodeFromID(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyCode FROM dbo.Currencies WHERE CurrencyID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CurrencyCode"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Currency Name based on ID
        public String getCurrencyNameFromID(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyName FROM dbo.Currencies WHERE CurrencyID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CurrencyName"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent Rate based on Agent ID and CurrencyID
        public String getAgentCurrencyRateFromAIDandCID(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 AssignedRate FROM dbo.AgentCurrencyRates WHERE CurrencyID = " + CID + " AND AgentID = " + AID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["AssignedRate"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent Own set rate based on Agent ID and Currency ID
        public String getAgentOwnRateFromAIDandCID(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 SetRate FROM dbo.AgentSetRates WHERE CurrencyID = " + CID + " AND AgentID = " + AID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SetRate"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Calculation between Set Rate and Own Rate
        public float getDifferenceBetweenSetandOwn(String SR, String OR)
        {
            float SetR = float.Parse(SR);
            float OwnR = float.Parse(OR);
            float finalValue = SetR - OwnR;

            return finalValue;
        }

        // Return's the Currency Country flag location
        public String getCurrencyFlag(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT ImageFileLocation FROM dbo.Currencies WHERE CurrencyID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["ImageFileLocation"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Currency ID based on the Currency Code
        public String getCurrencyIDByCurrencyCode(String CCode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.Currencies WHERE CurrencyCode = '" + CCode + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CurrencyID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Monthly AUD Allowed Limit Default 
        public String getMonthlyAUDLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'MonthlyAUDLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Yearly AUD Allowed Limit Default 
        public String getYearlyAUDLimit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT SettingsVariable FROM dbo.Settings WHERE SettingName = 'YearlyAUDLimit'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["SettingsVariable"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Currency associated with the Country
        public List<String> getCountryCurrencies(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<String> output = new List<String>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.CountryCurrencies WHERE CountryID = " + CID ;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output.Add(sdr["CurrencyID"].ToString());
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Country's Master Currency Code
        public String getCountryMasterCurrencyCode(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT MasterCurrencyCode FROM dbo.Countries WHERE CountryID = " + CID ;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["MasterCurrencyCode"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Country's Master Currency Name
        public String getCountryMasterCurrencyName(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT MasterCurrencyName FROM dbo.Countries WHERE CountryID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["MasterCurrencyName"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Country's Additional Currencies Code
        public String getCountryAdditionalCurrencyCodes(String CID)
        {
            String MasterCurrency = getCountryMasterCurrencyCode(CID);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CUR.CurrencyCode FROM dbo.CountryCurrencies CC INNER JOIN dbo.Currencies CUR ON CC.CurrencyID = CUR.CurrencyID WHERE CC.CountryID = " + CID + " AND CUR.CurrencyCode <> '" + MasterCurrency + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {

                        while (sdr.Read())
                        {
                            output = output + sdr["CurrencyCode"].ToString() + " | ";
                        }
                    
                }
                conn.Close();
            }
            int Length = output.Length;
            if (Length != 0)
            {
                output = output.Substring(0, (Length - 3));
            }
            

            return output;
        }

        // Return's the Linked Currencies for the Agent Foreign Bank 
        public String getLinkedCurrenciesForAgentForeignBankSetting(String FBAN)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.AgentForeignBankSettings WHERE AgentForeignBankSettingsID = " + FBAN;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CurrencyID"].ToString();
                    }
                }
                
                conn.Close();
            }

            return output;
        }
        
        // Return's the TOP 10 currencies for Master Company
        public List<String> getTop10CurrenciesForMasterCompany()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<String> output = new List<String>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.Currencies WHERE CurrencyCode <> 'AUD' AND ShowInDashboard = 'True'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output.Add(sdr["CurrencyID"].ToString());
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's All the Currencies for Master Company
        public List<String> getAllCurrenciesForMasterCompany()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            List<String> output = new List<String>();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.Currencies WHERE CurrencyCode <> 'AUD'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output.Add(sdr["CurrencyID"].ToString());
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Currency ID from the Currency Code
        public String getCurrencyIDFromCurrencyCode(String CCode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CurrencyID FROM dbo.Currencies WHERE CurrencyCode = '" + CCode + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CurrencyID"].ToString();
                    }
                }
                conn.Close();
            }

            return output;
        }

        // Return's the Agent's Assigned Rate for Currency
        public float getAgentAssignedRateForCurrencyFromMasterRate(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String MasterRate = String.Empty;
            String MarginRate = String.Empty;
            float output = 0;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 BuyingRate FROM dbo.MasterCompanyBuyRates WHERE CurrencyID = " + CID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        MasterRate = sdr["BuyingRate"].ToString();
                    }
                }
                conn.Close();
            }

            if (String.IsNullOrEmpty(MasterRate))
            {
                MasterRate = "0";
            }

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TOP 1 Margin FROM dbo.MasterCompanyRateMargins WHERE CurrencyID = " + CID + " ORDER BY CreatedDateTime DESC";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        MarginRate = sdr["Margin"].ToString();
                    }
                }
                conn.Close();
            }

            if (String.IsNullOrEmpty(MarginRate))
            {
                MarginRate = "0";
            }

            output = float.Parse(MasterRate) + float.Parse(MarginRate);

            return output;
        }

        
       
    }
}