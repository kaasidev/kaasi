﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="TransactionsByDateRange.aspx.cs" Inherits="KASI_Extend_.Reports.TransactionsByDateRange" EnableEventValidation="false" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">

    <div class="container_btns">
        <table class="tbl_width_1200"> <!-- MAIN TABLE STARTS HERE -->
            <tr>
                <td class="rep_main_headings">REPORTS <i class="material-icons_rep">chevron_right</i> Customer Transactions Report</td>
                <td>&nbsp;</td>
            </tr>

        </table> <!-- MAIN TABLE ENDS HERE -->
    </div>

    <div></div>

    <div class="container_one">
        <table class="tbl_width_1200" >
            <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_1160">
                                    <tr><td><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></td></tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="FromDate" CssClass="datePickerReport" runat="server" placeholder="From (dd/mm/yyyy)"></asp:TextBox>&nbsp;
                                            <asp:TextBox ID="EndDate" CssClass="datePickerReport" runat="server" placeholder="To (dd/mm/yyyy)"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="rep_dropdown"></asp:DropDownList>&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="Button1" runat="server" Text="Run Report" OnClick="Button1_Click" class="btn_green_nomargin" />
                                        </td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:right;"><i class="material-icons_rep">settings</i></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="container_one">
       
        <table class="tbl_width_1200" >
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="700px" Width="90%">
                       
                    </rsweb:ReportViewer>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>


    </form>
</asp:Content>
