﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Data.Sql;

namespace KASI_Extend_
{
    public partial class BankingAG : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.Banking BKN = new classes.Banking();
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

           
            if (!IsPostBack)
            {
                SetDefaultBank.Style.Add("display", "none");
                mask.Style.Add("display", "none");
                buildCountryDropdownList(Session["AgentID"].ToString());
                String hasDefaultBank = BKN.confirmBankViewDefaultExist(Session["AgentID"].ToString());
                classes.Agent AGT = new classes.Agent();
                lbl_TotalTXNS.Text = AGT.getAgentAwaitingTransTotal(Session["AgentID"].ToString());
                var AGTTotal = AGT.getAgentAUDAwaitingTransTotal(Session["AgentID"].ToString());
                if (!String.IsNullOrEmpty(AGTTotal))
                {
                    lbl_TotalAUD.Text = String.Format("{0:N2}", float.Parse(AGT.getAgentAUDAwaitingTransTotal(Session["AgentID"].ToString())));
                }
                else
                {
                    lbl_TotalAUD.Text = "0.00";
                }
                

                if (hasDefaultBank == "true")
                {
                    setDefaultCountry(Session["AgentID"].ToString());
                    setDefaultBank(Session["AgentID"].ToString());
                    setDefaultAccount(Session["AgentID"].ToString());
                    setDefaultStatsOnScreen();
                }
            }
            hidden_AgentID.Value = Session["AgentID"].ToString();

        }

        private void setDefaultCountry(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT CountryID FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND DefaultBank = 'Y'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["CountryID"].ToString();
                    }
                }
                conn.Close();
            }

            DDL_CountryList.SelectedValue = output;
            setHiddenValues(AID, output);

        }

        private void setDefaultBank(String AID)
        {
            classes.Banking BKN = new classes.Banking();
            String DValue = DDL_CountryList.SelectedValue;
            List<String> BankList = BKN.getForeignBankListForAgentByCountryID(AID, DDL_CountryList.SelectedValue);

            DDL_BankList.Items.Add(new ListItem("Select Bank", ""));
            foreach (var BankID in BankList)
            {
                String BankName = BKN.getBankNameFromBankID(BankID);
                DDL_BankList.Items.Add(new ListItem(BankName, BankID));
            }


            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT BankID FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + 
                    " AND CountryID = " + DDL_CountryList.SelectedValue + " AND DefaultBank = 'Y'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BankID"].ToString();
                    }
                }
                conn.Close();
            }

            DDL_BankList.SelectedValue = output;
        }

        private void setDefaultAccount(String AID)
        {
            classes.Banking BKN = new classes.Banking();
            List<String> AccountList = BKN.getForeignBankListAccountsForAgentByCountryID(AID, DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue);

            DDL_AccountList.Items.Add(new ListItem("Select Account", ""));
            foreach (var AccountID in AccountList)
            {
                DDL_AccountList.Items.Add(new ListItem(AccountID, AccountID));
            }


            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT ForeignBankAccountNumber FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID +
                    " AND CountryID = " + DDL_CountryList.SelectedValue + " AND BankID = '" + DDL_BankList.SelectedValue + "' AND DefaultBank = 'Y'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["ForeignBankAccountNumber"].ToString();
                    }
                }
                conn.Close();
            }

            DDL_AccountList.SelectedValue = output;
        }

        private void setDefaultStatsOnScreen()
        {
            classes.Currencies CUR = new classes.Currencies();
            String SelectedAccount = DDL_AccountList.SelectedValue;
            String SelectedCountryValue = DDL_CountryList.SelectedValue;
            String SelectedBankID = DDL_BankList.SelectedValue;
            classes.Banking BKN = new classes.Banking();
            classes.Transactions TRAN = new classes.Transactions();
            classes.Agent AGT = new classes.Agent();
            String FBAN = String.Empty;
            FBAN = BKN.getAgentFBSettingIDFromAgentIDCountryIDandAccountID(Session["AgentID"].ToString(), SelectedCountryValue, SelectedBankID, SelectedAccount);

            BankName.InnerText = DDL_BankList.SelectedItem.ToString();

            String LinkedCurrencies = CUR.getLinkedCurrenciesForAgentForeignBankSetting(FBAN);
            String[] Currency = LinkedCurrencies.Split(',');

            String currencyString = String.Empty;
            float AccTotalTrans = 0;
            String tempAccTotalTrans = String.Empty;
            float AccTotalAUD = 0;
            String tempAccTotalAUD = String.Empty;
            float AccTotalForeign = 0;
            String tempAccTotalForeign = String.Empty;

            int Length = currencyString.Length;
            if (Length != 0)
            {
                currencyString = currencyString.Substring(0, (Length - 3));
            }
            String[] CurrencyCodes = currencyString.Split('-');
            TableRow trow2 = new TableRow();
            foreach (var Cur in CurrencyCodes)
            {
                TableRow trow = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();

                TableCell tcell = new TableCell();
                tcell.Text = Cur;
                trow2.Cells.Add(tcell);
                

                tempAccTotalTrans = AGT.getAgentTransTotalBasedOnAgentIDAndCurrencyCode(Session["AgentID"].ToString(), Cur, DDL_BankList.SelectedValue.ToString());
                if (!String.IsNullOrEmpty(tempAccTotalTrans.ToString()))
                {
                    AccTotalTrans += float.Parse(tempAccTotalTrans);
                }
                else
                {
                    tempAccTotalTrans = "0.00";
                }

                tempAccTotalAUD = AGT.getAgentAUDTotalBasedOnAgentIDandCurrencyCode(Session["AgentID"].ToString(), Cur, DDL_BankList.SelectedValue.ToString());
                if (!String.IsNullOrEmpty(tempAccTotalAUD.ToString()))
                {
                    AccTotalAUD += float.Parse(tempAccTotalAUD);
                }
                else
                {
                    tempAccTotalAUD = "0.00";
                }

                tempAccTotalForeign = AGT.getAgentAUDTotalBasedOnAgentIDandCurrencyCodeInLocalCurrency(Session["AgentID"].ToString(), Cur, DDL_BankList.SelectedValue.ToString());
                if (!String.IsNullOrEmpty(tempAccTotalForeign.ToString()))
                {
                    AccTotalForeign += float.Parse(tempAccTotalForeign);
                }
                else
                {
                    tempAccTotalForeign = "0.00";
                }

                cell1.Text = String.Format("{0:N2}", String.Format("{0:N2}", float.Parse(tempAccTotalForeign)));
                cell2.Text = Cur;
                trow.Cells.Add(cell1);
                trow.Cells.Add(cell2);
                tbl_IndiCurr.Rows.Add(trow);
            }
            Tbl_Currencies.Rows.Add(trow2);
            lbl_AccTotalTrans.Text = AccTotalTrans.ToString();
            lbl_AccTotalAUD.Text = String.Format("{0:N2}", float.Parse(AccTotalAUD.ToString()));

            LBL_AccountNumber.Text = DDL_AccountList.SelectedItem.ToString();
            if (!IsPostBack)
            {
                lbl_LastFileCreation.Text = AGT.getAgentLastBankFileCreatedDate(Session["AgentID"].ToString());
            }
            


        }

        private void buildCountryDropdownList(String AID)
        {
            classes.Agent AGT = new classes.Agent();
            classes.Countries CTR = new classes.Countries();
            if (!IsPostBack)
            {
                DDL_CountryList.Items.Clear();
                DDL_Edit_CountryList.Items.Clear();
            }
            
            List<String> CountryList = AGT.getAgentAssignedCountries(AID);
            DDL_CountryList.Items.Add(new ListItem("Select country", ""));
            DDL_Edit_CountryList.Items.Add(new ListItem("Select country", ""));
            foreach (var country in CountryList)
            {
                String CountryName = CTR.getCountryNameByID(country);
                DDL_CountryList.Items.Add(new ListItem(CountryName, country));
                DDL_Edit_CountryList.Items.Add(new ListItem(CountryName, country));
            }

            //buildEditDefaultScreen(AID);
        }

        private void buildEditDefaultScreen(String AID)
        {
            classes.Agent AGT = new classes.Agent();
            classes.Banking BKN = new classes.Banking();
            String settings = AGT.getAgentDefaultForeignBankSettings(AID);

            String[] eachSetting = settings.Split('|');
            DDL_Edit_CountryList.SelectedValue = eachSetting[0].ToString();

            List<String> BankList = new List<String>();
            BankList = BKN.getForeignBankListForAgentByCountryID(AID, DDL_Edit_CountryList.SelectedValue);

            DDL_Edit_BankList.Items.Add(new ListItem("Select Bank", ""));
            foreach(var BankID in BankList)
            {
                String BankName = BKN.getBankNameFromBankID(BankID);
                DDL_Edit_BankList.Items.Add(new ListItem(BankName, BankID));
            }

            DDL_Edit_BankList.SelectedValue = eachSetting[1].ToString();

            List<String> AccountList = new List<String>();
            AccountList = BKN.getForeignBankListAccountsForAgentByCountryID(AID, DDL_Edit_CountryList.SelectedValue, DDL_Edit_BankList.SelectedValue);

            DDL_Edit_AccountList.Items.Add(new ListItem("Select Account", ""));
            foreach(var AccountID in AccountList)
            {
                DDL_Edit_AccountList.Items.Add(new ListItem(AccountID, AccountID));
            }

            DDL_Edit_AccountList.SelectedValue = eachSetting[2].ToString();
        }

        private void setHiddenValues(String AID, String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT BankID FROM dbo.AgentForeignBankSettings WHERE AgentID = " + AID + " AND CountryID = " + CID + " AND DefaultBank = 'Y'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        output = sdr["BankID"].ToString();
                    }
                }
                conn.Close();
            }

            hidden_AgentID.Value = AID;
            hidden_BankID.Value = output;
        }

        private void setAllBankStats(String AID, String CID, String BID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            classes.Banking BKN = new classes.Banking();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT COUNT(*) AS Total FROM dbo.Transactions WHERE Status = 'AWAITING TRANSFER' 
                AND AgentID = " + AID + " AND CountryID = " + CID + " AND BankID = '" + BID + "'";
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TableRow tRow = new TableRow();
                        TableCell tcell1 = new TableCell();
                        TableCell tcell2 = new TableCell();

                        tcell1.Text = BKN.getBankNameFromBankCode(BID);
                        tcell2.Text = sdr["Total"].ToString();
                        tRow.Cells.Add(tcell1);
                        tRow.Cells.Add(tcell2);
                        tbl_AllBankStats.Rows.Add(tRow);
                    }
                }
            }
        }

        protected void DDL_CountryList_Change(object sender, EventArgs e)
        {
           if (DDL_CountryList.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select a country');", true);
            }
           else
            {
                String SelectedCountryValue = DDL_CountryList.SelectedValue;
                classes.Banking BKN = new classes.Banking();
                List<String> BankIDList = BKN.getForeignBankListForAgentByCountryID(Session["AgentID"].ToString(), SelectedCountryValue);

                //TBL_BankList.Controls.Clear();
               
                DDL_BankList.Items.Clear();

                DDL_BankList.Items.Add(new ListItem("Select Bank", ""));
                foreach (var BankID in BankIDList)
                {
                    //TableRow tr = new TableRow();
                    //TableCell td = new TableCell();
                    //TextBox tb = new TextBox();

                    String BankName = BKN.getBankNameFromBankID(BankID);
                    DDL_BankList.Items.Add(new ListItem(BankName, BankID));

                    // td.Text = BankName;
                    //td.ID = "TD_" + BankName;
                    //td.Attributes.Add("onclick", "showBankStats(" + Session["AgentID"].ToString() + ", " + BankID + ");");
                    // td.Style.Add("cursor", "pointer");
                    // tr.Controls.Add(td);
                    // TBL_BankList.Controls.Add(tr);
                }
                SetDefaultBank.Style.Add("display", "none");
                mask.Style.Add("display", "none");
            }
            
        }

        protected void DDL_BankList_Change(object sender, EventArgs e)
        {
            
            if (DDL_BankList.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select a bank');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "clearTable", "clearDataTable();", true);
                BankID.Value = DDL_BankList.SelectedValue;
                String SelectedCountryValue = DDL_CountryList.SelectedValue;
                classes.Banking BKN = new classes.Banking();
                List<String> AccountIDList = BKN.getForeignBankListAccountsForAgentByCountryID(Session["AgentID"].ToString(), SelectedCountryValue, DDL_BankList.SelectedValue);



                DDL_AccountList.Items.Clear();

                DDL_AccountList.Items.Add(new ListItem("Select Account", ""));
                foreach (var AccountID in AccountIDList)
                {

                    DDL_AccountList.Items.Add(new ListItem(AccountID, AccountID));
                }
                SetDefaultBank.Style.Add("display", "none");
                mask.Style.Add("display", "none");
            }

            
        }

        protected void DDL_AccountList_Change(object sender, EventArgs e)
        {
            if (DDL_AccountList.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select an account');", true);
            }
            else
            {
                classes.Currencies CUR = new classes.Currencies();
                String SelectedAccount = DDL_AccountList.SelectedValue;
                String SelectedCountryValue = DDL_CountryList.SelectedValue;
                String SelectedBankID = DDL_BankList.SelectedValue;
                classes.Banking BKN = new classes.Banking();
                classes.Agent AGT = new classes.Agent();
                float AccTotalTrans = 0;
                String tempAccTotalTrans = String.Empty;
                float AccTotalAUD = 0;
                String tempAccTotalAUD = String.Empty;
                float AccTotalForeign = 0;
                String tempAccTotalForeign = String.Empty;


                String FBAN = String.Empty;
                FBAN = BKN.getAgentFBSettingIDFromAgentIDCountryIDandAccountID(Session["AgentID"].ToString(), SelectedCountryValue, SelectedBankID, SelectedAccount);

                BankName.InnerText = DDL_BankList.SelectedItem.ToString();

                String LinkedCurrencies = CUR.getLinkedCurrenciesForAgentForeignBankSetting(FBAN);
                
                String[] Currency = LinkedCurrencies.Split(',');

                String currencyString = String.Empty;
                String sendingCurrencyString = String.Empty;
                foreach (var Cur in Currency)
                {
                    currencyString += CUR.getCurrencyCodeFromID(Cur) + " - ";
                    sendingCurrencyString += "\'" + CUR.getCurrencyCodeFromID(Cur) + "\',";
                }
                int Length = currencyString.Length;
                if (Length != 0)
                {
                    currencyString = currencyString.Substring(0, (Length - 3));
                    sendingCurrencyString = sendingCurrencyString.Substring(0, (Length - 1));
                }
                CurrencyList.Value = sendingCurrencyString;
                String[] CurrencyCodes = currencyString.Split('-');
                TableRow trow2 = new TableRow();
                foreach (var Cur in CurrencyCodes)
                {
                    TableRow trow = new TableRow();
                    TableCell cell1 = new TableCell();
                    TableCell cell2 = new TableCell();

                    TableCell tcell = new TableCell();
                    tcell.Text = Cur;
                    trow2.Cells.Add(tcell);

                    tempAccTotalTrans = AGT.getAgentTransTotalBasedOnAgentIDAndCurrencyCode(Session["AgentID"].ToString(), Cur.Trim(), DDL_BankList.SelectedValue.ToString());
                    if (!String.IsNullOrEmpty(tempAccTotalTrans.ToString()))
                    {
                        AccTotalTrans += float.Parse(tempAccTotalTrans);
                    }
                    else
                    {
                        tempAccTotalTrans = "0.00";
                    }
                     
                    tempAccTotalAUD = AGT.getAgentAUDTotalBasedOnAgentIDandCurrencyCode(Session["AgentID"].ToString(), Cur.Trim(), DDL_BankList.SelectedValue.ToString());
                    if (!String.IsNullOrEmpty(tempAccTotalAUD.ToString()))
                    {
                        AccTotalAUD += float.Parse(tempAccTotalAUD);
                    }
                    else
                    {
                        tempAccTotalAUD = "0.00";
                    }

                    tempAccTotalForeign = AGT.getAgentAUDTotalBasedOnAgentIDandCurrencyCodeInLocalCurrency(Session["AgentID"].ToString(), Cur.Trim(), DDL_BankList.SelectedValue.ToString());
                    if (!String.IsNullOrEmpty(tempAccTotalForeign.ToString()))
                    {
                        AccTotalForeign += float.Parse(tempAccTotalForeign);
                    }
                    else
                    {
                        tempAccTotalForeign = "0.00";
                    }

                    cell1.Text = String.Format("{0:N2}", String.Format("{0:N2}", float.Parse(tempAccTotalForeign)));
                    cell2.Text = Cur;
                    trow.Cells.Add(cell1);
                    trow.Cells.Add(cell2);
                    tbl_IndiCurr.Rows.Add(trow);
                }
                Tbl_Currencies.Rows.Add(trow2);
                lbl_AccTotalTrans.Text = AccTotalTrans.ToString();
                lbl_AccTotalAUD.Text = String.Format("{0:N2}", float.Parse(AccTotalAUD.ToString()));
                LBL_AccountNumber.Text = DDL_AccountList.SelectedItem.ToString();
                SetDefaultBank.Style.Add("display", "none");
                mask.Style.Add("display", "none");
                ScriptManager.RegisterStartupScript(this, GetType(), "openDataTable", "openDataTable();", true);
            }

            
        }

        protected void DDL_Edit_CountryList_Change(object sender, EventArgs e)
        {
            classes.Banking BKN = new classes.Banking();
            List<String> BankIDList = BKN.getForeignBankListForAgentByCountryID(Session["AgentID"].ToString(), DDL_Edit_CountryList.SelectedValue);

            if (!IsPostBack)
            {
                DDL_Edit_BankList.Items.Clear();
            }
            DDL_Edit_BankList.Items.Add(new ListItem("Select Bank", ""));
            foreach (var BankID in BankIDList)
            {
                //TableRow tr = new TableRow();
                //TableCell td = new TableCell();
                //TextBox tb = new TextBox();

                String BankName = BKN.getBankNameFromBankID(BankID);
                DDL_Edit_BankList.Items.Add(new ListItem(BankName, BankID));

                // td.Text = BankName;
                //td.ID = "TD_" + BankName;
                //td.Attributes.Add("onclick", "showBankStats(" + Session["AgentID"].ToString() + ", " + BankID + ");");
                // td.Style.Add("cursor", "pointer");
                // tr.Controls.Add(td);
                // TBL_BankList.Controls.Add(tr);
            }
           
        }

        protected void DDL_Edit_BankList_Change(object sender, EventArgs e)
        {
            String SelectedCountryValue = DDL_Edit_CountryList.SelectedValue;
            classes.Banking BKN = new classes.Banking();
            List<String> AccountIDList = BKN.getForeignBankListAccountsForAgentByCountryID(Session["AgentID"].ToString(), SelectedCountryValue,DDL_Edit_BankList.SelectedValue);



            DDL_Edit_AccountList.Items.Clear();

            DDL_Edit_AccountList.Items.Add(new ListItem("Select Account", ""));
            foreach (var AccountID in AccountIDList)
            {

                DDL_Edit_AccountList.Items.Add(new ListItem(AccountID, AccountID));
            }
           
        }

        protected void DDL_Edit_BankList_SelectedIndexChanged(object sender, EventArgs e)
        {
            String SelectedCountryValue = DDL_Edit_CountryList.SelectedValue;
            classes.Banking BKN = new classes.Banking();
            List<String> AccountIDList = BKN.getForeignBankListAccountsForAgentByCountryID(Session["AgentID"].ToString(), SelectedCountryValue, DDL_Edit_BankList.SelectedValue);



            DDL_Edit_AccountList.Items.Clear();

            DDL_Edit_AccountList.Items.Add(new ListItem("Select Account", ""));
            foreach (var AccountID in AccountIDList)
            {

                DDL_Edit_AccountList.Items.Add(new ListItem(AccountID, AccountID));
            }
           
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            SetDefaultBank.Style.Add("display", "block");
          
            mask.Style.Add("display", "block");
           
        }

        protected void BTN_SaveSettings_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String SqlStmt1 = "UPDATE dbo.AgentForeignBankSettings Set DefaultBank = 'N' WHERE AgentID = " + Session["AgentID"].ToString();

            using (SqlCommand cmd = new SqlCommand(SqlStmt1))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            String SqlStmt2 = @"UPDATE dbo.AgentForeignBankSettings SET DefaultBank = 'Y' WHERE AgentID = " + Session["AgentID"].ToString() + 
                " AND CountryID=@CountryID AND BankID=@BankCode AND ForeignBankAccountNumber=@ForeignBankAccountNumber";

            using (SqlCommand cmd = new SqlCommand(SqlStmt2))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.Parameters.Add("@CountryID", SqlDbType.NVarChar).Value = DDL_Edit_CountryList.SelectedValue;
                cmd.Parameters.Add("@BankCode", SqlDbType.NVarChar).Value = DDL_Edit_BankList.SelectedValue;
                cmd.Parameters.Add("@ForeignBankAccountNumber", SqlDbType.NVarChar).Value = DDL_Edit_AccountList.SelectedValue;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            DDL_Edit_BankList.Items.Clear();
            DDL_Edit_AccountList.Items.Clear();
            //buildCountryDropdownList(Session["AgentID"].ToString());
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "BankSaved()", true);
            classes.Banking BKN = new classes.Banking();
            SetDefaultBank.Style.Add("display", "none");
            mask.Style.Add("display", "none");
            buildCountryDropdownList(Session["AgentID"].ToString());
            String hasDefaultBank = BKN.confirmBankViewDefaultExist(Session["AgentID"].ToString());
            classes.Agent AGT = new classes.Agent();
            lbl_TotalTXNS.Text = AGT.getAgentAwaitingTransTotal(Session["AgentID"].ToString());
            lbl_TotalAUD.Text = String.Format("{0:N2}", float.Parse(AGT.getAgentAUDAwaitingTransTotal(Session["AgentID"].ToString())));

            if (hasDefaultBank == "true")
            {
                setDefaultCountry(Session["AgentID"].ToString());
                setDefaultBank(Session["AgentID"].ToString());
                setDefaultAccount(Session["AgentID"].ToString());
                setDefaultStatsOnScreen();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "showsuccess('Bank Settings Successfully Saved');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "closedialog", "Close();", true);

        }

        protected void btn_CreateBankFile_Click1(object sender, EventArgs e)
        {
            if (lbl_AccTotalTrans.Text == "0")
            {
                Response.Write("<script>alert('You do not have any transactions for this bank.');</script>");
            }
            else
            {
                String selectedBank = DDL_BankList.SelectedValue;
                String result = String.Empty;

                classes.BankFileCreation BFC = new classes.BankFileCreation();
                classes.Agent AGT = new classes.Agent();
                if (selectedBank == "7010")
                {
                    result = BFC.createBOCBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }
                else if (selectedBank == "7056")
                {
                    result = BFC.createCOMBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }
                else if (selectedBank == "7719")
                {
                    result = BFC.createNSBBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }
                else if (selectedBank == "7278")
                {
                    result = BFC.createSAMBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }
                else if (selectedBank == "7287")
                {
                    result = BFC.createSEYBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }
                else if (selectedBank == "7083")
                {
                    result = BFC.createHNBBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }
                else if (selectedBank == "7454")
                {
                    result = BFC.createDFCCBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }
                else if (selectedBank == "7135")
                {
                    result = BFC.createPBBankFile(DDL_CountryList.SelectedValue, DDL_BankList.SelectedValue, DDL_AccountList.SelectedValue);
                }


                String[] splitresult = result.Split('^');

                if (splitresult[0] == "File Created")
                {
                    lbl_Message.Text = "Bank File Successfully Created";
                    lbl_LastFileCreation.Text = AGT.getAgentLastBankFileCreatedDate(Session["AgentID"].ToString());
                    lbl_TotalTXNS.Text = AGT.getAgentAwaitingTransTotal(Session["AgentID"].ToString());

                    //lbl_TotalAUD.Text = String.Format("{0:N2}", float.Parse(AGT.getAgentAUDAwaitingTransTotal(Session["AgentID"].ToString())));
                    setDefaultStatsOnScreen();

                    
                    String path = "~/BankFileCreation/" + splitresult[1];
                    string redirect = "<script>window.open('/FileDownloader.aspx?file=" + splitresult[1] + "&RC=BANK','_blank');</script>";
                    Response.Write(redirect);

                    //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Bank File Created Successfully');", true);
                }
                else
                {
                    lbl_Message.Text = "Error in creating bank file. Please contact your system administrator";
                    //Response.Write("<script>alert('Error in creating bank file. Please contact your system administrator');</script>");
                }
            }


            
        }
    }
}