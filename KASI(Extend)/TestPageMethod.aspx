﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestPageMethod.aspx.cs" Inherits="KASI_Extend_.TestPageMethod" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Button ID="clickMe" runat="server" Text="Click Me"  OnClick="ClickMe_Click"/>
        <asp:FileUpload ID="FileUpload1" runat="server" />

         <asp:Button ID="Button1" runat="server" Text="Encrypt Me"  OnClick="EncryptFile"/>

         <asp:Button ID="Button2" runat="server" Text="Decrypt Me"  OnClick="DecryptFile"/>

          <asp:Button ID="Button3" runat="server" Text="Test Bae 64"  OnClick="checkBase64"/>
    </div>
    </form>
</body>
</html>
