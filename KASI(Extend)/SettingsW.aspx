﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentControl/Initial.Master" AutoEventWireup="true" CodeBehind="SettingsW.aspx.cs" Inherits="KASI_Extend_.SettingsW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/tag-it.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
     <link href="css/toastr.min.css" rel="stylesheet" />
    <script type="text/javascript" charset="utf-8" src="js/jquery.circliful.min.js"></script>
     <script src="js/toastr.min.js"></script>
    <link href="css/jquery-ui.settings.css" rel="stylesheet" />
    <link href="css/datatables.settings.css" rel="stylesheet" />
    <link href="css/jquery.tagit.css" rel="stylesheet" />
    <link href="css/jquery.circliful.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />

    <script type="text/javascript">
        function updateCurrenciesforForeignBank() {
            var hidid= $("#hidFBAccid").val();
            var country = $("#ddl_EditFBSelectCounty").val();
            var bank = $("#ddl_EditFBSelectBank").val();
            var account = $("#ag_edit_bankaccountnumber").val();
            var currencies = "";
            var curArr = $("#ddl_edit_AllowedCurrencies").val();
            $.each(curArr, function (index, value) {
                currencies = currencies + value + ",";
            });
            $('#tbl_BankRouting').DataTable().destroy();
            $.ajax({
                url: 'Processor/updateForeignAccountCurrency.aspx',
                data: { cid: country, bid: bank, acc: account, FBAccID: hidid, curids: currencies },
                async: false,
                success: function (response) {
                    var arr = response.split('^');
                    if (arr[0] == "1") {
                        showsuccess("Updated successfully!", "");
                        DialogEditForeignBank.dialog('close');
                        loadForeignAccTable();
                       
                    }
                    else {
                        showerror(arr[1]);
                        loadForeignAccTable();
                    }
                }
            });

        }
        function editFBAcc(FBAccID)
        {
            $("#hidFBAccid").val(FBAccID);
            var CountryID;
            var CurrencyIDs;
            var BankID;
            var FBAccNumber;
            var CurrencyBuilder;
            var CurrencyList;

            $.ajax({
                url: 'JQDataFetch/getAgentAssignedCountriesByAgentID.aspx',
                async: false,
                success: function (response) {
                    $('#ddl_EditFBSelectCounty').empty();
                    var data = response.split('~');
                    data.sort();
                    $('#ddl_EditFBSelectCounty').append('<option value="">-- SELECT --</option>');
                    $.each(data, function (item) {
                        var sdata = data[item].split('|');

                        $('#ddl_EditFBSelectCounty').append('<option value="' + sdata[1] + '">' + sdata[0] + '</option>');
                    })
                }
            });

            $.ajax({
                url: 'JQDataFetch/getAgentForeignBankDetails.aspx',
                async: false,
                data: {
                    FBAccID: FBAccID
                },
                success: function (response) {
                    var data = response.split('|');
                    CountryID = data[0];
                    BankID = data[1];
                    CurrencyIDs = data[2];
                    FBAccNumber = data[3];
                    $('#ddl_EditFBSelectCounty').val(CountryID).change();
                }
            });


            $.ajax({
                url: 'JQDataFetch/getCountryFullBankList.aspx',
                async: false,
                data: {
                    CID: $('#ddl_EditFBSelectCounty :selected').val(),
                },
                success: function (response) {
                    $('#ddl_EditFBSelectBank').empty();
                    var data = response.split('~');
                    data.sort();
                    $('#ddl_EditFBSelectBank').append('<option value="">-- SELECT --</option>');
                    $.each(data, function (item) {
                        var sdata = data[item].split('|');

                        $('#ddl_EditFBSelectBank').append('<option value="' + sdata[1] + '">' + sdata[0] + '</option>');
                        $('#ddl_EditFBSelectBank').val(BankID).change();
                    })
                }
            });

            $.ajax({
                url: 'JQDataFetch/getAgentCurrencies.aspx',
                async: false,
                success: function (response) {
                    $('#ddl_edit_AllowedCurrencies').empty();
                    var data = response.split('~');
                    data.sort();
                    $.each(data, function (item) {
                        var sdata = data[item].split('|');
                        $("#ddl_edit_AllowedCurrencies").append('<option value="' + sdata[2] + '">' + sdata[1] + '</option>');
                       
                    });
                }
            });
            CurrencyBuilder = CurrencyIDs.split(',');
           // alert(CurrencyIDs);
            $('#ddl_edit_AllowedCurrencies').val(CurrencyBuilder);
           // $('#ddl_edit_AllowedCurrencies').trigger("change");


            $("#ag_edit_bankaccountnumber").val(FBAccNumber);
            DialogEditForeignBank.dialog('open');
        }


        $(document).ready(function () {
            $("[id$=txtRegDate]").datepicker({ dateFormat: 'dd/mm/yy' });
            $("[id$=txtRenDate]").datepicker({ dateFormat: 'dd/mm/yy' });

            $('#MCEdit_Telephone').mask('(00) 0000 0000');
            $('#MCEdit_Fax').mask('(00) 0000 0000')
            $('#MCEdit_State').mask('AAA');
            $('#MCEdit_Postcode').mask('0000');


        });

       
        function showerror(msg) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.error(msg, 'Error');

        }
        function showsuccess(msg, body) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.success(msg, body);

        }

        function SubmitNewDetails() {
            var regNumber = $("[id$=txtRegNumber]").val();
            var regDate = $("[id$=txtRegDate]").val();
            var renewDate = $("[id$=txtRenDate]").val();
            var agentId = $("[id$=hidden_AgentID]").val();
            $.ajax({
                url: 'Processor/UpdateNewDetails.aspx',
                data: {
                    reg: regNumber,
                    dat: regDate,
                    newd: renewDate,
                    aid: agentId
                },
                success: function (response) {
                    if (response == "1") {
                        $("[id$=AustracNumber]").html(regNumber);
                        $("[id$=AustracRegDate]").html(regDate);
                        $("[id$=AustracRenDate]").html(renewDate);
                        showsuccess("Updated successfully", "");
                        DialogAddNewBankAccount.dialog('close');
                    }
                    else {
                        var arr = response.split('^');
                        showerror(arr[1]);
                    }
                }
            })

        }
        $(document).ready(function () {
            $.ajaxSetup({
                beforeSend: function () {

                    $(".loading").show();
                },
                complete: function () {
                    $(".loading").hide();
                }
            });
            $('#div_settings02').hide();
            $('#div_settings03').hide();
            // Main Tabs 
            $('#BankingTab').hide();
            $('#BankingTab').hide();
            $('#ThirdMainTab').hide();
            $('#FourthMainTab').hide();
            $('#FifthMainTab').hide();
            $('#DIV_AgentCommission').hide();
            $('#DIV_AgentBanking').hide();
            $('#DIV_AgentMasterUser').hide();
            $("#SMTPTab").hide();
            $('#btn_AddNewSMTP').hide();
            $("#TR_NOSMTPDetails").hide();

           

          

            $("#btnSubmitNewDetails").click(function () {
                DialogAddNewBankAccount.dialog('open');
            });

            $("#btnEditTransferLimits").click(function () {
                DialogEditTransferLimits.dialog('open');
            });

            $('#GlobalSettingsClick').click(function () {
                $('#div_settings01').show();
                $('#div_settings02').hide();
                $('#div_settings03').hide();
            });

            $('#btn_EditSMTP').click(function () {
                DialogEditSMTP.dialog('open');
            })

            $('#AgentSettingsClick').click(function () {
                $('#div_settings01').hide();
                $('#div_settings02').show();
                $('#div_settings03').hide();
            });

            $('#ForeignBankClick').click(function () {
                $('#div_settings01').hide();
                $('#div_settings02').hide();
                $('#div_settings03').show();
            });

            $("#btn_TestSMTP").click(function () {
                $("#SMTP_Result_TR").show();
                $("#SMTPTestResult").text('Please wait. Testing SMTP Server settings.');
                $.ajax({
                    url: 'JQDataFetch/testSMTPServer.aspx',
                    data: {
                        SMTPServer: $('#EDIT_SMTPName').val(),
                        SMTPPort: $('#EDIT_SMTPPort').val(),
                        UName: $('#EDIT_SMTPUsername').val(),
                        PWord: $('#EDIT_SMTPPassword').val(),
                        SSL: $('#EDIT_SSLEnabled :selected').val(),
                        SendEmail: $('#EDIT_SMTPEmailAddress').val()
                    },
                    success: function (response) {
                        $('#SMTP_Result_TR').show();
                        $('#SMTPTestResult').text(response);
                    }
                })
            });



            // Settings Main Tab 01 Function
            $('#TD_Agent').click(function () {
                $('#AgentTab').show();
                $('#BankingTab').hide();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("set_tab_active");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("set_tab_inactive_right");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("set_tab_inactive_right");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("set_tab_inactive_right");
                //$('#TD_MainTab05').removeClass();
                //$('#TD_MainTab05').addClass("set_tab_inactive_right");
                $("#SMTPTab").hide();
            })

            // Settings Main Tab 02 Function
            $('#TD_Banking').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').show();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("set_tab_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("set_tab_active");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("set_tab_inactive_right");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("set_tab_inactive_right");
                //$('#TD_MainTab05').removeClass();
                //$('#TD_MainTab05').addClass("set_tab_inactive_right");
                $("#SMTPTab").hide();
            });

            // Settings Main Tab 03 Function
            $('#TD_MainTab03').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').hide();
                $('#ThirdMainTab').show();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("set_tab_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("set_tab_inactive_left");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("set_tab_active");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("set_tab_inactive_right");
                //$('#TD_MainTab05').removeClass();
                //$('#TD_MainTab05').addClass("set_tab_inactive_right");
                $("#SMTPTab").hide();
            });

            // Settings Main Tab 04 Function
            $('#TD_MainTab04').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').hide();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').show();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("set_tab_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("set_tab_inactive_left");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("set_tab_inactive_left");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("set_tab_active");
                //$('#TD_MainTab05').removeClass();
                //$('#TD_MainTab05').addClass("set_tab_inactive_right");
                $("#SMTPTab").show();
            });

            // Settings Main Tab 05 Function
            $('#TD_MainTab05').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').hide();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').show();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("set_tab_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("set_tab_inactive_left");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("set_tab_inactive_left");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("set_tab_inactive_left");
                //$('#TD_MainTab05').removeClass();
                //$('#TD_MainTab05').addClass("set_tab_active");
                $("#SMTPTab").hide();
            });

            $('#TD_AgentDetails').click(function () {
                $('#DIV_AgentDetails').show();
                $("#DIV_AgentCommission").hide();
                $('#DIV_AgentBanking').hide();
                $('#DIV_AgentMasterUser').hide();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("aa_tab_active");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("aa_tab_inactive_right");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("aa_tab_inactive_right");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("aa_tab_inactive_right");
            });

            $('#TD_AgentCommission').click(function () {
                $('#DIV_AgentDetails').hide();
                $("#DIV_AgentCommission").show();
                $('#DIV_AgentBanking').hide();
                $('#DIV_AgentMasterUser').hide();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("aa_tab_inactive_left");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("aa_tab_active");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("aa_tab_inactive_right");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("aa_tab_inactive_right");
            });

            $("#btn_ag_ExGain").click(function () {
                $("#ag_add_CommStruc").val('FXGain');
                $('#btn_ag_AUDGain').removeClass();
                $('#btn_ag_AUDGain').addClass("aa_select_btn_inactive");
                $('#btn_ag_ExGain').removeClass();
                $('#btn_ag_ExGain').addClass("aa_select_btn_active");
            });

            $('#TD_AgentBanking').click(function () {
                $('#DIV_AgentDetails').hide();
                $("#DIV_AgentCommission").hide();
                $('#DIV_AgentBanking').show();
                $('#DIV_AgentMasterUser').hide();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("aa_tab_inactive_left");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("aa_tab_inactive_left");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("aa_tab_inactive_right");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("aa_tab_active");
            });

            $('#TD_AgentMasterUser').click(function () {
                $('#DIV_AgentDetails').hide();
                $("#DIV_AgentCommission").hide();
                $('#DIV_AgentBanking').hide();
                $('#DIV_AgentMasterUser').show();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("aa_tab_inactive_left");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("aa_tab_inactive_left");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("aa_tab_active");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("aa_tab_inactive_left");
            });

            $("#btn_ag_AUDGain").click(function () {
                $("#ag_add_CommStruc").val('AUDGain');
                $('#btn_ag_AUDGain').removeClass();
                $('#btn_ag_AUDGain').addClass("aa_select_btn_active");
                $('#btn_ag_ExGain').removeClass();
                $('#btn_ag_ExGain').addClass("aa_select_btn_inactive");
            });

            $('#btn_edittransferfees').click(function () {

                $.ajax({
                    url: 'JQDataFetch/getAgentAssignedCountriesByAgentID.aspx',
                    data: {
                        AID: $('#<%=hidden_AgentID.ClientID%>').val(),
                    },
                    success: function (response) {
                        var initialdata = response.split('~');
                        initialdata.sort();
                        $('#TRF_CountryList').empty();
                        $('#TRF_CountryList').append('<option value="">-- SELECT -- </option>');
                        $.each(initialdata, function (item) {
                            var datasplit = initialdata[item].split('|');
                            $('#TRF_CountryList').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                        });
                    }
                })

               

                DialogEditTransferFees.dialog('open');
            });

            $('#TRF_CountryList').change(function () {
                $.ajax({
                    url: 'JQDataFetch/getAgentCurrenciesForTransferFeeEdit.aspx',
                    data: {
                        CID: $('#TRF_CountryList :selected').val(),
                    },
                    success: function (response) {
                        var data = response.split("~");
                        data.sort();
                        $('#TRF_CurrencyList').empty();
                        $('#TRF_CurrencyList').append('<option value="">-- SELECT -- </option>');
                        $.each(data, function (item) {
                            var fullsplit = data[item].split('|');
                            $('#TRF_CurrencyList').append('<option value="' + fullsplit[1] + '">' + fullsplit[0] + '</option>');
                        })
                    }
                })
            });

            $("#TRF_CurrencyList").change(function () {
                $.ajax({
                    url: 'JQDataFetch/getAgentTransferTiers.aspx',
                    data: {
                        CRID: $('#TRF_CurrencyList :selected').val(),
                        CTID: $('#TRF_CountryList :selected').val()
                    },
                    success: function (response) {
                        data = response.split('|');
                        $("#TRF_Tier1Value").val(data[1]);
                        $("#TRF_Tier1To").val(data[0]);
                        $("#TRF_Tier2To").val(data[2]);
                        $('#TRF_Tier2Value').val(data[3]);
                        $("#TRF_Tier3Value").val(data[5]);
                        $("#TRF_UpperLimit").val(data[4]);
                    }
                })
            });

            $("#btn_SaveTransferFees").click(function () {
                $.ajax({
                    url: 'Processor/UpdateTransferFees.aspx',
                    data: {
                        CURID: $('#TRF_CountryList :selected').val(),
                        COUTID: $('#TRF_CurrencyList :selected').val(),
                        T1A: $('#TRF_Tier1Value').val(),
                        T1T: $('#TRF_Tier1To').val(),
                        T2A: $('#TRF_Tier2Value').val(),
                        T2T: $('#TRF_Tier2To').val(),
                        T3A: $('#TRF_Tier3Value').val(),
                    },
                    success: function (response) {
                        alert('Transfer Fees Updated Successfully.');
                        DialogEditTransferFees.dialog('close');
                    }
                })
            });

            $('#btn_AddNewAccount').click(function () {
                DialogAddNewBankAccount.dialog('open');
            });

            $('#btn_AddNewFAccount').click(function () {

                $.ajax({
                    url: 'JQDataFetch/getAgentCurrencies.aspx',
                    async: false,
                    success: function (response) {
                        $('#ddl_AllowedCurrencies').empty();
                        var data = response.split('~');
                        data.sort();
                        $.each(data, function (item) {
                            var sdata = data[item].split('|');
                            $("#ddl_AllowedCurrencies").append('<option value="' + sdata[1] + '">' + sdata[1] + '</option>');
                        });
                    }
                });

                


                $.ajax({
                    url: 'JQDataFetch/getAgentAssignedCountriesByAgentID.aspx',
                    async: false,
                    success: function (response) {
                        $('#ddl_FBSelectCounty').empty();
                        var data = response.split('~');
                        data.sort();
                        $('#ddl_FBSelectCounty').append('<option value="">-- SELECT --</option>');
                        $.each(data, function (item) {
                            var sdata = data[item].split('|');
                            
                            $('#ddl_FBSelectCounty').append('<option value="' + sdata[1] + '">' + sdata[0] + '</option>');
                        })
                    }
                })
                DialogForeignBanks.dialog('open');
            });

            $('#btn_SaveSMTPEdit').click(function () {
                $.ajax({
                    url: 'Processor/UpdateAffiliateSMTPSettings.aspx',
                    beforeSend: function () {
                        //LoadingDIV.dialog('open');
                    },
                    data: {
                        Name: $('#EDIT_SMTPName').val(),
                        Port: $('#EDIT_SMTPPort').val(),
                        Email: $('#EDIT_SMTPEmailAddress').val(),
                        SSL: $("#EDIT_SSLEnabled :selected").val(),
                        UName: $('#EDIT_SMTPUsername').val(),
                        PWord: $('#EDIT_SMTPPassword').val()
                    },
                    complete: function () {
                        //LoadingDIV.dialog('close');
                    },
                    success: function (response) {
                        alert(response);
                        alert('SMTP Server details updated successfully');
                        DialogEditSMTP.dialog('close');
                    }
                })
            });

            $('#ddl_FBSelectCounty').change(function () {
                $.ajax({
                    url: 'JQDataFetch/getCountryFullBankList.aspx',
                    async: false,
                    data: {
                        CID: $('#ddl_FBSelectCounty').val(),
                    },
                    success: function (response) {
                        $('#ddl_FBSelectBank').empty();
                        var data = response.split('~');
                        data.sort();
                        $('#ddl_FBSelectBank').append('<option value="">-- SELECT --</option>');
                        $.each(data, function (item) {
                            var sdata = data[item].split('|');
                            
                            $('#ddl_FBSelectBank').append('<option value="' + sdata[1] + '">' + sdata[0] + '</option>');
                        })
                    }
                })
            })

            $('#btn_AddNewCountry').click(function () {
                DialogAddNewCountry.dialog('open');
            });

            $('#btn_SaveNewAgent').click(function () {
                $.ajax({
                    url: 'Processor/AddAgent.aspx',
                    data: {
                        AgName: $("#ag_add_agentname").val(),
                        AgAddLine1: $('#ag_add_addressline1').val(),
                        AgAddLine2: $('#ag_add_addressline2').val(),
                        AgSuburb: $('#ag_add_Suburb').val(),
                        AgState: $('#ag_add_State').val(),
                        AgPostcode: $('#ag_add_Postcode').val(),
                        AgPhone: $('#ag_add_Phone').val(),
                        AgFax: $('#ag_add_Fax').val(),
                        AgEmail: $('#ag_add_Email').val(),
                        AgABN: $('#ag_add_ABN').val(),
                        AgACN: $('#ag_add_ACN').val(),
                        AgFlatFee: $('#ag_add_FlatFee').val(),
                        AgAddPerc: $('#ag_add_AddPerc').val(),
                        AgCommStruc: $('#ag_add_CommStruc').val(),
                        AgCommPerc: $('#ag_add_CommPerc').val(),
                        AgCountries: $('#ag_add_Countries').val(),
                        AgMUName: $('#ag_add_MUName').val(),
                        AgMUPhone: $('#ag_add_MUPhone').val(),
                        AgMUMobile: $('#ag_add_MUMobile').val(),
                        AgMUEmail: $('#ag_add_MUEmail').val(),
                    },
                    success: function (response) {
                        alert('Agent Added Successfully');
                        AgentTbl.ajax.reload();
                        DialogAddNewAgent.dialog('close');
                    }
                })
            });

            $('#btn_SaveNewForeignBankAccount').click(function () {

                var str = "";
                $("#ddl_AllowedCurrencies option:selected").each(function () {
                    str += $(this).text() + " ";
                });
                //alert(str);
                $.ajax({
                    url: 'Processor/AddAgentForeignBankSettings.aspx',
                    async: false,
                    data: {
                        CID: $('#ddl_FBSelectCounty').val(),
                        BID: $('#ddl_FBSelectBank').val(),
                        AccID: $('#ag_add_bankaccountnumber').val(),
                        Curr: str,
                    },
                    success: function (response) {
                        alert('Foreign Bank Account Added Successfully');
                        $('#ddl_FBSelectBank').val(0);
                        $('#ddl_FBSelectCounty').val(0);
                        $('#ag_add_bankaccountnumber').val('');
                        $('#ddl_AllowedCurrencies option:selected').prop("selected", false);
                        CountryTbl.ajax.reload();
                        DialogForeignBanks.dialog('close');
                    }
                })


                
            })

            $('#BTN_Compliance').click(function () {
                DialogEditTransferLimits.dialog('open');
            });

            $('#btn_SetDefaultRoutingBanks').click(function () {
                $.ajax({
                    url: 'JQDataFetch/getAgentAssignedCountriesByAgentID.aspx',
                    async: false,
                    success: function (response) {
                        var data = response.split('~');
                        data.sort();
                        $('#ddl_DRBSelectCounty').empty();
                        $('#ddl_DRBSelectCounty').append('<option value="">-- SELECT -- </option>');
                        $.each(data, function (item) {
                            var sdata = data[item].split('|');
                            $('#ddl_DRBSelectCounty').append('<option value="' + sdata[1] + '">' + sdata[0] + '</option>');
                        });
                    }
                })
                DialogSetDefaultRoutingBanks.dialog('open');
            });

            $('#ddl_DRBSelectCounty').change(function () {
                $.ajax({
                    url: 'JQDataFetch/getCountryFullBankList.aspx',
                    async: false,
                    data: {
                        CID: $('#ddl_DRBSelectCounty').val(),
                    },
                    success: function (response) {
                        $('#ddl_DRBSelectBank').empty();
                        var data = response.split('~');
                        data.sort();
                        $('#ddl_DRBSelectBank').append('<option value="">-- SELECT --</option>');
                        $.each(data, function (item) {
                            var sdata = data[item].split('|');

                            $('#ddl_DRBSelectBank').append('<option value="' + sdata[1] + '">' + sdata[0] + '</option>');
                        })
                    }
                })
            })

            DialogEditTransferFees = $('#DIV_TransferFeeTiers').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "EDIT TRANSFER FEE TIERS",
            });

            DialogEditForeignBank = $('#DIV_EditForeignAccount').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "EDIT FOREIGN BANK ACCOUNT",
            });

            DialogEditSMTP = $('#div_EditSMTP').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "EDIT SMTP SETTINGS",
            });

            DialogEditSMTP.parent().appendTo($('form:first'));


            DialogForeignBanks = $('#div_AddNewForeignBank').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Add New Foreign Bank Account",
            });

            DialogSetDefaultRoutingBanks = $('#div_SetDefaultRoutingBanks').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Set Default Routing Banks",
            })

            DialogAddAgentCurrencies = $('#DIV_AddAgentCurrencies').dialog({
                autoOpen: false,
                modal: true,
                width: 980,
            });
            //doing
            DialogEditMasterCompany = $('#DIV_EditTrTiercompany').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Edit Company Details",
            });
            DialogEditTransferFeeTier = $('#DIV_Editcompany').dialog({
                autoOpen: false,
                modal: true,
                width: 680,
                title: "EDIT Transfer Tier",
            });
            DialogAddNewAgent = $('#DIV_AddNewAgent').dialog({
                autoOpen: false,
                modal: true,
                width: 680,
                title: "ADD NEW AGENT",
            });

            DialogChangePassword = $('#ChangePassword').dialog({
                autoOpen: false,
                modal: true,
                width: 320,
                title: "CHANGE PASSWORD",
            });

            DialogAddNewBankAccount = $('#DIV_SubmitDetails').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Submit New AUSTRAC Registration Details",
            });


            DialogSubmitNewDetails = $('#DIV_AddNewBankAccount').dialog({
                autoOpen: false,
                modal: true,
                width: 680,
                title: "ADD NEW LOCAL BANK ACCOUNT",
            });

            DialogAddNewFBankAccount = $('#DIV_AddNewFBankAccount').dialog({
                autoOpen: false,
                modal: true,
                width: 680,
                title: "ADD NEW FOREIGN BANK ACCOUNT",
            });

            DialogAddNewCountry = $('#DIV_AddNewCountry').dialog({
                autoOpen: false,
                modal: true,
                width: 320,
                title: "ADD NEW COUNTRY",
            })

            $('#btn_ChangePassword').click(function () {
                DialogChangePassword.dialog('open');
            });



            $('#btn_editagentdetails').click(function () {
                $.ajax({
                    url: 'JQDataFetch/getAgentInfoForEdit.aspx',
                    async: false,
                    success: function (data) {
                        //alert(JSON.stringify(data));
                        var info = data.split('|');
                        $('#MCEdit_AddressLine1').val(info[0]);
                        $('#MCEdit_AddressLine2').val(info[1]);
                        $('#MCEdit_Suburb').val(info[2]);
                        $('#MCEdit_State').val(info[3]);
                        $('#MCEdit_Postcode').val(info[4]);
                        $('#MCEdit_Telephone').val(info[5]);
                        $('#MCEdit_Fax').val(info[6]);
                        $('#MCEdit_Email').val(info[7]);
                        $('#MCEdit_ABN').val(info[8]);
                        $('#MCEdit_ACN').val(info[9]);
                    }
                })

                DialogEditMasterCompany.dialog('open');

            });

            $('#btn_ChangePasswordSubmit').click(function () {
                if ($('#txt_NewPassword').val() != $('#txt_NewPasswordConfirm').val()) {
                    alert('Passwords do not match');
                }
                else {
                    $.ajax({
                        url: 'JQDataFetch/verifyLoginPassword.aspx',
                        data: {
                            OP: $('#txt_CurrentPassword').val(),
                        },
                        success: function (result) {
                            if (result == 'FAIL') {
                                alert('Old Password is incorrect');
                            }
                            else {
                                $.ajax({
                                    url: 'Processor/saveNewPassword.aspx',
                                    data: {
                                        NP: $('#txt_NewPassword').val(),
                                    },
                                    success: function (response) {
                                        if (response == 'OK') {
                                            alert('Password Updated Successfully');
                                            DialogChangePassword.dialog('close');
                                        }
                                        else {

                                            alert('An error has occured. Please contact your system administrator');
                                            DialogChangePassword.dialog('close');
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            })

            $('#<%=myStat.ClientID%>').circliful({
                fillColor: '#000000',
                backgroundBorderWidth: '5',
            });

            $('#btn_EditMasterComp').click(function () {
                $.ajax({
                    url: 'JQDataFetch/saveAgentSettings.aspx',
                    data: {
                        address1: $("#MCEdit_AddressLine1").val(),
                        address2: $('#MCEdit_AddressLine2').val(),
                        suburb: $('#MCEdit_Suburb').val(),
                        state: $('#MCEdit_State').val(),
                        postcode: $('#MCEdit_Postcode').val(),
                        phone: $('#MCEdit_Telephone').val(),
                        fax: $('#MCEdit_Fax').val(),
                        email: $('#MCEdit_Email').val(),
                        abn: $('#MCEdit_ABN').val(),
                        acn: $('#MCEdit_ACN').val(),
                    },
                    success: function (data) {
                        var arr = data.split('^');
                        if (arr[0] != '0') {
                            showsuccess("", "Updated sucessfully");
                            setTimeout(function () {
                                location.reload();
                            }, 2000);

                        }
                        else {
                            showerror(arr[1]);
                        }
                    }
                })
            });
            loadForeignAccTable();

            AgentTbl = $('#tbl_Agent').DataTable({
                columns: [
                    { 'data': 'AgentID' },
                    { 'data': 'AgentName' },
                    { 'data': 'AgentSuburb' },
                    { 'data': 'AgentState' },
                    { 'data': 'AgentStatus' },
                    { 'data': 'ViewEdit' },
                ],
                "columnDefs": [
                    { className: "ali_left", "targets": [0, 1, 2] },
                    { className: "ali_right", "targets": [4] },
                    { className: "ali_center", "targets": [3] },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/AgentList.ashx',
                "order": [[0, "desc"]],
            });

            BankAccountTbl = $('#tbl_BankAccount').DataTable({
                columns: [
                    { 'data': 'BankAccountID' },
                    { 'data': 'BankName' },
                    { 'data': 'BSB' },
                    { 'data': 'AccountNumber' },
                    { 'data': 'AccountName' },
                    { 'data': 'Active' },
                    { 'data': 'ViewEdit' },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/MasterCompBankList.ashx',
                "order": [[0, "desc"]],
            });

            CountryTbl = $('#tbl_Countries').DataTable({
                ajax: {
                    url: 'JQDataFetch/getAgentForeignBankList.aspx',
                },
                "order": [[0, "desc"]]
            });

            $("#BTN_AddAgentCurrencies").click(function () {
                DialogAddAgentCurrencies.dialog('open');
            });

            $('#btn_AddNewAgent').click(function () {
                $("#tbl_Agent_Banking tr").remove();
                $.ajax({
                    url: 'JQDataFetch/getDepositMethods.aspx',
                    async: false,
                    success: function (data) {
                        var splitinfo = data.split('~');
                        $.each(splitinfo, function (index, item) {
                            var spdata = splitinfo[index].split('|');
                            var row = $("<tr><td><input type='checkbox' class='chk_banks' id='" + spdata[0] + "' name='CHK_" + spdata[1] + "' value='" + spdata[0] + "' /></td><td style='width:20px;'>&nbsp;</td><td>" + spdata[1] + "</td></tr>").appendTo('#tbl_Agent_Banking');
                            var rowx = $("<tr class='spacer5'><td>&nbsp;</td><td style='width:20px;'>&nbsp;</td><td>&nbsp;</td></tr>").appendTo('#tbl_Agent_Banking');
                        });
                    }
                });

                $('#ag_add_Countries').tagit({
                    availableTags: ["Sri Lanka", "Pakistan", "India", "United Arab Emirates", "Somalia", "Ghana", "Egypt"],
                })

                DialogAddNewAgent.dialog('open');
            })
        });

        function loadForeignAccTable() {
           
            $('#tbl_BankRouting').DataTable();
        }
        </script>

    <style type="text/css">
        .auto-style1 {
            font-family: 'Varela Round', sans-serif;
            color: #666666;
            font-size: 0.6em;
            text-align: center;
            background-color: #FFF;
            height: 25px;
            vertical-align: middle;
            padding-top: 5px;
            font-weight: bold;
            width: 200px;
        }
        .fdsfd {
            font-family: 'Asap Condensed', sans-serif;
            color: #FFFFFF;
            text-align: center;
            font-size: 1.25em;
            font-weight: 600;
        }
        </style>

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form name="form1" runat="server">
        <input id="hidFBAccid" type="hidden" />
      <div class="loading" style="width:100%;height:100%;background-color:white;opacity:0.5;z-index:99999;position:fixed;display:none"></div>
    <img class="loading" style="z-index:999999;top:50%;left:50%;position:fixed;display:none" src="images/loading.gif" />

    <div class="container_top_headings">
        <table class="tbl_width_1200">
            <tr style="height:70px; vertical-align:middle;">
                <td style="vertical-align:middle;"><span class="all_headings">Settings</span></td>
                <td style="vertical-align:middle; text-align:right;"><input type="button" class="aa_btn_red" id="btn_ChangePassword" value="Change Password" /></td>
            </tr>
        </table>
    </div>

    <div></div>
    
    <div class="container_one">
        <table class="tbl_width_1200">
            <tr>
                <td style="width:280px; vertical-align:top;">
                    <table style="width:100%;">
                        
                        <tr style="background-color:#76D17F; height:250px;">
                            <td style="vertical-align:bottom;">
                                <table class="tbl_width_240">
                                    <tr><td style="text-align:center;">
                                        <asp:Image ID="Image22" runat="server" ImageUrl="~/images/id-card-comp.png" />
                                        </td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="settings_agent_com_name"><asp:Label ID="MastCompName" runat="server" Text="Label"></asp:Label></td></tr>
                                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer5"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_280">
                                    <tr style="height:80px;">
                                        <td style="background-color:#D6DFDE; vertical-align:bottom;">
                                            <table class="tbl_width_120">
                                                <tr><td class="fdsfd">
                                                    <asp:Label ID="lbl_TotalCustomer" runat="server"></asp:Label></td></tr>
                                                <tr class="spacer5"><td>Total Customers</td></tr>
                                            </table>
                                        </td>
                                        <td style="width:5px;">&nbsp;</td>
                                        <td style="background-color:#D6DFDE; vertical-align:bottom;">
                                            <table class="tbl_width_120">
                                                <tr><td class="settings_agent_com_box_name">
                                                    <div id="myStat" data-dimension="250"  data-info="New Clients" data-width="30" data-fontsize="38"  data-fgcolor="#61a9dc" data-bgcolor="#eee" data-fill="#ddd"   data-icon-size="28" data-icon-color="#fff" runat="server"></div>
                                                    </td></tr>
                                                <tr class="spacer5"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer5"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_280">
                                    <tr style="height:80px;">
                                        <td style="background-color:#D6DFDE; vertical-align:bottom;">
                                            <table class="tbl_width_120">
                                                <tr><td class="settings_agent_com_box_name">&nbsp;</td></tr>
                                                <tr class="spacer5"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                        <td style="width:5px;">&nbsp;</td>
                                        <td style="background-color:#9EADA8; vertical-align:bottom;">
                                            <table class="tbl_width_120">
                                                <tr><td class="settings_agent_com_box_name" style="color:#FFFFFF;">&nbsp;</td></tr>
                                                <tr class="spacer5"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr><td>&nbsp;</td></tr>
                        
                        <tr class="set_tab_lrb_brdr" hidden="hidden"><td><input type="text" id="hidden_AgentID" runat="server" /></td></tr>
                    </table>
                </td>
                
                <td style="width:20px;">&nbsp;</td>
                
                <td style="width:900px;">
                    <table class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_900">
                                    <tr>
                                        <td class="set_tab_active" id="TD_Agent"> <!-- SETTINGS MAIN TAB01 -->
                                            <table class="tbl_width_150">
                                                <tr>
                                                    <td class="set_tab_icon">
                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/s_agents.png" />
                                                    </td>
                                                    <td class="set_tab_text">
                                                        PROFILE
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="set_tab_inactive_right" id="TD_Banking"> <!-- SETTINGS MAIN TAB02 -->
                                            <table class="tbl_width_150">
                                                <tr>
                                                    <td class="set_tab_icon">
                                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/images/banking_g.png" />
                                                    </td>
                                                    <td class="set_tab_text">
                                                        SETTINGS
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="set_tab_inactive_right" id="TD_MainTab03"> <!-- SETTINGS MAIN TAB03 -->
                                            <table class="tbl_width_150">
                                                <tr>
                                                    <td class="set_tab_icon">
                                                        <asp:Image ID="Image4" runat="server" ImageUrl="~/images/s_inactive_gry.png" />
                                                    </td>
                                                    <td class="set_tab_text">
                                                        BANKING</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="set_tab_inactive_right" id="TD_MainTab04"> <!-- SETTINGS MAIN TAB04 -->
                                            <table class="tbl_width_150">
                                                <tr>
                                                    <td class="set_tab_icon">
                                                        <asp:Image ID="Image5" runat="server" ImageUrl="~/images/s_inactive_gry.png" />
                                                    </td>
                                                    <td class="set_tab_text">
                                                        SYSTEM</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="set_tab_inactive_blank" id="TD_MainTab05"> <!-- SETTINGS MAIN TAB05 -->
                                            <table class="tbl_width_150">
                                                <tr>
                                                    <td class="set_tab_icon">
                                                        &nbsp;</td>
                                                    <td class="set_tab_text">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>

                                        <td class="set_tab_inactive_blank" id="TD_MainTab06"> <!-- SETTINGS MAIN TAB06 -->
                                            <table class="tbl_width_150">
                                                <tr>
                                                    <td class="set_tab_icon">
                                                        
                                                    <td class="set_tab_text">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="set_tab_lr_brdr"><td style="height:25px;">&nbsp;</td></tr>
                        <tr class="set_tab_lr_brdr">
                            <td>
                                <div id="AgentTab">
                                    <table class="tbl_width_860">

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Company/Contact Details</td>
                                                        <td class="ali_right"><input class="aa_btn_green" id="btn_editagentdetails" type="button" value="Edit Company Details"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                        <tr class="spacer5"><td>&nbsp;</td></tr>

                                        <tr style="border-bottom:0px solid #97ad77;">
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_help" >These details will appear in all the communication with your end customers</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>

                                        <tr><td>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">Address:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="MastCompAddressLine1" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">Telephone:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="MastCompTele" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">Fax:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="AgentFax" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">Email:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="MastCompEmail" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">ABN:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="MastCompABN" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">ACN:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="MastCompACN" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                            </table>
                                            </td></tr>

                                        <tr><td style="height:30px;">&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">AUSTRAC Registration Details</td>
                                                        <td class="ali_right"><input class="aa_btn_green" id="btnSubmitNewDetails" type="button" value="Submit AUSTRAC Details"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                        <tr class="spacer5"><td>&nbsp;</td></tr>

                                        <tr style="border-bottom:0px solid #97ad77; background-color:#EFEFEF;">
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="trn_notes_popup_details" style="font-size:0.65em; color:#417505; padding-top:7px; padding-bottom:5px; padding-left:10px;">Click the Submit AUSTRAC Details button and update AUSTRAC Registration Details and Submit.Flexewallet will contact you to confirm and obtain any supporting documents if needed.</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>

                                        <tr><td>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">Registration Number:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="AustracNumber" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">Registration Date:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="AustracRegDate" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:25%;">Renewal Date:</td>
                                                    <td class="ag_contents_text"><asp:Label ID="AustracRenDate" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                            </table>
                                            </td></tr>
                                        
                                        <tr><td style="height:30px;">&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Allocated Bank Accounts</td>
                                                        <td class="ali_right" style="height:30px;">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                        <tr class="spacer5"><td>&nbsp;</td></tr>

                                        <tr style="border-bottom:0px solid #97ad77; background-color:#EFEFEF;">
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="trn_notes_popup_details" style="font-size:0.65em; color:#417505; padding-top:7px; padding-bottom:5px; padding-left:10px;">These are the bank accounts assigned to you by Flexewallet to collect funds from your customers.</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>

                                        <tr><td>
                                            <asp:Table ID="tbl_AgentBankAssignment" runat="server" Width="100%">
                                                <asp:TableHeaderRow runat="server">
                                                    <asp:TableCell runat="server" CssClass="ag_tbl_headings" Width="10%">BSB</asp:TableCell>
                                                    <asp:TableCell runat="server" CssClass="ag_tbl_headings" Width="15%">Account No</asp:TableCell>
                                                    <asp:TableCell runat="server" CssClass="ag_tbl_headings" Width="35%">Account Name</asp:TableCell>
                                                    <asp:TableCell runat="server" CssClass="ag_tbl_headings" Width="40%">Bank</asp:TableCell>
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                           
                                            </td></tr>

                                        <tr><td style="height:30px;">&nbsp;</td></tr>

                                    <tr hidden="hidden"><td class="ali_right"><input class="aa_btn_green" id="btn_AddNewAgent" type="button" value="Add New Agent"/></td></tr>
                                    <tr hidden="hidden" class="aa_seperator"><td>&nbsp;</td></tr>
                                    <tr hidden="hidden"><td style="height:20px;">&nbsp;</td></tr>
                                    <tr hidden="hidden"><td>
                                        <table class="tbl_width_835" id="tbl_Agent">
                                            <thead>
                                                <td class="set_tbl_headings_l" style="width:50px;">ID</td>
                                                <td class="set_tbl_headings_l" style="width:350px;">AGENT NAME</td>
                                                <td class="set_tbl_headings_l" style="width:260px;">SUBURB</td>
                                                <td class="set_tbl_headings_l" style="width:260px;">STATE</td>
                                                <td class="set_tbl_headings_c" style="width:50px;">ACTIVE?</td>
                                                <td class="set_tbl_headings_r" style="width:50px;">EDIT/VIEW</td>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </td></tr>
                                    <!-- <tr><td>&nbsp;</td></tr> -->


                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Transfer Limits</td>
                                                        <td class="ali_right" style="height:30px;"><input style="visibility:hidden;" class="aa_btn_green" id="btnEditTransferLimits" type="button" value="Edit Transfer Limits"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                        <tr class="spacer5"><td>&nbsp;</td></tr>

                                        <tr class="wiz_bg_EFEFEF">
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="trn_notes_popup_details" style="font-size:0.65em; color:#417505; padding-top:7px; padding-bottom:5px; padding-left:10px;">These are the monthly and yearly limits set by Flexewallet for your end customers. If they have exceeded these limits they will be reviewed by Flexewallet. If you need higher limits permanently or temporarily please contact Flexewallet to discuss your needs.</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table style="width:100%;">
                                                    <tr>
                                                        <td class="ag_contents_label" style="width:25%;">Montly AUD Limit:</td>
                                                        <td class="ag_contents_text"><asp:Label ID="txt_agtmonlimit" runat="server" Text="Label"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ag_contents_label" style="width:25%;">Yearly AUD Limit:</td>
                                                        <td class="ag_contents_text"><asp:Label ID="txt_agtyearlylimit" runat="server" Text="Label"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ag_contents_label" style="width:25%;">Monthly Transactions Limit:</td>
                                                        <td class="ag_contents_text"><asp:Label ID="txt_agtmtranslimit" runat="server" Text="Label"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ag_contents_label" style="width:25%;">Yearly Transactions Limit:</td>
                                                        <td class="ag_contents_text"><asp:Label ID="txt_agtytranslimit" runat="server" Text="Label"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>
                                        <tr style="display:none;"><td>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td class="ag_box_full_brdr">
                                                        <table class="tbl_width_150">
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td class="ag_box_headings">MONTHLY AUD LIMIT</td></tr>
                                                            <tr><td>DEFAULT</td></tr>
                                                            <tr><td>
                                                                <asp:Label ID="txt_dftmonlimit" runat="server" Text="Label"></asp:Label></td></tr>
                                                            
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td>YOUR LIMIT</td></tr>
                                                            <tr><td>
                                                                </td></tr>
                                                            <tr><td>&nbsp;</td></tr>
                                                        </table>
                                                    </td>
                                                    <td style="width:20px;">&nbsp;</td>
                                                    <td class="ag_box_full_brdr">
                                                        <table class="tbl_width_150">
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td class="ag_box_headings">YEARLY AUD LIMIT</td></tr>
                                                            <tr><td>DEFAULT</td></tr>
                                                            <tr><td>
                                                                <asp:Label ID="txt_dftyearlylimit" runat="server" Text="Label"></asp:Label></td></tr>
                                                            
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td>YOUR LIMIT</td></tr>
                                                            <tr><td>
                                                                </td></tr>
                                                            <tr><td>&nbsp;</td></tr>
                                                        </table>
                                                    </td>
                                                    <td style="width:20px;">&nbsp;</td>
                                                    <td class="ag_box_full_brdr">
                                                        <table class="tbl_width_150">
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td class="ag_box_headings">MONTHLY TRANS LIMIT</td></tr>
                                                            <tr><td>DEFAULT</td></tr>
                                                            <tr><td>
                                                                <asp:Label ID="txt_dftmtranslimit" runat="server" Text="Label"></asp:Label></td></tr>
                                                            
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td>YOUR LIMIT</td></tr>
                                                            <tr><td>
                                                                </td></tr>
                                                            <tr><td>&nbsp;</td></tr>
                                                        </table>
                                                    </td>
                                                    <td style="width:20px;">&nbsp;</td>
                                                    <td class="ag_box_full_brdr">
                                                        <table class="tbl_width_150">
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td class="ag_box_headings">YEARLY TRANS LIMIT</td></tr>
                                                            <tr><td>DEFAULT</td></tr>
                                                            <tr><td>
                                                                <asp:Label ID="txt_dftytranslimit" runat="server" Text="Label"></asp:Label></td></tr>
                                                            
                                                            <tr><td>&nbsp;</td></tr>
                                                            <tr><td>YOUR LIMIT</td></tr>
                                                            <tr><td>
                                                                </td></tr>
                                                            <tr><td>&nbsp;</td></tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            </td></tr>











                                </table>
                                </div>
                                
                                <div id="BankingTab">
                                    <table class="tbl_width_860">

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Transfer Fee Tiers</td>
                                                        <td class="ali_right"><input class="aa_btn_green" id="btn_edittransferfees" type="button" value="Edit Transfer Fee Tiers"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>
                                        
                                        <tr class="spacer5"><td>&nbsp;</td></tr>

                                        <tr style="border-bottom:0px solid #97ad77; background-color:#EFEFEF;">
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="trn_notes_popup_details" style="font-size:0.65em; color:#417505; padding-top:7px; padding-bottom:5px; padding-left:10px;">Define your transfer fee tiers here</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>

                                        <tr> <!-- Transfer Fee Tiers Table -->
                                            <td style="width:860px;">
                                                <asp:Table ID="TBL_TransferTiers" style="width:860px;" runat="server"></asp:Table>
                                            </td>
                                        </tr>

                                        <tr><td style="height:25px;">&nbsp;</td></tr>

                                        

                                        <tr hidden="hidden"><td class="ali_right"><input class="aa_btn_green" id="btn_AddNewAccount" type="button" value="Add New Local Bank Account"/>&nbsp;&nbsp;&nbsp;</td></tr>
                                        <tr hidden="hidden"class="aa_seperator"><td>&nbsp;</td></tr>
                                        <tr hidden="hidden"><td style="height:20px;">&nbsp;</td></tr>
                                        <tr hidden="hidden"><td>
                                            <table class="tbl_width_760" id="tbl_BankAccount">
                                                <thead>
                                                    <td class="set_tbl_headings_l" style="width:60px;">ID</td>
                                                    <td class="set_tbl_headings_l" style="width:100px;">BANK NAME</td>
                                                    <td class="set_tbl_headings_l" style="width:100px;">BSB</td>
                                                    <td class="set_tbl_headings_l" style="width:100px;">ACCOUNT NUMBER</td>
                                                    <td class="set_tbl_headings_l" style="width:100px;">ACCOUNT NAME</td>
                                                    <td class="set_tbl_headings_l" style="width:100px;">ACTIVE?</td>
                                                    <td class="set_tbl_headings_r" style="width:100px;">EDIT/VIEW</td>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </table>
                                </div>

                                <div id="ThirdMainTab">
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Foreign Bank Accounts</td>
                                                        <td class="ali_right"><input class="aa_btn_green" id="btn_AddNewFAccount" type="button" value="Add A Foreign Bank Account"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        
                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>
                                        
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td>
                                            <table class="tbl_width_760" id="tbl_Countries">
                                                <thead>
                                                    <td class="tbl_heading_ben_fnt_01">Routing ID</td>
                                                    <td class="tbl_heading_ben_fnt">Country Name</td>
                                                    <td class="tbl_heading_ben_fnt">Bank Name</td>
                                                    <td class="tbl_heading_ben_fnt">Account Number</td>
                                                    <td class="tbl_heading_ben_fnt">&nbsp;</td>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </td></tr>
                                        
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Foreign Banks</td>
                                                        <td class="ali_right"><input class="aa_btn_green" id="btn_SetDefaultRoutingBanks" type="button" value="Set Default Routing Banks"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>
                                        
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td>
                                            <table class="tbl_width_760" id="tbl_BankRouting">
                                                <thead>
                                                    <td class="tbl_heading_ben_fnt_01">Routing ID</td>
                                                    <td class="tbl_heading_ben_fnt">Country</td>
                                                    <td class="tbl_heading_ben_fnt">Bank</td>
                                                    <td class="tbl_heading_ben_fnt">Routed To</td>
                                                    <td class="tbl_heading_ben_fnt">&nbsp;</td>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </td></tr>

                                        <tr><td>&nbsp;</td></tr>
                                    
                                    
                                    </table>
                                </div>

                                <div id="SMTPTab">
                                    <table class="tbl_width_860">
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860">
                                                                <tr>
                                                                    <td class="settings_headings">SMTP (Email) Server Address Settings</td>
                                                                    <td class="ali_right"><input class="aa_btn_green" id="btn_AddNewSMTP" type="button" value="Add New SMTP (Email) Server" /><input class="aa_btn_green" id="btn_EditSMTP" type="button" value="Edit SMTP (Email) Server"/></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>  
                                                        
                                                    <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                                    <tr><td>&nbsp;</td></tr>

                                                    <tr id="TR_NOSMTPDetails"><td class="settings_cont2">Currently no SMTP (Email) configuration exisit! Please click Add New SMTP (Email) Server button to configure your email settings.</td></tr>

                                                    <tr id="TR_SMTPDetails">
                                                        <td>
                                                            <table>
                                                                <tr> <!-- 01 -->
                                                                    <td>
                                                                        <table class="tbl_width_860"> 
                                                                            <tr>
                                                                                <td class="settings_cont1">SMTP Server Name/IP Address</td>
                                                                                <td class="settings_cont2">
                                                                                    <asp:Label ID="Lbl_smtpservername" runat="server" Text=""></asp:Label></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 02 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">SMTP Port</td>
                                                                                <td class="settings_cont2">
                                                                                    <asp:Label ID="Lbl_smtpport" runat="server" Text="Label"></asp:Label></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 03 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">SSL Enabled?</td>
                                                                                <td class="settings_cont2">
                                                                                    <asp:Label ID="Lbl_sslenabled" runat="server" Text="Label"></asp:Label></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 04 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">Username</td>
                                                                                <td class="settings_cont2">
                                                                                    <asp:Label ID="Lbl_username" runat="server" Text="Label"></asp:Label></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 05 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">Password</td>
                                                                                <td class="settings_cont2" id="td_password">************</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 06 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">Email Address</td>
                                                                                <td class="settings_cont2">
                                                                                    <asp:Label ID="Lbl_emailaddress" runat="server" Text="Label"></asp:Label></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>


                                                    <tr><td style="height: 20px;">&nbsp;</td></tr>                                                                                

                                    </table>
                                </div>

                                <div id="FifthMainTab">

                                </div>

                            </td>
                        </tr>
                        <tr class="set_tab_lrb_brdr" style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    
    <div class="container_one">
    </div>


    <div id="DIV_AddAgentCurrencies">
        </div>
       

<!-- +++++++++++++++++++++++++ EDIT TRANSFER TIER START +++++++++++++++++++++++++ -->
    <div id="DIV_EditTrTiercompany"> 
        
        <table class="tbl_width_600">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image21" runat="server" ImageUrl="~/images/agent.png" title="AGENT DETAILS" /></td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">
                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">ADDRESS LINE 1</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">ADDRESS LINE 2</td>
                                    </tr>
                                    <tr>
                                        <td style="width:270px;"><div><input class="aa_input" id="MCEdit_AddressLine1" style="width:100%;" type="email" value=""  /></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:270px;"><div><input class="aa_input" id="MCEdit_AddressLine2" style="width:100%;" type="email" value=""  /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">SUBURB</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">STATE</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">POSTCODE</td>
                                    </tr>
                                    <tr>
                                        <td style="width:270px;"><div><input class="aa_input" id="MCEdit_Suburb" style="width:100%;" type="text" value=""/></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:125px;"><div><input class="aa_input" id="MCEdit_State" style="width:100%;" type="text" value="" /></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:125px;"><div><input class="aa_input" id="MCEdit_Postcode" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">PHONE</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">FAX</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">EMAIL</td>
                                    </tr>
                                    <tr>
                                        <td style="width:125px;"><div><input class="aa_input" id="MCEdit_Telephone" style="width:100%;" type="text" value="" /></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:125px;"><div><input class="aa_input" id="MCEdit_Fax" style="width:100%;" type="text" value="" /></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:270px;"><div><input class="aa_input" id="MCEdit_Email" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">ABN</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">ACN</td>
                                    </tr>
                                    <tr>
                                        <td style="width:270px;"><div><input class="aa_input" id="MCEdit_ABN" style="width:100%;" type="text" value="" /></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:270px;"><div><input class="aa_input" id="MCEdit_ACN" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr style="height:20px;"><td>&nbsp;</td></tr>


                    </table>
                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr><td class="ali_right"><input class="aa_btn_red" id="btn_EditMasterComp" type="button" value="Save"/></td></tr>
                    </table>
                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>
        </table>
    </div>
    <!-- +++++++++++++++++++++++++ EDIT TRANSFER TIER END +++++++++++++++++++++++++ -->
         <!-- +++++++++++++++++++++++++ EDIT Submit New Details +++++++++++++++++++++++++ -->
        <div id="DIV_SubmitDetails">
        <table class="tbl_width_280">
            <tr style="height:20px;"><td>&nbsp;</td></tr>
            <tr>
                <td>
                  <table style="width:100%;">
                      <tbody>
                          <tr>
                                                    <td class="ag_contents_label" style="width:28%;">Registration Number:</td>
                                                    <td class="ag_contents_text"><input type="text" runat="server" id="txtRegNumber" class="aa_input" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:28%;">Registration Date:</td>
                                                    <td class="ag_contents_text"><input type="text" runat="server" id="txtRegDate" class="aa_input" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="ag_contents_label" style="width:28%;">Renewal Date:</td>
                                                    <td class="ag_contents_text"><input type="text" runat="server" id="txtRenDate" class="aa_input" /></td>
                                                </tr>
                       </tbody>
                  </table>
                </td>
            </tr>
            <tr style="height:20px;"><td>&nbsp;</td></tr>
        </table>
        <div style="width:100%">
            <input type="button" style="margin-top:20px;float:right" value="Submit" class="btn_green_nomargin" onclick="SubmitNewDetails()" />
        </div>
    </div>
         <!-- +++++++++++++++++++++++++ EDIT Submit New Details +++++++++++++++++++++++++ -->
    <!-- +++++++++++++++++++++++++ EDIT COMPANY START +++++++++++++++++++++++++ -->
    <div id="DIV_Editcompany"> 
        <table class="tbl_width_640">








            <tr style="height:20px;"><td>&nbsp;</td></tr>
            <tr><td>
                <table class="tbl_width_640">
                    <tr>
                        <td class="aa_tab_active">COMPANY DETAILS</td>
                        <td class="aa_tab_inactive_right">NOT IN USE</td>
                        <td class="aa_tab_inactive_right">NOT IN USE</td>
                        <td class="aa_tab_nocontent">&nbsp;</td>
                    </tr>
                </table>
                </td></tr>
            <tr><td class="aa_tab_lr_brdr">&nbsp;</td></tr>
            <tr class="aa_tab_lr_brdr">
                <td>
                    <table class="tbl_width_600">

                        <tr><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">ADDRESS LINE 1</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">ADDRESS LINE 2</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_290"><div><input class="aa_input" id="MCEdit_AddressLine1" style="width:100%;" type="email" value=""  /></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_290"><div><input class="aa_input" id="MCEdit_AddressLine2" style="width:100%;" type="email" value=""  /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="style__width_600">
                                    <tr>
                                        <td class="aa_label_font">SUBURB</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">STATE</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">POSTCODE</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_290"><div><input class="aa_input" id="MCEdit_Suburb" style="width:100%;" type="text" value=""/></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_135"><div><input class="aa_input" id="MCEdit_State" style="width:100%;" type="text" value="" /></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_135"><div><input class="aa_input" id="MCEdit_Postcode" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">PHONE</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">FAX</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">EMAIL</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_135"><div><input class="aa_input" id="MCEdit_Telephone" style="width:100%;" type="text" value="" /></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_135"><div><input class="aa_input" id="MCEdit_Fax" style="width:100%;" type="text" value="" /></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_290"><div><input class="aa_input" id="MCEdit_Email" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">ABN</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">ACN</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_290"><div><input class="aa_input" id="MCEdit_ABN" style="width:100%;" type="text" value="" /></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_290"><div><input class="aa_input" id="MCEdit_ACN" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_600">
                                    <tr>
                                        <td class="ali_right"><input class="aa_btn_red" id="btn_EditMasterComp" type="button" value="Save"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>


                    </table>
                </td>
            </tr>
            <tr class="aa_tab_lrb_brdr" style="height:20px;"><td>&nbsp;</td></tr>
            <tr style="height:20px;"><td>&nbsp;</td></tr>
        </table>
    </div>
    <!-- +++++++++++++++++++++++++ EDIT COMPANY END +++++++++++++++++++++++++ -->




<!-- *************** ADD NEW LOCAL BANK START *************** --> 
    <div id="DIV_AddNewBankAccount">
        <table class="tbl_width_640">
            <tr style="height:18px;"><td>&nbsp;</td></tr>
            <tr><td>
                <table class="tbl_width_640">
                    <tr>
                        <td class="aa_tab_active" id="TD_BankDetails">BANK DETAILS</td>
                        <td class="aa_tab_nocontent">&nbsp;</td>
                        <td class="aa_tab_nocontent">&nbsp;</td>
                        <td class="aa_tab_nocontent">&nbsp;</td>
                    </tr>
                </table>
                </td></tr>
            <tr><td class="aa_tab_lr_brdr" style="height:20px;">&nbsp;</td></tr>
            <tr class="aa_tab_lr_brdr">
                <td>
                    <div id="DIV_BankDetails">
                        <table class="tbl_width_600">

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">BANK NAME</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="ag_add_bankname" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr style="height:25px;"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">ACCOUNT NAME</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="ag_add_banksomething" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="style__width_600">
                                    <tr>
                                        <td class="aa_label_font">BSB</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">ACCOUNT NUMBER</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_135"><div><input class="aa_input" id="ag_add_Phone" style="width:100%;" type="text" value=""  /></div></td>
                                        <td class="style__width_20" style="text-align:center;">-</td>
                                        <td class="style__width_135"><div><input class="aa_input" id="ag_add_Fax" style="width:100%;" type="text" value=""  /></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_290"><div><input class="aa_input" id="ag_add_Email" style="width:100%;" type="text" value=""  /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                    </div>
                    
                </td>
            </tr>
            
            <tr class="spacer10 aa_tab_lr_brdr"><td>&nbsp;</td></tr>

            <tr class="aa_tab_lr_brdr">
                <td>
                    <table class="tbl_width_600">
                        <tr><td class="ali_right"><input class="aa_btn_red" id="btn_SaveNewBankAccount" type="button" value="Save"/></td></tr>
                    </table>
                </td>
            </tr>

            <tr class="aa_tab_lrb_brdr" style="height:20px;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>
        </table>
    </div>


<!-- *************** ADD NEW FOREIGN BANK START *************** --> 
    <div id="DIV_AddNewFBankAccount">
        <table class="tbl_width_640">
            <tr style="height:18px;"><td>&nbsp;</td></tr>
            <tr><td>
                <table class="tbl_width_640">
                    <tr>
                        <td class="aa_tab_active" id="TD_FBankDetails">BANK DETAILS</td>
                        <td class="aa_tab_nocontent">&nbsp;</td>
                        <td class="aa_tab_nocontent">&nbsp;</td>
                        <td class="aa_tab_nocontent">&nbsp;</td>
                    </tr>
                </table>
                </td></tr>
            <tr><td class="aa_tab_lr_brdr" style="height:20px;">&nbsp;</td></tr>
            <tr class="aa_tab_lr_brdr">
                <td>
                    <div id="DIV_FBankDetails">
                        <table class="tbl_width_600">

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">COUNTRY</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="ag_add_bankname2" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr style="height:25px;"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">BANK NAME</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="ag_add_banksm" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">ACCOUNT NAME</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="ag_add_banksmew" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="style__width_600">
                                    <tr>
                                        <td class="aa_label_font">BSB</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">ACCOUNT NUMBER</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_135"><div><input class="aa_input" id="ag_add_Phone" style="width:100%;" type="text" value=""  /></div></td>
                                        <td class="style__width_20" style="text-align:center;">-</td>
                                        <td class="style__width_135"><div><input class="aa_input" id="ag_add_Fax" style="width:100%;" type="text" value=""  /></div></td>
                                        <td class="style__width_20">&nbsp;</td>
                                        <td class="style__width_290"><div><input class="aa_input" id="ag_add_Email" style="width:100%;" type="text" value=""  /></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                    </div>
                    
                </td>
            </tr>
            
            <tr class="spacer10 aa_tab_lr_brdr"><td>&nbsp;</td></tr>

            <tr class="aa_tab_lr_brdr">
                <td>
                    <table class="tbl_width_600">
                        <tr><td class="ali_right"><input class="aa_btn_red" id="btn_SaveNewFBankAccount" type="button" value="Save"/></td></tr>
                    </table>
                </td>
            </tr>

            <tr class="aa_tab_lrb_brdr" style="height:20px;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>
        </table>
    </div>



<!-- *************** CHANGE PASSWORD DIV START *************** -->    
    <div id="ChangePassword">
            <table class="tbl_width_280">
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_280">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">CURRENT PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td style="width:280px;"><div><input class="aa_input" id="txt_CurrentPassword" style="width:100%;" type="password" value=""  /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr style="height:25px;"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">NEW PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td style="width:280px;"><div><input class="aa_input" id="txt_NewPassword" style="width:100%;" type="password" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">RE-ENTER NEW PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td style="width:280px;"><div><input class="aa_input" id="txt_NewPasswordConfirm" style="width:100%;" type="password" value=""  /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="ali_right"><input class="aa_btn_red" id="btn_ChangePasswordSubmit" type="button" value="Submit"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
            </table>
    </div>

        <div id="DIV_TransferFeeTiers">
            <table class="tbl_width_600">
                <tr hidden="hidden";><td>&nbsp;</td></tr>
                <tr class="cus_divider_solid success_bg" hidden="hidden";>
                    <td>
                        <table class="tbl_width_580">
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="success_font">1 Please select the country</td></tr>
                            <tr><td>&nbsp;</td></tr>

                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="aa_label_font">SELECT COUNTRY</td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="TRF_CountryList" style="width:300px;">
                                        
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="aa_label_font">SELECT CURRENCY</td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="TRF_CurrencyList" style="width:300px;">
                                   
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr hidden="hidden">
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td style="width:75px;" class="w_tabs_active">here</td>
                                <td style="width:75px;">&nbsp;</td>
                                <td style="width:75px;">&nbsp;</td>
                                <td style="width:75px;">&nbsp;</td>
                                <td style="width:75px;">&nbsp;</td>
                                <td style="width:75px;">&nbsp;</td>
                                <td style="width:75px;">&nbsp;</td>
                                <td style="width:75px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="aa_seperator_solid"><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table style="width: 500px; margin: 0 auto;">
                            <tr>
                                <td class="w_tier_labels">TIER 1</td>
                                <td class="auto-style1">TIER 2</td>
                                <td class="w_tier_labels">TIER 3</td>
                            </tr>
                        </table>
                    </td></tr>
                <tr><td>
                    <asp:Image ID="Image9" runat="server" ImageUrl="~/images/tiers.png" />
                    </td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td style="width:100px;"><input class="w_tier_inputs_ends" id="TRF_StartLimit" style="width:100%;" type="text" value="$ 1.00" disabled="disabled" /></td>
                                <td style="width:5px;">&nbsp;</td>
                                <td><input class="w_tier_inputs" id="TRF_Tier1Value" style="width:100%;" type="text" value="$"  /></td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="width:100px;"><input class="w_tier_inputs_middle" id="TRF_Tier1To" style="width:100%;" type="text" value="" /></td>
                                <td style="width:5px;">&nbsp;</td>
                                <td><input class="w_tier_inputs" id="TRF_Tier2Value" style="width:100%;" type="text" value="$"  /></td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="width:100px;"><input class="w_tier_inputs_middle" id="TRF_Tier2To" style="width:100%;" type="text" value="" /></td>
                                <td style="width:5px;">&nbsp;</td>
                                <td><input class="w_tier_inputs" id="TRF_Tier3Value" style="width:100%;" type="text" value="$"  /></td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="width:100px;"><input class="w_tier_inputs_ends" id="TRF_UpperLimit" style="width:100%;" type="text" value="" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="text-align:center; padding-top:10px; background-color:#EFEFEF;">
                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/images/upload.png" />
                                </td>
                                <td>&nbsp;</td>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="text-align:center; padding-top:10px; background-color:#EFEFEF;">
                                    <asp:Image ID="Image7" runat="server" ImageUrl="~/images/upload.png" />
                                </td>
                                <td>&nbsp;</td>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="text-align:center; padding-top:10px; background-color:#EFEFEF;">
                                    <asp:Image ID="Image8" runat="server" ImageUrl="~/images/upload.png" />
                                </td>
                                <td>&nbsp;</td>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="background-color:#EFEFEF;" class="w_tier_labels">CHARGE</td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="background-color:#EFEFEF;" class="w_tier_labels">CHARGE</td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="background-color:#EFEFEF;" class="w_tier_labels">CHARGE</td>
                                <td style="width:5px;">&nbsp;</td>
                                <td style="background-color:#EFEFEF;">&nbsp;</td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr><td class="ali_right"><input class="aa_btn_red" id="btn_SaveTransferFees" type="button" value="Save"/></td></tr>
                        </table>
                    </td>
                 </tr>

                <tr><td>&nbsp;</td></tr>

            </table>
        </div>

    <div id="div_EditSMTP">
        <table class="tbl_width_600">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px; background-color:#EFEFEF; text-align:center; vertical-align:middle;"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/server.png" title="SMTP SERVER SETTINGS" /></td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td style="height: 20px; background-color:#EFEFEF;">&nbsp;</td></tr>

            <tr>
                <td style="background-color:#EFEFEF;">
                    <div id="DIV_blank002">
                        <table class="tbl_width_560">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">SMTP SERVER NAME/IP ADDRESS</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">SMTP PORT</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_445"><div><input class="aa_input" id="EDIT_SMTPName" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input class="aa_input" id="EDIT_SMTPPort" style="width: 100%;" type="text" value=""  /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">EMAIL ADDRESS</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">SSL ENABLED?</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_445"><div><input class="aa_input" id="EDIT_SMTPEmailAddress" style="width: 100%;" type="text" value=""  /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_135">
                                                <div>
                                                    <select id="EDIT_SSLEnabled" required style="width:100%;">
                                                        <option value="">--Select--</option>
	                                                    <option value="1">Yes</option>
	                                                    <option value="2">No</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">USERNAME</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_SMTPUsername" style="width: 100%;" type="text" value=""  /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_SMTPPassword" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>

            <tr style="height: 20px; background-color:#EFEFEF;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

            <tr id="SMTP_Result_TR">
                <td id="SMTPTestResult">Message here</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr>
                            <td><input class="aa_btn_green" id="btn_TestSMTP" type="button" value="Test Connection" /></td>
                            <td class="ali_right"><input class="aa_btn_red" id="btn_SaveSMTPEdit" type="button" value="Update" /></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
    </div>

    <div id="div_AddNewForeignBank">
        <table class="tbl_width_600">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image12" runat="server" ImageUrl="~/images/bank.png" title="BANK ACCOUNT DETAILS" /></td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>
            
            
            
            
            
            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">COUNTRY</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">BANK</td>
                                    </tr>
                                    <tr>
                                        <td style="width:270px;">
                                            <div>
                                                
                                                <select id="ddl_FBSelectCounty" required style="width:100%;">
                                                    <option value="">--Select--</option>
	                                                <option value="1">Yes</option>
	                                                <option value="2">No</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:270px;">
                                            <div>
                                                
                                                <select id="ddl_FBSelectBank" required style="width:100%;">
                                                    <option value="">--Select--</option>
	                                                <option value="1">Yes</option>
	                                                <option value="2">No</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">ACCOUNT NUMBER</td>
                                    </tr>
                                    <tr>
                                        <td><div><input class="aa_input" id="ag_add_bankaccountnumber" style="width:100%;" type="text" value="" required/></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">SELECT ACCOUNT CURRENCY/S</td>
                                        <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_290"><div><select id="ddl_AllowedCurrencies" class="set_currency_sel_box" multiple = "multiple" size = "10" required></select></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td class="style__width_290"><div>&nbsp;</div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                    </table>

                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr><td class="ali_right"><input class="aa_btn_red" id="btn_SaveNewForeignBankAccount" type="button" value="Save"/></td></tr>
                        </table>
                    </td>
                 </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
    </div>

    <div id="div_SetDefaultRoutingBanks">
        <table class="tbl_width_600">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image13" runat="server" ImageUrl="~/images/bank.png" title="BANK ACCOUNT DETAILS" /></td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>
            
            
            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">
                        <tr class="background_FFFFFF">
                            <td>
                                <table class="tbl_width_520">
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="trn_notes_popup_details" style="font-size:0.7em; color:#999;">You can assign a Default Routing Bank per country below. All the transactions to banks in that country without a Foreign Bank Account will be automatically routed through the Default Routing Bank unless another Default Routing Bank is manually assigned to the destination bank. </td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">COUNTRY</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">DEFAULT ROUTING BANK</td>
                                    </tr>
                                    <tr>
                                        <td style="width:270px;">
                                            <div>
                                                <select id="ddl_DRBSelectCounty" required style="width:100%;">
                                                    <option value="">--Select--</option>
	                                                <option value="1">Yes</option>
	                                                <option value="2">No</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:270px;">
                                            <div>
                                                <select id="ddl_DRBSelectBank" required style="width:100%;">
                                                    <option value="">--Select--</option>
	                                                <option value="1">Yes</option>
	                                                <option value="2">No</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr style="height:20px;"><td>&nbsp;</td></tr>

                    </table>

                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr><td class="ali_right"><input class="aa_btn_red" id="btn_SaveDefaultRoutingBank" type="button" value="Save"/></td></tr>
                        </table>
                    </td>
                 </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
    </div>

        <div id="DIV_EditForeignAccount">
            <table class="tbl_width_600">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/bank.png" title="BANK ACCOUNT DETAILS" /></td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>
            
            
            
            
            
            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">COUNTRY</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                        <td class="aa_label_font">BANK</td>
                                    </tr>
                                    <tr>
                                        <td style="width:270px;">
                                            <div>
                                                
                                                <select id="ddl_EditFBSelectCounty" required style="width:100%;">
                                                    <option value="">--Select--</option>
	                                                <option value="1">Yes</option>
	                                                <option value="2">No</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="width:270px;">
                                            <div>
                                                
                                                <select id="ddl_EditFBSelectBank" required style="width:100%;">
                                                    <option value="">--Select--</option>
	                                                <option value="1">Yes</option>
	                                                <option value="2">No</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">ACCOUNT NUMBER</td>
                                    </tr>
                                    <tr>
                                        <td><div><input class="aa_input" id="ag_edit_bankaccountnumber" style="width:100%;" type="text" value="" required/></div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                        <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">SELECT ACCOUNT CURRENCY/S</td>
                                        <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                        <td class="aa_label_font">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_290"><div><select id="ddl_edit_AllowedCurrencies" class="set_currency_sel_box" multiple = "multiple" size = "10" required></select></div></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td class="style__width_290"><div>&nbsp;</div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                    </table>

                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr><td class="ali_right"><input class="aa_btn_red" id="btn_Edit_SaveNewForeignBankAccount" type="button" value="Save" onclick="updateCurrenciesforForeignBank()"/></td></tr>
                        </table>
                    </td>
                 </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
        </div>

    </form>
</asp:Content>
