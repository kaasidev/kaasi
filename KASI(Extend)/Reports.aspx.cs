﻿using Kapruka.Enterprise;
using Kapruka.PublicModels;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_
{
    public partial class Reports2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

            //getFileDetails();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/CustomerDepositsReport.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("TransactionsByDateRange.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/AgentStatsSummary.aspx");
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/TransactionsSummaryReport.aspx");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/ListOfTransactionsViewer.aspx");
        }

        protected void Button2_Click4(object sender, EventArgs e)
        {
            Response.Redirect("Reports/ComplianceReviewTransactionsReport.aspx");
        }

        protected void Button3_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/ManuallyKYCApprovedCustomersReport.aspx");
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/EscalatedtoCompliancTransactionsReport.aspx");
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports\\Trsummery.aspx");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/MonthlyTransactionsSummaryReport.aspx");
        }

        private void getFileDetails()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String hasFiles = "NO";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT MIN(TransactionID) AS LowTrans, MAX(TransactionID) AS UpTrans, COUNT(TransactionID) AS TotTrans
                        FROM dbo.Transactions WHERE InsertedIntoAustracFileByMaster = 'N'";
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        hasFiles = "YES";
                        if (!String.IsNullOrEmpty(sdr["LowTrans"].ToString()))
                        {
                            //IFTIStartTrans.InnerText = sdr["LowTrans"].ToString();
                            //IFTIEndTrans.InnerText = sdr["UpTrans"].ToString();
                            //IFTITotalTrans.InnerText = sdr["TotTrans"].ToString();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "closeHasFiles", "hideHasfiles();", true);
                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "closeHasFiles", "hideHasfiles();", true);
                        }
                        
                    }
                    
                }
                conn.Close();

                if (hasFiles == "NO")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "closeHasFiles", "hideHasfiles();", true);
                }
            }
        }

        protected void btnAustract_Click(object sender, EventArgs e)
        {
            SPNPleaseWait.Style.Add("display", "block");

            TransactionService transactionService = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var transactionList = transactionService.GetAll(x => x.InsertedIntoAustracFileByMaster == "N", null, "").ToList();
            //  var smrList = transactionService.GetAll(x => x.isSuspicious == "N" && x.InsertedIntoAustracFile=="N", null, "").ToList();
            // var ttrList = transactionService.GetAll(x => x.isThreshold == "N" && x.InsertedIntoAustracFile == "N", null, "").ToList();

            //var transactionList = transactionService.GetAll().ToList();
            //transactionList = (from tr in transactionList where tr.InsertedIntoAustracFileByMaster == "N" select tr).ToList() ;
            string todaysdate = DateTime.Now.ToString("yyyyMMdd");

            SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            MasterAustracRecordService MARServ = new MasterAustracRecordService(new UnitOfWorks(new KaprukaEntities()));

            DateTime today = DateTime.Now.Date;
            String justthedate = today.ToString("dd/MM/yyyy");
            var theNextNumber = "";

            var TheSequenceNumber = MARServ.GetAll(x => x.CreatedDate == today, null, "").SingleOrDefault();
            


            if (TheSequenceNumber != null)
            {
                theNextNumber = (Int32.Parse(TheSequenceNumber.FileSequenceNumber) + 1).ToString().PadLeft(2, '0');
                //TheSequenceNumber.CreatedDate = today;
                TheSequenceNumber.FileSequenceNumber = theNextNumber;
                MARServ.Update(TheSequenceNumber);
            }
            else
            {
                var NewSequence = new MasterAustracRecord();
                theNextNumber = "01";
                NewSequence.CreatedDate = today;
                NewSequence.FileSequenceNumber = theNextNumber;
                MARServ.Add(NewSequence);
            }

            String fileName = "IFTI-E" + todaysdate + theNextNumber;

            // IftieList iftModel = new IftieList();
            SmrList smrModel = new SmrList();
            TtrfbsList ttrModel = new TtrfbsList();
            IFTIModel.IftidraList iftModel = new IFTIModel.IftidraList();
            iftModel.VersionMajor = "1";
            iftModel.VersionMinor = "1";
            //iftModel= GenerateIFTReport(transactionList, fileName);
            iftModel = GenerateIFTReportML(transactionList, fileName);
            // smrModel = GenerateSMRReport(smrList, fileName);
            //  ttrModel = GenerateTTRReport(ttrList,fileName);

            //IftieList hd = new PublicModels.IftieList();

            //hd.FileName = "te";
            //hd.Ns1 = "dsds";
            //hd.Ns2 = "ds s ";
            ////lst.Add(hd);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            string filePath = Server.MapPath("/Files") + "\\" + fileName + ".xml";
            using (MemoryStream ms = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(ms, settings))
                {
                    var serializer = new XmlSerializer(typeof(IFTIModel.IftidraList));
                    serializer.Serialize(writer, iftModel);
                }

                ms.Position = 0;
                XDocument doc = XDocument.Load(new XmlTextReader(ms));
                doc.Root.Add(new XAttribute("versionMinor", "1"));
                doc.Root.Add(new XAttribute("versionMajor", "1"));
                doc.Save(filePath);

                DownloadTheFile(filePath, fileName);



                //ms.Close();
                //Response.Clear();
                //Response.ContentType = "application/force-download";
                //Response.AddHeader("content-disposition", "attachment;    filename=" + fileName + ".xml");
                //byte[] bytesInStream = ms.ToArray(); // simpler way of converting to array
                //Response.BinaryWrite(bytesInStream);
                //Response.End();
            }
            //  SerializeObject(iftModel, filePath, "\\xml" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xml");
            UpdateTable(fileName + ".xml");
        }

        public void UpdateTable(string fileName)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string sql = "update Transactions set InsertedIntoAustracFileNameByMaster='" + fileName + "',InsertedIntoAustracFileDateTimeByMaster=getdate(),InsertedIntoAustracFileByMaster='Y' where InsertedIntoAustracFileByMaster='N'";

            using (SqlCommand cmd = new SqlCommand(sql))
            {
                cmd.Connection = conn;
                conn.Open();

                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void DownloadTheFile(string url, string fileName)
        {
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                var fileReq = System.Net.HttpWebRequest.Create(url);

                //Create a response for this request
                var fileResp = fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xml\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
        }

        private Kapruka.PublicModels.IFTIModel.IftidraList GenerateIFTReportML(List<Transaction> transactionList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            CustomerDocumentService custDocSer = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            beneficiaryPaymentMethodService payMethodSer = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            CountryService countrySer = new CountryService(new UnitOfWorks(new KaprukaEntities()));
            AgentAssignedCountryService agencyAssSer = new AgentAssignedCountryService(new UnitOfWorks(new KaprukaEntities()));
            SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
            Kapruka.PublicModels.IFTIModel.IftidraList model = new Kapruka.PublicModels.IFTIModel.IftidraList();
            var theReNumber = SetServ.GetAll(x => x.SettingName == "AUSTRACReportingNumber", null, "").SingleOrDefault().SettingsVariable;

            model.FileName = fileName + ".xml";
            model.xmlns = null;

            model.renumber = theReNumber;

            model.Iftidra = new List<IFTIModel.Iftidra>();
            if (transactionList != null)
            {
                model.ReportCount = transactionList.Count();
            }
            else
            {
                model.ReportCount = 0;
            }

            Customer customerList = new Customer();
            Beneficiary beneficiaryList = new Beneficiary();
            CustomerDocument custDoc = new CustomerDocument();
            BeneficiaryPaymentMethod payMenthod = new BeneficiaryPaymentMethod();
            Kapruka.Repository.Agent agent = new Kapruka.Repository.Agent();
            // int kl = 0;
            foreach (Transaction item in transactionList)
            {
                //   if(kl==0)
                //   {

                customerList = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                beneficiaryList = beneService.GetAll(x => x.BeneficiaryID == item.BeneficiaryID, null, "").SingleOrDefault();
                var custDocList = custDocSer.GetAll(x => x.CustomerID == customerList.CustomerID, null, "").ToList();
                var goingCountry = countrySer.GetAll(x => x.CountryID == item.CountryID, null, "").SingleOrDefault().CountryName;
                var ReportingEntityNumber = SetServ.GetAll(x => x.SettingName == "AUSTRACReportingNumber", null, "").SingleOrDefault().SettingsVariable;
                
                if (custDocList.Count == 0)
                {
                    custDoc = null;
                }
                else
                {
                    custDoc = custDocList[0];
                }

                string benId = beneficiaryList.BeneficiaryID.Value.ToString();
                var payMenthodList = payMethodSer.GetAll(x => x.BeneficiaryID == benId, null, "").ToList();
                if (payMenthodList.Count == 0)
                {
                    payMenthod = null;
                }
                else
                {
                    payMenthod = payMenthodList[0];
                }
                int agentId = customerList.AgentID.Value;
                agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();


                IFTIModel.Iftidra dra = new IFTIModel.Iftidra();
                dra.Id = "ID_" + item.TransactionID.ToString();
                dra.Header = new IFTIModel.Header();
                dra.Header.Id = "ID_" + item.TransactionID.ToString() + "-0";
                dra.Header.InterceptFlag = "N";
                dra.Header.TxnRefNo = item.TransactionID.ToString();

                dra.Transaction = new IFTIModel.Transaction();
                dra.Transaction.Id = "ID_" + item.TransactionID.ToString() + "-1";
                dra.Transaction.TxnDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");
                dra.Transaction.CurrencyAmount = new IFTIModel.CurrencyAmount();
                dra.Transaction.CurrencyAmount.Id = "ID_" + item.TransactionID.ToString() + "-11";
                dra.Transaction.CurrencyAmount.Amount = item.DollarAmount.Value.ToString("N2");
                dra.Transaction.CurrencyAmount.Currency = "AUD";

                dra.Transaction.direction = "O";
                dra.Transaction.TfrType = new IFTIModel.TfrType();
                dra.Transaction.TfrType.Type = "M";
                dra.Transaction.TfrType.Id = "ID_" + item.TransactionID.ToString() + "-12";
                dra.Transaction.ValueDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");

                dra.Transferor = new IFTIModel.Transferor();//customer
                dra.Transferor.Id = "ID_" + item.TransactionID.ToString() + "-2";
                dra.Transferor.FullName = customerList.FullName;
                dra.Transferor.MainAddress = new IFTIModel.MainAddress();
                dra.Transferor.MainAddress.Addr = customerList.AddressLine1 + " " + customerList.AddressLine2;
                dra.Transferor.MainAddress.Country = customerList.CountryOfBirth;
                dra.Transferor.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-21";
                dra.Transferor.MainAddress.Postcode = customerList.Postcode;
                dra.Transferor.MainAddress.State = customerList.State;
                dra.Transferor.MainAddress.Suburb = customerList.Suburb;
                dra.Transferor.MainAddress.Country = customerList.Country;

                dra.Transferor.Phone = customerList.Mobile;
                dra.Transferor.Email = customerList.EmailAddress;
                dra.Transferor.CustNo = customerList.CustomerID.Value.ToString();
                if (!String.IsNullOrEmpty(customerList.DOB))
                {
                    dra.Transferor.Dob = ChangeDOB(customerList.DOB.ToString());
                }
                dra.Transferor.Identification = new IFTIModel.Identification();
                dra.Transferor.Identification.Id = "ID_" + item.TransactionID.ToString() + "-22";
                if (custDoc != null)
                {
                    String TypeOfDoc = String.Empty;
                    if (custDoc.TypeOfDocument == "Driver's Licence")
                    {
                        TypeOfDoc = "D";
                    }
                    else if (custDoc.TypeOfDocument == "Passport")
                    {
                        TypeOfDoc = "P";
                    }
                    dra.Transferor.Identification.Issuer = custDoc.IssuingAuthority;
                    dra.Transferor.Identification.Number = custDoc.DocumentNumber;
                    dra.Transferor.Identification.Type = TypeOfDoc;
                }
                else
                {
                    dra.Transferor.Identification.Issuer = "NO INFO";
                    dra.Transferor.Identification.Number = "NO INFO";
                    dra.Transferor.Identification.Type = "D";
                }

                dra.OrderingInstn = new IFTIModel.OrderingInstn();//Agent details
                dra.OrderingInstn.Id = "ID_" + item.TransactionID.ToString() + "-3";
                dra.OrderingInstn.Branch = new IFTIModel.Branch();
                dra.OrderingInstn.Branch.Id = "ID_" + item.TransactionID.ToString() + "-31";
                dra.OrderingInstn.Branch.MainAddress = new IFTIModel.MainAddress();
                dra.OrderingInstn.Branch.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-32";
                dra.OrderingInstn.Branch.MainAddress.Addr = agent.AgentAddress1;
                AgentAssignedCountry assC = new AgentAssignedCountry();
                assC = agencyAssSer.GetAll(x => x.AgentID == agent.AgentID, null, "").FirstOrDefault();
                Country agentCountry = countrySer.GetAll(x => x.CountryID == assC.CountryID.Value, null, "").SingleOrDefault();
                dra.OrderingInstn.Branch.MainAddress.Country = "AUSTRALIA";
                dra.OrderingInstn.Branch.MainAddress.Suburb = agent.AgentCity;
                dra.OrderingInstn.Branch.MainAddress.State = agent.AgentState;
                dra.OrderingInstn.Branch.MainAddress.Postcode = agent.AgentPostcode;
                dra.OrderingInstn.Branch.FullName = agent.AgentName;

                dra.OrderingInstn.ForeignBased = "N";

                dra.InitiatingInstn = new IFTIModel.InitiatingInstn();//no idea
                dra.InitiatingInstn.Id = "ID_" + item.TransactionID.ToString() + "-4";
                dra.InitiatingInstn.SameAsOrderingInstn = "Y";

                dra.SendingInstn = new IFTIModel.SendingInstn();//no idea
                dra.SendingInstn.Id = "ID_" + item.TransactionID.ToString() + "-5";
                dra.SendingInstn.FullName = SetServ.GetAll(x => x.SettingName == "MasterCompany", null, "").SingleOrDefault().SettingsVariable;
                dra.SendingInstn.MainAddress = new IFTIModel.MainAddress();
                dra.SendingInstn.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-51";
                dra.SendingInstn.MainAddress.Addr = SetServ.GetAll(x => x.SettingName == "MasterCompanyAddressLine1", null, "").SingleOrDefault().SettingsVariable;
                dra.SendingInstn.MainAddress.Country = SetServ.GetAll(x => x.SettingName == "MasterCompanyCountry", null, "").SingleOrDefault().SettingsVariable;
                dra.SendingInstn.MainAddress.Suburb = SetServ.GetAll(x => x.SettingName == "MasterCompanySuburb", null, "").SingleOrDefault().SettingsVariable;
                dra.SendingInstn.MainAddress.State = SetServ.GetAll(x => x.SettingName == "MasterCompanyState", null, "").SingleOrDefault().SettingsVariable;
                dra.SendingInstn.MainAddress.Postcode = SetServ.GetAll(x => x.SettingName == "MasterCompanyPostcode", null, "").SingleOrDefault().SettingsVariable;
                //dra.SendingInstn.Branch.FullName = SetServ.GetAll(x => x.SettingName == "MasterCompany", null, "").SingleOrDefault().SettingsVariable;


                beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
                var BPMDetails = BPMServ.GetAll(x => x.AccountID == item.AccountID.ToString(), null, "").SingleOrDefault();

                BankService BNKServ = new BankService(new UnitOfWorks(new KaprukaEntities()));
                var BankDetails = BNKServ.GetAll(x => x.BankCode == BPMDetails.BankID, null, "").SingleOrDefault();

                CountryService CNTServ = new CountryService(new UnitOfWorks(new KaprukaEntities()));
                var CNTDetails = CNTServ.GetAll(x => x.CountryID == BankDetails.CountryID, null, "").SingleOrDefault();

                dra.ReceivingInstn = new IFTIModel.ReceivingInstn();//no idea
                dra.ReceivingInstn.Id = "ID_" + item.TransactionID.ToString() + "-6";
                dra.ReceivingInstn.FullName = BPMDetails.BankName;
                dra.ReceivingInstn.MainAddress = new IFTIModel.MainAddress();
                dra.ReceivingInstn.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-61";
                dra.ReceivingInstn.MainAddress.Addr = BankDetails.AddressLine1.ToString();
                dra.ReceivingInstn.MainAddress.Suburb = BankDetails.Suburb;
                dra.ReceivingInstn.MainAddress.State = BankDetails.State;
                dra.ReceivingInstn.MainAddress.Country = CNTDetails.CountryName;


                dra.BeneficiaryInstn = new IFTIModel.BeneficiaryInstn();
                dra.BeneficiaryInstn.Id = "ID_" + item.TransactionID.ToString() + "-7";
                dra.BeneficiaryInstn.SameAsReceivingInstn = "Y";

                dra.Transferee = new IFTIModel.Transferee();//beneficiary
                dra.Transferee.Id = "ID_" + item.TransactionID.ToString() + "-9";
                dra.Transferee.FullName = beneficiaryList.BeneficiaryName;
                dra.Transferee.MainAddress = new IFTIModel.MainAddress();
                dra.Transferee.MainAddress.Addr = beneficiaryList.AddressLine1 + " " + beneficiaryList.AddressLine2;
                dra.Transferee.MainAddress.Country = beneficiaryList.CountryOfBirth;
                dra.Transferee.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-91";
                dra.Transferee.MainAddress.Postcode = beneficiaryList.Postcode;
                dra.Transferee.MainAddress.State = "NO STATE";
             
                dra.Transferee.MainAddress.Suburb = "NO SUBURB";
                dra.Transferee.MainAddress.Country = beneficiaryList.Country;

                dra.Transferee.Phone = beneficiaryList.Mobile;
                dra.Transferee.Account = new IFTIModel.Account();
                dra.Transferee.Account.Id = "ID_" + item.TransactionID.ToString() + "-92";
                if (payMenthod != null)
                {
                    dra.Transferee.Account.AcctNumber = payMenthod.AccountNumber;
                    if (string.IsNullOrEmpty(payMenthod.AccountName))
                    {
                        dra.Transferee.Account.Name = "";
                    }
                    else
                    {
                        if (payMenthod.AccountName.Length > 35)
                        {
                            dra.Transferee.Account.Name = payMenthod.AccountName.Substring(0,35);
                        }
                        else
                        {
                            dra.Transferee.Account.Name = payMenthod.AccountName;
                        }
                        
                    }
                    dra.Transferee.Account.City = payMenthod.BranchName;
                    dra.Transferee.Account.Country = goingCountry;
                }
                Country benCountry = countrySer.GetAll(x => x.CountryID == beneficiaryList.CountryID, null, "").SingleOrDefault();
                if (benCountry != null)
                {
                    dra.Transferee.Account.Country = benCountry.CountryName;
                }
                else
                {
                    dra.Transferee.Account.Country = goingCountry;
                }
                dra.AdditionalDetails = new IFTIModel.AdditionalDetails();
                dra.AdditionalDetails.Id = "ID_" + item.TransactionID.ToString() + "-10";
                dra.AdditionalDetails.reasonForTransfer = item.Purpose.ToString();

                model.Iftidra.Add(dra);
                //  }

                //  kl = kl + 1;
            }

            return model;
        }

        private string ChangeDOB(string dob)
        {
            if (!string.IsNullOrEmpty(dob))
            {
                string[] arr = dob.Split('/');
                int val1 = int.Parse(arr[1]);
                int val2 = int.Parse(arr[0]);

                string val1st = val1.ToString();
                if (val1 < 10)
                {
                    val1st = "0" + val1;
                }
                string val2st = val2.ToString();
                if (val2 < 10)
                {
                    val2st = "0" + val2;
                }

                return arr[2] + "-" + val1st + "-" + val2st;
            }
            else
            {
                return "";
            }
        }

        private TtrfbsList GenerateTTRReport(List<Transaction> ttrList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentService = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));

            TtrfbsList model = new TtrfbsList();
            model.Ttrfbs = new List<Ttrfbs>();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;

            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Ttrfbs = new List<Ttrfbs>();
            model.ReportCount = ttrList.Count().ToString("N0");

            foreach (Transaction item in ttrList)
            {
                Kapruka.Repository.Customer customer = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                Kapruka.Repository.Agent agent = agentService.GetAll(x => x.AgentID == customer.AgentID, null, "").SingleOrDefault();

                Kapruka.Repository.BeneficiaryPaymentMethod paymntMethod = paymentMethodService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                Ttrfbs ttr = new Ttrfbs();
                ttr.Header = new HeaderTTR();
                ttr.Header.InterceptFlag = "";
                ttr.Header.ReportingBranch = "";
                ttr.Header.TxnRefNo = "";

                ttr.Agent = new List<Kapruka.PublicModels.Agent>();
                ttr.Agent[0].Abn = agent.AgentABN;
                ttr.Agent[0].FullName = agent.AgentName;
                ttr.Agent[0].MainAddress = agent.AgentAddress1;
                ttr.Agent[0].PostalAddress = agent.AgentAddress1;
                ttr.Agent[0].Phone = agent.AgentPhone;
                ttr.Agent[0].IndOcc = "";
                ttr.Agent[0].Acn = agent.AgentACN;
                ttr.Agent[0].Arbn = "";
                ttr.Agent[0].BusinessStruct = "";
                ttr.Agent[0].Dob = "";
                ttr.Agent[0].Account = new AccountTTR();
                ttr.Agent[0].Account.Type = "";
                ttr.Agent[0].Identification = "";
                ttr.Agent[0].ElectDataSrc = "";
                ttr.Agent[0].PartyIsRecipient = "";

                ttr.Customer[0].FullName = customer.FullName;

                ttr.Customer[0].MainAddress = customer.AddressLine1;
                ttr.Customer[0].PostalAddress = customer.AddressLine1;
                ttr.Customer[0].Phone = customer.Mobile;
                ttr.Customer[0].IndOcc = "";
                ttr.Customer[0].Abn = "";
                ttr.Customer[0].Acn = "";
                ttr.Customer[0].Arbn = "";
                ttr.Customer[0].BusinessStruct = "";
                ttr.Customer[0].Dob = customer.DOB;
                ttr.Customer[0].Account.Type = paymntMethod.AccountType;
                ttr.Customer[0].Identification = "";
                ttr.Customer[0].ElectDataSrc = "";
                ttr.Customer[0].PartyIsRecipient = "";

                ttr.Recipient[0].Account = new AccountTTR();
                ttr.Recipient[0].Account.Type = "";
                ttr.Recipient[0].Dob = "";
                ttr.Recipient[0].FullName = "";
                ttr.Recipient[0].MainAddress = "";

                // need to set transaction details.not clear


            }

            return null;
        }

        private SmrList GenerateSMRReport(List<Transaction> smrList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));


            SmrList model = new SmrList();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;

            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Smr = new List<Smr>();
            model.ReportCount = smrList.Count().ToString("N0");

            foreach (var item in smrList)
            {
                Smr smr = new Smr();
                smr.Header = new HeaderSMR();
                smr.Header.InterceptFlag = "";
                smr.Header.ReportingBranch = "";
                smr.Header.ReReportRef = "";

                smr.SmDetails = new SmDetails();
                smr.SmDetails.DesignatedSvc = "";
                smr.SmDetails.DesignatedSvcEnquiry = "";
                smr.SmDetails.DesignatedSvcProvided = "";
                smr.SmDetails.DesignatedSvcRequested = "";
                smr.SmDetails.GrandTotal = "";
                smr.SmDetails.SuspReason = "";
                smr.SmDetails.SuspReasonOther = "";

                smr.SuspGrounds.GroundsForSuspicion = "";
                smr.SuspPerson = new List<SuspPerson>();//need to check whether customer or beneficiery



            }

            return model;

        }

        #region GenerateIFTReport
        private IftieList GenerateIFTReport(List<Transaction> transactionList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));


            IftieList model = new IftieList();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;
            model.Ns2 = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2";
            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Structured = new List<Structured>();
            model.ReportCount = transactionList.Count().ToString("N0");
            foreach (Transaction item in transactionList)
            {
                List<Customer> customerList = new List<Customer>();
                List<Beneficiary> beneficiaryList = new List<Beneficiary>();
                customerList = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").ToList();
                beneficiaryList = beneService.GetAll(x => x.BeneficiaryID == item.BeneficiaryID, null, "").ToList();

                Structured structure = new Structured();
                structure.Header = new HeaderIFT();
                structure.Transaction = new TransactionIFT();

                //Header section
                structure.Header.Id = "";
                structure.Header.InterceptFlag = "";
                structure.Header.TxnRefNo = "";

                //Transaction section
                structure.Transaction.TransferDate = item.CreatedDateTime.Value.ToString("dd MMM yyyy");
                structure.Transaction.Direction = "";//I or O in to Aus ot=r out to AUS
                structure.Transaction.CurrencyAmount = new CurrencyAmount();
                structure.Transaction.CurrencyAmount.Id = item.TransactionID.ToString();
                structure.Transaction.CurrencyAmount.Amount = item.RemittedAmount.Value.ToString("N2");//Not sure
                structure.Transaction.CurrencyAmount.Currency = item.RemittedCurrency;//not sure
                structure.Transaction.ValueDate = "";

                structure.Payer = new List<PayerIFT>();
                structure.Payee = new List<PayeeIFT>();
                //payer details
                foreach (Customer customer in customerList)
                {
                    PayerIFT payer = new PayerIFT();
                    payer.MainAddress = new MainAddress();
                    //Main Address section
                    payer.FullName = customer.FullName;
                    payer.MainAddress.Addr = customer.AddressLine1 + " " + customer.AddressLine2;
                    payer.MainAddress.Suburb = customer.Suburb;
                    payer.MainAddress.State = customer.State;
                    payer.MainAddress.Postcode = customer.Postcode;
                    payer.MainAddress.Country = customer.CountryOfBirth;

                    payer.Account = new List<AccountIFT>();
                    payer.Account = GenerateAccountDetailsForPayerIFT(item);
                    payer.Abn = "";
                    payer.Acn = "";
                    payer.Arbn = "";
                    payer.Identification = GenerateIdentificationForPayerIFT(item);
                    payer.CustNo = customer.CustomerID.Value.ToString();
                    payer.IndividualDetails = new IndividualDetailsIFT();
                    payer.IndividualDetails.Dob = customer.DOB;
                    payer.IndividualDetails.Id = customer.CustomerID.Value.ToString();
                    payer.IndividualDetails.PlaceOfBirth = new PlaceOfBirth();
                    payer.IndividualDetails.PlaceOfBirth.Country = customer.CountryOfBirth;
                    payer.IndividualDetails.PlaceOfBirth.Town = customer.PlaceOfBirth;

                    structure.Payer.Add(payer);
                }
                //payee details
                foreach (Beneficiary beneficiary in beneficiaryList)
                {
                    PayeeIFT payee = new PayeeIFT();
                    payee.MainAddress = new MainAddress();
                    //Main Address section
                    payee.FullName = beneficiary.BeneficiaryName;
                    payee.MainAddress.Addr = beneficiary.AddressLine1 + " " + beneficiary.AddressLine2;
                    payee.MainAddress.Suburb = beneficiary.Suburb;
                    payee.MainAddress.State = beneficiary.State;
                    payee.MainAddress.Postcode = beneficiary.Postcode;
                    payee.MainAddress.Country = beneficiary.CountryOfBirth;

                    payee.Account = new List<AccountIFT>();
                    payee.Account = GenerateAccountDetailsForPayerIFT(item);
                    payee.Identification = GenerateIdentificationForPayerIFT(item);


                    structure.Payee.Add(payee);
                }

                model.Structured.Add(structure);


            }
            return model;
        }

        private List<IdentificationIFT> GenerateIdentificationForPayeeIFT(Transaction item)
        {
            List<AccountIFT> accList = new List<AccountIFT>();
            return null;
        }

        private List<AccountIFT> GenerateAccountDetailsForPayeeIFT(Transaction trans)
        {
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            string transAccID = trans.AccountID.Value.ToString();
            string benId = trans.BeneficiaryID.Value.ToString();
            var lst = paymentMethodService.GetAll(x => x.AccountID == transAccID && x.BeneficiaryID == benId, null, "").ToList();
            List<AccountIFT> accList = new List<AccountIFT>();
            foreach (var item in lst)
            {
                AccountIFT acc = new AccountIFT();
                acc.Bsb = "";//no idea
                acc.Id = item.AccountID;
                acc.Number = item.AccountNumber;
                accList.Add(acc);

            }

            return accList;
        }
        //need to implement
        private List<IdentificationIFT> GenerateIdentificationForPayerIFT(Transaction item)
        {
            List<AccountIFT> accList = new List<AccountIFT>();
            return null;
        }

        private List<AccountIFT> GenerateAccountDetailsForPayerIFT(Transaction trans)
        {
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            string transAccID = trans.AccountID.Value.ToString();
            var lst = paymentMethodService.GetAll(x => x.AccountID == transAccID && x.CustomerID == trans.CustomerID, null, "").ToList();
            List<AccountIFT> accList = new List<AccountIFT>();
            foreach (var item in lst)
            {
                AccountIFT acc = new AccountIFT();
                acc.Bsb = "";//no idea
                acc.Id = item.AccountID;
                acc.Number = item.AccountNumber;
                accList.Add(acc);

            }

            return accList;
        }

        #endregion

        public void SerializeObject<T>(T serializableObject, string filePath, string name)
        {
            if (serializableObject == null) { return; }

            try
            {

                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    XmlSerializerNamespaces nam = new XmlSerializerNamespaces();

                    nam.Add("versionMinor", "1");
                    // nam.Add("xs", ":");
                    nam.Add("versionMajor", "1");

                    serializer.Serialize(stream, serializableObject, nam);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(filePath);
                    stream.Close();

                    Response.Clear();
                    Response.ContentType = "application/force-download";
                    Response.AddHeader("content-disposition", "attachment;    filename=" + name + ".xml");
                    byte[] bytesInStream = stream.ToArray(); // simpler way of converting to array
                    Response.BinaryWrite(bytesInStream);
                    Response.End();
                }





            }
            catch (Exception ex)
            {
               // lblError.Text = ex.ToString();
                //Log exception here
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }

            return objectOut;
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/CancelledTransactionsReport.aspx");
        }

        protected void Button11_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/SummaryReportViewer.aspx");
        }

        protected void Button5_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/CustomersWithAccountBalance.aspx");
            
        }

        protected void Button6_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/CustomersWithoutDocumentation.aspx");
        }

        protected void Button8_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/HighRiskCustomersWithTransactions.aspx");
        }

        protected void Button9_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/InveticoMasterReport.aspx");
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/ActivityReport.aspx");
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/MultiCustToSameBene.aspx");
        }
    }
}