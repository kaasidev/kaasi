﻿using Kapruka.Enterprise;
using Kapruka.PublicModels;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_
{
    public partial class Reports2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/CustomerDepositsReport.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("TransactionsByDateRange.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/AgentStatsSummary.aspx");
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("AgentAssignedRatesReport.aspx");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/ListOfTransactionsViewer.aspx");
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/EscalatedtoCompliancTransactionsReport.aspx");
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports\\Trsummery.aspx");
        }

        protected void btnAustract_Click(object sender, EventArgs e)
        {
            TransactionService transactionService = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            var transactionList = transactionService.GetAll(x => x.InsertedIntoAustracFileByMaster == "N", null, "").ToList();
            //  var smrList = transactionService.GetAll(x => x.isSuspicious == "N" && x.InsertedIntoAustracFile=="N", null, "").ToList();
            // var ttrList = transactionService.GetAll(x => x.isThreshold == "N" && x.InsertedIntoAustracFile == "N", null, "").ToList();
            string fileName = DateTime.Now.ToString("ddMMyyyyHHmmss");
            // IftieList iftModel = new IftieList();
            SmrList smrModel = new SmrList();
            TtrfbsList ttrModel = new TtrfbsList();
            IFTIModel.IftidraList iftModel = new IFTIModel.IftidraList();
            iftModel.VersionMajor = "1";
            iftModel.VersionMinor = "1";
            //iftModel= GenerateIFTReport(transactionList, fileName);
            iftModel = GenerateIFTReportML(transactionList, fileName);
            // smrModel = GenerateSMRReport(smrList, fileName);
            //  ttrModel = GenerateTTRReport(ttrList,fileName);

            //IftieList hd = new PublicModels.IftieList();

            //hd.FileName = "te";
            //hd.Ns1 = "dsds";
            //hd.Ns2 = "ds s ";
            ////lst.Add(hd);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            string filePath = Server.MapPath("/Files") + "\\" + fileName + ".xml";
            using (MemoryStream ms = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(ms, settings))
                {
                    var serializer = new XmlSerializer(typeof(IFTIModel.IftidraList));
                    serializer.Serialize(writer, iftModel);
                }

                ms.Position = 0;
                XDocument doc = XDocument.Load(new XmlTextReader(ms));
                doc.Root.Add(new XAttribute("versionMinor", "1"));
                doc.Root.Add(new XAttribute("versionMajor", "1"));
                doc.Save(filePath);

                DownloadTheFile(filePath, fileName);



                //ms.Close();
                //Response.Clear();
                //Response.ContentType = "application/force-download";
                //Response.AddHeader("content-disposition", "attachment;    filename=" + fileName + ".xml");
                //byte[] bytesInStream = ms.ToArray(); // simpler way of converting to array
                //Response.BinaryWrite(bytesInStream);
                //Response.End();
            }
            //  SerializeObject(iftModel, filePath, "\\xml" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xml");
            UpdateTable(fileName + ".xml");
        }

        public void UpdateTable(string fileName)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string sql = "update Transactions set InsertedIntoAustracFileNameByMaster='" + fileName + "',InsertedIntoAustracFileDateTimeByMaster=getdate(),InsertedIntoAustracFileByMaster='Y' where InsertedIntoAustracFileByMaster='N'";

            using (SqlCommand cmd = new SqlCommand(sql))
            {
                cmd.Connection = conn;
                conn.Open();

                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void DownloadTheFile(string url, string fileName)
        {
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                var fileReq = System.Net.HttpWebRequest.Create(url);

                //Create a response for this request
                var fileResp = fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xml\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
        }

        private Kapruka.PublicModels.IFTIModel.IftidraList GenerateIFTReportML(List<Transaction> transactionList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            CustomerDocumentService custDocSer = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            beneficiaryPaymentMethodService payMethodSer = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            CountryService countrySer = new CountryService(new UnitOfWorks(new KaprukaEntities()));
            AgentAssignedCountryService agencyAssSer = new AgentAssignedCountryService(new UnitOfWorks(new KaprukaEntities()));
            Kapruka.PublicModels.IFTIModel.IftidraList model = new Kapruka.PublicModels.IFTIModel.IftidraList();

            model.FileName = "IFT_" + fileName + ".xml";
            model.Xmlns = null;

            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 

            model.Iftidra = new List<IFTIModel.Iftidra>();
            if (transactionList != null)
            {
                model.ReportCount = transactionList.Count();
            }
            else
            {
                model.ReportCount = 0;
            }

            // int kl = 0;
            foreach (Transaction item in transactionList)
            {
                //   if(kl==0)
                //   {
                Customer customerList = new Customer();
                Beneficiary beneficiaryList = new Beneficiary();
                CustomerDocument custDoc = new CustomerDocument();
                BeneficiaryPaymentMethod payMenthod = new BeneficiaryPaymentMethod();
                Kapruka.Repository.Agent agent = new Kapruka.Repository.Agent();

                customerList = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                beneficiaryList = beneService.GetAll(x => x.BeneficiaryID == item.BeneficiaryID, null, "").SingleOrDefault();
                var custDocList = custDocSer.GetAll(x => x.CustomerID == customerList.CustomerID, null, "").ToList();
                if (custDocList.Count == 0)
                {
                    custDoc = null;
                }
                else
                {
                    custDoc = custDocList[0];
                }

                string benId = beneficiaryList.BeneficiaryID.Value.ToString();
                var payMenthodList = payMethodSer.GetAll(x => x.BeneficiaryID == benId, null, "").ToList();
                if (payMenthodList.Count == 0)
                {
                    payMenthod = null;
                }
                else
                {
                    payMenthod = payMenthodList[0];
                }
                int agentId = customerList.AgentID.Value;
                agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();


                IFTIModel.Iftidra dra = new IFTIModel.Iftidra();
                dra.Id = item.TransactionID.ToString();
                dra.Header = new IFTIModel.Header();
                dra.Header.Id = item.TransactionID.ToString() + "-0";
                dra.Header.InterceptFlag = "N";
                dra.Header.TxnRefNo = item.TransactionID.ToString();

                dra.Transaction = new IFTIModel.Transaction();
                dra.Transaction.Id = item.TransactionID.ToString() + "-1";
                dra.Transaction.TxnDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");
                dra.Transaction.CurrencyAmount = new IFTIModel.CurrencyAmount();
                dra.Transaction.CurrencyAmount.Id = item.TransactionID.ToString() + "-11";
                dra.Transaction.CurrencyAmount.Amount = item.RemittedAmount.Value.ToString("N2");
                dra.Transaction.CurrencyAmount.Currency = item.RemittedCurrency;
                dra.Transaction.CurrencyAmount.Id = "";
                dra.Transaction.Direction = "";
                dra.Transaction.TfrType = new IFTIModel.TfrType();
                dra.Transaction.TfrType.Type = item.TransactionType;
                dra.Transaction.TfrType.Id = "";
                dra.Transaction.ValueDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");

                dra.Transferor = new IFTIModel.Transferor();//customer
                dra.Transferor.FullName = customerList.FullName;
                dra.Transferor.MainAddress = new IFTIModel.MainAddress();
                dra.Transferor.MainAddress.Addr = customerList.AddressLine1 + " " + customerList.AddressLine2;
                dra.Transferor.MainAddress.Country = customerList.CountryOfBirth;
                dra.Transferor.MainAddress.Id = customerList.CustomerID.Value.ToString();
                dra.Transferor.MainAddress.Postcode = customerList.Postcode;
                dra.Transferor.MainAddress.State = customerList.State;
                dra.Transferor.MainAddress.Suburb = customerList.Suburb;

                dra.Transferor.Phone = customerList.Mobile;
                dra.Transferor.Email = customerList.EmailAddress;
                dra.Transferor.CustNo = customerList.CustomerID.Value.ToString();
                dra.Transferor.Dob = customerList.DOB;
                dra.Transferor.Identification = new IFTIModel.Identification();
                if (custDoc != null)
                {
                    dra.Transferor.Identification.Id = custDoc.CustDocID.ToString();
                    dra.Transferor.Identification.Issuer = custDoc.IssuingAuthority;
                    dra.Transferor.Identification.Number = custDoc.DocumentNumber;
                    dra.Transferor.Identification.Type = custDoc.TypeOfDocument;
                }
                dra.Transferee = new IFTIModel.Transferee();//beneficiary
                dra.Transferee.FullName = beneficiaryList.BeneficiaryName;
                dra.Transferee.MainAddress = new IFTIModel.MainAddress();
                dra.Transferee.MainAddress.Addr = beneficiaryList.AddressLine1 + " " + beneficiaryList.AddressLine2;
                dra.Transferee.MainAddress.Country = beneficiaryList.CountryOfBirth;
                dra.Transferee.MainAddress.Id = beneficiaryList.CustomerID.Value.ToString();
                dra.Transferee.MainAddress.Postcode = beneficiaryList.Postcode;
                dra.Transferee.MainAddress.State = beneficiaryList.State;
                dra.Transferee.MainAddress.Suburb = beneficiaryList.Suburb;

                dra.Transferee.Phone = beneficiaryList.Mobile;
                dra.Transferee.Account = new IFTIModel.Account();
                if (payMenthod != null)
                {
                    dra.Transferee.Account.AcctNumber = payMenthod.AccountNumber;
                    dra.Transferee.Account.Name = payMenthod.AccountName;
                    dra.Transferee.Account.City = payMenthod.BranchName;
                }
                Country benCountry = countrySer.GetAll(x => x.CountryID == beneficiaryList.CountryID, null, "").SingleOrDefault();
                if (benCountry != null)
                {
                    dra.Transferee.Account.Country = benCountry.CountryName;
                }
                else
                {
                    dra.Transferee.Account.Country = "";
                }


                dra.OrderingInstn = new IFTIModel.OrderingInstn();//Agent details
                dra.OrderingInstn.Branch = new IFTIModel.Branch();
                dra.OrderingInstn.Branch.Id = agent.AgentID.ToString();
                dra.OrderingInstn.Branch.MainAddress = new IFTIModel.MainAddress();
                dra.OrderingInstn.Branch.MainAddress.Addr = agent.AgentAddress1;
                AgentAssignedCountry assC = new AgentAssignedCountry();
                assC = agencyAssSer.GetAll(x => x.AgentID == agent.AgentID, null, "").FirstOrDefault();
                Country agentCountry = countrySer.GetAll(x => x.CountryID == assC.CountryID.Value, null, "").SingleOrDefault();
                dra.OrderingInstn.Branch.MainAddress.Country = agentCountry.CountryName;
                dra.OrderingInstn.Branch.MainAddress.Suburb = agent.AgentCity;
                dra.OrderingInstn.Branch.MainAddress.State = agent.AgentState;
                dra.OrderingInstn.Branch.MainAddress.Postcode = agent.AgentPostcode;
                dra.OrderingInstn.Branch.FullName = agent.AgentName;

                dra.OrderingInstn.ForeignBased = "N";//no idea

                dra.InitiatingInstn = new IFTIModel.InitiatingInstn();//no idea

                dra.ReceivingInstn = new IFTIModel.ReceivingInstn();//no idea

                dra.SendingInstn = new IFTIModel.SendingInstn();//no idea

                model.Iftidra.Add(dra);
                //  }

                //  kl = kl + 1;
            }

            return model;
        }

        private TtrfbsList GenerateTTRReport(List<Transaction> ttrList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentService = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));

            TtrfbsList model = new TtrfbsList();
            model.Ttrfbs = new List<Ttrfbs>();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;

            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Ttrfbs = new List<Ttrfbs>();
            model.ReportCount = ttrList.Count().ToString("N0");

            foreach (Transaction item in ttrList)
            {
                Kapruka.Repository.Customer customer = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                Kapruka.Repository.Agent agent = agentService.GetAll(x => x.AgentID == customer.AgentID, null, "").SingleOrDefault();

                Kapruka.Repository.BeneficiaryPaymentMethod paymntMethod = paymentMethodService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                Ttrfbs ttr = new Ttrfbs();
                ttr.Header = new HeaderTTR();
                ttr.Header.InterceptFlag = "";
                ttr.Header.ReportingBranch = "";
                ttr.Header.TxnRefNo = "";

                ttr.Agent = new List<Kapruka.PublicModels.Agent>();
                ttr.Agent[0].Abn = agent.AgentABN;
                ttr.Agent[0].FullName = agent.AgentName;
                ttr.Agent[0].MainAddress = agent.AgentAddress1;
                ttr.Agent[0].PostalAddress = agent.AgentAddress1;
                ttr.Agent[0].Phone = agent.AgentPhone;
                ttr.Agent[0].IndOcc = "";
                ttr.Agent[0].Acn = agent.AgentACN;
                ttr.Agent[0].Arbn = "";
                ttr.Agent[0].BusinessStruct = "";
                ttr.Agent[0].Dob = "";
                ttr.Agent[0].Account = new AccountTTR();
                ttr.Agent[0].Account.Type = "";
                ttr.Agent[0].Identification = "";
                ttr.Agent[0].ElectDataSrc = "";
                ttr.Agent[0].PartyIsRecipient = "";

                ttr.Customer[0].FullName = customer.FullName;

                ttr.Customer[0].MainAddress = customer.AddressLine1;
                ttr.Customer[0].PostalAddress = customer.AddressLine1;
                ttr.Customer[0].Phone = customer.Mobile;
                ttr.Customer[0].IndOcc = "";
                ttr.Customer[0].Abn = "";
                ttr.Customer[0].Acn = "";
                ttr.Customer[0].Arbn = "";
                ttr.Customer[0].BusinessStruct = "";
                ttr.Customer[0].Dob = customer.DOB;
                ttr.Customer[0].Account.Type = paymntMethod.AccountType;
                ttr.Customer[0].Identification = "";
                ttr.Customer[0].ElectDataSrc = "";
                ttr.Customer[0].PartyIsRecipient = "";

                ttr.Recipient[0].Account = new AccountTTR();
                ttr.Recipient[0].Account.Type = "";
                ttr.Recipient[0].Dob = "";
                ttr.Recipient[0].FullName = "";
                ttr.Recipient[0].MainAddress = "";

                // need to set transaction details.not clear


            }

            return null;
        }

        private SmrList GenerateSMRReport(List<Transaction> smrList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));


            SmrList model = new SmrList();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;

            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Smr = new List<Smr>();
            model.ReportCount = smrList.Count().ToString("N0");

            foreach (var item in smrList)
            {
                Smr smr = new Smr();
                smr.Header = new HeaderSMR();
                smr.Header.InterceptFlag = "";
                smr.Header.ReportingBranch = "";
                smr.Header.ReReportRef = "";

                smr.SmDetails = new SmDetails();
                smr.SmDetails.DesignatedSvc = "";
                smr.SmDetails.DesignatedSvcEnquiry = "";
                smr.SmDetails.DesignatedSvcProvided = "";
                smr.SmDetails.DesignatedSvcRequested = "";
                smr.SmDetails.GrandTotal = "";
                smr.SmDetails.SuspReason = "";
                smr.SmDetails.SuspReasonOther = "";

                smr.SuspGrounds.GroundsForSuspicion = "";
                smr.SuspPerson = new List<SuspPerson>();//need to check whether customer or beneficiery



            }

            return model;

        }

        #region GenerateIFTReport
        private IftieList GenerateIFTReport(List<Transaction> transactionList, string fileName)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));


            IftieList model = new IftieList();
            model.FileName = "IFT_" + fileName + ".xml";
            model.Ns1 = null;
            model.Ns2 = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2";
            model.ReNumber = "";//This is the business number allocated to a reporting entity as they enrol or register with AUSTRAC. This parameter is optional. However, it is required when the AUSTRAC Online user ID specified in the “userId” parameter is associated with more than one (1) reporting entity. 
            model.SchemaLocation = "http://austrac.gov.au/schema/reporting/IFTI-E-1-2 IFTI-E-1-2.xsd null ExternalTypeLibrary-1-1.xsd";
            model.Structured = new List<Structured>();
            model.ReportCount = transactionList.Count().ToString("N0");
            foreach (Transaction item in transactionList)
            {
                List<Customer> customerList = new List<Customer>();
                List<Beneficiary> beneficiaryList = new List<Beneficiary>();
                customerList = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").ToList();
                beneficiaryList = beneService.GetAll(x => x.BeneficiaryID == item.BeneficiaryID, null, "").ToList();

                Structured structure = new Structured();
                structure.Header = new HeaderIFT();
                structure.Transaction = new TransactionIFT();

                //Header section
                structure.Header.Id = "";
                structure.Header.InterceptFlag = "";
                structure.Header.TxnRefNo = "";

                //Transaction section
                structure.Transaction.TransferDate = item.CreatedDateTime.Value.ToString("dd MMM yyyy");
                structure.Transaction.Direction = "";//I or O in to Aus ot=r out to AUS
                structure.Transaction.CurrencyAmount = new CurrencyAmount();
                structure.Transaction.CurrencyAmount.Id = item.TransactionID.ToString();
                structure.Transaction.CurrencyAmount.Amount = item.RemittedAmount.Value.ToString("N2");//Not sure
                structure.Transaction.CurrencyAmount.Currency = item.RemittedCurrency;//not sure
                structure.Transaction.ValueDate = "";

                structure.Payer = new List<PayerIFT>();
                structure.Payee = new List<PayeeIFT>();
                //payer details
                foreach (Customer customer in customerList)
                {
                    PayerIFT payer = new PayerIFT();
                    payer.MainAddress = new MainAddress();
                    //Main Address section
                    payer.FullName = customer.FullName;
                    payer.MainAddress.Addr = customer.AddressLine1 + " " + customer.AddressLine2;
                    payer.MainAddress.Suburb = customer.Suburb;
                    payer.MainAddress.State = customer.State;
                    payer.MainAddress.Postcode = customer.Postcode;
                    payer.MainAddress.Country = customer.CountryOfBirth;

                    payer.Account = new List<AccountIFT>();
                    payer.Account = GenerateAccountDetailsForPayerIFT(item);
                    payer.Abn = "";
                    payer.Acn = "";
                    payer.Arbn = "";
                    payer.Identification = GenerateIdentificationForPayerIFT(item);
                    payer.CustNo = customer.CustomerID.Value.ToString();
                    payer.IndividualDetails = new IndividualDetailsIFT();
                    payer.IndividualDetails.Dob = customer.DOB;
                    payer.IndividualDetails.Id = customer.CustomerID.Value.ToString();
                    payer.IndividualDetails.PlaceOfBirth = new PlaceOfBirth();
                    payer.IndividualDetails.PlaceOfBirth.Country = customer.CountryOfBirth;
                    payer.IndividualDetails.PlaceOfBirth.Town = customer.PlaceOfBirth;

                    structure.Payer.Add(payer);
                }
                //payee details
                foreach (Beneficiary beneficiary in beneficiaryList)
                {
                    PayeeIFT payee = new PayeeIFT();
                    payee.MainAddress = new MainAddress();
                    //Main Address section
                    payee.FullName = beneficiary.BeneficiaryName;
                    payee.MainAddress.Addr = beneficiary.AddressLine1 + " " + beneficiary.AddressLine2;
                    payee.MainAddress.Suburb = beneficiary.Suburb;
                    payee.MainAddress.State = beneficiary.State;
                    payee.MainAddress.Postcode = beneficiary.Postcode;
                    payee.MainAddress.Country = beneficiary.CountryOfBirth;

                    payee.Account = new List<AccountIFT>();
                    payee.Account = GenerateAccountDetailsForPayerIFT(item);
                    payee.Identification = GenerateIdentificationForPayerIFT(item);


                    structure.Payee.Add(payee);
                }

                model.Structured.Add(structure);


            }
            return model;
        }

        private List<IdentificationIFT> GenerateIdentificationForPayeeIFT(Transaction item)
        {
            List<AccountIFT> accList = new List<AccountIFT>();
            return null;
        }

        private List<AccountIFT> GenerateAccountDetailsForPayeeIFT(Transaction trans)
        {
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            string transAccID = trans.AccountID.Value.ToString();
            string benId = trans.BeneficiaryID.Value.ToString();
            var lst = paymentMethodService.GetAll(x => x.AccountID == transAccID && x.BeneficiaryID == benId, null, "").ToList();
            List<AccountIFT> accList = new List<AccountIFT>();
            foreach (var item in lst)
            {
                AccountIFT acc = new AccountIFT();
                acc.Bsb = "";//no idea
                acc.Id = item.AccountID;
                acc.Number = item.AccountNumber;
                accList.Add(acc);

            }

            return accList;
        }
        //need to implement
        private List<IdentificationIFT> GenerateIdentificationForPayerIFT(Transaction item)
        {
            List<AccountIFT> accList = new List<AccountIFT>();
            return null;
        }

        private List<AccountIFT> GenerateAccountDetailsForPayerIFT(Transaction trans)
        {
            beneficiaryPaymentMethodService paymentMethodService = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            string transAccID = trans.AccountID.Value.ToString();
            var lst = paymentMethodService.GetAll(x => x.AccountID == transAccID && x.CustomerID == trans.CustomerID, null, "").ToList();
            List<AccountIFT> accList = new List<AccountIFT>();
            foreach (var item in lst)
            {
                AccountIFT acc = new AccountIFT();
                acc.Bsb = "";//no idea
                acc.Id = item.AccountID;
                acc.Number = item.AccountNumber;
                accList.Add(acc);

            }

            return accList;
        }

        #endregion

        public void SerializeObject<T>(T serializableObject, string filePath, string name)
        {
            if (serializableObject == null) { return; }

            try
            {

                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    XmlSerializerNamespaces nam = new XmlSerializerNamespaces();

                    nam.Add("versionMinor", "1");
                    // nam.Add("xs", ":");
                    nam.Add("versionMajor", "1");

                    serializer.Serialize(stream, serializableObject, nam);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(filePath);
                    stream.Close();

                    Response.Clear();
                    Response.ContentType = "application/force-download";
                    Response.AddHeader("content-disposition", "attachment;    filename=" + name + ".xml");
                    byte[] bytesInStream = stream.ToArray(); // simpler way of converting to array
                    Response.BinaryWrite(bytesInStream);
                    Response.End();
                }





            }
            catch (Exception ex)
            {
               // lblError.Text = ex.ToString();
                //Log exception here
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }

            return objectOut;
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports/CancelledTransactionsDetailReport.aspx");
        }

        protected void Button11_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/SummaryReportViewer.aspx");
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Reports/CustomerDepositsReport.aspx");
        }
    }
}