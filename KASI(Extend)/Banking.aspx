﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="Banking.aspx.cs" Inherits="KASI_Extend_.Banking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
    
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />

    <script type="text/javascript">
        function Redirect(URL) {
            window.location.href = URL;
            return false;
        };
        function closeLoad() {
           
            $("[id$=loadMask]").hide();
            $("[id$=loadImg]").hide();
        }
        function showAustractFileConfirm(){
            var con = confirm("Do you want to create AUSTRACT file?It might take some time.");
            if (con) {
                $("[id$=loadMask]").show();
                $("[id$=loadImg]").show();
                return true;
            }
            else {
                return false;
            }

}

        $(document).ready(function () {

            $('#div_bank02').hide();
            $('#div_bank03').hide();
            $('#div_bank04').hide();
            $('#div_bank05').hide();
            $('#div_bank06').hide();
            $('#div_bank07').hide();

            $('#BOCClick').click(function () {
                $('#div_bank02').hide();
                $('#div_bank03').hide();
                $('#div_bank04').hide();
                $('#div_bank05').hide();
                $('#div_bank06').hide();
                $('#div_bank07').hide();
                $('#div_bank01').show();
            });

            $('#CommBankClick').click(function () {
                $('#div_bank02').show();
                $('#div_bank01').hide();
                $('#div_bank03').hide();
                $('#div_bank04').hide();
                $('#div_bank05').hide();
                $('#div_bank06').hide();
                $('#div_bank07').hide();
            });

            $('#HNBClick').click(function () {
                $('#div_bank01').hide();
                $('#div_bank02').hide();
                $('#div_bank03').show();
                $('#div_bank04').hide();
                $('#div_bank05').hide();
                $('#div_bank06').hide();
                $('#div_bank07').hide();
            });

            $('#NSBClick').click(function () {
                $('#div_bank01').hide();
                $('#div_bank02').hide();
                $('#div_bank03').hide();
                $('#div_bank04').show();
                $('#div_bank05').hide();
                $('#div_bank06').hide();
                $('#div_bank07').hide();
            });

            $('#PBClick').click(function () {
                $('#div_bank01').hide();
                $('#div_bank02').hide();
                $('#div_bank03').hide();
                $('#div_bank04').hide();
                $('#div_bank05').show();
                $('#div_bank06').hide();
                $('#div_bank07').hide();
            });


            $('#SPClick').click(function () {
                $('#div_bank01').hide();
                $('#div_bank02').hide();
                $('#div_bank03').hide();
                $('#div_bank04').hide();
                $('#div_bank05').hide();
                $('#div_bank06').show();
                $('#div_bank07').hide();
            });

            $('#SBClick').click(function () {
                $('#div_bank01').hide();
                $('#div_bank02').hide();
                $('#div_bank03').hide();
                $('#div_bank04').hide();
                $('#div_bank05').hide();
                $('#div_bank06').hide();
                $('#div_bank07').show();
            });

             $('#btn_CreateBank01File').click(function () {
                $.ajax({
                    url: 'Processor/CreateBOCBankFile.aspx',
                    data: {
                        BankCode: '7010',
                        Del: ','
                    },
                    success: function (response) {
                        alert(response);
                        location.reload();
                    }
                });
            });

            $('#btn_CreateBank02File').click(function () {
                $.ajax({
                    url: 'Processor/CreateComBankFile.aspx',
                    data: {
                        BankCode: '7056',
                        Del: ','
                    },
                    success: function (response) {
                        alert(response);
                        location.reload();
                    }
                });
            });

            $('#btn_CreateBank03File').click(function () {
                $.ajax({
                    url: 'Processor/CreateHNBBankFile.aspx',
                    data: {
                        BankCode: '7083',
                        Del: ','
                    },
                    success: function (response) {
                        alert(response);
                        location.reload();
                    }
                });
            });

            $('#btn_CreateBank06File').click(function () {
                $.ajax({
                    url: 'Processor/CreateSAMPBankFile.aspx',
                    data: {
                        BankCode: '7278',
                    },
                    success: function (response) {
                        alert(response);
                        location.reload();
                    }
                });
            });

         

            $('#btn_CreateAustracFile').click(function () {
                $.ajax({
                    url: 'Processor/CreateAUSTRACFile.aspx',
                    success: function (response) {
                        alert('Austrac File Successfully Created')
                    }
                })
            });

            BANK01 = $('#bank01table').DataTable({
                ajax: {
                    url: 'JQDataFetch/getBOCPendingTrans.aspx',

                }
            });

            BANK02 = $('#bank02table').DataTable({
                ajax: {
                    url: 'JQDataFetch/getCommPendingTrans.aspx'
                }
                
            });

            BANK03 = $('#bank03table').DataTable({
                ajax: {
                    url: 'JQDataFetch/getHNBPendingTrans.aspx'
                }
            });

            BANK04 = $('#bank04table').DataTable({
                ajax: {
                    url: 'JQDataFetch/getNSBPendingTrans.aspx'
                }
            });

            BANK05 = $('#bank05table').DataTable({
                ajax: {
                    url: 'JQDataFetch/getPBPendingTrans.aspx'
                }
            });

            BANK06 = $('#bank06table').DataTable({
                ajax: {
                    url: 'JQDataFetch/getSampPendingTrans.aspx'
                }
            });

            BANK07 = $('#bank07table').DataTable({
                ajax: {
                    url: 'JQDataFetch/getSeyPendingTrans.aspx'
                }
            });
        });
        function LoadPPBFile() {
         //  alert("PBB startgin");
            $.ajax({
                url: 'Processor/CreatePBBankFile.aspx?BankCode=7135',
               
                success: function (response) {
                    alert(response);
                    // location.reload();
                },
                error: function (ee) {
                    alert(ee);
                }
            });
        }
    </script>
    <style type="text/css">
        .material-icons {
            font-size: 24px !important; /* Preferred icon size */
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    
    <form id="form1" runat="server">
        
      
              <div runat="server" id="loadMask" style="width:100%;height:100%;background-color:white;opacity:0.5;z-index:99999;position:fixed;display:none"></div>
    <img runat="server" id="loadImg"  style="z-index:999999;top:50%;left:50%;position:fixed;display:none" src="images/loading.gif" />
       
       
    <div class="container_btns">
        <table class="tbl_width_1200"> <!-- MAIN TABLE STARTS HERE -->
            <tr>
                <td class="rep_main_headings">BANKING</td>
                <td>&nbsp;</td>
                <td class="ali_right"><input type="button" style="display:none;" id="btn_ClickRedirect" value="*" onclick="return Redirect('BankingDash.aspx');"/>
                   
                    <asp:Button runat="server" ID="AustractBtn" CssClass="aa_btn_red" OnClientClick="return showAustractFileConfirm()" Text="Create IFTI-OUT File" OnClick="AustractBtn_Click" />
                </td>
            </tr>
        </table> <!-- MAIN TABLE ENDS HERE -->
    </div>


        <div class="container_one wiz_bg_EFEFEF">
            <table class="tbl_width_1200" >
                <tr>
                    <td style="width:280px; vertical-align:top;">
                        <table class="tbl_width_280 brdr_shadow">
                            <tr><td class="left_nav_headings">Banks</td></tr>
                            <tr><td class="left_nav_contents" id="BOCClick">Bank of Ceylon</td></tr>
                            <tr><td class="left_nav_contents" id="CommBankClick">Commercial Bank</td></tr>
                            <tr><td class="left_nav_contents" id="HNBClick">Hatton National Bank</td></tr>
                            <tr><td class="left_nav_contents" id="NSBClick">National Savings Bank</td></tr>
                            <tr><td class="left_nav_contents" id="PBClick">Peoples Bank</td></tr>
                            <tr><td class="left_nav_contents" id="SPClick">Sampath Bank</td></tr>
                            <tr><td class="left_nav_contents" id="SBClick">Seylan Bank</td></tr>
                        </table>
                    </td>
                    <td style="width:20px;">&nbsp;</td>
                    <td style="width:900px; vertical-align:top;" class="background_FFFFFF">


<!-- ***** BANK 01 - BANK OF CEYLON ********************************************* -->

             <div class="container_one no_padding_top_btm brdr_shadow" id="div_bank01">
                <table class="tbl_width_900">
                <tr><td class="bank_headings">Bank of Ceylon (BOC)</td></tr>
                    <tr><td class="bank_messages">
                        <table class="tbl_width_860">
                            <tr style="height:20px;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                        <td class="bank_info_file"><asp:Label ID="BOCLastFileUp" runat="server" Text="Label"></asp:Label></td>
                                        <td class="align__right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td style="width:140px;" class="bank_info_01">total transactions</td>
                                            <td style="width:80px;" class="bank_info_02"><asp:Label ID="BOCTransTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total AUD</td>
                                            <td style="width:140px;" class="bank_info_04"><asp:Label ID="BOCAUDTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total LKR</td>
                                            <td style="width:140px;" class="bank_info_05"><asp:Label ID="BOCLKRTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:180px;" class="align__right"><button class="btn_red" id="btn_CreateBank01File">Create Upload File</button></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>

                        </td></tr>
                <tr><td>
                <table class="tbl_width_860">

                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank01table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td></tr></table>
        </div>


<!-- ***** BANK 02 - COMMERCIAL BANK ********************************************* -->

             <div class="container_one no_padding_top_btm brdr_shadow" id="div_bank02">
                <table class="tbl_width_900">
                <tr><td class="bank_headings">Commercial Bank</td></tr>
                    <tr><td class="bank_messages">
                        <table class="tbl_width_860">
                            <tr style="height:20px;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                        <td class="bank_info_file"><asp:Label ID="COMLastFileUp" runat="server" Text="Label"></asp:Label></td>
                                        <td class="align__right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td style="width:140px;" class="bank_info_01">total transactions</td>
                                            <td style="width:80px;" class="bank_info_02"><asp:Label ID="COMTransTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total AUD</td>
                                            <td style="width:140px;" class="bank_info_04"><asp:Label ID="COMAUDTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total LKR</td>
                                            <td style="width:140px;" class="bank_info_05"><asp:Label ID="COMLKRTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:180px;" class="align__right"><button class="btn_red" id="btn_CreateBank02File">Create Upload File</button></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>


                        </td></tr>
                <tr><td>
                <table class="tbl_width_860">

                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank02table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td></tr></table>
        </div>



<!-- ***** BANK 03 - HATTON NATIONAL BANK ********************************************* -->

             <div class="container_one no_padding_top_btm brdr_shadow" id="div_bank03">
                <table class="tbl_width_900">
                <tr><td class="bank_headings">Hatton National Bank (HNB)</td></tr>
                    <tr><td class="bank_messages">
                        <table class="tbl_width_860">
                            <tr style="height:20px;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                        <td class="bank_info_file"><asp:Label ID="HNBLastFileUp" runat="server" Text="Label"></asp:Label></td>
                                        <td class="align__right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td style="width:140px;" class="bank_info_01">total transactions</td>
                                            <td style="width:80px;" class="bank_info_02"><asp:Label ID="HNBTransTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total AUD</td>
                                            <td style="width:140px;" class="bank_info_04"><asp:Label ID="HNBAUDTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total LKR</td>
                                            <td style="width:140px;" class="bank_info_05"><asp:Label ID="HNBLKRTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:180px;" class="align__right"><button class="btn_red" id="btn_CreateBank03File">Create Upload File</button></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>


                        </td></tr>
                <tr><td>
                <table class="tbl_width_860">

                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank03table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td></tr></table>
        </div>


<!-- ***** BANK 04 - NATIONAL SAVINGS BANK ********************************************* -->

             <div class="container_one no_padding_top_btm brdr_shadow" id="div_bank04">
                <table class="tbl_width_900">
                <tr><td class="bank_headings">National Savings Bank (NSB)</td></tr>
                    <tr><td class="bank_messages">
                        <table class="tbl_width_860">
                            <tr style="height:20px;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                        <td class="bank_info_file"><asp:Label ID="NSBLastFileUp" runat="server" Text="Label"></asp:Label></td>
                                        <td class="align__right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td style="width:140px;" class="bank_info_01">total transactions</td>
                                            <td style="width:80px;" class="bank_info_02"><asp:Label ID="NSBTransTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total AUD</td>
                                            <td style="width:140px;" class="bank_info_04"><asp:Label ID="NSBAUDTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total LKR</td>
                                            <td style="width:140px;" class="bank_info_05"><asp:Label ID="NSBLKRTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:180px;" class="align__right"><button class="btn_red" id="btn_CreateBank04File">Create Upload File</button></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>


                        </td></tr>
                <tr><td>
                <table class="tbl_width_860">

                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank04table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td></tr></table>
        </div>


<!-- ***** BANK 05 - PEOPLES BANK ********************************************* -->

             <div class="container_one no_padding_top_btm brdr_shadow" id="div_bank05">
                <table class="tbl_width_900">
                <tr><td class="bank_headings">People&#39;s Bank</td></tr>
                    <tr><td class="bank_messages">
                        <table class="tbl_width_860">
                            <tr style="height:20px;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                        <td class="bank_info_file"><asp:Label ID="PBLastFileUp" runat="server" Text="Label"></asp:Label></td>
                                        <td class="align__right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td style="width:140px;" class="bank_info_01">total transactions</td>
                                            <td style="width:80px;" class="bank_info_02"><asp:Label ID="PBTransTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total AUD</td>
                                            <td style="width:140px;" class="bank_info_04"><asp:Label ID="PBAUDTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total LKR</td>
                                            <td style="width:140px;" class="bank_info_05"><asp:Label ID="PBLKRTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:180px;" class="align__right">
                                                <input type="button" value="Create Upload File" class="btn_red" id="btn_CreateBank05File" onclick="LoadPPBFile()" />
                                                </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>


                        </td></tr>
                <tr><td>
                <table class="tbl_width_860">

                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank05table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td></tr></table>
        </div>


<!-- ***** BANK 06 - SAMPATH BANK ********************************************* -->

             <div class="container_one no_padding_top_btm brdr_shadow" id="div_bank06">
                <table class="tbl_width_900">
                <tr><td class="bank_headings">Sampath Bank</td></tr>
                    <tr><td class="bank_messages">
                        <table class="tbl_width_860">
                            <tr style="height:20px;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                        <td class="bank_info_file"><asp:Label ID="SPLastFileUp" runat="server" Text="Label"></asp:Label></td>
                                        <td class="align__right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td style="width:140px;" class="bank_info_01">total transactions</td>
                                            <td style="width:80px;" class="bank_info_02"><asp:Label ID="SPTransTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total AUD</td>
                                            <td style="width:140px;" class="bank_info_04"><asp:Label ID="SPAUDTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total LKR</td>
                                            <td style="width:140px;" class="bank_info_05"><asp:Label ID="SPLKRTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:180px;" class="align__right"><button class="btn_red" id="btn_CreateBank06File">Create Upload File</button></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>


                        </td></tr>
                <tr><td>
                <table class="tbl_width_860">

                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank06table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td></tr></table>
        </div>


<!-- ***** BANK 07 - SEYLAN BANK ********************************************* -->

             <div class="container_one no_padding_top_btm brdr_shadow" id="div_bank07">
                <table class="tbl_width_900">
                <tr><td class="bank_headings">Seylan Bank</td></tr>
                    <tr><td class="bank_messages">
                        <table class="tbl_width_860">
                            <tr style="height:20px;">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                        <td class="bank_info_file"><asp:Label ID="SBLastFileUp" runat="server" Text="Label"></asp:Label></td>
                                        <td class="align__right">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td style="width:140px;" class="bank_info_01">total transactions</td>
                                            <td style="width:80px;" class="bank_info_02"><asp:Label ID="SBTransTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total AUD</td>
                                            <td style="width:140px;" class="bank_info_04"><asp:Label ID="SBAUDTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:80px;" class="bank_info_03">total LKR</td>
                                            <td style="width:140px;" class="bank_info_05"><asp:Label ID="SBLKRTotal" runat="server" Text="Label"></asp:Label></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:180px;" class="align__right"><button class="btn_red" id="btn_CreateBank07File">Create Upload File</button></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>


                        </td></tr>
                <tr><td>
                <table class="tbl_width_860">

                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank07table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td></tr></table>
        </div>



<!-- ***** BANK 02 - COMMERCIAL BANK xxxx********************************************* -->

        <!--<div class="container_one no_padding_top_btm" id="div_bank02"> 
                <table class="tbl_width_900">
                    <tr><td class="bank_headings">Commercial Bank</td></tr>
                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank02table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>

        </div>-->



                    </td>
                </tr>
            </table>
        </div>








        <!--<div class="container_one"> <!-- BANK 03 - Hatton National Bank 
        <table class="tbl_width_1200 background_FFFFFF brdr_all_EBEBEB">  
            <tr><td>
                <table class="tbl_width_1160">
                    <tr><td style="height:20px;">&nbsp;</td></tr> 
                    <tr><td style="height:20px;" class="section_headings_fnt">Hatton National Bank</td></tr>
                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank03table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td>
            </tr>
        </table>
        </div>-->


        <!--<div class="container_one"> <!-- BANK 04 - National Savings Bank 
        <table class="tbl_width_1200 background_FFFFFF brdr_all_EBEBEB">
            <tr><td>
                <table class="tbl_width_1160">
                    <tr><td style="height:20px;">&nbsp;</td></tr> 
                    <tr><td style="height:20px;" class="section_headings_fnt">National Savings Bank</td></tr>
                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank04table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td>
            </tr>
        </table>
        </div>-->

        <!--<div class="container_one"> <!-- BANK 05 - Peoples Bank
        <table class="tbl_width_1200 background_FFFFFF brdr_all_EBEBEB">
            <tr><td>
                <table class="tbl_width_1160">
                    <tr><td style="height:20px;">&nbsp;</td></tr> 
                    <tr><td style="height:20px;" class="section_headings_fnt">Peoples Bank</td></tr>
                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank05table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td>
            </tr>
        </table>
        </div>-->


        <!--<div class="container_one"> <!-- BANK 06 - Sampath Bank
        <table class="tbl_width_1200 background_FFFFFF brdr_all_EBEBEB"> 
            <tr><td>
                <table class="tbl_width_1160">
                    <tr><td style="height:20px;">&nbsp;</td></tr> 
                    <tr><td style="height:20px;" class="section_headings_fnt">Sampath Bank</td></tr>
                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank06table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td>
            </tr>
        </table>
        </div>-->

        
        <!--<div class="container_one"> <!-- BANK 07 - Seylan Bank 
        <table class="tbl_width_1200 background_FFFFFF brdr_all_EBEBEB"> 
            <tr><td>
                <table class="tbl_width_1160">
                    <tr><td style="height:20px;">&nbsp;</td></tr> 
                    <tr><td style="height:20px;" class="section_headings_fnt">Seylan Bank</td></tr>
                    <tr><td style="height:20px;">&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" id="bank07table">
                                <thead>
                                    <tr>
                                        <td>TRANS ID</td>
                                        <td>DATE</td>
                                        <td>CUST ID</td>
                                        <td>CUST LAST NAME</td>
                                        <td>BENE NAME</td>
                                        <td>BANK NAME</td>
                                        <td>BRANCH</td>
                                        <td>ACC NO</td>
                                        <td>Rs AMOUNT</td>
                                    </tr>
                                </thead>
                            </table>

                        </td>
                    </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                </table>
                </td>
            </tr>
        </table>
        </div>-->


        

    </form>
</asp:Content>