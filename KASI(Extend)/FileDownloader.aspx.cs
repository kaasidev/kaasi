﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_
{
    public partial class FileDownloader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Stream stream = null;
            string fileName = Request.QueryString["file"];
            string receiver = Request.QueryString["RC"];
            string url = string.Empty;
            string extn = "";
            if (String.IsNullOrEmpty(receiver))
            {
                string[] fileArr = fileName.Split('.');
                if(fileArr.Length==1)
                {
                    url = Server.MapPath("/Files") + "\\" + fileName + ".xml";
                    extn = ".xml";
                }
                else
                {
                    url = Server.MapPath("/Files") + "\\" + fileName;
                    extn = "."+ fileArr[1];
                }
                
            }
            else
            {
                string[] fileArr = fileName.Split('.');
                if (fileArr.Length == 1)
                {
                    url = Server.MapPath("/BankFileCreation") + "\\" + fileName + ".xml";
                    extn = "";
                }
                else
                {
                    url = Server.MapPath("/BankFileCreation") + "\\" + fileName;
                    extn = "";
                }
               // url = Server.MapPath("/BankFileCreation") + "\\" + fileName;
            }
            
           
            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                var fileReq = System.Net.HttpWebRequest.Create(url);

                //Create a response for this request
                var fileResp = fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + ""+ extn + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
        }
    }
}