﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace KASI_Extend_
{
    public partial class Settings2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            loadMasterCompanyDetails();
            //loadCurrencies();
            buildAgentListforAAC();
            buildSettingsforAdmin();
        }

        private void loadMasterCompanyDetails()
        {
            MasterCompany MC = new MasterCompany();
            MastCompName.Text = MC.MasterCompanyName().ToUpper();
            //spn_MasterCompName.InnerText = MC.MasterCompanyName().ToUpper();
            //spn_MasterCompAddress.InnerText = MC.MasterCompanyFullAddress();
            MastCompAddressLine1.Text = MC.getMasterCompanyAddressLine1();
            if (MC.getMasterCompanyAddressLine2() == "")
            {
                MastCompAddressLine2.Visible = false;
            }
            else
            {
                MastCompAddressLine2.Text = MC.getMasterCompanyAddressLine2() + ",";
            }
            MastCompSubStaPos.Text = MC.getMasterCompanyAddressBundle();
            MastCompTele.Text = MC.getMasterCompanyTelephone();
            MastCompEmail.Text = MC.getMasterCompanyEmail();
            MastCompABN.Text = MC.getMasterCompanyABN();
            MastCompACN.Text = MC.getMasterCompanyACN();
            MastCompFax.Text = MC.getMasterCompanyFax();
           
        }

        private void loadCurrencies()
        {
           MasterCompany MC = new MasterCompany();
           Dictionary<String, String> Currencies = MC.getMasterCurrencies();

           foreach(var item in Currencies)
           {
               HtmlGenericControl s = new HtmlGenericControl("span");
               s.Attributes.Add("class", "settings_007_tags");
               s.InnerHtml = item.Key + " " + item.Value;
              // div_showcurrencies.Controls.Add(s);
               //div_showcurrencies.Controls.Add(new LiteralControl("&nbsp;"));
           }
        }

        private void buildAgentListforAAC()
       {
           SqlConnection conn = new SqlConnection();
           conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

           String strQuery = "SELECT AgentID, AgentName FROM dbo.Agents WHERE Status = 'Active'";
           String FirstBeneficiary = String.Empty;


           using (SqlCommand cmd = new SqlCommand())
           {
               cmd.CommandText = strQuery;
               cmd.Connection = conn;
               conn.Open();

               using (SqlDataReader sdr = cmd.ExecuteReader())
               {
                   DDL_AAC_AgentList.Items.Add(new ListItem("--SELECT--", "NOSELECT"));
                   while (sdr.Read())
                   {
                       var items = new ListItem
                       {
                           Text = sdr["AgentName"].ToString(),
                           Value = sdr["AgentID"].ToString()
                       };

                       DDL_AAC_AgentList.Items.Add(items);

                   }

               }

           }
       }

        private void buildSettingsforAdmin()
        {
            classes.Settings sett = new classes.Settings();

            SET_MAUDLimit.InnerText = sett.getSystemMonthlyAUDLimit();
            SET_YAUDLimit.InnerText = sett.getSystemYearlyAUDLimit();
            SET_MNbTransLimit.InnerText = sett.getSystemMonthlyTransLimit().ToString();
            SET_YNbTransLimit.InnerText = sett.getSystemYearlyTransLimit().ToString();
            SET_MDollarVariation.InnerText = sett.getSystemMonthlyVariationLimit().ToString();
            SET_NbMonthVariation.InnerText = sett.getSystemMonthsOfVariation().ToString();
            SET_IFITLocation.InnerText = sett.getSystemIFTIFileLocation();
            SET_SMRLocation.InnerText = sett.getSystemSMRFileLocation();
            SET_TTRLocation.InnerText = sett.getSystemTTRFileLocation();
            SET_NbMonthsBeforeCheck.InnerText = sett.getSystemNbMonthBeforeCheck();

        }
    }
}