﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentControl/Agent.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="KASI_Extend.AgentControl.Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">

                    <div class="header">
                        <h4 class="title">User Management</h4>
                    </div>
                    <div class="content">

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">

                    <div class="header">
                        <h4 class="title">SMTP Details</h4>
                    </div>
                    <div class="content">

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">

                    <div class="header">
                        <h4 class="title">Company Details</h4>
                    </div>
                    <div class="content">

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
