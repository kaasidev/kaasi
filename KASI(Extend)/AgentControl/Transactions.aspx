﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentControl/Agent.Master" AutoEventWireup="true" CodeBehind="Transactions.aspx.cs" Inherits="KASI_Extend.AgentControl.Transactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/datatables.min.js"></script>
    <link rel="stylesheet" href="../css/datatables.min.css" type="text/css">

    <style>
    table.dataTable#AllTransactions {
        font-family: 'Gotham Narrow Book';
        font-size: 13px !important;
        color: #444;
        line-height: 14px;
        border-bottom: 1px solid #EFEFEF;
        }

    table.dataTable#AllTransactions tbody th {
            font-family: 'Gotham Narrow Medium';
            font-size: 13px;
            padding: 10px 5px 10px 5px;
            color: #fff !important;
            border-bottom: 5px solid #2A7AAF;
            font-weight: normal !important;
            line-height: 14px !important;
        }

    table.dataTable#AllTransactions tbody td {
            border-bottom: 1px solid #EFEFEF;
            font-family: 'Gotham Narrow Book';
            padding-top: 10px;
            padding-bottom: 10px;
            font-size: 13px;
        }

    table.dataTable#AllTransactions tr.odd { background-color: #DAE5F4 !important; }
table.dataTable#AllTransactions tr.even { background-color: #B8D1F3; }

    table.dataTable#AllTransactions thead th, table.dataTable#AllTransactions thead td {
        font-family: 'Gotham Narrow Medium' !important;
        font-weight: normal !important;
        padding: 10px 0px 5px 5px;
        border-bottom: 5px solid #2A7AAF;
        font-size: 11px !important;
        color: #FFFFFF !important;
        background-color: #048ABB;
        line-height: 14px;
    }
    </style>


    <script type="text/javascript">

        $(document).ready(function () {

            var AllTransDataTable = $('#AllTransactions').DataTable({
                columns: [

                    { 'data': 'KAASIID' },
                    { 'data': 'BeneficiaryID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'AltTransID' },
                    { 'data': 'BeneficiaryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'Status' },

                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: '../DataHandlers/AGENTAllInwardDataHandler.ashx',
                "order": [[0, "desc"]],
            });
        })

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


  
<div style="padding-top:50px; padding-bottom:25px; background-color:#EFEFEF;">
    <div class="container container-table">
        <!-- <h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2">
          
        </h2>
        <h3 class="mbr-section-subtitle mbr-fonts-style align-center pb-5 mbr-light display-5">
                 
        </h3> -->
      


            <div class="col-md-12">
                <div class="header">
                        <h4 class="title">Transactions</h4>
                    </div>
                    <div class="content">
                        <table cellpadding="0" cellspacing="0" border="0" id="AllTransactions">
                                            <thead>
                                                <tr>
                                                    <td>KAASI REF</td>
                                                    <td>BeneID</td>
                                                    <td>DATE</td>
                                                    <td>AGENT</td>
                                                    <td>AGENT REF</td>
                                                    <td>BENE NAME</td>
                                                    <td>BANK</td>
                                                    <td>DOLLAR AMOUNT</td>
                                                    <td>STATUS</td>
                                                </tr>
                                            </thead>
                                        </table>

                    </div>

            </div>




    </div>

</div> 




    
</asp:Content>
