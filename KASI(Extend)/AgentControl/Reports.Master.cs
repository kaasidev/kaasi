﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.classes;

namespace KASI_Extend_.AgentControl
{
    public partial class Reports : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String FullName = String.Empty;
            String CompaneName = String.Empty;
            String AID = Session["LoggedUserID"].ToString();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT FullName FROM dbo.Logins WHERE LoginID = " + Session["LoggedUserID"].ToString();
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        FullName = sdr["FullName"].ToString();
                    }
                }
                conn.Close();
            }

            using (SqlCommand cmd2 = new SqlCommand())
            {
                cmd2.CommandText = "SELECT AgentName FROM dbo.Agents WHERE AgentID = " + Session["AgentID"].ToString();
                cmd2.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd2.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        CompaneName = sdr["AgentName"].ToString();
                    }
                }
                conn.Close();
            }

            AgentNameTitle.Text = CompaneName + " (" + FullName + ")";
        }
    }
}