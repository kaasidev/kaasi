﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kapruka.Repository;
using Kapruka.Enterprise;

namespace KASI_Extend.AgentControl
{
    public partial class Agent : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            int theLID = Int32.Parse(Session["LoggedUserID"].ToString());
            int theAID = Int32.Parse(Session["AgentID"].ToString());

            LoginService LGServ = new LoginService(new UnitOfWorks(new KaprukaEntities()));
            var LGDetails = LGServ.GetAll(x => x.LoginID == theLID, null, "").SingleOrDefault();

            //AgentService AGTServ = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            //var AGTDetails = AGTServ.GetAll(x => x.AgentID == theAID, null, "").SingleOrDefault();

            IncomingAgentService IAServ = new IncomingAgentService(new UnitOfWorks(new KaprukaEntities()));
            var IADetails = IAServ.GetAll(x => x.AgentID == theAID, null, "").SingleOrDefault();

            P_NameOfLoggedUser.InnerText = "Welcome to KAASI, " + LGDetails.FullName + " (" + IADetails.AgentName + ")" ;
        }
    }
}