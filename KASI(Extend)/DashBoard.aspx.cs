﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Kapruka.Service;
using Kapruka.Domain;
using System.Globalization;
using System.IO;

namespace KASI_Extend_
{
    public partial class DashBoard : System.Web.UI.Page
    {

        Customer customer = new Customer();
        Beneficiary beneficiary = new Beneficiary();
        Transactions transaction = new Transactions();
        HtmlGenericControl li;
        List<CustomerDocs> customerAllDocs;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["TypeOfLogin"] == null)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseTab", "closeWindow();", true);
            }

            String requestedCID = Request.QueryString["custid"].ToString();
            String CID = requestedCID;
            String FirstBeneficiary = String.Empty;



            loadAllCustomerDetails(CID);
            loadCustomerBeneficiaries(CID);
            loadAllCustomerDetailsForEdit(CID);
            txt_hidden_CustomerID.Text = CID;
            txt_hidden_CustomerID.Style.Add("display", "none");
            txt_hidden_FirstBeneficiaryID.Style.Add("display", "none");
            txt_hidden_AccountID.Style.Add("display", "none");
            txt_hidden_Rate.Style.Add("display", "none");
            hidden_KYCRunBefore.Style.Add("display", "none");
            hidden_SpendingPower.Style.Add("display", "none");
            txt_hidden_agentID.Style.Add("display", "none");
            //KYC_PleaseWait.Style.Add("display", "none");
            dsh_KYC_Fail.Style.Add("display", "none");
            dsh_KYC_Success.Style.Add("display", "none");
            dsh_KYC_NotRun.Style.Add("display", "none");
            dsh_KYC_DateLabel.Style.Add("display", "none");
            dsh_KYC_DateValue.Style.Add("display", "none");
            dsh_KYC_ByLabel.Style.Add("display", "none");
            dsh_KYC_ByValue.Style.Add("display", "none");
            dsh_KYC_InfoLabel.Style.Add("display", "none");
            dsh_KYC_InfoValue.Style.Add("display", "none");
            btn_OverrideKYC.Style.Add("display", "none");
            lbl_KYC_ManualKYCComments.Style.Add("display", "none");
            TR_ConfirmRUNKYC.Style.Add("display", "none");
            TR_PrimaryDocExp.Style.Add("display", "none");
            TR_NoPrimaryDocs.Style.Add("display", "none");
            TR_PrimaryDocExpSoon.Style.Add("display", "none");



            dsh_KYC_RefLabel.Style.Add("display", "none");
            dsh_KYC_RefValue.Style.Add("display", "none");

            TR_DisplayKYCErrors.Style.Add("display", "none");

            displayKYCInfo(CID);



            ErrorMessageArea.Visible = false;
            //MessageArea.Style.Add("display", "none");
            //loadAllBankAccounts();
            // WarningMessages(CID);
            checkDocuments(CID);
            checkIfKYCHasBeenPerformed(CID);
        }

        public void displayKYCInfo(String CID)
        {
            string[] formats = {"dd/MM/yyyy", "dd-MMM-yyyy", "yyyy-MM-dd",
                   "dd-MM-yyyy", "M/d/yyyy", "dd MMM yyyy"};
            var KYCCode = customer.getCustomerKYCResultCode(CID);

            if (KYCCode == "0")
            {
                dsh_KYC_Success.Style.Add("display", "block");
                dsh_KYC_DateLabel.Style.Add("display", "block");
                dsh_KYC_DateValue.Style.Add("display", "block");
                dsh_KYC_ByLabel.Style.Add("display", "block");
                dsh_KYC_ByValue.Style.Add("display", "block");
                dsh_KYC_InfoLabel.Style.Add("display", "block");
                dsh_KYC_InfoValue.Style.Add("display", "block");
                dsh_KYC_RefLabel.Style.Add("display", "block");
                dsh_KYC_RefValue.Style.Add("display", "block");
                dsh_KYC_Fail.Style.Add("display", "none");
                dsh_KYC_NotRun.Style.Add("display", "none");
                String CheckType = customer.getCustomerKYCCheckType(CID);

                dsh_KYC_RefValue.InnerText = customer.getCustomerKYCResultTransactionID(CID);
                dsh_KYC_DateValue.InnerText = DateTime.ParseExact(customer.getCustomerKYCCheckDateDate(CID), formats, CultureInfo.InvariantCulture, DateTimeStyles.None).ToString("dd/MM/yyyy");
                dsh_KYC_ByValue.InnerText = customer.getCustomerKYCRunBy(CID);

                if (CheckType == "MANUAL")
                {
                    lbl_KYC_ManualKYCComments.Style.Add("display", "block");
                    lbl_KYC_ManualKYCComments.Text = customer.getCustomerKYCNotes(CID);
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID) + " - " + CheckType;
                }
                else
                {
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID);
                }
                //showKYCResult(CID);
            }
            else if (String.IsNullOrEmpty(KYCCode))
            {
                dsh_KYC_NotRun.Style.Add("display", "block");
            }
            else
            {
                dsh_KYC_DateLabel.Style.Add("display", "block");
                dsh_KYC_DateValue.Style.Add("display", "block");
                dsh_KYC_ByLabel.Style.Add("display", "block");
                dsh_KYC_InfoLabel.Style.Add("display", "block");
                dsh_KYC_InfoValue.Style.Add("display", "block");
                dsh_KYC_RefLabel.Style.Add("display", "block");
                dsh_KYC_RefValue.Style.Add("display", "block");
                String CheckType = customer.getCustomerKYCCheckType(CID);

                dsh_KYC_RefValue.InnerText = customer.getCustomerKYCResultTransactionID(CID);
                dsh_KYC_DateValue.InnerText = customer.getCustomerKYCCheckDateDate(CID);
                dsh_KYC_Fail.Style.Add("display", "block");
                btn_OverrideKYC.Style.Add("display", "block");

                if (CheckType == "MANUAL")
                {
                    lbl_KYC_ManualKYCComments.Style.Add("display", "block");
                    lbl_KYC_ManualKYCComments.Text = customer.getCustomerKYCNotes(CID);
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID) + " - " + CheckType;
                }
                else
                {
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID);
                }
            }
        }

        public void checkDocuments(String CID)
        {

            classes.Customer CUST = new classes.Customer();
            String DocStatus = CUST.getCustomerDocumentCount(CID);

            var cService = new CustomerHandlerService();
            customerAllDocs = cService.GetCustomerAllDocs(Int32.Parse(CID));
            ValidOldestDoc stringDocument = null;
            var valiedOldestDoc = GetOldestDoc(customerAllDocs);
            if (valiedOldestDoc.Count > 0)
                stringDocument = valiedOldestDoc[0];

            if (DocStatus == "NODOCS")
            {
                //ErrorMessageArea.Visible = true;
                //errormessagedisplay.InnerText = "WARNING: This Customer has no identification documents.";
                TR_NoPrimaryDocs.Style.Add("display", "block");
            }
            else
            {
                var DocExp = CUST.checkIfCustomerDocumentsHaveAllExpired(CID);

                if (DocExp == true)
                {
                    CUST.ExpireKYC(CID);
                    //ErrorMessageArea.Visible = true;
                    //errormessagedisplay.InnerText = "WARNING: One or more documents have expired.";
                    TR_PrimaryDocExp.Style.Add("display", "block");
                }

                if (CUST.isDocumentExpiringin30Days(stringDocument) == true)
                {
                    DateTime today = DateTime.Now;
                    DateTime docExp = stringDocument.ExpiryDate;

                    double datediff = (docExp - today).TotalDays;
                    TR_PrimaryDocExpSoon.Style.Add("display", "block");
                    SPN_DocExpDate.InnerText = stringDocument.ExpiryDate.ToString().Substring(0, 10) + " (" + Convert.ToInt32(datediff) + " days)";
                }
            }
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            // First check if customer has valid documents

            var customerId = Convert.ToInt32(Request.QueryString["custid"].ToString());
            //KYC_PleaseWait.Style.Add("display", "block");
            if (hidden_KYCRunBefore.Text == "true")
            {
                TR_ConfirmRUNKYC.Style.Add("display", "block");
                TR_RUNKYC.Style.Add("display", "none");
            }
            else
            {
                CreateKYCEInfo(customerId);
                Response.Redirect("DashboardW.aspx?custid=" + customerId);
            }

           
        }

        private void showKYCResult(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT TransactionID, KYCResult, KYCCheckDate FROM dbo.Customers WHERE CustomerID = " + CID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        //Td1.InnerText = "KYC RESULT: " + sdr["KYCResult"].ToString();
                        //Td2.InnerText = "IdentityMind Check is completed for this customer.";
                        //Td3.InnerText = "Date: " + sdr["KYCCheckDate"].ToString();
                        //Td4.InnerText = "Transaction ID: " + sdr["TransactionID"].ToString();
                    }

                }
                conn.Close();
            }
        }

        private void CreateKYCEInfo(int customerId, bool recheckKyc = false)
        {

            var cService = new CustomerHandlerService();
            var customerForKyc = cService.GetCustomerById(customerId);
            Customer CUST = new Customer();
            var resulCall = new ResultKycCall();
            if (customerForKyc.IsIndividual)
            {
                if (string.IsNullOrEmpty(customerForKyc.FirstName))
                {
                    //ErrorMessageArea.Visible = true;
                    //errormessagedisplay.InnerText = "Info: First name is mandatory";
                    TR_DisplayKYCErrors.Style.Add("display", "inline");
                    LBL_KYCErrors.Text = "Warning: Customer First Name is Mandatory.";

                    return;

                }

                var resultCallMe = new CustomerValidDoc();

                if (customerAllDocs.Count() == 0)  // check doc valid
                {
                    resultCallMe.Message = "No valid documents found";

                }
                else
                {

                    if (CheckDocSValidDate(customerAllDocs) == false)  // check doc valid
                    {
                        resultCallMe.Message = "All Documents Have Expired. Please upload new documents to proceed.";

                    }
                    else
                    {
                        resultCallMe.CanConnectApi = true;
                    }
                }
                if (recheckKyc == false)
                {
                    resultCallMe = ValidateCustomerDocsAndKcyStatus(customerAllDocs, customerId);

                    if (resultCallMe.Status == false)
                    {
                        //ErrorMessageArea.Visible = true;
                        //errormessagedisplay.InnerText = "Info: " + resultCallMe.Message;
                        TR_DisplayKYCErrors.Style.Add("display", "inline");
                        LBL_KYCErrors.Text = resultCallMe.Message;

                        return;
                    }
                    else
                        if (resultCallMe.RecordExist)
                        {
                            //ErrorMessageArea.Visible = true;
                            //errormessagedisplay.InnerText = "Info: " + resultCallMe.Message;
                            TR_DisplayKYCErrors.Style.Add("display", "inline");
                            LBL_KYCErrors.Text = resultCallMe.Message;
                            displayKYCInfo(customerId.ToString());

                            return;
                        }
                }

                if (resultCallMe.CanConnectApi)
                    resulCall = CallCustomerKcy(customerId, cService, customerForKyc, resulCall, customerAllDocs);



            }

            else
            {
                if (string.IsNullOrEmpty(customerForKyc.BusinessName))
                {
                    //ErrorMessageArea.Visible = true;
                    //errormessagedisplay.InnerText = "Info: Business legal name is mandatory";
                    TR_DisplayKYCErrors.Style.Add("display", "inline");
                    LBL_KYCErrors.Text = "Warning: Business Legal Name is Mandatory";

                    return;

                }
                var customerAllDocs = cService.GetCustomerAllDocs(customerId);
                var resultCallMe = ValidateCustomerDocsAndKcyStatus(customerAllDocs, customerId);

                if (resultCallMe.Status == false)
                {
                    //ErrorMessageArea.Visible = true;
                    //errormessagedisplay.InnerText = "Info: " + resultCallMe.Message;
                    TR_DisplayKYCErrors.Style.Add("display", "inline");
                    LBL_KYCErrors.Text = resultCallMe.Message;

                    return;
                }
                else
                    if (resultCallMe.RecordExist)
                    {
                        //ErrorMessageArea.Visible = true;
                        //errormessagedisplay.InnerText = "Info: " + resultCallMe.Message;
                        TR_DisplayKYCErrors.Style.Add("display", "inline");
                        LBL_KYCErrors.Text = resultCallMe.Message;
                        displayKYCInfo(customerId.ToString());
                        return;
                    }
                    else
                        resulCall = CallMerchant(customerId, cService, customerForKyc, resulCall);
            }

            if (resulCall.Status)
            {
                //ErrorMessageArea.Visible = true;
                //errormessagedisplay.InnerText = "Info: This Customer has successfully created in KYCE. " + resulCall.Message;
                TR_DisplayKYCErrors.Style.Add("display", "inline");
                dsh_KYC_NotRun.Style.Add("display", "none");
                dsh_KYC_Fail.Style.Add("display", "none");
                dsh_KYC_Success.Style.Add("display", "block");
                LBL_KYCErrors.Text = "This customer has been successfully KYCed.";
                String splitResult = String.Empty;

                displayKYCInfo(customerId.ToString());
                setRiskLevel(CUST.getCustomerRiskLevel(customerId.ToString()));
            }
            else
            {
                //ErrorMessageArea.Visible = true;
                //errormessagedisplay.InnerText = "Warning: Failed " + resulCall.Message;
                TR_DisplayKYCErrors.Style.Add("display", "inline");
                LBL_KYCErrors.Text = "KYC Failed: " + resulCall.Message;

                displayKYCInfo(customerId.ToString());
            }


        }

        private static ResultKycCall CallMerchant(int customerId, CustomerHandlerService cService,
            CustomerForKyc customerForKyc, ResultKycCall resulCall)
        {
            var merchant = new MerchantKyced();
            merchant.profile = "FLEXEWALLET";
            if (customerForKyc.TradingName != null)
                merchant.dba = customerForKyc.TradingName;
            else
                merchant.dba = "";

            merchant.amn = customerForKyc.BusinessName;

            merchant.website = "";
            if (customerForKyc.City != null)
                merchant.ac = customerForKyc.City;
            else
                merchant.ac = "";
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    merchant.aco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    merchant.aco = "AU";
            }
            else
                merchant.aco = "AU";
            if (customerForKyc.FirstName != null)
                merchant.afn = customerForKyc.FirstName;
            else
                merchant.afn = "";
            if (customerForKyc.LastName != null)
                merchant.aln = customerForKyc.LastName;
            else
                merchant.aln = "";
            if (customerForKyc.Mobile != null)
                merchant.aph = customerForKyc.Mobile;
            else
                merchant.aph = "";
            if (customerForKyc.Postcode != null)
                merchant.az = customerForKyc.Postcode;
            else
                merchant.az = "";

            if (customerForKyc.State != null)
                merchant.@as = customerForKyc.State;
            else
                merchant.@as = "";
            if (customerForKyc.StreetName != null)
                merchant.asn = customerForKyc.StreetName;
            else
                merchant.asn = "";
            if (customerForKyc.ABN != null)
                merchant.ataxId = customerForKyc.ABN;
            else
                merchant.ataxId = "";

            merchant.bin = "";
            if (customerForKyc.RiskLevel != null)
                merchant.bin = customerForKyc.RiskLevel;

            var runBy = "";
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            resulCall = cService.CreateKycedMerchant(merchant, customerId, customerForKyc.RecordID, runBy);
            return resulCall;
        }

        private CustomerValidDoc ValidateCustomerDocsAndKcyStatus(List<CustomerDocs> docs, int customerId)
        {
            var obj = new CustomerValidDoc();

            if (docs.Count() == 0)  // check doc valid
            {
                obj.Message = "No valid documents found";
                return obj;
            }

            if (CheckDocSValidDate(docs) == false)  // check doc valid
            {
                obj.Message = "All Documents Have Expired. Please upload new documents to proceed.";
                return obj;
            }

            var serVic = new CustomerHandlerService();
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        var doNumber = item.DocumentNumber;
                        var checkDocNumberList = serVic.GetCustomerAllDocsByDocumentNumber(doNumber);
                        if (checkDocNumberList.Count() > 0)
                        {
                            var validCustomerTran = IsCustomerHasValidTransactionId(checkDocNumberList);
                            if (validCustomerTran.status)
                            {
                                if (validCustomerTran.Customer != null)
                                {
                                    var updateKyc = new CustomerOrMerchantResponseKyced();
                                    updateKyc.agentTransactionId = validCustomerTran.Customer.agentTransactionID;
                                    updateKyc.resultCode = validCustomerTran.Customer.resultCode;
                                    updateKyc.resultDescription = validCustomerTran.Customer.resultDescription;
                                    updateKyc.transactionId = validCustomerTran.Customer.TransactionID;
                                    //  var cusInfo = serVic.GetCustomerById(validCustomerTran.customerId);
                                    var runBy = "";
                                    if (HttpContext.Current.Session["usernameLogged"] != null)
                                        runBy = HttpContext.Current.Session["usernameLogged"].ToString();
                                    var UpdateNeedCustomer = serVic.GetCustomerById(customerId);
                                    serVic.UpdateCustomerByKASIWEBAPIData(updateKyc, customerId,
                                        UpdateNeedCustomer.RecordID, runBy, false);
                                    var updateInfoCustomer = serVic.GetCustomerById(UpdateNeedCustomer.CustomerID);
                                    obj.Status = true;
                                    obj.RecordExist = true;
                                    obj.Message = " Result " + updateInfoCustomer.KCYResult + ". KYC checked date " + updateInfoCustomer.KYCCheckDate.Date.ToShortDateString();
                                    return obj;
                                }
                            }
                            else
                            {
                                obj.Status = true;
                                obj.CanConnectApi = true;
                                return obj;
                            }

                        }
                        else
                        {
                            obj.Status = true;
                            obj.CanConnectApi = true;
                            return obj;
                        }
                    }

                }

            }

            return obj;

        }

        private ResultKycCall CallCustomerKcy(int customerId, CustomerHandlerService cService,
            CustomerForKyc customerForKyc,
            ResultKycCall resulCall, List<CustomerDocs> docsCustomer)
        {

            var cusKyc = new CustomerKyced();
            cusKyc.profile = "FLEXEWALLET";
            if (customerForKyc.Title != null)
                cusKyc.title = customerForKyc.Title;
            else
                cusKyc.title = null;
            if (customerForKyc.FirstName != null)
                cusKyc.bfn = customerForKyc.FirstName;
            else
                cusKyc.bfn = null;

            if (customerForKyc.LastName != null)
                cusKyc.bln = customerForKyc.LastName;
            else
                cusKyc.bln = null;
            if (customerForKyc.DOB != null)
            {

                cusKyc.dob = ConvertDateFormat(customerForKyc.DOB);
            }
            else
                cusKyc.dob = null;
            if (customerForKyc.FirstName != null)
                cusKyc.man = customerForKyc.CustomerID.ToString();
            else
                cusKyc.man = null;
            if (customerForKyc.City != null)
                cusKyc.bc = customerForKyc.City;
            else
                cusKyc.bc = null;
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    cusKyc.bco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    cusKyc.bco = "AU";
            }
            else
                cusKyc.bco = "AU";

            if (customerForKyc.State != null)
                cusKyc.bs = customerForKyc.State;
            else
                cusKyc.bs = null;
            if (customerForKyc.StreetName != null)
                cusKyc.bsn = customerForKyc.StreetName;
            else
                cusKyc.bsn = null;
            if (customerForKyc.Postcode != null)
                cusKyc.bz = customerForKyc.Postcode;
            else
                cusKyc.bz = null;
            if (customerForKyc.Mobile != null)
                cusKyc.pw = customerForKyc.Mobile;
            else
                cusKyc.pw = null;
            if (customerForKyc.EmailAddress != null)
                cusKyc.tea = customerForKyc.EmailAddress;
            else
                cusKyc.tea = null;
            string bas64Convert = null;
            //var filePath = Utilities.ReadConfigValue("customerDocsPath",
            //    "C:\\WebProjects\\KASI-Latest\\KASI(Extend)\\KASI(Extend)\\CustomerDocuments\\{0}");
            var pathFormat = "~/CustomerDocuments/{0}";
            pathFormat = string.Format(pathFormat, customerId);
            var filePath = Server.MapPath(pathFormat);
           
            cusKyc.faceImage = null;
            //if (Directory.Exists(filePath))
            //{
            //    var files = Directory.GetFiles(filePath);

            //    var valiedOldestDoc = GetOldestDoc(docsCustomer);
            //    var stringDocumentGuid = "";
            //    if (valiedOldestDoc.Count > 0)
            //        stringDocumentGuid = valiedOldestDoc[0].DocumentGUID;
              
            //    if (files != null)
            //    {
            //        if (!string.IsNullOrEmpty(stringDocumentGuid))
            //        {
            //            var selectedFile = (from sef in files where Path.GetFileName(sef) == stringDocumentGuid select sef).ToList();
            //            if (selectedFile.Count > 0)
            //            {
            //                var firstFileObj = selectedFile[0];
            //                var decFilePath = firstFileObj;// DecryptFile(firstFileObj, customerId.ToString());
            //                bas64Convert = Utilities.ConvertFileToBase64(decFilePath);
            //                //if (File.Exists(decFilePath))
            //                //  File.Delete(decFilePath);

            //            }
            //        }
            //    }
            //}
            //if (bas64Convert != null)
            //    cusKyc.faceImage = bas64Convert;

            // if (customerForKyc.FaceImage != null)
            // {
            //  var image = Utilities.ByteArrayToImage(customerForKyc.FaceImage);
            //  if (image != null)
            // cusKyc.faceImage = Utilities.ImageToBase64(image, System.Drawing.Imaging.ImageFormat.Png);


            // }
            //  cusKyc.faceImage = null;
            cusKyc.documentImageFront = null;
            //cusKyc.documentImangeBack = "";
            cusKyc.securityNumber = null;
            cusKyc.ip = null;
            //if (customerdoc != null)
            //{
            //    if (customerdoc.hasDriverLicence)
            //    {
            //        //var imageDriver = Utilities.ByteArrayToImage(customerdoc.DocumentDriverImage1);
            //        cusKyc.documentImageFront = customerdoc.DocumentDriverImage1;
            //    }
            //}
            var runBy = "";
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            resulCall = cService.CreateKycedCustomer(cusKyc, customerId, customerForKyc.RecordID, runBy);
            return resulCall;
        }

        private string DecryptFile(string filePath, string cusID)
        {
            string fPath = "";

            var tempPath = HttpContext.Current.Server.MapPath("~/temp/");
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);
            var decFiLeName = "customerFile_" + cusID + Path.GetExtension(filePath);
            fPath = tempPath + decFiLeName;
            Utilities.Decrypt(filePath, fPath);

            return fPath;
        }

        private List<ValidOldestDoc> GetOldestDoc(List<CustomerDocs> objList)
        {
            var listOlderLIst = new List<ValidOldestDoc>();
            foreach (var item in objList)
            {
                var obj = new ValidOldestDoc();
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    obj.ExpiryDate = expiryDate;
                    obj.RecordID = item.RecordID;
                    obj.CustomerID = item.CustomerID;
                    obj.DocumentGUID = item.DocumentGUID;
                    obj.DocumentName = item.DocumentName;
                    obj.TypeOfDocument = item.TypeOfDocument;
                    obj.TypeOfDocumentID = item.TypeOfDocumentID;
                    obj.DocumentNumber = item.DocumentNumber;
                    obj.hasPassport = item.hasPassport;
                    listOlderLIst.Add(obj);
                }
            }

            objList = objList.OrderByDescending(x => x.ExpiryDate).ToList();
            return listOlderLIst;
        }



        private string ConvertDateFormat(string date)
        {
            var dateS = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
            return dateS;
        }

        public void loadAllCustomerDetailsForEdit(String CID)
        {
            //lastname.Value = customer.getCustomerLastName(CID);
            //firstnames.Value = customer.getCustomerFirstName(CID);
            //addressline1.Value = customer.getCustomerAddressLine1(CID);
            //addressline2.Value = customer.getCustomerAddressLine2(CID);
            //suburb.Value = customer.getCustomerSuburb(CID);
            //state.Value = customer.getCustomerState(CID);
            //postcode.Value = customer.getCustomerPostcode(CID);
            //phonehome.Value = customer.getCustomerHomeNumber(CID);
            //phonework.Value = customer.getCustomerWorkNumber(CID);
            //phonemobile.Value = customer.getCustomerMobile(CID);
            //dob.Value = customer.getCustomerDOB(CID);
            //email1.Value = customer.getCustomerEmailAddress1(CID);
            //email2.Value = customer.getCustomerEmailAddress2(CID);
            //edit_notes.Value = customer.getCustomerNotes(CID);
            //edit_password.Value = customer.getCustomerPassword(CID);
            String hasNotes = customer.getCustomerNotes(CID);
            trnotes.Style.Add("display", "none");
            if (!String.IsNullOrEmpty(hasNotes))
            {
                shownotes.InnerHtml = hasNotes;
                trnotes.Style.Add("display", "block");
            }

            hidden_SpendingPower.Text = customer.getCustomerSpendingPower(CID);



            //if (edit_notes.Value == "")
            //{
            //    trnotes.Visible = false;
            //}

        }

        public void loadAllCustomerDetails(String CID)
        {
            String TypeOfCustomer = customer.getCustomerTypeNamed(CID);

            if (TypeOfCustomer == "INDIVIDUAL")
            {
                div_cust_full_name.InnerText = CID + " - " + customer.getCustomerFullName(CID);
            }
            else
            {
                div_cust_full_name.InnerText = CID + " - " + customer.getCustomerBusinessName(CID);
            }

            div_CustomerDOB.InnerText = customer.getCustomerDOB(CID);
            div_CustomerFullAddress.InnerText = customer.getCustomerFullAddress(CID);
            div_CustomerEmail1.InnerText = customer.getCustomerEmailAddress1(CID);
            div_CustomerEmail2.InnerText = customer.getCustomerEmailAddress2(CID);
            div_CustomerMobile.InnerText = customer.getCustomerMobile(CID);
            div_CustomerHomeNumber.InnerText = customer.getCustomerHomeNumber(CID);
            div_CustomerWorkNumber.InnerText = customer.getCustomerWorkNumber(CID);
            String AccBalance = customer.getCustomerAccountBalance(CID);
            div_customertype.InnerText = customer.getCustomerTypeNamed(CID);
            DIV_BankAccountBalance.InnerText = String.Format("{0:C2}", float.Parse(AccBalance));
            String DollarValue = customer.getCustomerAccountBalance(CID);
            Decimal DDollarValue = decimal.Parse(Regex.Replace(DollarValue, @"[^\d.]", ""));
            hidden_txt_accountbalance.Text = DDollarValue.ToString();
            hidden_txt_accountbalance.Style.Add("display", "none");
            hidden_risklevel.Value = customer.getCustomerRiskLevel(CID);
            setRiskLevel(hidden_risklevel.Value);

            string CustomerHasPic = customer.getCustomerPicPath(CID);
            txt_hidden_agentID.Text = customer.getOwnerAgentIDFromCustomerID(CID);
            div_owningAgent.InnerText = customer.getCustomerOwningAgentName(CID);

            showStatsAbove(CID);

            SPN_ID.InnerText = CID;
            SPN_NAME.InnerText = customer.getCustomerFullName(CID);
            hidden_hasKYC.Value = customer.getCustomerHasKYC(CID);
            hidden_CurrentSentAmount.Value = customer.getYearlyRemittancetotal(CID);
            hidden_KYCExpiryDays.Value = customer.KYCExpiryDays(CID);
            hidden_NbTrans.Value = customer.getSumYearlyRemit(CID);

            if (customer.confirmCustomerKYCHasBeenDone(CID) == true)
            {
                hidden_KYCRunBefore.Text = "true";
            }
            else
            {
                hidden_KYCRunBefore.Text = "false";
            }


            String CompanyVerifiedStatus = customer.getCompanyVerifiedStatus(CID);
            if (CompanyVerifiedStatus == "false")
            {
                IMG_CompanyVerified.Attributes["src"] = "images/medal-gry.png";
                IMG_CompanyVerified.Attributes["title"] = "Non KASI Verified Customer";
            }
            else
            {
                IMG_CompanyVerified.Attributes["src"] = "images/medal.png";
                chk_CompanyVerified.Checked = true;
                IMG_CompanyVerified.Attributes["title"] = "Customer Is A Verified Customer";
            }

            String HasKYC = customer.getCustomerHasKYC(CID);
            if (HasKYC == "false")
            {
                IMD_HasKYC.Attributes["src"] = "images/group-gry.png";
                IMD_HasKYC.Attributes["title"] = "KYC Information Pending";
            }
            else
            {
                IMD_HasKYC.Attributes["src"] = "images/group.png";
                IMD_HasKYC.Attributes["title"] = "KYC Information Available";
            }

            String HasHundredPoints = customer.getCustomerIDPointsTotal(CID);
            Int32 HHP = 0;
            if (HasHundredPoints != "")
            {
                if (HasHundredPoints != "0")
                {
                    HHP = Int32.Parse(HasHundredPoints);
                    hidden_hasID.Value = "Y";
                }
                else
                {
                    HHP = 0;
                    hidden_hasID.Value = "N";
                }
            }
            else
            {
                HHP = 0;
                hidden_hasID.Value = "N";
            }


            if (HHP >= 100)
            {
                Image1.Attributes["src"] = "images/id-card.png";
                Image1.Attributes["title"] = "Customer Identifcation Completed";
                IMD_HasKYC.Style["cursor"] = "hand";
            }
            else
            {
                Image1.Attributes["title"] = "Customer Identifcation Pending";
                IMD_HasKYC.Style["cursor"] = "pointer";
            }







            if (CustomerHasPic.Equals("NA"))
            {
                string customerGender = customer.getCustomerGender(CID);
                if (customerGender.ToString().Equals("Male"))
                {
                    img_CustomerPic.Attributes.Add("src", "images/avatar-male.jpg");
                }
                else if (customerGender.ToString().Equals("Female"))
                {
                    img_CustomerPic.Attributes.Add("src", "images/avatar-female.jpg");
                }
                else
                {
                    img_CustomerPic.Attributes.Add("src", "images/avatar-unknown.jpg");
                }
            }
            else
            {
                img_CustomerPic.Attributes.Add("src", CustomerHasPic);
            }
        }

        public void showStatsAbove(String CID)
        {
            Agent AGT = new Agent();
            // Get the Agent's Limits to ensure customer has not gone past those limits
            String owningAgent = customer.getOwnerAgentIDFromCustomerID(CID);
            float AgentMAUDLimit = float.Parse(AGT.getAgentMonthlyAUDLimit(owningAgent));
            float AgentYAUDLimit = float.Parse(AGT.getAgentYearlyAUDLimit(owningAgent));
            float AgentMNbLimit = float.Parse(AGT.getAgentMonthlyTransLimit(owningAgent));
            float AgentYNbLimit = float.Parse(AGT.getAgentYearlyTransLimit(owningAgent));

            String MonthlyRemit = customer.getMonthlyRemittanceTotal(CID);
            float MRemit = float.Parse(MonthlyRemit);
            DIV_MonthlyRemittanceTotal.InnerText = String.Format("{0:C2}", MRemit);
            DIV_MonthlyRemittanceTotal.Attributes.Add("title", String.Format("{0:C2}", MRemit) + " / " + String.Format("{0:C2}", AgentMAUDLimit));

            if (MRemit >= AgentMAUDLimit)
            {
                DIV_MonthlyRemittanceTotal.Style.Add("color", "red");

            }

            String YearlyRemit = customer.getYearlyRemittancetotal(CID);
            float YRemit = float.Parse(YearlyRemit);
            DIV_YearlyRemittanceTotal.InnerText = String.Format("{0:C2}", YRemit);
            DIV_YearlyRemittanceTotal.Attributes.Add("title", String.Format("{0:C2}", YRemit) + " / " + String.Format("{0:C2}", AgentYAUDLimit));
            DIV_Totals.Attributes.Add("title", "Monthly Limit: " + AgentMNbLimit + " / Yearly Limit: " + AgentYNbLimit);

            if (YRemit >= AgentYAUDLimit)
            {
                DIV_YearlyRemittanceTotal.Style.Add("color", "red");
            }

            DIV_Totals.InnerText = customer.getSumMonthlyRemit(CID) + "/" + customer.getSumYearlyRemit(CID);
        }

        public void setRiskLevel(String RiskLevel)
        {
            if (RiskLevel == "4")
            {

                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_3");
                td_risk4.Attributes.Add("class", "risk_4");
            }
            else if (RiskLevel == "3")
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_3");
                td_risk4.Attributes.Add("class", "risk_neutral");
            }
            else if (RiskLevel == "2")
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_neutral");
                td_risk4.Attributes.Add("class", "risk_neutral");
            }
            else if (RiskLevel == "1")
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_neutral");
                td_risk3.Attributes.Add("class", "risk_neutral");
                td_risk4.Attributes.Add("class", "risk_neutral");
            }
            else
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_3");
                td_risk4.Attributes.Add("class", "risk_4");
            }
        }

        public void loadCustomerBeneficiaries(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT * FROM dbo.Beneficiaries WHERE CustomerID = " + CID + " AND Active = 'Y'";
            String FirstBeneficiary = String.Empty;


            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    ddl_List_Beneficiaries.Items.Add(new ListItem("--SELECT--", "NOSELECT"));
                    while (sdr.Read())
                    {
                        var items = new ListItem
                        {
                            Text = sdr["BeneficiaryName"].ToString(),
                            Value = sdr["BeneficiaryID"].ToString()
                        };

                        ddl_List_Beneficiaries.Items.Add(items);
                        //ddl_List_Beneficiaries.Attributes["onchange"] = "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")";
                        /*li = new HtmlGenericControl("li");
                        list_beneficiaries.Controls.Add(li);

                        HtmlGenericControl divAnchor = new HtmlGenericControl("div");
                        divAnchor.Attributes.Add("class", "beneficiary_bullet");

                        HtmlGenericControl imgAnchor = new HtmlGenericControl("img");
                        imgAnchor.Attributes.Add("src", "images/ben-avatar.png");
                        imgAnchor.Attributes.Add("width", "16px");

                        divAnchor.Controls.Add(imgAnchor);
                        li.Controls.Add(divAnchor);

                        HtmlGenericControl divAnchor2 = new HtmlGenericControl("div");
                        divAnchor2.InnerText = sdr["BeneficiaryName"].ToString();*/


                        //divAnchor2.Attributes.Add("onclick", "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")");

                        // li.Controls.Add(divAnchor2);


                        /*li.Attributes.Add("onclick", "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")");
                        li.Attributes.Add("class", "beneficiary_link");
                        li.InnerText = sdr["BeneficiaryName"].ToString();

                        list_beneficiaries.Controls.Add(li);
                        */

                    }

                }

            }
        }

        public void WarningMessages(String CID)
        {
            // Check if customer has KYC After Yearly $1000 limit

            float YearRemit = float.Parse(customer.getYearlyRemittancetotal(CID).ToString());
            String HasKYC = customer.getCustomerHasKYC(CID);

            if (YearRemit >= 1000)
            {
                if (HasKYC == "false")
                {
                    //TD_WarningMessage.InnerText = "Customer has reached transaction limit without KYC. Please complete KYC or this transaction will have to be approved by either an Admin or Compliance Team.";
                }
            }

        }

        private bool CheckDocSValidDate(List<CustomerDocs> docs)
        {
            var validDocFound = false;
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        validDocFound = true;
                        return validDocFound;
                    }
                }
            }

            return validDocFound;
        }

        private CustomerValidYear IsCustomerHasKycMoreThanOneYear(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {

                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (customerKcCheck.KYCCheckDate != null || customerKcCheck.KYCCheckDate != DateTime.MinValue)
                {
                    var KYCCheckDateyear = customerKcCheck.KYCCheckDate.AddYears(1);
                    var currentDate = DateTime.Now;
                    if (currentDate > KYCCheckDateyear)
                    {
                        validDocFound.status = true;
                        validDocFound.customerId = customerforKyc.CustomerID;

                    }

                }
            }

            return validDocFound;
        }
        protected class CustomerValidYear
        {
            public int customerId { get; set; }
            public bool status { get; set; }
            public CustomerForKyc Customer { get; set; }
        }
        private CustomerValidYear IsCustomerHasKycLessThanOneYear(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {
                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (customerKcCheck.KYCCheckDate != null)
                {
                    var KYCCheckDateyear = customerKcCheck.KYCCheckDate.AddYears(1);
                    var currentDate = DateTime.Now;
                    if (currentDate < KYCCheckDateyear)
                    {
                        validDocFound.status = true;
                        validDocFound.customerId = customerforKyc.CustomerID;
                        validDocFound.Customer = customerKcCheck;
                        return validDocFound;
                    }

                }
            }

            return validDocFound;
        }

        private CustomerValidYear IsCustomerHasValidTransactionId(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {
                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (!string.IsNullOrEmpty(customerKcCheck.TransactionID))
                {

                    validDocFound.status = true;
                    validDocFound.customerId = customerforKyc.CustomerID;
                    validDocFound.Customer = customerKcCheck;

                    return validDocFound;
                }
            }

            return validDocFound;
        }

        private CustomerValidDoc ValidateKYCRecords(List<CustomerDocs> docs, int customerId)
        {
            var obj = new CustomerValidDoc();

            if (docs.Count() == 0)  // check doc valid
            {
                obj.Message = "No valid documents found";
                return obj;
            }

            if (CheckDocSValidDate(docs) == false)  // check doc valid
            {
                obj.Message = "All documents have expired. Please upload new primary document.";
                return obj;
            }

            var serVic = new CustomerHandlerService();
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        var doNumber = item.DocumentNumber;
                        var checkDocNumberList = serVic.GetCustomerAllDocsByDocumentNumber(doNumber);
                        if (checkDocNumberList.Count() > 0)
                        {
                            var oneYearLessCheck = IsCustomerHasKycLessThanOneYear(checkDocNumberList);
                            if (oneYearLessCheck.status)
                            {
                                var cusInfo = serVic.GetCustomerById(oneYearLessCheck.customerId);
                                obj.Status = true;
                                obj.RecordExist = true;
                                obj.Message = " Result " + cusInfo.KCYResult + ". KYC checked date " + cusInfo.KYCCheckDate.Date.ToShortDateString();
                                return obj;
                            }
                        }
                    }
                }
            }

            return obj;
        }


        protected void checkIfKYCHasBeenPerformed(String CID)
        {
            Customer cust = new Customer();
            String hasKYC = cust.getCustomerHasKYC(CID);
            var customerId = Convert.ToInt32(CID);
            var customerAllDocs = new CustomerHandlerService().GetCustomerAllDocs(customerId);
            var validateResultKYC = ValidateKYCRecords(customerAllDocs, customerId);

        }

        protected void confirm_YES_RunKYC_Click(object sender, EventArgs e)
        {
            var customerId = Convert.ToInt32(Request.QueryString["custid"].ToString());
            CreateKYCEInfo(customerId, true);
            TR_ConfirmRUNKYC.Style.Add("display", "none");
            TR_RUNKYC.Style.Add("display", "block");
            Response.Redirect("DashboardW.aspx?custid=" + customerId);
        }

        protected void confirm_NO_RunKYC_Click(object sender, EventArgs e)
        {
            TR_ConfirmRUNKYC.Style.Add("display", "none");
            TR_RUNKYC.Style.Add("display", "block");
        }
    }
}