﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using KASI_Extend_.classes;

namespace KASI_Extend_
{
    public partial class AdminDash : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            MasterCompany MC = new MasterCompany();
            Agent AGT = new Agent();
            loadLast20Notifications();
            TotalDepositedThisMonth();
            DIV_TotalProfit.Attributes["data-to"] = MC.getMasterCompanyProfitForThisMonth();
            DIV_AgentCommission.Attributes["data-to"] = AGT.getAllAgentsMoneyForThisMonth();
        }

        protected void loadLast20Notifications()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AG.AgentName, AT.* FROM dbo.AuditTrail AT INNER JOIN dbo.Agents AG ON AT.AgentID = AG.AgentID";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            output = output + "<li><p><a href='#'>" + sdr["AgentName"].ToString() + " " + sdr["Comments"].ToString() + "</a><span class='timeline-icon'><i class='fa fa-comment'></i></span><span class='timeline-date'>" + sdr["AuditDateTime"].ToString() + "</span></p></li>";
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
            }
           

            admin_last20_notifications.InnerHtml = output;
        }

        protected void TotalDepositedThisMonth()
        {
            DepositsCredits DC = new DepositsCredits();

            DIV_TotalDepositedThisMonth.Attributes["data-to"] = String.Format("{0:N2}", DC.getTotalDeposited().ToString());
        }
    }
}