﻿using Kapruka.Enterprise;
using Kapruka.PublicModels;
using Kapruka.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace KASI_Extend_
{
    public partial class ReportsW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports\\CustomerDepositsReport.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports\\TransactionsByDateRange.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports\\AgentStatsSummary.aspx");
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports\\AgentAssignedRatesReport.aspx");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports\\ListOfTransactionsViewer.aspx");
        }

        protected void btnCreateAUSFile_Click(object sender, EventArgs e)
        {
            if (Session["AgentID"] != null)
            {
                int agentId = int.Parse(Session["AgentID"].ToString());
                TransactionService transactionService = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
                var transactionList = transactionService.GetAll(x => x.InsertedIntoAustracFile == "N" && x.AgentID == agentId, null, "").ToList();
                //  var smrList = transactionService.GetAll(x => x.isSuspicious == "N" && x.InsertedIntoAustracFile=="N", null, "").ToList();
                // var ttrList = transactionService.GetAll(x => x.isThreshold == "N" && x.InsertedIntoAustracFile == "N", null, "").ToList();

                string todaysdate = DateTime.Now.ToString("yyyyMMdd");
                SettingsService SetServ = new SettingsService(new UnitOfWorks(new KaprukaEntities()));
                AffiliateAustracService AASServ = new AffiliateAustracService(new UnitOfWorks(new KaprukaEntities()));

                DateTime today = DateTime.Now.Date;
                String justthedate = today.ToString("dd/MM/yyyy");
                var theNextNumber = "";
                var theAgentID = Int32.Parse(Session["AgentID"].ToString());

                var TheSequenceNumber = AASServ.GetAll(x => x.CreatedDate == today && x.AgentID == theAgentID, null, "").SingleOrDefault();

                if (TheSequenceNumber != null)
                {
                    theNextNumber = (Int32.Parse(TheSequenceNumber.FileSequenceNumber) + 1).ToString().PadLeft(2, '0');
                    //TheSequenceNumber.CreatedDate = today;
                    TheSequenceNumber.FileSequenceNumber = theNextNumber;
                    AASServ.Update(TheSequenceNumber);
                }
                else
                {
                    var NewSequence = new AffiliateAustracRecord();
                    theNextNumber = "01";
                    NewSequence.CreatedDate = today;
                    NewSequence.AgentID = theAgentID;
                    NewSequence.FileSequenceNumber = theNextNumber;
                    AASServ.Add(NewSequence);
                }

                String fileName = "IFTI-E" + todaysdate + theNextNumber;


                //string fileName = "IFTI-DRA" + DateTime.Now.ToString("yyyyMMdd") + "01";
                // IftieList iftModel = new IftieList();
                SmrList smrModel = new SmrList();
                TtrfbsList ttrModel = new TtrfbsList();
                IFTIModel.IftidraList iftModel = new IFTIModel.IftidraList();
                iftModel.VersionMajor = "1";
                iftModel.VersionMinor = "1";
                //iftModel= GenerateIFTReport(transactionList, fileName);
                iftModel = GenerateIFTReportML(transactionList, fileName, agentId);
                // smrModel = GenerateSMRReport(smrList, fileName);
                //  ttrModel = GenerateTTRReport(ttrList,fileName);

                //IftieList hd = new PublicModels.IftieList();

                //hd.FileName = "te";
                //hd.Ns1 = "dsds";
                //hd.Ns2 = "ds s ";
                ////lst.Add(hd);
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                string filePath = Server.MapPath("/Files") + "\\" + fileName + ".xml";
                using (MemoryStream ms = new MemoryStream())
                {
                    using (var writer = XmlWriter.Create(ms, settings))
                    {
                        var serializer = new XmlSerializer(typeof(IFTIModel.IftidraList));
                        serializer.Serialize(writer, iftModel);
                    }

                    ms.Position = 0;
                    XDocument doc = XDocument.Load(new XmlTextReader(ms));
                    doc.Root.Add(new XAttribute("versionMinor", "1"));
                    doc.Root.Add(new XAttribute("versionMajor", "1"));
                    doc.Save(filePath);

                   // DownloadTheFile(filePath, fileName);



                    //ms.Close();
                    //Response.Clear();
                    //Response.ContentType = "application/force-download";
                    //Response.AddHeader("content-disposition", "attachment;    filename=" + fileName + ".xml");
                    //byte[] bytesInStream = ms.ToArray(); // simpler way of converting to array
                    //Response.BinaryWrite(bytesInStream);
                    //Response.End();
                }
                //  SerializeObject(iftModel, filePath, "\\xml" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xml");
                UpdateTable(fileName + ".xml", agentId);
                loadMask.Style["display"] = "none";
                loadImg.Style["display"] = "none";
                string redirect = "<script>window.open('/FileDownloader.aspx?file=" + fileName + "','_blank');</script>";
                Response.Write(redirect);
            }
            else
            {
                Response.Redirect("/Login.aspx");
            }
        }

        public void UpdateTable(string fileName, int agentId)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString);

            string sql = "update Transactions set InsertedIntoAustracFileName='" + fileName + "',InsertedIntoAustracFileDateTime=getdate(),InsertedIntoAustracFile='Y' where InsertedIntoAustracFile='N' and AgentID=" + agentId;

            using (SqlCommand cmd = new SqlCommand(sql))
            {
                cmd.Connection = conn;
                conn.Open();

                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        public void DownloadTheFile(string url, string fileName)
        {
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                var fileReq = System.Net.HttpWebRequest.Create(url);

                //Create a response for this request
                var fileResp = fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xml\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
        }
        private Kapruka.PublicModels.IFTIModel.IftidraList GenerateIFTReportML(List<Transaction> transactionList, string fileName, int agent_Id)
        {
            CustomerService custService = new CustomerService(new UnitOfWorks(new KaprukaEntities()));
            BeneficiaryService beneService = new BeneficiaryService(new UnitOfWorks(new KaprukaEntities()));
            CustomerDocumentService custDocSer = new CustomerDocumentService(new UnitOfWorks(new KaprukaEntities()));
            beneficiaryPaymentMethodService payMethodSer = new beneficiaryPaymentMethodService(new UnitOfWorks(new KaprukaEntities()));
            AgentService agentSer = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            CountryService countrySer = new CountryService(new UnitOfWorks(new KaprukaEntities()));
            AgentAssignedCountryService agencyAssSer = new AgentAssignedCountryService(new UnitOfWorks(new KaprukaEntities()));
            Kapruka.PublicModels.IFTIModel.IftidraList model = new Kapruka.PublicModels.IFTIModel.IftidraList();

            var AgentDetails = agentSer.GetAll(x => x.AgentID == agent_Id, null, "").SingleOrDefault();

            model.FileName = fileName + ".xml";
            model.xmlns = null;

            model.renumber = AgentDetails.AustracReNumber;

            model.Iftidra = new List<IFTIModel.Iftidra>();
            if (transactionList != null)
            {
                model.ReportCount = transactionList.Count();
            }
            else
            {
                model.ReportCount = 0;
            }

            // int kl = 0;
            foreach (Transaction item in transactionList)
            {
                //   if(kl==0)
                //   {
                Customer customerList = new Customer();
                Beneficiary beneficiaryList = new Beneficiary();
                CustomerDocument custDoc = new CustomerDocument();
                BeneficiaryPaymentMethod payMenthod = new BeneficiaryPaymentMethod();
                Kapruka.Repository.Agent agent = new Kapruka.Repository.Agent();

                customerList = custService.GetAll(x => x.CustomerID == item.CustomerID, null, "").SingleOrDefault();
                beneficiaryList = beneService.GetAll(x => x.BeneficiaryID == item.BeneficiaryID, null, "").SingleOrDefault();
                var custDocList = custDocSer.GetAll(x => x.CustomerID == customerList.CustomerID, null, "").ToList();
                if (custDocList.Count == 0)
                {
                    custDoc = null;
                }
                else
                {
                    custDoc = custDocList[0];
                }

                string benId = beneficiaryList.BeneficiaryID.Value.ToString();
                var payMenthodList = payMethodSer.GetAll(x => x.BeneficiaryID == benId, null, "").ToList();
                if (payMenthodList.Count == 0)
                {
                    payMenthod = null;
                }
                else
                {
                    payMenthod = payMenthodList[0];
                }
                int agentId = customerList.AgentID.Value;
                agent = agentSer.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();


                IFTIModel.Iftidra dra = new IFTIModel.Iftidra();
                dra.Id = "ID_" + item.TransactionID.ToString();
                dra.Header = new IFTIModel.Header();
                dra.Header.Id = "ID_" + item.TransactionID.ToString() + "-0";
                dra.Header.InterceptFlag = "N";
                dra.Header.TxnRefNo = item.TransactionID.ToString();

                dra.Transaction = new IFTIModel.Transaction();
                dra.Transaction.Id = "ID_" + item.TransactionID.ToString() + "-1";
                dra.Transaction.TxnDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");
                dra.Transaction.CurrencyAmount = new IFTIModel.CurrencyAmount();
                dra.Transaction.CurrencyAmount.Id = "ID_" + item.TransactionID.ToString() + "-11";
                dra.Transaction.CurrencyAmount.Amount = item.DollarAmount.Value.ToString("N2");
                dra.Transaction.CurrencyAmount.Currency = "AUD";

                dra.Transaction.direction = "O";
                dra.Transaction.TfrType = new IFTIModel.TfrType();
                dra.Transaction.TfrType.Type = "M";
                dra.Transaction.TfrType.Id = "ID_" + item.TransactionID.ToString() + "-12";
                dra.Transaction.ValueDate = item.CreatedDateTime.Value.ToString("yyyy-MM-dd");

                dra.Transferor = new IFTIModel.Transferor();//customer
                dra.Transferor.Id = "ID_" + item.TransactionID.ToString() + "-2";
                dra.Transferor.FullName = customerList.FullName;
                dra.Transferor.MainAddress = new IFTIModel.MainAddress();
                dra.Transferor.MainAddress.Addr = customerList.AddressLine1 + " " + customerList.AddressLine2;
                dra.Transferor.MainAddress.Country = customerList.CountryOfBirth;
                dra.Transferor.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-21";
                dra.Transferor.MainAddress.Postcode = customerList.Postcode;
                dra.Transferor.MainAddress.State = customerList.State;
                dra.Transferor.MainAddress.Suburb = customerList.Suburb;
                dra.Transferor.MainAddress.Country = customerList.Country;

                dra.Transferor.Phone = customerList.Mobile;
                dra.Transferor.Email = customerList.EmailAddress;
                dra.Transferor.CustNo = customerList.CustomerID.Value.ToString();
                if (String.IsNullOrEmpty(customerList.DOB))
                {
                    dra.Transferor.Dob = " ";
                }
                else
                {
                    dra.Transferor.Dob = ChangeDOB(customerList.DOB.ToString());
                }
                
                dra.Transferor.Identification = new IFTIModel.Identification();
                dra.Transferor.Identification.Id = "ID_" + item.TransactionID.ToString() + "-22";
                if (custDoc != null)
                {
                    String TypeOfDoc = String.Empty;
                    if (custDoc.TypeOfDocument == "Driver's Licence")
                    {
                        TypeOfDoc = "D";
                    }
                    else if (custDoc.TypeOfDocument == "Passport")
                    {
                        TypeOfDoc = "P";
                    }
                    dra.Transferor.Identification.Issuer = custDoc.IssuingAuthority;
                    dra.Transferor.Identification.Number = custDoc.DocumentNumber;
                    dra.Transferor.Identification.Type = TypeOfDoc;
                }
                else
                {
                    dra.Transferor.Identification.Issuer = "NO INFO";
                    dra.Transferor.Identification.Number = "NO INFO";
                    dra.Transferor.Identification.Type = "D";
                }

                dra.OrderingInstn = new IFTIModel.OrderingInstn();//Agent details
                dra.OrderingInstn.Id = "ID_" + item.TransactionID.ToString() + "-3";
                dra.OrderingInstn.Branch = new IFTIModel.Branch();
                dra.OrderingInstn.Branch.Id = "ID_" + item.TransactionID.ToString() + "-31";
                dra.OrderingInstn.Branch.MainAddress = new IFTIModel.MainAddress();
                dra.OrderingInstn.Branch.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-32";
                dra.OrderingInstn.Branch.MainAddress.Addr = agent.AgentAddress1;
                AgentAssignedCountry assC = new AgentAssignedCountry();
                assC = agencyAssSer.GetAll(x => x.AgentID == agent.AgentID, null, "").FirstOrDefault();
                Country agentCountry = countrySer.GetAll(x => x.CountryID == assC.CountryID.Value, null, "").SingleOrDefault();
                dra.OrderingInstn.Branch.MainAddress.Country = agentCountry.CountryName;
                dra.OrderingInstn.Branch.MainAddress.Suburb = agent.AgentCity;
                dra.OrderingInstn.Branch.MainAddress.State = agent.AgentState;
                dra.OrderingInstn.Branch.MainAddress.Postcode = agent.AgentPostcode;
                dra.OrderingInstn.Branch.FullName = agent.AgentName;

                dra.OrderingInstn.ForeignBased = "N";//no idea

                dra.InitiatingInstn = new IFTIModel.InitiatingInstn();//no idea
                dra.InitiatingInstn.Id = "ID_" + item.TransactionID.ToString() + "-4";
                dra.InitiatingInstn.SameAsOrderingInstn = "Y";

                dra.SendingInstn = new IFTIModel.SendingInstn();//no idea
                dra.SendingInstn.Id = "ID_" + item.TransactionID.ToString() + "-5";
                //dra.SendingInstn.SameAsOrderingInstn = "Y";

                dra.ReceivingInstn = new IFTIModel.ReceivingInstn();//no idea
                dra.ReceivingInstn.Id = "ID_" + item.TransactionID.ToString() + "-6";
                dra.ReceivingInstn.FullName = "PB";
                dra.ReceivingInstn.MainAddress = new IFTIModel.MainAddress();
                dra.ReceivingInstn.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-61";
                dra.ReceivingInstn.MainAddress.Addr = "No.75, Sir Chittampalam A. Gardiner Mawatha";
                dra.ReceivingInstn.MainAddress.Suburb = "Colombo-02";
                dra.ReceivingInstn.MainAddress.State = "Western";
                dra.ReceivingInstn.MainAddress.Country = "Sri Lanka";

                dra.BeneficiaryInstn = new IFTIModel.BeneficiaryInstn();
                dra.BeneficiaryInstn.Id = "ID_" + item.TransactionID.ToString() + "-7";
                dra.BeneficiaryInstn.SameAsReceivingInstn = "Y";

                dra.Transferee = new IFTIModel.Transferee();//beneficiary
                dra.Transferee.Id = "ID_" + item.TransactionID.ToString() + "-9";
                dra.Transferee.FullName = beneficiaryList.BeneficiaryName;
                dra.Transferee.MainAddress = new IFTIModel.MainAddress();
                dra.Transferee.MainAddress.Addr = beneficiaryList.AddressLine1 + " " + beneficiaryList.AddressLine2;
                dra.Transferee.MainAddress.Country = beneficiaryList.CountryOfBirth;
                dra.Transferee.MainAddress.Id = "ID_" + item.TransactionID.ToString() + "-91";
                dra.Transferee.MainAddress.Postcode = beneficiaryList.Postcode;
                dra.Transferee.MainAddress.State = beneficiaryList.State;
                dra.Transferee.MainAddress.Suburb = beneficiaryList.Suburb;
                dra.Transferee.MainAddress.Country = beneficiaryList.Country;

                dra.Transferee.Phone = beneficiaryList.Mobile;
                dra.Transferee.Account = new IFTIModel.Account();
                dra.Transferee.Account.Id = "ID_" + item.TransactionID.ToString() + "-92";
                if (payMenthod != null)
                {
                    dra.Transferee.Account.AcctNumber = payMenthod.AccountNumber;
                    if (string.IsNullOrEmpty(payMenthod.AccountName))
                    {
                        dra.Transferee.Account.Name = "";
                    }
                    else
                    {
                        dra.Transferee.Account.Name = payMenthod.AccountName;
                    }
                    dra.Transferee.Account.City = payMenthod.BranchName;
                    dra.Transferee.Account.Country = "SRI LANKA"; // Need to change here to get the country name
                }
                Country benCountry = countrySer.GetAll(x => x.CountryID == beneficiaryList.CountryID, null, "").SingleOrDefault();
                if (benCountry != null)
                {
                    dra.Transferee.Account.Country = benCountry.CountryName;
                }
                else
                {
                    dra.Transferee.Account.Country = "SRI LANKA";
                }


                model.Iftidra.Add(dra);
                //  }

                //  kl = kl + 1;
            }

            return model;
        }

        private string ChangeDOB(string dob)
        {
            if (!string.IsNullOrEmpty(dob))
            {
                string[] arr = dob.Split('/');
                int val1 = int.Parse(arr[1]);
                int val2 = int.Parse(arr[0]);

                string val1st = val1.ToString();
                if (val1 < 10)
                {
                    val1st = "0" + val1;
                }
                string val2st = val2.ToString();
                if (val2 < 10)
                {
                    val2st = "0" + val2;
                }

                return arr[2] + "-" + val1st + "-" + val2st;
            }
            else
            {
                return "";
            }
        }

        private string GetAgentRegNumber(int agentId)
        {
            AgentService ser = new AgentService(new UnitOfWorks(new KaprukaEntities()));
            var agent = ser.GetAll(x => x.AgentID == agentId, null, "").SingleOrDefault();
            return agent.AustracReNumber;
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Response.Redirect("ReportsAgent/AF01ListOfTransactionsByBank.aspx");
        }


        protected void Button2_Click2(object sender, EventArgs e)
        {
            Response.Redirect("ReportsAgent/CustomerDepositsReport.aspx");
        }

        protected void Button3_Click1(object sender, EventArgs e)
        {
            Response.Redirect("ReportsAgent/BankTotalAverageReport.aspx");
        }
    }
}