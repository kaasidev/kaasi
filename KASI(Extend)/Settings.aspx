﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="KASI_Extend_.Settings2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/tag-it.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>

    <!-- CSS FILES-->
    <link href="css/jquery-ui.settings.css" rel="stylesheet" />
    <link href="css/datatables.settings.css" rel="stylesheet" />
    <link href="css/jquery.tagit.css" rel="stylesheet" />
    <link href="css/select2.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />
    <script src="js/select2.js"></script>
     <link href="css/toastr.min.css" rel="stylesheet" />
     <script src="js/toastr.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#edit_austracregdate").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#edit_austracregexpiry").datepicker({ dateFormat: 'dd/mm/yy' });

            $("#ag_add_austracregdate").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#ag_add_austracregexpiry").datepicker({ dateFormat: 'dd/mm/yy' });


        });
        function showerror(msg) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.error(msg, 'Error');

        }
        function showsuccess(msg, body) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.success(msg, body);

        }
        function openCurrencyForEdit(CID)
        {
            DialogViewCountries.dialog('open');
            $.ajax({
                url: 'JQDataFetch/getCountryDetails.aspx',
                async: false,
                data: {
                    CID: CID
                },
                success: function (response) {
                    var initialsplit = response.split('~');
                    $('#EDIT_CCode').text(initialsplit[0]);
                    $('#EDIT_CName').text(initialsplit[1]);
                    $('#EDIT_AddCodes').text(initialsplit[2]);
                    $('#EDIT_CFlagFile').attr('src', initialsplit[3]);
                    $('#EDIT_CNTName').text(initialsplit[4]);
                }
            })
        }


        function openBankAccountForEdit(BAID)
        {
            $("#hidMasterCompanyId").val(BAID);
            $.ajax({
                url: 'JQDataFetch/GetMasterBankDetails.aspx',
                data: {
                    id: BAID,
                    bankName: $('#ag_add_bankname').val(),
                    bsb: $('#ag_add_Bsb').val() + '-' + $('#ag_add_Bsb').val(),
                    account: $('#ag_add_Account').val(),
                    accountName: $('#ag_add_banksomething').val(),

                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    $('#edit_bankname').val(data.BankName);
                    $('#edit_accountname').val(data.AccountName);
                    $('#edit_BSB1').val(data.BSB);
                    $('#edit_BSB2').val(data.BSB2);
                    $('#edit_AccountNumber').val(data.AccountNumber);
                   
                }
            });
            DialogEditBankAccount.dialog('open');
        }

        function openUsersForEdit(UID)
        {
            $("#hidUserID").val(UID);
            $.ajax({
                url: 'JQDataFetch/getUserDetails.aspx',
                data: {
                    id: UID,
                },
                async: false,
                success: function (response) {
                    var datad = $.parseJSON(response);
                    if (datad.LoginType == "AGENT") {
                        $("#DIV_EditAdminUser").hide();
                        $("#DIV_EditAgentUser").show();
                        $('#hidden_AccountType').val("AGENT");
                        $.ajax({
                            url: 'JQDataFetch/getAllAgents.aspx',
                            async: false,
                            success: function (data) {
                                $('#SLT_Edit_AgentList').empty();
                                $('#SLT_Edit_AgentList').append("<option value='NOSELECT'>-- SELECT -- </option>");
                                var initialsplit = data.split('~');
                                initialsplit.sort();
                                $.each(initialsplit,
                                    function (index, item) {
                                        var datasplit = initialsplit[index].split('|');
                                        if (datad.AgentID == datasplit[1]) {
                                            $('#SLT_Edit_AgentList').append("<option selected value='" + datasplit[1] + "'>" + datasplit[0] + "</option>");
                                        }
                                        else {
                                            $('#SLT_Edit_AgentList').append("<option value='" + datasplit[1] + "'>" + datasplit[0] + "</option>");
                                        }
                                    });

                            }
                        });

                        $("#editagent_firstname").val(datad.FirstName);
                        $("#editagent_lastname").val(datad.LastName);
                        $("#editagent_email").val(datad.Email);
                        $("#editagent_username").val(datad.UserName);

                        if (datad.CompMan == "0")
                        {
                            $('#CHK_EditCompMan').prop('checked', false);
                        }
                        else
                        {
                            $('#CHK_EditCompMan').prop('checked', true);
                        }
                       
                    }
                    else {
                        $("#DIV_EditAdminUser").show();
                        $("#DIV_EditAgentUser").hide();
                        $('#hidden_AccountType').val("MASTER");
                        $("#editadmin_firstname").val(datad.FirstName);
                        $("#editadmin_lastname").val(datad.LastName);
                        $("#editadmin_email").val(datad.Email);
                        $("#editadmin_username").val(datad.UserName);
                        $('#editagent_compmanpin').val(datad.CompManPIN);
                    }


                }
            })
            DialogEditUsers.dialog('open');
        }


        function openAgentForEdit(AID) {

            $("#errormdivessagedisplay").hide();
            $("#errormdivessagedisplay").text("");
            $("#hidEditAgentId").val(AID);
            $('#btn_DeleteAgent').hide();
            $('#btn_InactivateAgent').hide();

            var MCMAUDLimit;
            var MCYAUDLimit;
            var MCMNbLimit;
            var MCYNbLimit;
            var AGMAUDLimit;
            var AGYAUDLimit;
            var AGMNbLimit;
            var AGYNbLimit;

            $.ajax({
                url: 'JQDataFetch/getMasterTransLimits.aspx',
                async: false,
                success: function (response) {
                    var data = response.split('|');
                    MCMAUDLimit = data[0];
                    MCYAUDLimit = data[1];
                    MCMNbLimit = data[2];
                    MCYNbLimit = data[3];
                }
            });

            $.ajax({
                url: 'JQDataFetch/getAgentTransLimit.aspx',
                async: false,
                data: {
                    AgentID: AID,
                },
                success: function (response) {
                    var data = response.split('|');
                    AGMAUDLimit = data[0];
                    AGYAUDLimit = data[1];
                    AGMNbLimit = data[2];
                    AGYNbLimit = data[3];
                }
            });

            $("#edit_slider-range-max").slider({
                range: "max",
                min: 0,
                max: Math.round(MCMAUDLimit),
                value: Math.round(AGMAUDLimit),
                step: 500,
                slide: function (event, ui) {
                    $('#edit_amount').val(ui.value);
                }
            });
            $('#edit_amount').val($('#edit_slider-range-max').slider("value"));

            $('#edit_slider-yaudlimit').slider({
                range: "max",
                min: 0,
                max: Math.round(MCYAUDLimit),
                value: Math.round(AGYAUDLimit),
                step: 500,
                slide: function (event, ui) {
                    $('#edit_yaudtrans').val(ui.value);
                }
            })
            $('#edit_yaudtrans').val($('#edit_slider-yaudlimit').slider("value"));

            $('#edit_slider-mnblimit').slider({
                range: "max",
                min: 1,
                max: Math.round(MCMNbLimit),
                value: Math.round(AGMNbLimit),
                slide: function (event, ui) {
                    $('#edit_mnbtrans').val(ui.value);
                }
            })
            $('#edit_mnbtrans').val($('#edit_slider-mnblimit').slider("value"));

            $("#edit_slider-ynblimit").slider({
                range: "max",
                min: 0,
                max: Math.round(MCYNbLimit),
                value: Math.round(AGYNbLimit),
                slide: function (event, ui) {
                    $('#edit_ynbtrans').val(ui.value);
                }
            })
            $('#edit_ynbtrans').val($('#edit_slider-ynblimit').slider("value"));



            $.ajax({
                url: 'JQDataFetch/checkAgentHasRecords.aspx',
                data: {
                    AID: AID,
                },
                async: false,
                success: function (response) {
                    if (response == "INACTIVE")
                    {
                        $('#btn_InactivateAgent').show();
                    }
                    else
                    {
                        $('#btn_DeleteAgent').show();
                    }
                }
            })

            $.ajax({
                url: 'JQDataFetch/checkAgentHasKYCInfo.aspx',
                async: false,
                data: {
                    AID: AID,
                },
                success: function (response) {
                    if (response != "NOKYC")
                    {
                        var data = response.split('|');
                        $("#errormdivessagedisplay").show();
                        $("#errormdivessagedisplay").text("KYC Check was performed on " + data[1] + ". Result: " + data[0]);
                    }
                }
            })

            $.ajax({
                url: 'Processor/GetAgentDetails.aspx?id='+AID,
                async: false,
                success: function (st) {
                    var data = $.parseJSON(st);
                    $("#ag_edit_agentname").val(data.AgentName);
                    $('#ag_edit_addressline1').val(data.AgentAddress1);
                    $('#ag_edit_addressline2').val(data.AgentAddress2);
                    $('#ag_edit_Suburb').val(data.AgentCity);
                    $('#ag_edit_State').val(data.AgentState);
                    $('#ag_edit_Postcode').val(data.AgentPostcode);
                    $('#ag_edit_Phone').val(data.AgentPhone);
                    $('#ag_edit_Fax').val(data.AgentFax);
                    $('#ag_edit_Email').val(data.AgentEmail);
                    $('#ag_edit_ABN').val(data.AgentABN);
                    $('#ag_edit_ACN').val(data.AgentACN);
                    $('#ag_edit_FlatFee').val(data.AgentFeeFlat),
                    $('#ag_edit_AddPerc').val(data.AgentFeeCommission),
                    $('#edit_austracregno').val(data.AgentAustracNumber),
                    $('#edit_austracregdate').val(data.AgentAustracRegDate),
                    $('#edit_austracregexpiry').val(data.AgentAustracRenewalDate),
                    CommStruct = data.AgentCommissionStructure,
                    $('#ag_edit_CommPerc').val(data.AgentTransGainCommission),
                   
                    $('#ag_edit_Countries').select2();
                    var cList = data.AgentCountries.split(',');
                    var array = [];
                    $.each(cList, function (index, item) {
                        array.push(item);
                    });

                    if (CommStruct == "AUDGain") {
                        $('#btn_ag_edit_ExGain').hide();
                        $("#btn_ag_edit_AUDGain").attr('class', 'aa_select_btn_active')
                    }
                    else
                    {
                        $('#btn_ag_edit_AUDGain').hide();
                        $("#btn_ag_edit_ExGai").attr('class', 'aa_select_btn_active')
                    }


                    $("#ag_edit_Countries").select2('val', array);
                    
                    $("#tbl_Edit_Agent_Banking tr").remove();
                    $.ajax({
                        url: 'JQDataFetch/getDepositMethods.aspx',
                        async: false,
                        success: function (datas) {
                           
                            var splitinfo = datas.split('~');
                            var idlst = "";
                            $.each(splitinfo, function (index, item) {
                                
                                var spdata = splitinfo[index].split('|');
                                var slArr = data.BankIds.split(',');
                                if (jQuery.inArray(spdata[0], slArr) > -1) {
                                    idlst = idlst + spdata[0] + ",";
                                    var row = $("<tr><td class='settings_edit_agent_bankaccs_2'><input type='checkbox' checked class='chk_banks' onchange=\"checkBankList('" + spdata[0] + "')\" id='" + spdata[0] + "' name='CHK_" + spdata[1] + "' value='" + spdata[0] + "' /></td><td style='width:5px;'>&nbsp;</td><td class='settings_edit_agent_bankaccs'>" + spdata[1] + "</td></tr>").appendTo('#tbl_Edit_Agent_Banking');
                                    var rowx = $("<tr class='spacer5'><td>&nbsp;</td><td style='width:5px;'>&nbsp;</td><td>&nbsp;</td></tr>").appendTo('#tbl_Edit_Agent_Banking');
                                }
                                else {
                                    var row = $("<tr><td class='settings_edit_agent_bankaccs_2'><input type='checkbox' class='chk_banks' onchange=\"checkBankList('" + spdata[0] + "')\" id='" + spdata[0] + "' name='CHK_" + spdata[1] + "' value='" + spdata[0] + "' /></td><td style='width:5px;'>&nbsp;</td><td class='settings_edit_agent_bankaccs'>" + spdata[1] + "</td></tr>").appendTo('#tbl_Edit_Agent_Banking');
                                    var rowx = $("<tr class='spacer5'><td>&nbsp;</td><td style='width:5px;'>&nbsp;</td><td>&nbsp;</td></tr>").appendTo('#tbl_Edit_Agent_Banking');
                                }
                            });
                            $("#hidSelectedIdList").val(idlst);

                        }
                    });
                    
                   
                }
            });

            DialogEditAddAgent.dialog('open');


        }

        $(document).ready(function () {
            $("#errormdivessagedisplay").hide();
            $("#errormdivessagedisplay").text("");

            $("#errormbttomdivessagedisplay").hide();
            $("#errormbttomdivessagedisplay").text("");

            $('#ag_add_Bsb').mask('000');
            $('#ag_add_Bsb2').mask('000');
            $('#edit_BSB1').mask('000');
            $('#edit_BSB2').mask('000');

            $('#ag_add_MUPhone').mask('(00) 0000 0000');
            $('#ag_add_MUMobile').mask('0000 000 000');
            $('#ag_edit_MUPhone').mask('(00) 0000 0000');
            $("#ag_edit_MUMobile").mask('0000 000 000');
            $('#ag_add_Phone').mask('(00) 0000 0000');

            $('#ag_add_austracregdate').mask('00/00/0000');
            $('#ag_add_austracregexpiry').mask('00/00/0000');

            $('#ag_edit_Phone').mask('(00) 0000 0000');
            $('#ag_edit_Fax').mask('(00) 0000 0000')
            $('#ag_edit_State').mask('AAA');
            $('#ag_edit_Postcode').mask('0000');
            $('#ag_edit_ABN').mask('00 000 000 000');
            $('#ag_edit_ACN').mask('000 000 000');

            $('#addnewagent_compmanpin').mask('0000');
            $('#editagent_compmanpin').mask('0000');

            $('#div_settings02').hide();
            $('#div_settings03').hide();
            // Main Tabs 
            $('#BankingTab').hide();
            $('#BankingTab').hide();
            $('#ThirdMainTab').hide();
            $('#FourthMainTab').hide();
            $('#FifthMainTab').hide();
            $('#DIV_AgentCommission').hide();
            $('#DIV_AgentBanking').hide();
            $('#DIV_AgentMasterUser').hide();
            $("#DIV_AgentLimits").hide();
            $('#DIV_AddNewAgentUser').hide();
            $("#TR_NOSMTPDetails").hide();
            $('#TR_SMTPDetails').hide();
            $('#btn_AddNewSMTP').hide();
            $("#btn_EditSMTP").hide();
            //$("#SMTPTestResult").hide();
            $('#SMTP_Result_TR').hide();

            $('#btn_ActivateUser').hide();
            $("#btn_InactiveUser").hide();

            $('#BTN_ag_BackStep1').hide();
            $('#BTN_ag_Step2').hide();
            $('#BTN_ag_Step3').hide();
            $("#BTN_ag_Step4").hide();
            $('#BTN_ag_BackStep2').hide();
            $('#BTN_ag_BackStep3').hide();
            $("#BTN_ag_BackStep4").hide();
            $("#btn_SaveNewAgent").hide();
            

            $('#DIV_Edit_AgentCommission').hide();
            $('#DIV_Edit_AgentBanking').hide();
            $('#DIV_Edit_AgentMasterUser').hide();
            $('#DIV_EditAgentLimits').hide();

            $('#ag_add_State').mask('AAA');
            $("#MCEdit_State").mask('AAA');
            $('#ag_add_Postcode').mask('0000');
            $("#MCEdit_Postcode").mask('0000');
            

            $('#ag_add_Fax').mask('(00) 0000 0000');
            $('#ag_add_ABN').mask('00 000 000 000');
            $('#ag_add_ACN').mask('000 000 000');
            $('#MCEdit_Telephone').mask('(00) 0000 0000');

            $('#MCEdit_Fax').mask('(00) 0000 0000');
            $('#MCEdit_ABN').mask('00 000 000 000');
            $('#MCEdit_ACN').mask('000 000 000');

            var MCMAUDLimit;
            var MCYAUDLimit;
            var MCMNbLimit;
            var MCYNbLimit;

            $.ajax({
                url: 'JQDataFetch/getMasterTransLimits.aspx',
                async: false,
                success: function (response) {
                    var data = response.split('|');
                    MCMAUDLimit = data[0];
                    MCYAUDLimit = data[1];
                    MCMNbLimit = data[2];
                    MCYNbLimit = data[3];
                }
            });

            $("#slider-range-max").slider({
                range: "max",
                min: 0,
                max: Math.round(MCMAUDLimit),
                value: Math.round(MCMAUDLimit),
                step: 500,
                slide: function (event, ui) {
                    $('#amount').val(ui.value);
                }
            });
            $('#amount').val($('#slider-range-max').slider("value"));

            $('#slider-yaudlimit').slider({
                range: "max",
                min: 0,
                max: Math.round(MCYAUDLimit),
                value: Math.round(MCYAUDLimit),
                step: 500,
                slide: function (event, ui) {
                    $('#yaudtrans').val(ui.value);
                }
            })
            $('#yaudtrans').val($('#slider-yaudlimit').slider("value"));


            $('#slider-mnblimit').slider({
                range: "max",
                min: 1,
                max: Math.round(MCMNbLimit),
                value: Math.round(MCMNbLimit),
                slide: function (event, ui) {
                    $('#mnbtrans').val(ui.value);
                }
            })
            $('#mnbtrans').val($('#slider-mnblimit').slider("value"));

            $("#slider-ynblimit").slider({
                range: "max",
                min: 0,
                max: Math.round(MCYNbLimit),
                value: Math.round(MCYNbLimit),
                slide: function (event, ui) {
                    $('#ynbtrans').val(ui.value);
                }
            })
            $('#ynbtrans').val($('#slider-ynblimit').slider("value"));

            // ************* POPULATE DROPDOWN BOXES WITH COUNTRIES ********

            $.ajax({
                url: 'JQDataFetch/getAllCountriesList.aspx',
                async: false,
                success: function (response) {
                    var data = response.split('|');
                    data.sort();
                    $('#ag_add_Countries').empty();
                    $('#ag_edit_Countries').empty();
                    $.each(data, function (index, item) {
                        $('#ag_add_Countries').append('<option value="' + data[index] + '">' + data[index] + '</option>');
                        $('#ag_edit_Countries').append('<option value="' + data[index] + '">' + data[index] + '</option>');
                    });
                    
                }
            })

            // ************* END POPULATE *************
            
            // Check SMTP Details and display appropriate section
            $.ajax({
                url: 'JQDataFetch/getMCSMTPDetails.aspx',
                async: false,
                success: function (response) {
                    var splitres = response.split('|');
                    if (splitres[0] == "") {
                        $("#TR_NOSMTPDetails").show();
                        $('#btn_AddNewSMTP').show();
                    }
                    else {
                        $('#TR_SMTPDetails').show();
                        $("#btn_EditSMTP").show();
                        $("#td_smtpservername").text(splitres[0]);
                        $('#td_smtpport').text(splitres[1]);
                        if (splitres[2] == 2)
                        {
                            $('#td_sslenabled').text('false');
                        }
                        else
                        {
                            $('#td_sslenabled').text('true');
                        }
                        
                        $('#td_username').text(splitres[3]);
                        $('#td_emailaddress').text(splitres[5]);
                    }
                }
            });

            
            // ******* ADD NEW AGENT NAVIGATION ***********

            $("#BTN_ag_Step1").click(function () {

                var isField1Valid = $('#ag_add_agentname').valid();
                //var isField2Valid = $('#ag_streetno').valid();
                //var isField3Valid = $('#ag_streetname').valid();
                //var isField4Valid = $('#ag_streettype').valid();
                var isField5Valid = $('#ag_add_addressline1').valid();
                var isField6Valid = $('#ag_add_Suburb').valid();
                var isField7Valid = $('#ag_add_State').valid();
                var isField8Valid = $('#ag_add_Postcode').valid();
                var isField9Valid = $('#ag_country').valid();
                var isField10Valid = $('#ag_add_ABN').valid();
                var isField11Valid = $('#ag_add_Phone').valid();
                var isField12Valid = $('#ag_add_Email').valid();


                if (isField1Valid == true && isField5Valid == true && isField6Valid == true && isField7Valid == true && isField8Valid == true && isField9Valid == true && isField10Valid == true && isField11Valid == true && isField12Valid == true)
                {
                    $('#DIV_AgentDetails').hide();
                    $("#DIV_AgentCommission").show();
                    $('#DIV_AgentBanking').hide();
                    $('#DIV_AgentMasterUser').hide();
                    $('#TD_AgentDetails').removeClass();
                    $('#TD_AgentDetails').addClass("wiz_tab_inactive_left");
                    $('#TD_AgentCommission').removeClass();
                    $('#TD_AgentCommission').addClass("wiz_tab_active");
                    $('#TD_AgentMasterUser').removeClass();
                    $('#TD_AgentMasterUser').addClass("wiz_tab_inactive_right");
                    $('#TD_AgentBanking').removeClass();
                    $('#TD_AgentBanking').addClass("wiz_tab_inactive_right");
                    $('#BTN_ag_Step1').hide();
                    $('#BTN_ag_BackStep1').show();
                    $('#BTN_ag_Step2').show();
                }
                else
                {
                    //alert('Not Triggered');
                }
                
            });


            $("#BTN_ag_BackStep1").click(function () {
                $('#DIV_AgentDetails').show();
                $("#DIV_AgentCommission").hide();
                $('#DIV_AgentBanking').hide();
                $('#DIV_AgentMasterUser').hide();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("wiz_tab_active");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("wiz_tab_inactive_right");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("wiz_tab_inactive_right");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("wiz_tab_inactive_right");
                $("#BTN_ag_BackStep1").hide();
                $('#BTN_ag_Step2').hide();
                $('#BTN_ag_Step1').show();
            });


            $('#addnewadmin_email').click(function () {
                $('#addnewadmin_username').val($('#addnewadmin_email').val());
            });

            $("#BTN_ag_Step2").click(function () {

                    $('#DIV_AgentDetails').hide();
                    $("#DIV_AgentCommission").hide();
                    $('#DIV_AgentBanking').show();
                    $('#DIV_AgentMasterUser').hide();
                    $('#TD_AgentDetails').removeClass();
                    $('#TD_AgentDetails').addClass("wiz_tab_inactive_left");
                    $('#TD_AgentCommission').removeClass();
                    $('#TD_AgentCommission').addClass("wiz_tab_inactive_left");
                    $('#TD_AgentMasterUser').removeClass();
                    $('#TD_AgentMasterUser').addClass("wiz_tab_inactive_right");
                    $('#TD_AgentBanking').removeClass();
                    $('#TD_AgentBanking').addClass("wiz_tab_active");
                    $('#BTN_ag_Step2').hide();
                    $("#BTN_ag_BackStep1").hide();
                    $('#BTN_ag_BackStep2').show();
                    $('#BTN_ag_Step3').show();
                

            });

            $('#BTN_ag_BackStep2').click(function () {
                $('#DIV_AgentDetails').hide();
                $("#DIV_AgentCommission").show();
                $('#DIV_AgentBanking').hide();
                $('#DIV_AgentMasterUser').hide();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("wiz_tab_inactive_left");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("wiz_tab_active");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("wiz_tab_inactive_right");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("wiz_tab_inactive_right");
                $('#BTN_ag_Step2').show();
                $("#BTN_ag_BackStep1").show();
                $('#BTN_ag_BackStep2').hide();
                $('#BTN_ag_Step3').hide();
            });

            $('#BTN_ag_Step3').click(function () {

                var c = $('#ag_add_Countries').valid();
                if (c == false)
                {
                    alert('Please select a country and bank account before proceeding.')
                }
                else
                {

                    
                        $('#DIV_AgentDetails').hide();
                        $("#DIV_AgentCommission").hide();
                        $('#DIV_AgentBanking').hide();
                        $('#DIV_AgentMasterUser').show();
                        $('#TD_AgentDetails').removeClass();
                        $('#TD_AgentDetails').addClass("wiz_tab_inactive_left");
                        $('#TD_AgentCommission').removeClass();
                        $('#TD_AgentCommission').addClass("wiz_tab_inactive_left");
                        $('#TD_AgentMasterUser').removeClass();
                        $('#TD_AgentMasterUser').addClass("wiz_tab_active");
                        $('#TD_AgentBanking').removeClass();
                        $('#TD_AgentBanking').addClass("wiz_tab_inactive_left");
                        $('#BTN_ag_Step3').hide();
                        $("#BTN_ag_BackStep2").hide();
                        $('#BTN_ag_BackStep3').show();
                        $('#BTN_ag_Step4').show();



                    
                }
            });

        $("#BTN_ag_Step4").click(function () {

            var isField1Valid = $('#ag_add_MUName').valid();
            var ifField2Valid = $('#ag_add_MUEmail').valid();
            var isField3Value = $('#ag_add_MLName').valid();
            var isField4Value = $('#ag_hidden_EmailValid').val();
            if (isField1Valid == true && ifField2Valid == true && isField3Value == true && isField4Value == "FALSE")
            {
                $('#DIV_AgentDetails').hide();
                $("#DIV_AgentCommission").hide();
                $('#DIV_AgentBanking').hide();
                $('#DIV_AgentMasterUser').hide();
                $('#DIV_AgentLimits').show();
                $('#BTN_ag_BackStep3').hide();
                $('#BTN_ag_Step4').hide();
                $('#BTN_ag_BackStep4').show();
                $('#btn_SaveNewAgent').show();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("wiz_tab_inactive_left");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("wiz_tab_inactive_left");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("wiz_tab_inactive_left");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("wiz_tab_inactive_left");
                $('#TD_AgentLimits').removeClass();
                $('#TD_AgentLimits').addClass("wiz_tab_active");
            }

            });

            $('#BTN_ag_BackStep4').click(function () {
                $('#DIV_AgentDetails').hide();
                $("#DIV_AgentCommission").hide();
                $('#DIV_AgentBanking').hide();
                $('#DIV_AgentMasterUser').show();
                $('#DIV_AgentLimits').hide();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("wiz_tab_inactive_left");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("wiz_tab_inactive_left");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("wiz_tab_active");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("wiz_tab_inactive_left");
                $('#TD_AgentLimits').removeClass();
                $('#TD_AgentLimits').addClass("wiz_tab_inactive_right");
                $('#BTN_ag_Step3').hide();
                $("#BTN_ag_BackStep2").hide();
                $("#BTN_ag_BackStep4").hide();
                $('#BTN_ag_BackStep3').show();
                $('#btn_SaveNewAgent').hide();
                $('#BTN_ag_Step4').show();
            })

            $('#BTN_ag_BackStep3').click(function () {
                $('#DIV_AgentDetails').hide();
                $("#DIV_AgentCommission").hide();
                $('#DIV_AgentBanking').show();
                $('#DIV_AgentMasterUser').hide();
                $('#TD_AgentDetails').removeClass();
                $('#TD_AgentDetails').addClass("wiz_tab_inactive_left");
                $('#TD_AgentCommission').removeClass();
                $('#TD_AgentCommission').addClass("wiz_tab_inactive_left");
                $('#TD_AgentMasterUser').removeClass();
                $('#TD_AgentMasterUser').addClass("wiz_tab_inactive_right");
                $('#TD_AgentBanking').removeClass();
                $('#TD_AgentBanking').addClass("wiz_tab_active");
                $('#BTN_ag_Step3').show();
                $("#BTN_ag_BackStep2").show();
                $('#BTN_ag_BackStep3').hide();
                $('#btn_SaveNewAgent').hide();
            });




            $('#GlobalSettingsClick').click(function () {
                $('#div_settings01').show();
                $('#div_settings02').hide();
                $('#div_settings03').hide();
            });

            $('#AgentSettingsClick').click(function () {
                $('#div_settings01').hide();
                $('#div_settings02').show();
                $('#div_settings03').hide();
            });

            $('#ForeignBankClick').click(function () {
                $('#div_settings01').hide();
                $('#div_settings02').hide();
                $('#div_settings03').show();
            });

            // Settings Main Tab 01 Function
            $('#TD_Agent').click(function () {
                $('#AgentTab').show();
                $('#BankingTab').hide();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("tr_tabs_active");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("tr_tabs_inactive_right");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("tr_tabs_inactive_right");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("tr_tabs_inactive_right");
                $('#TD_MainTab05').removeClass();
                $('#TD_MainTab05').addClass("tr_tabs_inactive_right");
            })

            // Settings Main Tab 02 Function
            $('#TD_Banking').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').show();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("tr_tabs_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("tr_tabs_active");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("tr_tabs_inactive_right");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("tr_tabs_inactive_right");
                $('#TD_MainTab05').removeClass();
                $('#TD_MainTab05').addClass("tr_tabs_inactive_right");
            });

            // Settings Main Tab 03 Function
            $('#TD_MainTab03').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').hide();
                $('#ThirdMainTab').show();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("tr_tabs_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("tr_tabs_inactive_left");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("tr_tabs_active");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("tr_tabs_inactive_right");
                $('#TD_MainTab05').removeClass();
                $('#TD_MainTab05').addClass("tr_tabs_inactive_right");
            });

            // Settings Main Tab 04 Function
            $('#TD_MainTab04').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').hide();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').show();
                $('#FifthMainTab').hide();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("tr_tabs_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("tr_tabs_inactive_left");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("tr_tabs_inactive_left");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("tr_tabs_active");
                $('#TD_MainTab05').removeClass();
                $('#TD_MainTab05').addClass("tr_tabs_inactive_right");
            });

            // Settings Main Tab 05 Function
            $('#TD_MainTab05').click(function () {
                $('#AgentTab').hide();
                $('#BankingTab').hide();
                $('#ThirdMainTab').hide();
                $('#FourthMainTab').hide();
                $('#FifthMainTab').show();
                $('#TD_Agent').removeClass();
                $('#TD_Agent').addClass("tr_tabs_inactive_left");
                $('#TD_Banking').removeClass();
                $('#TD_Banking').addClass("tr_tabs_inactive_left");
                $('#TD_MainTab03').removeClass();
                $('#TD_MainTab03').addClass("tr_tabs_inactive_left");
                $('#TD_MainTab04').removeClass();
                $('#TD_MainTab04').addClass("tr_tabs_inactive_left");
                $('#TD_MainTab05').removeClass();
                $('#TD_MainTab05').addClass("tr_tabs_active");
            });



            $('#TD_Edit_AgentDetails').click(function () {
                $('#DIV_Edit_AgentDetails').show();
                $("#DIV_Edit_AgentCommission").hide();
                $('#DIV_Edit_AgentBanking').hide();
                $('#DIV_Edit_AgentMasterUser').hide();
                $('#TD_Edit_AgentDetails').removeClass();
                $('#TD_Edit_AgentDetails').addClass("wiz_tab_active");
                $('#TD_Edit_AgentCommission').removeClass();
                $('#TD_Edit_AgentCommission').addClass("wiz_tab_inactive_right");
                $('#TD_Edit_AgentMasterUser').removeClass();
                $('#TD_Edit_AgentMasterUser').addClass("wiz_tab_inactive_right");
                $('#TD_Edit_AgentBanking').removeClass();
                $('#TD_Edit_AgentBanking').addClass("wiz_tab_inactive_right");
                $("#DIV_EditAgentLimits").hide();
                $('#TD_Edit_AgentLimits').removeClass();
                $('#TD_Edit_AgentLimits').addClass("wiz_tab_inactive_right");
            });

            $('#TD_Edit_AgentCommission').click(function () {
                $('#DIV_Edit_AgentDetails').hide();
                $("#DIV_Edit_AgentCommission").show();
                $('#DIV_Edit_AgentBanking').hide();
                $('#DIV_Edit_AgentMasterUser').hide();
                $('#TD_Edit_AgentDetails').removeClass();
                $('#TD_Edit_AgentDetails').addClass("wiz_tab_inactive_left");
                $('#TD_Edit_AgentCommission').removeClass();
                $('#TD_Edit_AgentCommission').addClass("wiz_tab_active");
                $('#TD_Edit_AgentMasterUser').removeClass();
                $('#TD_Edit_AgentMasterUser').addClass("wiz_tab_inactive_right");
                $('#TD_Edit_AgentBanking').removeClass();
                $('#TD_Edit_AgentBanking').addClass("wiz_tab_inactive_right");
                $("#DIV_EditAgentLimits").hide();
                $('#TD_Edit_AgentLimits').removeClass();
                $('#TD_Edit_AgentLimits').addClass("wiz_tab_inactive_right");
            });

            $("#btn_ag_ExGain").click(function () {
                $("#ag_add_CommStruc").val('FXGain');
                $('#btn_ag_AUDGain').removeClass();
                $('#btn_ag_AUDGain').addClass("aa_select_btn_inactive");
                $('#btn_ag_ExGain').removeClass();
                $('#btn_ag_ExGain').addClass("aa_select_btn_active");
            });

            $('#ag_edit_MUEmail').blur(function () {
                $.ajax({
                    url: 'Processor/verifyEmailAddressExistsAgentWithoutID.aspx',
                    beforeSend: function () {
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        Email: $('#ag_edit_MUEmail').val(),
                        AID: $('#hidEditAgentId').val(),
                    },
                    complete: function () {
                        LoadingDIV.dialog('close');
                    },
                    success: function (response) {
                        //LoadingDIV.dialog('close');
                        if (response == "true") {
                            alert("This email address is already registered. Please choose a new one");
                            $('#ag_edit_hiddenEmail').val("TRUE");
                        }
                        else {
                            $('#ag_edit_hiddenEmail').val("FALSE");
                        }
                    },
                    error: function () {
                        LoadingDIV.dialog('close');
                    }
                })
            });

            $("#ag_add_MUEmail").blur(function () {
                $.ajax({
                    url: 'Processor/verifyEmailAddressExistsAgent.aspx',
                    beforeSend: function () {
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        Email: $('#ag_add_MUEmail').val(),
                    },
                    complete: function() {
                        LoadingDIV.dialog('close');
                    },
                    success: function (response) {
                        //LoadingDiv.dialog('close');
                        if (response == "true")
                        {
                            alert("This email address is already registered. Please choose a new one");
                            $('#ag_hidden_EmailValid').val("TRUE");
                        }
                        else
                        {
                            $('#ag_hidden_EmailValid').val("FALSE");
                        }
                    },
                    error: function () {
                        LoadingDIV.dialog('close');
                    }
                })
            });

            $('#TD_Edit_AgentBanking').click(function () {
                $('#DIV_Edit_AgentDetails').hide();
                $("#DIV_Edit_AgentCommission").hide();
                $('#DIV_Edit_AgentBanking').show();
                $('#DIV_Edit_AgentMasterUser').hide();
                $('#TD_Edit_AgentDetails').removeClass();
                $('#TD_Edit_AgentDetails').addClass("wiz_tab_inactive_left");
                $('#TD_Edit_AgentCommission').removeClass();
                $('#TD_Edit_AgentCommission').addClass("wiz_tab_inactive_left");
                $('#TD_Edit_AgentMasterUser').removeClass();
                $('#TD_Edit_AgentMasterUser').addClass("aa_tab_inactive_right");
                $('#TD_Edit_AgentBanking').removeClass();
                $('#TD_Edit_AgentBanking').addClass("wiz_tab_active");
                $('#TD_Edit_AgentLimits').removeClass();
                $('#TD_Edit_AgentLimits').addClass("wiz_tab_inactive_right");
                $("#DIV_EditAgentLimits").hide();
            });

            $("#TD_Edit_AgentLimits").click(function () {
                $('#DIV_Edit_AgentDetails').hide();
                $("#DIV_Edit_AgentCommission").hide();
                $('#DIV_Edit_AgentBanking').hide();
                $('#DIV_Edit_AgentMasterUser').hide();
                $('#TD_Edit_AgentDetails').removeClass();
                $('#TD_Edit_AgentDetails').addClass("wiz_tab_inactive_left");
                $('#TD_Edit_AgentCommission').removeClass();
                $('#TD_Edit_AgentCommission').addClass("wiz_tab_inactive_left");
                $('#TD_Edit_AgentMasterUser').removeClass();
                $('#TD_Edit_AgentMasterUser').addClass("wiz_tab_inactive_left");
                $('#TD_Edit_AgentBanking').removeClass();
                $('#TD_Edit_AgentBanking').addClass("wiz_tab_inactive_left");
                $('#TD_Edit_AgentLimits').removeClass();
                $('#TD_Edit_AgentLimits').addClass("wiz_tab_active");
                $("#DIV_EditAgentLimits").show();
            })

            $('#btn_CountriesClose ').click(function () {
                DialogViewCountries.dialog('close');
            });

            $("#btn_ag_AUDGain").click(function () {
                $("#ag_add_CommStruc").val('AUDGain');
                $('#btn_ag_AUDGain').removeClass();
                $('#btn_ag_AUDGain').addClass("aa_select_btn_active");
                $('#btn_ag_ExGain').removeClass();
                $('#btn_ag_ExGain').addClass("aa_select_btn_inactive");
            });

            $('#btn_AddNewAccount').click(function () {
                $("#hidMasterCompanyId").val("");
                DialogAddNewBankAccount.dialog('open');
            });

            $('#btn_AddNewCountry').click(function () {
                DialogAddNewCountry.dialog('open');
            });

            $('#btn_EditLimitsVariations').click(function () {
                $.ajax({
                    url: 'JQDataFetch/getVariationLimits.aspx',
                    async: false,
                    success: function (response) {
                        var data = response.split('|');
                        $('#EDIT_MAUDLimit').val(data[0]);
                        $('#EDIT_YAUDLimit').val(data[1]);
                        $("#EDIT_MTransLimit").val(data[2]);
                        $('#EDIT_YTransLimit').val(data[3]);
                        $('#EDIT_MVariation').val(data[4]);
                        $('#EDIT_MToCheck').val(data[5]);
                    }
                });
                DialogEditLimitsVariations.dialog('open');
            });

            $('#btn_EditFileLocations').click(function () {
                DialogEditFileLocations.dialog('open');
            });

            $('#btn_EditSMTP').click(function () {
                $.ajax({
                    url: 'JQDataFetch/getMasterSMTPServerDetails.aspx',
                    async: false,
                    success: function (response) {
                        var data = response.split('|');
                        $("#EDIT_SMTPName").val(data[0]);
                        $("#EDIT_SMTPPort").val(data[1]);
                        $("#EDIT_SMTPEmailAddress").val(data[2]);
                        $("#EDIT_SSLEnabled option:contains("+data[3]+")").attr('selected', true);
                        $("#EDIT_SMTPUsername").val(data[4]);
                        $("#EDIT_SMTPPassword").val(data[5]);
                    }
                })
                DialogEditSMTP.dialog('open');
            });

            $('#btn_SaveSMTPEdit').click(function () {
                $.ajax({
                    url: 'Processor/SaveSMTPSettingsAdmin.aspx',
                    beforeSend: function() {
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        Name: $('#EDIT_SMTPName').val(),
                        Port: $('#EDIT_SMTPPort').val(),
                        Email: $('#EDIT_SMTPEmailAddress').val(),
                        SSL: $("#EDIT_SSLEnabled :selected").val(),
                        UName: $('#EDIT_SMTPUsername').val(),
                        PWord: $('#EDIT_SMTPPassword').val()
                    },
                    complete: function() {
                        LoadingDIV.dialog('close');
                    },
                    success: function (response) {
                        alert('SMTP Server details updated successfully');
                        DialogEditSMTP.dialog('close');
                    }
                })
            });

            $('#btn_SaveNewBankAccount').click(function () {
                var Field1 = $("#ag_add_bankname").valid();

                if (Field1 == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });

            $('#BTN_SendNewPassword_Agent').click(function () {
                $.ajax({
                    url: 'Processor/generateNewAgentPassword.aspx',
                    data: {
                        LID: $('#hidUserID').val(),
                    },
                    success: function (response) {
                        alert('New Password has been issued to the user.');
                    }
                })
            })

            $("#BTN_SendNewPassword_Admin").click(function () {
                $.ajax({
                    url: 'Processor/generateNewAgentPassword.aspx',
                    data: {
                        LID: $('#hidUserID').val(),
                    },
                    success: function (response) {
                        alert('New Password has been issued to the user.');
                    }
                })
            });

            $("#btn_TestSMTP").click(function () {
                $("#SMTP_Result_TR").show();
                $("#SMTPTestResult").text('Please wait. Testing SMTP Server settings.');
                $.ajax({
                    url: 'JQDataFetch/testSMTPServer.aspx',
                    data: {
                        SMTPServer: $('#EDIT_SMTPName').val(),
                        SMTPPort: $('#EDIT_SMTPPort').val(),
                        UName: $('#EDIT_SMTPUsername').val(),
                        PWord: $('#EDIT_SMTPPassword').val(),
                        SSL: $('#EDIT_SSLEnabled :selected').val(),
                        SendEmail: $('#EDIT_SMTPEmailAddress').val()
                    },
                    success: function (response) {
                        $('#SMTP_Result_TR').show();
                        $('#SMTPTestResult').text(response);
                    }
                })
            });
            
            $('#btn_SaveNewAgent').click(function () {

                var isField1Valid = $('#ag_add_MUName').valid();
                var ifField2Valid = $('#ag_add_MUEmail').valid();
                var isField3Value = $('#ag_add_MLName').valid();
                var isField4Value = $('#ag_hidden_EmailValid').val();

                if (isField1Valid == true && ifField2Valid == true && isField3Value == true && isField4Value == "FALSE")
                {
                    var coun = "";
                    var c = $('#ag_add_Countries').val().length;
                    if (c > 0) {
                        $.each($('#ag_add_Countries').val(), function (index, item) {
                            coun = coun + item + ",";
                        });

                        $.ajax({
                            url: 'Processor/AddAgent.aspx',
                            beforeSend: function() {
                                LoadingDIV.dialog('open');
                            },
                            data: {
                                AgName: $("#ag_add_agentname").val(),
                                AgAddLine1: $('#ag_add_addressline1').val(),
                                AgAddLine2: $('#ag_add_addressline2').val(),
                                AgSuburb: $('#ag_add_Suburb').val(),
                                AgState: $('#ag_add_State').val(),
                                AgPostcode: $('#ag_add_Postcode').val(),
                                AgPhone: $('#ag_add_Phone').val(),
                                AgFax: $('#ag_add_Fax').val(),
                                AgEmail: $('#ag_add_Email').val(),
                                AgABN: $('#ag_add_ABN').val(),
                                AgACN: $('#ag_add_ACN').val(),
                                AgFlatFee: $('#ag_add_FlatFee').val(),
                                AgAddPerc: $('#ag_add_AddPerc').val(),
                                AgCommStruc: $('#ag_add_CommStruc').val(),
                                AgCommPerc: $('#ag_add_CommPerc').val(),
                                AgCountries: coun,
                                AgMUName: $('#ag_add_MUName').val(),
                                AgMULName: $('#ag_add_MLName').val(),
                                AgMUPhone: $('#ag_add_MUPhone').val(),
                                AgMUMobile: $('#ag_add_MUMobile').val(),
                                AgMUEmail: $('#ag_add_MUEmail').val(),
                                AgBanks: $("#hidSelectedIdList").val(),
                                AgAUSRegNo: $('#ag_add_austracregno').val(),
                                AgAUSRegDate: $('#ag_add_austracregdate').val(),
                                AgAUSRegExp: $('#ag_add_austracregexpiry').val(),
                                AgMAUDLimit: $('#amount').val(),
                                AgYAUDLimit: $('#yaudtrans').val(),
                                AgMNbLimit: $('#mnbtrans').val(),
                                AgYNbLimit: $('#ynbtrans').val(),
                            },
                            complete: function() {
                                LoadingDIV.dialog('close');
                            },
                            success: function (response) {
                                alert('Agent Added Successfully');

                                //$("#errormbttomdivessagedisplay").show();
                                //$("#errormbttomdivessagedisplay").text(response);
                                //runkycChekc();
                                AgentTbl.ajax.reload();
                                DialogAddNewAgent.dialog('close');
                            }
                        });
                    }
                    else {
                        alert("Select a country");
                    }
                }


                
            })

            DialogEditUsers = $('#DIV_EditUsers').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Edit User Account",
            });

            LoadingDIV = $('#DIV_Loading').dialog({
                modal: true,
                autoOpen: false,
                width: 120,
                dialogClass: 'dialog_transparent_background',
            });

            LoadingDIV.siblings('.ui-dialog-titlebar').remove();

            DialogEditLimitsVariations = $('#div_EditLimitsVariations').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Edit Limits & Variations",
            });

            DialogEditFileLocations = $('#div_EditFileLocations').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "EDIT FILE LOCATION SETTINGS",
            });

            DialogEditSMTP = $('#div_EditSMTP').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Edit SMTP (Email) Server Address Settings",
            });

            DialogViewCountries = $('#div_ViewCountries').dialog({
                autoOpen: false,
                modal: true,
                width: 480,
                title: "View Country Information",
            });

            DialogEditBankAccount = $('#DIV_EditBankAccount').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Edit Local Bank Accounts",
            });

            DialogAddAgentCurrencies = $('#DIV_AddAgentCurrencies').dialog({
                autoOpen: false,
                modal: true,
                width: 980,
            });

            DialogEditMasterCompany = $('#DIV_Editcompany').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Edit Company Information",
            });

            DialogAddNewAgent = $('#DIV_AddNewAgent').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Add New Agent",
            });

            DialogEditAddAgent = $('#DIV_EditNewAgent').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Edit Agent",
            });

            DialogChangePassword = $('#ChangePassword').dialog({
                autoOpen: false,
                modal: true,
                width: 320,
                title: "CHANGE PASSWORD",
            });

            DialogAddNewBankAccount = $('#DIV_AddNewBankAccount').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Add New Local Bank Account",
            });


            DialogAddNewCountry = $('#DIV_AddNewCountry').dialog({
                autoOpen: false,
                modal: true,
                width: 320,
                title: "ADD NEW COUNTRY",
            });

            DialogAddNewUser = $('#DIV_AddNewUsers').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Add A New User",
            });

            $('#btn_ChangePassword').click(function () {
                DialogChangePassword.dialog('open');
            });

            $('#btn_AgentUser').click(function () {

                $.ajax({
                    url: 'JQDataFetch/getAllAgents.aspx',
                    async: false,
                    success: function (data) {
                        $('#SLT_AgentList').empty();
                        $('#SLT_AgentList').append("<option value='NOSELECT'>-- SELECT -- </option>");
                        var initialsplit = data.split('~');
                        initialsplit.sort();
                        $.each(initialsplit,
                            function (index, item) {
                                var datasplit = initialsplit[index].split('|');
                                $('#SLT_AgentList').append("<option value='" + datasplit[1] + "'>" + datasplit[0] + "</option>");
                            });

                    }
                });


                $('#DIV_AddNewAdminUser').hide();
                $('#DIV_AddNewAgentUser').show();
                $('#hidden_AccountType').val('AGENT');
            });

            $('#btn_AdminUser2').click(function () {
                $('#DIV_AddNewAdminUser').show();
                $('#DIV_AddNewAgentUser').hide();
                $('#hidden_AccountType').val('MASTER');
            });



            $('#<%=IMG_EditMasterComp.ClientID%>').click(function () {
                $.ajax({
                    url: 'JQDataFetch/getMasterCompanyInfoForEdit.aspx',
                    async: false,
                    success: function (data) {
                        var info = data.split('|');
                        $('#MCEdit_AddressLine1').val(info[0]);
                        $('#MCEdit_AddressLine2').val(info[1]);
                        $('#MCEdit_Suburb').val(info[2]);
                        $('#MCEdit_State').val(info[3]);
                        $('#MCEdit_Postcode').val(info[4]);
                        $('#MCEdit_Telephone').val(info[5]);
                        $('#MCEdit_Fax').val(info[6]);
                        $('#MCEdit_Email').val(info[7]);
                        $('#MCEdit_ABN').val(info[8]);
                        $('#MCEdit_ACN').val(info[9])
                    }
                })

                DialogEditMasterCompany.dialog('open');

            });

            $('#btn_SaveNewUser').click(function () {
                if ($('#hidden_AccountType').val() == "AGENT") {

                    var isField1Value = $('#SLT_AgentList').valid();
                    var isField2Value = $('#addnewagent_firstname').valid();
                    var isField3Value = $('#addnewagent_lastname').valid();
                    var isField4Value = $('#addnewagent_email').valid();

                    if (isField1Value == true && isField2Value == true && isField3Value == true && isField4Value == true) {
                        $.ajax({
                            url: 'Processor/AddNewAgentUser.aspx',
                            beforeSend: function () {
                                LoadingDIV.dialog('open');
                            },
                            data: {
                                AID: $('#SLT_AgentList :selected').val(),
                                FName: $('#addnewagent_firstname').val(),
                                LName: $('#addnewagent_lastname').val(),
                                Email: $('#addnewagent_email').val(),
                                UName: $('#addnewagent_username').val(),
                                Pword: $('#addnewagent_password').val(),
                                AccountType: $('#hidden_AccountType').val(),
                                CompMan: $('#CHK_ComplianceManager').is(':checked'),
                                CompManPIN: $('#addnewagent_compmanpin').val(),
                            },
                            complete: function () {
                                LoadingDIV.dialog('close');
                            },
                            success: function (response) {
                                alert('Agent added successfully');
                                DialogAddNewUser.dialog('close');
                                UserTbl.ajax.reload();
                            }
                        });
                    }
                } else {

                    var isField1Value = $('#addnewadmin_firstname').valid();
                    var isField2Value = $('#addnewadmin_lastname').valid();
                    var isField3Value = $('#addnewadmin_email').valid();

                    if (isField1Value == true && isField2Value == true && isField3Value == true) {
                        $.ajax({
                            url: 'Processor/AddNewAdminUser.aspx',
                            beforeSend: function() {
                                LoadingDIV.dialog('open');
                            },
                            data: {
                                FName: $('#addnewadmin_firstname').val(),
                                LName: $('#addnewadmin_lastname').val(),
                                Email: $('#addnewadmin_email').val(),
                                UName: $('#addnewadmin_username').val(),
                                Pword: $('#addnewadmin_password').val(),
                                AccountType: $('#hidden_AccountType').val(),
                            },
                            complete: function () {
                                LoadingDIV.dialog('close');
                            },
                            success: function (response) {
                                alert('Admin added successfully');
                                DialogAddNewUser.dialog('close');
                                UserTbl.ajax.reload();
                            }
                        });
                    }
                }
            });

            $('#btn_EditUser').click(function () {
                if ($('#hidden_AccountType').val() == "AGENT") {
                    $.ajax({
                        url: 'Processor/EditNewAgentUser.aspx',
                        beforeSend: function() {
                            LoadingDIV.dialog('open');
                        },
                        data: {
                            AID: $('#SLT_Edit_AgentList :selected').val(),
                            FName: $('#editagent_firstname').val(),
                            LName: $('#editagent_lastname').val(),
                            Email: $('#editagent_email').val(),
                            UName: $('#editagent_username').val(),
                            CompMan: $('#CHK_EditCompMan').is(':checked'),
                            CompManPIN: $('#editagent_compmanpin').val(),
                            UID:$('#hidUserID').val(),
                            AccountType: "AGENT",

                        },
                        complete: function() {
                            LoadingDIV.dialog('close');
                        },
                        success: function (response) {
                            alert('Agent updated successfully');
                            DialogEditUsers.dialog('close');
                            UserTbl.ajax.reload();
                        }
                    });
                } else {
                    $.ajax({
                        url: 'Processor/EditNewAdminUser.aspx',
                        data: {
                            FName: $('#editadmin_firstname').val(),
                            LName: $('#editadmin_lastname').val(),
                            Email: $('#editadmin_email').val(),
                            UName: $('#editadmin_username').val(),
                            UID: $('#hidUserID').val(),
                            AccountType: "MASTER",
                        },
                        success: function (response) {
                            alert('Admin updated successfully');
                            DialogEditUsers.dialog('close');
                            UserTbl.ajax.reload();
                        }
                    });
                }
            });

            $('#btn_ChangePasswordSubmit').click(function () {

                var isField1Value = $('#txt_NewPassword	').valid();
                var isField2Value = $('#txt_NewPasswordConfirm').valid();

                if (isField1Value == true && isField2Value == true)
                {
                    if ($('#txt_NewPassword').val() != $('#txt_NewPasswordConfirm').val()) {
                        alert('Passwords do not match');
                    }
                    else {
                        $.ajax({
                            url: 'JQDataFetch/verifyLoginPassword.aspx',
                            data: {
                                OP: $('#txt_CurrentPassword').val(),
                            },
                            success: function (result) {
                                if (result == 'FAIL') {
                                    alert('Old Password is incorrect');
                                }
                                else {
                                    $.ajax({
                                        url: 'Processor/saveNewPassword.aspx',
                                        beforeSend: function () {
                                            LoadingDIV.dialog('open');
                                        },
                                        data: {
                                            NP: $('#txt_NewPassword').val(),
                                        },
                                        complete: function () {
                                            LoadingDIV.dialog('close');
                                        },
                                        success: function (response) {
                                            if (response == 'OK') {
                                                alert('Password Updated Successfully');
                                                DialogChangePassword.dialog('close');
                                            }
                                            else {

                                                alert('An error has occured. Please contact your system administrator');
                                                DialogChangePassword.dialog('close');
                                            }
                                        }
                                    })
                                }
                            }
                        })
                    }
                }
                
            })

            $('#btn_UpdateLimits').click(function () {
                $.ajax({
                    url: 'Processor/UpdateLimitsAndVariation.aspx',
                    beforeSend: function () {
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        MAL: $("#EDIT_MAUDLimit").val(),
                        YAL: $("#EDIT_YAUDLimit").val(),
                        MTL: $("#EDIT_MTransLimit").val(),
                        YTL: $("#EDIT_YTransLimit").val(),
                        MV: $("#EDIT_MVariation").val(),
                        MVM: $("#EDIT_MToCheck").val(),
                    },
                    complete: function () {
                        LoadingDIV.dialog('close');
                    },
                    success: function (response) {
                        alert("Settings updated successfully.");
                        DialogEditLimitsVariations.dialog('close');
                        location.reload();
                    }
                })
            });

            $('#btn_EditMasterComp').click(function () {
                $.ajax({
                    url: 'Processor/SaveMasterCompanyEdits.aspx',
                    beforeSend: function () {
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        Info1: $("#MCEdit_AddressLine1").val(),
                        Info2: $('#MCEdit_AddressLine2').val(),
                        Info3: $('#MCEdit_Suburb').val(),
                        Info4: $('#MCEdit_State').val(),
                        Info5: $('#MCEdit_Postcode').val(),
                        Info6: $('#MCEdit_Telephone').val(),
                        Info7: $('#MCEdit_Fax').val(),
                        Info8: $('#MCEdit_Email').val(),
                        Info9: $('#MCEdit_ABN').val(),
                        Info10: $('#MCEdit_ACN').val()
                     
                    },
                    complete: function () {
                        LoadingDIV.dialog('close');
                        alert("Successfully saved!");
                    },
                    success: function () {
                        location.reload();
                    }
                })
            });

            AgentTbl = $('#tbl_Agent').DataTable({
                columns: [
                    { 'data': 'AgentID' },
                    { 'data': 'AgentName' },
                    { 'data': 'AgentSuburb' },
                    { 'data': 'AgentState' },
                    { 'data': 'AgentStatus' },
                    { 'data': 'ViewEdit' },
                ],
                "columnDefs": [
                    { className: "ali_left", "targets": [0, 1, 2] },
                    { className: "ali_right", "targets": [4] },
                    { className: "ali_center", "targets": [3] },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/AgentList.ashx',
                "order": [[0, "desc"]],
            });

            BankAccountTbl = $('#tbl_BankAccount').DataTable({
                columns: [
                    { 'data': 'BankAccountID' },
                    { 'data': 'BankName' },
                    { 'data': 'BSB' },
                    { 'data': 'AccountNumber' },
                    { 'data': 'AccountName' },
                    { 'data': 'Active' },
                    { 'data': 'ViewEdit' },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/MasterCompBankList.ashx',
                "order": [[0, "desc"]],
            });

            CountryTbl = $('#tbl_Countries').DataTable({
                columns: [
                    { 'data': 'CountryID' },
                    { 'data': 'CountryName' },
                    { 'data': 'MasterCurrencyCode' },
                    { 'data': 'MasterCurrencyName' },
                    { 'data': 'ViewEdit' },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/MasterCurrencyList.ashx',
                "order": [[0, "desc"]],
            });

            UserTbl = $('#tbl_Users').DataTable({
                columns: [
                    { 'data': 'LoginID' },
                    { 'data': 'FullName' },
                    { 'data': 'LoginType' },
                    { 'data': 'AgentName'},
                    { 'data': 'Email' },
                    { 'data': 'Active' },
                    { 'data': 'ViewEdit' },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/LoginDataHandler.ashx',
                "order": [[0, "desc"]],
            });

            $("#BTN_AddAgentCurrencies").click(function () {
                DialogAddAgentCurrencies.dialog('open');
            });

            $("#btn_AddNewUser").click(function () {
                $('#hidden_AccountType').val('MASTER');
                $("#hidUserID").val("");
                DialogAddNewUser.dialog('open');
            })

            $('#btn_AddNewAgent').click(function () {
                $("#hidEditAgentId").val("");
                $("#tbl_Agent_Banking tr").remove();
                $.ajax({
                    url: 'JQDataFetch/getDepositMethods.aspx',
                    async: false,
                    success: function (data) {
                        var splitinfo = data.split('~');
                        $.each(splitinfo, function (index, item) {
                            var spdata = splitinfo[index].split('|');
                            var row = $("<tr><td class='settings_edit_agent_bankaccs_2'><input type='checkbox' class='chk_banks' onchange=\"checkBankList('" + spdata[0] + "')\" id='" + spdata[0] + "' name='CHK_" + spdata[1] + "' value='" + spdata[0] + "' /></td><td style='width:5px;'>&nbsp;</td><td class='settings_edit_agent_bankaccs'>" + spdata[1] + "</td></tr>").appendTo('#tbl_Agent_Banking');
                            var rowx = $("<tr class='spacer5'><td>&nbsp;</td><td style='width:5px;'>&nbsp;</td><td>&nbsp;</td></tr>").appendTo('#tbl_Agent_Banking');
                        });
                    }
                });


                $('#ag_add_Countries').select2();

                DialogAddNewAgent.dialog('open');
            })
        });

      
        function checkBankList(id) {
            var ischecked = $("#" + id).is(":checked");
            var checkedIds = $("#hidSelectedIdList").val();
            if (ischecked) {
                checkedIds = checkedIds + id + ",";
            }
            else {
                checkedIds= checkedIds.replace(id, '');
            }
            var idArr = checkedIds.split(',');
            var newAr = "";

            $.each(idArr, function (key, value) {
                if (value != "") {
                    newAr = newAr + value + ",";
                }
            });
            $("#hidSelectedIdList").val(newAr);

        }

        function runkycChekc() {
           
            $.ajax({
               
                url: 'Processor/checkKYCAgent.aspx',
                beforeSend: function () {
                    LoadingDIV.dialog('open');
                },
                data: {
                    AgId: $("#hidEditAgentId").val(),
                },
                complete: function () {
                    LoadingDIV.dialog('close');
                    
                },
                success: function (response) {
                  
                    $("#errormdivessagedisplay").show();
                    $("#errormdivessagedisplay").text(response);
                   
                }
            });

        }

        function editagent() {
            if ($("#hidSelectedIdList").val() != "") {
                var coun = "";
                var c = $('#ag_edit_Countries').val().length;
                if (c > 0) {
                    $.each($('#ag_edit_Countries').val(), function (index, item) {
                        coun = coun + item + ",";
                    });

                    $.ajax({
                        url: 'Processor/EditAgent.aspx',
                        beforeSend: function () {
                            LoadingDIV.dialog('open');
                        },
                        data: {
                            AgId: $("#hidEditAgentId").val(),
                            AgName: $("#ag_edit_agentname").val(),
                            AgAddLine1: $('#ag_edit_addressline1').val(),
                            AgAddLine2: $('#ag_edit_addressline2').val(),
                            AgSuburb: $('#ag_edit_Suburb').val(),
                            AgState: $('#ag_edit_State').val(),
                            AgPostcode: $('#ag_edit_Postcode').val(),
                            AgPhone: $('#ag_edit_Phone').val(),
                            AgFax: $('#ag_edit_Fax').val(),
                            AgEmail: $('#ag_edit_Email').val(),
                            AgABN: $('#ag_edit_ABN').val(),
                            AgACN: $('#ag_edit_ACN').val(),
                            AgFlatFee: $('#ag_edit_FlatFee').val(),
                            AgAddPerc: $('#ag_edit_AddPerc').val(),
                            AgCommStruc: $('#ag_edit_CommStruc').val(),
                            AgCommPerc: $('#ag_edit_CommPerc').val(),
                            AgCountries: coun,
                            AgMUName: $('#ag_edit_MUName').val(),
                            AgMUPhone: $('#ag_edit_MUPhone').val(),
                            AgMUMobile: $('#ag_edit_MUMobile').val(),
                            AgMUEmail: $('#ag_edit_MUEmail').val(),
                            AgBanks: $("#hidSelectedIdList").val(),
                            AgAusNum: $("#edit_austracregno").val(),
                            AgAusDate: $("#edit_austracregdate").val(),
                            AgAusExpDate: $("#edit_austracregexpiry").val(),
                            MAUDLimit: $('#edit_amount').val(),
                            YAUDLimit: $('#edit_yaudtrans').val(),
                            MNbLimit: $('#edit_mnbtrans').val(),
                            YNbLimit: $('#edit_ynbtrans').val()
                        },
                        complete: function () {
                            LoadingDIV.dialog('close');
                            AgentTbl.ajax.reload();
                        },
                        success: function (response) {
                            alert('Agent Edited Successfully');
                            AgentTbl.ajax.reload();
                            DialogEditAddAgent.dialog('close');
                        }
                    });
                }
                else {
                    alert("Select a country");
                }
            }
            else {
                showerror("Invalid selection.Please select at least one account!");
            }
        }

        function saveMasterCompany() {
            if ($("#Form4").valid() && $("#ag_add_bankname").val() != '' && $("#ag_add_accountname").val() != '' && $("#ag_add_Bsb").val() != '' && $("#ag_add_Bsb2").val() != '' && $("#ag_add_Account").val() != '') {


                $.ajax({
                    url: 'Processor/AddMasterCompanyBranch.aspx',
                    beforeSend: function () {
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        id: 0,
                        bankName: $('#ag_add_bankname').val(),
                        bsb: $('#ag_add_Bsb').val() + '-' + $('#ag_add_Bsb').val(),
                        account: $('#ag_add_Account').val(),
                        accountName: $('#ag_add_accountname').val(),

                    },
                    complete: function () {
                        LoadingDIV.dialog('close');
                    },
                    success: function (response) {
                        alert('New Bank Account Successfully Created');
                        DialogAddNewBankAccount.dialog('close');
                        BankAccountTbl.ajax.reload();
                    }
                });
            }
            else {
                showerror("Please fill mandatory fields");
            }
        }
        function editeMasterCompany() {
            if ($("#FORM_EditBankAccount").valid() && $('#edit_bankname').val() != '' && $('#edit_accountname').val() != '' && $('#edit_BSB1').val() != '' && $('#edit_BSB2').val() != '' && $('#edit_AccountNumber').val() != '') {


                $.ajax({
                    url: 'Processor/AddMasterCompanyBranch.aspx',
                    beforeSend: function () {
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        id: $('#hidMasterCompanyId').val(),
                        bankName: $('#edit_bankname').val(),
                        bsb: $('#edit_BSB1').val() + '-' + $('#edit_BSB2').val(),
                        account: $('#edit_AccountNumber').val(),
                        accountName: $('#edit_accountname').val(),
                    },
                    complete: function () {
                        LoadingDIV.dialog('close');
                    },
                    success: function (response) {
                        showsuccess('Bank Account updated successfully');
                        DialogEditBankAccount.dialog('close');
                        BankAccountTbl.ajax.reload();
                    }
                });
            }
            else {
                showerror("Please fill mandatory fields");
            }
        }
    </script>

    <style type="text/css">
        .threshold_txt_box {
            font-family: 'Varela Round', sans-serif;
            font-size: 0.7rem;
            color: #666 !important;
            text-align: right;
            width: 50px;
            padding-right: 0px;
            height: 20px;
            border:0 !important;
            border-radius: 0px !important;
            background-color: #EFEFEF !important;
            font-weight: bold;
        }
        #edit_slider-range-max {
            background: #fff !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <div class="container_top_headings">
        <table class="tbl_width_1200">
            <tr style="height:70px; vertical-align:middle;">
                <td style="vertical-align:middle;"><span class="all_headings">Settings</span></td>
                <td style="vertical-align:middle; text-align:right;"><button class="btn_red" id="btn_ChangePassword">Change Password</button></td>
            </tr>
        </table>
    </div>

    <div class="container_one" style="background: #EFEFEF;">
        <table class="tbl_width_1200">
            <tr>
                <td style="width: 280px; vertical-align: top;">
                    <table style="width: 100%;" class="background_FFFFFF">
                        <tr>
                            <td style="background-image:url('../images/com_bg.jpg'); width:280px; height:150px;">
                                <table class="tbl_width_240" style="vertical-align:top;">
                                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table style="width:100%;">
                                                <tr style="height:25px;">
                                                    <td class="set_comp_heading"><asp:Label ID="MastCompName" runat="server" Text="Label"></asp:Label></td>
                                                    <td style="width:25px;" class="set_edit_details_icon"><asp:Image CssClass="xxx" ID="IMG_EditMasterComp" runat="server" ImageUrl="~/images/set_16_ia.png"  /></td>

                                                </tr>
                                            </table>
                                        </td>
                                        

                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>

                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_240">
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td><asp:Image ID="Image13" runat="server" ImageUrl="~/images/fw-logo.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </table>
                                

                                </td>
                            </tr>

                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image15" runat="server" ImageUrl="~/images/c_icon_01.png" title="ADDRESS" /></td>
                                            <td class="cus_c"><asp:Label ID="MastCompAddressLine1" runat="server" Text="Label"></asp:Label>,&nbsp;<asp:Label ID="MastCompAddressLine2" runat="server" Text="Label"></asp:Label>&nbsp;<asp:Label ID="MastCompSubStaPos" runat="server" Text="Label"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image16" runat="server" ImageUrl="~/images/c_icon_03.png" title="TELEPHONE" /></td>
                                            <td class="cus_c"><asp:Label ID="MastCompTele" runat="server" Text="Label"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image17" runat="server" ImageUrl="~/images/c_icon_06.png" title="FAX" /></td>
                                            <td class="cus_c"><asp:Label ID="MastCompFax" runat="server" Text="Label"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            
                                            <td class="cus_h"><asp:Image ID="Image18" runat="server" ImageUrl="~/images/c_icon_02.png" title="EMAIL" /></td>
                                            <td class="cus_c"><asp:Label ID="MastCompEmail" runat="server" Text="Label"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            
                                            <td class="cus_h"><asp:Image ID="Image19" runat="server" ImageUrl="~/images/abn.png" title="ABN" /></td>
                                            <td class="cus_c"><asp:Label ID="MastCompABN" runat="server" Text="Label"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            
                                            <td class="cus_h"><asp:Image ID="Image20" runat="server" ImageUrl="~/images/acn.png" title="ACN" /></td>
                                            <td class="cus_c"><asp:Label ID="MastCompACN" runat="server" Text="Label"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                      
                    </table>
                </td>
                
                <td style="width: 20px;">&nbsp;</td>

                <td style="width: 900px;">
                    <table>
                        <tr>
                            <td>
                                <table class="tbl_width_900">
                                    <tr>
                                        <td class="tr_tabs_active" id="TD_Agent" style="width:150px;"><table><tr><td style="vertical-align:middle; width:86px;">Affiliates</td><td style="width:24px;"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/agent.png" /></td></tr></table></td>
                                        <td class="tr_tabs_inactive_right" id="TD_Banking" style="width:150px;">Banking</td>
                                        <td class="tr_tabs_inactive_right" id="TD_MainTab03" style="width:150px;">Countries</td>
                                        <td class="tr_tabs_inactive_right" id="TD_MainTab04" style="width:150px;"><table><tr><td style="vertical-align:middle; width:86px;">System</td><td style="width:24px;"><asp:Image ID="Image3" runat="server" ImageUrl="~/images/user.png" /></td></tr></table></td>
                                        <td class="tr_tabs_inactive_right" id="TD_MainTab05" style="width:150px;"><table><tr><td style="vertical-align:middle; width:86px;">Users</td><td style="width:24px;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/user.png" /></td></tr></table></td>
                                        <td id="TD_MainTab06" style="width:150px;">&nbsp;</td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        
                        <tr><td style="border-top:5px solid #76D17F; background-color:#FFFFFF; height:20px;">&nbsp;</td></tr>
                                                
                        <tr class="background_FFFFFF">
                            <td>
                                <div id="AgentTab">
                                    <table class="tbl_width_860">
                                        
                                        <tr><td>&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Affiliates List</td>
                                                        <td class="ali_right"><input class="aa_btn_green" id="btn_AddNewAgent" type="button" value="Add New Affiliate" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>
                                        
                                        <tr><td style="height: 20px;">&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860" id="tbl_Agent">
                                                    <thead>
                                                        <td class="set_tbl_headings_l" style="width: 50px;">ID</td>
                                                        <td class="set_tbl_headings_l" style="width: 350px;">AFFILIATE NAME</td>
                                                        <td class="set_tbl_headings_l" style="width: 260px;">SUBURB</td>
                                                        <td class="set_tbl_headings_l" style="width: 260px;">STATE</td>
                                                        <td class="set_tbl_headings_c" style="width: 50px;">ACTIVE?</td>
                                                        <td class="set_tbl_headings_r" style="width: 50px;">EDIT/VIEW</td>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="BankingTab">
                                    <table class="tbl_width_860">
                                        <tr><td>&nbsp;</td></tr>
                                        
                                        <tr><td class="ali_right"></td></tr>
                                        
                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Local Bank Accounts</td>
                                                        <td class="ali_right"><input class="aa_btn_green" id="btn_AddNewAccount" type="button" value="Add New Local Bank Account" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                        <tr><td style="height: 20px;">&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                <table class="tbl_width_860" id="tbl_BankAccount">
                                                    <thead>
                                                        <td class="set_tbl_headings_l" style="width: 60px;">ID</td>
                                                        <td class="set_tbl_headings_l" style="width: 100px;">BANK NAME</td>
                                                        <td class="set_tbl_headings_l" style="width: 100px;">BSB</td>
                                                        <td class="set_tbl_headings_l" style="width: 100px;">ACCOUNT NUMBER</td>
                                                        <td class="set_tbl_headings_l" style="width: 100px;">ACCOUNT NAME</td>
                                                        <td class="set_tbl_headings_l" style="width: 100px;">ACTIVE?</td>
                                                        <td class="set_tbl_headings_r" style="width: 100px;">EDIT/VIEW</td>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="ThirdMainTab">
                                    <table class="tbl_width_860">
                                        <tr>
                                            <td class="ali_right">
                                               <!-- <input class="aa_btn_green" id="btn_AddNewCountry" type="button" value="Add New Country" />--></td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td class="settings_headings">Countries List</td>
                                                        <td class="ali_right" style="height:30px;">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                        <tr>
                                            <td style="height: 20px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="tbl_width_780" id="tbl_Countries">
                                                    <thead>
                                                        <td class="tbl_heading_ben_fnt_01">Country ID</td>
                                                        <td class="tbl_heading_ben_fnt">Country Name</td>
                                                        <td class="tbl_heading_ben_fnt">Master Currency Code</td>
                                                        <td class="tbl_heading_ben_fnt">Master Currency Name</td>
                                                        
                                                        <td class="tbl_heading_ben_fnt">VIEW</td>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="FourthMainTab">
                                    <table style="width:100%;">

                                        <tr><td>&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860">
                                                                <tr>
                                                                    <td class="settings_headings">Limits and Variations Settings</td>
                                                                    <td class="ali_right"><input class="aa_btn_green" id="btn_EditLimitsVariations" type="button" value="Edit Limits & Variations" /></td>
                                                                </tr>
                                                            </table></td></tr>

                                                    <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                                    <tr><td>&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr> <!-- LIMITS & VARIATIONS -->
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Monthly AUD Limit</td>
                                                                    <td class="settings_cont2"><span id="SET_MAUDLimit" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image4" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="Set the highest Monthly AUD transaction amount before it is escalated for compliance review by the RNP." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Yearly AUD Limit</td>
                                                                    <td class="settings_cont2"><span id="SET_YAUDLimit" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image5" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="Set the highest Yearly AUD transaction amount before it is escalated for compliance review by the RNP." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Monthly Number of Transaction Limit</td>
                                                                    <td class="settings_cont2"><span id="SET_MNbTransLimit" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image30" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="Set the highest Monthly Number of transaction amount before it is escalated for compliance review by the RNP." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Yearly Number of Transaction Limit</td>
                                                                    <td class="settings_cont2"><span id="SET_YNbTransLimit" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image31" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="Set the highest Yearly Number of transaction amount before it is escalated for compliance review by the RNP." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Monthly Allowed Variation</td>
                                                                    <td class="settings_cont2"><span id="SET_MDollarVariation" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image32" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="System analyses the highest remitted amount based on number of months and if it is higher than this amount, it will be flagged to Review for unsual transaction sending pattern." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Number of Months to Check Variation</td>
                                                                    <td class="settings_cont2"><span id="SET_NbMonthVariation" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image33" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="The number of months to analyse the remittance sending pattern." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Number of Months before running the check against customer</td>
                                                                    <td class="settings_cont2"><span id="SET_NbMonthsBeforeCheck" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image34" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="The number of months after the customer has joined before the check is engaged." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860"> 
                                                                <tr>
                                                                    <td class="settings_cont1">Number of Months to recheck KYC</td>
                                                                    <td class="settings_cont2"><span id="Span1" runat="server"></span></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><asp:Image ID="Image35" runat="server" ImageUrl="~/images/green_question_mark.png" Width="20" Height="20" ToolTip="The number of months before a new KYC has to be run against customer for outward transaction." /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>

                                        <tr hidden="hidden" class="aa_seperator_divider" style="background-color:#FFF;"><td>&nbsp;</td></tr>

                                        <tr style="background-color:#fff"> <!-- FILE LOCATIONS -->
                                            <td hidden="hidden">
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860">
                                                                <tr>
                                                                    <td class="settings_headings">File Location Settings</td>
                                                                    <td class="ali_right"><input class="aa_btn_green" id="btn_EditFileLocations" type="button" value="Edit File Locations" /></td>
                                                                </tr>
                                                            </table></td></tr>

                                                    <tr class="aa_seperator spacer10"><td>&nbsp;</td></tr>

                                                    <tr><td>&nbsp;</td></tr>

                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860">
                                                                <tr>
                                                                    <td>
                                                                        <table class="tbl_width_860"> 
                                                                            <tr>
                                                                                <td class="settings_cont1">IFTI File Location</td>
                                                                                <td class="settings_cont2"><span id="SET_IFITLocation" runat="server"></span></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="tbl_width_860"> 
                                                                            <tr>
                                                                                <td class="settings_cont1">SMR File Location</td>
                                                                                <td class="settings_cont2"><span id="SET_SMRLocation" runat="server"></span></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="tbl_width_860"> 
                                                                            <tr>
                                                                                <td class="settings_cont1">TTR File Location</td>
                                                                                <td class="settings_cont2"><span id="SET_TTRLocation" runat="server"></span></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr><td>&nbsp;</td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                
                                                </table>
                                            </td>
                                        </tr>

                                        <tr hidden="hidden" class="aa_seperator_divider" style="background-color:#fff;"><td>&nbsp;</td></tr>

                                        <tr style="background-color:#FFF"><td>&nbsp;</td></tr>
                                        
                                        <tr style="background-color:#FFF">
                                            <td>
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_860">
                                                                <tr>
                                                                    <td class="settings_headings">SMTP (Email) Server Address Settings</td>
                                                                    <td class="ali_right"><input class="aa_btn_green" id="btn_AddNewSMTP" type="button" value="Add New SMTP (Email) Server" /><input class="aa_btn_green" id="btn_EditSMTP" type="button" value="Edit SMTP (Email) Server" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>  
                                                        
                                                    <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                                    <tr><td>&nbsp;</td></tr>

                                                    <tr id="TR_NOSMTPDetails"><td class="settings_cont2">Currently no SMTP (Email) configuration exisit! Please click Add New SMTP (Email) Server button to configure your email settings.</td></tr>

                                                    <tr id="TR_SMTPDetails">
                                                        <td>
                                                            <table>
                                                                <tr> <!-- 01 -->
                                                                    <td>
                                                                        <table class="tbl_width_860"> 
                                                                            <tr>
                                                                                <td class="settings_cont1">SMTP Server Name/IP Address</td>
                                                                                <td class="settings_cont2" id="td_smtpservername">mail.kapruka.com.au</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 02 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">SMTP Port</td>
                                                                                <td class="settings_cont2" id="td_smtpport">232222</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 03 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">SSL Enabled?</td>
                                                                                <td class="settings_cont2" id="td_sslenabled">No</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 04 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">Username</td>
                                                                                <td class="settings_cont2" id="td_username">kapruka_smtp_user</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 05 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">Password</td>
                                                                                <td class="settings_cont2" id="td_password">************</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr> <!-- 06 -->
                                                                    <td>
                                                                        <table class="tbl_width_860">
                                                                            <tr>
                                                                                <td class="settings_cont1">Email Address</td>
                                                                                <td class="settings_cont2" id="td_emailaddress">kapruka@kapruka.com.au</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="FifthMainTab">
                                    <table class="tbl_width_860">
                                        
                                        <tr><td>&nbsp;</td></tr>

                                        


                                        <tr>
                                            <td>
                                                <table class="tbl_width_860">
                                                                <tr>
                                                                    <td class="settings_headings">KAASI User Accounts</td>
                                                                    <td class="ali_right"><input class="aa_btn_green" id="btn_AddNewUser" type="button" value="Add New User" /></td>
                                                                </tr>
                                                            </table>
                                            </td>

                                        </tr>

                                        <tr class="aa_seperator_thick spacer10"><td>&nbsp;</td></tr>

                                        <tr><td style="height: 20px;">&nbsp;</td></tr>

                                        <tr>
                                            <td>
                                                <table class="tbl_width_860" id="tbl_Users">
                                                    <thead>
                                                        <td class="tbl_heading_ben_fnt_01">Login ID</td>
                                                        <td class="tbl_heading_ben_fnt">Full Name</td>
                                                        <td class="tbl_heading_ben_fnt">Login Type</td>
                                                        <td class="tbl_heading_ben_fnt">Affiliate Name</td>
                                                        <td class="tbl_heading_ben_fnt">Email Address</td>
                                                        <td class="tbl_heading_ben_fnt">Active</td>
                                                        <td class="tbl_heading_ben_fnt">Edit/View</td>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    s</div>
                            </td>
                        </tr>

                        <tr class="background_FFFFFF" style="height:20px;"><td>&nbsp;</td></tr>

                    </table>
                </td>
            </tr>
        </table>
    </div>

<!--    <div class="container_one">
    </div> -->


    <div id="DIV_AddAgentCurrencies">
        <form id="AgentWorks" runat="server">
            <table class="tbl_width_120">
                <tr>
                    <td style="text-align: center;">
                        <asp:DropDownList ID="DDL_AAC_AgentList" runat="server"></asp:DropDownList>
                        <asp:CompareValidator ID="val14" runat="server" ControlToValidate="DDL_AAC_AgentList" ErrorMessage="Required" Operator="NotEqual" ValueToCompare="NOSELECT" ForeColor="Red" SetFocusOnError="true" />
                    </td>
                </tr>
                <tr>
                    <td id="TBL_AAC_AvailableCurr">&nbsp;

                    </td>

                </tr>
            </table>
        </form>
    </div>

    <!-- +++++++++++++++++++++++++ EDIT COMPANY START +++++++++++++++++++++++++ -->
    <div id="DIV_Editcompany">
        <form id="Form1">
            <table class="tbl_width_600">
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image21" runat="server" ImageUrl="~/images/agent.png" title="AGENT DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <table class="tbl_width_560">

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">ADDRESS LINE 1</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">ADDRESS LINE 2</td>
                                        </tr>
                                        <tr>
                                            <td style="width:270px;"><div><input class="aa_input" id="MCEdit_AddressLine1" style="width: 100%;" type="email" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:270px;"><div><input class="aa_input" id="MCEdit_AddressLine2" style="width: 100%;" type="email" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">SUBURB</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STATE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">POSTCODE</td>
                                        </tr>
                                        <tr>
                                            <td style="width:270px;"><div><input class="aa_input" id="MCEdit_Suburb" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:125px;"><div><input class="aa_input" id="MCEdit_State" style="width: 100%;text-transform: uppercase;" type="text" value=""/></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:125px;"><div><input class="aa_input" id="MCEdit_Postcode" style="width: 100%;" type="number" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">PHONE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">FAX</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">EMAIL</td>
                                        </tr>
                                        <tr>
                                            <td style="width:125px;"><div><input class="aa_input" id="MCEdit_Telephone" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:125px;"><div><input class="aa_input" id="MCEdit_Fax" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:270px;"><div><input class="aa_input" id="MCEdit_Email" style="width: 100%;" type="email" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">ABN</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">ACN</td>
                                        </tr>
                                        <tr>
                                            <td style="width:270px;"><div><input class="aa_input" id="MCEdit_ABN" style="width: 100%;" type="email" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:270px;"><div><input class="aa_input" id="MCEdit_ACN" style="width: 100%;" type="email" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                           


                            <tr style="height:20px;"><td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td class="ali_right"><input class="aa_btn_red" id="btn_EditMasterComp" type="button" value="Save" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer10"><td>&nbsp;</td>
                </tr>
            </table>
        </form>
    </div>
    <!-- +++++++++++++++++++++++++ EDIT COMPANY END +++++++++++++++++++++++++ -->

    <!-- +++++++++++++++++++++++++ ADD NEW AGENT START +++++++++++++++++++++++++ -->
    <div id="DIV_AddNewAgent">
        <form id="Form2">
            <table class="tbl_width_600">
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_AgentDetails"><asp:Image ID="Image9" runat="server" ImageUrl="~/images/agent.png" title="AGENT DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_AgentCommission"><asp:Image ID="Image8" runat="server" ImageUrl="~/images/savings.png" title="COMMISSION" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_AgentBanking"><asp:Image ID="Image10" runat="server" ImageUrl="~/images/bank.png" title="BANK DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_AgentMasterUser"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/user.png" title="MASTER USER" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_AgentLimits"><asp:Image ID="Image6" runat="server" ImageUrl="~/images/limits.png" title="AGENT LIMITS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>
                
                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <!-- TAB01 - AGENT DETAILS -->
                        <div id="DIV_AgentDetails">
                            
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">AFFILIATE (COMPANY) NAME</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="ag_add_agentname" style="width: 100%;" type="text" value="" required /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">UNIT NO</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NO *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET TYPE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_50"><div><input class="aa_input" id="ag_unitno" name="ag_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td class="style__width_10">&nbsp;</td>
                                                <td class="style__width_75"><div><input class="aa_input" id="ag_streetno" name="ag_streetno" style="width:100%;" type="text" value="" required /></div></td>
                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_290"><div><input class="aa_input" id="ag_streetname" name="ag_streetname" style="width:100%;" type="text" value="" required /></div></td>
                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                        
                                                        <select id="ag_streettype" required style="width:100%;">
                                                            <option value="">--Select--</option>
	                                                        <option value="1">Avenue</option>
	                                                        <option value="2">Access</option>
	                                                        <option value="3">Alley</option>
	                                                        <option value="4">Alleyway</option>
	                                                        <option value="5">Amble</option>
	                                                        <option value="6">Anchorage</option>
	                                                        <option value="7">Approach</option>
	                                                        <option value="8">Arcade</option>
	                                                        <option value="9">Artery</option>
	                                                        <option value="10">Boulevard</option>
	                                                        <option value="11">Bank</option>
	                                                        <option value="12">Basin</option>
	                                                        <option value="13">Beach</option>
	                                                        <option value="14">Bend</option>
	                                                        <option value="15">Block</option>
	                                                        <option value="16">Bowl</option>
	                                                        <option value="17">Brace</option>
	                                                        <option value="18">Brae</option>
	                                                        <option value="19">Break</option>
	                                                        <option value="20">Bridge</option>
	                                                        <option value="21">Broadway</option>
	                                                        <option value="22">Brow</option>
	                                                        <option value="23">Bypass</option>
	                                                        <option value="24">Byway</option>
	                                                        <option value="25">Crescent</option>
	                                                        <option value="26">Causeway</option>
	                                                        <option value="27">Centre</option>
	                                                        <option value="28">Centreway</option>
	                                                        <option value="29">Chase</option>
	                                                        <option value="30">Circle</option>
	                                                        <option value="31">Circlet</option>
	                                                        <option value="32">Circuit</option>
	                                                        <option value="33">Circus</option>
	                                                        <option value="34">Close</option>
	                                                        <option value="35">Colonnade</option>
	                                                        <option value="36">Common</option>
	                                                        <option value="37">Concourse</option>
	                                                        <option value="38">Copse</option>
	                                                        <option value="39">Corner</option>
	                                                        <option value="40">Corso</option>
	                                                        <option value="41">Court</option>
	                                                        <option value="42">Courtyard</option>
	                                                        <option value="43">Cove</option>
	                                                        <option value="44">Crossing</option>
	                                                        <option value="45">Crossroad</option>
	                                                        <option value="46">Crossway</option>
	                                                        <option value="47">Cruiseway</option>
	                                                        <option value="48">Cul-de-sac</option>
	                                                        <option value="49">Cutting</option>
	                                                        <option value="50">Drive</option>
	                                                        <option value="51">Dale</option>
	                                                        <option value="52">Dell</option>
	                                                        <option value="53">Deviation</option>
	                                                        <option value="54">Dip</option>
	                                                        <option value="55">Distributor</option>
	                                                        <option value="56">Driveway</option>
	                                                        <option value="57">Entrance</option>
	                                                        <option value="58">Edge</option>
	                                                        <option value="59">Elbow</option>
	                                                        <option value="60">End</option>
	                                                        <option value="61">Esplanade</option>
	                                                        <option value="62">Estate</option>
	                                                        <option value="63">Expressway</option>
	                                                        <option value="64">Extension</option>
	                                                        <option value="65">Fairway</option>
	                                                        <option value="66">Fire Track</option>
	                                                        <option value="67">Firetrail</option>
	                                                        <option value="68">Flat</option>
	                                                        <option value="69">Follow</option>
	                                                        <option value="70">Footway</option>
	                                                        <option value="71">Foreshore</option>
	                                                        <option value="72">Formation</option>
	                                                        <option value="73">Freeway</option>
	                                                        <option value="74">Front</option>
	                                                        <option value="75">Frontage</option>
	                                                        <option value="76">Garden</option>
	                                                        <option value="77">Gap</option>
	                                                        <option value="78">Gardens</option>
	                                                        <option value="79">Gate</option>
	                                                        <option value="80">Gates</option>
	                                                        <option value="81">Glade</option>
	                                                        <option value="82">Glen</option>
	                                                        <option value="83">Grange</option>
	                                                        <option value="84">Green</option>
	                                                        <option value="85">Ground</option>
	                                                        <option value="86">Grove</option>
	                                                        <option value="87">Gulley</option>
	                                                        <option value="88">Heights</option>
	                                                        <option value="89">Highroad</option>
	                                                        <option value="90">Highway</option>
	                                                        <option value="91">Hill</option>
	                                                        <option value="92">Interchange</option>
	                                                        <option value="93">Intersection</option>
	                                                        <option value="94">Junction</option>
	                                                        <option value="95">Key</option>
	                                                        <option value="96">Lane</option>
	                                                        <option value="97">Landing</option>
	                                                        <option value="98">Laneway</option>
	                                                        <option value="99">Lees</option>
	                                                        <option value="100">Line</option>
	                                                        <option value="101">Link</option>
	                                                        <option value="102">Little</option>
	                                                        <option value="103">Lookout</option>
	                                                        <option value="104">Loop</option>
	                                                        <option value="105">Lower</option>
	                                                        <option value="106">Mews</option>
	                                                        <option value="107">Mall</option>
	                                                        <option value="108">Meander</option>
	                                                        <option value="109">Mew</option>
	                                                        <option value="110">Mile</option>
	                                                        <option value="111">Motorway</option>
	                                                        <option value="112">Mount</option>
	                                                        <option value="113">Nook</option>
	                                                        <option value="114">Outlook</option>
	                                                        <option value="115">Place</option>
	                                                        <option value="116">Parade</option>
	                                                        <option value="117">Park</option>
	                                                        <option value="118">Parklands</option>
	                                                        <option value="119">Parkway</option>
	                                                        <option value="120">Part</option>
	                                                        <option value="121">Pass</option>
	                                                        <option value="122">Path</option>
	                                                        <option value="123">Piazza</option>
	                                                        <option value="124">Pier</option>
	                                                        <option value="125">Plateau</option>
	                                                        <option value="126">Plaza</option>
	                                                        <option value="127">Pocket</option>
	                                                        <option value="128">Point</option>
	                                                        <option value="129">Port</option>
	                                                        <option value="130">Promenade</option>
	                                                        <option value="131">Quadrant</option>
	                                                        <option value="132">Quad</option>
	                                                        <option value="133">Quadrangle</option>
	                                                        <option value="134">Quay</option>
	                                                        <option value="135">Quays</option>
	                                                        <option value="136">Road</option>
	                                                        <option value="137">Ramble</option>
	                                                        <option value="138">Ramp</option>
	                                                        <option value="139">Range</option>
	                                                        <option value="140">Reach</option>
	                                                        <option value="141">Reserve</option>
	                                                        <option value="142">Rest</option>
	                                                        <option value="143">Retreat</option>
	                                                        <option value="144">Ride</option>
	                                                        <option value="145">Ridge</option>
	                                                        <option value="146">Ridgeway</option>
	                                                        <option value="147">Right Of Way</option>
	                                                        <option value="148">Ring</option>
	                                                        <option value="149">Rise</option>
	                                                        <option value="150">River</option>
	                                                        <option value="151">Riverway</option>
	                                                        <option value="152">Riviera</option>
	                                                        <option value="153">Roads</option>
	                                                        <option value="154">Roadside</option>
	                                                        <option value="155">Roadway</option>
	                                                        <option value="156">Ronde</option>
	                                                        <option value="157">Rosebowl</option>
	                                                        <option value="158">Rotary</option>
	                                                        <option value="159">Round</option>
	                                                        <option value="160">Route</option>
	                                                        <option value="161">Row</option>
	                                                        <option value="162">Rue</option>
	                                                        <option value="163">Run</option>
	                                                        <option value="164">Street</option>
	                                                        <option value="165">Service</option>
	                                                        <option value="166">Siding</option>
	                                                        <option value="167">Slope</option>
	                                                        <option value="168">Sound</option>
	                                                        <option value="169">Spur</option>
	                                                        <option value="170">Square</option>
	                                                        <option value="171">Stairs</option>
	                                                        <option value="172">State Highway</option>
	                                                        <option value="173">Steps</option>
	                                                        <option value="174">Streets</option>
	                                                        <option value="175">Strip</option>
	                                                        <option value="176">Terrace</option>
	                                                        <option value="177">Tarnice Way</option>
	                                                        <option value="178">Thoroughfare</option>
	                                                        <option value="179">Tollway</option>
	                                                        <option value="180">Top</option>
	                                                        <option value="181">Tor</option>
	                                                        <option value="182">Towers</option>
	                                                        <option value="183">Track</option>
	                                                        <option value="184">Trail</option>
	                                                        <option value="185">Trailer</option>
	                                                        <option value="186">Triangle</option>
	                                                        <option value="187">Trunkway</option>
	                                                        <option value="188">Turn</option>
	                                                        <option value="189">Underpass</option>
	                                                        <option value="190">Upper</option>
	                                                        <option value="191">View</option>
	                                                        <option value="192">Vale</option>
	                                                        <option value="193">Viaduct</option>
	                                                        <option value="194">Villa</option>
	                                                        <option value="195">Vista</option>
	                                                        <option value="196">Way</option>
	                                                        <option value="197">Wade</option>
	                                                        <option value="198">Walk</option>
	                                                        <option value="199">Walkway</option>
	                                                        <option value="200">Wharf</option>
	                                                        <option value="201">Wynd</option>
	                                                        <option value="202">Yard</option>
	                                                        <option value="203">Domain</option>
	                                                        <option value="204">Strand</option>
	                                                        <option value="205">Boulevarde</option>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">ADDRESS LINE 1</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_addressline1" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_addressline2" style="width: 100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_Suburb" style="width: 100%;" type="text" value="" required /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_State" style="width: 100%;text-transform: uppercase;" type="text" value="" required /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_Postcode" style="width: 100%;" type="text" value="" required /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="ag_country" style="width:100%;" type="text" value="Australia" required />
                                                    
                                                    </div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">PHONE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">FAX</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">EMAIL</td>
                                            </tr>
                                            <tr>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_Phone" style="width: 100%;" type="text" value="" required /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_Fax" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_Email" style="width: 100%;" type="email" value="" required/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">ABN</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ACN</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_ABN" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_ACN" style="width: 100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font"><span id='SPN_GovernBodyName'>AUSTRAC</span> REGISTRATION NUMBER</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">REGISTERD DATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">EXPIRY DATE</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_austracregno" style="width: 270px;" type="text" value=""/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_austracregdate" style="width:125px; padding-right:0px; padding-left:5px;" type="text" value="" placeholder="DD/MM/YYYY" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_austracregexpiry" style="width:125px; padding-right:0px; padding-left:5px;" type="text" value="" placeholder="DD/MM/YYYY"/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>

                            </table>
                        </div>

                        <!-- TAB02 - AGENT COMMISSION -->
                        <div id="DIV_AgentCommission">
                            
                            <table class="tbl_width_560">

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">TRANSACTION (FLAT/MIN) FEE $</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ADDITIONAL FEE PERCENTAGE %</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_FlatFee" style="width: 100%;" type="number" value="0" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_AddPerc" style="width: 100%;" type="number" value="0" required/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">SELECT COMMISSION STRUCTURE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">COMMISSION PERCENTAGE %</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px; padding-top: 0px;">
                                                    <table style="width:270px;">
                                                        <tr>
                                                            <td class="aa_select_btn_active" id="btn_ag_ExGain">Exchange Rate Gain</td>
                                                            <td style="width:10px;">&nbsp;</td>
                                                            <td class="aa_select_btn_inactive" id="btn_ag_AUDGain">AUD Commission</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px; vertical-align:top;"><div><input class="aa_input" id="ag_add_CommPerc" style="width: 100%;" type="number" value="0" required/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>

                            </table>
                        </div>

                        <div id="DIV_AgentBanking">
                            <!-- TAB03 - AGENT BANKING -->
                            <input type="hidden" id="hidSelectedIdList" />
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560" id="tbl_Agent_Banking">
                                            <tr>
                                                <td class="aa_label_font">SELECT</td>
                                                <td style="width: 20px;">&nbsp;</td>
                                                <td class="aa_label_font">AVAILABLE BANKS/DEPOSIT METHODS</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">SELECT COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_600">
                                                    <div>
                                                        <select multiple="multiple" id="ag_add_Countries" required>
                                                            <option value="Sri Lanka">Sri Lanka</option>
                                                            <option value="India">India</option>
                                                            <option value="Pakistan">Pakistan</option>
                                                            <option value="Indonesia">Indonesia</option>
                                                            <option value="China">China</option>
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option value="Philippines">Philippines</option>
                                                           <option value="United Arab Emirates">United Arab Emirates</option>
                                                        </select>
                                                       </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height: 20px;"><td>&nbsp;</td></tr>

                            </table>
                        </div>

                        <div id="DIV_AgentMasterUser">
                            <!-- TAB04 - AGENT MASTER USER -->
                            <table class="tbl_width_560">

                                <tr class="info_bg">
                                    <td>
                                        <table class="tbl_width_520">
                                            <tr class="spacer15"><td>&nbsp;</td></tr>
                                            <tr><td class="info_font_b">INFORMATION</td></tr>
                                            <tr><td class="info_font">Master User details below will be used to create the Master User Login Account in the system for this Agent. Login details will be automatically sent to the email address provided below.</td></tr>
                                            <tr class="spacer15"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height: 20px;"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">FIRST NAME OF THE MASTER USER*</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">LAST NAME OF THE MASTER USER*</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_MUName" style="width: 100%;" type="text" value="" required /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><input class="aa_input" id="ag_add_MLName" style="width: 100%;" type="text" value="" required /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">PHONE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">MOBILE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">EMAIL*</td>
                                            </tr>
                                            <tr>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_MUPhone" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_add_MUMobile" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_add_MUEmail" style="width: 100%;" type="email" value="" required/><input class="aa_input" id="ag_hidden_EmailValid" style="width: 100%;" type="text" hidden="hidden" value="TRUE"/></div></td>
                                            </tr>
                                            <tr style="height:20px;"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </div>

                        <div id="DIV_AgentLimits">
                            <!-- TAB05 - AGENT LIMITS -->
                            <table class="tbl_width_560">
                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td colspan="3" class="aa_label_font"><label for="amount">Monthly AUD Threshold to escalate to agent compliance $:</label> <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;"></td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_290" colspan="3">
                                            <div id="slider-range-max"></div>
                                           
                                        </td>
                                    </tr>
                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td class="aa_label_font" colspan="3"><label for="amount">Yearly AUD Threshold to escalate to agent compliance:</label> <input type="text" id="yaudtrans" readonly style="border:0; color:#f6931f; font-weight:bold;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="style__width_290"><div id="slider-yaudlimit"></div></td>
                                    </tr>

                                    <tr>
                                        <td class="aa_label_font" colspan="3"><label for="amount">Monthly Nb of Transactions Threshold to escalate to agent compliance:</label> <input type="text" id="mnbtrans" readonly style="border:0; color:#f6931f; font-weight:bold;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="style__width_290"><div id="slider-mnblimit"></div></td>
                                    </tr>

                                    <tr style="height:20px;"><td>&nbsp;</td></tr>

                                    <tr>
                                        <td class="aa_label_font" colspan="3"><label for="amount">Yearly Nb of Transactions Threshold to escalate to agent compliance:</label> <input type="text" id="ynbtrans" readonly style="border:0; color:#f6931f; font-weight:bold;" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="style__width_290"><div id="slider-ynblimit"></div></td>
                                    </tr>
                                    <tr style="height:20px;"><td>&nbsp;</td></tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                        </div>



                    </td>
                </tr>

                <tr class="aa_tab_lr_brdr">
                    <td>
                        <table class="tbl_width_560">
                            <tr>
                                <td class="ali_right">
                                    <input type="text" value="FXGain" id="ag_add_CommStruc" hidden="hidden" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <div style="width:600px;display:none; vertical-align:top;" class="alert_01_red_message" id="errormbttomdivessagedisplay" ></div>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_600">
                                        <tr>
                                            <td><input class="wiz_btn_green_outline" id="BTN_ag_BackStep4" type="button" value="Back" hidden="hidden" /> <input class="wiz_btn_green_outline" id="BTN_ag_BackStep2" type="button" value="Back" hidden="hidden"/> <input class="wiz_btn_green_outline" id="BTN_ag_BackStep3" type="button" value="Back" hidden="hidden" />  <input class="wiz_btn_green_outline" id="BTN_ag_BackStep1" type="button" value="Back" hidden="hidden"/></td>
                                            <td style="text-align:right;">&nbsp;<input class="wiz_btn_green_outline" id="BTN_ag_Step4" type="button" value="Next" hidden="hidden" /> <input class="wiz_btn_green_outline" id="BTN_ag_Step3" type="button" value="Next" hidden="hidden" /> <input class="wiz_btn_green_outline" id="BTN_ag_Step2" type="button" value="Next" hidden="hidden" /> <input class="wiz_btn_green_outline" id="BTN_ag_Step1" type="button" value="Next" /> <input class="aa_btn_red" id="btn_SaveNewAgent" type="button" value="Create New Agent" hidden="hidden" /></td>
                                        </tr>
                                    </table>
                                </td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10">
                    <td>&nbsp;</td>
                </tr>
            </table>
        </form>
    </div>
    <!-- +++++++++++++++++++++++++ ADD NEW AGENT END +++++++++++++++++++++++++ -->


    <!-- *************** ADD NEW LOCAL BANK START *************** -->
    <div id="DIV_AddNewBankAccount">
        <form id="Form4">
            <table class="tbl_width_600">
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_BankDetails"><asp:Image ID="Image14" runat="server" ImageUrl="~/images/bank.png" title="BANK DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>
                
                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_BankDetails">
                            <table class="tbl_width_560">

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="aa_label_font">BANK NAME</td></tr>
                                            <tr><td><div><input class="aa_input" id="ag_add_bankname" style="width: 100%;" type="text" value="" required /></div></td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="aa_label_font">ACCOUNT NAME</td></tr>
                                            <tr><td><div><input class="aa_input" id="ag_add_accountname" style="width: 100%;" type="text" value="" required /></div></td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">BSB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ACCOUNT NUMBER</td>
                                            </tr>
                                            <tr>
                                                <td style="width:57px;"><div><input class="aa_input" id="ag_add_Bsb" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:11px; text-align:center;">-</td>
                                                <td style="width:57px;"><div><input class="aa_input" id="ag_add_Bsb2" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:415px;"><div><input class="aa_input" id="ag_add_Account" style="width: 100%;" type="number" value="" required/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </div>

                    </td>
                </tr>

                <tr style="height:20px;" class="wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr><td class="ali_right"><input class="aa_btn_red" id="btn_SaveNewBankAccount" type="button" value="Save" onclick="saveMasterCompany()" /></td></tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

            </table>
        </form>
    </div>

    <!-- *************** EDIT BANK ACCOUNT *********************** -->
        <div id="DIV_EditBankAccount">
        <form id="FORM_EditBankAccount">
            <table class="tbl_width_600">
                <tr style="height: 18px;">
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_EditBankDetails"><asp:Image ID="Image12" runat="server" ImageUrl="~/images/bank.png" title="BANK DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_EditBankDetails">
                            <input type="hidden" id="hidMasterCompanyId" />
                            <table class="tbl_width_560">

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="aa_label_font">BANK NAME</td></tr>
                                            <tr><td><div><input class="aa_input" id="edit_bankname" required style="width: 100%;" type="text" value="" /></div></td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="aa_label_font">ACCOUNT NAME</td></tr>
                                            <tr><td class="style__width_600"><div><input class="aa_input" required id="edit_accountname" style="width: 100%;" type="text" value="" /></div></td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">BSB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ACCOUNT NUMBER</td>
                                            </tr>
                                            <tr>
                                                <td style="width:57px;"><div><input class="aa_input" required id="edit_BSB1" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:11px; text-align: center;">-</td>
                                                <td style="width:57px;"><div><input class="aa_input" required id="edit_BSB2" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:415px;"><div><input class="aa_input" required id="edit_AccountNumber" style="width: 100%;" type="number" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>

                            </table>
                        </div>

                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr><td class="ali_right"><input class="aa_btn_red" id="btn_EditBankAccount" type="button" value="Update" onclick="editeMasterCompany()" /></td></tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>
            </table>
        </form>
    </div>


    <!-- *************** CHANGE PASSWORD DIV START *************** -->
    <div id="ChangePassword">
        <form id="Form3">
            <table class="tbl_width_280">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="tbl_width_280">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">CURRENT PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 280px;">
                                                <div>
                                                    <input class="aa_input" id="txt_CurrentPassword" style="width: 100%;" type="password" value="" /></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr style="height: 25px;">
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">NEW PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 280px;">
                                                <div>
                                                    <input class="aa_input" id="txt_NewPassword" style="width: 100%;" type="password" value="" required /></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10">
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">RE-ENTER NEW PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 280px;">
                                                <div>
                                                    <input class="aa_input" id="txt_NewPasswordConfirm" style="width: 100%;" type="password" value="" required /></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10">
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="ali_right">
                                                <input class="aa_btn_red" id="btn_ChangePasswordSubmit" type="button" value="Submit" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer10">
                    <td>&nbsp;</td>
                </tr>
            </table>
        </form>
    </div>

    <!-- ************* ADD NEW USERS ***************** -->
    <div id="DIV_AddNewUsers">
      
            <table class="tbl_width_600">
                <tr style="height: 18px;"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td class="wiz_tab_active" id="TD_BankDetails2"><asp:Image ID="Image29" runat="server" ImageUrl="~/images/user.png" title="MASTER USER" /></td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_AddNewAdminUser">
                            <form id="frmNewdminUser">
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_select_btn_active" id="btn_AdminUser">Admin User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_inactive" id="btn_AgentUser">Agent User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_inactive" id="btn_CustomerUser">Customer User</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">FIRST NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">LAST NAME *</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" required id="addnewadmin_firstname" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" required id="addnewadmin_lastname" style="width: 100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">EMAIL ADDRESS *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_560"><div><input class="aa_input" required id="addnewadmin_email" style="width: 100%;" type="email" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">USERNAME *</td>

                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290">
                                                    <div>
                                                        <input class="aa_input" required id="addnewadmin_username" style="width: 100%;" type="text" value="" readonly="readonly"/></div>
                                                </td>

                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_290">&nbsp; </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                                </form>
                        </div>


                        <div id="DIV_AddNewAgentUser">
                            <form id="fromNewAgentUser">
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_select_btn_inactive" id="btn_AdminUser2">Admin User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_active" id="btn_AgentUser2">Agent User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_inactive" id="btn_CustomerUser2">Customer User</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SELECT AGENT *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><select id="SLT_AgentList" required style="width:100%;"></select></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">FIRST NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">LAST NAME *</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input required class="aa_input" id="addnewagent_firstname" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input required class="aa_input" id="addnewagent_lastname" style="width: 100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">EMAIL ADDRESS *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_600">
                                                    <div>
                                                        <input class="aa_input" required id="addnewagent_email" style="width: 100%;" type="email" value="" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10">
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">USERNAME *</td>

                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input required class="aa_input" id="addnewagent_username" style="width: 100%;" type="text" value="" /></div></td>
                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_290">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">&nbsp;</td>

                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">OVERIDE PIN</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290">
                                                    <input id="CHK_ComplianceManager" type="checkbox" /> Is a compliance manager</td>
                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_290"><input class="aa_input" id="addnewagent_compmanpin" style="width: 100%;" type="number" value="" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                                </form>
                        </div>

                    </td>
                </tr>

                <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td class="ali_right"><input type="text" id="hidden_AccountType" hidden="hidden" /><input class="aa_btn_red" id="btn_SaveNewUser" type="button" value="Save" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>
            </table>
       
    </div>

    <!-- ************************ END ADD NEW USERS ***************************** -->


    <!-- ************* EDIT USERS ***************** -->
    <div id="DIV_EditUsers">
        <form id="FORM_EditUsers">
            <input type="hidden" id="hidUserID" />
            <table class="tbl_width_600">
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td class="wiz_tab_active" id="TD_Edit_BankDetails2"><asp:Image ID="Image26" runat="server" ImageUrl="~/images/user.png" title="USER DETAILS" /></td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                                <td style="width:50px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_EditAdminUser">
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_select_btn_active" id="btn_Edit_AdminUser">Admin User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_inactive" id="btn_Edit_AgentUser">Agent User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_inactive" id="btn_Edit_CustomerUser">Customer User</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">FIRST NAME</td>

                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">LAST NAME</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290">
                                                    <div>
                                                        <input class="aa_input" required id="editadmin_firstname" style="width: 100%;" type="text" value="" /></div>
                                                </td>

                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_290">
                                                    <div>
                                                        <input class="aa_input" required id="editadmin_lastname" style="width: 100%;" type="text" value="" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="style__width_560">
                                            <tr>
                                                <td class="aa_label_font">EMAIL ADDRESS</td>
                                            </tr>
                                            <tr>
                                                <td style="width:560px;"><div><input required class="aa_input" id="editadmin_email" style="width:100%;" type="email" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="style__width_560">
                                            <tr>
                                                <td class="aa_label_font">USERNAME</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input required class="aa_input" id="editadmin_username" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><input required type="button" value="Send New Password" class="aa_btn_green" style="width:100%" id="BTN_SendNewPassword_Admin" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>
                            </table>
                        </div>


                        <div id="DIV_EditAgentUser">
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_select_btn_inactive" id="btn_EditAdminUser2">Admin User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_active" id="btn_EditAgentUser2">Agent User</td>
                                                <td style="width:10px;">&nbsp;</td>
                                                <td class="aa_select_btn_inactive" id="btn_EditCustomerUser2">Customer User</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SELECT AGENT</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><select required id="SLT_Edit_AgentList" style="width:100%;"></select></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">FIRST NAME</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">LAST NAME</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input required class="aa_input" id="editagent_firstname" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input required class="aa_input" id="editagent_lastname" style="width: 100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="aa_label_font">EMAIL ADDRESS</td></tr>
                                            <tr><td style="width:560px;"><div><input required class="aa_input" id="editagent_email" style="width: 100%;" type="email" value="" /></div></td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">USERNAME</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input required class="aa_input" id="editagent_username" style="width: 100%;" type="text" value="" readonly="readonly"/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><input required type="button" value="Send New Password" class="aa_btn_green" style="width:100%" id="BTN_SendNewPassword_Agent" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="aa_label_font">&nbsp;</td></tr>
                                            <tr><td style="width:560px;">
                                                <input id="CHK_EditCompMan" type="checkbox" />Is a compliance manager OVERRIDE PIN: <input type="text" id="editagent_compmanpin" class="aa_input" /></td></tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td class="ali_right"><input type="text" id="hidden_Edit_AccountType" hidden="hidden" /><input class="aa_btn_red" id="btn_ActivateUser" type="button" value="Activate User" />&nbsp;<input class="aa_btn_red" id="btn_InactiveUser" type="button" value="Inactivate User" />&nbsp;<input class="aa_btn_red" id="btn_EditUser" type="button" value="Update" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>
            </table>
        </form>
    </div>

    <!-- ************************ END EDIT USERS ***************************** -->

    <!-- +++++++++++++++++++++++++ EDIT AGENT START +++++++++++++++++++++++++ -->
    <div id="DIV_EditNewAgent">
        <input type="hidden" id="hidEditAgentId" />
        <form id="FRM_EditAgent">
            <table class="tbl_width_600">

                <tr><td>&nbsp;</td></tr>
                 
                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_Edit_AgentDetails"><asp:Image ID="Image22" runat="server" ImageUrl="~/images/agent.png" title="AGENT DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_Edit_AgentCommission"><asp:Image ID="Image23" runat="server" ImageUrl="~/images/savings.png" title="COMMISSION" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_Edit_AgentBanking"><asp:Image ID="Image24" runat="server" ImageUrl="~/images/bank.png" title="BANK DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_Edit_AgentLimits"><asp:Image ID="Image27" runat="server" ImageUrl="~/images/limits.png" title="AGENT LIMITS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_Edit_AgentDetails">
                            <!-- TAB01 - AGENT DETAILS -->
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="aa_label_font">AGENT (COMPANY) NAME</td></tr>
                                            <tr><td style="width:560px;"><div><input class="aa_input" id="ag_edit_agentname" style="width: 100%;" type="text" value="" required/></div></td></tr>
                                            <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">ADDRESS LINE 1</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_addressline1" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_addressline2" style="width: 100%;" type="text" value=""/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_Suburb" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_edit_State" style="width: 100%;text-transform: uppercase;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_edit_Postcode" style="width: 100%;" type="text" value="" required/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">PHONE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">FAX</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">EMAIL</td>
                                            </tr>
                                            <tr>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_edit_Phone" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="ag_edit_Fax" style="width: 100%;" type="text" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_Email" style="width: 100%;" type="text" value="" required/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">ABN</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ACN</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_ABN" style="width: 100%;" type="text" value="" required/></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_ACN" style="width: 100%;" type="text" value="" required/></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">AUSTRAC REGISTRATION NUMBER</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">REGISTERD DATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">EXPIRY DATE</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="edit_austracregno" style="width: 270px;" type="text" value="" required /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="edit_austracregdate" style="width:125px; padding-right:0px; padding-left:5px;" type="text" value="" placeholder="DD/MM/YYYY" required /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:125px;"><div><input class="aa_input" id="edit_austracregexpiry" style="width:125px; padding-right:0px; padding-left:5px;" type="text" value="" placeholder="DD/MM/YYYY" required /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        
                                            <div style="width:560px; display:none; vertical-align:top;" class="ag_kyc_message" id="errormdivessagedisplay"></div>
                                                  
                                        

                                    </td></tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                            </table>
                        </div>

                        <div id="DIV_Edit_AgentCommission">
                            <!-- TAB02 - AGENT COMMISSION -->
                            <table class="tbl_width_560">

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">TRANSACTION (FLAT/MIN) FEE $</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ADDITIONAL FEE PERCENTAGE %</td>
                                            </tr>
                                            <tr>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_FlatFee" style="width: 100%;" type="number" value="" /></div></td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px;"><div><input class="aa_input" id="ag_edit_AddPerc" style="width: 100%;" type="number" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height: 25px;"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">SELECT COMMISSION STRUCTURE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">COMMISSION PERCENTAGE %</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 0px; width:270px;">
                                                    <table style="width:270px;">
                                                        <tr>
                                                            <td class="aa_select_btn_active" id="btn_ag_edit_ExGain">Exchange Rate Gain</td>
                                                            <td class="aa_select_btn_inactive" id="btn_ag_edit_AUDGain">AUD Commission</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="vertical-align: top; width:270px;"><div><input class="aa_input" id="ag_edit_CommPerc" style="width: 100%;" type="number" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height:20px;"><td>&nbsp;</td></tr>

                            </table>
                        </div>

                        <div id="DIV_Edit_AgentBanking">
                            <!-- TAB03 - AGENT BANKING -->
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560" id="tbl_Edit_Agent_Banking">
                                            <tr>
                                                <td class="aa_label_font">SELECT</td>
                                                <td style="width: 20px;">&nbsp;</td>
                                                <td class="aa_label_font">AVAILABLE BANKS/DEPOSIT METHODS</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">SELECT COUNTRIES</td>
                                            </tr>
                                            <tr>
                                                <td class="tbl_width_560">
                                                    <div>
                                                        <select multiple="multiple" id="ag_edit_Countries" class="settings_edit_agent_bankaccs_3">
                                                            <option value="Sri Lanka">Sri Lanka</option>
                                                            <option value="India">India</option>
                                                            <option value="Pakistan">Pakistan</option>
                                                            <option value="Indonesia">Indonesia</option>
                                                            <option value="China">China</option>
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option value="Philippines">Philippines</option>
                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height: 25px;">
                                    <td>&nbsp;</td>
                                </tr>



                            </table>
                        </div>

                        <div id="DIV_Edit_AgentMasterUser">
                            <!-- TAB04 - AGENT MASTER USER -->
                            <table class="tbl_width_560">

                                <tr class="info_bg">
                                    <td>
                                        <table class="tbl_width_520">
                                            <tr class="spacer15"><td>&nbsp;</td></tr>
                                            <tr><td class="info_font_b">INFORMATION</td></tr>
                                            <tr><td class="info_font">Master User details can be edited in the main Settings screen under Users tab.</td></tr>
                                            <tr class="spacer15"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="aa_info_at_top">Master User details below will be used to create the Master User Login Account in the system for this Agent. Login details will be automatically sent to the email address provided below.</td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="aa_label_font">NAME OF THE MASTER USER*</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_560">
                                                    <div>
                                                        <input class="aa_input" id="ag_edit_MUName" style="width: 100%;" type="text" value="" /><input class="aa_input" id="ag_edit_MLName" style="width: 100%;" type="text" value="" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10">
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <table class="style__width_560">
                                            <tr>
                                                <td class="aa_label_font">PHONE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">MOBILE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">EMAIL*</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_135">
                                                    <div>
                                                        <input class="aa_input" id="ag_edit_MUPhone" style="width: 100%;" type="number" value="" /></div>
                                                </td>
                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                                        <input class="aa_input" id="ag_edit_MUMobile" style="width: 100%;" type="number" value="" /></div>
                                                </td>
                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_290">
                                                    <div>
                                                        <input class="aa_input" id="ag_edit_MUEmail" style="width: 100%;" type="text" value="" /><input class="aa_input" id="ag_edit_hiddenEmail" style="width: 100%;" type="text" value="FALSE" hidden="hidden" /></div>
                                                </td>
                                            </tr>
                                            </table>
                                    </td>
                                </tr>

                            </table>
                        </div>

                        <div id="DIV_EditAgentLimits">
                            <!-- TAB05 - AGENT LIMITS -->
                            <table class="tbl_width_560">
                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="aa_label_font"><label for="amount">MONTHLY AUD THRESHOLD BEFORE ESCALATE TO AGENT COMPLIANCE</label></td>
                                                    <td style="text-align:right;"><input type="text" id="edit_amount" readonly class="threshold_txt_box"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr><td><div id="edit_slider-range-max"></div></td></tr>
                                    <tr style="height:20px;"><td>&nbsp;</td></tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="aa_label_font"><label for="amount">YEARLY AUD THRESHOLD BEFORE ESCALATE TO AGENT COMPLIANCE</label></td>
                                                    <td style="text-align:right;"><input type="text" id="edit_yaudtrans" readonly class="threshold_txt_box" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr><td><div id="edit_slider-yaudlimit"></div></td></tr>
                                    <tr style="height:20px;"><td>&nbsp;</td></tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="aa_label_font"><label for="amount">MONTHLY NUMBER OF TRANSACTIONS THRESHOLD BEFORE ESCALATE TO AGENT COMPLIANCE</label></td>
                                                    <td style="text-align:right;"><input type="text" id="edit_mnbtrans" readonly class="threshold_txt_box" /></td>
                                                    
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    
                                    <tr><td><div id="edit_slider-mnblimit"></div></td></tr>
                                    <tr style="height:20px;"><td>&nbsp;</td></tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="aa_label_font" colspan="3"><label for="amount">YEARLY NUMBER OF TRANSACTIONS THRESHOLD BEFORE ESCALATE TO AGENT COMPLIANCE</label></td>
                                                    <td style="text-align:right;"><input type="text" class="threshold_txt_box" id="edit_ynbtrans" readonly /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr><td><div id="edit_slider-ynblimit"></div></td></tr>

                                    <tr style="height:20px;"><td>&nbsp;</td></tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                        </div>



                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr hidden="hidden">
                                <td><input type="text" value="FXGain" id="ag_edit_CommStruc" hidden="hidden" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input type="button" value="Run KYC Check"   class="aa_btn_green" onclick="runkycChekc();" style="display:none;" /></td>
                                <td class="ali_right">&nbsp;<input class="aa_btn_red" id="btn_InactivateAgent" type="button" value="Inactivate Agent"  style="display:none;"/>&nbsp;<input class="aa_btn_red" id="btn_SaveEditAgent" type="button" value="Update" onclick="editagent()" /></td>
                                   
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>
            </table>
        </form>
    </div>
    <!-- +++++++++++++++++++++++++ EDIT AGENT END +++++++++++++++++++++++++ -->

    <!-- +++++++++++++++++++++++++ EDIT SYSTEM START +++++++++++++++++++++++++ -->

    <div id="div_EditLimitsVariations">
        <table class="tbl_width_600">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px; background-color:#EFEFEF; text-align:center; vertical-align:middle;"><asp:Image ID="Image25" runat="server" ImageUrl="~/images/server.png" title="SMTP SERVER SETTINGS" /></td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

            <tr class="wiz_bg_EFEFEF">
                <td>
                    <div id="DIV_blank001">
                        <table class="tbl_width_560">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">MONTHLY AUD LIMIT</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">YEARLY AUD LIMIT</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_MAUDLimit" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_YAUDLimit" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">MONTHLY TRANSACTIONS LIMIT</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">YEARLY TRANSACTIONS LIMIT</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_MTransLimit" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_YTransLimit" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">MONTHLY VARIATION</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">MONTHS TO CHECK VARIATION</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_MVariation" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_MToCheck" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>

            <tr class="wiz_bg_EFEFEF" style="height: 20px;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr><td class="ali_right"><input class="aa_btn_red" id="btn_UpdateLimits" type="button" value="Update" /></td></tr>
                    </table>
                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
    </div>

    <div id="div_EditFileLocations">
        <table class="tbl_width_640">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_640">
                        <tr>
                            <td class="aa_tab_active">FILE LOCATIONS</td>
                            <td class="aa_tab_nocontent">&nbsp;</td>
                            <td class="aa_tab_nocontent">&nbsp;</td>
                            <td class="aa_tab_nocontent">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

            <tr class="aa_tab_lr_brdr">
                <td>
                    <div id="DIV_blank004">
                        <table class="tbl_width_600">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">IFTI FILE LOCATION</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_600"><div><input class="aa_input" id="xxxxxxx" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">SMR FILE LOCATION Location</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_600"><div><input class="aa_input" id="xxxxffxjxx" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">TTR FILE LOCATION</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_600"><div><input class="aa_input" id="xxxjuxgfxxx" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>

            <tr class="aa_tab_lrb_brdr" style="height: 20px;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_640">
                        <tr><td class="ali_right"><input class="aa_btn_red" id="btn_xxxxxhgxjxjxj" type="button" value="Update" /></td></tr>
                    </table>
                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
    </div>

    <div id="div_EditSMTP">
        <table class="tbl_width_600">
            
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px; background-color:#EFEFEF; text-align:center; vertical-align:middle;"><asp:Image ID="Image7" runat="server" ImageUrl="~/images/server.png" title="SMTP SERVER SETTINGS" /></td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

            <tr>
                <td style="background-color:#EFEFEF;">
                    <div id="DIV_blank002">
                        <table class="tbl_width_560">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">SMTP SERVER NAME/IP ADDRESS</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">SMTP PORT</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_445"><div><input class="aa_input" id="EDIT_SMTPName" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input class="aa_input" id="EDIT_SMTPPort" style="width: 100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">EMAIL ADDRESS</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">SSL ENABLED?</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_445"><div><input class="aa_input" id="EDIT_SMTPEmailAddress" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_135">
                                                <div>
                                                    <select id="EDIT_SSLEnabled" required style="width:100%;">
                                                        <option value="">--Select--</option>
	                                                    <option value="1">Yes</option>
	                                                    <option value="2">No</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="aa_label_font">USERNAME</td>
                                            <td class="aa_label_font" style="width:20px;">&nbsp;</td>
                                            <td class="aa_label_font">PASSWORD</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_SMTPUsername" style="width: 100%;" type="text" value="" /></div></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td class="style__width_290"><div><input class="aa_input" id="EDIT_SMTPPassword" style="width: 100%;" type="password" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>

            <tr style="height: 20px; background-color:#EFEFEF;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

            <tr id="SMTP_Result_TR">
                <td id="SMTPTestResult">Message here</td></tr>
            
            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr>
                            <td><input class="aa_btn_green" id="btn_TestSMTP" type="button" value="Test Connection" /></td>
                            <td class="ali_right"><input class="aa_btn_red" id="btn_SaveSMTPEdit" type="button" value="Update" /></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
    </div>

    <div id="div_ViewCountries">
        <table class="tbl_width_440">
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr><td class="settings_headings" id="EDIT_CNTName"></td></tr>
            <tr><td class="aa_seperator_thick spacer5">&nbsp;</td></tr>
            <tr><td class="spacer5">&nbsp;</td></tr>
            <tr>
                <td>
                    <table class="tbl_width_440">
                        <tr>
                            <td style="width:100px; vertical-align:top;"><img id="EDIT_CFlagFile" /></td>
                            <td>
                                <table>
                                    <tr><td class="spacer5">&nbsp;</td></tr>
                                    <tr>
                                        <td class="settings_cont1" style="width:110px;">Main Currency</td>
                                        <td id="EDIT_CCode" class="settings_cont2"></td>
                                    </tr>
                                    <tr>
                                        <td class="settings_cont1">Currency Name</td>
                                        <td class="settings_cont2" id="EDIT_CName"></td>
                                    </tr>
                                    <tr>
                                        <td class="settings_cont1">Other Currencies</td>
                                        <td class="settings_cont2" id="EDIT_AddCodes"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr hidden="hidden"><td><input class="aa_btn_red" id="btn_CountriesClose" type="button" value="Close" /></td></tr>

        </table>
    </div>
        <div id="DIV_Loading"> <!-- Loading DIV -->
        <table class="tbl_width_120">
            <tr>
                <td style="text-align:center;">
                    <asp:Image ID="Image28" runat="server" ImageUrl="~/images/loading.gif" Height="60px" Width="60px" />
                </td>
            </tr>
            <tr><td class="fnt_pleasewait">Please wait...</td></tr>
        </table>
    </div>
</asp:Content>
