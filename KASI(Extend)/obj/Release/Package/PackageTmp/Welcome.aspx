﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="KASI_Extend_.Welcome" %>



<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/additional-methods.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jszip.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.print.min.js"></script>

    <script src="js/toastr.min.js"></script>
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />
    <link href="css/toastr.min.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery.jqplot.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
    <link href="css/datatables.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/buttons.dataTables.min.css" />

    <link rel="stylesheet" type="text/css" href="css/kaasi_common.css" />
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Condensed:300,400,500,600,700" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Asap+Condensed:400,500,600,700" rel="stylesheet" />

    <style>
    .wid_headings {
        font-family: 'Encode Sans Condensed', sans-serif;
        font-size: 1.5em !important;
        font-weight: 400;
        color: #666;
        vertical-align: top;
        padding-top: 20px;
    }

    .top_date {
        background-color: #7CC8D4;
        vertical-align: middle;
        color: #FFFFFF;
        text-align: right;
        padding-right: 20px;
    }
    .top_link_active {
        background-color: #A9D4A6;
        vertical-align: middle;
        color: #FFFFFF;
        text-align: center;
    }
    .top_link_inactive {
        background-color: #DCDCDC;
        vertical-align: middle;
        color: #FFFFFF;
        text-align: center;
    }
    .top_link_inactive:hover {
        background-color: #76D17F;
        cursor: pointer;
    }

    
  

        .auto-style1 {
            height: 15px;
        }

    .tab01_active {
        background-color: #FFFFFF;
        border-left: 10px solid #76D17F;
        height:100px;
    }
    .tab02_inactive {
        background-color: #DCDCDC;
        border-left: 10px solid #DCDCDC;
        height:100px;
    } 
  
    table.dataTable#TBL_Notifications {
        font-family: 'Gotham Narrow Book';
        font-size: 12px !important;
        color: #666;
        line-height: 14px;
        border-bottom: 1px solid #EFEFEF;
        }
    table.dataTable#TBL_Notifications tbody th,
    table.dataTable#TBL_Notifications tbody td {
        padding: 8px 5px 5px 5px;
        border-bottom: 1px solid #EFEFEF !important;
        background-color: #FFFFFF !important;
        vertical-align: top !important;
        }

    table.dataTable#TBL_Notifications thead th, table.dataTable#TBL_Notifications thead td {
        font-family: 'Gotham Narrow Medium' !important;
        font-weight: normal !important;
        padding: 10px 0px 5px 5px;
        border-bottom: 2px solid #EFEFEF;
        font-size: 11px !important;
        color: #666 !important;
        background-color: #EFEFEF;
    }



    .not_C01 {
        border-left: 1px solid #EFEFEF;
    }
    .not_C02 {
        
    }
    .not_C03 {
        
    }
    .not_C04 {
        
    }
    .not_C05 {
        border-right: 1px solid #EFEFEF;
        width: 40px;
        text-align: center !important;
    }
    </style>

    <script type="text/javascript">

        var dd = 300000;

        function getNbNotifications()
        {
            $.ajax({
                url: 'JQDataFetch/getNotificationCount.aspx',
                async: false,
                success: function (response) {
                    $('#SPN_NbNotifications').text(response);
                }
            });
        }


        $(document).ready(function () {

            $("#TBL_Notifications2").hide();

            getNbNotifications();


        });

        function deleteNotification(NotifID)
        {
            $.ajax({
                url: 'Processor/deleteNotification.aspx',
                data: {
                    NID: NotifID,
                },
                success: function (response) {
                    alert('Notification Deleted');
                    NotificationsTbl.ajax.reload(null, false);
                    getNbNotifications();
                }
            })
        }

        function calcNewRate(CurrencyName) {

            var BankFeedRate = $("#curMargin_" + CurrencyName).val();
            var Margin = $('#curBuy_' + CurrencyName).val();
            //$('#curBuy_' + CurrencyName).val(Margin.toFixed(4));
            var total = parseFloat(BankFeedRate) - parseFloat(Margin)
            $("#tdNewRate" + CurrencyName).val(total.toFixed(4));
        }

        function showerror(msg) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.error(msg, 'Error');

        }
        function showsuccess(msg, body) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.success(msg, body);

        }


      
      
        function ChangeCurrencyRate(inputId, rate) {
            var changedVal = $("#curBuy_" + inputId).val();
            if (!isNaN(changedVal)) {
                var newVal = Number($("#curBuy_" + inputId).val()) - Number(rate);
                $("#tdNewRate" + inputId).val(newVal);
            }

        }
        function setCurrencyRates() {
            $.ajax({
                url: 'JQDataFetch/getTradingCurencies.aspx',
                success: function (response) {
                    document.getElementById('TBL_SetExchngeRateClient').innerHTML = response;

                },
            })
        }

        function getCurrencyRatesApi() {

            $.ajax({
                type: 'POST',
                url: "Welcome.aspx/GetCurrencyRatesApi",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var currObj = response.d;
                    console.log(currObj.rates);
                }
            });
        }

        function openLoadingDIV()
        {
            LoadingDIV.dialog('open');
        }

        function closeLoadingDIV()
        {
            LoadingDIV.dialog('close');
        }
        function updateCurrency() {
            var valueList = "";
            var idlist = $("#hidCurrencyIdList").val();
            var arr = idlist.split('^');
            jQuery.each(arr, function (i, val) {
                if (val != "") {
                    var subrr = val.split('_');
                    valueList = valueList + $("#" + val).val() + "^" + subrr[1] + ",";
                }
            });


            var secondValueList = "";
            var secondIDList = $('#hidCurrencyBuyList').val();
            var secarr = secondIDList.split("^");
            jQuery.each(secarr, function (i, val) {
                if (val != "") {
                    var subrr = val.split('_');
                    secondValueList = secondValueList + $("#" + val).val() + "^" + subrr[1] + ",";
                }
            });


            $.ajax({
                url: 'Processor/UpdateMasterCompanyRateMargins.aspx',
                data: {
                    curr: valueList,
                    curr2: secondValueList,
                },
                success: function (response) {
                    if (response == '1') {
                        showsuccess('Sucessfully updated', '');
                        location.reload();
                    }
                    else {
                        var err = response.split('^');
                        showerror(err[1]);
                    }

                },
            })

        }


        $(document).ready(function () {
            AllCustTbl = $("#Tbl_AllCustomers").DataTable({});

            NotificationsTbl = $("#TBL_Notifications").DataTable({
                "lengthMenu": [[5, 10, 25], [5, 10, 25]],
                "pageLength": 5,
                columns: [
                    { 'data': 'NotificationID' },
                    { 'data': 'Type' },
                    { 'data': 'Description' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'Action' },
                ],
                "columnDefs": [
                    { className: "not_C01", "targets": [0] },
                    { className: "not_C02", "targets": [1] },
                    { className: "not_C03", "targets": [2] },
                    { className: "not_C04", "targets": [3] },
                    { className: "not_C05", "targets": [4] },
                ],
                bServerSide: true,
                sAjaxSource: 'DataHandlers/AllNotificationsDataHandler.ashx',
                "order": [[0, "desc"]]
            });

            setCurrencyRates();



            $('#SearchCustomer').autocomplete({
                source: "JQDataFetch/getCustomersForAutocomplete.aspx",
                select: function (event, ui) {
                    window.open('dashboard.aspx?custid=' + ui.item.id, '_blank');
                    $(this).val("");
                    return false;
                }
            });



            // Modal Popup for setting the exchange rate


            DIVSetExchange = $('#DIV_SetExchangeRate').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Set Exchange Rate Margins',
            });

            DIVAllExchangeRates = $('#FullExchangeRateTable').dialog({
                modal: true,
                autoOpen: false,
                width: 480,
                title: 'Current Exchange Rates',
            })

            FullCustListDialog = $("#FullCustomerListDIV").dialog({
                modal: true,
                autoOpen: false,
                width: 1500,
                title: 'View All Customers',
            });


            LoadingDIV = $('#DIV_Loading').dialog({
                modal: true,
                autoOpen: false,
                width: 120,
                dialogClass: 'dialog_transparent_background',
            });

            $('#btn_EditRate').click(function () {
                DIVSetExchange.dialog('open');
            });

            $("#btn_ShowMore").click(function () {
                DIVAllExchangeRates.dialog('open');
            })


            $("#btn_ViewFullCustList").click(function () {
                AllCustTbl.destroy();
                AllCustTbl = $("#Tbl_AllCustomers").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    columns: [
                        { 'data': 'CustomerID' },
                        { 'data': 'AgentName' },
                        { 'data': 'LastName' },
                        { 'data': 'FirstName' },
                        { 'data': 'CountryOfBirth' },
                        { 'data': 'Nationality' },
                        { 'data': 'MinorAddress' },
                        { 'data': 'Mobile' },
                        { 'data': 'EmailAddress' },
                        { 'data': 'openCust'},
                    ],
                    FixedColumns: true,
                    bServerSide: true,
                    stateSave: true,
                    "pageLength": 15,
                    sAjaxSource: 'DataHandlers/AgentAllCustomersDataHandler.ashx',
                    "order": [[0, "desc"]],
                })
                FullCustListDialog.dialog('open');
            })

            $("#TD_Dashboard").click(function () {
                $('#TBL_Notifications2').hide();
                $('#TBL_Dashboard').show();
                $('#TD_Dashboard').toggleClass('top_link_active');
                $('#TD_Notifications').toggleClass('top_link_inactive');

            });

            $('#TD_Notifications').click(function () {
                $('#TBL_Notifications2').show();
                $('#TBL_Dashboard').hide();
                $('#TD_Notifications').toggleClass('top_link_active');
                $('#TD_Dashboard').toggleClass('top_link_inactive');
            })

        });







    </script>

    


    <!-- GOOGLE CHART -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        google.charts.load('current', { 'packages': ['bar'] });
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
            var data = new google.visualization.arrayToDataTable([
                ['Move', 'Percentage'],
                ["07/07", 440],
                ["08/07", 310],
                ["09/07", 120],
                ["10/07", 520],
                ["11/07", 30],
                ["12/07", 480],
                ["13/07", 380],
            ]);

            var options = {
                width: '255',
                height: '150',
                fontSize: '8',
                legend: { position: 'none' },
                colors: ['#CCC', '#004411'],
                chart: {
                    //title: 'Chess opening moves',
                    //subtitle: 'popularity by percentage'
                },
                axes: {
                    x: {
                        0: { side: 'top', label: 'Last 7 Days...' } // Top x-axis.
                    },
                    
                },
                bar: { groupWidth: "95%" },

                // backgroundColor: '#F00',
                axisTitlesPosition: 'none',
                hAxis: {
                    textPosition: 'none',
                },
                vAxis: {
                    baselineColor: "#DCDCDC",
                    gridlines: { color: '#EFEFEF', count: '-1' },
                    textPosition: "none",
                    textStyle: { color: '#666666', fontName: 'Proxima Nova', fontSize: '9' },
                },
                
            };

            var chart = new google.charts.Bar(document.getElementById('top_x_div'));
            // Convert the Classic options to Material options.
            chart.draw(data, google.charts.Bar.convertOptions(options));
        };
    </script>


</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

</asp:content>
<asp:content id="Content3" contentplaceholderid="ContentMainSection" runat="server">
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1_welcome" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="container_btns">
        <table class="tbl_width_1200"> <!-- MAIN TABLE STARTS HERE -->
            <tr>
                <td style="width:280px; vertical-align:top;"><input style="width:280px; height:30px; vertical-align:middle !important;" id="SearchCustomer" type="text" class="aa_input" placeholder="Search Customer by ID/Name/Mobile/DOB"/></td>
                <td style="width:20px;">&nbsp;</td>
                <td style="vertical-align:top;"><button class="aa_btn_green" id="btn_ViewFullCustList" onclick="return false">View All Customers</button></td>
                <td><button class="aa_btn_green" id="btn_AddCustomer" onclick="return false" style="display:none;">Add New Customer</button><asp:Button ID="Button1" runat="server" Text="Send Email" class="aa_btn_green" OnClick="Button1_Click" style="display:none;"/></td>
                <td style="text-align:right; width:185px; vertical-align:middle;"><button class="aa_btn_red" id="btn_EditRate" onclick="return false">Set Exchange Rate Margins</button></td>
            </tr>
        </table> <!-- MAIN TABLE ENDS HERE -->
    </div>

    <div class="container_one" style="background-color:#EFEFEF;">
        <table class="tbl_width_1200">
            <tr>
                <td>
                    <table class="tbl_width_1200">
                        <tr style="height:285px;">
                            <td class="bg_FFFFFF brdr_all_DCDCDC" style="width:895px;">
                                <table class="tbl_width_855">
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td class="ad_head_welcome">NOTIFICATIONS</td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_855" id="TBL_Notifications">
				                                <thead>
                                                    <tr>
                                                        <td class="not_H01">REF</td>
							                            <td class="tbl_heading_ben_fnt">TYPE</td>
								                        <td class="tbl_heading_ben_fnt">DESCRIPTION</td>
								                        <td class="tbl_heading_ben_fnt_02">DATE</td>
                                                        <td class="tbl_heading_ben_fnt_02">ACTION</td>                                                  
                                                    </tr>
						                        </thead>
				                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                </table>
                            </td>
                            <td style="width:20px;">&nbsp;</td>
                            <td class="bg_FFFFFF brdr_all_DCDCDC" style="width:285px;">
                                <table class="tbl_width_245">
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td class="ad_head_welcome">DAILY DEPOSITS</td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <asp:Table id="BankMonies" runat="server" CssClass="tbl_width_255">
                                                <asp:TableRow></asp:TableRow>
                                            </asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table class="tbl_width_1200">
                        <tr style="height:200px;">
                            <td class="bg_FFFFFF brdr_all_DCDCDC" style="width:590px;">
                                <table class="tbl_width_550">
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td class="ad_head_welcome">EXCHANGE RATES</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_550">
                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                                <tr><td class="ad_cont_welcome_01">Current rates from Australian Dollar (AUD) to...</td></tr>
                                                <tr class="spacer10 welcome_ex_rate_003"><td>&nbsp;</td></tr>
                                                <tr><td><asp:Table ID="TBL_ExchangeRates" runat="server"></asp:Table></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_255">
                                                <tr><td class="spacer10">&nbsp;</td></tr>
                                                <tr>
                                                    <td><asp:Button ID="BTN_RefreshExchangeRate" runat="server" Text="Refresh" OnClick="RefreshAccounts_Click" UseSubmitBehavior="false" CssClass="welcome_refresh_btn" />&nbsp;<input id="btn_ShowMore" type="button" value="More" class="welcome_refresh_btn" /></td>
                                                </tr>
                                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:20px;">&nbsp;</td>
                            <td class="bg_FFFFFF brdr_all_DCDCDC" style="width:285px;">
                                <table class="tbl_width_255">
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                    <tr><td class="ad_head_welcome">TRANSACTIONS</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_255">
                                                                <tr><td class="wel_summary_main"><span id="TotalTansSPN" runat="server"></span></td></tr>
                                                                <tr><td class="wel_summary_btm">Total Transactions Today</td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="height:25px;"><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_245">
                                                                <tr>
                                                                    <td style="width:75px;" class="wel_summary_main_sml"><span id="CompTransSPN" runat="server"></span></td>
                                                                    <td style="width:10px;">&nbsp;</td>
                                                                    <td style="width:75px;" class="wel_summary_main_sml"><span id="PendTransSPN" runat="server"></span></td>
                                                                    <td style="width:10px;">&nbsp;</td>
                                                                    <td style="width:75px;" class="wel_summary_main_sml"><span id="BlockTransSPN" runat="server"></span></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_255">
                                                                <tr>
                                                                    <td style="width:75px;" class="wel_summary_btm">Completed</td>
                                                                    <td style="width:15px;" class="wel_summary_btm_2">&nbsp;</td>
                                                                    <td style="width:75px;" class="wel_summary_btm">Pending</td>
                                                                    <td style="width:15px;" class="wel_summary_btm_2">&nbsp;</td>
                                                                    <td style="width:75px;" class="wel_summary_btm">Blocked</td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                </table>
                            </td>
                            <td style="width:20px;">&nbsp;</td>
                            <td class="bg_FFFFFF brdr_all_DCDCDC" style="width:285px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>


<div class="container_one" style="background-color:#EFEFEF; display:none;">
    <table class="tbl_width_1200">
        <tr>
            <td>
                <table class="tbl_width_1200" style="display:none;">
                    <tr>
                        <td style="width:120px;">
                            <table style="width:120px;">
                                <tr><td class="tab01_active">&nbsp;</td></tr>
                                <tr><td class="tab02_inactive">&nbsp;</td></tr>
                                <tr><td style="height:100px;">&nbsp;</td></tr>
                                <tr><td style="height:100px;">&nbsp;</td></tr>
                                <tr><td style="height:100px;">&nbsp;</td></tr>
                            </table>
                        </td>
                        <td style="background-color:#FFF;">
                            <table>
                                <tr>
                                    <td>fdsfdsfds</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="spacer10"><td>&nbsp;</td></tr>
        <tr>
            <td>
                <table class="tbl_width_1200 background_FFFFFF">
                    <tr style="height:50px;">
                        
                        <td class="top_link_active" id="TD_Dashboard">
                            <table>
                                <tr style="height:50px;">
                                    <td style="width:50px;">&nbsp;</td>
                                    <td style="vertical-align:middle;">DASHBOARD</td>
                                </tr>
                            </table>
                        </td>
                        <td class="top_link_inactive" id="TD_Notifications">Notifications&nbsp;<span class="ui-li-count ui-btn-corner-all countBubl">(12)</span></td>
                        <td class="top_date">Today is <span id="SPN_TodayDate" runat="server"></span></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td class="spacer10">&nbsp;</td></tr>
    </table>

    <table class="tbl_width_1200" id="TBL_Dashboard">
        <tr>
            <td>
                <table class="tbl_width_1200">
                    <tr style="height:150px;">
                        <td style="width:293px;" class="background_FFFFFF">&nbsp;</td>
                        <td style="width:10px;">&nbsp;</td>
                        <td style="width:292px; vertical-align:top;" class="background_FFFFFF">
                            <table style="width:100%">
                                        <tr><td class="wel_widget_heading" style="height:40px;">CURRENT Exchange Rates</td></tr>
                                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                                        
                                        
                                    </table>
                        </td>
                        <td style="width:10px;">&nbsp;</td>
                        <td style="width:292px;" class="background_FFFFFF">
                            <table class="tbl_width_255">
                                <tr><td>&nbsp;</td></tr>
                                                    
                                                    
                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td>
                                                            <div id="top_x_div" style="width:255px; height: 150px;"></div>
                                                        </td>
                                                    </tr>

                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr><td>
                                               
                                                        </td></tr>
                                                </table>
                        </td>
                        <td style="width:10px;">&nbsp;</td>
                        <td style="width:293px; vertical-align:top;" class="background_FFFFFF">
                            <table class="tbl_width_293">
                                <tr><td class="wel_widget_heading" style="height:40px;">Daily Deposits</td></tr>
                                <tr>
                                    <td>
                                        <table class="tbl_width_263">
                                            <tr class="spacer15"><td>&nbsp;</td></tr>

                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>


                            
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    <table class="tbl_width_1200" id="TBL_Notifications2">
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td style="width:895px; background-color:#FFF;">
                                    <table class="tbl_width_855">
                                        <tr><td class="wid_headings">Notifications</td></tr>
                                        <tr class="spacer15"><td>&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                        <tr class="spacer15"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                                <td style="width:20px;">&nbsp;</td>
                                <td style="width:285px; background-color:#FFF;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>



                    
                </tr>
            </table>

    <table class="tbl_width_1200" hidden="hidden">
        <tr>
            <td style="width:280px; background-color:#76D17F;">
                <table class="tbl_width_280">
                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                    <tr><td class="wel_welcome_font_left"><span id="SPN_Dashboard">Dashboard</span> | <span id="SPN_Notifications">Notifications</span></td></tr>
                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                </table>
            </td>
            <td style="width:20px; background-color:#FFFFFF;">&nbsp;</td>
            <td style="width:900px; background-color:#FFFFFF;">
                <table class="tbl_width_900">
                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                    <tr><td class="wel_welcome_font">Welcome back to KAASI, <span id="SPN_LoggedUser" style="font-weight:600;" runat="server"></span>..!</td></tr>
                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:280px; background-color:#D6DFDE;">
                <table class="tbl_width_280">
                    <tr><td>&nbsp;</td></tr>
                </table>
            </td>
            <td style="width:20px; background-color:#EFEFEF;">&nbsp;</td>
            <td style="width:900px; background-color:#FFFFFF;">
                <table class="tbl_width_900">
                    <tr style="background-color:#EFEFEF; height:20px;"><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        </table>

        <div id="DIV_mainstats" class="front">
            <table class="tbl_width_1200">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="tbl_width_1200" hidden="hidden">
                            <tr>
                                <td style="width:280px; background-color:#D6DFDE;">
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td>
                                                <table class="tbl_width_240">
                                                    <tr><td class="wel_welcome_notifications_heading"><span id="SPN_NbNotifications"></span> Notifications</td></tr>
                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr><td class="wel_welcome_exrate_msg">
                                                        <asp:Table ID="TBL_Notification" runat="server"></asp:Table>
                                                        </td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width:20px; background-color:#EFEFEF;">&nbsp;</td>
                                <td style="width:900px; background-color:#FFFFFF;">
                        <table class="tbl_width_900" style="height:300px;">
                            <tr>
                                <td style="width:296px; height:150px;">
                                    
                                </td>
                                <td style="width:6px; background-color:#EFEFEF;">&nbsp;</td>
                                <td style="width:296px;">
                                    <table style="width:100%">
                                        <tr><td class="wel_widget_heading">Daily Transactions</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="background-color:#EFEFEF;" class="auto-style1">&nbsp;</td>
                                <td style="width:296px;">
                                    <table style="width:100%">
                                        <tr><td class="wel_widget_heading">Daily Deposits</td></tr>
                                        <tr class="spacer15"><td></td></tr>
                                        
                                        <tr>
                                            <td>
                                                <table class="tbl_width_255">
                                                    <tr><td class="spacer10">&nbsp;</td></tr>
                                                    <tr>
                                                        <td><asp:Button ID="RefreshAccounts" runat="server" Text="Refresh" OnClick="RefreshAccounts_Click" UseSubmitBehavior="false" CssClass="welcome_refresh_btn" /></td>
                                                    </tr>
                                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>


                
            </table>
        </div>
    

            
        </div>


    <table>
        <tr><td>&nbsp;</td></tr>
    </table>



            

    





    





    </form>


    <!-- SET EXCHANGE RATE START -->
    <div id="DIV_SetExchangeRate">

        <table class="tbl_width_600">

            <tr><td>&nbsp;</td></tr>
            
            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td class="wiz_tab_active"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/exchange.png" title="EXCHANGE RATES" /></td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td style="height: 20px; background-color:#EFEFEF;">&nbsp;</td></tr>

            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">
                        <tr class="background_FFFFFF">
                            <td>
                                <table class="tbl_width_520">
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="trn_notes_popup_details" style="font-size:0.7em; color:#999;"><span style="color: rgb(153, 153, 153); font-family: &quot;Varela Round&quot;, sans-serif; font-size: 11.2px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">You can set an Exchange Rate Margin on top of the Bank Feed below. This will not affect the Agents setup under AUD Commision structure. The New Rate will be the only visible rate&nbsp; to your Agents.</span></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_280">
                        <tr><td>&nbsp;</td></tr>
                        <tr><td><div id="TBL_SetExchngeRateClient" >Exchange Rate Table</div></td></tr>
                        
                    </table>
                </td>
            </tr>

            <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

            <tr><td><div style="width:100%; text-align:right;"><input type="button" value="Save Margins" class="aa_btn_red" onclick="updateCurrency()" /></div></td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>
    </div>
    <!-- SET EXCHANGE RATE END -->


    <div id="DIV_Loading"> <!-- Loading DIV -->
		<table class="tbl_width_120">
			<tr>
				<td style="text-align:center;">
					<asp:Image ID="Image3" runat="server" ImageUrl="~/images/loading.gif" Height="60px" Width="60px" />
				</td>
			</tr>
			<tr><td class="fnt_pleasewait">Please wait...</td></tr>
		</table>
	</div>

    <div id="FullCustomerListDIV">
        <table class="tbl_width_1160">
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table id="Tbl_AllCustomers" class="display nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <td>Customer ID</td>
                                <td>Agent</td>
                                <td>Last Name</td>
                                <td>First Name(s)</td>
                                <td>Country of Birth</td>
                                <td>Nationality</td>
                                <td>Address</td>
                                <td>Mobile</td>
                                <td>Email Address</td>
                                <td>Open</td>
                            </tr>
                        </thead>
                    </table>
                </td>
            </tr>
        </table>


        
    </div>

    <div id="FullExchangeRateTable">
        <table class="tbl_width_440">
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table class="tbl_width_440">
                        <tr><td class="wel_welcome_exrate_msg">Current rates from Australian Dollar (AUD) to...</td></tr>
                        <tr class="spacer10 welcome_ex_rate_003"><td>&nbsp;</td></tr>
                        <tr><td><asp:Table ID="TBL_AllExchangeRates" runat="server"></asp:Table></td></tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>


    
</asp:content>
