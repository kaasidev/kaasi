﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentControl/Initial.Master" AutoEventWireup="true" CodeBehind="ReportsW.aspx.cs" Inherits="KASI_Extend_.ReportsW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 183px;
        }
        .reports_white_box {
            background-color: #FFFFFF;
            height: 50px;
            width: 185px;
            border: 1px solid #DCDCDC;
        }
        .reports_white_box_none {
            background-color: #EFEFEF;
            height: 50px;
            width: 185px;
            border: 1px solid #EFEFEF;
        }
        .out_rep_btn {
            background-color: #01bf49;
            width: 100%;
            color: #FFFFFF;
            font-family: 'Varela Round', sans-serif !important;
            font-size: 13px !important;
            height: 45px;
            margin-bottom: 0px !important;
            border-radius: 0 !important;
            border: 5px solid #FFFFFF;
        }
        .out_rep_btn:hover {
            cursor: pointer;
            background-color: #02CC4F;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
      <script type="text/javascript">
    
        function showAustractFileConfirm() {
            var con = confirm("Do you want to create AUSTRACT file?It might take some time.");
            if (con) {
                $("[id$=loadMask]").show();
                $("[id$=loadImg]").show();
                return true;
            }
            else {
                return false;
            }

          }
          </script>
    <form id="form1" runat="server">
         <div runat="server" id="loadMask" style="width:100%;height:100%;background-color:white;opacity:0.5;z-index:99999;position:fixed;display:none"></div>
    <img runat="server" id="loadImg"  style="z-index:999999;top:50%;left:50%;position:fixed;display:none" src="images/loading.gif" />

        <div class="container_top_headings">
            <table class="tbl_width_1200">
                <tr style="height:70px; vertical-align:middle;">
                    <td style="vertical-align:middle;"><span class="all_headings">Reports</span></td>
                    <td style="vertical-align:middle; text-align:right;"><asp:Button CssClass="aa_btn_red" runat="server" ID="btnCreateAUSFile" Text="Create Austrac IFTI File" OnClick="btnCreateAUSFile_Click" OnClientClick="return showAustractFileConfirm()" /></td>
                </tr>
            </table>
        </div>

        <div class="container_one">
            <table class="tbl_width_1200">
                <tr>
                    <td class="reports_white_box" style="vertical-align:top;">
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td style=" height:120px; text-align:center;"><asp:Image ID="Image7" runat="server" ImageUrl="~/images/document.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td class="rep_description">List of Transactions Report</td></tr>
                                        <tr><td class="rep_description">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td><asp:Button ID="Button7" runat="server" Text="SELECT" CssClass="out_rep_btn" OnClick="Button7_Click" /></td></tr>
                        </table>
                    </td>
                    
                    <td style="width:18px;">&nbsp;</td>
                     
                    <td class="reports_white_box" style="vertical-align:top;">
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td style=" height:120px; text-align:center;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/analyticss.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td class="rep_description">List of Transactions By Bank Report</td></tr>
                                        <tr><td class="rep_description">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td><asp:Button ID="Button1" runat="server" Text="SELECT" CssClass="out_rep_btn" OnClick="Button1_Click1" /></td></tr>
                        </table>
                    </td>
                 
                    <td style="width:18px;">&nbsp;</td> <!-- SEPERATOR 06 -->
                    
                    <td class="reports_white_box" style="vertical-align:top;">
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                        <tr><td style="text-align:center;"><asp:Image ID="Image3" runat="server" ImageUrl="~/images/exchanges.png" /></td></tr>
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                        <tr><td class="in_rep_heading">Customer Credits & Debits</td></tr>
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td><asp:Button ID="Button2" runat="server" Text="SELECT" CssClass="out_rep_btn" OnClick="Button2_Click2" /></td></tr>
                        </table>
                    </td>

                    <td style="width:18px;">&nbsp;</td>
                     
                    <td class="reports_white_box" style="vertical-align:top;">
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <table class="tbl_width_160"> 
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                        <tr><td style="text-align:center;"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/dollars.png" /></td></tr>
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                        <tr><td class="in_rep_heading">Bank Rate Average Report</td></tr>
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td><asp:Button ID="Button3" runat="server" Text="SELECT" CssClass="out_rep_btn" OnClick="Button3_Click1"  /></td></tr>
                        </table>
                    </td>
                    
                    <td style="width:18px;">&nbsp;</td>
                     
                    <td class="reports_white_box_none" style="vertical-align:top;">
                        <table style="width:100%">
                            <tr>
                                <td>
                                    <table class="tbl_width_160"> 
                                        <tr><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    
                    <td style="width:18px;">&nbsp;</td>
                     
                    <td class="reports_white_box_none" style="vertical-align:top;">
                        <table style="width:100%">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
        </div>

    </form>

</asp:Content>
