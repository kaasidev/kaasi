﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="BankingDash.aspx.cs" Inherits="KASI_Extend_.BankingDash" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
    
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />

    <style>
        .stats_box {
  display: inline-block;
  list-style: none outside none;
  margin-left: 0;
  margin-top: 20px;
  padding: 0;
}
.stats_box li {
  background: #EEEEEE;
  box-shadow: 0 0 0 1px #F8F8F8 inset, 0 0 0 1px #CCCCCC;
  display: inline-block;
  line-height: 18px;
  margin: 0 10px 10px;
  padding: 0 10px;
  text-shadow: 0 1px 0 rgba(255, 255, 255, 0.6);
  float: left;
}
.stats_box .stat_text {
  float: left;
  font-size: 12px;
  padding: 9px 10px 7px 0;
  text-align: left;
  min-width: 150px;
  position: relative;
}
.stats_box .stat_text strong {
  display: block;
  font-size: 16px;
}
.stats_box .stat_text .percent {
  color: #444;
  float: right;
  font-size: 20px;
  font-weight: bold;
  position: absolute;
  right: 0;
  top: 17px;
}
.stats_box .stat_text .percent.up {
  color: #46a546;
}
.stats_box .stat_text .percent.down {
  color: #C52F61;
}
    </style>

    <script type="text/javascript">


        $(document).ready(function () {

            
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">
        <ul class="stats_box">
	<li>
	    <div class="sparkline bar_week"></div>
	    <div class="stat_text">
		<strong>2.345</strong>Weekly Visit
		<span class="percent down"> <i class="fa fa-caret-down"></i> -16%</span>
	    </div>
	</li>
	<li>
	    <div class="sparkline line_day"></div>
	    <div class="stat_text">
		<strong>165</strong>Daily Visit
		<span class="percent up"> <i class="fa fa-caret-up"></i> +23%</span>
	    </div>
	</li>
	<li>
	    <div class="sparkline pie_week"></div>
	    <div class="stat_text">
		<strong>$2 345.00</strong>Weekly Sale
		<span class="percent"> 0%</span>
	    </div>
	</li>
	<li>
	    <div class="sparkline stacked_month"></div>
	    <div class="stat_text">
		<strong>$678.00</strong>Monthly Sale
		<span class="percent down"> <i class="fa fa-caret-down"></i> -10%</span>
	    </div>
	</li>
    </ul>
    
    </form>

</asp:Content>
