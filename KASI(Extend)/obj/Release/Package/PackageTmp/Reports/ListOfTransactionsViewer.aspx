﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportsNav.Master" AutoEventWireup="true" CodeBehind="ListOfTransactionsViewer.aspx.cs" Inherits="KASI_Extend_.Reports.ListOfTransactionsViewer" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

<header>
    
    <style>
        element.style {
            border-bottom: 10px solid #F00 !important;
        }
        /*input[type="image"]{
            padding:0px !important;
        }*/
        .ui-accordion .ui-accordion-header {
            font-size: 0.7em !important;
        }
        .ui-accordion-header-active {
            background-color: #EFEFEF !important;
            color: #666;
            font-weight: bold !important;
            font-size: 0.7em !important;
            border: 1px solid #CCC;
        }
         input[type="image"]{
                  padding:0px !important;
                    }
        
    </style>
   

       <script type="text/javascript">
           $(document).ready(function () {
               $("#accordion").accordion({
                   collapsible: true,
                   active: false,
               });

               $("#slider-range").slider({
                   range: true,
                   min: 0,
                   max: $('#<%=TXT_MaxTransVal.ClientID%>').val(),
                   step: 100,
                   values: [0, $('#<%=TXT_MaxTransVal.ClientID%>').val()],
                   slide: function (event, ui) {
                       $("#<%=amount.ClientID%>").val("$" + ui.values[0] + " - $" + ui.values[1]);
                   }
               });
               $("#<%=amount.ClientID%>").val("$" + $("#slider-range").slider("values", 0) +
                   " - $" + $("#slider-range").slider("values", 1));
           })
       </script>
      
   </header>

    <form id="form1" runat="server">

    <div class="container_top_headings">
        <table class="tbl_width_1200">
            <tr style="height:70px; vertical-align:middle;">
                <td style="vertical-align:middle;"><span class="all_headings">All Transactions Report</span></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="container_one no_padding_btm wiz_bg_EFEFEF">
        <table class="tbl_width_1200" >
            <tr>
                <td class="background_FFFFFF">
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_1160">
                                    <tr><td><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></td></tr>
                                    <tr>
                                        <td style="width:150px;"><asp:TextBox ID="FromDate" CssClass="aa_input datePickerReport" runat="server" style="width:100%;" placeholder="From (dd/mm/yyyy)"></asp:TextBox></td>
                                        <td style="width:10px;">&nbsp;</td>
                                        <td style="width:150px;"><asp:TextBox ID="EndDate" CssClass="aa_input datePickerReport" runat="server" placeholder="To (dd/mm/yyyy)"></asp:TextBox>
                                            
                                            
                                            
                                        </td>
                                        <td style="width:20px;">
                                        <td style="vertical-align:top;"><asp:DropDownList ID="DropDownList1"  runat="server" CssClass="rep_dropdown "></asp:DropDownList></td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:right;">
                                            <asp:TextBox ID="TXT_MaxTransVal" runat="server"></asp:TextBox><asp:Button ID="btnViewReport" style="padding-left:20px;" runat="server" Text="Run Report" class="aa_btn_green" OnClick="Button1_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>

                        <tr id="additionalsearch">
                            <td style="height:20px;">
                                <table class="tbl_width_1160">
                                    <tr>
                                        <td>
                                            <div id="accordion">
                                                <table>
                                                    <tr>
                                                        <td>ADVANCED SEARCH FILTERS</td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td style="width:150px;">
                                                                <label for="amount">AUD Transaction Value:</label>
                                                                <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;" runat="server">
                                                                <div id="slider-range"></div>
                                                            </td>
                                                            <td style="width:10px;">&nbsp;</td>
                                                            <td style="width:150px;">&nbsp;  
                                                            </td>
                                                            <td style="width:20px;">
                                                            <td style="vertical-align:top;">&nbsp;</td>
                                                            <td>STATUS<br />
                                                                <select id="SLT_Status" multiple="multiple">
                                                                    <option value="PENDING">PENDING</option>
                                                                    <option value="AWAITING TRANSER">AWAITING TRANSFER</option>
                                                                    <option value="SUCCESSFUL">SUCCESSFUL</option>
                                                                    <option value="REVIEW">REVIEW</option>
                                                                    <option value="MASTER REVIEW">MASTER REVIEW</option>
                                                                    <option value="CANCELLED">CANCELLED</option>

                                                                </select>
                                                            </td>
                                                            <td style="text-align:right;">&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>

                                                            
                                                        </tr>
                                                        <tr><td>&nbsp;</td></tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr><td style="height:20px;">&nbsp;</td></tr>


                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="container_one no_padding_top_btm wiz_bg_EFEFEF">
        <table class="tbl_width_1200">
            <tr style="height:20px;"><td>&nbsp;</td></tr>
            <tr style="background-color:#FFFFFF;">
                <td>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" AsyncRendering="false" SizeToReportContent="True" Width="100%" Height="500px" Font-Bold="True">
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>
        

        
    </form>
</asp:Content>

