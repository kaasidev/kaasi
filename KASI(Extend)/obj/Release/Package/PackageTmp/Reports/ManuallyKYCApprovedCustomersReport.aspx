﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportsNav.Master" AutoEventWireup="true" CodeBehind="ManuallyKYCApprovedCustomersReport.aspx.cs" Inherits="KASI_Extend_.Reports.ManuallyKYCApprovedCustomersReport" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700" rel="stylesheet" />

    <style>
              
                input[type="image"]{
                  padding:0px !important;
                    }
            </style>
  
    <form id="form1" runat="server">

    <div class="container_top_headings">
        <table class="tbl_width_1200">
            <tr style="height:70px; vertical-align:middle;">
                <td style="vertical-align:middle;"><span class="all_headings">Manually KYC Approved Customers Report</span></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="container_one no_padding_btm wiz_bg_EFEFEF">
        <table class="tbl_width_1200" >
            <tr>
                <td class="background_FFFFFF">
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_1160">
                                    <tr><td><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></td></tr>
                                    <tr>
                                        <td style="width:150px;"><asp:TextBox ID="FromDate" CssClass="aa_input datePickerReport" runat="server" placeholder="From (dd/mm/yyyy)"></asp:TextBox></td>
                                        <td style="width:10px;">&nbsp;</td>
                                        <td style="width:150px;"><asp:TextBox ID="EndDate" CssClass="aa_input datePickerReport" runat="server" placeholder="To (dd/mm/yyyy)"></asp:TextBox></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="vertical-align:top;"><asp:DropDownList ID="DropDownList1"  runat="server" CssClass="rep_dropdown "></asp:DropDownList></td>
                                        <td style="text-align:right;"><asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="aa_btn_green" OnClick="Button1_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="container_one no_padding_top_btm wiz_bg_EFEFEF">
        <table class="tbl_width_1200" >
            <tr class="spacer10"><td>&nbsp;</td></tr>
            <tr style="background-color:#FFFFFF;">
                <td>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" AsyncRendering="false" SizeToReportContent="True" Width="100%" Height="500px">
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>
        

        
    </form>
</asp:Content>
