﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceReviewTransactionsReport.aspx.cs" MasterPageFile="~/ReportsNav.Master" Inherits="KASI_Extend_.Reports.ComplianceReviewTransactionsReport" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    
<style>

    input[type="image"] {
        padding: 0px !important;
    }

    #ctl00_ContentMainSection_ReportViewer1_ctl05 {
        /*border-bottom: 5px solid red !important;*/
        width: 1160px !important;
    }

    #ctl00_ContentMainSection_ReportViewer1_ctl05_ctl00_CurrentPage {
        height: 25px;
        width: 25px;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }

    #ctl00_ContentMainSection_ReportViewer1_ctl05_ctl00_First {
        
        margin-top: 10px;
    }

    input {
        background-color: #EFEFEF;
    }

    .custom_input_css {
        background-color: #FFF;
    }

    .rw_custom_css {
        
    }

</style>
  
    <form id="form1" runat="server">

    <div class="container_top_headings">
        <table class="tbl_width_1200">
            <tr style="height:70px; vertical-align:middle;">
                <td style="vertical-align:middle;"><span class="all_headings">Compliance Review Transactions Report</span></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="container_one no_padding_btm wiz_bg_EFEFEF">
        <table class="tbl_width_1200" >
            <tr>
                <td class="background_FFFFFF">
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_1160">
                                    <tr><td><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></td></tr>
                                    <tr>
                                        <td style="width:150px;"><asp:TextBox ID="FromDate" CssClass="aa_input datePickerReport custom_input_css" runat="server" placeholder="From (dd/mm/yyyy)"></asp:TextBox></td>
                                        <td style="width:10px;">&nbsp;</td>
                                        <td style="width:150px;"><asp:TextBox ID="EndDate" CssClass="aa_input datePickerReport custom_input_css" style="width:100%;" runat="server" placeholder="To (dd/mm/yyyy)"></asp:TextBox></td>
                                        <td style="width:20px;">&nbsp;</td>
                                        <td style="vertical-align:top;"><asp:DropDownList ID="DropDownList1"  runat="server" CssClass="rep_dropdown "></asp:DropDownList></td>
                                        <td style="text-align:right;"><asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="aa_btn_green" OnClick="Button1_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="container_one no_padding_top_btm wiz_bg_EFEFEF">
        <table class="tbl_width_1200" >
            <tr style="height:20px;"><td>&nbsp;</td></tr>
            <tr style="background-color:#FFFFFF;">
                <td>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="rw_custom_css" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" AsyncRendering="false" SizeToReportContent="True" Width="100%" Height="500px">
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>
        

        
    </form>
</asp:Content>
