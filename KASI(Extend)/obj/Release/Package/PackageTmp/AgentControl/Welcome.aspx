﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentControl/Agent.Master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="KASI_Extend.AgentControl.Welcome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- CSS FILES -->
    <link href="../css/jquery-ui.min.css" rel="stylesheet" />
    <link href="../css/datatables.min.transactions.css" rel="stylesheet" />


    <style type="text/css">
        body {
            color: #666666;
        }


        #gridDivSuccess, #gridDivUnSuccess {
            max-width: 70%;
            margin: 0 auto;
            width: 70%;
            min-height: 150px;
            text-align: center;
            font-weight: bold;
        }
        
        .Grid {
            
        }
        .Grid th {
            font-family: 'Gotham Narrow Medium';
            font-size: 13px;
            padding: 10px 5px 10px 5px;
            color: #fff;
            background-color: #048ABB;
            border-bottom: 5px solid #2A7AAF;
            font-weight: normal !important;
            line-height: 14px !important;
        }
        /* CSS to change the GridLines color */
        .Grid td {
            border-bottom: 1px solid #EFEFEF;
            font-family: 'Gotham Narrow Book';
            padding-top: 6px;
            padding-bottom: 6px;
            font-size: 13px;
        }

        .modal { background: rgba(000, 000, 000, 0.8); min-height:1000000px; }

        #drop_zone {
            font-family: 'Gotham Narrow Book' !important;
            max-width: 100%;
            margin: 0 auto;
            width: 100%;
            min-height: 150px;
            text-align: center;
            font-size: 14px;
            border: 1px dashed #999;
            height: 150px;
            background-color: #FFFFFF;
            border-radius: 10px;
            color: #BBB;
        }

         #spanCon{
            font-family: 'Gotham Narrow Medium' !important;
            color: #048ABB;
            font-size: 18px;
            display: block;
            vertical-align: middle;
            line-height: normal;
            margin-top: 50px;
            text-align: center;
        }
        .in_btn_red {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 0.7em !important;
            color: #fff;
            border-radius: 3px;
            padding-top: 6px;
            padding-bottom: 7px;
            padding-left: 20px;
            padding-right: 20px;
            background-color: #e74c3c;
            border-color: #e74c3c;
            margin: 0 !important;
            vertical-align: middle;
        }
        .in_btn_red:hover {
            border-color: #EA6557;
            cursor: pointer;
            background-color: #EA6557 !important;
        }
        .in_btn_blue {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 0.7em !important;
            color: #fff;
            border-radius: 3px;
            background-color: #3498db;
            border-color: #3498db;
            vertical-align: middle;
            margin: 0 !important;
            padding-top: 6px;
            padding-bottom: 7px;
            padding-left: 20px;
            padding-right: 20px;
        }
        .in_btn_blue:hover {
            background-color: #4EA5E0;
            border-color: #4EA5E0;
            cursor: pointer;
        }

        .rep_head_left {
            padding-left: 10px !important;
            text-align: left;
            border-right: 1px solid #0380ad;
            border-top: 1px solid #048ABB;
        }
        .rep_head_right {
            padding-right: 10px !important;
            text-align: right;
            border-right: 1px solid #0380ad;
            border-top: 1px solid #048ABB;
        }
        .rep_head_center {
            text-align: center;
            border-right: 1px solid #0380ad;
            border-top: 1px solid #048ABB;
        }

        .FA_CL_01 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            border-left: 1px solid #EFEFEF;
            width:5%;
        }
        .FA_CL_02 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            width:20%;
        }
        .FA_CL_03 {
            
            color: #666666 !important;
            letter-spacing: -0.5px;
            text-align: left !important;
            border-right: 1px solid #EFEFEF;
            width:75%;
            padding-left: 10px;
        }
        .SU_CL_03 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            width:5%;
            padding-left: 10px;
            padding-right: 10px;
        }
        .SU_CL_04 {
            text-align: left !important;
            border-right: 1px solid #EFEFEF;
            padding-left: 10px;
            }
        .SU_CL_05 {
            text-align: left !important;
            border-right: 1px solid #EFEFEF;
            padding-left: 10px;
            }
                .SU_CL_06 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            padding-right: 10px;
            padding-left: 10px;
            }
                                .SU_CL_07 {
            text-align: right !important;
            border-right: 1px solid #EFEFEF;
            padding-right: 10px;
            }
                                .SU_CL_08 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            padding-right: 10px;
            padding-left: 10px;
            }
        .in_btn_file_browse {
            font-family: 'Gotham Narrow Book' !important;
            font-size: 12px !important;
            color: #666;
            border-radius: 3px;
            background-color: #FFFFFF;
            border: 1px solid #bdc5c9 !important;
            vertical-align: middle;
            margin: 0 !important;
            height: 35px;
            padding-left: 25px;
            padding-right: 25px;
        }

        .in_btn_file_browse:hover {
            background-color: #048ABB;
            border-color: #048ABB !important;
            cursor: pointer;
            color: #FFFFFF;
        }



    </style>

    <script type="text/javascript">
        function ShowPopup() {
            
            $("#btnShowPopup").click();
        }
        var canCallErrorBox = '<%=canCall%>';
        $(document).ready(function () {
            //$("#ContentMainSection_buttonsection").hide();

            $('#<%=BTN_Browse.ClientID%>').click(function () {
                document.getElementById('<%=FileUpload1.ClientID%>').click();
            });

            $('#<%=FileUpload1.ClientID%>').change(function () {
                var path = $(this).val();
                if (path != '' && path != null) {
                    var q = path.substring(path.lastIndexOf('\\') + 1);
                    $('#<%=LBL_FileName.ClientID%>').html(q);
                    $('#<%=BTN_UploadFile.ClientID%>').css("display", "inline");
                 }
            });

            if (canCallErrorBox) {
                $("#SPN_ErrorMessage").text(canCallErrorBox);
                $('#MDL_MsgDialog').modal('toggle');
            }

            if (window.File && window.FileList && window.FileReader) {
                /************************************ 
                 * All the File APIs are supported. * 
                 * Entire code goes here.           *
                 ************************************/


                /* Setup the Drag-n-Drop listeners. */
                var dropZone = document.getElementById('drop_zone');
                dropZone.addEventListener('dragover', handleDragOver, false);
                dropZone.addEventListener('drop', handleDnDFileSelect, false);

            }
            else {
                alert('Sorry! this browser does not support HTML5 File APIs.');
            }

            function handleDragOver(event) {
                event.stopPropagation();
                event.preventDefault();
                var dropZone = document.getElementById('spanCon');
                dropZone.innerHTML = "Drop now";
            };

            function validaFileSize() {

                var ccc = 1024 * 1024 * 4;

                if ((document.getElementById("ContentMainSection_FileUpload1").files[0].size) > ccc) {
                    alert("Maximum file size is 4MB.");
                    return false;
                }

                return true;
            }

            function handleDnDFileSelect(event) {
                event.stopPropagation();
                event.preventDefault();
                var agId = $("#agentDrop").val();
                /* Read the list of all the selected files. */
                files = event.dataTransfer.files;

                /* Consolidate the output element. */
                var form = document.getElementById('form1');
                var data = new FormData(form);

                for (var i = 0; i < files.length; i++) {
                    s = files[i].name;
                    //fileNameChange = files[i].name;
                    //if (s != "")
                    //    fileNameChange = s;
                    data.append(files[i].name, files[i]);

                }
                if (s != null) {
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200 && xhr.responseText) {
                            //alert("Upload done!" + xhr.responseText);
                            var dropZone = document.getElementById('spanCon');
                            dropZone.innerHTML = "File Successfully Uploaded.";
                            window.location.reload();
                        } else {
                            //alert("upload failed!");
                        }
                    };
                    xhr.open('POST', "Welcome.aspx?q=1");
                    // xhr.setRequestHeader("Content-type", "multipart/form-data");
                    xhr.send(data);
                }
                else {
                    var dropZone = document.getElementById('spanCon');
                    dropZone.innerHTML = "DRAG & DROP YOUR IFTI FILE HERE";

                }

            }
        })

        </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




    <!-- STATS SECTION TOP STSRTS HERE -->

<div style="background-color:#DCDCDC;">
    <div class="container" style="padding-top:100px; padding-bottom:50px;">
        <div class="media-container-row">

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5">
                        This Week
                    </h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            $
                        </span>
                        <span class="price-figure mbr-fonts-style display-2" id="LBL_ThisWeek" runat="server">
                              </span>
                        
                    </div>
                </div>
                
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5">
                        This Month
                    </h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            $
                        </span>
                        <span class="price-figure mbr-fonts-style display-2" id="LBL_ThisMonth" runat="server">
                            </span>
                    </div>
                </div>
                
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5">
                        This Year
                    </h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            $
                        </span>
                        <span class="price-figure mbr-fonts-style display-2" id="LBL_ThisYear" runat="server">
                            </span>
                    </div>
                </div>
                
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5">
                        All Time</h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            $
                        </span>
                        <span class="price-figure mbr-fonts-style display-2" id="LBL_AllTime" runat="server">
                            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




    <!-- STATS SECTION TOP ENDS HERE -->


    <!-- NEW DROP FILES SECTION START -->
<div style="padding-top:50px; padding-bottom: 25px; background-color:#EFEFEF;">
    <div class="container">
        <div class="media-container-row">
            <div class="col-md-8 align-center">
                <h2 class="pb-3 mbr-fonts-style display-5">
                    Import Transactions/IFTI Files
                </h2>

                <div class="social-list pl-0 mb-0">
                    <div class="content">
                        <div id="agentDropDiv" style="display:none;"></div>
                        <div id="iploadSectionDiv">
                            <div id="drop_zone"><span id="spanCon">DRAG &amp; DROP YOUR IFTI FILE HERE</span><br />
                                Maximum File Size: 4MB</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    



<!-- NEW DROP FILES SECTION END -->

<div class="container-fluid" style="background-color:#EFEFEF;">
    <div class="row align-center">
        <div class="col-md-12">
            <div class="content text-center" >
                <p class="text-info">or browse the file you want to upload</p>
                <asp:FileUpload ID="FileUpload1" runat="server" style="display:none;" accept=".xls,.xlsx,.csv" />
                <input id="BTN_Browse" class="in_btn_file_browse" type="button" value="Browse..." runat="server" />
                <br />
                <asp:Label ID="LBL_FileName" runat="server" class="page_contents_large"></asp:Label><br />
                <asp:Button ID="BTN_UploadFile" runat="server" Text="UPLOAD" CssClass="btn btn-info left " OnClientClick="return validaFileSize();" OnClick="BTN_UploadFile_Click" Style="display: none; cursor: pointer;"/>&nbsp;
                <button type="button" style="display:none;" id="btnShowPopup" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#MDL_MsgDialog">Launch demo modal</button>
            </div>
        </div>
    </div>
</div>
        
<div class="container-fluid" style="background-color:#EFEFEF; padding-bottom:25px;" id="DIV_ButtonRow" runat="server">
    <div class="container container-table">
        <div class="row" >
            <div class="col-md-12">
                <table style="width:100%; background-color:#DCDCDC;">
                    <tr>
                        <td style="width:50%; vertical-align:middle; padding-top:10px; padding-bottom:10px; padding-left: 10px; padding-right: 5px;">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <div id="buttonsection" runat="server">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td>
                                                        <span class="failmessage"><asp:Label ID="failureLabel" runat="server"></asp:Label></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                        <span class="successmessage"><asp:Label ID="successlabel" runat="server"></asp:Label></span>
                                                    </td>
                                                    <td style="text-align:right;">
                                                        <asp:Button ID="refreshButton" Text="OK" runat="server" Visible="false"  class="in_btn_blue" OnClick="refreshButton_Click" />&nbsp;
                                                        <asp:Button ID="lastInsert" Text="Import" runat="server" Visible="false" CommandArgument="c"  class="in_btn_blue" OnClick="lastInsert_Click1"  />&nbsp;
                                                        <asp:Button ID="resetButton" Text="Reset" runat="server" Visible="false"  class="in_btn_red" OnClick="resetButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td>&nbsp;
                                                </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
            </div>
            </div>

    </div>
</div><!-- ends here -->       

<div class="container-fluid" style="background-color:#EFEFEF;">
<div class="container container-table">
    <div class="content" style="padding-bottom: 25px;">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <div id="gridDivSuccess" runat="server" >
                        <asp:GridView ID="GridView1" CssClass="Grid" runat="server" AllowPaging="true" PageSize="10" OnPageIndexChanging="GridView1_PageIndexChanging" AutoGenerateColumns="False" Width="100%">
                        <RowStyle BackColor="#DAE5F4" />
                        <AlternatingRowStyle BackColor="#B8D1F3" />
                        <Columns>
                            <asp:TemplateField HeaderText="ROW" ItemStyle-CssClass="FA_CL_01" HeaderStyle-CssClass="rep_head_center">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("RowNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TXN REF" ItemStyle-CssClass="FA_CL_02" HeaderStyle-CssClass="rep_head_center">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("TransRefNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TXN DATE" ItemStyle-CssClass="SU_CL_03" HeaderStyle-CssClass="rep_head_center">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("MoneyRecFromCustDateTime", "{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SENDER" ItemStyle-CssClass="SU_CL_04" HeaderStyle-CssClass="rep_head_left">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("FullName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="RECEIVER" ItemStyle-CssClass="SU_CL_05" HeaderStyle-CssClass="rep_head_left">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("BeneFullName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BANK" ItemStyle-CssClass="SU_CL_06" HeaderStyle-CssClass="rep_head_center">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("BeneNameOfInstitution") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AMOUNT" ItemStyle-CssClass="SU_CL_07" HeaderStyle-CssClass="rep_head_right">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("TotalAmount", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" ItemStyle-CssClass="SU_CL_08" HeaderStyle-CssClass="rep_head_center">
                                <ItemTemplate>
                                    <asp:Label ID="LabelName" runat="server" Text='<%# Eval("ErrorList") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField> 
                        </Columns>
                        </asp:GridView> 
                        <div>&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>

<!-- FAILED TABLE -->
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <div id="gridDivUnSuccess" runat="server">
                        <asp:GridView ID="GridView2" CssClass="Grid" runat="server" AllowPaging="true" PageSize="10" OnPageIndexChanging="GridView2_PageIndexChanging" AutoGenerateColumns="False" Width="100%">
                            <RowStyle BackColor="#DAE5F4" />
                            <AlternatingRowStyle BackColor="#B8D1F3" />
                            <Columns>
                                <asp:TemplateField HeaderText="ROW" ItemStyle-CssClass="FA_CL_01" HeaderStyle-CssClass="rep_head_center">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelName" runat="server" Text='<%# Eval("RowNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TXN REF" ItemStyle-CssClass="FA_CL_02" HeaderStyle-CssClass="rep_head_center">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelName" runat="server" Text='<%# Eval("TransRefNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="COMMENTS" ItemStyle-CssClass="FA_CL_03" HeaderStyle-CssClass="rep_head_left">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelName" runat="server" Text='<%# Eval("ErrorList") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="content">
                                    
                </div>
            </div>
        </div>

    </div>

</div>
</div>            
                    
             

    
    






    
<!-- ***** DOWNLOAD TEMPLATE BUTTON ***** -->
<div style="background-color:#292C3B; padding-top:25px;">
    <div class="container" style="">
        <div class="container">
            <div class="media-container-row align-center mbr-white">
                <div class="col-12">
                    <div style="text-align:center;">
                        <a href="../Templates/IFTI-DRA IN.xls" class="btn btn-info center-block">Download Template</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




    <div id="MDL_MsgDialog" class="modal fade" role="dialog" data-backdrop="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error Notification</h4>
      </div>
      <div class="modal-body">
        <p><span id="SPN_ErrorMessage"></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</asp:Content>

