﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="KASI_Extend_.Reports2" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Condensed:400,500,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:300,400,700" rel="stylesheet" />

    <link href="css/jquery-ui.min.css" rel="stylesheet" />

    <style>
        .ui-dialog {
            padding: 0em !important;
        }
        .ui-dialog .ui-dialog-content {
            padding: 0px 0px 0px 0px !important;
        }
        .ui-widget-content {
            border: none !important;
        }
        .modal-heading {
            font-family: 'Yanone Kaffeesatz', sans-serif;
            font-size: 1.5em;
            color: rgb(98, 203, 49);
            font-weight: normal;
            line-height: normal !important;
            vertical-align: middle;
            height: 80px;
        }
        .modal-font {
            font-family: 'Yanone Kaffeesatz', sans-serif;
            font-size: 1em;
            color: #6a6c6f;
            line-height: 1.25em;
            font-weight: 500;
}
    </style>

    <script type="text/javascript">

        function hideHasfiles()
        {
            $("#DIV_file_yes").hide();
            $('#DIV_file_processing').hide();
            $('#DIV_file_no').show();
            $('#<%=btnAustract.ClientID%>').hide();
        }

        function showHasFiles()
        {
            $("#DIV_file_yes").show();
            $('#DIV_file_processing').hide();
            $('#DIV_file_no').hide();
            $('#<%=btnAustract.ClientID%>').show();
        }

        $(document).ready(function () {

            //$('#DIV_file_processing').hide();
            //$('#DIV_file_no').hide();

           var IFTIModal = $('#IFTIModal').dialog({
                modal: true,
                autoOpen: false,
                width: 480,
                
            }).parent().appendTo("form");

           $('#<%=btnCloseAustrac.ClientID%>').click(function () {
               IFTIModal.dialog('close');
           })

           // IFTIModal.siblings('.ui-dialog-titlebar').remove();

            $("#<%=btnModalOpenAustract.ClientID%>").click(function (e) {
                $('#IFTIModal').dialog('open');
                e.preventDefault();
                return false;
            });

        });
    </script>

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <form id="form1" runat="server">

    <div class="container_top_headings">
        <table class="tbl_width_1200">
            <tr style="height:70px; vertical-align:middle;">
                <td style="vertical-align:middle;"><span class="all_headings">Reports</span></td>
                <td style="vertical-align:middle; text-align:right;">
                    &nbsp;<asp:Button ID="btnModalOpenAustract" runat="server" Text="Create Austrac IFTI File" CssClass="aa_btn_red"/></td>
            </tr>
        </table>
    </div>

    <div class="container_one wiz_bg_EFEFEF no_padding_btm">
        <table class="tbl_width_1200">
            <tr>
                <td>
                    <table style="width:100%;" class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_160">
                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                    <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image7" runat="server" ImageUrl="~/images/document_av.png" /></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="rep_description">All Transactions <br /> Report</td></tr>
                                    <tr><td class="rep_description">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button7" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button7_Click" /></td></tr>
                    </table>
                </td>

                <td class="style__width_20">&nbsp;</td>

                <td>
                    <table style="width:100%;" class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_160">
                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                    <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/notepad_av.png" /></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="rep_description">Cancelled Transactions Report</td></tr>
                                    <tr><td class="rep_description">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button11" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button11_Click" /></td></tr>
                    </table>
                </td>
                
                <td style="width:20px;">&nbsp;</td>

                <td>
                    <table style="width:100%;" class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_160">
                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                    <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/tasks_av.png" /></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="rep_description">Compliance Review Transactions Report</td></tr>
                                    <tr><td class="rep_description">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button4" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button2_Click4"  /></td></tr>
                    </table>
                </td>

                <td style="width:20px;">&nbsp;</td> <!-- SEPERATOR 06 -->
                    
                <td>
                    <table style="width:100%;" class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_160">
                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                    <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/clipboard_av.png" /></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="rep_description">Manually KYC Approved Customers Report</td></tr>
                                    <tr><td class="rep_description">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button3" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button3_Click1"  /></td></tr>
                    </table>
                </td>

                    <td style="width:20px;">&nbsp;</td>

                <td>
                    <table style="width:100%;" class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_160">
                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                    <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image3" runat="server" ImageUrl="~/images/calendar_av.png" /></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="rep_description">Monthly Revenue<br />Report</td></tr>
                                    <tr><td class="rep_description">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button1" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button5_Click"  /></td></tr>
                    </table>
                </td>
                    
                <td style="width:20px;">&nbsp;</td>

                <td>
                    <table style="width:100%;" class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_160">
                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                    <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image4" runat="server" ImageUrl="~/images/trnsummary_av.png" /></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="rep_description">Transactions Summary Report</td></tr>
                                    <tr><td class="rep_description">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button2" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button6_Click"  /></td></tr>
                    </table>
                </td>
                     
                   

                </tr>
            </table>
        </div>

        <div class="container_one wiz_bg_EFEFEF no_padding_top">
            <table class="tbl_width_1200">
                <tr style="height:25px;"><td>&nbsp;</td></tr>
                <tr>

                    <td>
                        <table style="width:100%;" class="background_FFFFFF">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image5" runat="server" ImageUrl="~/images/settings_av.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td class="rep_description">Customers with Credit in Account Report</td></tr>
                                        <tr><td class="rep_description">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button5" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button5_Click1"/></td></tr>
                        </table>
                    </td>

                    <td style="width:20px;">&nbsp;
                    </td>

                    <td>
                        <table style="width:100%;" class="background_FFFFFF">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image6" runat="server" ImageUrl="~/images/nodocument_av.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td class="rep_description">Customers without Documents Report</td></tr>
                                        <tr><td class="rep_description">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><div><asp:Button ID="Button6" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button6_Click1"/></div></td></tr>
                        </table>
                    </td>

                    <td style="width:20px;">&nbsp;</td>

                    <td>
                        <table style="width:100%;" class="background_FFFFFF">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image8" runat="server" ImageUrl="~/images/warning_av.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td class="rep_description">High Risk Customers Report</td></tr>
                                        <tr><td class="rep_description">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button8" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button8_Click1"/></td></tr>
                        </table>
                    </td>

                    <td style="width:20px;">&nbsp;</td>

                    <td>
                        <table style="width:100%;" class="background_FFFFFF">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image10" runat="server" ImageUrl="~/images/warning_av.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td class="rep_description">Activity Report</td></tr>
                                        <tr><td class="rep_description">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button10" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button10_Click" /></td></tr>
                        </table>
                    </td>

                    <td style="width:20px;">&nbsp;</td>

                    <td>
                        <table style="width:100%;" class="background_FFFFFF">
                            <tr>
                                <td>
                                    <table class="tbl_width_160">
                                        <tr><td style="height:20px;">&nbsp;</td></tr>
                                        <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image12" runat="server" ImageUrl="~/images/warning_av.png" /></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td class="rep_description">Multi Customer to Same Bene</td></tr>
                                        <tr><td class="rep_description">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button12" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button12_Click" /></td></tr>
                        </table>
                    </td>

                    <td style="width:20px;">&nbsp;</td>

                    <td>
                        <table style="width:100%;" class="background_FFFFFF">
                        <tr>
                            <td>
                                <table class="tbl_width_160">
                                    <tr><td style="height:20px;">&nbsp;</td></tr>
                                    <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image9" runat="server" ImageUrl="~/images/document_av.png" /></td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td class="rep_description">KAASI Monthly Payment Report</td></tr>
                                    <tr><td class="rep_description">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button9" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button9_Click1" /></td></tr>
                    </table>
                    </td>



                </tr>
            </table>
        </div>

     <div id="IFTIModal" >
        <table class="tbl_width_480">
            <tr><td style="background-color:#76D17F;" class="spacer5">&nbsp;</td></tr>
            <tr>
                <td style="background-color:#F7F9FA; border-bottom:1px solid #e5e5e5;">
                    <table class="tbl_width_440">
                        <tr><td class="modal-heading">Create AUSTRAC IFTI File</td></tr>
                    </table>
                </td>
            </tr>
         </table>
         <div id="DIV_file_yes">
             <table class="tbl_width_440">
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
                 <tr><td class="modal-font">You are about to generate the Austrac IFTI File. This process may take several minutes and you will not be able to perform any other function until this is completed.</td></tr>
                 <tr><td>&nbsp;</td></tr>
                 <tr>
                     <td>
                         <table>
                             <tr><td class="modal-font">Starting Transaction: <span id="IFTIStartTrans" runat="server"></span></td><td>&nbsp;</td></tr>
                             <tr><td class="modal-font">Ending Transaction: <span id="IFTIEndTrans" runat="server"></span></td><td>&nbsp;</td></tr>
                             <tr><td class="modal-font">Total Number of Transaction:</td><td><span id="IFTITotalTrans" runat="server"></span></td></tr>
                         </table>
                     </td>
                 </tr>
                 
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
             </table>
         </div>
         <div id="DIV_file_processing">
             <table class="tbl_width_440">
                 <tr><td>&nbsp;</td></tr>
                 <tr><td><span id="SPNPleaseWait" runat="server" style="display:none;">Please wait...processing...</span>Processing</td></tr>
                 <tr><td>&nbsp;</td></tr>
             </table>
         </div>
         <div id="DIV_file_no">
             <table class="tbl_width_440">
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
                 <tr><td class="normal_font">You do not have any unreported transactions.</td></tr>
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
             </table>
         </div>
         <table class="tbl_width_480">
            <tr>
                <td style="background-color:#F7F9FA; border-top:1px solid #e5e5e5;">
                    <table class="tbl_width_440">
                        <tr class="spacer15"><td>&nbsp;</td></tr>
                        <tr><td style="text-align:right;"><asp:Button runat="server" CssClass="aa_btn_red" Text="Create File" ID="btnAustract" CausesValidation="False" OnClick="btnAustract_Click" />&nbsp;<asp:Button runat="server" UseSubmitBehavior="false" CssClass="aa_btn_red" Text="Close" ID="btnCloseAustrac" /></td></tr>
                        <tr class="spacer15"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
         </table>
     </div>   


    </form>

</asp:Content>
