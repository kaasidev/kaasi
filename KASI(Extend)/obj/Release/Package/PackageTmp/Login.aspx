﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="KASI_Extend_.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

       <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>
    <link href="css/jquery-ui.min.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700" rel="stylesheet" />

    <style>
        .pw_heading {
            font-family: 'Yanone Kaffeesatz', sans-serif;
            text-align: center;
            font-size: 1.5em;
            color: #5fcf80;
        }




        .pw_msg {
            font-family: 'Yanone Kaffeesatz', sans-serif;
            font-size: 1em !important;
            text-align: center;
            color: #666;
            line-height:18px;
        }
        .pw_err_msg {
            font-family: 'Yanone Kaffeesatz', sans-serif;
            font-size: 1em !important;
            color: #e15258;
            line-height:18px;
        }
    </style>

    <script type="text/javascript">

        function shakeErrorMsg()
        {
            $('#<%=err_msg.ClientID%>').effect('shake');
        }

        $(document).ready(function () {
            ForgotPassword = $('#DIV_ForgotPassword').dialog({
                modal: true,
                autoOpen: false,
                width: 320,
                title: 'Forgot Password',
            });

            $(".ui-dialog-titlebar").hide();

            $('#TD_ForgotPassword').click(function () {
                ForgotPassword.dialog('open');
            });

            $('#BTN_Submit').click(function () {
                if ($('#txt_EmailField1').valid() && $('#txt_EmailField2').valid())
                {
                    if ($('#txt_EmailField1').val() == $('#txt_EmailField2').val()) {
                        $('#SPN_Message').text('If this email address exists in our system, you will receive an email with instructions.');
                        $.ajax({
                            url: 'Processor/sendPasswordResetDetails.aspx',
                            data: {
                                Email: $('#txt_EmailField1').val(),
                            },
                            success: function (response) {

                            }
                        })
                    }
                    else {
                        $("#SPN_Message").text('Email addresses do not match. Please re-enter them again');
                    }
                }
                else
                {
                    $("#SPN_Message").text('Please enter a valid email address in the fields provided.');
                }
                
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <form id="form1" runat="server">

    <div class="container_one">
            <table class="tbl_width_320">
                <tr><td>&nbsp;</td></tr>
                <tr><td class="cus_name">Login to your KAASI account</td></tr>
            </table>

            <table class="tbl_width_320 brdr_shadow background_FFFFFF">
                <tr><td>
                    <table class="tbl_width_280">
                        <tr><td style="height:40px;">&nbsp;</td></tr>
                        <tr><td>
                            <asp:Image ID="Image1" runat="server" Height="128px" ImageUrl="~/images/money-bag.png" />
                            </td></tr>
                        <tr><td style="height:25px;">&nbsp;</td></tr>
                        <tr><td><input id="txt_uname" class="login_001" style="width:100%;" type="text"  value="" runat="server" placeholder="username" /></td></tr>
                        
                        <tr><td><input id="txt_pword" style="width:100%;" class="login_001" type="password"  value="" runat="server" placeholder="password" /></td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td><asp:Button ID="btn_ViewDebitCredit" runat="server" class="btn_green_login" style="width:100%;" Text="LOGIN" OnClick="Button1_Click" />
                            </td></tr>
                        <tr><td class="aa_forgot_pwd" id="TD_ForgotPassword">Forgot Your Password?</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td style="height:40px;" class="login_err"><span id="err_msg" runat="server"></span></td></tr>
                    </table>
                </td></tr>
            </table>

            <table class="tbl_width_320">
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
            </table>
    </div>

        
    </form>

    <div id="DIV_ForgotPassword">
        <form id="form2">
            <table class="tbl_width_280">
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr class="pw_heading"><td>Need help with your password?</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr class="pw_msg"><td>Enter the email address you use for KAASI below, and we'll send you your password.</td></tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_280">
                            <tr>
                                <td><div><input id="txt_EmailField1" type="email" required="required" style="width: 100%;" class="aa_input"/></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr class="pw_msg"><td>Please re-enter your email address below.</td></tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_280">
                            <tr>
                                <td><div><input id="txt_EmailField2" type="email" required="required" style="width: 100%;" class="aa_input"/></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr><td style="text-align: center !important;"><span class="pw_err_msg" id="SPN_Message"></span></td></tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table class="tbl_width_280">
                            <tr>
                                <td><input id="BTN_Cancel" type="button" class="aa_btn_red" value="Cancel" /></td>
                                <td style="text-align:right;"><input id="BTN_Submit" type="button" class="aa_btn_green" value="Send Request" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
            </table>
        </form>
        </div>

</asp:Content>
