﻿<%@ Page Title="" Language="C#" MasterPageFile="Import.Master" AutoEventWireup="true" CodeBehind="inSettings.aspx.cs" Inherits="KASI_Extend.Inwards.inSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- CSS FILES -->
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="../css/datatables.min.transactions.css" rel="stylesheet" />

    <!-- JS FILES -->
    <script type="text/javascript" charset="utf-8" src="../js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/jquery.dataTables.min.js"></script>

    <style>
    /* CSS OVERRIDES */
    label {
        display: none !important;
    }

    </style>

    <script type="text/javascript">
        var LoadingDIV;
        var allCompanies;
        $(document).ready(function () {

            // DATATABLE QUERY HANDLERS ************************************************************************
            var allUsers = $("#usermanagement").DataTable({
                columns: [
                    { 'data': 'LoginID' },
                    { 'data': 'CompanyName' },
                    { 'data': 'LoginEmail' },
                    { 'data': 'FullName' },
                    { 'data': 'Status' }
                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: '../DataHandlers/InwardLoginDataHandler.ashx',
                "order": [[0, "desc"]],
            });

             allCompanies = $("#companymanagement").DataTable({
                columns: [
                    { 'data': 'AgentID' },
                    { 'data': 'AgentName' },
                    { 'data': 'AgentCountry' },
                    { 'data': 'Status' },
                { 'data': 'View' }
                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: '../DataHandlers/InwardAgentDataHandler.ashx',
                "order": [[0, "desc"]],
            })

            // DIALOG HANDLERS ***************************************************************************
            AddNewUserDialog = $('#DIV_AddNewUser').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Add New User",
             });

            ManageMasterSettings = $('#DIV_MasterSettings').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Add New User",
            })

            AddNewCompanyDialog = $('#DIV_AddNewCompany').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
                title: "Add New Company",
            })


            // CLICK EVENT HANLDERS **************************************************************
            $('#add_NewUser').click(function () {
                AddNewUserDialog.dialog('open');
            });

            $('#edit_Settings').click(function () {
                ManageMasterSettings.dialog('open');
            });

            $('#add_NewCompany').click(function () {

                clearIncomigAgentFields();
                $(".modal_headings").text('Add New Remitter');
                AddNewCompanyDialog.dialog('open');
            });

            $('#BTN_CloseNewCompany').click(function () {
                AddNewCompanyDialog.dialog('close');
            });

            $('#BTN_CloseNewUser').click(function () {
                AddNewUserDialog.dialog('close');
            });

            $('#BTN_CloseManageSettingsDialog').click(function () {
                ManageMasterSettings.dialog('close');
            });

            $('#BTN_CloseManageSettings').click(function () {
                ManageMasterSettings.dialog('close');
            })

            $('#BTN_SaveSettings').click(function () {
                $.ajax({
                    url: 'Processor/saveMasterSettings.aspx',
                    data: {
                        NbMonths: $('#<%=DDL_NbMonthKYCExpires.ClientID%> :selected').val(),
                        DLimit: $('#<%=TXT_DollarLimit.ClientID%>').val(),
                    },
                    success: function (response) {
                        if (response == "OK") {
                            alert('Settings Saved Successfully');
                            location.reload();
                        }
                        else {
                            alert(response);
                        }
                        
                    }
                })
            })

            $('#Tab01').click(function () {
                $("tabclicked").val(1);
                firstTabClicked();
            });

            $('#Tab02').click(function () {
                $("tabclicked").val(2);
                secondTabClicked();
            });

            $('#Tab03').click(function () {
                $("tabclicked").val(2);
                thirdTabClicked();
            })

            $('#BTN_CloseAddNewUserDialog').click(function () {
                AddNewUserDialog.dialog('close');
            });

            $('#BTN_CloseNewRemitter').click(function () {

            })

            $('#BTN_SaveNewUser').click(function () {
                
                $.ajax({
                    url: '../Processor/AddNewInwardLogin.aspx',
                    data: {
                        AID: $('#<%=DDL_AgentList.ClientID%> :selected').val(),
                        Fname: $('#in_user_first_name').val(),
                        LName: $('#in_user_last_name').val(),
                        Email: $('#in_user_email_address').val(),
                        Uname: $('#in_user_username').val()
                    },
                    async: false,
                    success: function (response) {
                        alert(response);
                        AddNewUserDialog.dialog('close');
                        allUsers.ajax.reload();
                        $('#in_user_first_name').val('');
                        $('#in_user_last_name').val('');
                        $('#in_user_email_address').val('');
                        $('#in_user_username').val('');
                        $('#<%=DDL_AgentList.ClientID%> option:eq(0)').attr('selected', 'selected');
                    }
             })
            });

            $('#in_user_email_address').blur(function () {
                $('#in_user_username').val($('#in_user_email_address').val());
            })

            LoadingDIV = $('#DIV_Loading').dialog({
                modal: true,
                autoOpen: false,
                width: 120,
                dialogClass: 'dialog_transparent_background',
            });

            LoadingDIV.siblings('.ui-dialog-titlebar').remove();


            insertNewRemitter();

        });

        function secondTabClicked() {

            $('#DIV_AddNewUserSection').css('display', 'none');
            $('#DIV_AddNewCompanySection').css('display', 'block');
            $('#DIV_ManageSettings').css('display', "none");
            $('#Tab01').removeClass();
            $('#Tab01').addClass("tabs_style_01_inactive");
            $('#Tab02').removeClass();
            $('#Tab02').addClass("tabs_style_01_active");
            $('#Tab03').removeClass();
            $('#Tab03').addClass("tabs_style_01_inactive");

            
        }

        function thirdTabClicked() {
            $('#DIV_AddNewUserSection').css('display', 'none');
            $('#DIV_AddNewCompanySection').css('display', 'none');
            $('#DIV_ManageSettings').css('display', "block");
            $('#Tab01').removeClass();
            $('#Tab01').addClass("tabs_style_01_inactive");
            $('#Tab02').removeClass();
            $('#Tab02').addClass("tabs_style_01_inactive");
            $('#Tab03').removeClass();
            $('#Tab03').addClass("tabs_style_01_active");
        }

        function firstTabClicked() {

            $('#DIV_AddNewUserSection').css('display', 'block');
            $('#DIV_AddNewCompanySection').css('display', 'none');
            $('#DIV_ManageSettings').css('display', "none");
            $('#Tab01').removeClass();
            $('#Tab01').addClass("tabs_style_01_active");
            $('#Tab02').removeClass();
            $('#Tab02').addClass("tabs_style_01_inactive");
            $('#Tab03').removeClass();
            $('#Tab03').addClass("tabs_style_01_inactive");
        }


        function clearIncomigAgentFields() {
            $("#agentIdedit").val('');
            $('#in_comp_name').val('');
            $('#in_comp_address_line_1').val('');
            $('#in_comp_address_line_2').val('');
            $('#in_comp_city').val('');
            $('#in_comp_state_province_region').val('');
            $('#in_comp_zip_postal_code').val('');
            $('#in_comp_country').val('');
            $('#in_comp_telephone').val('');
            $('#in_comp_email_1').val('');
            $('#in_comp_email_2').val('');
            $('#in_comp_website').val('');

        };

        

        function insertNewRemitter() {


            $('#BTN_SaveNewCompany').click(function () {

                var editid = $("#agentIdedit").val();

                var form = $("#addnewcompanyForm");

                if ($('#in_comp_email_2').val() == "") {
                    $('#in_comp_email_2').focus();
                }

              
                if ($('#in_comp_email_1').val() == "") {
                    $('#in_comp_email_1').focus();
                }

               if ($('#in_comp_website').val() == "") {
                   $('#in_comp_website').focus();
               }

               if ($('#in_comp_telephone').val() == "") {
                   $('#in_comp_telephone').focus();
               }

             
               if ($('#in_comp_country').val() == "") {
                   $('#in_comp_country').focus();
               }

               if ($('#in_comp_zip_postal_code').val() == "") {
                   $('#in_comp_zip_postal_code').focus();
               }

               if ($('#in_comp_state_province_region').val() == "") {
                   $('#in_comp_state_province_region').focus();
               }

               if ($('#in_comp_city').val() == "") {
                   $('#in_comp_city').focus();
               }

               if ($('#in_comp_address_line_2').val() == "") {
                   $('#in_comp_address_line_2').focus();
               }

               if ($('#in_comp_address_line_1').val() == "") {
                   $('#in_comp_address_line_1').focus();
               }

               if ($('#in_comp_name').val() == "") {
                   $('#in_comp_name').focus();
               }




                if (form.valid() && $('#in_comp_address_line_1').val() != ""
                    && $('#in_comp_address_line_2').val() != "" 
                    && $('#in_comp_address_line_2').val() != ""
                    && $('#in_comp_city').val() != ""
                    && $('#in_comp_state_province_region').val() != "" && $('#in_comp_zip_postal_code').val() != ""
                    && $('#in_comp_country').val() != "" && $('#in_comp_telephone').val() != ""
                    && $('#in_comp_email_1').val() != "" && $('#in_comp_email_2').val() != ""
                    && $('#in_comp_website').val() != ""
                    ) {


                    if (editid != "") {


                        $.ajax({
                            url: 'Processor/addNewRemitter.aspx',
                            beforeSend: function () {

                                LoadingDIV.dialog('open');
                            },
                            data: {
                                AgentId: editid,
                                NR: $('#in_comp_name').val(),
                                ADL1: $('#in_comp_address_line_1').val(),
                                ADL2: $('#in_comp_address_line_2').val(),
                                City: $('#in_comp_city').val(),
                                State: $('#in_comp_state_province_region').val(),
                                PCode: $('#in_comp_zip_postal_code').val(),
                                Country: $('#in_comp_country').val(),
                                Phone: $('#in_comp_telephone').val(),
                                Email1: $('#in_comp_email_1').val(),
                                Email2: $('#in_comp_email_2').val(),
                                Web: $('#in_comp_website').val()
                            },
                            complete: function () {
                                LoadingDIV.dialog('close');
                                $("#agentIdedit").val('');
                            },
                            success: function (response) {
                                //var datasplit = response.split('|');
                                LoadingDIV.dialog('close');
                                AddNewCompanyDialog.dialog('close');
                                allCompanies.ajax.reload();
                            }
                        });

                    }
                    else {

                        $.ajax({
                            url: 'Processor/addNewRemitter.aspx',
                            beforeSend: function () {

                                LoadingDIV.dialog('open');
                            },
                            data: {
                                NR: $('#in_comp_name').val(),
                                ADL1: $('#in_comp_address_line_1').val(),
                                ADL2: $('#in_comp_address_line_2').val(),
                                City: $('#in_comp_city').val(),
                                State: $('#in_comp_state_province_region').val(),
                                PCode: $('#in_comp_zip_postal_code').val(),
                                Country: $('#in_comp_country').val(),
                                Phone: $('#in_comp_telephone').val(),
                                Email1: $('#in_comp_email_1').val(),
                                Email2: $('#in_comp_email_2').val(),
                                Web: $('#in_comp_website').val()
                            },
                            complete: function () {
                                LoadingDIV.dialog('close');
                            },
                            success: function (response) {
                                //var datasplit = response.split('|');
                                AddNewCompanyDialog.dialog('close');
                                allCompanies.ajax.reload();

                            }
                        });
                    }
                }

            });
        }


        function EditMe(agentId) {

            callIncomingAgentData(agentId);
        };

        

        function callIncomingAgentData(agetnId) {
            clearIncomigAgentFields();
            $("#agentIdedit").val(agetnId);
            $(".modal_headings").text('Update Remitter');
            $.ajax({
                url: '../JQDataFetch/getIncomingAgentById.aspx',
                async: false,
                data: {
                    agentId: agetnId,
                },
                dataType: "json",
                success: function (data) {

                    $('#in_comp_name').val(data.AgentName);
                    $('#in_comp_address_line_1').val(data.AgentAddress1);
                    $('#in_comp_address_line_2').val(data.AgentAddress2);
                    $('#in_comp_city').val(data.AgentCity);
                    $('#in_comp_state_province_region').val(data.AgentState);
                    $('#in_comp_zip_postal_code').val(data.AgentPostcode);
                    $('#in_comp_country').val(data.AgentCountry);
                    $('#in_comp_telephone').val(data.AgentPhone);
                    $('#in_comp_email_1').val(data.AgentEmail);
                    $('#in_comp_email_2').val(data.AgentEmail2);
                    $('#in_comp_website').val(data.Website);

                    AddNewCompanyDialog.dialog('open');

                }
            });


        }

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">
    <input type="hidden" id="tabclicked" />
    <div class="bg_DCDCDC">
        <table class="tbl_width_1200">
            <tr class="spacer20">
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="page_headings">Inward Settings</td>
            </tr>
            <tr class="spacer20">
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <div class="bg_EFEFEF">
        <div>
            <table class="tbl_width_1200">

                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td style="width: 280px; vertical-align:top;">
                                    <table class="tbl_width_280">
                                        <tr style="height:150px; background-color:#048ABB;"><td>&nbsp;</td></tr>
                                        <tr style="height:50px; background-color:#FFFFFF;"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                                <td style="width: 20px;">&nbsp;</td>
                                <td style="width: 900px;">
                                    <table class="tbl_width_900">
                                        <tr>
                                            <td>
                                                <table class="tbl_width_900">
                                                    <tr>
                                                        <td class="tabs_style_01_active" style="width: 75px;" id="Tab01">
                                                            <asp:Image ID="Image9" runat="server" ImageUrl="images/surgeon.png" title="USERS" /></td>
                                                        <td class="width_spacer5">&nbsp;</td>
                                                        <td class="tabs_style_01_inactive" style="width: 75px;" id="Tab02">
                                                            <asp:Image ID="Image1" runat="server" ImageUrl="images/remitter.png" title="REMITTERS" /></td>
                                                        <td class="width_spacer5">&nbsp;</td>
                                                        <td class="tabs_style_01_inactive" style="width: 75px;" id="Tab03">
                                                            <asp:Image ID="Image4" runat="server" ImageUrl="images/settings.png" title="SETTINGS" />
                                                        </td>
                                                        <td class="width_spacer5">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                                        <td class="tabs_style_01_spacer" style="width: 60px;">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="spacer3">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr class="spacer5" style="background-color: #39BBED;">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr class="spacer20 bg_FFFFFF">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr class="bg_FFFFFF">
                                            <td>
                                                <div id="DIV_AddNewUserSection">
                                                    <table class="tbl_width_860">
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_860">
                                                                    <tr>
                                                                        <td class="txn_tab_headings">User Accounts</td>
                                                                        <td style="text-align:right;"><input id="add_NewUser" type="button" class="btn_drkblue-2" value="Add New User Account" /></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                
                                                                <table cellpadding="0" cellspacing="0" border="0" id="usermanagement">
                                                                    <thead>
                                                                        <tr>
                                                                            <td>USER ID</td>
                                                                            <td>COMPANY</td>
                                                                            <td>EMAIL ADDRESS</td>
                                                                            <td>NAME</td>
                                                                            <td>STATUS</td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div id="DIV_AddNewCompanySection" style="display: none;">
                                                    <table class="tbl_width_860">
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_860">
                                                                    <tr>
                                                                        <td class="txn_tab_headings">Remitters</td>
                                                                        <td style="text-align:right;"><input id="add_NewCompany" type="button" class="btn_drkblue-2" value="Add New Remitter" /></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" border="0" id="companymanagement">
                                                                    <thead>
                                                                        <tr>
                                                                            <td>COMPANY ID</td>
                                                                            <td>COMPANY NAME</td>
                                                                            <td>COUNTRY</td>
                                                                            <td>STATUS</td>
                                                                            <td>EDIT</td>
                                                                        </tr>
                                                                    </thead>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </div>

                                                <div id="DIV_ManageSettings" style="display: none;">
                                                    <table class="tbl_width_860">
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_860">
                                                                    <tr>
                                                                        <td class="txn_tab_headings">Global Settings</td>
                                                                        <td style="text-align:right;"><input id="edit_Settings" type="button" class="btn_drkblue-2" value="Edit Global Settings" /></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_860">
                                                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                    <tr class="bg_F4F5F7">
                                                                        <td>
                                                                            <table class="tbl_width_820">
                                                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                                                <tr>
                                                                                    <td class="modal_settings_col_01">Number of Months Before KYC Expires</td>
                                                                                    <td class="modal_settings_col_02"><span id="LBL_NbMonthsKYCExpire" runat="server">6</span> Months </td>
                                                                                </tr>
                                                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                                                <tr>
                                                                                    <td class="modal_settings_col_01">Inward Transaction Dollar Limit Before Additional Documentation</td>
                                                                                    <td class="modal_settings_col_02">$&nbsp;<span id="LBL_DollarLimit" runat="server"></span></td>
                                                                                </tr>
                                                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="bg_FFFFFF spacer20">
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>


    <div id="DIV_AddNewUser">
        <table class="tbl_width_640">
            <tr class="bg_3498DB">
                <td>
                    <table class="tbl_width_640">
                        <tr>
                            <td class="modal_headings">Add New User Account</td>
                            <td class="modal_top_blue_btns" id="BTN_CloseAddNewUserDialog">X</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="bg_EFEFEF">
                <td>
                    <table class="tbl_width_600">
                        <tr class="spacer20"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_600">
                                    <tr style="height: 50px;">
                                        <td style="width: 50px;" class="modal_tab_active" id="TD_InwardUserDetails">
                                            <asp:Image ID="Image5" runat="server" ImageUrl="images/s_insurance.png" title="USER DETAILS" /></td>
                                        <td style="width: 50px;" class="modal_tab_inactive_right" id="TD_InwardUserSettings">
                                            <asp:Image ID="Image6" runat="server" ImageUrl="images/s_settings.png" title="USER SETTINGS" /></td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="bg_FFFFFF">
                            <td>
                                <table class="tbl_width_600">
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">Select Remitter (Company) Name</td>
                                                </tr>
                                                <tr>
                                                    <td><div>
                                                        <asp:DropDownList ID="DDL_AgentList" runat="server" CssClass="modal_input_dropdown" style="width:100%;"></asp:DropDownList>
                                                        </div></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                        <tr class="spacer10">
                                            <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">First Name</td>
                                                    <td class="modal_labels" style="width: 20px;">&nbsp;</td>
                                                    <td class="modal_labels">Last Name</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_user_first_name" type="text" style="width: 100%;" value="" required /></td>
                                                    <td>&nbsp</td>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_user_last_name"  style="width: 100%;" value="" required /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">Email Address</td>
                                                    <td class="modal_labels" style="width: 20px;">&nbsp;</td>
                                                    <td class="modal_labels">Username</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_user_email_address" type="email" style="width: 100%;" value="" required /></td>
                                                    <td>&nbsp</td>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_user_username"  style="width: 100%;" value="" required readonly="readonly" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>






                                </table>
                            </td>
                        </tr>




                    </table>
                </td>
            </tr>

            <tr class="bg_EFEFEF">
                            <td>
                                <table class="tbl_width_600">
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <input type="button" value="Cancel" id="BTN_CloseNewUser" class="btn_drkblue-1" />&nbsp;
                                        <input type="button" value="Save" id="BTN_SaveNewUser" class="in_btn_blue" /></td>
                                    </tr>
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
        </table>
    </div>


        <div id="DIV_MasterSettings">
        <table class="tbl_width_640">
            <tr class="bg_3498DB">
                <td>
                    <table class="tbl_width_640">
                        <tr>
                            <td class="modal_headings">Manage Settings</td>
                            <td class="modal_top_blue_btns" id="BTN_CloseManageSettingsDialog">X</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="bg_EFEFEF">
                <td>
                    <table class="tbl_width_600">
                        <tr class="spacer20"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_600">
                                    <tr style="height: 50px;">
                                        <td style="width: 50px;" class="modal_tab_active" id="TD_MasterSettings">
                                            <asp:Image ID="Image10" runat="server" ImageUrl="images/s_settings.png" title="USER SETTINGS" /></td>
                                        <td style="width: 50px;">
                                            &nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="bg_FFFFFF">
                            <td>
                                <table class="tbl_width_600">
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <table class="tbl_width_600">
                                                <tr>
                                                    <td class="modal_labels">Number of Months KYC Expires</td>
                                                    <td>&nbsp;</td>
                                                    <td style="text-align:right">
                                                        <asp:DropDownList ID="DDL_NbMonthKYCExpires" runat="server"></asp:DropDownList> Months</td>
                                                    
                                                </tr>
                                                <tr><td>&nbsp;</td></tr>
                                                <tr>
                                                    <td class="modal_labels">Inward Transaction Dollar Limit Before Additional Documentation</td>
                                                    <td>&nbsp;</td>
                                                    <td style="text-align:right">
                                                        <asp:TextBox ID="TXT_DollarLimit" runat="server"></asp:TextBox></td>
                                                    
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                   





                                </table>
                            </td>
                        </tr>




                    </table>
                </td>
            </tr>

            <tr class="bg_EFEFEF">
                            <td>
                                <table class="tbl_width_600">
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <input type="button" value="Cancel" id="BTN_CloseManageSettings" class="btn_drkblue-1" />&nbsp;
                                        <input type="button" value="Save" id="BTN_SaveSettings" class="in_btn_blue" /></td>
                                    </tr>
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
        </table>
    </div>
  </form>
    <div id="DIV_AddNewCompany">
        <form id="addnewcompanyForm">
             <input type="hidden" id="agentIdedit" />
        <table class="tbl_width_640">
            <tr class="bg_3498DB">
                <td>
                    <table class="tbl_width_640">
                        <tr>
                            <td class="modal_headings">Add New Remitter</td>
                            <td class="modal_top_blue_btns" id="BTN_CloseNewRemitter">X</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="bg_EFEFEF">
                <td>
                    <table class="tbl_width_600">
                        <tr class="spacer20">
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table class="tbl_width_600">
                                    <tr style="height: 50px;">
                                        <td style="width: 50px;" class="modal_tab_active" id="TD_AgentDetails">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="images/s_insurance.png" title="AGENT DETAILS" /></td>
                                        <td style="width: 50px;" class="modal_tab_inactive_right" id="TD_AgentCommission">
                                            <asp:Image ID="Image8" runat="server" ImageUrl="images/s_settings.png" title="COMMISSION" /></td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                        <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="bg_FFFFFF">
                            <td>
                                <table class="tbl_width_600">
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">Remitter (Company) Name</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_name" style="width: 100%;" type="text" value="" required /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">Address Line 1</td>
                                                    <td class="modal_labels" style="width: 20px;">&nbsp;</td>
                                                    <td class="modal_labels">Address Line 2</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_address_line_1" type="text" style="width: 100%;" value="" required /></td>
                                                    <td>&nbsp</td>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_address_line_2"  style="width: 100%;" value="" required /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels" style="width: 270px;">City</td>
                                                    <td class="modal_labels" style="width: 20px;">&nbsp;</td>
                                                    <td class="modal_labels">State/Province/Region</td>
                                                    <td class="modal_labels" style="width: 20px;">&nbsp;</td>
                                                    <td class="modal_labels">ZIP/Postal Code</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_city" style="width: 100%;" type="text" value="" required /></td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_state_province_region" style="width: 100%;" type="text" value="" required /></td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_zip_postal_code" style="width: 100%;" type="text" value="" required /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">Country</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_country" style="width: 100%;" type="text" value="" required /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">Telephone</td>
                                                    <td class="modal_labels" style="width: 20px;">&nbsp;</td>
                                                    <td class="modal_labels">Website</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox required" id="in_comp_telephone" style="width: 100%;" type="text" value="" required /></td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_website" style="width: 100%;" type="text" value="" required /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_560">
                                                <tr>
                                                    <td class="modal_labels">Email 1</td>
                                                    <td class="modal_labels" style="width: 20px;">&nbsp;</td>
                                                    <td class="modal_labels">Email 2</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_email_1" style="width: 100%;" type="text" value="" required" /></td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input class="modal_input_textbox" id="in_comp_email_2" style="width: 100%;" type="text" value="" required /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr id="createbuttonDiv">
                            <td>
                                <table class="tbl_width_600">
                                    <tr class="spacer10">
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <input type="button" value="Cancel" id="BTN_CloseNewCompany" class="btn_drkblue-1" />&nbsp;
                                        <input type="button" value="Save" id="BTN_SaveNewCompany" class="in_btn_blue" /></td>
                                    </tr>
                                    <tr class="spacer20">
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>


                    </table>
                </td>
            </tr>
        </table>

        </form>

    </div>
    <div id="DIV_Loading">
        <!-- Loading DIV -->
        <table class="tbl_width_120">
            <tr>
                <td style="text-align: center;">
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/loading.gif" Height="60px" Width="60px" />
                </td>
            </tr>
            <tr>
                <td class="fnt_pleasewait">Please wait...</td>
            </tr>
        </table>
    </div>


      
</asp:Content>
