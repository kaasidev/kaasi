﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Inward/Import.Master" AutoEventWireup="true" CodeBehind="importIFTI.aspx.cs" Inherits="KASI_Extend.importIFTI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style type="text/css">
        #gridDivSuccess, gridDivUnSuccess {
            max-width: 70%;
            margin: 0 auto;
            width: 70%;
            min-height: 150px;
            text-align: center;
            font-weight: bold;
        }

        .Grid {
            
        }

        .Grid th {
            font-family: 'Gotham Narrow Medium';
            font-size: 13px;
            padding: 10px 5px 10px 5px;
            color: #fff;
            background-color: #048ABB;
            border-bottom: 5px solid #2A7AAF;
        }
        /* CSS to change the GridLines color */
        .Grid td {
            border-bottom: 1px solid #EFEFEF;
            font-family: 'Gotham Narrow Book';
            padding-top: 6px;
            padding-bottom: 6px;
            font-size: 13px;
        }
        .rep_head_left {
            padding-left: 10px !important;
            text-align: left;
            border-right: 1px solid #0380ad;
        }
        .rep_head_right {
            padding-right: 10px !important;
            text-align: right;
            border-right: 1px solid #0380ad;
        }
        .rep_head_center {
            text-align: center;
            border-right: 1px solid #0380ad;
        }

        .FA_CL_03 {
            text-align: left !important;
            border-right: 1px solid #EFEFEF;
            width:950px;
            padding-left: 10px;
        }

        .SU_CL_01 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            width:50px;
        }
        .SU_CL_02 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            width:200px;
        }
        .SU_CL_03 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            width:90px;
        }
                        .SU_CL_04 {
            text-align: left !important;
            border-right: 1px solid #EFEFEF;
            width:280px;
            padding-left: 10px;
        }
        .SU_CL_05 {
            text-align: left !important;
            border-right: 1px solid #EFEFEF;
            width:280px;
            padding-left: 10px;
        }
        /* Bank */
        .SU_CL_06 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            width:80px;
        }
        /* AUD Amount */
        .SU_CL_07 {
            text-align: right !important;
            border-right: 1px solid #EFEFEF;
            padding-right: 10px;
            width:100px;
        }
        /* Status */
        .SU_CL_08 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
            width:120px;
        }

        .CL_01 {
            text-align: center !important;
            border-right: 1px solid #EFEFEF;
        }
        .CL_02 {
            text-align: right !important;
            border-right: 1px solid #EFEFEF;
            padding-right: 5px;
        }
        .CL_03 {
            text-align: left !important;
            border-right: 1px solid #EFEFEF;
            padding-left: 5px;
        }


        #BTN_Browse {
            cursor: pointer;
        }


        #drop_zone {
            font-family: 'Gotham Narrow Book' !important;
            max-width: 100%;
            margin: 0 auto;
            width: 100%;
            min-height: 150px;
            text-align: center;
            font-size: 14px;
            border: 1px dashed #999;
            height: 150px;
            background-color: #FFFFFF;
            border-radius: 10px;
            color: #BBB;
        }

        #spanCon {
            font-family: 'Gotham Narrow Medium' !important;
            color: #048ABB;
            font-size: 18px;
            display: block;
            vertical-align: middle;
            line-height: normal;
            margin-top: 50px;
            text-align: center;
        }

        .remitter_dropdown {
            font-family: 'Gotham Narrow Book' !important;
            font-size: 13px !important;
            height: 35px !important;
        }

        .brwse_btn {
            border: 1px dashed #999;
            text-align: center;
        }
        .disabledDropdown {
            cursor: default;
            background-color: #F4F5F7;
            color: #333 !important;
        }
        .failmessage {
            font-family: 'Gotham Narrow Medium';
            font-size: 16px;
            color: #C0392B;
            text-decoration: none;
            outline: none;
            font-weight: normal;
        }
        .successmessage {
            font-family: 'Gotham Narrow Medium';
            font-size: 16px;
            color: #3AC162;
            text-decoration: none;
            outline: none;
            font-weight: normal;
        }
    </style>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <form id="form1" runat="server">

        <div class="bg_DCDCDC">
            <table class="tbl_width_1200">
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 35px;">
                        <table class="tbl_width_1200">
                            <tr style="height: 35px;">
                                <td class="txn_tab_headings_blk" style="vertical-align: middle; color: #34495E; width: 1000px;">Hello Fazal, welcome back to KAASI...</td>
                                <td style="width: 200px;" onclick="location.href ='../Welcome.aspx'">
                                    <div class="in_btn_outward" style="display:none;">
                                        <table>
                                            <tr style="text-align: right;">
                                                <td style="width: 165px; vertical-align: middle;">Outward Transactions</td>
                                                <td style="width: 35px;">1</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer15">
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div class="bg_DCDCDC">
            <table class="tbl_width_1200">

                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td class="bg_EFEFEF" style="width: 285px; vertical-align: top;">
                                    <table class="tbl_width_285">
                                        <tr class="spacer15">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="tbl_width_255">
                                                    <tr>
                                                        <td class="dsh_headings" style="height: 20px; vertical-align: top;">Transactions</td>
                                                    </tr>
                                                    <tr class="spacer5">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_255">
                                                                <tr style="height: 75px;">
                                                                    <td style="background-color: #048ABB;" class="dsh_numbers_white"><span id="PendTransSPN" runat="server"></span></td>
                                                                    <td style="width: 10px;">&nbsp;</td>
                                                                    <td style="background-color: #DCDCDC;" class="dsh_numbers_black"><span id="TotalTansSPN" runat="server"></span></td>
                                                                    <td style="width: 10px;">&nbsp;</td>
                                                                    <td class="dsh_numbers_black" style="background-color: #DCDCDC;"><span id="ThisMonthTransSPN" runat="server"></span></td>
                                                                </tr>
                                                                <tr style="height: 25px;">
                                                                    <td style="background-color: #048ABB; width: 78px;" class="dsh_headings_white">Pending</td>
                                                                    <td style="width: 10px;">&nbsp;</td>
                                                                    <td class="dsh_headings_black" style="width: 79px; background-color: #DCDCDC;">Today</td>
                                                                    <td style="width: 10px;">&nbsp;</td>
                                                                    <td class="dsh_headings_black" style="width: 78px; background-color: #DCDCDC;">This Month</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="spacer15">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 20px; height: 111px;">&nbsp;</td>
                                <td class="bg_FFFFFF" style="width: 285px; vertical-align: top;">
                                    <table class="tbl_width_285">
                                        <tr style="height: 155px;">
                                            <td style="width: 140px; background-color: #EFEFEF; vertical-align: top;">
                                                <table class="tbl_width_140">
                                                    <tr class="spacer15">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="dsh_headings" style="height: 20px; vertical-align: top; text-align: center;">Failed KYC</td>
                                                    </tr>
                                                    <tr class="spacer5">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_110">
                                                                <tr>
                                                                    <td style="height: 97px; background-color: #DCDCDC; border-bottom: 6px solid #DB5E59;" class="dsh_numbers_red"><span id="SPN_FailedKYC" runat="server"></span></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="spacer15">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 5px; background-color: #DCDCDC;">&nbsp;</td>
                                            <td style="width: 140px; background-color: #EFEFEF; vertical-align: top;">
                                                <table class="tbl_width_140">
                                                    <tr class="spacer15">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="dsh_headings" style="height: 20px; vertical-align: top; text-align: center;">Pending KYC</td>
                                                    </tr>
                                                    <tr class="spacer5">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_110">
                                                                <tr>
                                                                    <td style="height: 97px; background-color: #DCDCDC; border-bottom: 6px solid #F5B854;" class="dsh_numbers_orange"><span id="SPN_PendingKYC" runat="server"></span></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="spacer15">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>


                                </td>
                                <td style="width: 20px;">&nbsp;</td>
                                <td class="bg_EFEFEF" style="width: 285px; vertical-align: top;">
                                    <table class="tbl_width_285">
                                        <tr class="spacer15">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="tbl_width_255">
                                                    <tr>
                                                        <td class="dsh_headings" style="height: 20px; vertical-align: top;">Recent Activity</td>
                                                    </tr>
                                                    <tr class="spacer5">
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 20px;">&nbsp;</td>
                                <td class="bg_FFFFFF" style="width: 285px; vertical-align: top;">
                                    <table class="tbl_width_285">
                                        <tr class="spacer15">
                                            <td>&nbsp;</td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>




        <div class="bg_EFEFEF">
            <table class="tbl_width_1200">
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td class="txn_tab_headings_blk" style="text-align: center;">Import Transactions/IFTI Files</td>
                                <td style="text-align: right;">
                                    <%--<asp:Button ID="Button2" runat="server"  OnClientClick="showAgentDropDiv();"Text="Import Transactions File"  class=""/>--%>
                                    <input type="button" class="btn_main_blue" style="display: none;" onclick="showAgentDropDiv();" value="Import Transactions File" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="page_contents_large" style="text-align: center; padding: 0px 175px;">Please select the Remitter (Company) from the drop down menu below to upload their Inward Transactions IFTI File. Then Browse for the file or simply drag and drop the file below.</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: center;">
                        <asp:DropDownList ID="agentDrop" runat="server" class="remitter_dropdown" ClientIDMode="Static" Style="width: 590px;"></asp:DropDownList></td>
                </tr>

                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>

                <!-- <<<< THIS IS THE TR WITH THE BROWSE AND DROP -->
                <tr id="TR_FileUploadManagement" runat="server">
                    <td>
                        <div id="agentDropDiv" style="display: none;"></div>
                        <div id="iploadSectionDiv">
                            <div id="drop_zone"><span id="spanCon">DRAG &amp; DROP YOUR IFTI FILE HERE</span><br />
                                Maximum File Size: 4MB</div>
                        </div>

                        <table class="tbl_width_1200">
                            <tr class="spacer20">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <asp:FileUpload ID="BTN_FileUpload" runat="server" accept=".xls,.xlsx" Style="display: none;" />
                                    <div style="padding-bottom: 10px;">
                                        <input id="BTN_Browse" class="in_btn_file_browse" type="button" value="Browse..." runat="server" /></div>
                                    <asp:Label ID="LBL_FileName" runat="server" class="page_contents_large"></asp:Label><br />
                                    <asp:Button ID="BTN_Uploadfile" runat="server" Text="Upload File" OnClientClick="return validaFileSize();" OnClick="BTN_Uploadfile_Click" Style="display: none; cursor: pointer;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td style="width: 285px; vertical-align: top;">&nbsp;</td>
                                <td style="width: 20px;">&nbsp;</td>
                                <td style="width: 895px;">
                                    <div id="TR_Stage2" style="display: none;">
                                        <table>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!--<tr id="TR_Stage1"><td>

                    <table>
                        <tr>
                            <td>Stage 1 - Choose your overseas remitter.</td>
                            <td>&nbsp;</td>
                            <td></td>
                        </tr>
                    </table>
                    
                                     </td></tr>-->


                <tr id="TR_ErrorMessage" runat="server" style="display: none;">
                    <td>
                        <asp:Label ID="lbl_ErrorMessage" runat="server" Text=""></asp:Label>
                    </td>
                </tr>

            </table>
        </div>

        <div id="DIV_ImportFile" runat="server" style="display: none;">
            <table class="tbl_width_1200">
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" runat="server" accept=".xls,.xlsx,.csv" /></td>
                                <td>
                                    <asp:Button ID="Button1" runat="server" Text="Import" OnClientClick="return validaFileSize();" OnClick="Button1_Click" /></td>
                                <td>
                                    <asp:Button ID="btn_DownloadIFTI" runat="server" Text="Download Import/IFTI Template" OnClick="btn_DownloadIFTI_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>&nbsp;</td>
                    <td></td>
                </tr>
            </table>



        </div>




        <div class="wiz_bg_EFEFEF">
            <!-- IMPORT RESULTS TABLE -->
            <table class="tbl_width_1200">
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr class="bg_DCDCDC">
                                <td style="width: 1180px; vertical-align:middle; padding-top:10px; padding-bottom:10px; padding-left: 10px; padding-right: 5px;">
                                    <table style="width:100%;">
                                        <tr>
                                            <td>
                                                <div id="buttonsection" runat="server">
                                                    <table style="width:1180px;">
                                                        <tr>
                                                            <td>
                                                                <span class="failmessage"><asp:Label ID="failureLabel" runat="server"></asp:Label></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                <span class="successmessage"><asp:Label ID="successlabel" runat="server"></asp:Label></span>
                                                            </td>
                                                            <td style="text-align:right;">
                                                                <asp:Button ID="refreshButton" Text="OK" runat="server" Visible="false" OnClick="refreshPage" class="in_btn_blue" />&nbsp;
                                                                <asp:Button ID="lastInsert" Text="Import" runat="server" Visible="false" OnClick="InsertRecords" class="in_btn_blue" />&nbsp;
                                                                <asp:Button ID="resetButton" Text="Reset" runat="server" Visible="false" OnClick="refreshPage" class="in_btn_red" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%-- <input type="button" value="Success" id="successbutton" onclick="showSuccess();" runat="server" visible="false" />
                                                    <input type="button" value="Failure" id="failurebutton" onclick="showFailure();" runat="server" visible="false" />--%>
                                                </div>
                                            </td>
                                            <td>&nbsp;
                                                </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td>

                        <div id="gridDivUnSuccess" style="display: none;" runat="server">
                            <div>&nbsp;</div>

                            <asp:GridView ID="GridView2" CssClass="Grid" runat="server" AllowPaging="true" PageSize="40" OnPageIndexChanging="GridView2_PageIndexChanging"
                                AutoGenerateColumns="False" EmptyDataText="No records" Width="100%">
                                <RowStyle BackColor="#DAE5F4" />
                                <AlternatingRowStyle BackColor="#B8D1F3" />
                                <Columns>


                                    <asp:TemplateField HeaderText="Row" ItemStyle-CssClass="SU_CL_01" HeaderStyle-CssClass="rep_head_center">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("RowNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TXN Reference" ItemStyle-CssClass="SU_CL_02" HeaderStyle-CssClass="rep_head_center">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("TransRefNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments" ItemStyle-CssClass="FA_CL_03" HeaderStyle-CssClass="rep_head_left">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("ErrorList") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </div>


                        <!-- SUCCESS TABLE -->

                        <div id="gridDivSuccess" style="display: none;" runat="server">
                            <div>&nbsp;</div>
                            <asp:GridView ID="GridView1" CssClass="Grid" runat="server" AllowPaging="true" PageSize="40" OnPageIndexChanging="GridView1_PageIndexChanging"
                                AutoGenerateColumns="False" EmptyDataText="No records" Width="100%">
                                <RowStyle BackColor="#DAE5F4" />
                                <AlternatingRowStyle BackColor="#B8D1F3" />
                                <Columns>


                                    <asp:TemplateField HeaderText="Row" ItemStyle-CssClass="SU_CL_01" HeaderStyle-CssClass="rep_head_center">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("RowNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TXN Reference" ItemStyle-CssClass="SU_CL_02" HeaderStyle-CssClass="rep_head_center">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("TransRefNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TXN Date" ItemStyle-CssClass="SU_CL_03" HeaderStyle-CssClass="rep_head_center">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("MoneyRecFromCustDateTime", "{0:d}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sender Name" ItemStyle-CssClass="SU_CL_04" HeaderStyle-CssClass="rep_head_left">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("FullName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Beneficiary Name" ItemStyle-CssClass="SU_CL_05" HeaderStyle-CssClass="rep_head_left">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("BeneFullName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bank" ItemStyle-CssClass="SU_CL_06" HeaderStyle-CssClass="rep_head_center">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("BeneNameOfInstitution") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AUD Amount" ItemStyle-CssClass="SU_CL_07" HeaderStyle-CssClass="rep_head_right">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("TotalAmount", "{0:N2}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="SU_CL_08" HeaderStyle-CssClass="rep_head_center">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelName" runat="server" Text='<%# Eval("ErrorList") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </div>





                    </td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
            </table>




        </div>
        <!-- IMPORT RESULTS TABLE -->


        <div id="DIV_ErrorMsg">
            &nbsp;
        </div>
        <asp:HiddenField ID="selectedDropAgent" runat="server" ClientIDMode="Static" />
    </form>


    <script type="text/javascript">
        var ErrorMSGDIV;
        var canCallErrorBox = '<%=canCall%>';
        $(document).ready(function () {

            if ($("#selectedDropAgent").val() != "") {
                $("#agentDrop").val($("#selectedDropAgent").val());
                $("#agentDrop").prop("disabled", true);
                $("#agentDrop").addClass("disabledDropdown");
            }

            $('#<%=agentDrop.ClientID%>').change(function () {
                $("#ContentMainSection_gridDivUnSuccess").hide();
                $("#ContentMainSection_gridDivSuccess").hide();
                if ($('#<%=agentDrop.ClientID%> :selected').val() != "-- SELECT --") {

                    $('#<%=TR_FileUploadManagement.ClientID%>').css("display", "block");

                    $('#TR_Stage2').css("display", "block");
                    $('#<%=BTN_Browse.ClientID%>').css("display", "inline");
                    $("#selectedDropAgent").val($("#agentDrop").val());

                    $("#agentDrop").prop("disabled", true);
                    $("#agentDrop").addClass("disabledDropdown");
                }
                else {
                    $('#TR_FileUploadManagement').css("display", "none");
                    $('#TR_Stage2').css("display", "none");
                    $('#<%=BTN_Browse.ClientID%>').css("display", "none");
                    $("#agentDrop").prop("disabled", false);
                    $("#agentDrop").removeClass("disabledDropdown");
                }

                $("#ContentMainSection_buttonsection").hide();

            });

            $('#<%=BTN_Browse.ClientID%>').click(function () {
                document.getElementById('<%=BTN_FileUpload.ClientID%>').click();
            });



            ErrorMSGDIV = $("#DIV_ErrorMsg").dialog({
                modal: true,
                autoOpen: false,
                width: 480,
            });

            $('#<%=BTN_FileUpload.ClientID%>').change(function () {
                 var path = $(this).val();
                 if (path != '' && path != null) {
                     var q = path.substring(path.lastIndexOf('\\') + 1);
                     $('#<%=LBL_FileName.ClientID%>').html(q);
                     $('#<%=BTN_Uploadfile.ClientID%>').css("display", "inline");
                 }
             });



             $('#agentDrop').change(function () {
                 $('#TR_Stage2').show();
             })


             if (canCallErrorBox) {

                 $("#DIV_ErrorMsg").text(canCallErrorBox);
                 ErrorMSGDIV.dialog('open');
             }


        });


        function showAgentDropDiv() {
            console.log('test');
            $("#agentDropDiv").show();
        }

        //function errorDialog(errorMsg) {
        //    console.log('test');


        //    $("#DIV_ErrorMsg").dialog('open');
        //}

        function validaFileSize() {

            var ccc = 1024 * 1024 * 4;

            if ((document.getElementById("ContentMainSection_BTN_FileUpload").files[0].size) > ccc) {
                alert("Maximum file size is 4MB.");
                return false;
            }

            return true;
        }


        function showSuccess() {

            $("#ContentMainSection_gridDivSuccess").show();
            $("#ContentMainSection_gridDivUnSuccess").hide();
        }

        function showFailure() {
            $("#ContentMainSection_gridDivUnSuccess").show();
            $("#ContentMainSection_gridDivSuccess").hide();
        }


        function validateFileType() {


            var fileExtension = ['xls'];
            var imUpload = getInboundFile();

            if (imUpload.val() == "") {
                alert("Please upload file with an extension of xls.");
                return false;
            }


            if ($.inArray(imUpload.val().split('.').pop().toLowerCase(), fileExtension) == -1) {

                alert("Only .xls file can be allowed.");
                return false;
            }

            return true;

        }

        function getInboundFile() {
            return $("#FileUpload1");


        }

        if (window.File && window.FileList && window.FileReader) {
            /************************************ 
             * All the File APIs are supported. * 
             * Entire code goes here.           *
             ************************************/


            /* Setup the Drag-n-Drop listeners. */
            var dropZone = document.getElementById('drop_zone');
            dropZone.addEventListener('dragover', handleDragOver, false);
            dropZone.addEventListener('drop', handleDnDFileSelect, false);

        }
        else {
            alert('Sorry! this browser does not support HTML5 File APIs.');
        }


        function handleDragOver(event) {
            event.stopPropagation();
            event.preventDefault();
            var dropZone = document.getElementById('spanCon');
            dropZone.innerHTML = "Drop now";
        }

        function handleDnDFileSelect(event) {
            event.stopPropagation();
            event.preventDefault();
            var agId = $("#agentDrop").val();
            /* Read the list of all the selected files. */
            files = event.dataTransfer.files;

            /* Consolidate the output element. */
            var form = document.getElementById('form1');
            var data = new FormData(form);

            for (var i = 0; i < files.length; i++) {
                s = files[i].name;
                //fileNameChange = files[i].name;
                //if (s != "")
                //    fileNameChange = s;
                data.append(files[i].name, files[i]);

            }
            if (s != null) {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200 && xhr.responseText) {
                        //alert("Upload done!");
                        var dropZone = document.getElementById('spanCon');
                        dropZone.innerHTML = "File Successfully Uploaded.";
                        //window.location.href = window.location.href+"u=1";
                        //window.location.reload();

                        console.log(xhr.responseText);
                        // alert(xhr.responseText);
                        window.location = window.location.pathname + "?u=1";
                    } else {
                        //alert("upload failed!");
                    }
                };
                xhr.open('POST', "importIFTI.aspx?q=1");
                // xhr.setRequestHeader("Content-type", "multipart/form-data");
                xhr.send(data);
            }
            else {
                var dropZone = document.getElementById('spanCon');
                dropZone.innerHTML = "Drop files here";

            }

        }

    </script>

</asp:Content>
