﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="ManageInbound.aspx.cs" Inherits="KASI_Extend.ManageInbound" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>

    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/toastr.min.css" rel="stylesheet" />
	<link href="css/datatables.min.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            function showerror(msg) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 6000
                };
                toastr.error(msg, 'Error');
            }
            function showsuccess(msg, body) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 6000
                };
                toastr.success(msg, body);
            }


        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">



</asp:Content>
