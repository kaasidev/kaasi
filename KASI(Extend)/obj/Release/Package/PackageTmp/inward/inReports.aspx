﻿<%@ Page Title="" Language="C#" MasterPageFile="Import.Master" AutoEventWireup="true" CodeBehind="inReports.aspx.cs" Inherits="KASI_Extend.Inwards.inReports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .in_rep_btn {
            background-color: #048ABB;
            width: 100%;
            color: #FFFFFF;
            font-family: 'Gotham Narrow Medium' !important;
            font-size: 14px !important;
            height: 45px;
            margin-bottom: 0px !important;
            border-radius: 0 !important;
            border: 5px solid #FFFFFF;
        }
        .in_rep_btn:hover {
            cursor: pointer;
            background-color: #04A1DB;
        }
        .in_rep_heading {
            font-family: 'Gotham Narrow Medium' !important;
            font-size: 14px !important;
            color: #666;
            vertical-align: top;
            text-align: center;
            line-height: 15px;
            height: 30px;
        }

    </style>

    <script type="text/javascript">

        $(document).ready(function () {

            var IFTIModal = $('#IFTIModal').dialog({
                modal: true,
                autoOpen: false,
                width: 480,

            });

            $('#btnModalOpenAustract').click(function () {
                IFTIModal.dialog('open');
            })

        });


        </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">
        <div class="bg_DCDCDC">
            <table class="tbl_width_1200">
                <tr class="spacer20">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="page_headings">Inward Reports</td>
                    <td style="text-align:right"><asp:Button ID="btnModalOpenAustract" runat="server" Text="Create Austrac IFTI File" CssClass="aa_btn_red" /></td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

    <div class="bg_EFEFEF">
        <table class="tbl_width_1200">
            <tr class="spacer20"><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table class="tbl_width_1200">
                        <tr>
                            <td class="reports_white_box" style="vertical-align:top;">
                                <table style="width:100%;">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_160">
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                <tr><td style="text-align:center;"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/notepad_av.png" /></td></tr>
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                <tr><td class="in_rep_heading">Inward Transactions Report</td></tr>
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td><asp:Button ID="Button1" runat="server" Text="SELECT" CssClass="in_rep_btn" OnClick="Button1_Click" /></td></tr>
                                </table>
                            </td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="reports_white_box" style="vertical-align:top;">
                                <table style="width:100%;" class="background_FFFFFF">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_160">
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                <tr><td style=" height:100px; text-align:center;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/notepad_av.png" /></td></tr>
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                <tr><td class="in_rep_heading">KYC Failed Beneficiaries Report</td></tr>
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td><asp:Button ID="Button2" runat="server" Text="SELECT" CssClass="in_rep_btn" OnClick="Button2_Click" /></td></tr>
                                </table>
                            </td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="reports_white_box" style="vertical-align:top;">
                                <table style="width:100%;">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_160">
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                <tr><td style="text-align:center;"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/notepad_av.png" /></td></tr>
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                                <tr><td class="in_rep_heading">Monthly Revenue Report</td></tr>
                                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td><asp:Button ID="Button3" runat="server" Text="SELECT" CssClass="in_rep_btn" OnClick="Button3_Click" /></td></tr>
                                </table>
                            </td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="reports_white_box_none">&nbsp;</td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="reports_white_box_none">&nbsp;</td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="reports_white_box_none">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td class="spacer20">&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>

        <div id="IFTIModal" >
        <table class="tbl_width_480">
            <tr><td style="background-color:#76D17F;" class="spacer5">&nbsp;</td></tr>
            <tr>
                <td style="background-color:#F7F9FA; border-bottom:1px solid #e5e5e5;">
                    <table class="tbl_width_440">
                        <tr><td class="modal-heading">Create AUSTRAC IFTI File</td></tr>
                    </table>
                </td>
            </tr>
         </table>
         <div id="DIV_file_yes">
             <table class="tbl_width_440">
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
                 <tr><td class="modal-font">You are about to generate the Austrac IFTI File. This process may take several minutes and you will not be able to perform any other function until this is completed.</td></tr>
                 <tr><td>&nbsp;</td></tr>
                 <tr>
                     <td>
                         <table>
                             <tr><td class="modal-font">Starting Transaction: <span id="IFTIStartTrans" runat="server"></span></td><td>&nbsp;</td></tr>
                             <tr><td class="modal-font">Ending Transaction: <span id="IFTIEndTrans" runat="server"></span></td><td>&nbsp;</td></tr>
                             <tr><td class="modal-font">Total Number of Transaction:</td><td><span id="IFTITotalTrans" runat="server"></span></td></tr>
                         </table>
                     </td>
                 </tr>
                 
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
             </table>
         </div>
         <div id="DIV_file_processing">
             <table class="tbl_width_440">
                 <tr><td>&nbsp;</td></tr>
                 <tr><td><span id="SPNPleaseWait" runat="server" style="display:none;">Please wait...processing...</span>Processing</td></tr>
                 <tr><td>&nbsp;</td></tr>
             </table>
         </div>
         <div id="DIV_file_no">
             <table class="tbl_width_440">
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
                 <tr><td class="normal_font">You do not have any unreported transactions.</td></tr>
                 <tr style="height:20px;"><td>&nbsp;</td></tr>
             </table>
         </div>
         <table class="tbl_width_480">
            <tr>
                <td style="background-color:#F7F9FA; border-top:1px solid #e5e5e5;">
                    <table class="tbl_width_440">
                        <tr class="spacer15"><td>&nbsp;</td></tr>
                        <tr><td style="text-align:right;"><asp:Button runat="server" CssClass="aa_btn_red" Text="Create File" ID="btnAustract" CausesValidation="False"/>&nbsp;<asp:Button runat="server" UseSubmitBehavior="false" CssClass="aa_btn_red" Text="Close" ID="btnCloseAustrac" /></td></tr>
                        <tr class="spacer15"><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
         </table>
     </div>  




    </form>
</asp:Content>
