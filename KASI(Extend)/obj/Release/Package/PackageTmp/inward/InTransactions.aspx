﻿<%@ Page Title="" Language="C#" MasterPageFile="Import.Master" AutoEventWireup="true" CodeBehind="InTransactions.aspx.cs" Inherits="KASI_Extend.Inwards.InTransactions" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- JS FILES -->
    <script type="text/javascript" charset="utf-8" src="../js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/jquery.dataTables.min.js"></script>

    <!-- CSS FILES -->
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="../css/datatables.min.transactions.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/kaasi_common.css" />

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet" />
    <script type="text/javascript">
        var ReviewDialog;

        function displayReviewNotes(sTransID) {
            //alert(sCustID);
            $.ajax({
                url: '../JQDataFetch/getInwardTransNotes.aspx',
                async: false,
                data: {
                    TID: sTransID,
                },
                success: function (response) {
                    $('#DIV_Notes').empty();
                    if (response != "NORECORDS") {
                        var initialsplit = response.split('~');

                        $.each(initialsplit, function (item) {
                            var datasplit = initialsplit[item].split('|');

                            $('#DIV_Notes').append('<table class="tbl_width_540"><tr><td class="trn_notes_heading">' + datasplit[2] + ' - ' + datasplit[0] + ' - ' + datasplit[3] + ' (' + datasplit[4] + ')' + '</td></tr><tr><td class="trn_notes_contents">' + datasplit[1] + '</td></tr><tr class="spacer10"><td>&nbsp;</td></tr></table>');
                        });
                    }

                }
            })
        }



        $(document).ready(function () {

            $('#TXT_Add_TRN_Notes').keyup(function () {
                
                if ($('#TXT_Add_TRN_Notes').val().length > 0) {
                    $('#btn_SaveNewNote').css("display", "inline");
                }
                else {
                    $('#btn_SaveNewNote').css("display", "none");
                }

            });

            var ManualKYCDialog = $('#DIV_ManualKYC').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "Manual KYC",
            });

            $('#BTN_ManualKYC').click(function () {
                //ManualKYCDialog.dialog('open');
                $('#TR_ManualKYC').css('display', 'table-row');
                $('#BTN_ManualKYC').css('display', 'none');
            });

            $('#TD_CloseMKYCDialog').click(function () {
                ManualKYCDialog.dialog('close');
            });


            $('#BTN_ApproveKYC').click(function () {
                if ($('#TXT_KYCApprovalReason').val() != "") {
                    $.ajax({
                        url: 'Processor/ManualPassKYC.aspx',
                        data: {
                            BID: $('#TXT_InBeneficiaryID').val(),
                            KYCComments: $('#TXT_KYCApprovalReason').val()
                        },
                        success: function (response) {
                            if (response == "OK") {
                                alert("KYC has been successfully approved.");
                                callCustomerData($('#TXT_TransID').val());
                                $("#beneinboundId").val($('#TXT_InBeneficiaryID').val());
                                $('#TR_ManualKYC').css('display', 'none');
                                callKycCheck();
                                AllTransDataTable.ajax.reload();
                                KycFailedDatatable.ajax.reload();
                            }
                            else {
                                alert(response);
                            }
                        }
                    })
                }
                else {
                    alert('Please enter a reason before accepting KYC');
                }
                
            });

            $('#BTN_DeclinedKYC').click(function () {
                if ($('#TXT_KYCApprovalReason').val() != "") {
                    $.ajax({
                        url: 'Processor/DeclineManualKYC.aspx',
                        data: {
                            BID: $('#TXT_InBeneficiaryID').val(),
                            KYCComments: $('#TXT_KYCApprovalReason').val()
                        },
                        success: function (response) {
                            if (response == "OK") {
                                alert("KYC has been flagged as failed.");
                                callCustomerData($('#TXT_TransID').val());
                                $("#beneinboundId").val($('#TXT_InBeneficiaryID').val());
                                $('#TR_ManualKYC').css('display', 'none');
                                callKycCheck();
                                AllTransDataTable.ajax.reload();
                                KycFailedDatatable.ajax.reload();
                            }
                            else {
                                alert(response);
                            }
                        }
                    })
                }
                else {
                    alert('Please enter a reason before accepting KYC');
                }

            });

            

            var KycFailedDatatable = $('#failedKYCtransactions').DataTable({
                columns: [

                    { 'data': 'KAASIID' },
                    { 'data': 'BeneficiaryID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'AltTransID' },
                    { 'data': 'BeneficiaryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'Status' },

                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: '../DataHandlers/FailedKYCInwardsTransactionsDataHandler.ashx',
                "order": [[0, "desc"]],
            });

            var AllTransDataTable = $('#alltransactions').DataTable({
                columns: [

                    { 'data': 'KAASIID' },
                    { 'data': 'BeneficiaryID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'AltTransID' },
                    { 'data': 'BeneficiaryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'Status' },
                    { 'data': 'KYCStatus'}

                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: '../DataHandlers/AllInwardsTransactions.ashx',
                "order": [[0, "desc"]],
            });

            var PendTransDataTable = $('#pendingtransactions').DataTable({
                columns: [

                    { 'data': 'KAASIID' },
                    { 'data': 'BeneficiaryID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'AltTransID' },
                    { 'data': 'BeneficiaryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'Status' },

                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: '../DataHandlers/PendingInwardsTransactions.ashx',
                "order": [[0, "desc"]],
            });

            ReviewAuditTable = $('#TBL_REWV_Audit').DataTable({

            });

            ReviewDocsTable = $('#REWV_Docs').DataTable({
                "columnDefs": [
                    { "width": "145px", "targets": 1 }, // TXN REF
                ],
            });

            $("#DIV_CompReview_Main").show();
            $("#DIV_CompReview_Limits").hide();
            $("#DIV_CompReview_Notes").hide();
            $("#DIV_CompReview_Documents").hide();
            $("#DIV_CompReview_Audit").hide();
            setTranPopWindow();
            setTransactionTableClickEvent();

            $(".ui-icon-closethick").click(function () {
                reloadPageTran();
            });

            //$('#DIV_PendingTransactionsTable').css('display', 'none');

            // EVENT HANDLERS

            function resetTabs() {
                $("#DIV_CompReview_Main").show();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("modal_tab_active");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("modal_tab_inactive_right");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("modal_tab_inactive_right");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("modal_tab_inactive_right");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("modal_tab_inactive_right");
            }

            $('#Tab02').click(function () {
                $("tabclicked").val(2);
                secondTabClicked();

            });

            $('#Tab01').click(function () {
                $("tabclicked").val(1);
                firstTabClicked();
            })

            $('#Tab03').click(function () {
                $("tabclicked").val(3);
                thirdTabClicked();
            })

            $('#TD_CloseDialog').click(function () {
                ReviewDialog.dialog('close');
                resetTabs();
            });

            $('#BTN_CloseReviewWindow').click(function () {
                ReviewDialog.dialog('close');
                resetTabs();
            })

            

        });

        function firstTabClicked() {
            $('#DIV_AllTransactionsTable').css('display', 'none');
            $('#DIV_PendingTransactionsTable').css('display', 'block');
            $('#DIV_FailedKYCTransactionsTable').css('display', 'none');
            $('#Tab02').removeClass();
            $('#Tab02').addClass("tabs_style_01_inactive");
            $('#Tab03').removeClass();
            $('#Tab03').addClass("tabs_style_01_inactive");
        }
        function secondTabClicked() {
            $('#DIV_AllTransactionsTable').css('display', 'block');
            $('#DIV_PendingTransactionsTable').css('display', 'none');
            $('#DIV_FailedKYCTransactionsTable').css('display', 'none');
            $('#Tab02').removeClass();
            $('#Tab02').addClass("tabs_style_01_active");
            $('#Tab03').removeClass();
            $('#Tab03').addClass("tabs_style_01_inactive");
        }

        function thirdTabClicked() {
            $('#DIV_AllTransactionsTable').css('display', 'none');
            $('#DIV_PendingTransactionsTable').css('display', 'none');
            $('#DIV_FailedKYCTransactionsTable').css('display', 'block');
            $('#Tab02').removeClass();
            $('#Tab02').addClass("tabs_style_01_inactive");
            $('#Tab03').removeClass();
            $('#Tab03').addClass("tabs_style_01_active");
        };

        


        function setTransactionTableClickEvent() {

            $('#failedKYCtransactions tbody').on('click', 'tr', function (event) {
                var id = this.id;;
                $('#TR_ManualKYC').css('display', 'none');
                var sTransID = $('td', this).eq(0).text();
                var beneId = $('td', this).eq(1).text();
                $("#beneinboundId").val(beneId);
                $("#transactID").val(sTransID);
                $('#TXT_InBeneficiaryID').val(beneId);
                $('#TXT_TransID').val(sTransID);

                callCustomerData(sTransID);

                callKycCheck();

                displayReviewNotes(sTransID);

                $("#hidden_CustID").val(beneId);
                $('#hidden_TransID').val(sTransID);

                ReviewDocsTable.destroy();

                ReviewDocsTable = $('#REWV_Docs').DataTable({
                    ajax: {
                        url: '../JQDataFetch/getIncomingFiles.aspx',
                        async: false,
                        data: {
                            CID: beneId,
                            TID: sTransID,
                        }
                    }
                });

                ReviewAuditTable.destroy();
                ReviewAuditTable = $("#TBL_REWV_Audit").DataTable({
                    //initComplete: function () {
                    //    $("#TBL_REWV_Audit_filter").detach().appendTo('#new-search-area');
                    //},
                    "bLengthChange": false,
                    "columnDefs": [
                        { className: "tbl_audit_c1", "targets": [0] },
                        { className: "tbl_audit_c2", "targets": [1] },
                    ],
                    ajax: {
                        url: '../JQDataFetch/getInTransAudit.aspx',
                        async: false,
                        data: {
                            TID: sTransID,
                        }
                    },


                });
            })

            $('#pendingtransactions tbody').on('click', 'tr', function (event) {
                var id = this.id;;

                var sTransID = $('td', this).eq(0).text();
                var beneId = $('td', this).eq(1).text();
                $("#beneinboundId").val(beneId);
                $("#transactID").val(sTransID);

                callCustomerData(sTransID);

                callKycCheck();

                displayReviewNotes(sTransID);

                $("#hidden_CustID").val(beneId);
                $('#hidden_TransID').val(sTransID);

                ReviewDocsTable.destroy();

                ReviewDocsTable = $('#REWV_Docs').DataTable({
                    ajax: {
                        url: '../JQDataFetch/getIncomingFiles.aspx',
                        async: false,
                        data: {
                            CID: beneId,
                            TID: sTransID,
                        }
                    }
                });

                ReviewAuditTable.destroy();
                ReviewAuditTable = $("#TBL_REWV_Audit").DataTable({
                    //initComplete: function () {
                    //    $("#TBL_REWV_Audit_filter").detach().appendTo('#new-search-area');
                    //},
                    "bLengthChange": false,
                    "columnDefs": [
                        { className: "tbl_audit_c1", "targets": [0] },
                        { className: "tbl_audit_c2", "targets": [1] },
                    ],
                    ajax: {
                        url: '../JQDataFetch/getInTransAudit.aspx',
                        async: false,
                        data: {
                            TID: sTransID,
                        }
                    },


                });
            });

            $('#alltransactions tbody').on('click', 'tr', function (event) {
                var id = this.id;;

                var sTransID = $('td', this).eq(0).text();
                var beneId = $('td', this).eq(1).text();
                $("#beneinboundId").val(beneId);
                $("#transactID").val(sTransID);
               
                callCustomerData(sTransID);

                callKycCheck();

                displayReviewNotes(sTransID);

                $("#hidden_CustID").val(beneId);
                $('#hidden_TransID').val(sTransID);

                ReviewDocsTable.destroy();

                ReviewDocsTable = $('#REWV_Docs').DataTable({
                    ajax: {
                        url: '../JQDataFetch/getIncomingFiles.aspx',
                        async: false,
                        data: {
                            CID: beneId,
                            TID: sTransID,
                        }
                    }
                });

                ReviewAuditTable.destroy();
                ReviewAuditTable = $("#TBL_REWV_Audit").DataTable({
                    //initComplete: function () {
                    //    $("#TBL_REWV_Audit_filter").detach().appendTo('#new-search-area');
                    //},
                    "bLengthChange": false,
                    "columnDefs": [
                        { className: "tbl_audit_c1", "targets": [0] },
                        { className: "tbl_audit_c2", "targets": [1] },
                    ],
                    ajax: {
                        url: '../JQDataFetch/getInTransAudit.aspx',
                        async: false,
                        data: {
                            TID: sTransID,
                        }
                    },


                });

            });


            $("#btn_SaveNewNote").click(function () {
                if ($('#TXT_Add_TRN_Notes').val() != "") {
                    $.ajax({
                        url: '../Processor/AddNewInTransNote.aspx',
                        data: {
                            TID: $("#hidden_TransID").val(),
                            CID: $('#hidden_CustID').val(),
                            Notes: $('#TXT_Add_TRN_Notes').val(),
                        },
                        success: function (response) {
                            alert('Notes added successfully');
                            $("#TXT_Add_TRN_Notes").val('');
                            $('#DIV_Notes').empty();
                            displayReviewNotes($("#hidden_TransID").val());
                            ReviewAuditTable.ajax.reload();
                        }
                    })
                }
            });
        }

        function callCustomerData(sTransID) {

            $.ajax({
                url: '../JQDataFetch/getInwardTransactionCustomer.aspx',
                async: false,
                data: {
                    trID: sTransID,
                },
                dataType: "json",
                success: function (data) {
                    
                    $("#REVW_DollarAmount").text(data.DollarAmount + ' AUD');
                    $("#REWV_AgentRefID").text(data.AlTransID);
                    $('#REWV_KAASIID').text(data.KAASIRefID);
                    $('#TransactionDate').text(data.MoneyRecDate);
                    $("#REVW_CustomerID").text(data.CustomerName);
                    $("#REVW_CustAddress").text(data.CustomerAddress);
                    $("#REVW_CustMobile").text(data.ContactNo);
                    $("#REVW_Purpose").text(data.Purpose);
                    $("#REVW_BeneName").text(data.BeneficiaryName);
                    $('#REVW_BeneAddress').text(data.BeneFullAddress);
                    $("#Last30daysDollar").text(data.MonthlyAudAmount);
                    $("#LastYearDollar").text(data.YearlyAudAmount);
                    $("#Last30daysNb").text(data.MonthlyTransaction);
                    $("#LastYearNb").text(data.YealryTransaction);
                    $("#REVW_AffiliateName").text(data.AffiliateName);
                    //alert(data.KYCStatus);
                    if (data.KYCStatus == null) {
                        $('#TD_KYCMessage').text('WARNING! KYC Check has not been completed.');
                        $('#BTN_RunKYCCheck').css("display", "inline");
                        $('#BTN_ManualKYC').css("display", "none");
                    }
                    else if (data.KYCStatus.indexOf("Success") >= 0) {
                        $('#TD_KYCMessage').removeClass();
                        $('#TD_KYCMessage').addClass("line_err_success");
                        $('#TD_KYCMessage').text('SUCCESS! KYC Check has been completed.');
                        $('#BTN_RunKYCCheck').css("display", "none");
                        $('#BTN_ManualKYC').css("display", "none");
                    }
                    else if (data.KYCStatus.indexOf("Manual") >= 0) {
                        if (data.KYCStatus.indexOf("Failed") >= 0) {
                            $('#TD_KYCMessage').removeClass();
                            $('#TD_KYCMessage').addClass("line_err_danger");
                            $('#TD_KYCMessage').text('DANGER! KYC Check has failed.');
                            $('#BTN_RunKYCCheck').css("display", "none");
                            $('#BTN_ManualKYC').css("display", "inline");
                        }
                        else {
                            $('#TD_KYCMessage').removeClass();
                            $('#TD_KYCMessage').addClass("line_err_success");
                            $('#TD_KYCMessage').text('SUCCESS! KYC Check has been completed.');
                            $('#BTN_RunKYCCheck').css("display", "none");
                            $('#BTN_ManualKYC').css("display", "none");
                        }
                    }
                    else if (data.KYCStatus.indexOf("Failed") >= 0) {
                        $('#TD_KYCMessage').removeClass();
                        $('#TD_KYCMessage').addClass("line_err_danger");
                        $('#TD_KYCMessage').text('DANGER! KYC Check has failed.');
                        $('#BTN_RunKYCCheck').css("display", "none");
                        $('#BTN_ManualKYC').css("display", "inline");
                    }
                    

                }
            });

            ReviewDialog.dialog('open');
        }



        function setTranPopWindow() {

            ReviewDialog = $('#ReviewTransactionDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "Transaction Details",
            });


            $("#Review_TD_001").click(function () {
                $("#DIV_CompReview_Main").show();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("modal_tab_active");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("modal_tab_inactive_right");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("modal_tab_inactive_right");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("modal_tab_inactive_right");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("modal_tab_inactive_right");
            });

            // LIMITS
            $("#Review_TD_002").click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").show();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("modal_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("modal_tab_active");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("modal_tab_inactive_right");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("modal_tab_inactive_right");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("modal_tab_inactive_right");
            });

            // NOTES
            $("#Review_TD_003").click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").show();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("modal_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("modal_tab_inactive_left");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("modal_tab_active");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("modal_tab_inactive_right");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("modal_tab_inactive_right");
            });

            // DOCUMENTS
            $("#Review_TD_004").click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Audit").hide();
                $("#DIV_CompReview_Documents").show();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("modal_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("modal_tab_inactive_left");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("modal_tab_inactive_left");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("modal_tab_active");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("modal_tab_inactive_right");
            });

            $('#Review_TD_005').click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").show();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("modal_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("modal_tab_inactive_left");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("modal_tab_inactive_left");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("modal_tab_inactive_left");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("modal_tab_active");
            })

            $('#PendTD').click(function () {
                $('#DIV_PendingTrans').show();
                $('#DIV_AllTrans').hide();
                $('#DIV_ComplianceTable').hide();
                $('#DIV_CancelledTable').hide();
                $('#DIV_AdminReview').hide();
                $('#PendTD').removeClass();
                $('#PendTD').addClass("tr_tabs_active");
                $('#CompTD').removeClass();
                $('#CompTD').addClass("tr_tabs_inactive_right");
                $('#FailTD').removeClass();
                $('#FailTD').addClass("tr_tabs_inactive_right");
                $('#AllTD').removeClass();
                $('#AllTD').addClass("tr_tabs_inactive_right");
            });
        }

        function runKycCheck() {
            var beneId = $("#beneinboundId").val();
            if (beneId) {
                //callLoadingDiv();
                PageMethods.RunKycCheckStatus(beneId, successCall);
            }
            
        }


        function callKycCheck() {
            var beneId = $("#beneinboundId").val();
            if (beneId) {
                //callLoadingDiv();
                PageMethods.CheckKCYStatus(beneId, successCall);
            }
            


        }

        function successCall(res) {

            //callHidingDiv();

           // $("#dsh_KYC_Success").hide();
            //$("#dsh_KYC_Fail").hide();
            //$("#dsh_KYC_NotRun").hide();

            $("#dsh_KYC_DateLabel").hide();
            $("#dsh_KYC_DateValue").hide();
            $("#dsh_KYC_DateValue").text('');

            $("#dsh_KYC_ByLabel").hide();
            $("#dsh_KYC_ByValue").hide();
            $("#dsh_KYC_ByValue").text('');

            $("#dsh_KYC_RefLabel").hide();
            $("#dsh_KYC_RefValue").hide();
            $("#dsh_KYC_RefValue").text('');

            $("#dsh_KYC_InfoLabel").hide();
            $("#dsh_KYC_InfoValue").hide();
            $("#dsh_KYC_InfoValue").text('');

            //console.log(res.Status);
            
            if (res.Status != null) {
                if (res.Status == 0 || res.Status == -1) {
                    $('#TR_KYCResults').css("display", "table-row");
                }
                else {
                    $('#TR_KYCResults').css("display", "none");
                }
                if (res.KyCheckDate != null) {

                    $("#dsh_KYC_DateLabel").show();
                    $("#dsh_KYC_DateValue").show();
                    $("#dsh_KYC_DateValue").text(res.KyCheckDate);
                }
                if (res.KycBy != null) {

                    $("#dsh_KYC_ByLabel").show();
                    $("#dsh_KYC_ByValue").show();
                    $("#dsh_KYC_ByValue").text(res.KycBy);
                }
                if (res.TransactionId != null) {

                    $("#dsh_KYC_RefLabel").show();
                    $("#dsh_KYC_RefValue").show();
                    $("#dsh_KYC_RefValue").text(res.TransactionId);
                }
                if (res.ResultDescription != null) {

                    $("#dsh_KYC_InfoLabel").show();
                    $("#dsh_KYC_InfoValue").show();
                    $("#dsh_KYC_InfoValue").text(res.ResultDescription);
                }
                //$("#dsh_KYC_Success").show();
            }
            else {
                $('#TR_KYCResults').css("display", "none;");
            }
            
               
                

            


        }

        function reloadPageTran() {
            location.reload();
        }

        function callLoadingDiv() {
            // $("#buttonyesKychidden").val("Y");
            //$("#DIV_Loading").show();
        }
        function callHidingDiv() {
            // $("#buttonyesKychidden").val("Y");
            //$("#DIV_Loading").hide();
        }


    </script>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="inboundListTranScript" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <div class="bg_DCDCDC">
            <table class="tbl_width_1200">
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="page_headings">Inward Transactions</td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div class="bg_EFEFEF">
            <div>
                <table class="tbl_width_1200">
                    <tr class="spacer20">
                        <td>
                            <input id="hidden_CustID" type="text" style="display:none;" /><input id="hidden_TransID" type="text" style="display:none;"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="tbl_width_1200">
                                <tr>
                                    
                                    <td class="tabs_style_01_active" style="width: 75px;" id="Tab01"><asp:Image ID="Image002" runat="server" ImageUrl="images/alltxns.png" title="PENDING TRANSACTIONS" /></td>
                                    <td class="width_spacer5">&nbsp;</td>
                                    <td class="tabs_style_01_inactive" style="width: 75px;" id="Tab02"><asp:Image ID="Image001" runat="server" ImageUrl="images/alltxns.png" title="ALL TRANSACTIONS" /></td>
                                    <td class="width_spacer5">&nbsp;</td>
                                    <td class="tabs_style_01_inactive" style="width: 75px;" id="Tab03"><asp:Image ID="Image003" runat="server" ImageUrl="images/shield.png" title="KYC FAILED TRANSACTIONS" /></td>
                                    <td class="width_spacer5">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 75px;">&nbsp;</td>
                                    <td class="tabs_style_01_spacer" style="width: 60px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="spacer3"><td>&nbsp;</td></tr>
                    <tr class="spacer5" style="background-color: #39BBED;"><td>&nbsp;</td></tr>
                    <tr class="bg_FFFFFF spacer20"><td>&nbsp;</td></tr>
                </table>
            </div>

            <div id="DIV_AllTransactionsTable" style="display:none;>
                <table class="tbl_width_1200">
                    <tr class="bg_FFFFFF">
                        <td>
                            <table class="tbl_width_1160">
                                <tr><td class="txn_tab_headings">All Transactions</td></tr>
                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" id="alltransactions">
                                            <thead>
                                                <tr>
                                                    <td>KAASI REF</td>
                                                    <td>BeneID</td>
                                                    <td>DATE</td>
                                                    <td>AGENT</td>
                                                    <td>AGENT REF</td>
                                                    <td>BENE NAME</td>
                                                    <td>BANK</td>
                                                    <td>DOLLAR AMOUNT</td>
                                                    <td>STATUS</td>
                                                    <td>KYC STATUS</td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="spacer20 bg_FFFFFF"><td>&nbsp;</td></tr>
                </table>
            </div>

            <div id="DIV_PendingTransactionsTable" >
                 <table class="tbl_width_1200">
                    <tr class="bg_FFFFFF">
                        <td>
                            <table class="tbl_width_1160">
                                <tr><td class="txn_tab_headings">Pending Transactions</td></tr>
                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" id="pendingtransactions">
                                            <thead>
                                                <tr>
                                                    <td>KAASI REF</td>
                                                    <td>BeneID</td>
                                                    <td>DATE</td>
                                                    <td>AGENT</td>
                                                    <td>AGENT REF</td>
                                                    <td>BENE NAME</td>
                                                    <td>BANK</td>
                                                    <td>DOLLAR AMOUNT</td>
                                                    <td>STATUS</td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="spacer20 bg_FFFFFF"><td>&nbsp;</td></tr>
                </table>
            </div>

            <div id="DIV_FailedKYCTransactionsTable" style="display:none;">
                 <table class="tbl_width_1200">
                    <tr class="bg_FFFFFF">
                        <td>
                            <table class="tbl_width_1160">
                                <tr><td class="txn_tab_headings">KYC Failed Transactions</td></tr>
                                <tr class="spacer20"><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" id="failedKYCtransactions">
                                            <thead>
                                                <tr>
                                                    <td>KAASI REF</td>
                                                    <td>BeneID</td>
                                                    <td>DATE</td>
                                                    <td>AGENT</td>
                                                    <td>AGENT REF</td>
                                                    <td>BENE NAME</td>
                                                    <td>BANK</td>
                                                    <td>DOLLAR AMOUNT</td>
                                                    <td>STATUS</td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="spacer20 bg_FFFFFF"><td>&nbsp;</td></tr>
                </table>
            </div>

        </div>

        <div class="spacer20 bg_EFEFEF">&nbsp;</div>
        <div class="spacer20 bg_EFEFEF">&nbsp;</div>



        <div id="ReviewTransactionDiv">
            <!-- THIS IS THE COMPLAINCE REVIEW POPUP DIALOG BOX -->
            <%--<div id="DIV_Loading">
                <!-- Loading DIV -->
                <table class="tbl_width_120">
                    <tr>
                        <td style="text-align: center;">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/loading.gif" Height="60px" Width="60px" style="display:none;" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fnt_pleasewait">Please wait...</td>
                    </tr>
                </table>
            </div>--%>
            
            <div>
                <table class="tbl_width_640">
                    <tr class="bg_3498DB">
                        <td>
                            <table class="tbl_width_640">
                                <tr>
                                    <td class="modal_headings">Transaction Details</td>
                                    <td class="modal_top_blue_btns" id="TD_CloseDialog">X</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="bg_EFEFEF">
                        <td>
                            <table class="tbl_width_600">
                                <tr class="spacer20"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_600">
                                            <tr style="height: 50px;">
                                                <td style="width: 50px;" class="modal_tab_active" id="Review_TD_001">
                                                    <asp:Image ID="Image5" runat="server" ImageUrl="images/s_alltxns.png" title="TRANSACTION" /></td>
                                                <td style="width: 50px;" class="modal_tab_inactive_right" id="Review_TD_002">
                                                    <asp:Image ID="Image4" runat="server" ImageUrl="images/s_profits.png" title="LIMITS" /></td>
                                                <td style="width: 50px;" class="modal_tab_inactive_right" id="Review_TD_003">
                                                    <asp:Image ID="Image9" runat="server" ImageUrl="~/images/paper.png" title="NOTES" /></td>
                                                <td style="width: 50px;" class="modal_tab_inactive_right" id="Review_TD_004">
                                                    <asp:Image ID="Image7" runat="server" ImageUrl="~/images/folder.png" title="DOCUMENTS" /></td>
                                                <td style="width: 50px;" class="modal_tab_inactive_right" id="Review_TD_005">
                                                    <asp:Image ID="Image19" runat="server" ImageUrl="images/s_chat.png" title="AUDIT" /></td>
                                                <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                                <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                                <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                                <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                                <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                                <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                                <td style="width: 50px;" class="modal_tab_inactive_none">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>



                                <tr class="bg_FFFFFF">
                                    <td>
                                        <table class="tbl_width_600">
                                            <tr class="spacer20"><td>&nbsp;</td></tr>
                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="modal_sml_headings_thin">TXN REF: <span class="modal_sml_headings" id="REWV_KAASIID"></span>&nbsp;&nbsp|&nbsp;&nbsp;<span id="REWV_AgentRefID" class="modal_sml_headings"></span></td>
                                                            <td style="text-align:right;" class="modal_info_contents" id="TransactionDate"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="spacer10"><td>&nbsp;</td></tr>
                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr class="bg_F4F5F7">
                                                            <td class="line_err_warning" id="TD_KYCMessage"></td>
                                                            <td style="text-align:right; vertical-align: middle; padding:10px;"><input id="BTN_RunKYCCheck" type="button" value="Run KYC Check" onclick="return runKycCheck();" class="in_btn_blue" style="display:none;"/><input id="BTN_ManualKYC" type="button" value="Manual KYC" class="in_btn_red" style="display:none;" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer2"><td>&nbsp;</td></tr>

                                            <tr id="TR_ManualKYC" style="display:none;">
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr class="bg_F4F5F7">
                                                            <td>
                                                                <table class="tbl_width_520">
                                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                                    <tr><td><textarea id="TXT_KYCApprovalReason" cols="20" rows="2" class="modal_failed_kyc_comments"></textarea></td></tr>
                                                                    <tr class="spacer5"><td>&nbsp;</td></tr>
                                                                    <tr><td><input id="TXT_InBeneficiaryID" type="text" style="display:none;"/><input id="TXT_TransID" type="text" style="display:none;"/></td>
                                                                    </tr>
                                                                    <tr><td><input id="BTN_ApproveKYC" type="button" value="Approve KYC" class="in_btn_blue"/>&nbsp;<input id="BTN_DeclinedKYC" type="button" value="Decline KYC" class="in_btn_red"/></td>

                                                                    </tr>
                                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>

                                            <tr id="TR_KYCResults" style="display:none;">
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr class="bg_F4F5F7">
                                                            <td>
                                                                <table class="tbl_width_520">
                                                                    <tr>
                                                                        <td>
                                                                            <table class="tbl_width_520">
                                                                                <tr class="spacer15"><td>&nbsp;</td></tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table class="tbl_width_520">
                                                                                            <tr>
                                                                                                <td class="modal_kyc_headings" id="dsh_KYC_DateLabel" style="display: none; width:165px;">Last Check Date/Time</td>
                                                                                                <td class="modal_kyc_contents" id="dsh_KYC_DateValue" style="display: none; width:365px;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="modal_kyc_headings" id="dsh_KYC_ByLabel" style="display: none;">Checked By</td>
                                                                                                <td class="modal_kyc_contents" id="dsh_KYC_ByValue" style="display: none;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="modal_kyc_headings" id="dsh_KYC_RefLabel" style="display: none;">Reference</td>
                                                                                                <td class="modal_kyc_contents" id="dsh_KYC_RefValue" style="display: none;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="modal_kyc_headings" id="dsh_KYC_InfoLabel" style="display: none;">Comments</td>
                                                                                                <td class="modal_kyc_contents" id="dsh_KYC_InfoValue" style="display: none;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="modal_kyc_headings" id="dsh_KYC_ManKYCLabel" style="display: none;">Manual Comments</td>
                                                                                                <td class="modal_kyc_contents" id="dsh_KYC_ManKYCValue" style="display: none;">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="dsh_KYC_none_font" style="display: none;"><asp:Label ID="lbl_KYC_ManualKYCComments" runat="server" Text="Label"></asp:Label></td>
                                                                                </tr>
                                                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>



                                 


                                            <tr class="spacer20"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>



                            </table>
                        </td>
                    </tr>

                





                



                <!--<tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr><td class="settings_headings">Transaction Details - <span id="REWV_TransID"></span>&nbsp;<span id="REWV_DateTime"></span></td></tr>
                    <tr class="spacer5"><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>-->

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <table class="tbl_width_560">
                            <!--<tr>
                        <td>
                            <table class="tbl_width_560">
                                <tr class="">
                                    <td style="width:54px; vertical-align:top;"><table style="width:54px;"><tr><td class="txn_status_line_green spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/trn_ok.png" title="TRANSACTION" /></td>
                                    <td style="width:108px; vertical-align:top;"><table style="width:108px;"><tr><td class="txn_status_line_green spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/trn_ok.png" title="TRANSACTION" /></td>
                                    <td style="width:108px; vertical-align:top;"><table style="width:108px;"><tr><td class="txn_status_line_gray spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image208" runat="server" ImageUrl="~/images/trn_no.png" title="TRANSACTION" /></td>
                                    <td style="width:108px; vertical-align:top;"><table style="width:108px;"><tr><td class="txn_status_line_gray spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image209" runat="server" ImageUrl="~/images/trn_no.png" title="TRANSACTION" /></td>
                                    <td style="width:54px; vertical-align:top;"><table style="width:54px;"><tr><td class="txn_status_line_gray spacer10">&nbsp;</td></tr></table></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="">
                        <td>
                            <table class="tbl_width_560">
                                <tr>
                                    <td class="txn_status_headings">TXN CREATE</td>
                                    <td class="txn_status_headings">TXN REVIEW</td>
                                    <td class="txn_status_headings">TXN APPROVE</td>
                                    <td class="txn_status_headings">TXN COMPLETE</td>
                                </tr>
                            </table>
                        </td>
                    </tr> -->



                        </table>
                    </td>
                </tr>










                <tr class="bg_EFEFEF">
                    <td>
                        <div id="DIV_CompReview_Main"> <!-- MAIN TAB -->
                            <table class="tbl_width_600">
                                <tr hidden="hidden"><td><input id="REVW_TransID" name="REVW_TransID" type="text" /><input id="REVW_CustID" name="REVW_CustID" type="text" /></td></tr>
                                <tr class="bg_FFFFFF">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="modal_sml_headings">Receive Amount</td></tr>
                                            <tr class="spacer10"><td>&nbsp;</td></tr>
                                            <tr><td class="modal_numbers_blue"><span id="REVW_DollarAmount"></span></td></tr>
                                            <tr class="spacer25"><td>&nbsp;</td></tr>
                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td style="width:270px;" class="modal_sml_headings">Receiver Details</td>
                                                            <td style="width:270px;" class="modal_sml_headings">Sender Details</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_270_l">
                                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">ID/NAME</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_BeneName"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">ADDRESS</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_BeneAddress"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">CONTACT NO</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_BeneMobile"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">RELATIONSHIP</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_BeneRelationship"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">BANK</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_OriginalBank"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="tbl_width_270_l">
                                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">ID/NAME</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_CustomerID"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">ADDRESS</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_CustAddress"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">CONTACT NO</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_CustMobile"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">REMITTER (COMPANY)</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_AffiliateName"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">OCCUPATION</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_Occupation"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">PURPOSE OF TRANSACTION</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_Purpose"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">SOURCE OF FUNDS</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_SourceOfFunds"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                    <tr><td class="modal_info_headings">NOTES</td></tr>
                                                                    <tr><td class="modal_info_contents"><span id="REVW_Notes"></span></td></tr>
                                                                    <tr><td class="spacer15">&nbsp;</td></tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="spacer20"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr class="wiz_bg_EFEFEF spacer10">
                                    <td>&nbsp;</td>
                                </tr>

                                <tr class="wiz_bg_EFEFEF">
                                    <td>
                                        <table class="tbl_width_560">


                                            <tr hidden="hidden" class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_520">
                                                        <tr class="spacer10">
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_520">

                                                                    <tr>
                                                                        <td class="trn_popup_heading">Receive Option</td>
                                                                        <td class="trn_popup_details"><span id="REVW_TransType"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="trn_popup_heading">Destination</td>
                                                                        <td class="trn_popup_details"><span id="REVW_Destination"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="trn_popup_heading">Fee</td>
                                                                        <td class="trn_popup_details"><span id="REVW_Fee"></span></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <!-- <tr class="spacer5" id="REVW_TR_RoutingInfo_5px"><td>What is this?</td></tr> -->

                            </table>
                        </div>

                        <div id="DIV_CompReview_Notes">
                            <!-- NOTES TAB -->
                            <table class="tbl_width_600">
                                <tr class="bg_FFFFFF">
                                    <td>
                                        <table class="tbl_width_600">
                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr><td class="modal_sml_headings">Add a New Note</td></tr>
                                                        <tr class="spacer5"><td>&nbsp;</td></tr>
                                                        <tr><td class="modal_sml_instructions">You can add a new note for this transaction below. If you would like to notify the Remitter (Company), check the notify check box.</td></tr>
                                                        <tr class="spacer15"><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <textarea name="TXT_Add_TRN_Notes" id="TXT_Add_TRN_Notes" style="width: 100%; height: 55px; resize: none; line-height: normal !important; padding-top: 8px;" class="aa_input"></textarea></td>
                                                        </tr>
                                                        <tr class="spacer5">
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_530">
                                                                    <tr>
                                                                        <td class="ali_right">
                                                                            <input class="aa_btn_green" id="btn_SaveNewNote" type="button" value="Add Note" style="display:none;" /></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="spacer15">
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="bg_FFFFFF">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="modal_sml_headings">Transaction Notes</td></tr>
                                            <tr class="spacer5"><td>&nbsp;</td></tr>
                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <div id="DIV_Notes" style="overflow-y: auto; height: 250px;" class="tbl_width_530"></div>
                                                </td>
                                            </tr>
                                            <tr class="background_FFFFFF spacer15">
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="spacer10"><td>&nbsp;</td>
                            </table>
                        </div>

                        <div id="DIV_CompReview_Documents"> <!-- SUPPORTING DOCUMENTS TAB -->
                            <table class="tbl_width_600">
                                <tr class="wiz_bg_EFEFEF">
                                    <td>
                                        <table class="tbl_width_600">
                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr><td class="modal_sml_headings">Upload Supporting Documents</td></tr>
                                                        <tr class="spacer5"><td>&nbsp;</td></tr>
                                                        <tr><td class="modal_sml_instructions">You are strongly encouraged to upload any supporting documents for this transaction below. This may include proof of funds, purpose of transfer, etc.</td></tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="background_FFFFFF">
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td class="background_FFFFFF">
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="modal_info_headings">DOCUMENT DESCRIPTION</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <input class="aa_input" id="docdescription" name="docdescription" style="width: 430px;" type="text" value="" />
                                                                </div>
                                                            </td>
                                                            <td style="text-align: right; vertical-align: top;">
                                                                <input type="file" name="UploadFile1" id="UploadFile1" style="display: none;" /><input type="button" class="aa_btn_green" value="Browse..." onclick="document.getElementById('UploadFile1').click();" /></td>
                                                        </tr>
                                                        <tr class="spacer5">
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_notes_popup_details" style="font-size: 0.7em; color: #999;">Selected File: <span id="otherdoctext" style="color: #666;">Please browse for a file...</span></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr class="spacer5">
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="aa_btn_red" id="btn_UploadOtherFiles" type="button" value="Upload File" /></td>
                                                            <td style="text-align: right;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="background_FFFFFF spacer15">
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr class="bg_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_600">
                                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_560">
                                                                    <tr class="background_FFFFFF"><td class="modal_sml_headings">Supporting Documents</td></tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            


                                            
                                            <tr class="spacer5 bg_FFFFFF"><td>&nbsp;</td></tr>
                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td>
                                                                <table id="REWV_Docs">
                                                                    <thead>
                                                                        <!-- <td>Date</td> INCLUDE THIS -->
                                                                        <td>DOCUMENT DESCRIPTION</td>
                                                                        <td>VIEW</td>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer15 background_FFFFFF">
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr class="spacer10"><td>&nbsp;</td>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="DIV_CompReview_Limits"> <!-- LIMITS TAB -->
                            <table class="tbl_width_600">
                                <tr class="bg_FFFFFF">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="modal_sml_headings">Monthly AUD Amount</td>
                                                            <td class="modal_sml_headings">Yearly AUD Amount</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacer5">&nbsp;</td>
                                                            <td class="spacer5">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="modal_sml_instructions">Last 30 Days</td>
                                                            <td class="modal_sml_instructions">Last 365 Days</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacer10">&nbsp;</td>
                                                            <td class="spacer10">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="modal_numbers_blue"><span id="Last30daysDollar"></span></td>
                                                            <td class="modal_numbers_blue"><span id="LastYearDollar"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="spacer25"><td>&nbsp;</td></tr>
                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="modal_sml_headings">Monthly Transactions</td>
                                                            <td class="modal_sml_headings">Yearly Transactions</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacer5">&nbsp;</td>
                                                            <td class="spacer5">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="modal_sml_instructions">Last 30 Days</td>
                                                            <td class="modal_sml_instructions">Last 365 Days</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacer10">&nbsp;</td>
                                                            <td class="spacer10">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="modal_numbers_blue"><span id="Last30daysNb"></span></td>
                                                            <td class="modal_numbers_blue"><span id="LastYearNb"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="spacer20"><td>&nbsp;</td></tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>
                            </table>
                        </div>

                        <div id="DIV_CompReview_Audit"> <!-- AUDIT TAB -->
                            <table class="tbl_width_600">
                                <tr class="bg_FFFFFF">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr><td class="modal_sml_headings">Audit Trail</td></tr>
                                            <tr class="spacer5"><td>&nbsp;</td></tr>
                                            <tr><td class="modal_sml_instructions">A complete record of history of this transaction can be found below. In addition to step-by-step details, you can also see at a glance when and who made changes in each step.</td></tr>
                                            <tr class="spacer15"><td>&nbsp;</td></tr>

                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_560">
                                                                    <tr>
                                                                        <td>
                                                                            <div id="new-search-area"></div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>

                                                                            <table id="TBL_REWV_Audit" class="tbl_width_560">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <td style="width: 260px;">Record Date</td>
                                                                                        <td style="width: 300px;">Audit Record</td>
                                                                                    </tr>
                                                                                </thead>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>




                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="background_FFFFFF spacer15">
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>
                            </table>


                        </div>

                    </td>
                </tr>

                <tr><td><input type="text" id="REVW_CountryID" hidden="hidden" /></td></tr>

                <tr class="bg_EFEFEF">
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td style="text-align: right;">
                                    <input class="in_btn_darkblue" id="BTN_CloseReviewWindow" type="button" value="Close" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer20 bg_EFEFEF"><td>&nbsp;</td></tr>

            </table>
            </div>

            
        </div>

        <div id="DIV_ManualKYC">
            <table class="tbl_width_640">
                <tr>
                    <td>
                        <table class="tbl_width_640">
                                <tr class="bg_3498DB">
                                    <td class="modal_headings">Manual KYC</td>
                                    <td class="modal_top_blue_btns" id="TD_CloseMKYCDialog">X</td>
                                </tr>
                            </table>
                    </td>

                </tr>

                <tr>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>

                </tr>

            </table>
        </div>


        <asp:HiddenField ID="beneinboundId" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="transactID" ClientIDMode="Static" runat="server" />
    </form>
</asp:Content>

