﻿<%@ Page Title="" Language="C#" MasterPageFile="Reports.Master" AutoEventWireup="true" CodeBehind="InwardTransactionReport.aspx.cs" Inherits="KASI_Extend.inward.Reports.InwardTransactionReport" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">
        <div class="bg_DCDCDC">
            <table class="tbl_width_1200">
                <tr class="spacer20">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="page_headings">Inward Transactions Report</td>
                    <td style="text-align:right">&nbsp;</td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div class="bg_EFEFEF">
            <table class="tbl_width_1200">
                <tr class="spacer20"><td>&nbsp;</td></tr>
                <tr class="bg_FFFFFF">
                    <td>
                        <table class="tbl_width_1160">
                            <tr class="spacer20"><td>&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_1160">
                                        <tr>
                                            <td style="width:180px;"><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager><asp:TextBox ID="txtfrom" CssClass="datePickerReport modal_input_textbox" style="width:100%;" runat="server" placeholder="From (dd/mm/yyyy)"></asp:TextBox></td>
                                            <td style="width:10px;">&nbsp;</td>
                                            <td style="width:180px;"><asp:TextBox ID="txtto" CssClass="datePickerReport modal_input_textbox" style="width:100%;" runat="server" placeholder="To (dd/mm/yyyy)"></asp:TextBox></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:280px;"><asp:DropDownList ID="DDL_AgentID" runat="server" style="width:100%;" CssClass="modal_input_dropdown"></asp:DropDownList></td>
                                            <td>&nbsp;</td>
                                            <td style="text-align:right;"><asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="in_btn_blue" OnClick="btnViewReport_Click"/></td>
                                        </tr>
                                    </table>

                                    
                                    
                                </td>
                            </tr>
                            <tr class="spacer20"><td>&nbsp;</td></tr>
                        </table>
                    </td>

                </tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr class="bg_FFFFFF">
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td>
                                    <table class="tbl_width_1160">
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                        <tr><td><asp:Label ID="LBL_ErrorMessage" runat="server" style="display:none;"></asp:Label>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" CssClass="reportviewer_css" Width="100%"></rsweb:ReportViewer></td></tr>
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer20"><td>&nbsp;</td></tr>
                <tr class="spacer20"><td>&nbsp;</td></tr>                
            </table>
        </div>
        
        
    </form>
</asp:Content>
