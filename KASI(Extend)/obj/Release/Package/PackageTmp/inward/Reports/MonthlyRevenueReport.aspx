﻿<%@ Page Title="" Language="C#" MasterPageFile="Reports.Master" AutoEventWireup="true" CodeBehind="MonthlyRevenueReport.aspx.cs" Inherits="KASI_Extend.inward.Reports.MonthlyRevenueReport" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">
        <div class="bg_DCDCDC">
            <table class="tbl_width_1200">
                <tr class="spacer20">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="page_headings">Monthly Revenue Report</td>
                    <td style="text-align:right">&nbsp;</td>
                </tr>
                <tr class="spacer20">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div class="bg_EFEFEF">
            <table class="tbl_width_1200">
                <tr class="spacer20"><td>&nbsp;</td></tr>
                <tr class="bg_FFFFFF">
                    <td>
                        <table class="tbl_width_1160">
                            <tr class="spacer20"><td>&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_1160">
                                        <tr>
                                            <td style="width:180px;"><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:TextBox ID="txtfrom" CssClass="datePickerReport" runat="server" placeholder="From (dd/mm/yyyy)"></asp:TextBox>&nbsp;</td>
                                            <td style="width:10px;">&nbsp;</td>
                                            <td style="width:180px;"><asp:TextBox ID="txtto" CssClass="datePickerReport" runat="server" placeholder="To (dd/mm/yyyy)"></asp:TextBox>&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="btn_green_nomargin" OnClick="btnViewReport_Click"/></td>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td style="width:280px;">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td style="text-align:right;">&nbsp;</td>
                                        </tr>
                                    </table>

                                    
                                    
                                </td>
                            </tr>
                            <tr class="spacer20"><td>&nbsp;</td></tr>
                        </table>
                    </td>

                </tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr class="bg_FFFFFF">
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td>
                                    <table class="tbl_width_1160">
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                        <tr><td><asp:Label ID="Label1" runat="server" style="display:none;"></asp:Label>
                                            <asp:Label ID="LBL_ErrorMessage" runat="server" style="display:none;"></asp:Label>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%"></rsweb:ReportViewer></td></tr>
                                        <tr class="spacer20"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="spacer20"><td>&nbsp;</td></tr>
                <tr class="spacer20"><td>&nbsp;</td></tr>                
            </table>
        </div>

        
    </form>
</asp:Content>
