﻿<%@ Page Title="" Language="C#" MasterPageFile="~/inward/Import.Master" AutoEventWireup="true" CodeBehind="NonKYCCustomers.aspx.cs" Inherits="KASI_Extend.inward.Reports.NonKYCCustomers" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:TextBox ID="AgentID" runat="server"></asp:TextBox>
        <asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="btn_green_nomargin" OnClick="btnViewReport_Click"/>
        <p>&nbsp;</p>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%"></rsweb:ReportViewer>
    </form>
</asp:Content>
