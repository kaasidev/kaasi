﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AgentControl/Initial.Master" AutoEventWireup="true" CodeBehind="AgentW.aspx.cs" Inherits="KASI_Extend_.AgentW" %>

<%@ Register assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>

    <!-- CSS STYLE FILES -->
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />
    
    <link href="css/kaasi_common.css" rel="stylesheet" />

    <link href="css/toastr.min.css" rel="stylesheet" />
    
    <style>
        table.dataTable#TBL_Notifications {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 0.7rem !important;
            color: #666 !important;
            border-bottom: 1px solid #FFFFFF;

        }
        table.dataTable#TBL_Notifications thead th, table.dataTable#TBL_Notifications thead td {
            font-family: 'Varela Round', sans-serif !important;
            padding: 10px 5px 8px 5px;
            border-bottom: 2px solid #B9E4A6;
            font-size: 0.8em !important;
            color: #333 !important;
            background-color: #D3EDC8 !important;
            font-weight: 400 !important;
        }
        table.dataTable#TBL_Notifications tbody th,
        table.dataTable#TBL_Notifications tbody td {
            padding: 7px 5px 5px 5px !important;
            border-bottom: 1px solid #DCDCDC !important;
            background-color: #FFFFFF !important;
            vertical-align: top !important;
            
        }
        .not_C01 {
            width: 35px !important;
            border-right: 1px solid #FFFFFF;
            text-align: center !important;
        }
        .not_C02 {
            width: 35px !important;
            border-right: 1px solid #FFFFFF;
            text-align: center !important;
        }
        .not_C03 {
            width: 560px !important;
            border-right: 1px solid #FFFFFF;
            text-align: left !important;
        }
        .not_C04 {
            width: 172px !important;
            border-right: 1px solid #FFFFFF;
            text-align: center !important;
        }
        .not_C05 {
            width: 40px !important;
            text-align: center !important;
        }
        .not_H01 {
            text-align: center !important;
            padding: 0px !important;
        }
        .cur_code {
            font-family: 'Varela Round', sans-serif !important;
            vertical-align: middle !important;
            font-size: 13px !important;
            font-weight: 400;
            color: #FFF !important;
            width: 80px;
            height: 40px;
            background-color: #01bf49;
            padding: 0px !important;
            text-align: center !important;
            padding-top: 2px !important;
        }
        .cur_amount {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 14px !important;
            font-weight: 700;
            width: 168px;
            height: 40px;
            background-color: #EFEFEF;
            border: 5px solid #01bf49;
            margin: 0px !important;
            border-radius: 0px !important;
            text-align: right !important;
            color: #666 !important;
        }
        .cal_icon {
            background-color: #EFEFEF;
            text-align:right;
            padding-right: 2px !important;
        }
        .sel_curr_msg {
            background-color: #EFEFEF;
            font-size: 0.7em !important;
            font-weight: bold;
            text-align: left !important;
            padding-left: 10px !important;
            vertical-align: middle !important;
        }
        .sel_curr {
            background-color: #FFFFFF;
            width: 100% !important;
            border: 5px solid #EFEFEF;
            border-radius: 0px !important;
            height: 40px !important;
            font-family: 'Varela Round', sans-serif !important;

        }
        .ex_rate {
            background-color: #FFFFFF;
            width: 100% !important;
            border: 5px solid #EFEFEF;
            border-radius: 0px !important;
            height: 40px !important;
            margin: 0px !important;
            text-align: right !important;
            color: #999 !important;
            font-size: 14px !important;
            font-weight: 700;
        }
    </style>

	<script src="js/toastr.min.js"></script>
    <script type="text/javascript">

        function calcNewRate(CurrencyName) {
            var BankFeedRate = $("#curKAASIRate_" + CurrencyName).val();
            var Margin = $('#curMargin_' + CurrencyName).val();
            var total = parseFloat(BankFeedRate) + parseFloat(Margin)
            $("#tdNewRate" + CurrencyName).val(total);
        }

        function showerror(msg) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.error(msg, 'Error');

        }
        function showsuccess(msg, body) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.success(msg, body);

        }

        function deleteNotification(NotifID) {
            //alert(NotifID);
            $.ajax({
                url: 'Processor/deleteAgentNotification.aspx',
                data: {
                    NID: NotifID,
                },
                success: function (response) {
                    if (response == "OK") {
                        //alert('Notification Deleted');
                        NotificationsTbl.ajax.reload(null, false);
                        //getNbNotifications();
                    }
                    else {
                        alert(response);
                    }
                    
                }
            })
        }


        $(document).ready(function () {
            $("#bus_countryofbirth").change(function () {
                $("#bcountry").val($("#bus_countryofbirth :selected").text());
            });
            $("#bus_nationality").change(function () {
                $("#bnationality").val($("#bus_nationality :selected").text());
            });
            $('#btn_SubmitBusiness').hide();
            $('#DIV_Business02').hide();
            $('#DIV_AuthorizedContactMsg').hide();
            $('#btn_BusinessBack').hide();
            $('#btn_BusinessNext').hide();
            $('#btn_Submit').hide();
            $('#TD_Spacer_001').addClass("img_hide_only");

            $('#BABN').mask('00 000 000 000');
            $('#ppbus_state').mask('AAA');
            $('#ppbus_state').css('text-transform', 'uppercase');
            $('#bus_state').mask('AAA');
            $('#bus_state').css('text-transform', 'uppercase');
            $('#BPostalState').mask('AAA');
            $('#BPostalState').css('text-transform', 'uppercase');
            $('#BPostalPostcode').mask('0000');
            $('#ppbus_postcode').mask('0000');

            $('#bus_postcode').mask('0000');

            $('#pbus_phonemobile').mask('0000 000 000');
            $('#pbus_phonework').mask('(00) 0000 0000');
            $('#pbus_phonehome').mask('(00) 0000 0000');

            NotificationsTbl = $('#TBL_Notifications').DataTable({
                "lengthMenu": [[5, 10, 25], [5, 10, 25]],
                "pageLength": 5,
                columns: [
                    { 'data': 'AgentNotificationID' },
                    { 'data': 'Type' },
                    { 'data': 'Description' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'Action' },
                ],
                "columnDefs": [
                    { className: "not_C01", "targets": [0] },
                    { className: "not_C02", "targets": [1] },
                    { className: "not_C03", "targets": [2] },
                    { className: "not_C04", "targets": [3] },
                    { className: "not_C05", "targets": [4] },
                ],
                bServerSide: true,
                //stateSave: true,
                sAjaxSource: 'DataHandlers/AGENTNotificationDataHandler.ashx',
                "order": [[0, "desc"]]
            })

            $('#btn_BusinessNext').click(function () {
                if ($('#form_AddBusiness').valid() && $("#BBusinessName").val() != ''
                    && $("#BABN").val() != '' && $("#bus_streetno").val() != '' && $("#bus_streetname").val() != ''
                    && $("#bus_streettype").val() != '0' && $("#bus_suburb").val() != '' && $("#bus_state").val() != '0'
                    && $("#bus_postcode").val() != '' && $("#pos_streetno").val() != '' && $("#pos_streetname").val() != '' && $("#pos_streettype").val() != '') {
                    $('#btn_BusinessBack').show();
                    $("#btn_BusinessNext").hide();
                    $('#DIV_Business02').show();
                    $('#DIV_Business01').hide();
                    $('#DIV_AuthorizedContactMsg').show();
                    $('#btn_SubmitBusiness').show();
                    $('#TD_AddBusiness_002').removeClass();
                    $('#TD_AddBusiness_002').addClass("wiz_tab_active");
                    $('#TD_AddBusiness_001').removeClass();
                    $('#TD_AddBusiness_001').addClass("wiz_tab_inactive_left");
                }
                else {
                    showerror("Please fill all required fields mark with astrick(*) ");
                }
            });

            $('#btn_BusinessBack').click(function () {
                $('#btn_BusinessBack').hide();
                $("#btn_BusinessNext").show();
                $('#DIV_Business02').hide();
                $('#DIV_Business01').show();
                $('#DIV_AuthorizedContactMsg').hide();
                $('#btn_SubmitBusiness').hide();
                $('#TD_AddBusiness_002').removeClass();
                $('#TD_AddBusiness_002').addClass("wiz_tab_inactive_right");
                $('#TD_AddBusiness_001').removeClass();
                $('#TD_AddBusiness_001').addClass("wiz_tab_active");
            })

            $.ajaxSetup({
                beforeSend: function () {
                    $(".loading").show();
                },
                complete: function () {
                    $(".loading").hide();
                }
            });
            setCurrencyRates();

            $.ajax({
                url: 'JQDataFetch/getAgentCurrencies.aspx',
                async: false,
                success: function (data) {
                    var Currencies = data.split('~');
                    Currencies.sort();
                    $.each(Currencies, function (item) {
                        var datasplit = Currencies[item].split('|');

                        $('#SEL_Currency').append('<option value="' + datasplit[2] + '">' + datasplit[0] + ' - ' + datasplit[1] + '</option>');
                    })
                }
            });

            $('#SEL_Currency').change(function () {
                $.ajax({
                    url: 'JQDataFetch/getRateForCurrency.aspx',
                    data: {
                        CID: $('#SEL_Currency :selected').val(),
                    },
                    success: function (response) {
                        
                        var splitres = response.split('|');
                        $('#txt_Rate').val(splitres[0]);
                        $('#SPN_CurrencyCode').text(splitres[1]);
                        $('#txt_FValue').val('');
                        $('#txt_AUDValue').val('');
                    }
                })
            });

            $("#txt_AUDValue").keyup(function () {
                var AUDVal = parseFloat($("#txt_AUDValue").val());
                var rate = parseFloat($('#txt_Rate').val());
                var sendRate = AUDVal * rate;
                $('#txt_FValue').val(sendRate.toFixed(2));
            });

            $('#txt_FValue').keyup(function () {
                var FValue = parseFloat($('#txt_FValue').val());
                var rate = parseFloat($('#txt_Rate').val());
                var sendRate = FValue / rate;
                $('#txt_AUDValue').val(sendRate.toFixed(2));
            })

            $('#SearchCustomer').autocomplete({
                source: "JQDataFetch/getCustomerForAutocompleteWithAgentID.aspx",
                select: function (event, ui) {
                    window.open('dashboardw.aspx?custid=' + ui.item.id, '_blank');
                    $(this).val("");
                    return false;
                }
            });

            $('#dob').mask('00/00/0000');
            $('#state').mask('AAA');
            $('#postcode').mask('0000');
            $('#phonehome').mask('(00) 0000 0000');
            $('#phonework').mask('(00) 0000 0000');
            $('#phonemobile').mask('0000 000 000');



            $('#btn_EditRate').click(function () {
                SetRateDialog.dialog('open');
            });


            AddCustomerDialog = $('#AddCustomerDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Add New Customer (Individual)',
            });

            PersonalOrBusinessDialog = $('#DIV_PersonalOrBusiness').dialog({
                autoOpen: false,
                modal: true,
                width: 320,
                title: 'CUSTOMER TYPE',
            });

            AddBusinessDialog = $('#AddBusinessDIV').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Add New Customer',
            });

            SetRateDialog = $('#DIV_SetExchangeRate').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Set Exchange Rates',
            });



            $('#btn_AddCustomer').click(function () {
                //AddCustomerDialog.dialog('open');
                PersonalOrBusinessDialog.dialog('open');
            });

            $('#btn_PersonTr').click(function () {
                PersonalOrBusinessDialog.dialog('close');
                $('#DIV_Business01').hide();
                $('#DIV_Business02').show();
                $('#DIV_AuthorizedContactMsg').hide();
                $('#btn_Submit').show();
                $('#btn_BusinessNext').hide();
                $('#btn_BusinessBack').hide();
                $('#TD_AddBusiness_001').removeClass();
                $('#TD_AddBusiness_001').addClass("wiz_tab_inactive_none img_hide_only");
                $('#TD_AddBusiness_002').removeClass();
                $('#TD_AddBusiness_002').addClass("wiz_tab_active");
                $('#TD_Spacer_001').removeClass();
                $('#TD_Spacer_001').addClass("wiz_tab_inactive_none");
                AddBusinessDialog.dialog('open');
                //AddCustomerDialog.dialog('open');
            });

            $('#btn_Business').click(function () {
                PersonalOrBusinessDialog.dialog('close');
                $('#DIV_Business01').show();
                $('#DIV_Business02').hide();
                $('#DIV_AuthorizedContactMsg').hide();
                $('#btn_Submit').hide();
                $('#btn_BusinessNext').show();
                AddBusinessDialog.dialog('open');
                $('#TD_AddBusiness_001').removeClass();
                $('#TD_AddBusiness_001').addClass("wiz_tab_active");
                $('#TD_AddBusiness_002').removeClass();
                $('#TD_AddBusiness_002').addClass("wiz_tab_inactive_right");
                $('#TD_Spacer_001').removeClass();
                $('#TD_Spacer_001').addClass("img_hide_only");
            });

            $('#btn_SubmitBusiness').click(function () {
                $("#isbusi").val("busi");
                if ($('#form_AddBusiness').valid() && $('#bus_lastname').val() != '' && $('#bus_title').val() != '' && $('#bus_firstnames').val() != '' && $('#bus_countryofbirth').val() != ''
                    && $('#bus_dob').val() != '' && $('#bus_placeofbirth').val() != '' && $('#bus_nationality').val() != '') {
                    var formData = $("#form_AddBusiness").serialize();
                    // console.log(formData);
                    $.ajax({
                        url: 'Processor/AddNewBusiness.aspx',
                        type: 'post',
                        data: formData,
                        success: function (data) {
                            showsuccess(data);
                            location.reload();
                        },
                        error: function (xhr, err) {
                            showerror("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                            showerror("responseText: " + xhr.responseText);
                        }

                    });
                }
                else {
                    showerror("Please fill all fields mark with astrick(*)");
                }

            });
           


            $('#btn_Submit').click(function () {
                if ($('#form_AddBusiness').valid() && $("#bus_lastname").val() != ''
                    && $("#bus_title").val() != '0' && $("#bus_givenNames").val() != '' && $("#bus_countryofbirth").val() != '0'
                    && $("#bus_dob").val() != '' && $("#bus_placeofbirth").val() != '' && $("#bus_nationality").val() != '0'
                    && $("#pbus_streetno").val() != '' && $("#pbus_streetname").val() != '' && $("#pbus_streettype").val() != '' && $("#ppbus_suburb").val() != ''
                    && $("#ppbus_state").val() != '' && $("#ppbus_postcode").val() != '' && $("#pbus_phonemobile").val() != '' && $("#pbus_email1").val() != '') {
                    var LName = $('#lastname').val();
                    $.ajax({
                        url: 'JQDataFetch/performTriangularVerification.aspx',
                        data: {
                            LN: $('#bus_lastname').val(),
                            DOB: $('#bus_dob').val(),
                            MOB: $('#pbus_phonemobile').val(),
                        },
                        success: function (response) {
                            var data = response.split('|');
                            if (data[0] == "Y") {
                                alert('This customer already exists as Customer ID ' + data[1]);
                            }
                            else {
                                AddNewCustomer();
                               
                            }
                        }
                    });
                } else {
                    showerror("Please fill all fields mark with asterick(*)");
                }
                //$('#form1').valid();                
            });
            function AddNewCustomer() {
                $("#isbusi").val("indi");
                var formData = $("#form_AddBusiness").serialize();
                // console.log(formData);
                $.ajax({
                    url: 'Processor/AddNewBusiness.aspx',
                    type: 'post',
                    data: formData,
                    success: function (data) {
                        showsuccess(data);
                        $('#form_AddBusiness').each(function () {
                            this.reset();
                        });
                        location.reload();
                    },
                    error: function (xhr, err) {
                        showerror("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                        showerror("responseText: " + xhr.responseText);
                    }

                });
            }
            $('#form_AddCustomer').validate({
                errorPlacement: function (error, element) {
                    return false;
                },
                rules: {
                    lastname: {
                        required: true,
                    },
                },
                submitHandler: function (form) {
                    $.ajax({
                        url: 'Processor/AddCustomerDetails.aspx',
                        type: 'post',
                        data: {
                            lastname: $('#lastname').val(),
                            firstnames: $('#firstnames').val(),
                            dob: $('#dob').val(),
                            unitno: $("#unitno").val(),
                            streetno: $('#streetno').val(),
                            streetname: $('#streetname').val(),
                            streettype: $('#streettype :selected').text(),
                            suburb: $('#suburb').val(),
                            state: $('#state').val(),
                            postcode: $('#postcode').val(),
                            homephone: $('#phonehome').val(),
                            workphone: $('#phonework').val(),
                            mobile: $('#phonemobile').val(),
                            email1: $('#email1').val(),
                            email2: $('#email2').val(),
                            countryofbirth: $("#countryofbirth :selected").text(),
                            placeofbirth: $('#placeofbirth').val(),
                            nationality: $('#nationality :selected').text(),
                            aka: $('#aka').val(),
                            title: $('#title').val(),
                            hidagentid: $('#<%=hidden_agentid.ClientID%>').val(),
                        },
                        success: function (data) {
                            alert(data);
                            $('#form_AddBusiness').each(function () {
                                this.reset();
                            });
                            location.reload();
                        },
                        error: function (xhr, err) {
                            alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                            alert("responseText: " + xhr.responseText);
                        }

                    });
                    return false;
                }
            });

            $('#phonemobile').focusout(function () {
                $.ajax({
                    url: 'JQDataFetch/VerifyMobileDuplicate.aspx',
                    data: {
                        MOB: $('#phonemobile').val(),
                    },
                    success: function (response) {
                        if (response == "Y") {
                            alert('This mobile number is already registered');
                        }
                    },
                })
            });


            $('#form1').validate({

                rules: {
                    lastname: {
                        required: true,
                    },


                },

                submitHandler: function (form) {
                    // do other things for a valid form
                    //form.submit();
                    alert('Submitting');


                    return false;
                }
            });


        });


        function setCurrencyRates() {
            console.log('test');
            $.ajax({
                url: 'JQDataFetch/getCurrencyRateTable.aspx',
                async: false,
                data: {
                    agentId: $('#hidAgentId').val(),
                },
                success: function (response) {
                    document.getElementById('TBL_SetExchngeRateClient').innerHTML = response;

                    //   $("#TBL_SetExchngeRateClient").html(response);
                },
            })
        }
        function updateCurrency() {
            var valueList = "";
            var idlist = $("#hidCurrencyIdList").val();
            var arr = idlist.split('^');
            jQuery.each(arr, function (i, val) {
                if (val != "") {
                    var subrr = val.split('_');
                    valueList = valueList + $("#" + val).val() + "^" + subrr[1] + ",";
                }
            });

            $.ajax({
                url: 'JQDataFetch/updateCurrencyRate.aspx',
                data: {
                    agentId: $('#hidAgentId').val(),
                    curr: valueList
                },
                success: function (response) {
                    if (response == 'Y') {
                        showsuccess('Sucessfully updated', '');
                        SetRateDialog.dialog('close');
                        location.reload();
                    }
                    else {
                        var err = response.split('^');
                        showerror(err[1]);
                    }
                    // document.getElementById('TBL_SetExchngeRateClient').innerHTML = response;

                    //   $("#TBL_SetExchngeRateClient").html(response);
                },
            })
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
     <form id="form1" runat="server">

    <div class="container_btns">
        <table class="tbl_width_1200"> <!-- MAIN TABLE STARTS HERE -->
            <tr>
                <td style="width:285px; vertical-align:top !important;"><input style="width:100%;" id="SearchCustomer" type="text" class="aa_input" placeholder="Search Customer by ID/Name/Mobile/DOB" /></td>
                <td style="width:20px;">&nbsp;</td>
                <td><button class="aa_btn_green" id="btn_AddCustomer" onclick="return false">Add New Customer</button></td>
                <td class="todays_rate" style="text-align:right;">&nbsp;</td>
                <td style="text-align:right; width:185px;"><button class="aa_btn_red" id="btn_EditRate" onclick="return false">Set Exchange Rates</button></td>
            </tr>
        </table> <!-- MAIN TABLE ENDS HERE -->
    </div>


<div class="container_one no_padding_btm">
    <table class="tbl_width_1200">
        <tr><td><input type="text" id="TXT_firstTimeLogin" runat="server" hidden="hidden"/>
            
            </td></tr>

        <tr>
            <td>
                <table class="tbl_width_1200">
                    <tr style="height:65px;">
                        <td style="vertical-align:top; width:50px;">
                            <table>
                                <tr><td class="ag_logo">Logo</td></tr>
                            </table>
                        </td>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="ag_greeting" style="width:930px; vertical-align:top; padding-top:5px;"><span id="AgentNameDIV" runat="server" /><br /><span class="ag_greeting_date"><label id="ag_date" runat="server"></label></span></td>
                        <td style="vertical-align:bottom;" class="ali_right">
                            <table class="ag_tbl_heading_act" style="background-color:#DCDCDC;">
                                <tr><td class="ag_dash_main_headings">CURRENT ACTIVITIES</td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
</div>

<div class="container_one no_padding_top_btm">
    <table class="tbl_width_1200">
        <tr><td class="spacer10" style="background-color: #DCDCDC;">&nbsp;</td></tr>
        <tr><td style="background-color: #DCDCDC;">
            <table class="tbl_width_1180 background_FFFFFF">
                <tr><td class="spacer10">&nbsp;</td></tr>
                <tr>
                    <td>
                        <table style="width:100%; height:50px;">
                            
                            <tr>
                                <td class="ag_dash_seperator" style="width:17%;">
                                    <table class="tbl_width_180">
                                        <tr><td class="ag_dash_numbers" style="color:#62cb31 !important;"><span id="spannbtrans" runat="server"></span></td></tr>
                                        <tr><td class="ag_dash_letters">TOT TXNS THIS MONTH</td></tr>
                                    </table>
                                </td>
                                <td class="ag_dash_seperator" style="width:16.5%;">
                                    <table class="tbl_width_180">
                                        <tr><td class="ag_dash_numbers"><span id="spannbpendtrans" runat="server"></span></td></tr>
                                        <tr><td class="ag_dash_letters">ALL PENDING TXNS</td></tr>
                                    </table>
                                </td>
                                <td class="ag_dash_seperator" style="width:16.5%;">
                                    <table class="tbl_width_180">
                                        <tr><td class="ag_dash_numbers"><span id="spannbreviewtrans" runat="server"></span></td></tr>
                                        <tr><td class="ag_dash_letters">compliance review</td></tr>
                                    </table>
                                </td>
                                <td class="ag_dash_seperator" style="width:16.5%;">
                                    <table class="tbl_width_180">
                                        <tr><td class="ag_dash_donumbers"><span id="spanaudpendingamount" runat="server"></span></td></tr>
                                        <tr><td class="ag_dash_letters">total pending</td></tr>
                                    </table>
                                </td>
                                <td class="ag_dash_seperator" style="width:16.5%;">
                                    <table class="tbl_width_180">
                                        <tr><td class="ag_dash_donumbers"><span id="spanaudsentamount" runat="server"></span></td></tr>
                                        <tr><td class="ag_dash_letters">TOT remitted THIS MONTH</td></tr>
                                    </table>
                                </td>
                                <td style="width:17%;">
                                    <table class="tbl_width_180">
                                        <tr><td class="ag_dash_donumbers"><span id="spanaudreviewamount" runat="server"></span></td></tr>
                                        <tr><td class="ag_dash_letters">total to be reviewed</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td class="spacer10">&nbsp;</td></tr>
            </table>
            </td></tr>
        <tr><td class="spacer10" style="background-color: #DCDCDC;">&nbsp;</td></tr>
        
        <tr class="spacer15"><td>&nbsp;</td></tr>

        <tr>
            <td>
                <table style="width:100%; background-color:#DCDCDC;">
                    <tr>
                        <td style="width:75%;"" class="ag_dash_main_headings">NOTIFICATIONS</td>
                        <td style="width:25%;"" class="ag_dash_main_headings">CALCULATOR</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr><td class="spacer10" style="background-color: #DCDCDC;">&nbsp;</td></tr>

        <tr>
            <td style="background-color: #DCDCDC;">
                <table class="tbl_width_1180 background_FFFFFF" style="height:200px;">
                    <tr>
                        <td style="width:882px; vertical-align:top;">
                            <table class="tbl_width_842">
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_855" id="TBL_Notifications">
				                                <thead>
                                                    <tr>
                                                        <td class="not_H01">REF</td>
							                            <td class="tbl_heading_ben_fnt">TYPE</td>
								                        <td class="tbl_heading_ben_fnt">DESCRIPTION</td>
								                        <td class="tbl_heading_ben_fnt_02">DATE</td>
                                                        <td class="tbl_heading_ben_fnt_02">ACTION</td>                                                  
                                                    </tr>
						                        </thead>
				                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                </table>
                        </td>
                        <td style="background-color: #DCDCDC; width:10px;">&nbsp;</td>
                        <td style="width:288px; text-align: center; vertical-align: top;">
                            <div id="DIV_Calculator">
                                <table class="tbl_width_248">
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                    <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_248">
                                                <tr>
                                                    <td class="sel_curr_msg">Select Currency</td>
                                                    <td class="cal_icon"><asp:Image ID="IMG_EditMasterComp" runat="server" ImageUrl="~/images/calculating.png"  /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <table class="tbl_width_248">
                                                <tr>
                                                    <td><select id="SEL_Currency" class="sel_curr"><option></option></select></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer5"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td><input id="txt_Rate" type="text" class="ex_rate" /></td>
                                    </tr>
                                    <tr class="spacer5"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_248">
                                                <tr>
                                                    <td class="cur_code">AUD</td>
                                                    <td><input id="txt_AUDValue" type="text" class="cur_amount" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer5"><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <table class="tbl_width_248">
                                                <tr>
                                                    <td class="cur_code"><span id="SPN_CurrencyCode"></span></td>
                                                    <td><input id="txt_FValue" type="text" class="cur_amount" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer_20"><td>&nbsp;</td></tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr><td class="spacer10" style="background-color: #DCDCDC;">&nbsp;</td></tr>

        <tr><td>&nbsp;</td></tr>

        <tr>
            <td>
                <table style="width:100%; background-color:#DCDCDC;">
                    <tr>
                        <td style="width:25%;"" class="ag_dash_main_headings">CURRENT RATES</td>
                        <td style="width:25%;"" class="ag_dash_main_headings">DAILY DEPOSITS</td>
                        <td style="width:25%;"" class="ag_dash_main_headings">&nbsp;</td>
                        <td style="width:25%;"" class="ag_dash_main_headings">&nbsp;</td>
                    </tr>
                </table>
            </td></tr>

        <tr><td class="spacer10" style="background-color: #DCDCDC;">&nbsp;</td></tr>
        
        <tr>
            <td style="background-color: #DCDCDC;">
                <table class="tbl_width_1180 background_FFFFFF" style="height:150px;">
                    <tr>
                        <td style="width:288px;">
                            <table class="tbl_width_268">
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <asp:Table ID="tbl_Currencies" runat="server" CssClass="tbl_width_265">
                                        <asp:TableHeaderRow>
                                            <asp:TableHeaderCell CssClass="ag_dash_currency_head1">&nbsp;</asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="ag_dash_currency_head1">&nbsp;</asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="ag_dash_currency_head2">RATE</asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="ag_dash_currency_head2">MY RATE</asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="ag_dash_currency_head2">MARGIN</asp:TableHeaderCell>
                                        </asp:TableHeaderRow>
                                    </asp:Table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #DCDCDC; width:10px;">&nbsp;</td>
                        <td style="width:287px;">
                            <table class="tbl_width_280">
                                <tr>
                                    <td>1</td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color: #DCDCDC; width:10px;">&nbsp;</td>
                        <td style="width:287px; text-align: center; vertical-align: middle;">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/stats.png" />
                        </td>
                        <td style="background-color: #DCDCDC; width:10px;">&nbsp;</td>
                        <td style="width:288px; text-align: center; vertical-align: middle;">
                            <table class="tbl_width_267">
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <asp:Table id="BankMonies" runat="server" CssClass="tbl_width_255">
                                            <asp:TableRow>
                                                <asp:TableCell CssClass="welcome_daily_deposits_01">Deposit Method</asp:TableCell>
                                                <asp:TableCell CssClass="welcome_daily_deposits_02">Amount</asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table class="tbl_width_255">
                                            <tr>
                                                <td><asp:Button ID="RefreshAccounts" runat="server" Text="RELOAD" OnClick="RefreshAccounts_Click" UseSubmitBehavior="false" CssClass="welcome_refresh_btn" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr><td class="spacer10" style="background-color: #DCDCDC;">&nbsp;</td></tr>

        <tr><td style="height:40px;" class="kaasi_default_font">&nbsp;</td></tr>

    </table>
</div>


<!-- SET EXCHANGE RATE START -->
    <div id="DIV_SetExchangeRate">
        <table class="tbl_width_600">
            <tr><td>&nbsp;</td></tr>
            
            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td class="wiz_tab_active"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/exchange.png" title="EXCHANGE RATES" /></td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                            <td style="width:50px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td style="height: 20px; background-color:#EFEFEF;">&nbsp;</td></tr>

            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">
                        <tr>
                            <td>
                                <input id="hidAgentId" type="hidden" value="<%=Session["AgentID"].ToString() %>" />
                                <div id="TBL_SetExchngeRateClient">Exchange rate table</div>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>

            <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

            <tr><td><input type="button" style="float:right" value="Save Exchange Rate/s" class="aa_btn_red" onclick="updateCurrency()" /></td></tr>

            <tr class="spacer10"><td>&nbsp;</td></tr>

        </table>


        <%--<asp:Table ID="TBL_SetExchngeRate" runat="server" CssClass="tbl_width_280">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell CssClass="style__width_10">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell CssClass="aa_label_font">KAASI RATE</asp:TableHeaderCell>
                <asp:TableHeaderCell CssClass="style__width_10">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell CssClass="aa_label_font">MY RATE</asp:TableHeaderCell>
                <asp:TableHeaderCell CssClass="style__width_10">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell CssClass="aa_label_font">MARGIN</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>
        --%>
        <div style="width:100%">
            
        </div>
    <%--    <table style="margin-top:20px">
            <tr>
                <td>
                    <asp:Button ID="BTN_SetRates" runat="server" Text="Set Rate" OnClick="BTN_SetRates_Click" UseSubmitBehavior="false" />
                </td>
            </tr>
        </table>--%>
    </div>
    <!-- SET EXCHANGE RATE END -->


    </form>




   <div id="DIV_PersonalOrBusiness"> <!-- SELECT IF CUSTOMER IS BUSINESS OR PERSONAL START -->
        <table class="tbl_width_280">
            <tr><td>&nbsp;</td></tr>
            <tr><td class="wel_nc_type_msg">Please select the customer type to proceed...</td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table class="tbl_width_240">
                        <tr>
                            <td style="width:105px; text-align:right !important;"><input class="aa_btn_green_newcust" id="btn_PersonTr" type="button" value="Individual" /></td>
                            <td style="width:20px;">&nbsp;</td>
                            <td style="width:105px; text-align:left !important;"><input class="aa_btn_green_newcust" id="btn_Business" type="button" value="Business" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="spacer10"><td>&nbsp;</td></tr>
        </table>
    </div> <!-- SELECT IF CUSTOMER IS BUSINESS OR PERSONAL END -->

    <div id="AddBusinessDIV">
        <div class="edit__form__main">
            <form id="form_AddBusiness" method="get">
                 <input type="hidden" id="bcountry" name="bcountry" value="" />
                <input type="hidden" id="bnationality" name="bnationality" value="" />
                 <input type="hidden" id="isbusi" name="isbusi" value="" />
                <input type="hidden" name="type" value="new" />
               <input type="text" name="hidden_agentid" id="hidden_agentid" runat="server" hidden="hidden"/>
            <table class="tbl_width_600">
                
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_AddBusiness_001"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/company.png" title="COMPANY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_AddBusiness_002"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/user.png" title="AUTHORIZED CONTACT" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none" id="TD_Spacer_001">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>
                
                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_Business01">
                            <table class="tbl_width_560" >
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">FULL BUSINESS NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ABN/ACN *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_393"><div><input class="aa_input" id="BBusinessName" name="BBusinessName" required style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;"></td>
                                                <td class="style__width_186"><div><input class="aa_input" id="BABN" name="BABN" style="width:100%;" required type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">TRADING NAME (IF APPLICABLE)</td>
                                               
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="BTradingName" name="BTradingName" style="width:100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">Busi. Contct</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">Busi. Email</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">Busi. Web site</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BBContact" name="BBContact" style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BBEmail" name="BBEmail" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BBWeb" name="BBWeb" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font_heading">PRINCIPLE PLACE OF BUSINESS (PO BOX IS NOT ACCEPTABLE)</td>
                                            </tr>
                                            <tr hidden="hidden">
                                                <td><div><input class="aa_input" id="BAddressLine1" name="BAddressLine1" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">UNIT NO</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NO *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET TYPE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_50"><div><input class="aa_input" id="bus_unitno" name="bus_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td class="style__width_10">&nbsp;</td> 
                                                <td class="style__width_75"><div><input class="aa_input" required id="bus_streetno" name="bus_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_290"><div><input class="aa_input" required id="bus_streetname" name="bus_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                                        <select id="bus_streettype" name="bus_streettype" required style="width:100%;">
                                                            <option value="0">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_600"><div><input class="aa_input" id="BAddressLine2" name="BAddressLine2" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input required class="aa_input" id="bus_suburb" name="bus_suburb" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input required class="aa_input" id="bus_state" name="bus_state" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input required class="aa_input" id="bus_postcode" name="bus_postcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BSuburb" name="BSuburb" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BState" name="BState" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostcode" name="BPostcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="bus_country" name="bus_country" style="width:100%;" type="text" value="Australia" readonly="readonly"  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                 <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font_heading">POSTAL ADDRESS (IF DIFFERENT FROM ABOVE)</td>
                                            </tr>
                                            <tr hidden="hidden">
                                                <td><div><input class="aa_input" id="BPostalAddressLine1" name="BPostalAddressLine1" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">UNIT NO</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NO *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET TYPE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_50"><div><input class="aa_input" id="pos_unitno" name="pos_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td class="style__width_10">&nbsp;</td>
                                                <td class="style__width_75"><div><input required class="aa_input" id="pos_streetno" name="pos_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_290"><div><input required class="aa_input" id="pos_streetname" name="pos_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                                        <select id="pos_streettype" name="pos_streettype" required style="width:100%;">
                                                             <option value="">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">POSTAL ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="BPostalAddressLine2" name="BPostalAddressLine2" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BPostalSuburb" name="BPostalSuburb" style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostalState" name="BPostalState" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostalPostcode" name="BPostalPostcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="pos_country" name="pos_country" style="width:100%;" type="text" value="Australia" readonly="readonly"  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>
                                            
                            </table>
                        </div>
                    </td>
                </tr>
                

                <tr>
                    <td>
                        <div id="DIV_AuthorizedContactMsg">
                            <table>
                                <tr>
                                    <td>
                                       &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>


                <!-- ADD NEW CUSTOMER DIV 2 -->
                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_Business02">
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">

                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="aa_label_font">LAST NAME *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">TITLE *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">GIVEN NAME/S *</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style__width_186"><div><input class="aa_input" required id="bus_lastname" name="bus_lastname" style="width:100%;" type="text" value="" /></div></td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td style="width:83px;">
                                                                <div>
                                                                    <select id="bus_title" name="bus_title" required style="width:100%;">
                                                                        <option value="0">--Select--</option>
	                                                                    <option value="Mr">Mr</option>
	                                                                    <option value="Mrs">Mrs</option>
	                                                                    <option value="Miss">Miss</option>
	                                                                    <option value="Ms">Ms</option>
	                                                                    <option value="Dr">Dr</option>
	                                                                    <option value="Other">Other</option>
                                                                    </select>                                   
                                                                </div>
                                                            </td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td style="width:290px;"><div><input class="aa_input" required id="bus_givenNames" name="bus_givenNames" style="width:100%;" type="text"  value=""  /></div></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="aa_label_font">COUNTRY OF BIRTH *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">DATE OF BIRTH (DOB) *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">PLACE OF BIRTH *</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style__width_186">
                                                                <div>
                                                                    <select id="bus_countryofbirth" name="bus_countryofbirth" required style="width:100%;">
	                                                                    <option selected="selected" value="0">--Select--</option>
	                                                                    <option value="1">Afghanistan</option>
	                                                                    <option value="2">Albania</option>
	                                                                    <option value="3">Algeria</option>
	                                                                    <option value="4">Andorra</option>
	                                                                    <option value="5">Angola</option>
	                                                                    <option value="6">Antigua and Barbuda</option>
	                                                                    <option value="7">Argentina</option>
	                                                                    <option value="8">Armenia</option>
	                                                                    <option value="9">Australia</option>
	                                                                    <option value="10">Austria</option>
	                                                                    <option value="11">Azerbaijan</option>
	                                                                    <option value="12">Bahamas</option>
	                                                                    <option value="13">Bahrain</option>
	                                                                    <option value="14">Bangladesh</option>
	                                                                    <option value="15">Barbados</option>
	                                                                    <option value="16">Belarus</option>
	                                                                    <option value="17">Belgium</option>
	                                                                    <option value="18">Belize</option>
	                                                                    <option value="19">Benin</option>
	                                                                    <option value="20">Bhutan</option>
	                                                                    <option value="21">Bolivia</option>
	                                                                    <option value="22">Bosnia and Herzegovina</option>
	                                                                    <option value="23">Botswana</option>
	                                                                    <option value="24">Brazil</option>
	                                                                    <option value="25">Brunei</option>
	                                                                    <option value="26">Bulgaria</option>
	                                                                    <option value="27">Burkina Faso</option>
	                                                                    <option value="28">Burundi</option>
	                                                                    <option value="29">Cambodia</option>
	                                                                    <option value="30">Cameroon</option>
	                                                                    <option value="31">Canada</option>
	                                                                    <option value="32">Cape Verde</option>
	                                                                    <option value="33">Central African Republic</option>
	                                                                    <option value="34">Chad</option>
	                                                                    <option value="35">Chile</option>
	                                                                    <option value="36">China</option>
	                                                                    <option value="37">Colombia</option>
	                                                                    <option value="38">Comoros</option>
	                                                                    <option value="39">Congo (Kinshasa)</option>
	                                                                    <option value="40">Congo (Brazzaville)</option>
	                                                                    <option value="41">Costa Rica</option>
	                                                                    <option value="42">Cote d Ivoire (Ivory Coast)</option>
	                                                                    <option value="43">Croatia</option>
	                                                                    <option value="44">Cuba</option>
	                                                                    <option value="45">Cyprus</option>
	                                                                    <option value="46">Czech Republic</option>
	                                                                    <option value="47">Denmark</option>
	                                                                    <option value="48">Djibouti</option>
	                                                                    <option value="49">Dominica</option>
	                                                                    <option value="50">Dominican Republic</option>
	                                                                    <option value="51">Ecuador</option>
	                                                                    <option value="52">Egypt</option>
	                                                                    <option value="53">El Salvador</option>
	                                                                    <option value="54">Equatorial Guinea</option>
	                                                                    <option value="55">Eritrea</option>
	                                                                    <option value="56">Estonia</option>
	                                                                    <option value="57">Ethiopia</option>
	                                                                    <option value="58">Fiji</option>
	                                                                    <option value="59">Finland</option>
	                                                                    <option value="60">France</option>
	                                                                    <option value="61">Gabon</option>
	                                                                    <option value="62">Gambia</option>
	                                                                    <option value="63">Georgia</option>
	                                                                    <option value="64">Germany</option>
	                                                                    <option value="65">Ghana</option>
	                                                                    <option value="66">Greece</option>
	                                                                    <option value="67">Grenada</option>
	                                                                    <option value="68">Guatemala</option>
	                                                                    <option value="69">Guinea</option>
	                                                                    <option value="70">Guinea-Bissau</option>
	                                                                    <option value="71">Guyana</option>
	                                                                    <option value="72">Haiti</option>
	                                                                    <option value="73">Honduras</option>
	                                                                    <option value="74">Hungary</option>
	                                                                    <option value="75">Iceland</option>
	                                                                    <option value="76">India</option>
	                                                                    <option value="77">Indonesia</option>
	                                                                    <option value="78">Iran</option>
	                                                                    <option value="79">Iraq</option>
	                                                                    <option value="80">Ireland</option>
	                                                                    <option value="81">Israel</option>
	                                                                    <option value="82">Italy</option>
	                                                                    <option value="83">Jamaica</option>
	                                                                    <option value="84">Japan</option>
	                                                                    <option value="85">Jordan</option>
	                                                                    <option value="86">Kazakhstan</option>
	                                                                    <option value="87">Kenya</option>
	                                                                    <option value="88">Kiribati</option>
	                                                                    <option value="89">Korea (North)</option>
	                                                                    <option value="90">Korea (South)</option>
	                                                                    <option value="91">Kuwait</option>
	                                                                    <option value="92">Kyrgyzstan</option>
	                                                                    <option value="93">Laos</option>
	                                                                    <option value="94">Latvia</option>
	                                                                    <option value="95">Lebanon</option>
	                                                                    <option value="96">Lesotho</option>
	                                                                    <option value="97">Liberia</option>
	                                                                    <option value="98">Libya</option>
	                                                                    <option value="99">Liechtenstein</option>
	                                                                    <option value="100">Lithuania</option>
	                                                                    <option value="101">Luxembourg</option>
	                                                                    <option value="102">Macedonia</option>
	                                                                    <option value="103">Madagascar</option>
	                                                                    <option value="104">Malawi</option>
	                                                                    <option value="105">Malaysia</option>
	                                                                    <option value="106">Maldives</option>
	                                                                    <option value="107">Mali</option>
	                                                                    <option value="108">Malta</option>
	                                                                    <option value="109">Marshall Islands</option>
	                                                                    <option value="110">Mauritania</option>
	                                                                    <option value="111">Mauritius</option>
	                                                                    <option value="112">Mexico</option>
	                                                                    <option value="113">Micronesia</option>
	                                                                    <option value="114">Moldova</option>
	                                                                    <option value="115">Monaco</option>
	                                                                    <option value="116">Mongolia</option>
	                                                                    <option value="117">Montenegro</option>
	                                                                    <option value="118">Morocco</option>
	                                                                    <option value="119">Mozambique</option>
	                                                                    <option value="120">Myanmar (Burma)</option>
	                                                                    <option value="121">Namibia</option>
	                                                                    <option value="122">Nauru</option>
	                                                                    <option value="123">Nepal</option>
	                                                                    <option value="124">Netherlands</option>
	                                                                    <option value="125">New Zealand</option>
	                                                                    <option value="126">Nicaragua</option>
	                                                                    <option value="127">Niger</option>
	                                                                    <option value="128">Nigeria</option>
	                                                                    <option value="129">Norway</option>
	                                                                    <option value="130">Oman</option>
	                                                                    <option value="131">Pakistan</option>
	                                                                    <option value="132">Palau</option>
	                                                                    <option value="133">Panama</option>
	                                                                    <option value="134">Papua New Guinea</option>
	                                                                    <option value="135">Paraguay</option>
	                                                                    <option value="136">Peru</option>
	                                                                    <option value="137">Philippines</option>
	                                                                    <option value="138">Poland</option>
	                                                                    <option value="139">Portugal</option>
	                                                                    <option value="140">Qatar</option>
	                                                                    <option value="141">Romania</option>
	                                                                    <option value="142">Russia</option>
	                                                                    <option value="143">Rwanda</option>
	                                                                    <option value="144">Saint Kitts and Nevis</option>
	                                                                    <option value="145">Saint Lucia</option>
	                                                                    <option value="147">Samoa</option>
	                                                                    <option value="148">San Marino</option>
	                                                                    <option value="149">Sao Tome and Principe</option>
	                                                                    <option value="150">Saudi Arabia</option>
	                                                                    <option value="151">Senegal</option>
	                                                                    <option value="152">Serbia</option>
	                                                                    <option value="153">Seychelles</option>
	                                                                    <option value="154">Sierra Leone</option>
	                                                                    <option value="155">Singapore</option>
	                                                                    <option value="156">Slovakia</option>
	                                                                    <option value="157">Slovenia</option>
	                                                                    <option value="158">Solomon Islands</option>
	                                                                    <option value="159">Somalia</option>
	                                                                    <option value="160">South Africa</option>
	                                                                    <option value="161">Spain</option>
	                                                                    <option value="162">Sri Lanka</option>
	                                                                    <option value="163">Sudan</option>
	                                                                    <option value="164">Suriname</option>
	                                                                    <option value="165">Swaziland</option>
	                                                                    <option value="166">Sweden</option>
	                                                                    <option value="167">Switzerland</option>
	                                                                    <option value="168">Syria</option>
	                                                                    <option value="169">Tajikistan</option>
	                                                                    <option value="170">Tanzania</option>
	                                                                    <option value="171">Thailand</option>
	                                                                    <option value="172">Timor-Leste (East Timor)</option>
	                                                                    <option value="173">Togo</option>
	                                                                    <option value="174">Tonga</option>
	                                                                    <option value="175">Trinidad and Tobago</option>
	                                                                    <option value="176">Tunisia</option>
	                                                                    <option value="177">Turkey</option>
	                                                                    <option value="178">Turkmenistan</option>
	                                                                    <option value="179">Tuvalu</option>
	                                                                    <option value="180">Uganda</option>
	                                                                    <option value="181">Ukraine</option>
	                                                                    <option value="182">United Arab Emirates</option>
	                                                                    <option value="183">United Kingdom</option>
	                                                                    <option value="184">United States</option>
	                                                                    <option value="185">Uruguay</option>
	                                                                    <option value="186">Uzbekistan</option>
	                                                                    <option value="187">Vanuatu</option>
	                                                                    <option value="188">Vatican City</option>
	                                                                    <option value="189">Venezuela</option>
	                                                                    <option value="190">Vietnam</option>
	                                                                    <option value="191">Yemen</option>
	                                                                    <option value="192">Zambia</option>
	                                                                    <option value="193">Zimbabwe</option>
	                                                                    <option value="194">Abkhazia</option>
	                                                                    <option value="195">Taiwan</option>
	                                                                    <option value="197">Northern Cyprus</option>
	                                                                    <option value="198">Pridnestrovie (Transnistria)</option>
	                                                                    <option value="200">South Ossetia</option>
	                                                                    <option value="202">Christmas Island</option>
	                                                                    <option value="203">Cocos (Keeling) Islands</option>
	                                                                    <option value="206">Norfolk Island</option>
	                                                                    <option value="207">New Caledonia</option>
	                                                                    <option value="208">French Polynesia</option>
	                                                                    <option value="209">Mayotte</option>
	                                                                    <option value="210">Saint Barthelemy</option>
	                                                                    <option value="211">Saint Martin</option>
	                                                                    <option value="212">Saint Pierre and Miquelon</option>
	                                                                    <option value="213">Wallis and Futuna</option>
	                                                                    <option value="216">Bouvet Island</option>
	                                                                    <option value="217">Cook Islands</option>
	                                                                    <option value="218">Niue</option>
	                                                                    <option value="219">Tokelau</option>
	                                                                    <option value="220">Guernsey</option>
	                                                                    <option value="221">Isle of Man</option>
	                                                                    <option value="222">Jersey</option>
	                                                                    <option value="223">Anguilla</option>
	                                                                    <option value="224">Bermuda</option>
	                                                                    <option value="227">British Virgin Islands</option>
	                                                                    <option value="228">Cayman Islands</option>
	                                                                    <option value="230">Gibraltar</option>
	                                                                    <option value="231">Montserrat</option>
	                                                                    <option value="232">Pitcairn Islands</option>
	                                                                    <option value="233">Saint Helena</option>
	                                                                    <option value="235">Turks and Caicos Islands</option>
	                                                                    <option value="236">Northern Mariana Islands</option>
	                                                                    <option value="237">Puerto Rico</option>
	                                                                    <option value="238">American Samoa</option>
	                                                                    <option value="240">Guam</option>
	                                                                    <option value="248">U.S. Virgin Islands</option>
	                                                                    <option value="250">Hong Kong</option>
	                                                                    <option value="251">Macau</option>
	                                                                    <option value="252">Faroe Islands</option>
	                                                                    <option value="253">Greenland</option>
	                                                                    <option value="254">French Guiana</option>
	                                                                    <option value="255">Guadeloupe</option>
	                                                                    <option value="256">Martinique</option>
	                                                                    <option value="257">Reunion</option>
	                                                                    <option value="258">Aland Islands</option>
	                                                                    <option value="259">Aruba</option>
	                                                                    <option value="260">Netherlands Antilles</option>
	                                                                    <option value="261">Svalbard</option>
	                                                                    <option value="264">Antarctica Territories</option>
	                                                                    <option value="265">Kosovo</option>
	                                                                    <option value="266">Palestinian Territories</option>
	                                                                    <option value="267">Western Sahara</option>
	                                                                    <option value="273">Sint Maarten</option>
	                                                                    <option value="274">South Sudan</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td class="style__width_186"><div><input required class="aa_input datePickerDOB" id="bus_dob" name="bus_dob" placeholder="DD/MM/YYYY" style="width:100%;" type="text" value="" /></div></td>
                                                            <td style="width:18px;"class="style__width_21">&nbsp;</td>
                                                            <td class="style__width_186"><div><input required class="aa_input" id="bus_placeofbirth" name="bus_placeofbirth" style="width:100%;" type="text" value=""  /></div></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                    <table class="tbl_width_560">
                        <tr>
                            <td class="aa_label_font">NATIONALITY *</td>
                            <td class="aa_label_font">&nbsp;</td>
                            <td class="aa_label_font">ALSO KNOWN AS (AKA)</td>
                        </tr>
                        <tr>
                            <td class="style__width_186">
                                <div>
                                    <select id="bus_nationality" name="bus_nationality" required style="width:100%;">
	                                    <option selected="selected" value="0">--Select--</option>
	                                    <option value="1">Afghani</option>
	                                    <option value="2">Albanian</option>
	                                    <option value="3">Algerian</option>
	                                    <option value="4">American</option>
	                                    <option value="5">Andorran</option>
	                                    <option value="6">Angolan</option>
	                                    <option value="202">Anguillan</option>
	                                    <option value="203">Antarctic</option>
	                                    <option value="7">Antiguans</option>
	                                    <option value="8">Argentinean</option>
	                                    <option value="9">Armenian</option>
	                                    <option value="204">Arubian</option>
	                                    <option value="10">Australian</option>
	                                    <option value="11">Austrian</option>
	                                    <option value="12">Azerbaijani</option>
	                                    <option value="208">Bahameese</option>
	                                    <option value="13">Bahamian</option>
	                                    <option value="14">Bahraini</option>
	                                    <option value="15">Bangladeshi</option>
	                                    <option value="16">Barbadian</option>
	                                    <option value="17">Barbudans</option>
	                                    <option value="206">Barthlemois</option>
	                                    <option value="19">Belarusian</option>
	                                    <option value="20">Belgian</option>
	                                    <option value="21">Belizean</option>
	                                    <option value="22">Beninese</option>
	                                    <option value="207">Bermudan</option>
	                                    <option value="23">Bhutanese</option>
	                                    <option value="24">Bolivian</option>
	                                    <option value="25">Bosnian</option>
	                                    <option value="26">Brazilian</option>
	                                    <option value="27">British</option>
	                                    <option value="28">Bruneian</option>
	                                    <option value="29">Bulgarian</option>
	                                    <option value="30">Burkinabe</option>
	                                    <option value="31">Burmese</option>
	                                    <option value="32">Burundian</option>
	                                    <option value="33">Cambodian</option>
	                                    <option value="34">Cameroonian</option>
	                                    <option value="35">Canadian</option>
	                                    <option value="36">Cape Verdean</option>
	                                    <option value="224">Caymanian</option>
	                                    <option value="37">Central African</option>
	                                    <option value="38">Chadian</option>
	                                    <option value="39">Chilean</option>
	                                    <option value="40">Chinese</option>
	                                    <option value="212">Christmas Islander</option>
	                                    <option value="209">Cocossian</option>
	                                    <option value="41">Colombian</option>
	                                    <option value="42">Comoran</option>
	                                    <option value="43">Congolese</option>
	                                    <option value="210">Cook Islander</option>
	                                    <option value="45">Costa Rican</option>
	                                    <option value="46">Croatian</option>
	                                    <option value="47">Cuban</option>
	                                    <option value="211">Curaaoan</option>
	                                    <option value="48">Cypriot</option>
	                                    <option value="49">Czech</option>
	                                    <option value="50">Danish</option>
	                                    <option value="51">Djiboutian</option>
	                                    <option value="52">Dominican</option>
	                                    <option value="54">Dutch</option>
	                                    <option value="56">Dutchwoman</option>
	                                    <option value="58">Ecuadorean</option>
	                                    <option value="59">Egyptian</option>
	                                    <option value="60">Emirian</option>
	                                    <option value="61">Equatorial Guinean</option>
	                                    <option value="62">Eritrean</option>
	                                    <option value="63">Estonian</option>
	                                    <option value="64">Ethiopian</option>
	                                    <option value="214">Falkland Islander</option>
	                                    <option value="215">Faroese</option>
	                                    <option value="65">Fijian</option>
	                                    <option value="66">Filipino</option>
	                                    <option value="67">Finnish</option>
	                                    <option value="68">French</option>
	                                    <option value="216">French Guianese</option>
	                                    <option value="233">French Polynesian</option>
	                                    <option value="69">Gabonese</option>
	                                    <option value="70">Gambian</option>
	                                    <option value="71">Georgian</option>
	                                    <option value="72">German</option>
	                                    <option value="73">Ghanaian</option>
	                                    <option value="217">Gibralterian</option>
	                                    <option value="74">Greek</option>
	                                    <option value="218">Greenlander</option>
	                                    <option value="75">Grenadian</option>
	                                    <option value="219">Guadeloupean</option>
	                                    <option value="220">Guamanian</option>
	                                    <option value="76">Guatemalan</option>
	                                    <option value="77">Guinean</option>
	                                    <option value="79">Guyanese</option>
	                                    <option value="80">Haitian</option>
	                                    <option value="81">Herzegovinian</option>
	                                    <option value="82">Honduran</option>
	                                    <option value="221">Hong Konger</option>
	                                    <option value="83">Hungarian</option>
	                                    <option value="84">I-Kiribati</option>
	                                    <option value="85">Icelander</option>
	                                    <option value="86">Indian</option>
	                                    <option value="87">Indonesian</option>
	                                    <option value="88">Iranian</option>
	                                    <option value="89">Iraqi</option>
	                                    <option value="91">Irish</option>
	                                    <option value="92">Israeli</option>
	                                    <option value="93">Italian</option>
	                                    <option value="94">Ivorian</option>
	                                    <option value="95">Jamaican</option>
	                                    <option value="96">Japanese</option>
	                                    <option value="97">Jordanian</option>
	                                    <option value="98">Kazakhstani</option>
	                                    <option value="99">Kenyan</option>
	                                    <option value="100">Kittian and Nevisian</option>
	                                    <option value="101">Kuwaiti</option>
	                                    <option value="102">Kyrgyz</option>
	                                    <option value="223">Kyrgyzstani</option>
	                                    <option value="103">Laotian</option>
	                                    <option value="104">Latvian</option>
	                                    <option value="105">Lebanese</option>
	                                    <option value="106">Liberian</option>
	                                    <option value="107">Libyan</option>
	                                    <option value="108">Liechtensteiner</option>
	                                    <option value="109">Lithuanian</option>
	                                    <option value="110">Luxembourger</option>
	                                    <option value="226">Macanese</option>
	                                    <option value="111">Macedonian</option>
	                                    <option value="246">Mahoran</option>
	                                    <option value="112">Malagasy</option>
	                                    <option value="113">Malawian</option>
	                                    <option value="114">Malaysian</option>
	                                    <option value="115">Maldivan</option>
	                                    <option value="116">Malian</option>
	                                    <option value="117">Maltese</option>
	                                    <option value="222">Manx</option>
	                                    <option value="118">Marshallese</option>
	                                    <option value="228">Martinican</option>
	                                    <option value="119">Mauritanian</option>
	                                    <option value="120">Mauritian</option>
	                                    <option value="121">Mexican</option>
	                                    <option value="122">Micronesian</option>
	                                    <option value="123">Moldovan</option>
	                                    <option value="124">Monacan</option>
	                                    <option value="125">Mongolian</option>
	                                    <option value="225">Montenegrin</option>
	                                    <option value="229">Montserratian</option>
	                                    <option value="126">Moroccan</option>
	                                    <option value="127">Mosotho</option>
	                                    <option value="18">Motswana</option>
	                                    <option value="128">Motswana</option>
	                                    <option value="129">Mozambican</option>
	                                    <option value="130">Namibian</option>
	                                    <option value="131">Nauruan</option>
	                                    <option value="132">Nepalese</option>
	                                    <option value="230">New Caledonian</option>
	                                    <option value="134">New Zealander</option>
	                                    <option value="135">Ni-Vanuatu</option>
	                                    <option value="136">Nicaraguan</option>
	                                    <option value="137">Nigerian</option>
	                                    <option value="138">Nigerien</option>
	                                    <option value="232">Niuean</option>
	                                    <option value="231">Norfolk Islander</option>
	                                    <option value="139">North Korean</option>
	                                    <option value="140">Northern Irish</option>
	                                    <option value="227">Northern Mariana Islander</option>
	                                    <option value="141">Norwegian</option>
	                                    <option value="142">Omani</option>
	                                    <option value="143">Pakistani</option>
	                                    <option value="144">Palauan</option>
	                                    <option value="237">Palestinian</option>
	                                    <option value="145">Panamanian</option>
	                                    <option value="146">Papua New Guinean</option>
	                                    <option value="147">Paraguayan</option>
	                                    <option value="148">Peruvian</option>
	                                    <option value="235">Pitcairn Islander</option>
	                                    <option value="149">Polish</option>
	                                    <option value="150">Portuguese</option>
	                                    <option value="236">Puerto Rican</option>
	                                    <option value="151">Qatari</option>
	                                    <option value="152">Romanian</option>
	                                    <option value="153">Russian</option>
	                                    <option value="154">Rwandan</option>
	                                    <option value="238">Saint Helenian</option>
	                                    <option value="155">Saint Lucian</option>
	                                    <option value="242">Saint Vincentian</option>
	                                    <option value="234">Saint-Pierrais</option>
	                                    <option value="156">Salvadoran</option>
	                                    <option value="157">Samoan</option>
	                                    <option value="158">San Marinese</option>
	                                    <option value="159">Sao Tomean</option>
	                                    <option value="160">Saudi</option>
	                                    <option value="161">Scottish</option>
	                                    <option value="162">Senegalese</option>
	                                    <option value="163">Serbian</option>
	                                    <option value="164">Seychellois</option>
	                                    <option value="165">Sierra Leonean</option>
	                                    <option value="166">Singaporean</option>
	                                    <option value="167">Slovakian</option>
	                                    <option value="168">Slovenian</option>
	                                    <option value="169">Solomon Islander</option>
	                                    <option value="170">Somali</option>
	                                    <option value="171">South African</option>
	                                    <option value="172">South Korean</option>
	                                    <option value="173">Spanish</option>
	                                    <option value="174">Sri Lankan</option>
	                                    <option value="175">Sudanese</option>
	                                    <option value="176">Surinamer</option>
	                                    <option value="177">Swazi</option>
	                                    <option value="178">Swedish</option>
	                                    <option value="179">Swiss</option>
	                                    <option value="180">Syrian</option>
	                                    <option value="181">Taiwanese</option>
	                                    <option value="182">Tajik</option>
	                                    <option value="183">Tanzanian</option>
	                                    <option value="184">Thai</option>
	                                    <option value="247">Tibetan </option>
	                                    <option value="57">Timorese</option>
	                                    <option value="185">Togolese</option>
	                                    <option value="240">Tokelauan</option>
	                                    <option value="186">Tongan</option>
	                                    <option value="187">Trinidadian or Tobagonian</option>
	                                    <option value="188">Tunisian</option>
	                                    <option value="189">Turkish</option>
	                                    <option value="241">Turkmen</option>
	                                    <option value="239">Turks and Caicos Islander</option>
	                                    <option value="190">Tuvaluan</option>
	                                    <option value="191">Ugandan</option>
	                                    <option value="192">Ukrainian</option>
	                                    <option value="193">Uruguayan</option>
	                                    <option value="194">Uzbekistani</option>
	                                    <option value="195">Venezuelan</option>
	                                    <option value="196">Vietnamese</option>
	                                    <option value="243">Virgin Islander-UK</option>
	                                    <option value="244">Virgin Islander-US</option>
	                                    <option value="245">Wallisian</option>
	                                    <option value="198">Welsh</option>
	                                    <option value="213">Western Saharan</option>
	                                    <option value="199">Yemenese</option>
	                                    <option value="200">Zambian</option>
	                                    <option value="201">Zimbabwean</option>
	                                    <option value="205">landic</option>
                                    </select>
                                </div>
                            </td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="style__width_393"><div><input class="aa_input" id="bus_aka" name="bus_aka" style="width:100%;" type="text"  value="" /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">OCCUPATION</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="bus_occupation" name="bus_occupation" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                    <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                    <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>

                <tr style="background-color:#EFEFEF;" hidden="hidden">
                    <td>
                    <table class="tbl_width_560" >
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td class="aa_label_font">CURRENT ADDRESS (Type address here to auto fill)</td>
                        </tr>
                        <tr hidden="hidden">
                            <td class="style__width_600"><div><input class="aa_input" id="bus_full_address" name="bus_full_address" style="width:100%;" type="text"  value="" <!--onFocus="geolocate()-->"></input></div></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                    </td>
                </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">UNIT NO</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET NO *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET NAME *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET TYPE *</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_50"><div><input class="aa_input" id="pbus_unitno" name="pbus_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                            <td class="style__width_10">&nbsp;</td>
                                            <td class="style__width_75"><div><input class="aa_input" required id="pbus_streetno" name="pbus_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" required id="pbus_streetname" name="pbus_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135">
                                                <div>
                                        
                                                    <select id="pbus_streettype" name="pbus_streettype" required style="width:100%;">
                                                         <option value="">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr hidden="hidden">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">ADDRESS LINE 1</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">ADDRESS LINE 2</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_route" name="pbus_route" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_addressline2" name="pbus_addressline2" style="width:100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">SUBURB *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STATE *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">POSTCODE *</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input required class="aa_input" id="ppbus_suburb" name="ppbus_suburb" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input required class="aa_input" id="ppbus_state" name="ppbus_state" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input required class="aa_input" id="ppbus_postcode" name="ppbus_postcode" style="width:100%;" type="text" value=""  /></div></td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">COUNTRY</td>
                                        </tr>
                                        <tr>
                                            <td><div><input class="aa_input" id="pbus_country" name="pbus_country" style="width:100%;" type="text" value="Australia"  /></div></td>
                                        </tr>
                                        <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                        <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">HOME PHONE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">WORK PHONE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">MOBILE PHONE *</td>
                                        </tr>
                                        <tr>
                                            <td style="width:175px;"><input class="aa_input" id="pbus_phonehome" name="pbus_phonehome" style="width:100%;" type="text"/></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:174px;"><input class="aa_input" id="pbus_phonework" name="pbus_phonework" style="width:100%;" type="text"/></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:175px;"><input required class="aa_input" id="pbus_phonemobile" name="pbus_phonemobile" style="width:100%;" type="text"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">EMAIL ADDRESS 1 *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">EMAIL ADDRESS 2</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input required class="aa_input" id="pbus_email1" name="pbus_email1" style="width:100%;" type="email" value="" /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_email2" name="pbus_email2" style="width:100%;" type="email" value=""  /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr style="height:20px;"><td class="spacer10">&nbsp;</td></tr>

                        </table>
                    </td>
                </tr>
                                </table>
                </div>
                    </td>
                </tr>
                
                <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td style="text-align: right;"><input class="wiz_btn_green_outline" id="btn_BusinessNext" type="button" value="Next" /> <input class="wiz_btn_green_outline" id="btn_BusinessBack" type="button" value="Back" /> <input class="aa_btn_red" id="btn_SubmitBusiness" type="button" value="Create Business" /> <input class="aa_btn_red" id="btn_Submit" type="button" value="Create Individual" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

            </table>
            </form>
        </div>
    </div>

</asp:Content>
