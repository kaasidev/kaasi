﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Text;
using Kapruka.Service.Logging;
using System.Data.Entity;
using Kapruka.Domain;

namespace KASI_Extend_
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Database.SetInitializer<MyContextKaasiModelQ>(null);
           // Database.SetInitializer<MyContextKaasi>(null);

            //SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Exception ex = Server.GetLastError().GetBaseException();

            var useragent = Request.UserAgent;
            var browser = Request.Browser;
            var userIpAddress = Request.UserHostAddress;

            StringBuilder template = new StringBuilder();
            template.AppendLine("Type            : [Type]");
            template.AppendLine("Name            : [Name]");
            template.AppendLine("Version         : [Version]");
            template.AppendLine("Major Version   : [Major Version]");
            template.AppendLine("Minor Version   : [Minor Version]");
            //  template.AppendLine("Platform        : [Platform]");
            template.AppendLine("UserAgent       : [UserAgent]");
            template.AppendLine("userIpAddress       : [userIpAddress]");

            if (browser != null)
                template.Replace("[Type]", browser.Type)
                        .Replace("[Name]", browser.Browser)
                        .Replace("[Version]", browser.Version)
                        .Replace("[Major Version]", browser.MajorVersion.ToString())
                        .Replace("[Minor Version]", browser.MinorVersion.ToString())
             .Replace("[userIpAddress]", userIpAddress);

            if (useragent != null)
                template.Replace("[UserAgent]", useragent.ToString());

            LogManager.GetLogger(Server.GetType())
                .Error("====== Unhandled error on web ======", ex);
        }
    }
}
