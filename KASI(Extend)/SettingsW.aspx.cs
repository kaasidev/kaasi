﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace KASI_Extend_
{
    public partial class SettingsW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            loadMasterCompanyDetails();
            //loadCurrencies();
            //buildAgentListforAAC();
            showTransferLimits();
            showSMTPDetails(Session["AgentID"].ToString());
            loadAgentBankAssignments(Session["AgentID"].ToString());
            showStats(Session["AgentID"].ToString());
            //buildCountryList(Session["AgentID"].ToString());
        }

        private void loadMasterCompanyDetails()
        {
            var AgentID = Session["AgentID"].ToString();
            hidden_AgentID.Value = AgentID;
            MasterCompany MC = new MasterCompany();
            Agent AG = new Agent();
            MastCompName.Text = AG.getAgentCompanyName(AgentID);
            //spn_MasterCompName.InnerText = MC.MasterCompanyName().ToUpper();
            //spn_MasterCompAddress.InnerText = MC.MasterCompanyFullAddress();
            Kapruka.Repository.Agent agent = new Kapruka.Repository.Agent();
            agent = AG.getAgentFullAddress(agent,AgentID);
            
            MastCompAddressLine1.Text = AG.getAgentFullAddress(AgentID);
            MastCompTele.Text = AG.getAgentTelephoneNumber(AgentID);
            AgentFax.Text = AG.getAgentFaxNumber(AgentID);
            MastCompEmail.Text = AG.getAgentEmailAddress(AgentID);;
            MastCompABN.Text = AG.getAgentABN(AgentID);
            MastCompACN.Text = AG.getAgentACN(AgentID);
            AustracNumber.Text = AG.getAgentAustracRegNumber(AgentID);
            txtRegNumber.Value = AustracNumber.Text;
            if (AG.getAgentAustracRegDate(AgentID).ToString() != "")
            {
                AustracRegDate.Text = Convert.ToDateTime(AG.getAgentAustracRegDate(AgentID)).ToString("dd/MM/yyyy");
            }
            else
            {
                AustracRegDate.Text = "";
            }
            txtRegDate.Value = AustracRegDate.Text;
            if (AG.getAgentAustracRenewalDate(AgentID).ToString() != "")
            {
                AustracRenDate.Text = Convert.ToDateTime(AG.getAgentAustracRenewalDate(AgentID)).ToString("dd/MM/yyyy");
            }
            else
            {
                AustracRenDate.Text = "";
            }

            txtRenDate.Value = AustracRenDate.Text;
            loadAgentAssignedAustralianBanks(AgentID);
            //buildAgentTiers(AgentID);
            showAgentOverrideLimits(AgentID);
            buildTransferTiersList(AgentID);

        }

        private void loadAgentAssignedAustralianBanks(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT MCB.AccountName, MCB.AccountNumber, MCB.BankName, MCB.BSB FROM dbo.AgentAssignedBanks AAB INNER JOIN dbo.MasterCompanyBanks MCB ON MCB.BankAccountID = AAB.BankAccountID WHERE AAB.AgentID = " + AID;
            
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = strQuery;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            TableRow row = new TableRow();
                            TableCell cell1 = new TableCell();
                            TableCell cell2 = new TableCell();
                            TableCell cell3 = new TableCell();
                            TableCell cell4 = new TableCell();
                            TableCell cell5 = new TableCell();
                            cell2.Text = sdr["BSB"].ToString();
                            cell2.CssClass = "ag_contents_text";
                            cell3.Text = sdr["AccountNumber"].ToString();
                            cell3.CssClass = "ag_contents_text";
                            cell4.Text = sdr["AccountName"].ToString();
                            cell4.CssClass = "ag_contents_text";
                            cell5.Text = sdr["BankName"].ToString();
                            cell5.CssClass = "ag_contents_text";
                            row.Cells.Add(cell2);
                            row.Cells.Add(cell3);
                            row.Cells.Add(cell4);
                            row.Cells.Add(cell5);
                            tbl_AgentBankAssignment.Rows.Add(row);
                        }
                    }
                    
                }
            }
        }

        private void loadAgentBankAssignments(String AID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT MCB.BSB, MCB.AccountNumber, MCB.AccountName, MCB.BankName FROM dbo.AgentAssignedBanks AAB
                                        INNER JOIN dbo.MasterCompanyBanks MCB ON AAB.MasterBankAccountID = MCB.BankAccountID WHERE AAB.AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        TableRow tr = new TableRow();
                        TableCell cell1 = new TableCell();
                        TableCell cell2 = new TableCell();
                        TableCell cell3 = new TableCell();
                        TableCell cell4 = new TableCell();

                        cell1.Text = sdr["BSB"].ToString();
                        cell2.Text = sdr["AccountNumber"].ToString();
                        cell3.Text = sdr["AccountName"].ToString();
                        cell4.Text = sdr["BankName"].ToString();
                        cell1.CssClass = "ag_contents_text";
                        cell2.CssClass = "ag_contents_text";
                        cell3.CssClass = "ag_contents_text";
                        cell4.CssClass = "ag_contents_text";
                        tr.Cells.Add(cell1);
                        tr.Cells.Add(cell2);
                        tr.Cells.Add(cell3);
                        tr.Cells.Add(cell4);
                        tbl_AgentBankAssignment.Rows.Add(tr);
                    }
                }
                conn.Close();
            }
        }

        private void loadCurrencies()
        {
            MasterCompany MC = new MasterCompany();
            Dictionary<String, String> Currencies = MC.getMasterCurrencies();

            foreach (var item in Currencies)
            {
                HtmlGenericControl s = new HtmlGenericControl("span");
                s.Attributes.Add("class", "settings_007_tags");
                s.InnerHtml = item.Key + " " + item.Value;
                // div_showcurrencies.Controls.Add(s);
                //div_showcurrencies.Controls.Add(new LiteralControl("&nbsp;"));
            }
        }

        private void showTransferLimits()
        {
            classes.Settings ST = new classes.Settings();

            txt_dftmonlimit.Text = ST.getSystemMonthlyAUDLimit();
            txt_dftyearlylimit.Text = ST.getSystemYearlyAUDLimit();
            txt_dftmtranslimit.Text = ST.getSystemMonthlyTransLimit().ToString();
            txt_dftytranslimit.Text = ST.getSystemYearlyTransLimit().ToString();
        }

        private void showAgentOverrideLimits(String AID)
        {
            classes.Agent AGT = new classes.Agent();
            classes.Settings ST = new classes.Settings();
            String Limit1 = AGT.getAgentMonthlyAUDLimit(AID);
            String Limit2 = AGT.getAgentYearlyAUDLimit(AID);
            String Limit3 = AGT.getAgentMonthlyTransLimit(AID);
            String Limit4 = AGT.getAgentYearlyTransLimit(AID);

            if (Limit1 != "")
            {
                txt_agtmonlimit.Text = String.Format("{0:C2}", float.Parse(Limit1));
            }
            else
            {
                txt_agtmonlimit.Text = ST.getSystemMonthlyAUDLimit();
            }

            if (Limit2 != "")
            {
                txt_agtyearlylimit.Text = String.Format("{0:C2}", float.Parse(Limit2));
            }
            else
            {
                txt_agtyearlylimit.Text = ST.getSystemYearlyAUDLimit();
            }

            if (Limit3 != "")
            {
                txt_agtmtranslimit.Text = Limit3;
            }
            else
            {
                txt_agtmtranslimit.Text = ST.getSystemMonthlyTransLimit().ToString();
            }

            if (Limit4 != "")
            {
                txt_agtytranslimit.Text = Limit4;
            }
            else
            {
                txt_agtytranslimit.Text = ST.getSystemYearlyTransLimit().ToString();
            }

        }

        private void buildTransferTiersList(String AID)
        {
            TBL_TransferTiers.Width = new Unit("100%");
            String table = String.Empty;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            SqlConnection conn2 = new SqlConnection();
            conn2.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"SELECT AAC.CountryID, CTR.CountryName FROM dbo.AgentAssignedCountries AAC INNER JOIN dbo.Countries CTR
                                    ON CTR.CountryID = AAC.CountryID WHERE AgentID = " + AID;
                cmd.Connection = conn;
                conn.Open();

                

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            table += "<table style='width:860px;'><tr><td>";

                            table += "<table style='width:860px;'><tr>";

                            table += "<td class='ag_contents_head_country' style='width:19%; text-align:left; font-weight:bold;'>" + sdr["CountryName"].ToString().ToUpper() + "</td>";
                            table += "<td class='ag_contents_head_t1' style='width:21%; text-align:right;'>&nbsp;</td>";
                            table += "<td class='ag_contents_head_t1' style='width:6%; text-align:right;'>T1</td>";
                            table += "<td class='ag_contents_head_t2' style='width:21%; text-align:right;'>&nbsp;</td>";
                            table += "<td class='ag_contents_head_t2' style='width:6%; text-align:right;'>T2</td>";
                            table += "<td class='ag_contents_head_t3' style='width:21%; text-align:right;'>&nbsp;</td>";
                            table += "<td class='ag_contents_head_t3' style='width:6%; text-align:right;'>T3</td>";

                            table += "</tr></table>";
                            table += "</td></tr><tr><td>";

                            table += "<table style='width:100%;'>";
                            using (SqlCommand cmd2 = new SqlCommand())
                            {
                                cmd2.CommandText = @"SELECT *, CURR.CurrencyCode FROM dbo.AgentTransactionFeeTiers ATFT INNER JOIN dbo.Currencies CURR
                                                    ON CURR.CurrencyID = ATFT.CurrencyID WHERE AgentID = " + AID +" AND CountryID = " + sdr["CountryID"].ToString();
                                cmd2.Connection = conn2;
                                conn2.Open();

                                using (SqlDataReader sdr2 = cmd2.ExecuteReader())
                                {
                                    while (sdr2.Read())
                                    {
                                        table += "<tr>";

                                        table += "<td class='ag_contents_text' style='width:19%; text-align:left;'>" + sdr2["CurrencyCode"].ToString() + "</td>";
                                        table += "<td class='ag_contents_label' style='width:21%; text-align:right; background-color:#FFFFFF;'>" + String.Format("{0:C2}", float.Parse(sdr2["Tier1From"].ToString())) + " - " + String.Format("{0:C2}", float.Parse(sdr2["Tier1To"].ToString())) + "</td>";
                                        table += "<td class='ag_contents_text' style='width:6%; text-align:right; font-weight:bold;'>" + String.Format("{0:C2}", float.Parse(sdr2["Tier1Amount"].ToString())) + "</td>";
                                        table += "<td class='ag_contents_label' style='width:21%; text-align:right; background-color:#FFFFFF;'>" + String.Format("{0:C2}", float.Parse(sdr2["Tier2From"].ToString())) + " - " + String.Format("{0:C2}", float.Parse(sdr2["Tier2To"].ToString())) + "</td>";
                                        table += "<td class='ag_contents_text' style='width:6%; text-align:right; font-weight:bold;'>" + String.Format("{0:C2}", float.Parse(sdr2["Tier2Amount"].ToString())) + "</td>";
                                        table += "<td class='ag_contents_label' style='width:21%; text-align:right; background-color:#FFFFFF;'>" + String.Format("{0:C2}", float.Parse(sdr2["Tier3From"].ToString())) + " - " + String.Format("{0:C2}", float.Parse(sdr2["Tier3To"].ToString())) + "</td>";
                                        table += "<td class='ag_contents_text' style='width:6%; text-align:right; font-weight:bold;'>" + String.Format("{0:C2}", float.Parse(sdr2["Tier3Amount"].ToString())) + "</td>";

                                        table += "</tr>";
                                    }
                                }

                                conn2.Close();
                            }

                            table += "</table>";
                            table += "</td></tr><tr class='spacer10'><td>&nbsp;</td></tr></table>";
                        }
                    }
                }
                conn.Close();
            }

            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            cell.Text = table;
            row.Cells.Add(cell);
            TBL_TransferTiers.Rows.Add(row);

            
        }

        private void showSMTPDetails(String AID)
        {
            classes.Agent AGT = new classes.Agent();

            Lbl_smtpservername.Text = AGT.getSMTPServerName(AID);
            Lbl_smtpport.Text = AGT.getSMTPPort(AID);
            Lbl_sslenabled.Text = AGT.getSMTPSSLSetting(AID);
            Lbl_username.Text = AGT.getSMTPUsername(AID);
            Lbl_emailaddress.Text = AGT.getSMTPEmailAddress(AID);

            //EDIT_SMTPName.Value = AGT.getSMTPServerName(AID);
            //EDIT_SMTPPort.Value = AGT.getSMTPPort(AID);
            //EDIT_SMTPEmailAddress.Value = AGT.getSMTPEmailAddress(AID);
            //EDIT_SMTPUsername.Value = AGT.getSMTPUsername(AID);
            //EDIT_SMTPPassword.Value = AGT.getSMTPPassword(AID);
        }

        protected void btn_TestSMTP_Click(object sender, EventArgs e)
        {
            //SMTPTestResult.InnerText = "Please wait. Testing SMTP Connection.";
            //String ReceivingEmail = "sj@invetico.com.au";

            //SmtpClient smtpClient = new SmtpClient();
            //NetworkCredential basicCredentials = new NetworkCredential(EDIT_SMTPUsername.Value, EDIT_SMTPPassword.Value);
            //MailMessage message = new MailMessage();
            //MailAddress fromAddress = new MailAddress(EDIT_SMTPEmailAddress.Value);

            //smtpClient.Host = EDIT_SMTPName.Value;
            //smtpClient.UseDefaultCredentials = false;
            //smtpClient.Credentials = basicCredentials;

            //message.From = fromAddress;
            //message.Subject = "SMTP Server Test Email";
            //message.IsBodyHtml = true;
            //message.Body = "<h1>TEST SUCCESSFULL</h1>";
            //message.To.Add(ReceivingEmail);

            //try
            //{
            //    smtpClient.Send(message);
            //    SMTPTestResult.InnerText = "SMTP Server Configuration Successfull";
            //}
            //catch (Exception ex)
            //{
            //    SMTPTestResult.InnerText = ex.Message;
            //}
        }

        private void showStats(String AID)
        {
            classes.Agent AGT = new classes.Agent();
            lbl_TotalCustomer.Text = AGT.getAgentTotalNumberOfCustomers(AID);

            float totCust = float.Parse(AGT.getAgentTotalNumberOfCustomers(AID));
            float actCust = float.Parse(AGT.getAgentCustomersThatHaveTransactedInLast12Month(AID));

            float ActPerc = (actCust / totCust) * 100;

            myStat.Attributes.Add("data-total", "100");
            myStat.Attributes.Add("data-percent", ActPerc.ToString());
        }

    }
}