﻿using Kapruka.Enterprise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_
{
    public partial class Transactions : System.Web.UI.Page
    {
        string tranId = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            else
            {
                txt_IsComplianceManager.Value = Session["IsComplianceManager"].ToString();
                txt_hiddenCompliancePIN.Value = Session["CompliancePIN"].ToString();
                SPN_LoggedUserName.InnerText = Session["LoggedPersonName"].ToString();

                String TransID = String.Empty;

                TransID = Request.QueryString["transid"];

                if (!String.IsNullOrEmpty(TransID))
                {
                    var tranID = Convert.ToInt32(TransID);
                    var transaction = new TransactionService(new Kapruka.Repository.
                        UnitOfWorks(new Kapruka.Repository.KaprukaEntities())).
                        GetTransactionById(tranID);

                    tranObJectVal.Value =  transaction.Status.ToString();
                    dollaramount.Value = transaction.DollarAmount.ToString();

                    rateTran.Value = transaction.Rate.ToString();
                    cusIdVal.Value = transaction.CustomerID.ToString();
                    tranIDval.Value = transaction.TransactionID.ToString();
                    bankName.Value = transaction.Bank.ToString();
                    banckOrign.Value = transaction.SourceOfFunds.ToString();
                    //txt_workingTransID.Value = TransID;
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "openReviewDialog(" + transaction.TransactionID
                    //+ "," + transaction.CustomerID + "," + transaction.DollarAmount + "," + transaction.Rate
                    //+ ",'" + transaction.Bank + "'"
                    //+ ",'" + transaction.SourceOfFunds + "'"
                    //  + ",'" + transaction.Status + "'"
                    //+ "" + ");", true);
                }
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

        }
    }
}