﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using System.IO;
using KASI_Extend_.classes;
using Kapruka.Enterprise;

namespace KASI_Extend_
{
    public partial class FillPDF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

            CustomerService CustServ = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            TransactionService TransServ = new TransactionService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            BeneficiaryService BeneServ = new BeneficiaryService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            beneficiaryPaymentMethodService BPMServ = new beneficiaryPaymentMethodService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            AgentService AGTServ = new AgentService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));

            int TransacID = Int32.Parse(Request.QueryString["TID"].ToString());
            int custId = 0;
            var TransDetails = TransServ.GetAll(x => x.TransactionID == TransacID, null, "").SingleOrDefault();
            custId = TransDetails.CustomerID.Value;
            var CustDetails = CustServ.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault();
            var BeneDetails = BeneServ.GetAll(x => x.BeneficiaryID == TransDetails.BeneficiaryID, null, "").SingleOrDefault();
            var BeneAccountDetails = BPMServ.GetAll(x => x.AccountID == TransDetails.AccountID.ToString(), null, "").SingleOrDefault();
            var AgentDetails = AGTServ.GetAll(x => x.AgentID == TransDetails.AgentID, null, "").SingleOrDefault();


            String FONT = @"C:\inetpub\wwwroot\KAASI\fonts\MyriadPro-Cond.ttf";
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment;filename=frm_RemAdviceForm.pdf");

            PdfReader r = new PdfReader(new RandomAccessFileOrArray(Request.MapPath(@"Templates\Txn_Receipt_v2.pdf")), null);
            var baseFont = BaseFont.CreateFont(FONT, BaseFont.CP1252, BaseFont.EMBEDDED);

            using (PdfStamper ps = new PdfStamper(r, Response.OutputStream))
            {
                AcroFields af = ps.AcroFields;
                ps.AcroFields.SetFieldProperty("AgentCompanyName_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("AgentCompanyName_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("AgentAddress_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("AgentAddress_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("ABN_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("ABN_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("CustomerNo_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("CustomerNo_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderName_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderName_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderAddress_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderAddress_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderContactNumber_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderContactNumber_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderSourceOfFunds_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("SenderSourceOfFunds_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Transaction Ref", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Transaction Ref_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Date", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Date_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("ABN_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("ABN_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Send Amount", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Send Amount_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Fee", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Fee_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Total", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Total_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Receive Method", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Receive Method_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Exchange Rate", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Exchange Rate_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Receive Amount", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Receive Amount_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("RecName_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("RecName_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("RecAddress_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("RecAddress_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Receive Method", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Receive Method_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("BenContactNo_1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("BenContactNo_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Relationship", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Relationship_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Purpose", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Purpose_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Bank", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Bank_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Branch", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Branch_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Account No", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Account No_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Customer NameRow1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Customer NameRow1_2", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Agent NameRow1", "textfont", baseFont, null);
                ps.AcroFields.SetFieldProperty("Agent NameRow1_2", "textfont", baseFont, null);


                af.SetField("AgentCompanyName_1", AgentDetails.AgentName);
                af.SetField("AgentCompanyName_2", AgentDetails.AgentName);
                af.SetField("AgentAddress_1", AgentDetails.AgentAddress1 + " " + AgentDetails.AgentAddress2 + " " + AgentDetails.AgentCity + " " + AgentDetails.AgentState + " " + AgentDetails.AgentPostcode + " | " + AgentDetails.AgentPhone + " | " + AgentDetails.AgentEmail);
                af.SetField("AgentAddress_2", AgentDetails.AgentAddress1 + " " + AgentDetails.AgentAddress2 + " " + AgentDetails.AgentCity + " " + AgentDetails.AgentState + " " + AgentDetails.AgentPostcode + " | " + AgentDetails.AgentPhone + " | " + AgentDetails.AgentEmail);
                af.SetField("ABN_1", "ABN: " + AgentDetails.AgentABN);
                af.SetField("ABN_2", "ABN: " + AgentDetails.AgentABN);
                af.SetField("CustomerNo_1", CustDetails.CustomerID.ToString());
                af.SetField("CustomerNo_2", CustDetails.CustomerID.ToString());
                af.SetField("SenderName_1", CustDetails.FullName);
                af.SetField("SenderName_2", CustDetails.FullName);
                if (String.IsNullOrEmpty(CustDetails.UnitNo))
                {
                    af.SetField("SenderAddress_1", CustDetails.StreetNo + " " + CustDetails.StreetName + " " + CustDetails.Suburb + " " + CustDetails.State + " " + CustDetails.Postcode);
                    af.SetField("SenderAddress_2", CustDetails.StreetNo + " " + CustDetails.StreetName + " " + CustDetails.Suburb + " " + CustDetails.State + " " + CustDetails.Postcode);
                }
                else
                {
                    af.SetField("SenderAddress_1", CustDetails.UnitNo + "/" + CustDetails.StreetNo + " " + CustDetails.StreetName + " " + CustDetails.Suburb + " " + CustDetails.State + " " + CustDetails.Postcode);
                    af.SetField("SenderAddress_2", CustDetails.UnitNo + "/" + CustDetails.StreetNo + " " + CustDetails.StreetName + " " + CustDetails.Suburb + " " + CustDetails.State + " " + CustDetails.Postcode);
                }
                
                af.SetField("SenderContactNumber_1", CustDetails.Mobile);
                af.SetField("SenderContactNumber_2", CustDetails.Mobile);
                af.SetField("SenderSourceOfFunds_1", TransDetails.SourceOfFunds);
                af.SetField("SenderSourceOfFunds_2", TransDetails.SourceOfFunds);
                af.SetField("Transaction Ref", TransDetails.TransactionID.ToString());
                af.SetField("Transaction Ref_2", TransDetails.TransactionID.ToString());
                af.SetField("Date", TransDetails.CreatedDateTime.ToString().Substring(0, 10));
                af.SetField("Date_2", TransDetails.CreatedDateTime.ToString().Substring(0, 10));
                af.SetField("Send Amount", String.Format("{0:N2}", float.Parse(TransDetails.DollarAmount.ToString())) + " AUD");
                af.SetField("Send Amount_2", String.Format("{0:N2}", float.Parse(TransDetails.DollarAmount.ToString())) + " AUD");
                af.SetField("Fee", String.Format("{0:N2}", float.Parse(TransDetails.ServiceCharge.ToString())) + " AUD");
                af.SetField("Fee_2", String.Format("{0:N2}", float.Parse(TransDetails.ServiceCharge.ToString())) + " AUD");
                var SendingTotal = float.Parse(TransDetails.DollarAmount.ToString()) + float.Parse(TransDetails.ServiceCharge.ToString());
                af.SetField("Total", String.Format("{0:N2}", float.Parse(SendingTotal.ToString())) + " AUD");
                af.SetField("Total_2", String.Format("{0:N2}", float.Parse(SendingTotal.ToString()))+ " AUD");
                if (BeneAccountDetails.AccountNumber.Contains("Advice") || BeneAccountDetails.AccountNumber.Contains("Advise"))
                {
                    af.SetField("Receive Method", "Advise & Pay");
                    af.SetField("Receive Method_2", "Advise & Pay");
                }
                else
                {
                    af.SetField("Receive Method", "Account Credit");
                    af.SetField("Receive Method_2", "Account Credit");
                }
                af.SetField("Exchange Rate", String.Format("{0:N2}", float.Parse(TransDetails.Rate.ToString())) + " " + TransDetails.RemittedCurrency);
                af.SetField("Exchange Rate_2", String.Format("{0:N2}", float.Parse(TransDetails.Rate.ToString())) + " " + TransDetails.RemittedCurrency);
                af.SetField("Receive Amount", String.Format("{0:N2}", float.Parse(TransDetails.RemittedAmount.ToString())) + " " + TransDetails.RemittedCurrency);
                af.SetField("Receive Amount_2", String.Format("{0:N2}", float.Parse(TransDetails.RemittedAmount.ToString())) + " " + TransDetails.RemittedCurrency);


                af.SetField("RecName_1", BeneDetails.BeneficiaryName);
                af.SetField("RecName_2", BeneDetails.BeneficiaryName);

                af.SetField("RecAddress_1", BeneDetails.AddressLine1 + " " + BeneDetails.Suburb + " " + BeneDetails.Country);
                af.SetField("RecAddress_2", BeneDetails.AddressLine1 + " " + BeneDetails.Suburb + " " + BeneDetails.Country);

                af.SetField("BenContactNo_1", BeneDetails.Mobile);
                af.SetField("BenContactNo_2", BeneDetails.Mobile);

                af.SetField("Relationship", BeneDetails.Relationship);
                af.SetField("Relationship_2", BeneDetails.Relationship);

                af.SetField("Purpose", TransDetails.Purpose);
                af.SetField("Purpose_2", TransDetails.Purpose);

                af.SetField("Bank", BeneAccountDetails.BankName);
                af.SetField("Bank_2", BeneAccountDetails.BankName);

                af.SetField("Branch", BeneAccountDetails.BranchName);
                af.SetField("Branch_2", BeneAccountDetails.BranchName);

                af.SetField("Account No", BeneAccountDetails.AccountNumber);
                af.SetField("Account No_2", BeneAccountDetails.AccountNumber);

                af.SetField("Customer NameRow1", CustDetails.FullName);
                af.SetField("Customer NameRow1_2", CustDetails.FullName);

                af.SetField("Agent NameRow1", AgentDetails.AgentName);
                af.SetField("Agent NameRow1_2", AgentDetails.AgentName);
                

                ps.FormFlattening = true;

            }
           
            Response.End();
           // Response.WriteFile(Request.MapPath(@"Templates\Txn_Receipt_v233333.pdf"));
        }
    }
}