﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Reports
{
    public partial class Trsummery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                divCon.Visible = false;
                DataTable agents = new DataTable();

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                    try
                    {

                        SqlDataAdapter adapter = new SqlDataAdapter("SELECT AgentName, AgentID FROM dbo.Agents", conn);
                        adapter.Fill(agents);

                        DropDownList1.DataSource = agents;
                        DropDownList1.DataTextField = "AgentName";
                        DropDownList1.DataValueField = "AgentID";
                        DropDownList1.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    conn.Close();
                }
                DropDownList1.Items.Insert(0, new ListItem("-- SELECT --", "0"));

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string from = FromDate.Text;
            string to = EndDate.Text;
            string agentId = DropDownList1.SelectedItem.Value;
            if (Session["AgentID"] == null)
            {
                agentId = "0";
            }
            GetNoofTransactions(agentId, from, to);
            GetTotalSend(agentId, from, to);
            GetTotalRecieved(agentId, from, to);
            GetTotalServiceCharges(agentId, from, to);
            GetTotalAgentCommision(agentId, from, to);
            GetTotalFlexCharges(agentId, from, to);
            divCon.Visible = true;
        }

        public void GetNoofTransactions(string AID,string from,string to)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                if (AID == "0")
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select count(TransactionID) as NoofTransactions from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "') ";
                }
                else
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select count(TransactionID) as NoofTransactions from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "') and AgentID=" + AID;
                }
                
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output =int.Parse(sdr["NoofTransactions"].ToString()).ToString("N0");
                        }
                    }
                    conn.Close();
                }
            }

            tdNoofTr.InnerHtml = output;
        }

        public void GetTotalSend(string AID, string from, string to)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = "";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                if (AID == "0")
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(RemittedAmount) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "')  group by Country,RemittedCurrency";
                }
                else
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(RemittedAmount) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "') and AgentID=" + AID + " group by Country,RemittedCurrency";
                }
                
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + "<tr><td>" + sdr["RemAmount"].ToString() + "</td>";
                            output = output + "<td>" + sdr["Country"].ToString() + "</td>";
                            output = output + "<td>" + sdr["RemittedCurrency"].ToString() + "</td></tr>";
                           
                        }
                    }
                    conn.Close();
                }
            }

            tbodyRemi.InnerHtml = output;
        }

        public void GetTotalRecieved(string AID, string from, string to)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = "";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                if (AID == "0")
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(DollarAmount) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "')  group by Country,RemittedCurrency";
                }
                else
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(DollarAmount) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "') and AgentID=" + AID + " group by Country,RemittedCurrency";
                }

                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + "<tr><td>" + sdr["RemAmount"].ToString() + "</td>";
                            output = output + "<td>" + sdr["Country"].ToString() + "</td>";
                            output = output + "<td>" + sdr["RemittedCurrency"].ToString() + "</td></tr>";

                        }
                    }
                    conn.Close();
                }
            }

            tbody1.InnerHtml = output;
        }

        public void GetTotalServiceCharges(string AID, string from, string to)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = "";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                if (AID == "0")
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(ServiceCharge) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "')  group by Country,RemittedCurrency";
                }
                else
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(ServiceCharge) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "') and AgentID=" + AID + " group by Country,RemittedCurrency";
                }

                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + "<tr><td>" + sdr["RemAmount"].ToString() + "</td>";
                            output = output + "<td>" + sdr["Country"].ToString() + "</td>";
                            output = output + "<td>" + sdr["RemittedCurrency"].ToString() + "</td></tr>";

                        }
                    }
                    conn.Close();
                }
            }

            tbody2.InnerHtml = output;
        }

        public void GetTotalAgentCommision(string AID, string from, string to)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = "";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                if (AID == "0")
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(AgentMoney) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "')  group by Country,RemittedCurrency";
                }
                else
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(AgentMoney) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "') and AgentID=" + AID + " group by Country,RemittedCurrency";
                }

                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + "<tr><td>" + sdr["RemAmount"].ToString() + "</td>";
                            output = output + "<td>" + sdr["Country"].ToString() + "</td>";
                            output = output + "<td>" + sdr["RemittedCurrency"].ToString() + "</td></tr>";

                        }
                    }
                    conn.Close();
                }
            }

            tbody3.InnerHtml = output;
        }

        public void GetTotalFlexCharges(string AID, string from, string to)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            String output = "";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                if (AID == "0")
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(MasterMoney) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "')  group by Country,RemittedCurrency";
                }
                else
                {
                    cmd.CommandText = "SET DATEFORMAT dmy select SUM(MasterMoney) as RemAmount,Country,RemittedCurrency from Transactions where CreatedDateTime between convert(datetime,'" + from + "') and convert(datetime,'" + to + "') and AgentID=" + AID + " group by Country,RemittedCurrency";
                }

                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            output = output + "<tr><td>" + sdr["RemAmount"].ToString() + "</td>";
                            output = output + "<td>" + sdr["Country"].ToString() + "</td>";
                            output = output + "<td>" + sdr["RemittedCurrency"].ToString() + "</td></tr>";

                        }
                    }
                    conn.Close();
                }
            }

            tbody4.InnerHtml = output;
        }
    }
}