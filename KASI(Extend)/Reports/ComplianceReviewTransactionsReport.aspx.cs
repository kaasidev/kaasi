﻿using KASI_Extend_.classes;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Reports
{
    public partial class ComplianceReviewTransactionsReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                if (Session["AgentID"] == null)
                {
                    DataTable agents = new DataTable();

                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                        try
                        {

                            SqlDataAdapter adapter = new SqlDataAdapter("SELECT AgentName, AgentID FROM dbo.Agents", conn);
                            adapter.Fill(agents);

                            DropDownList1.DataSource = agents;
                            DropDownList1.DataTextField = "AgentName";
                            DropDownList1.DataValueField = "AgentID";
                            DropDownList1.DataBind();
                        }
                        catch (Exception ex)
                        {

                        }
                        conn.Close();
                    }
                    DropDownList1.Items.Insert(0, new ListItem("-- SELECT --", "0"));
                }
                else
                {
                    DropDownList1.Visible = false;
                }
            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text, EndDate.Text, DropDownList1.SelectedValue.ToString());
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "Reports/ComplianceReviewTransactionsReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text),
                new ReportParameter("EndDate", EndDate.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(string fromDate, string toDate, String SelectedAgent)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = null;
                if (Session["AgentID"] == null)
                {
                    if (DropDownList1.SelectedItem.Value == "0")
                    {
                        cmd = new SqlCommand("SET DATEFORMAT dmy select A.*,(select FullName from Customers where CustomerID=A.CustomerID) AS CustomerLastName,(select Mobile from Customers where CustomerID=A.CustomerID) as CustomerMobile,(select BeneficiaryName from Beneficiaries where BeneficiaryID=A.BeneficiaryID) AS BeneficiaryName,(select CountryName from Countries where CountryID=(select CountryID from BeneficiaryPaymentMethods where AccountID=A.AccountID)) As Country from (select BeneficiaryID,AccountID,TransactionID,FORMAT(CreatedDateTime,'dd/MM/yyyy') AS CreatedDateTime,CONCAT(CONVERT(Decimal(9,2), ISNULL(DollarAmount,0)),' ','AUD') AS DollarAmount,CONCAT(CONVERT(Decimal(9,2),ISNULL(ServiceCharge,0)),' ','AUD') AS ServiceCharge,CONCAT(CONVERT(Decimal(9,2),(DollarAmount+ServiceCharge)),' ','AUD') AS TotalCharge,CONCAT(CONVERT(Decimal(9,2), ISNULL(RemittedAmount,0)),' ',RemittedCurrency) AS RemittedAmount,Isnull(CancellationFee,0) as CancFee,ComplianceReason as RevReason,ReviewedBy as RevBy,VoidReason as CancReason,CancelledBy as CancBy,Bank,RemittedCurrency,Status,CONCAT(CONVERT(Decimal(9,2),Rate),' ',RemittedCurrency) AS Rate,CustomerID,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent from Transactions where CreatedDateTime between Convert(datetime,'" + fromDate + "') and Convert(datetime,'" + toDate + "') and Status='REVIEW' ) AS A", conn);
                    }
                    else
                    {
                        cmd = new SqlCommand("SET DATEFORMAT dmy select A.*,(select FullName from Customers where CustomerID=A.CustomerID) AS CustomerLastName,(select Mobile from Customers where CustomerID=A.CustomerID) as CustomerMobile,(select BeneficiaryName from Beneficiaries where BeneficiaryID=A.BeneficiaryID) AS BeneficiaryName,(select CountryName from Countries where CountryID=(select CountryID from BeneficiaryPaymentMethods where AccountID=A.AccountID)) As Country from (select BeneficiaryID,AccountID,TransactionID,FORMAT(CreatedDateTime,'dd/MM/yyyy') AS CreatedDateTime,CONCAT(CONVERT(Decimal(9,2), ISNULL(DollarAmount,0)),' ','AUD') AS DollarAmount,CONCAT(CONVERT(Decimal(9,2),ISNULL(ServiceCharge,0)),' ','AUD') AS ServiceCharge,CONCAT(CONVERT(Decimal(9,2),(DollarAmount+ServiceCharge)),' ','AUD') AS TotalCharge,CONCAT(CONVERT(Decimal(9,2), ISNULL(RemittedAmount,0)),' ',RemittedCurrency) AS RemittedAmount,Isnull(CancellationFee,0) as CancFee,ComplianceReason as RevReason,ReviewedBy as RevBy,VoidReason as CancReason,CancelledBy as CancBy,Bank,RemittedCurrency,Status,CONCAT(CONVERT(Decimal(9,2),Rate),' ',RemittedCurrency) AS Rate,CustomerID,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent from Transactions where CreatedDateTime between Convert(datetime,'" + fromDate + "') and Convert(datetime,'" + toDate + "') and AgentID=" + DropDownList1.SelectedItem.Value + " and Status='REVIEW') AS A", conn);

                    }
                }
                else
                {

                    string agentName = new Agent().getAgentNameByID(Session["AgentID"].ToString());
                    cmd = new SqlCommand("SET DATEFORMAT dmy select A.*,(select FullName from Customers where CustomerID=A.CustomerID) AS CustomerLastName,(select Mobile from Customers where CustomerID=A.CustomerID) as CustomerMobile,(select BeneficiaryName from Beneficiaries where BeneficiaryID=A.BeneficiaryID) AS BeneficiaryName,(select CountryName from Countries where CountryID=(select CountryID from BeneficiaryPaymentMethods where AccountID=A.AccountID)) As Country from (select BeneficiaryID,AccountID,TransactionID,FORMAT(CreatedDateTime,'dd/MM/yyyy') AS CreatedDateTime,CONCAT(CONVERT(Decimal(9,2), ISNULL(DollarAmount,0)),' ','AUD') AS DollarAmount,CONCAT(CONVERT(Decimal(9,2),ISNULL(ServiceCharge,0)),' ','AUD') AS ServiceCharge,CONCAT(CONVERT(Decimal(9,2),(DollarAmount+ServiceCharge)),' ','AUD') AS TotalCharge,CONCAT(CONVERT(Decimal(9,2), ISNULL(RemittedAmount,0)),' ',RemittedCurrency) AS RemittedAmount,Isnull(CancellationFee,0) as CancFee,ComplianceReason as RevReason,ReviewedBy as RevBy,VoidReason as CancReason,CancelledBy as CancBy,Bank,RemittedCurrency,Status,CONCAT(CONVERT(Decimal(9,2),Rate),' ',RemittedCurrency) AS Rate,CustomerID,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + agentName + "' AS SelectedAgent from Transactions where CreatedDateTime between Convert(datetime,'" + fromDate + "') and Convert(datetime,'" + toDate + "') and AgentID=" + Session["AgentID"].ToString() + " and Status='REVIEW') AS A", conn);
                }

                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);


            }

            return dt;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}