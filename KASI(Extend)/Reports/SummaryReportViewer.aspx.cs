﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Reports
{
    public partial class SummaryReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                
            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text);
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "Reports/SummaryReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text)
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(string fromDate)
        {
            string newD = GetNewDate(fromDate);
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            DateTime dat = Convert.ToDateTime(newD);
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = null;
                string query = "SET DATEFORMAT dmy select D.*,(concat((select DATENAME(MONTH,'" + dat.ToString("dd/MM/yyyy") + "')),' ',(select DATENAME(YEAR,'" + dat.ToString("dd/MM/yyyy") + "')))) as SelectedMonthYear from(select A.AgentName,Count(T.TransactionID) as TotalTransactions,(select Count(T.TransactionID) as TotalTransactions" +

                               "  from Transactions as T "+
                               " inner join Agents as A on T.AgentID = A.AgentID "+
                               "  where LEFT(CONVERT(varchar, T.CreatedDateTime, 112), 6)= '"+ dat.ToString("yyyyMM") + "' and T.Status = 'CANCELLED') as CancelledTransactions "+

                               "  from Transactions as T "+
                               " inner join Agents as A on T.AgentID = A.AgentID "+
                               "  where LEFT(CONVERT(varchar, T.CreatedDateTime, 112), 6)= '" + dat.ToString("yyyyMM") + "' group by A.AgentName) as D";
                cmd = new SqlCommand(query, conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);


            }

            return dt;
        }

        private string GetNewDate(string fromDate)
        {
            string[] arr = fromDate.Split(' ');
            if(arr[0]=="January")
            {
                return "01/01/" + arr[1];
            }
            else if(arr[0] == "February")
            {
                return "01/02/" + arr[1];
            }
            else if (arr[0] == "March")
            {
                return "01/03/" + arr[1];
            }
            else if (arr[0] == "April")
            {
                return "01/04/" + arr[1];
            }
            else if (arr[0] == "May")
            {
                return "01/05/" + arr[1];
            }
            else if (arr[0] == "June")
            {
                return "01/06/" + arr[1];
            }
            else if (arr[0] == "July")
            {
                return "01/07/" + arr[1];
            }
            else if (arr[0] == "August")
            {
                return "01/08/" + arr[1];
            }
            else if (arr[0] == "September")
            {
                return "01/09/" + arr[1];
            }
            else if (arr[0] == "October")
            {
                return "01/10/" + arr[1];
            }
            else if (arr[0] == "November")
            {
                return "01/11/" + arr[1];
            }
            else
            {
                return "01/12/" + arr[1];
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}