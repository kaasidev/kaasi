﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Kapruka.Service.Logging;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace KASI_Extend_.Reports
{
    public partial class CustomersWithAccountBalance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

            if (!IsPostBack)
            {
                if (Session["AgentID"] == null)
                {
                    DataTable agents = new DataTable();
                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                        try
                        {

                            SqlDataAdapter adapter = new SqlDataAdapter("SELECT AgentName, AgentID FROM dbo.Agents", conn);
                            adapter.Fill(agents);

                            DDL_AgentList.DataSource = agents;
                            DDL_AgentList.DataTextField = "AgentName";
                            DDL_AgentList.DataValueField = "AgentID";
                            DDL_AgentList.DataBind();
                        }
                        catch (Exception ex)
                        {

                        }
                        conn.Close();
                        DDL_AgentList.Items.Insert(0, new ListItem("-- SELECT --", "0"));
                    }
                }
                else
                {
                    DDL_AgentList.Visible = false;
                }
            }
        }

        private void ShowReport()
        {
            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand("REPORT_getCustomersWithAccountBalance", conn))
                {
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AgentID", DDL_AgentList.SelectedValue);
                        cmd.Parameters.AddWithValue("@AgentName", DDL_AgentList.SelectedItem.Text);
                        da.Fill(dt);
                    }
                }
            }
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "Reports/CustomersWithBalanceInAccount.rdlc";
            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("AgentID", DDL_AgentList.SelectedItem.ToString())
            };
            ReportViewer1.LocalReport.Refresh();
        }


        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}