﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Reports
{
    public partial class TransactionsSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                DataTable agents = new DataTable();

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                    try
                    {

                        SqlDataAdapter adapter = new SqlDataAdapter("SELECT AgentName, AgentID FROM dbo.Agents", conn);
                        adapter.Fill(agents);

                        DropDownList1.DataSource = agents;
                        DropDownList1.DataTextField = "AgentName";
                        DropDownList1.DataValueField = "AgentID";
                        DropDownList1.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    conn.Close();
                }
                DropDownList1.Items.Insert(0, new ListItem("-- SELECT --", "0"));

            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text, EndDate.Text, DropDownList1.SelectedValue.ToString());
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "Reports/TrSummaryReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text),
                new ReportParameter("EndDate", EndDate.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(string fromDate, string toDate, String SelectedAgent)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = null;
                string sql = "";
                if (Session["AgentID"] == null)
                {
                    

                    if (DropDownList1.SelectedItem.Value == "0")
                    {
                        sql = "SET DATEFORMAT dmy select A.*,(select Count(TransactionID) from Transactions " +
                         "   where Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '"+fromDate+"') and convert(datetime, '"+toDate+"') " +
                         "   ) as CalCount," +
                         "   (select sum(RemittedAmount) from Transactions" +
                         "  where Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                        "    ) as CalAmount," +
                       "     (select avg(ServiceCharge) from Transactions" +
                        "      where Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                      "      ) as AvgCalFee," +
                      "'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent" +
                     "        from(select C.CountryName, T.RemittedCurrency, Count(TransactionID) as TransactionCount, sum(T.ServiceFee) as TotalTransactionFee, sum(DollarAmount) as AUDAmount, sum(T.RemittedAmount) as ForeignAmount, avg(DollarAmount) as AvgDollarAmount, avg(ServiceCharge) as AvgFee, avg(RemittedAmount) as AvgForeignAmount" +
                     "       from Transactions as T inner join Countries as C on T.CountryID = C.CountryID" +
                     "       where convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                     "       group by C.CountryName, T.RemittedCurrency) as A";

                        cmd = new SqlCommand(sql, conn);
                    }
                    else
                    {
                        sql = "SET DATEFORMAT dmy select A.*,(select Count(TransactionID) from Transactions " +
                        "   where AgentID=" + DropDownList1.SelectedItem.Value + " and Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "') " +
                        "   ) as CalCount," +
                        "   (select sum(RemittedAmount) from Transactions" +
                        "   where AgentID=" + DropDownList1.SelectedItem.Value + " and Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                       "    ) as CalAmount," +
                      "     (select avg(ServiceCharge) from Transactions" +
                       "      where AgentID=" + DropDownList1.SelectedItem.Value + " and Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                     "      ) as AvgCalFee," +
                       "'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent" +
                    "        from(select C.CountryName, T.RemittedCurrency, Count(TransactionID) as TransactionCount, sum(T.ServiceCharge) as TotalTransactionFee, sum(DollarAmount) as AUDAmount, sum(RemittedAmount) as ForeignAmount, avg(DollarAmount) as AvgDollarAmount, avg(ServiceCharge) as AvgFee, avg(RemittedAmount) as AvgForeignAmount" +
                    "       from Transactions as T inner join Countries as C on T.CountryID = C.CountryID" +
                    "       where T.AgentID="+ DropDownList1.SelectedItem.Value + " and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                    "       group by C.CountryName, T.RemittedCurrency) as A";

                        cmd = new SqlCommand(sql, conn);
                    }
                }
                else
                {
       

                    sql = "SET DATEFORMAT dmy select A.*,(select Count(TransactionID) from Transactions " +
                       "   where AgentID=" + Session["AgentID"].ToString() + " and Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "') " +
                       "   ) as CalCount," +
                       "   (select sum(RemittedAmount) from Transactions" +
                       "   where AgentID=" + Session["AgentID"].ToString() + " and Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                      "    ) as CalAmount," +
                     "     (select avg(ServiceCharge) from Transactions" +
                      "      where AgentID=" + Session["AgentID"].ToString() + " and Status = 'CANCELLED' and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                    "      ) as AvgCalFee," +
                      "'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + Session["AgentID"].ToString() + "' AS SelectedAgent" +
                   "        from(select C.CountryName, T.RemittedCurrency, Count(TransactionID) as TransactionCount, sum(T.ServiceCharge) as TotalTransactionFee, sum(DollarAmount) as AUDAmount, sum(RemittedAmount) as ForeignAmount, avg(DollarAmount) as AvgDollarAmount, avg(ServiceCharge) as AvgFee, avg(RemittedAmount) as AvgForeignAmount" +
                   "       from Transactions as T inner join Countries as C on T.CountryID = C.CountryID" +
                   "       where T.AgentID=" + Session["AgentID"].ToString() + " and convert(datetime, CreatedDateTime) between convert(datetime, '" + fromDate + "') and convert(datetime, '" + toDate + "')" +
                   "       group by C.CountryName, T.RemittedCurrency) as A";

                  
                    cmd = new SqlCommand(sql, conn);
                }
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);


            }

            return dt;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}