﻿using Microsoft.Reporting.WebForms;
using Kapruka.Service.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using KAASI.StoreSensitiveDataService;

namespace KASI_Extend_.Reports
{
    public partial class MonthlyTransactionsSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
              

            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text);
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "Reports/MonthlySummaryReport - Copy.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text)
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(string fromDate)
        {
            CultureInfo ci = new CultureInfo("en-GB");
            string newD = GetNewDate(fromDate);
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
            DateTime dat = Convert.ToDateTime(newD,ci);
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = null;
                string sql = "";
                var monyearPart = GetMonthYear(fromDate);
               // if(ddlAgents.SelectedItem.Value=="1")
               // {

                       // do  not use
                    //sql = @"SET DATEFORMAT dmy select A.*,(select count(TransactionID) from Transactions where Status='CANCELLED' and LEFT(CONVERT(varchar, CreatedDateTime, 112), 6)= '" + dat.ToString("yyyyMM") + "' and AgentID=A.AgentID) as TotalCanc,isnull((select sum(ServiceCharge) from Transactions where Status='CANCELLED' and LEFT(CONVERT(varchar, CreatedDateTime, 112), 6)= '" + dat.ToString("yyyyMM") + "' and AgentID=A.AgentID),0) as TotalCanFee,(concat((select DATENAME(MONTH,'" + dat.ToString("dd/MM/yyyy") + "')),' ',(select DATENAME(YEAR,'" + dat.ToString("dd/MM/yyyy") + "')))) as SelectedMonthYear " +

                    //  "  from(select A.AgentName,A.AgentID, Count(T.TransactionID) as NumOfTransactions, Sum(T.ServiceCharge) as TotalFees, " +
                    //"   Sum(T.DollarAmount) as AUDTotal, A.AgentFeeFlat as TotalFlatFee, A.AgentFeeCommission as AddFeePerc " +
                    // "   from Transactions as T inner join Agents as A on T.AgentID = A.AgentID " +
                    // "   where LEFT(CONVERT(varchar, T.CreatedDateTime, 112), 6) = '" + dat.ToString("yyyyMM") + "' and T.Status<>'CANCELLED'" +
                    // "   group by A.AgentName, A.AgentFeeCommission,A.AgentFeeFlat,A.AgentID) as A";



                    sql = @"SET DATEFORMAT dmy select isnull(statusnoCan.MCommishGainPercTotal,0) as MCommishGainPercTotal,isnull(statusnoCan.MAdditionalTransPercTotal,0) as MAdditionalTransPercTotal,isnull(statusnoCan.MTransferFeeTotal,0) as MTransferFeeTotal,isnull(statusCan.AgentName,statusnoCan.AgentName) as AgentName,isnull(statusCan.AgentID,statusnoCan.AgentID) as AgentID,isnull(statusnoCan.AddFeePerc,0) as AddFeePerc,isnull(statusCan.MTransferFee,0) as MTransferFee,isnull(statusCan.MAdditionalTransPerc,0) as MAdditionalTransPerc,isnull(statusnoCan.TotalFlatFee,0) as TotalFlatFee ,isnull(statusnoCan.AUDTotal,0) as AUDTotal,isnull(statusnoCan.NumOfTransactions,0) as NumOfTransactions, isnull(statusnoCan.TotalFees,0)
                            as TotalFees, ISNULL(statusCan.TotalCanc, 0 ) TotalCanc , ISNULL(statusCan.TotalCanFee, 0 ) TotalCanFee ,'" + fromDate  +"' SelectedMonthYear  from " +
                            @"(select A.AgentName,A.AgentID, Sum(T.DollarAmount) as AUDTotal,count(T.TransactionID) as NumOfTransactions, Sum(T.ServiceCharge) 
                            as TotalFees,A.AgentFeeFlat as TotalFlatFee, A.AgentFeeCommission as AddFeePerc,Sum(IsNull(T.MTransferFee,0)) as MTransferFee,Sum(IsNull(T.MAdditionalTransPerc,0)) as MAdditionalTransPerc,sum(isnull(T.MTransferFeeTotal,0)) as  MTransferFeeTotal,sum(isnull(T.MAdditionalTransPercTotal,0)) as  MAdditionalTransPercTotal,sum(isnull(T.MCommishGainPercTotal,0)) as  MCommishGainPercTotal from [dbo].Transactions T
                            inner join [dbo].Agents as A on T.AgentID = A.AgentID
                             and (T.Status<>'CANCELLED' and CONVERT(CHAR(4),  T.CreatedDateTime, 100)=@month 
                             and CONVERT(CHAR(4),  T.CreatedDateTime, 120)=@year ) 
                            group by A.AgentName,A.AgentID,A.AgentFeeCommission,A.AgentFeeFlat)  statusnoCan
                            full outer join (
                            select A.AgentName,A.AgentID,count(T.TransactionID) as TotalCanc, Sum(T.CancellationFee) 
                            as TotalCanFee,Sum(IsNull(T.MTransferFee,0)) as MTransferFee,Sum(IsNull(T.MAdditionalTransPerc,0)) as MAdditionalTransPerc,sum(isnull(T.MTransferFeeTotal,0)) as  MTransferFeeTotal,sum(isnull(T.MAdditionalTransPercTotal,0)) as  MAdditionalTransPercTotal,sum(isnull(T.MCommishGainPercTotal,0)) as  MCommishGainPercTotal from [dbo].Transactions T
                            inner join [dbo].Agents as A on T.AgentID = A.AgentID
                             and (T.Status='CANCELLED' and CONVERT(CHAR(4),  T.CreatedDateTime, 100)=@month 
                             and CONVERT(CHAR(4),  T.CreatedDateTime, 120)=@year ) 
                             group by A.AgentName, A.AgentID,A.AgentFeeCommission,A.AgentFeeFlat
                            ) statusCan 
                            on statusnoCan.AgentID=statusCan.AgentID
                            order by statusnoCan.AgentID";
                

                   

             //   }
               
              //  else
              //  {
              //      sql = "" +
              //       "  declare @AllAgents as Table(" +
              //      "   AgentName varchar(50), " +
              //      "   NumOfTransactions INT, " +
              //        " TotalFees money, " +
              //       "  AUDTotal money, " +
              //      "   TotalFlatFee money, " +
              //       "  AddFeePerc money, " +
              //      "   TotalCanc INT, " +
              //     "    SelectedMonthYear varchar(20) " +
              //    "     ) " +
              //        " DECLARE @AgentID INT,@AgentName varchar(100), @FlatPerc  decimal " +
              //        "  DECLARE cur CURSOR FOR  SELECT AgentID, AgentName, AgentFeeCommission FROM Agents " +
              //       "   open cur " +
              //      "    FETCH NEXT FROM cur " +
              //     "    INTO @AgentID, @AgentName, @FlatPerc " +
              //    "     WHILE @@FETCH_STATUS = 0 " +
              //   "      BEGIN " +
              //  "       insert into @AllAgents(NumOfTransactions, TotalFees, AUDTotal, TotalFlatFee, AgentName, AddFeePerc, TotalCanc, SelectedMonthYear) " +
              // "        select A.*, (select @AgentName) as AgentName,(select @FlatPerc) as AddFeePerc,(select count(TransactionID) from Transactions where Status = 'CANCELLED' and LEFT(CONVERT(varchar, CreatedDateTime, 112), 6)= '" + dat.ToString("yyyyMM") + "' and AgentID = @AgentID) as TotalCanc,'' as SelectedMonthYear " +
              //"          from(select Count(T.TransactionID) as NumOfTransactions, Sum(T.ServiceCharge) as TotalFees, " +
              //        " Sum(T.DollarAmount) as AUDTotal, Sum(A.AgentFeeFlat) as TotalFlatFee " +
              //       "   from Transactions as T right outer join Agents as A on T.AgentID = A.AgentID " +
              //      "    where LEFT(CONVERT(varchar, T.CreatedDateTime, 112), 6) = '" + dat.ToString("yyyyMM") + "' and T.AgentID = @AgentID " +
              //     "     ) as A " +


              //    "     FETCH NEXT FROM cur " +
              //   "      INTO @AgentID, @AgentName, @FlatPerc " +
              //  "       END " +
              //  "       CLOSE cur " +
              // "        DEALLOCATE cur " +

              //"         select* from @AllAgents ";
              //  }

               


                cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@year", monyearPart.YearPart);
                cmd.Parameters.AddWithValue("@month", monyearPart.MonthPart);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
               

            }

            return dt;
        }

        protected class DateSplit{
            public string MonthPart{get;set;}
            public string YearPart{get;set;}

        }

        private DateSplit GetMonthYear(string fromDate)
        {
            var obj = new DateSplit();
            var splitDate = fromDate.Split(' ');
            obj.MonthPart = splitDate[0].Substring(0,3);
            obj.YearPart = splitDate[1].Trim();

            return obj;
        }
        private string GetNewDate(string fromDate)
        {
            string[] arr = fromDate.Split(' ');
            if (arr[0] == "January")
            {
                return "01/01/" + arr[1];
            }
            else if (arr[0] == "February")
            {
                return "01/02/" + arr[1];
            }
            else if (arr[0] == "March")
            {
                return "01/03/" + arr[1];
            }
            else if (arr[0] == "April")
            {
                return "01/04/" + arr[1];
            }
            else if (arr[0] == "May")
            {
                return "01/05/" + arr[1];
            }
            else if (arr[0] == "June")
            {
                return "01/06/" + arr[1];
            }
            else if (arr[0] == "July")
            {
                return "01/07/" + arr[1];
            }
            else if (arr[0] == "August")
            {
                return "01/08/" + arr[1];
            }
            else if (arr[0] == "September")
            {
                return "01/09/" + arr[1];
            }
            else if (arr[0] == "October")
            {
                return "01/10/" + arr[1];
            }
            else if (arr[0] == "November")
            {
                return "01/11/" + arr[1];
            }
            else
            {
                return "01/12/" + arr[1];
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}