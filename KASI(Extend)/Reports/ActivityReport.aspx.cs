﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Microsoft.Reporting.WebForms;

namespace KASI_Extend.Reports
{
    public partial class ActivityReport : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session["TypeOfLogin"].ToString() == "Agent")
                {
                    this.Page.MasterPageFile = "~/AgentControl/Reports.Master";
                }
            }
            catch (Exception ex)
            {

            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

            if (!IsPostBack)
            {
               
            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text, EndDate.Text);
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = @"Reports\ActivityReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text),
                new ReportParameter("EndDate", EndDate.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(string fromDate, string toDate)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = new SqlCommand("REPORT_getActivityReport", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                //cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            return dt;
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(FromDate.Text) || String.IsNullOrEmpty(EndDate.Text))
            {
                LBL_ErrorMessage.Style.Add("display", "block");
                LBL_ErrorMessage.Text = "Please select date range before running report.";
            }
            else
            {
                
                    LBL_ErrorMessage.Style.Add("display", "none");
                    LBL_ErrorMessage.Text = "";
                    ShowReport();
                
            }
        }
    }
}