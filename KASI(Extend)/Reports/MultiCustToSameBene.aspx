﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportsNav.Master" AutoEventWireup="true" CodeBehind="MultiCustToSameBene.aspx.cs" Inherits="KASI_Extend.Reports.MultiCustToSameBene" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %><%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <header>
    
    <style>
        element.style {
            border-bottom: 10px solid #F00 !important;
        }
        /*input[type="image"]{
            padding:0px !important;
        }*/
       
         input[type="image"]{
                  padding:0px !important;
                    }
        
    </style>
   
      
   </header>
    <form id="form1" runat="server">

        <div class="container_btns">
        <table class="tbl_width_1200"> <!-- MAIN TABLE STARTS HERE -->
            <tr>
                <td class="rep_main_headings">REPORTS <i class="material-icons_rep">chevron_right</i> Activity Report</td>
                <td>&nbsp;</td>
            </tr>

        </table> <!-- MAIN TABLE ENDS HERE -->
    </div>

    <div class="container_one no_padding_btm">
        <table class="tbl_width_1200" >
            <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_1160">
                                    <tr><td><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></td></tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="aa_btn_green" OnClick="btnViewReport_Click" />
                                        </td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:right;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="container_one no_padding_top_btm">
     
        <table class="tbl_width_1200" >
            <tr style="height:20px;"><td>&nbsp;</td></tr>
            <tr style="border:1px solid #DCDCDC;">
                <td>
                    <p>&nbsp;<asp:Label ID="LBL_ErrorMessage" runat="server" style="display:none;"></asp:Label></p>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" AsyncRendering="true" SizeToReportContent="True" Width="100%" Height="500px">
                    
        </rsweb:ReportViewer>
                   
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>
        

        
    </form>
</asp:Content>
