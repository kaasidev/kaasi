﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using KASI_Extend_.classes;
using Microsoft.Reporting.WebForms;

namespace KASI_Extend.Reports
{
    public partial class MultiCustToSameBene : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session["TypeOfLogin"].ToString() == "Agent")
                {
                    this.Page.MasterPageFile = "~/AgentControl/Reports.Master";
                }
            }
            catch (Exception ex)
            {

            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

            if (!IsPostBack)
            {

            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData();
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = @"Reports\MultiCustomerToSameBeneficiary.rdlc";

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData()
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = new SqlCommand("REPORT_getSameBeneficiaryReceivingFromMultipleCustomerReport", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            return dt;
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}