﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportsNav.Master" AutoEventWireup="true" CodeBehind="Trsummery.aspx.cs" Inherits="KASI_Extend_.Reports.Trsummery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
   
    <form id="form1" runat="server">
      
        <div class="container_btns">
        <table class="tbl_width_1200"> <!-- MAIN TABLE STARTS HERE -->
            <tr>
                <td class="rep_main_headings">REPORTS <i class="material-icons_rep">chevron_right</i> List of Transactions</td>
                <td>&nbsp;</td>
            </tr>

        </table> <!-- MAIN TABLE ENDS HERE -->
    </div>

    <div></div>

    <div class="container_one">
        <table class="tbl_width_1200" >
            <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_1160">
                                    <tr><td><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></td></tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="FromDate" CssClass="datePickerReport" runat="server" placeholder="From (dd/mm/yyyy)"></asp:TextBox>&nbsp;
                                            <asp:TextBox ID="EndDate" CssClass="datePickerReport" runat="server" placeholder="To (dd/mm/yyyy)"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="rep_dropdown"></asp:DropDownList>&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="btn_green_nomargin" OnClick="Button1_Click" />
                                        </td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:right;"><i class="material-icons_rep">settings</i></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="container_one" id="divCon" runat="server">
     <div id="nbTransactions">
         <table class="tbl_width_1200" >
            <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="tbl_width_1160" style="width:200px;margin-left:2px">
                                    <tr><td></td></tr>
                                    <tr>
                                        <td>
                                           Transaction Count
                                        </td>
                                        <td id="tdNoofTr" runat="server">&nbsp;</td>
                                  
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <strong style="margin-left:20px">A summary report of transactions</strong>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Amount</th>
                                        <th>Country</th>
                                        <th>Currency</th>

                                        </tr>
                                        
                                    </thead>
                                    <tbody id="tbodyRemi" runat="server">

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
              <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <strong style="margin-left:20px">Total Recieved</strong>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Amount</th>
                                        <th>Country</th>
                                        <th>Currency</th>

                                        </tr>
                                        
                                    </thead>
                                    <tbody id="tbody1" runat="server">

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>

               <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <strong style="margin-left:20px">Service Charges</strong>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Amount</th>
                                        <th>Country</th>
                                        <th>Currency</th>

                                        </tr>
                                        
                                    </thead>
                                    <tbody id="tbody2" runat="server">

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
              <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <strong style="margin-left:20px">Total Agent Comission</strong>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Amount</th>
                                        <th>Country</th>
                                        <th>Currency</th>

                                        </tr>
                                        
                                    </thead>
                                    <tbody id="tbody3" runat="server">

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>

              <tr>
                <td class="background_FFFFFF brdr_shadow">
                    <strong style="margin-left:20px">Total Flexwallet Comission</strong>
                    <table class="tbl_width_1160">
                        <tr style="height:20px;"><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Amount</th>
                                        <th>Country</th>
                                        <th>Currency</th>

                                        </tr>
                                        
                                    </thead>
                                    <tbody id="tbody4" runat="server">

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </td>
            </tr>
        </table>
       
     </div>
     
    </div>
        
         
    </form>
</asp:Content>
