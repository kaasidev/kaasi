﻿using KASI_Extend_.classes;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend_.Reports
{
    public partial class ManuallyKYCApprovedCustomersReport : System.Web.UI.Page
    {
           protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                if (Session["AgentID"] == null)
                {
                    DataTable agents = new DataTable();

                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                        try
                        {

                            SqlDataAdapter adapter = new SqlDataAdapter("SELECT AgentName, AgentID FROM dbo.Agents", conn);
                            adapter.Fill(agents);

                            DropDownList1.DataSource = agents;
                            DropDownList1.DataTextField = "AgentName";
                            DropDownList1.DataValueField = "AgentID";
                            DropDownList1.DataBind();
                        }
                        catch (Exception ex)
                        {

                        }
                        conn.Close();
                    }
                    DropDownList1.Items.Insert(0, new ListItem("-- SELECT --", "0"));
                }
                else
                {
                    DropDownList1.Visible = false;
                }
            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text, EndDate.Text, DropDownList1.SelectedValue.ToString());
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "Reports/ManuallyKYCApprovedCustomersReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text),
                new ReportParameter("EndDate", EndDate.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(string fromDate, string toDate, String SelectedAgent)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = null;
                string query = "";
                if (Session["AgentID"] == null)
                {
                    if (DropDownList1.SelectedItem.Value == "0")
                    {
                        query = "SET DATEFORMAT dmy select FullName,C.CustomerID,convert(varchar(10),KYCCheckDate,101) as KYCCheckDate,C.KYCResult,TransactionID,A.RunBy,C.manualKYCPerfomedBy,C.resultDescription,C.manualKYCNotes,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent " +
                                " from Customers as C inner join KYCAudit as A on C.CustomerID = A.CustomerID " +
                                " where A.KYCRunDate between Convert(datetime, '" + fromDate + "') and Convert(date,'" + toDate + "')";
                        cmd = new SqlCommand(query, conn);
                    }
                    else
                    {
                        query = "SET DATEFORMAT dmy select FullName,C.CustomerID,convert(varchar(10),KYCCheckDate,101) as KYCCheckDate,C.KYCResult,TransactionID,A.RunBy,C.manualKYCPerfomedBy,C.resultDescription,C.manualKYCNotes,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent " +
                               " from Customers as C inner join KYCAudit as A on C.CustomerID = A.CustomerID " +
                               " where A.KYCRunDate between Convert(datetime, '" + fromDate + "') and Convert(date,'" + toDate + "') and C.AgentID="+ DropDownList1.SelectedItem.Value;
                        cmd = new SqlCommand(query, conn);
                    }
                }
                else
                {

                    string agentName = new Agent().getAgentNameByID(Session["AgentID"].ToString());
                    query = "SET DATEFORMAT dmy select FullName,C.CustomerID,convert(varchar(10),KYCCheckDate,101) as KYCCheckDate,C.KYCResult,TransactionID,A.RunBy,C.manualKYCPerfomedBy,C.resultDescription,C.manualKYCNotes,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent " +
                                 " from Customers as C inner join KYCAudit as A on C.CustomerID = A.CustomerID " +
                                 " where A.KYCRunDate between Convert(datetime, '" + fromDate + "') and Convert(date,'" + toDate + "') and C.AgentID="+ Session["AgentID"].ToString();
                    cmd = new SqlCommand(query, conn);
                }

                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);


            }

            return dt;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}