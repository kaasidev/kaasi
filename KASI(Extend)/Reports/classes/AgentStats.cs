﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.Reports.classes
{
    public class AgentStats
    {
        public int AgentID { get; set; }
        public String AgentName { get; set; }
        public int TotalTrans { get; set; }
        public float TotalDollarPending { get; set; }
        public float TotalDollarSent { get; set; }

    }
}