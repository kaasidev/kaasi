﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using KASI_Extend_.Reports.classes;
using KASI_Extend_.classes;

namespace KASI_Extend_.Reports.classes
{
    public class ReportAgentStats
    {
        private static List<AgentStats> _lstAgent = null;

        public static List<AgentStats> GetAllAgentStats()
        {
            _lstAgent = new List<AgentStats>();
            KASI_Extend_.classes.Agent agt = new KASI_Extend_.classes.Agent();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT AgentID FROM dbo.Agents WHERE Status = 'Y'";
                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                _lstAgent.Add(new AgentStats()
                                {
                                    AgentID = Int32.Parse(sdr["AgentID"].ToString()),
                                    AgentName = agt.getAgentNameByID(sdr["AgentID"].ToString()),
                                    TotalTrans = Int32.Parse(agt.getAgentNumberOfTransactionsDaily(sdr["AgentID"].ToString())),
                                    TotalDollarPending = 0,
                                    TotalDollarSent = 0
                                });
                            }
                        }
                    }
                }
            }

            return _lstAgent;
        }


    }
}