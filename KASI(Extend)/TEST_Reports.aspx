﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="TEST_Reports.aspx.cs" Inherits="KASI_Extend_.TEST_Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 16px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <form id="form1" runat="server">

    <div class="container_btns">
        <table class="tbl_width_1200"> <!-- MAIN TABLE STARTS HERE -->
            <tr>
                <td class="rep_main_headings">REPORTS</td>
                <td style="text-align:right;"><asp:Button runat="server" CssClass="aa_btn_red" Text="Create Austrac File" ID="btnAustract" OnClick="btnAustract_Click" /></td>
            </tr>

        </table> <!-- MAIN TABLE ENDS HERE -->
    </div>

        <div class="container_one wiz_bg_EFEFEF">
           
             <table class="tbl_width_1200" style="margin-top:30px" >
                <tr>
                       <td class="background_FFFFFF brdr_shadow">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/document_av.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">All Transactions Detail Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button7" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button7_Click" /></td></tr>
                        </table>
                        </td>
                    <td class="background_FFFFFF brdr_shadow">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image11" runat="server" ImageUrl="~/images/document_av.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Cancelled Transactions Detail Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button11" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button11_Click" /></td></tr>
                        </table>
                        </td>
                    <td style="width:20px;">&nbsp;</td>
                     <td class="background_FFFFFF brdr_shadow">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/document_av.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Credit/Debit Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button1" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button1_Click1" /></td></tr>
                        </table>
                        </td>
                 <td style="width:20px;">&nbsp;</td> <!-- SEPERATOR 06 -->
                    
                    <td class="background_FFFFFF brdr_shadow">
                    <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                        </td>
                    <td style="width:20px;">&nbsp;</td>
                     <td class="background_FFFFFF brdr_shadow">
                    <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                        </td>
                    <td style="width:20px;">&nbsp;</td>
                     <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image12" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Customers With Account Balance</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button12" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button12_Click"/></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                    <td style="width:20px;">&nbsp;</td>
                     <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image10" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Customers Without Docs With Trans</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button10" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button10_Click"  /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                    <td style="width:20px;">&nbsp;</td>
                     <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">High Risk Customers With Transactions</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button9" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button9_Click1"  /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                    <td style="width:20px;">&nbsp;</td>
                     <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image113" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Transaction Summary Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button131" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button11_Click1" /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                    <td style="width:20px;">&nbsp;</td>
                      <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Cancelled Transaction Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button2" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button2_Click1" /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                   
                     <td style="width:20px;">&nbsp;</td>
                      <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Compliance Review Transactions Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button4" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button2_Click4"  /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>

                     <td style="width:20px;">&nbsp;</td>
                      <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Manually KYC Approved Customers Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button3" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button3_Click1"  /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                    
                     <td style="width:20px;">&nbsp;</td>
                      <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">KYC Failed Customers Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button5" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button5_Click"  /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                     <td style="width:20px;">&nbsp;</td>
                      <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Transaction Summary Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button6" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button6_Click1"  /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>

                     <td style="width:20px;">&nbsp;</td>
                      <td class="background_FFFFFF">
                    <table style="width:100%;"><tr><td>
                        <table class="tbl_width_160">
                            <tr><td style="height:20px;">&nbsp;</td></tr>
                            <tr><td style=" height:120px; text-align:center;">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/time-is-money.png" />
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td class="rep_description">Monthly Transactions Summary Report</td></tr>
                            <tr><td class="rep_description">&nbsp;</td></tr>
                        </table>
                        </td></tr>
                            <tr><td class="rep_topline rep_select" style="height:35px;"><asp:Button ID="Button8" runat="server" Text="SELECT" CssClass="rep_btn" OnClick="Button8_Click1"  /></td></tr>
                        </table>
                        
                     <table class="tbl_width_160"> 
                         <tr><td>&nbsp;</td></tr>
                     </table>
                     
                     </td>
                </tr>
            </table>
        </div>

    </form>

</asp:Content>
