﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kasi-main.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="KASI_Extend_.DashBoard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/datatables.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.validate.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.mask.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.ui.widget.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.fileupload.js"></script>
    <script src="js/jquery.sessionTimeout.js" type="text/javascript"></script>
    <script src="js/toastr.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jszip.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/buttons.print.min.js"></script>

    <!-- CSS STYLES -->
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />
    <link href="css/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/buttons.dataTables.min.css" />

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Condensed:300,400,500,600,700" rel="stylesheet" />

    <style>
        .error1 {
            border-left: 5px solid rgb(192, 57, 43);
            background-color: #fff3f2;
        }
        .error1_font {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 0.7em !important;
            font-weight: normal;
            line-height: normal;
            color: rgb(192, 57, 43);
        }
        .txn_popup_recamount {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 1.25em;
            font-weight: normal;
            color: #62cb31;
            padding-bottom: 10px;
            padding-left: 15px;
            background-color: #FFFFFF;
            font-weight: 400;
            text-align: left;
        }
    </style>


    <script type="text/javascript">
        function closeWindow()
        {
            alert('This session has timed out. Please login again.');
            window.top.close();
        }
        function showerror(msg) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.error(msg, 'Error');
		   
        }
        function showsuccess(msg, body) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.success(msg, body);
		  
        }
             function loadcustomer() {
            $.ajax({
                url: 'JQDataFetch/getcustomerdetails.aspx?id=<%=Request.QueryString["custid"] %>',

                success: function (res) {
                    var response = $.parseJSON(res);
                 
                    $("#hidCustId").val(response.CustomerID);
                    if (response.IsIndividual == false) {
                        $("#isbusi").val('busi');
                        $("#BBusinessName").val(response.BusinessName);
                        $("#BABN").val(response.ABN);
                        $("#BTradingName").val(response.TradingName);
                        $("#BBContact").val(response.BusinessContact );
                        $("#BBEmail").val(response.BusinessEmail);
                        $("#BBWeb").val(response.BusinessWebSite);
                        $("#BAddressLine1").val(response.AddressLine1);
                        $("#BPostalAddressLine2").val(response.PostalAddressAddressLine2);
                        $("#pos_unitno").val(response.PostalAddressUnitNo);
                        $("#pos_streetno").val(response.PostalAddressStreetNo);
                        $("#pos_streetname").val(response.PostalAddressStreetName);
                        $("#pos_streettype").val(response.PostalAddressStreetType);
                        $("#BPostalSuburb").val(response.PostalAddressSuburb);

                        $("BPostalState").val(response.PostalAddressState);

                        $("BPostalPostcode").val(response.PostalAddressPostcode);
                        $("pos_country").val(response.PostalAddressCountry);

                        $("bus_unitno").val(response.PrincipleUNitNo);
                        $("bus_streetno").val(response.PrincipleStreetNo);
                        $("bus_streetname").val(response.PrincipleStreetName);
                        $("bus_streettype").val(response.PrincipleStreetType);
                        $("bus_suburb").val(response.PrincipleStreetSuburb);
                        $("bus_state").val(response.PrincipleStreetState);

                        $("bus_postcode").val(response.PrincipleStreetPosalCode);
                        $("bus_country").val(response.PrincipleCountry);
                        $("#tblIndi").hide();
                    }
                    else {
                        $("#isbusi").val('indi');
                        $("#TD_EditBusiness_001").hide();
                        $('#TD_EditBusiness_002').removeClass('wiz_tab_inactive_right');
                        $('#TD_EditBusiness_002').addClass('wiz_tab_active');
                        $("#divBusiness").hide();
                        $("#tblIndi").show();
                        
                        
                    }
                    $("#pbus_unitno").val(response.UnitNo);
                    $("#pbus_streetno").val(response.StreetNo);
                    $("#pbus_streetname").val(response.StreetName);
                    $("#pbus_streettype").val(response.StreetType);
                    $("#pbus_addressline2").val(response.AddressLine2);
                    $("#ppbus_suburb").val(response.Suburb);
                    $("#ppbus_state").val(response.State);

                    $("#ppbus_postcode").val(response.Postcode);
                    $("#pbus_country").val(response.Country);
                    $("#bus_lastname").val(response.LastName);
                    $("#bus_title").val(response.Title);
                    $("#bus_givenNames").val(response.FirstName);
                    $("#bus_dob").val(response.DOB);
                    $("#bus_placeofbirth").val(response.PlaceOfBirth);
                    $("#bus_nationality").val(response.Nationality);
                    $("#bus_aka").val(response.AKA);
                    $("#bus_occupation").val(response.Occupation);
                    $("#pbus_phonework").val(response.TelWork);
                    $("#pbus_phonemobile").val(response.Mobile);
                    $("#pbus_phonehome").val(response.TelHome);
                    $("#pbus_email1").val(response.EmailAddress);
                    $("#pbus_email2").val(response.EmailAddress2);
                    $("#edit_notes").val(response.Notes);
                    $("#edit_password").val(response.AccountPassword);
                    if ((response.AccountPassword == "" || response.AccountPassword == null) && (response.Notes == "" || response.Notes == null)) {
                        $("[id$=NotesAreaTR]").hide();
                    }
                    else {
                        if (response.AccountPassword == "" || response.AccountPassword == null)
                        {
                            $("#trpassword").hide();
                        }
                        else {
                            $("#trpassword").show();
                            $("#showpassword").html(response.AccountPassword);

                        }
                        if (response.Notes == "" || response.Notes == null) {
                            $("#trnotes").hide();
                        }
                        else {
                            $("#trnotes").show();
                            $("#shownotes").html(response.Notes);
                        }
                        $("[id$=NotesAreaTR]").show();
                    }
                  
                    $("#bus_countryofbirth option").filter(function () {
                        return this.text == response.CountryOfBirth;
                    }).attr('selected', true);
                    $("#bus_nationality option").filter(function () {
                        return this.text == response.Nationality;
                    }).attr('selected', true);
                    $("#bcountry").val(response.CountryOfBirth);
                    $("#bnationality").val(response.Nationality);
                    
                }
            });
        }

        function openViewTransaction(TID)
        {
            $('#TransIDUpper').text(TID);

            $.ajax({
                url: 'JQDataFetch/getAdminViewTransactionDetails.aspx',
                async: false,
                data: {
                    TID: TID,
                },
                success: function (data) {
                    var splitdata = data.split('|');
                    $("#DollarAmount").text(parseFloat(splitdata[0]).toFixed(2));
                    $("#ServiceFee").text(parseFloat(splitdata[1]).toFixed(2));
                    $("#AUDAmount").text((parseFloat(splitdata[0]) + parseFloat(splitdata[1])).toFixed(2));
                    $("#Rate").text(parseFloat(splitdata[2]).toFixed(2));
                    $("#FAmount").text(parseFloat(splitdata[3]).toFixed(2) + ' ' + splitdata[4]);
                    $("#CustomerID").text(splitdata[5]);
                    $("#CustName").text(splitdata[6]);
                    $("#CustAddress").text(splitdata[7]);
                    $("#CustMobile").text(splitdata[8]);
                    $("#CustEmail").text(splitdata[9]);
                    $("#TransPurpose").text(splitdata[10]);
                    $('#TransSourceOfFunds').text(splitdata[11]);
                    $("#TransNotes").text(splitdata[12]);
                    $('#BeneName').text(splitdata[13]);
                    $('#BeneAddress').text(splitdata[14]);
                    $("#IDDetails").text(splitdata[15]);
                    $("#Relationship").text(splitdata[16]);
                    $("#OriginalBank").text(splitdata[17]);
                    $('#OriginalBankAccount').text(splitdata[18]);
                    $('#AccountName').text(splitdata[19]);
                    $("#BeneAccountID").text(splitdata[20]);
                }
            })

            ViewTransactionDialog.dialog('open');
        }

        function getRateForCountryCurrency()
        {
            var setRate = 0;
            $.ajax({
                url: 'JQDataFetch/getExchangeRateForAgent.aspx',
                async: false,
                data: {
                    AID: $('#<%=txt_hidden_agentID.ClientID%>').val(),
                    AccID: $('#<%=txt_hidden_AccountID.ClientID%>').val()
                },
                success: function(response) {
                    setRate = response;
                }
            })

            return setRate;
        }

        function calculateSendingTotal(amount, exrate)
        {
            var total = parseFloat(amount) * parseFloat(exrate);
            return total;
        }


        function openSendMoney(BID, BankName, BankBranch, CurrencyType)
        {
            

            if($('#<%=hidden_txt_accountbalance.ClientID%>').val() != 0)
            {
                $("#hidden_currencycode").val(CurrencyType);
                $("#divCurrencySending").text(CurrencyType);
                $('#TR_SM_NegativeSend').hide();
                $('#TR_SM_ExceedSend').hide();
                $('#SM_btn_Confirm').hide();
                $('#SM_btn_Cancel').hide();
                var todayrateint = 0;
                $('#sendingBank').val(BankName);
                $('#sendingBankBranch').val(BankBranch);               
                ShowSendMoney.dialog('open');
                $('#SM_btn_Send').show();
                $('#SM_btn_Confirm').attr("disabled", false);
                $('#SM_servicecharge').attr("readonly", true);
                $('#SM_exchangerate').attr("readonly", true);
                var AccBalance = $('#<%=hidden_txt_accountbalance.ClientID%>').val()
                $('#SPN_AddTR_AvailableFunds').text('$' + parseFloat(AccBalance, 10).toFixed(2));
                //$('#SM_availablefunds').val($('#<%=hidden_txt_accountbalance.ClientID%>').val());

                var amtServiceCharge = calculateServiceCharge($('#<%=hidden_txt_accountbalance.ClientID%>').val());
                $('#SM_servicecharge').val(parseFloat(amtServiceCharge).toFixed(2));

                var amtToSend = parseFloat(AccBalance) - parseFloat($('#SM_servicecharge').val());
                $('#SM_amounttobesent').val(parseFloat(amtToSend).toFixed(2));
                $('#SM_div_sending').text("$ " + parseFloat(AccBalance).toFixed(2));

                var disExRate = getRateForCountryCurrency();
                $('#SM_exchangerate').val(parseFloat(disExRate).toFixed(2));
                $('#SM_div_todayrate').text(parseFloat(disExRate).toFixed(2));

                var totalSend = calculateSendingTotal(amtToSend, disExRate);
                $('#SM_div_lkrvalue').text(parseFloat(totalSend).toFixed(2));
                $('#txt_hidden_foreignamount').val(parseFloat(totalSend).toFixed(2));

                $('#hidden_enoughfunds').val("Y");
				
                return false;
            }
            else
            {
                alert('Please reload customer credit before proceeding with transaction');
            }    
        }

        function updateIDPicture()
        {
            $('#<%=Image1.ClientID%>').attr("src", "images/id-card.png");
            $('#<%=Image1.ClientID%>').attr("title", "Customer Identification Completed");
            $('#<%=hidden_hasID.ClientID%>').val('Y');
            $('#<%=IMD_HasKYC.ClientID%>').css('cursor', 'hand');
        }

        function onCompletionReload()
        {
            location.reload();
        }

        function onCompletionReloadDocTable()
        {
            FileMainTable.ajax.reload(null, false);
        }

        function highlightRiskLevel()
        {
            $.ajax({
                url: 'JQDataFetch/getCustomerRiskLevel.aspx',
                data: {
                    CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                },
                success: function(data) {
                    $('#<%=hidden_risklevel.ClientID%>').val(data);
                    if (data == 1)
                    {
                        $('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
                        $('#<%=td_risk2.ClientID%>').attr("class", "risk_neutral");
                        $('#<%=td_risk3.ClientID%>').attr("class", "risk_neutral");
                        $('#<%=td_risk4.ClientID%>').attr("class", "risk_neutral");
                    }
                    else if (data == 2)
                    {
                        $('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
                        $('#<%=td_risk2.ClientID%>').attr("class", "risk_2");
                        $('#<%=td_risk3.ClientID%>').attr("class", "risk_neutral");
                        $('#<%=td_risk4.ClientID%>').attr("class", "risk_neutral");
                    }
                    else if (data == 3)
                    {
                        $('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
                        $('#<%=td_risk2.ClientID%>').attr("class", "risk_2");
                        $('#<%=td_risk3.ClientID%>').attr("class", "risk_3");
                        $('#<%=td_risk4.ClientID%>').attr("class", "risk_neutral");
                    }
                    else
                    {
                        $('#<%=td_risk1.ClientID%>').attr("class", "risk_1");
                        $('#<%=td_risk2.ClientID%>').attr("class", "risk_2");
                        $('#<%=td_risk3.ClientID%>').attr("class", "risk_3");
                        $('#<%=td_risk4.ClientID%>').attr("class", "risk_4");
                    }
                }
            })
        }

        function populateBeneficaryDetails(BID) {
            $('#DIV_BeneficiaryDetails').show();
            $('#tr_ben_info').show();
            $('#<%=hidden_selectedBeneficiary.ClientID%>').val(BID);
            $.ajax({
                url: 'JQDataFetch/getBeneficiaryDetails.aspx',
                async: false,
                data: {
                    BeneID: BID,
                },
                beforeSend: function () {
                    LoaderWait.dialog('open')
                },
                complete: function () {
                    LoaderWait.dialog('close')
                },
                success: function (data) {
                    var splitdata = data.split('|');
                    $('#div_BeneficiaryName').text(splitdata[18]);
                    var fulladdress = splitdata[3] + " " + splitdata[4] + " " + splitdata[5] + " " + splitdata[6] + " " + splitdata[7];
                    $('#div_BeneficiaryAddress').text(fulladdress);
                   // $('#div_BeneficiaryTelHome').text(splitdata[8]);
                    //$('#div_BeneficiaryTelWork').text(splitdata[9]);
                    //$('#div_ContactNo').text(splitdata[10]);
                    $('#div_EmailAddress').text(splitdata[10]);
                    $('#div_BeneficiaryNationalityCardID').text(splitdata[11]);
                    //$('#div_BeneficiaryPassportID').text(splitdata[13]);
                    $('#div_BeneficiaryNotes').text(splitdata[14]);
                    $('#div_Relationship').text(splitdata[12]);
                    $("#div_AKA").text(splitdata[17]);
                    $("#div_DOB").text(splitdata[14].substring(0,10));
                    $("#div_PlaceOfBirth").text(splitdata[15]);
                    $("#div_CountryOfBirth").text(splitdata[13]);
                    $("#div_Nationality").text(splitdata[16]);
                    $('#div_ContactNo').text(splitdata[9]);

                    $('#edit_bene_fullname').val(splitdata[1]);
                    $('#edit_bene_addressline1').val(splitdata[2]);
                    $('#edit_bene_addressline2').val(splitdata[3]);
                    $('#edit_bene_suburb').val(splitdata[4]);
                    $('#edit_bene_state').val(splitdata[5]);
                    $('#edit_bene_postcode').val(splitdata[6]);
                    $('#edit_bene_country').val(splitdata[7]);
                    $('#edit_bene_homephone').val(splitdata[8]);
                    $('#edit_bene_workphone').val(splitdata[9]);
                    $('#edit_bene_mobile').val(splitdata[10]);
                    $('#edit_bene_email').val(splitdata[11]);
                    $('#edit_bene_nationalid').val(splitdata[12]);
                    $('#edit_bene_relationship').val(splitdata[15]);

                    loadBeneficiaryAccounts(BID);
                    //disableOverCount(BID);
                    disableHomeDelivery(BID);

                }
            });
        }

        function disableHomeDelivery(BID)
        {
            $.ajax({
                url: 'JQDataFetch/checkBeneficiaryForHomeDelivery.aspx',
                data: {
                    BeneID: BID,
                },
                success: function(response)
                {
                    if (response == "NO")
                    {
                        $('#btn_HomeDelivery').removeClass("btn_blue");
                        $('#btn_HomeDelivery').addClass("btn_disabled");
                        $('#btn_HomeDelivery').prop('onclick', null).off('click');
                    }
                    else
                    {

                    }
                }
            });
        }

        function disableOverCount(BID)
        {
            $.ajax({
                url: 'JQDataFetch/checkBeneficiaryHasID.aspx',
                data: {
                    BeneID: BID,
                },
                success: function(response) {
                    if (response == "NO")
                    {
                        $('#btn_OverTheCounter').removeClass("btn_blue");
                        $('#btn_OverTheCounter').addClass("btn_disabled");
                        $('#btn_OverTheCounter').prop('onclick', null).off('click');
                    }
                    else (response == "YES")
                    {
                        //$('#btn_OverTheCounter').removeClass("btn_disabled");
                        //$('#btn_OverTheCounter').addClass("btn_blue");
                        //$('#btn_OverTheCounter').prop('onclick', null).off('click');
                    }
                    
                }
            });

        }

        function openMsgDIV(CurrentDialogName, Message, ClickAction)
        {
            $('#msgtext').text(Message);
            if (ClickAction == '1')
            {
                $('#ok_button_dialog').attr('onClick', 'onCompletionReload()');
            }
            else if (ClickAction = '2')
            {
                $('#ok_button_dialog').attr('onClick', 'onCompletionReloadDocTable()');
            }
            MsgDIV.dialog('open');
        }

        function loadBeneficiaryAccounts(BID)
        {
            $.ajax({
                url: 'JQDataFetch/getBeneficiaryAccounts.aspx',
                data: {
                    BeneID: BID,
                },
                success: function(response) {
                    $('#BeneAccountTable > tbody').empty();
                    if (response != 'NORESULTS')
                    {
                        var maindata = response.split('~');
                        $.each(maindata, function(index, item)
                        {
                            var spdata = maindata[index].split('|');
                            $('#<%=txt_hidden_AccountID.ClientID%>').val(spdata[0]);
                             
                            $('#BeneAccountTable').append('<tr><td class="tbl_contents_ben_fnt_01" style="background-color:#FFFFFF;">' + spdata[1] + '</td><td class="tbl_contents_ben_fnt_02" style="background-color:#FFFFFF;">' + spdata[2] + '</td><td class="tbl_contents_ben_fnt_02" style="background-color:#FFFFFF;">' + spdata[3] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:40px;">' + spdata[4] + '</td><td class="tbl_contents_ben_fnt_03" style="background-color:#FFFFFF; width:40px;">' + spdata[5] + '</td></tr>');
                        });
                    }
                    
                    
                }
            });
        }

        function openBeneficiaryAccountEdit(BankName, BranchName, AccountNumber, AccountID)
        {
            ShowEditAccount.dialog('open');

            $('#hidden_AccountID').val(AccountID);

            $.ajax({
                url: 'JQDataFetch/getAllBanks.aspx',
                async: false,
                success: function(data) {
                    var Banks = data.split('~');
                    Banks.sort();
                    $.each(Banks, function(item) {
                        var datasplit = Banks[item].split('|');
                        $('#ddl_Banks').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                    });

                }   
            });
           
            var text1 = BankName;
            $('#ddl_Banks option').filter(function() {
                return this.text == text1;
            }).attr('selected', true);
            
            
                $.ajax({
                    url: 'JQDataFetch/getAllBankBranches.aspx',
                    async: false,
                    data: {
                        BID: $('#ddl_Banks').val(),
                    },
                    success: function(data) {
                        var Branches = data.split('~');
                        Branches.sort();
                        $('#Edit_ddl_Branches').empty();
                        $.each(Branches, function(item) {
                            var datasplit = Branches[item].split('|');
                           
                            $('#Edit_ddl_Branches').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                        });
                    }
                });

                var text2 = BranchName;
                $('#Edit_ddl_Branches option').filter(function() {
                    return this.text == text2;
                }).attr('selected', true);
                
                $('#Edit_AccountNumber').val(AccountNumber);
        }

        function calculateAmountToBeSentOtherThanLKR(amt, CT, rate)
        {
            
            var avfund = amt;
            var therate = rate;
            var amtsent = (avfund * therate) - 25;
            return amtsent;
            
        }

        function calculateAmountToBeSent(amt, CT)
        {
            var avfund = amt;
            if (CT == 'LKR')
            {
                if (avfund <= 1010)
                {
                    var amtsent = avfund - 10;
                    return amtsent;
                }
                else
                {
                    var amtsent = avfund - 15;
                    return amtsent;
                }
            }
            else 
            {
                var amtsent = avfund - 25;
                return amtsent;
            }
            
        }

        function calculateServiceCharge(amount)
        {
            if (amount <= 1000)
            {
                servicecharge = 10;
                return servicecharge;
            }
            else
            {
                servicecharge = 15;
                return servicecharge;
            }

        }

        function updateWarningPanel(CID)
        {

        }
        


        $(document).ready(function () {
            $(".datePickerReport").datepicker({ dateFormat: 'dd/mm/yy' });

            $(".datePickerMonthReport").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true
            });

            $('#DIV_DocumentsOther').hide();
            $('#DIV_EditCustomerNotesPW').hide();
            //$('#TD_EditCustomer_001').click(function () {
            //    $('#DIV_EditCustomerDetails').show();
            //    $('#DIV_EditCustomerNotesPW').hide();
            //    $('#TD_EditCustomer_001').removeClass();
            //    $('#TD_EditCustomer_001').addClass("wiz_tab_active");
            //    $('#TD_EditCustomer_002').removeClass();
            //    $('#TD_EditCustomer_002').addClass("wiz_tab_inactive_right");
            //})
            //$('#TD_EditCustomer_002').click(function () {
            //    $('#DIV_EditCustomerDetails').hide();
            //    $('#DIV_EditCustomerNotesPW').show();
            //    $('#TD_EditCustomer_001').removeClass();
            //    $('#TD_EditCustomer_001').addClass("wiz_tab_inactive_left");
            //    $('#TD_EditCustomer_002').removeClass();
            //    $('#TD_EditCustomer_002').addClass("wiz_tab_active");
            //})
            $('#TD_EditCustomer_003').click(function () {
                $('#DIV_EditCustomerDetails').hide();
                $('#DIV_EditCustomerNotesPW').show();
                $('#TD_EditBusiness_001').removeClass();
                $('#TD_EditBusiness_001').addClass("wiz_tab_inactive_right");
                $('#TD_EditBusiness_002').removeClass();
                $('#TD_EditBusiness_002').addClass("wiz_tab_inactive_left");
                $('#TD_EditCustomer_003').removeClass();
                $('#TD_EditCustomer_003').addClass("wiz_tab_active");
            });
            $('#TD_EditBusiness_002').click(function () {
                $("#divBusiness").hide();
                $("#tblIndi").show();
                $('#DIV_EditCustomerDetails').show();
                $('#DIV_EditCustomerNotesPW').hide();
                $('#TD_EditBusiness_001').removeClass();
                $('#TD_EditBusiness_001').addClass("wiz_tab_inactive_right");
                $('#TD_EditCustomer_003').removeClass();
                $('#TD_EditCustomer_003').addClass("wiz_tab_inactive_right");
                $('#TD_EditBusiness_002').removeClass();
                $('#TD_EditBusiness_002').addClass("wiz_tab_active");
            });
            $('#TD_EditBusiness_001').click(function () {
                $('#DIV_EditCustomerDetails').show();
                $('#DIV_EditCustomerNotesPW').hide();
                $("#divBusiness").show();
                $("#tblIndi").hide();
                $('#TD_EditBusiness_001').removeClass();
                $('#TD_EditBusiness_001').addClass("wiz_tab_active");
                $('#TD_EditCustomer_003').removeClass();
                $('#TD_EditCustomer_003').addClass("wiz_tab_inactive_right");
                $('#TD_EditBusiness_002').removeClass();
                $('#TD_EditBusiness_002').addClass("wiz_tab_inactive_right");
            });



            $('#DIV_TRN_Add_Error_Funds').hide();
            $('#DIV_TRN_Errors').hide();
            $('#DIV_TRN_Add_Error_Purpose').hide();
            $('#DIV_TRN_Add_Error_Source').hide();
            $('#TR_PromoCode').hide();
            $('#DIV_002').hide();
            $('#DIV_003').hide();
            $('#DIV_004').hide();
            $('#TD_001').removeClass();
            $('#TD_001').addClass("dsh_tab_active");

            $.sessionTimeout({
                warnAfter: 900000,
                redirAfter: 1200000,
                logoutUrl: 'Login.aspx',
                redirUrl: 'login.aspx',
                keepAliveUrl: 'DataHandlers/KeepAlive.ashx',


            });

            // TABS
            $('#TD_001').click(function () {
                $('#DIV_001').show();
                $('#DIV_002').hide();
                $('#DIV_003').hide();
                $('#DIV_004').hide();
                $('#TD_001').removeClass();
                $('#TD_001').addClass("dsh_tab_active");
                $('#TD_002').removeClass();
                $('#TD_002').addClass("dsh_tab_inactive");
                $('#TD_003').removeClass();
                $('#TD_003').addClass("dsh_tab_inactive");
                $('#TD_004').removeClass();
                $('#TD_004').addClass("dsh_tab_inactive");
            });

            $('#TD_002').click(function () {
                $('#DIV_001').hide();
                $('#DIV_002').show();
                $('#DIV_003').hide();
                $('#DIV_004').hide();
                $('#TD_001').removeClass();
                $('#TD_001').addClass("dsh_tab_inactive");
                $('#TD_002').removeClass();
                $('#TD_002').addClass("dsh_tab_active");
                $('#TD_003').removeClass();
                $('#TD_003').addClass("dsh_tab_inactive");
                $('#TD_004').removeClass();
                $('#TD_004').addClass("dsh_tab_inactive");
            });

            $('#TD_003').click(function () {
                $('#DIV_001').hide();
                $('#DIV_002').hide();
                $('#DIV_003').show();
                $('#DIV_004').hide();
                $('#TD_001').removeClass();
                $('#TD_001').addClass("dsh_tab_inactive");
                $('#TD_002').removeClass();
                $('#TD_002').addClass("dsh_tab_inactive");
                $('#TD_003').removeClass();
                $('#TD_003').addClass("dsh_tab_active");
                $('#TD_004').removeClass();
                $('#TD_004').addClass("dsh_tab_inactive");
            });

            $('#TD_004').click(function () {
                $('#DIV_001').hide();
                $('#DIV_002').hide();
                $('#DIV_003').hide();
                $('#DIV_004').show();
                $('#TD_001').removeClass();
                $('#TD_001').addClass("dsh_tab_inactive");
                $('#TD_002').removeClass();
                $('#TD_002').addClass("dsh_tab_inactive");
                $('#TD_003').removeClass();
                $('#TD_003').addClass("dsh_tab_inactive");
                $('#TD_004').removeClass();
                $('#TD_004').addClass("dsh_tab_active");
            });

            loadcustomer();
            $("#bus_countryofbirth").change(function () {
                $("#bcountry").val($("#bus_countryofbirth :selected").text());
            });
            $("#bus_nationality").change(function () {
                $("#bnationality").val($("#bus_nationality :selected").text());
            });
            $('#ppbus_state').mask('AAA');
            $('#ppbus_state').css('text-transform', 'uppercase');
            $('#bus_state').mask('AAA');
            $('#bus_state').css('text-transform', 'uppercase');
            $('#BPostalState').mask('AAA');
            $('#BPostalState').css('text-transform', 'uppercase');
            $('#BPostalPostcode').mask('0000');
            $('#ppbus_postcode').mask('0000');

            $('#bus_postcode').mask('0000');

            $('#pbus_phonemobile').mask('0000 000 000');
            $('#pbus_phonework').mask('(00) 0000 0000');
            $('#pbus_phonehome').mask('(00) 0000 0000');
            $('#expirydatetxt').mask('99/99/9999');
            $('#overide_total').hide();

            var todayrate = 0;

            $('#UploadFile2').fileupload({
                url: 'DataHandlers/UploadOtherFiles.ashx?upload=start',
                autoUpload: false,
                replaceFileInput: false,
                add: function(e, data) {
                    $('#btn_UploadOtherFiles').off('click').on('click', function () {
                        if ($('#docdescription').val() == "")
                        {
                            alert('Please provide a description to upload this file');
                        }
                        else
                        {
                            data.submit();
                        }
                        
                    });
                },
                success: function(response,status) {
                    alert('File Uploaded Successfully');
                    //openMsgDIV('None', 'File has been successfully uploaded', '2');
                    FileOtherTable.ajax.reload(null, false);
                    $('#docdescription').val('');
                }
            });

            $('#UploadFile2').bind('fileuploadsubmit', function(e, data) {
                data.formData = {
                    CustID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    DocDesc: $('#docdescription').val(),
                };
                if (!data.formData.CustID) {
                    return false;
                }
            });

            $('#UploadFile1').fileupload({
                url: 'DataHandlers/FileUploadHandler.ashx?upload=start',
                autoUpload: false,
                replaceFileInput:false,
                add: function(e, data) {
                    $('#btn_UploadFile').off('click').on('click', function () {

                        var ccc = 1024 * 1024 * 4;

                        if ((document.getElementById("UploadFile1").files[0].size) > ccc) {
                            alert("Maximum file size is 4MB.");
                            return false;
                        }


                        if ($('#documentnumbertext').val() != "" && $('#issuingauthoritytxt').val() != "" && $('#expirydatetxt').val() != "" && $('#typeofdocumentddl :selected').val != "")
                        {
                            $('#documentnumbertxt').removeClass('err_redborder');
                            $('#issuingauthoritytxt').removeClass('err_redborder');
                            $('#expirydatetxt').removeClass('err_redborder');
                            $('#typeofdocumentddl').removeClass('err_redborder');
                            data.submit();
                        }
                        else
                        {
                            if ($('#documentnumbertxt').val() == "") {
                                $('#documentnumbertxt').addClass('err_redborder');
                            }
                            else
                            {
                                $('#documentnumbertxt').removeClass('err_redborder');
                            }

                            if ($('#issuingauthoritytxt').val() == "") {
                                $('#issuingauthoritytxt').addClass('err_redborder');
                            }
                            else
                            {
                                $('#issuingauthoritytxt').removeClass('err_redborder');
                            }

                            if($('#expirydatetxt').val() == "") {
                                $('#expirydatetxt').addClass('err_redborder');
                            }
                            else
                            {
                                $('#expirydatetxt').removeClass('err_redborder');
                            }

                            if($('#typeofdocumentddl :selected').val() == "")
                            {
                                $('#typeofdocumentddl').addClass('err_redborder');
                            }
                            else
                            {
                                $('#typeofdocumentddl').removeClass('err_redborder');
                            }
                            alert('Please make sure that you have entered all the required information for the document.')
                        }
                        
                    });
                    
                },
                success: function(reponse,status) {
                    alert('File Uploaded Successfully');
                    FileMainTable.ajax.reload(null, false);
                    //openMsgDIV('None', 'File Uploaded Successfully', '2');
                    highlightRiskLevel();   
                    updateIDPicture();
                    $('#typeofdocumentddl').find('option:first').attr('selected', 'selected');
                    $('#UploadFile1').val('');
                    $('#selectedfilename').text('');
                    $('#documentnumbertxt').val('');
                    $('#issuingauthoritytxt').val('');
                    $('#expirydatetxt').val('');
                },
                error: function(error) {
                    alert(error);
                },
            });

            $('#UploadFile1').bind('fileuploadsubmit', function(e, data) {
                data.formData = {
                    CustID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    DocNum: $('#documentnumbertxt').val(),
                    IssueAuth: $('#issuingauthoritytxt').val(),
                    ExpDate: $('#expirydatetxt').val(),
                    HDNPts: $('#hdnpoints').val(),
                    TypeOfDoc: $('#typeofdocumentddl :selected').text(),
                    TODID: $('#typeofdocumentddl').val(),
                };
                if (!data.formData.CustID) {
                    return false;
                }
            });

            $('#txt_AdminCode').focusout(function(){
                $.ajax({
                    url: 'JQDataFetch/verifyAdminOverrideCode.aspx',
                    data: {
                        UID: $('#hidden_LoggedUserID').val(),
                        ORCode: $('#txt_AdminCode').val(),
                    },
                    success: function(response) {
                        if (response == 'Y')
                        {
                            alert('Admin Override Confirmed');
                            $('#SM_btn_Confirm').attr("disabled", false);
                            $('#hidden_goingStatus').val('PENDING');
                            $('#hidden_adminoverride').val('Y');
                        }
                        else
                        {
                            alert('Incorrect Admin Override. Try again or escalate to compliance team.')
                            $('#hidden_goingStatus').val('REVIEW');
                        }
                    }
                })
            });

            
            

            $('#SM_exchangerate').change(function() {
                var newtotal = $('#SM_amounttobesent').val() * $('#SM_exchangerate').val();
                $('#SM_div_lkrvalue').text(newtotal);
                $('#SM_div_todayrate').text($('#SM_exchangerate').val());
                $('#txt_hidden_Rate').val($('#SM_exchangerate').val());
            });

            $('#btn_verifyhnb').hide();

            $('#ddl_Banks').change(function() {

                $.ajax({
                    url: 'JQDataFetch/getAllBankBranches.aspx',
                    data: {
                        BID: $('#ddl_Banks').val(),
                    },
                    success: function(data) {
                        var Branches = data.split('~');
                        Branches.sort();
                        $('#Edit_ddl_Branches').empty();
                        $.each(Branches, function(item) {
                            var datasplit = Branches[item].split('|');
                           
                            $('#Edit_ddl_Branches').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                        });
                    }
                });
            });

          <%--  $('#<%=dob.ClientID%>').mask('00/00/0000');
            $('#<%=state.ClientID%>').mask('AAA');
            $('#<%=postcode.ClientID%>').mask('0000');
            $('#<%=phonehome.ClientID%>').mask('(00) 0000 0000');
            $('#<%=phonework.ClientID%>').mask('(00) 0000 0000');
            $('#<%=phonemobile.ClientID%>').mask('0000 000 000');--%>

            $('#edit_bene_homephone').mask('000 0 000000');
            $('#edit_bene_workphone').mask('000 0 000000');
            $('#edit_bene_mobile').mask('000 0000000');

            $('#DIV_BeneficiaryDetails').hide();
            $('#tr_ben_info').hide();

            LoaderWait = $('#LoadingDiv').dialog({
                autoOpen: false,
                modal: true,
                width: 710,
            });

            ModifyKYC = $('#DIV_ModifyKYC').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
            });

            MsgDIV = $('#DIV_Message_OK').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
            });

            ViewTransactionDialog = $("#ViewTransactionDIV").dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'View Transaction Details',
            });

            DocumentsDialog = $('#DIV_Documents').dialog({
                autoOpen: false,
                modal: true,
                width: 640,
            });

            $('#<%=ddl_List_Beneficiaries.ClientID%>').change(function() {
                
                if ($('#<%=ddl_List_Beneficiaries.ClientID%> option:selected').val() == 'NOSELECT')
                {
                    $('#tr_ben_info').hide();
                }
                else
                {
                    populateBeneficaryDetails($('#<%=ddl_List_Beneficiaries.ClientID%>').val());
                }
                
            });

            


            //populateBeneficaryDetails($('#<%=txt_hidden_FirstBeneficiaryID.ClientID%>').val());

            $('#SM_btn_Send').click(function() {
                $('#DIV_TRN_Errors').hide();
                $('#DIV_TRN_Add_Error_Purpose').hide();
                $('#DIV_TRN_Add_Error_Source').hide();
                $("#DIV_TRN_Add_Error_Funds").hide();

				

                if ($('#SM_Purpose :selected').val() != "NOSELECT" && $('#SM_SourceOfFunds :selected').val() != "NOSELECT" && $('#hidden_enoughfunds').val() == "Y")
                {
                    var SPower = parseFloat($('#<%=hidden_SpendingPower.ClientID%>').val());
                    var AlreadySentThisMonth = parseFloat($("#<%=DIV_MonthlyRemittanceTotal.ClientID%>").text().replace('$', ''));
                    var CalcTotalForThisMonth = AlreadySentThisMonth + parseFloat($('#SM_amounttobesent').val());
                    $("#hidden_goingStatus").val('PENDING');
                    

                    if (CalcTotalForThisMonth > SPower)
                    {
                        $("#TR_SM_ExceedSend").show();
                        $("#hidden_goingStatus").val('REVIEW');
                    }

                    $('#SM_btn_Confirm').show();
                    $('#SM_btn_Cancel').show();
                    $('#SM_btn_Send').hide();
                    $('#SM_amounttobesent').attr("readonly", true);
                    $('#SM_servicecharge').attr("readonly", true); 

                    var availfunds = parseFloat($('#<%=hidden_txt_accountbalance.ClientID%>').val());
                    var sendfunds = parseFloat($('#SM_amounttobesent').val());
                    var sendcharges = parseFloat($('#SM_servicecharge').val());
				
                    var hassent = $('#<%=hidden_CurrentSentAmount.ClientID%>').val();
                    var totalsending = parseFloat(hassent) + parseFloat(sendfunds);
                    var kycdaysleft = $('#<%=hidden_KYCExpiryDays.ClientID%>').val();

                    var totalmthtrans = $('#<%=hidden_NbTrans.ClientID%>').val();
                    var thisTransSend = parseInt(totalmthtrans) + 1;

                    var hKYC = $('#<%=hidden_hasKYC.ClientID%>').val();
                    var RiskLevelTest = $('#<%=hidden_risklevel.ClientID%>').val();

                    var totalsend = sendfunds + sendcharges;

                }
                else
                {
					
                    if ($('#SM_Purpose :selected').val() == "NOSELECT")
                    {
                        $('#DIV_TRN_Errors').show();
                        $('#DIV_TRN_Add_Error_Purpose').show();
                    }

                    if ($('#SM_SourceOfFunds :selected').val() == "NOSELECT")
                    {
                        $('#DIV_TRN_Errors').show();
                        $('#DIV_TRN_Add_Error_Source').show();
                    }

                    if ($('#hidden_enoughfunds').val() == "N")
                    {
                        $('#DIV_TRN_Errors').show();
                        $("#DIV_TRN_Add_Error_Funds").show();
                    }
                }
                
            });

            $('#<%=IMD_HasKYC.ClientID%>').click(function() {
                if ($('#<%=hidden_hasID.ClientID%>').val() == 'Y')
                {
                    $('#CHK_ConfirmInfo').attr('checked', false);
                    $('#btn_kyc_add_Submit').removeClass('btn_red');
                    $('#btn_kyc_add_Submit').addClass('btn_disabled');
                    $('#btn_kyc_add_Submit').attr('disabled', true);
                    $.ajax({
                        url: 'JQDataFetch/getCustomerKYC.aspx',
                        data: {
                            CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                        },
                        success: function(data) {
                            if (data != "NOKYC")
                            {
                                var splitdata = data.split('|');
                                $('#maritalstatus').val(splitdata[0]);
                                $('#findep').val(splitdata[1]);
                                $('#AURes').val(splitdata[2]);
                                $('#ResStat').val(splitdata[3]);
                                $('#EmpStat').val(splitdata[4]);
                                $('#occupation').val(splitdata[5]);
                                $('#salary').val(splitdata[6]);
                                $('#frequency').val(splitdata[7]);
                                $('#otherincome').val(splitdata[8]);
                                $('#comments').val(splitdata[9]);

                                $.ajax({
                                    url: 'JQDataFetch/getKYCInfo.aspx',
                                    data: {
                                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                                    },
                                    success: function(response) {
                                        var resp = response.split('|');
                                        $('#TD_ExpInfo').text('Last updated by ' + resp[0] + ' on ' + resp[1] + '. Expiry Date: ' + resp[2]);
                                    }
                                })
                            }

                        
                        },
                    });
                    ShowKYC.dialog('open');
                }
                
            });

            $('#UploadFile1').change(function() {
                var filename = $(this).val().split('\\').pop();
                $('#selectedfilename').text(filename);
            });

            $('#UploadFile2').change(function() {
                var filename = $(this).val().split('\\').pop();
                $('#otherdoctext').text(filename);
            });

            $("#<%=btn_OverrideKYC.ClientID%>").click(function () {
                ModifyKYC.dialog('open');
            })

            $('#SM_btn_Confirm').click(function() {
                sendingTotal = $('#SM_div_lkrvalue').text();
                $.ajax({
                    url: 'Processor/AddTransaction.aspx',
                    beforeSend: function() {
                        ShowSendMoney.dialog('close');
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                        CLN: $('#AddBene_lastname').val(),
                        BID: $('#<%=hidden_selectedBeneficiary.ClientID%>').val(),
                        BN: $('#div_BeneficiaryName').text(),
                        AID: $('#<%=txt_hidden_AccountID.ClientID%>').val(),
                        Rate: $('#SM_exchangerate').val(),
                        DM: $('#SM_amounttobesent').val(),
                        SC: $('#SM_servicecharge').val(),
                        RA: sendingTotal,
                        PT: 'N',
                        CUR: $("#hidden_currencycode").val(),
                        ST: 'PENDING',
                        AB: $('#txt_remainingaccountbalance').val(),
                        Bank: $('#sendingBank').val(),
                        Branch: $('#sendingBankBranch').val(),
                        Purpose: $('#SM_Purpose').val(),
                        Source: $('#SM_SourceOfFunds').val(),
                        CR: $('#hidden_compliancereason').val(),
                        Notes: $('#TXT_Add_TRN_Notes').val(),
                    },
                    complete: function() {
                        LoadingDIV.dialog('close');
                    },
                    success: function(response) {
                        var datasplit = response.split('|');
                        showsuccess('', datasplit[0]);
                        setTimeout(function () {
                            $.ajax({
                                url: 'FillPDF.aspx',
                                aynsc: false,
                                data: {
                                    TID: datasplit[1],
                                },
                                success: function (data) {
                                    //alert('PDF File now available');
                                },

                            });
                            location.reload();
                            ShowSendMoney.dialog('close');
                        }, 1000);
					 
                    }
                });
               
            });

            $('#SM_btn_Cancel').click(function() {
                $('#TR_SM_NegativeSend').hide();
                $('#SM_btn_Confirm').hide();
                $('#SM_btn_Cancel').hide();
                $('#SM_btn_Send').show();
                $('#SM_amounttobesent').attr("readonly", false);
                $('#SM_servicecharge').attr("readonly", false);
            });

            $('#SM_btn_overidetotal').click(function() {
                $('#overide_total').show();
                $('#overide_total').val($('#SM_div_lkrvalue').text());
            });

           
            table = $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: 'JQDataFetch/getCustomerPastRecords.aspx',
                    data: {
                        custid: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    }
                },
               "columnDefs": [
                   { className: "align__left", "targets": [0, 1, 2, 8] },
                   { className: "align__right", "targets": [3, 4, 5, 6, 7, 9] },
               ],
               "order": [[ 0, "desc" ]]
            });

            var FileMainTable = $('#FileUploadMainTbl').DataTable({
                ajax: {
                    url: 'JQDataFetch/getCustomerMainFiles.aspx',
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    }
                }
            });

            var FileOtherTable = $('#FileUploadOtherTbl').DataTable({
                ajax: {
                    url: 'JQDataFetch/getCustomerOtherFiles.aspx',
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    }
                }
            });

            $('#DebitCreditTbl').DataTable({
                ajax: {
                    url: 'JQDataFetch/getCustomerDebitsCredits.aspx',
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    }
                },
                "order": [[0, "asc"]],
            });

            EditCustomerDialog = $('#EditCustomerDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "Customer Details",
            });

            AddBeneficiaryDialog = $('#AddBeneficiaryDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "Add Beneficiary",
            });

            EditBeneficiaryDialog = $('#EditBeneficiaryDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Edit Beneficiary',
            });

            AddCreditDialog = $('#AddCreditDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Add Credit',
            })

            ShowDebitCreditDialog = $('#ShowCreditDebitsDIV').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'View Account',
            });

            ShowOverTheCounter = $('#OverTheCounter').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Over The Counter Transaction',
            });
            

            ShowEditAccount = $('#EditAccount').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Edit Beneficiary Account',
            });

            ShowAddNewAccount = $("#AddNewAccount").dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'ADD BENEFICIARY ACCOUNT',
            });

            ShowHomeDelivery = $('#HomeDelivery').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Home Delivery',
            });

            ShowCompanyVerified = $('#DIV_Modal_CompanyVerified').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
            });

            ShowCompanyVerified.siblings('.ui-dialog-titlebar').remove();

            $('#OpenModalCVerified').click(function() {
                ShowCompanyVerified.dialog('open');
            });

            ShowSendMoney = $('#SendMoneyDIV').dialog({
                modal: true,
                autoOpen: false,
                width: 480,
                title: 'TRANSFER MONEY',
            });

            ShowKYC = $('#DIV_Modal_KYC').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: 'Know Your Customer (KYC) Additional Information',
            });

            LoadingDIV = $('#DIV_Loading').dialog({
                modal: true,
                autoOpen: false,
                width: 120,
                dialogClass: 'dialog_transparent_background',
            });

            LoadingDIV.siblings('.ui-dialog-titlebar').remove();

            $('#btn_AddKYC').click(function() {
               
            });

            $('#BTN_DocumentsClick').click(function() {

                $('#documentnumbertxt').val('');
                $('#issuingauthoritytxt').val('');
                $('#expirydatetxt').val('');
                $('#hdnpoints').val('');


                $.ajax({
                    url: 'JQDataFetch/getDocumentTypes.aspx',
                    async: false,
                    success: function(data) {
                        $('#typeofdocumentddl').empty();
                        $('#typeofdocumentddl').append('<option value="">-- SELECT -- </option>');
                        var Docs = data.split('~');
                        Docs.sort();
                        $.each(Docs, function (item) {
                            var datasplit = Docs[item].split('|');
                            $('#typeofdocumentddl').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                        });
                    }
                });
                DocumentsDialog.dialog('open');
            });

            $('#typeofdocumentddl').change(function () {
                $.ajax ({
                    url: 'JQDataFetch/getDocumentPoints.aspx',
                    data: {
                        DID: $('#typeofdocumentddl').val(),
                    },
                    success: function(response) {
                        $('#hdnpoints').val(response);
                    }
                });
            });

            $('#btn_kyc_add_Submit').click(function() {
                $.ajax({
                    url: 'Processor/performKYCUpdate.aspx',
                    beforeSend: function() {
                        ShowKYC.dialog('close');
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                        MS: $('#maritalstatus').val(),
                        FD: $('#findep').val(),
                        AURes: $('#AURes').val(),
                        RS: $('#ResStat').val(),
                        ES: $('#EmpStat').val(),
                        OC: $('#occupation').val(),
                        SL: $('#salary').val(),
                        FR: $('#frequency').val(),
                        OI: $('#otherincome').val(),
                        CM: $('#comments').val(),
                    },
                    complete: function() {
                        LoadingDIV.dialog('close');
                    },
                    success: function(response) {
                        alert('KYC Updated successfully');
                        $('#<%=IMD_HasKYC.ClientID%>').attr('src', 'images/group.png');
                        $('#<%=IMD_HasKYC.ClientID%>').attr('title', 'KYC Information Available');
                        $('#<%=hidden_hasKYC.ClientID%>').val('true');
                        $('#<%=hidden_KYCExpiryDays.ClientID%>').val('365');
                        highlightRiskLevel();
                        ShowKYC.dialog('close');
                    }
                });
            });

            $('#btn_HomeDelivery').click(function() {
                ShowHomeDelivery.dialog('open');
                var amountToBeSent = calculateAmountToBeSent($('#<%=hidden_txt_accountbalance.ClientID%>').val());
                $('#HD_availablefunds').val($('#<%=hidden_txt_accountbalance.ClientID%>').val());
                $('#HD_amounttobesent').val(amountToBeSent);
                var ServiceCharge = calculateServiceCharge(amountToBeSent);
                $('#HD_servicecharge').val(ServiceCharge);
                $('#HD_div_sending').text(amountToBeSent);

                var totalsend = amountToBeSent * todayrate;
                $('#HD_div_lkrvalue').text(totalsend);
            })

            $('#btn_AddAccount').click(function() {
                ShowAddNewAccount.dialog('open');
                $.ajax({
                    url: 'JQDataFetch/getAllBanks.aspx',
                    async: false,
                    success: function(data) {
                        var Banks = data.split('~');
                        Banks.sort();
                        $.each(Banks, function(item) {
                            var datasplit = Banks[item].split('|');
                            $('#Add_ddl_Banks').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                        });

                    }   
                });
            });

            $('#Add_ddl_Banks').change(function() {
                $.ajax({
                    url: 'JQDataFetch/getAllBankBranches.aspx',
                    data: {
                        BID: $('#Add_ddl_Banks').val(),
                    },
                    success: function(data) {
                        var Branches = data.split('~');
                        Branches.sort();
                        $('#Add_ddl_Branches').empty();
                        $.each(Branches, function(item) {
                            var datasplit = Branches[item].split('|');
                           
                            $('#Add_ddl_Branches').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                        });
                    }
                });
                
                
            });

            $('#add_AccountNumber').blur(function() {
                if ($('#Add_ddl_Banks').val() == "7278")
                {
                    $('#btn_verifyhnb').show();
                }
            });

            $('#btn_verifyhnb').click(function() {
                if ($('#add_AccountNumber').val() != "")
                {
                    $.ajax({
                        url: 'APIValidations/SAMPATH_ValidateAccount.aspx',
                        data: {
                            BeneAccount: $('#add_AccountNumber').val(),
                        },
                        success: function(result) {
                            $('#VerificationMsg').text(result);
                            alert(result);
                        }
                    })
                }
            });

            $("#btn_ManKYC_Success").click(function () {
                $('#hdn_ManKYC_Outcome').val('SUCCESS');
                $('#btn_ManKYC_Success').attr('class', 'aa_select_btn_active');
                $('#btn_ManKYC_Failed').attr('class', 'aa_select_btn_inactive');
            })

            $("#btn_ManKYC_Failed").click(function () {
                $('#hdn_ManKYC_Outcome').val('FAILED');
                $('#btn_ManKYC_Failed').attr('class', 'aa_select_btn_active');
                $('#btn_ManKYC_Success').attr('class', 'aa_select_btn_inactive');
            });

            $("#BTN_ManKYCSave").click(function () {
                if (!$('#CHK_ManKYC_Confirm').is(':checked'))
                {
                    alert('Please confirm that you are manually overwritting this KYC');
                }
                else
                {
                    if ($('#TXT_ManKYCNotes').val() == "")
                    {
                        alert('Please enter valid notes to proceed');
                    }
                    else
                    {
                        if ($('#hdn_ManKYC_Outcome').val() == "")
                        {
                            alert('Please select outcome to proceed');
                        }
                        else
                        {
                            $.ajax({
                                url: 'Processor/updateCustomerKYCManually.aspx',
                                beforeSend: function() {
                                    LoadingDIV.dialog('open');
                                },
                                data: {
                                    CID: $("#<%=txt_hidden_CustomerID.ClientID%>").val(),
                                    Outcome: $('#hdn_ManKYC_Outcome').val(),
                                    Notes: $('#TXT_ManKYCNotes').val(),
                                },
                                complete: function() {
                                    LoadingDIV.dialog('close');
                                },
                                success: function (response) {
                                if (response == 'OK') {
                                    alert('KYC details have been manually updated');
                                    location.reload();
                                }
                            }
                            });
                        }
                        
                    }
                    
                }
            })

            $('#btn_addnewaccountcomplete').click(function() {
                $.ajax({
                    url: 'Processor/AddBeneficiaryAccount.aspx',
                    data: {
                        BID: $('#<%=hidden_selectedBeneficiary.ClientID%>').val(),
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                        BankID: $('#Add_ddl_Banks').val(),
                        BankName: $('#Add_ddl_Banks :selected').text(),
                        BranchID: $('#Add_ddl_Branches').val(),
                        BranchName: $('#Add_ddl_Branches :selected').text(), 
                        AccNum: $('#add_AccountNumber').val(),
                        AccType: $('#Add_Account_Type').val(),
                    },
                    success: function() {
                        alert('Account has been successfully added');
                        ShowAddNewAccount.dialog('close');
                        loadBeneficiaryAccounts($('#<%=hidden_selectedBeneficiary.ClientID%>').val());
                    },
                    error: function(xhr,err){
                        alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                        alert("responseText: "+xhr.responseText);	
                    }
                })
            });

            $('#btn_AddBeneficiary').click(function() {
                $.ajax({
                    url: 'JQDataFetch/getAgentAssignedCountries.aspx',
                    async: false,
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                    },
                    success: function(data)
                    {
                        var Countries = data.split('~');
                        Countries.sort();
                        $.each(Countries, function(item)
                        {
                            var datasplit = Countries[item].split('|');
                            $('#AddBene_Country').empty().append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                        });
                    }
                })
                AddBeneficiaryDialog.dialog('open');
            });

            $('#btn_AddCredit').click(function() {
                $('#TR_addcrediterrormsg').hide();
                AddCreditDialog.dialog('open');
            })

            $('#btn_EditCustomer').click(function () {

                $.ajax({
                    url: 'JQDataFetch/getAllAgents.aspx',
                    async: false,
                    success: function(response) {
                        var initialsplit = response.split('~');
                        initialsplit.sort();
                        $('#agent_ownership').empty();
                        $('#agent_ownership').append('<option value="">-- SELECT -- </option>');
                        $.each(initialsplit, function(item) {
                            var finalsplit = initialsplit[item].split('|');
                            $('#agent_ownership').append('<option value="' + finalsplit[1] + '">' + finalsplit[0] + '</option>');
                        })
                    }
                });

                var selectValue = $('#<%=div_owningAgent.ClientID%>').text();
                $('#agent_ownership option').filter(function() {
                    return this.text == selectValue;
                }).attr('selected', true);

                EditCustomerDialog.dialog('open');
            });

            $('#btn_AddBeneficiarySubmit').click(function() {
                $.ajax ({
                    url: 'Processor/AddNewBeneficiary.aspx',
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                        BeneName: $('#AddBene_FullName').val(),
                        AdLine1: $('#AddBene_AddressLine1').val(),
                        AdLine2: $('#AddBene_AddressLine2').val(),
                        Suburb: $('#AddBene_Suburb').val(),
                        Postcode: $('#AddBene_Postcode').val(),
                        Country: $('#AddBene_Country').val(),
                    },
                    success: function(response) {
                        alert('Beneficiary Successfully Added');
                        location.reload();
                    },
                    error: function(xhr,err){
                        alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                        alert("responseText: "+xhr.responseText);	
                    }
                });
            });

            $('#<%=chk_CompanyVerified.ClientID%>').change(function() {
                $.ajax ({
                    url: 'Processor/UpdateCompanyVerified.aspx',
                    data: {
                        CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                        CVerified: $('#<%=chk_CompanyVerified.ClientID%>').is(':checked'),
                    },
                    success: function(response) {
                        alert('Successfully updated');
                        location.reload();
                    }
                });
            });

            $('#btn_Submit').click(function() {
                $('#customereditform').submit();
            })

            $('#btn_edit_Submit').click(function() {
                $('#beneficiaryeditform').submit();
            });

            $('#btn_EditBeneficiary').click(function() {
                EditBeneficiaryDialog.dialog('open');
            })

            $('#btn_ViewDebitCredit').click(function() {
                ShowDebitCreditDialog.dialog('open');
            })

            $('#btn_OverTheCounter').click(function() {
                ShowOverTheCounter.dialog('open');
                $('#OTC_availablefunds').val($('#<%=hidden_txt_accountbalance.ClientID%>').val());
                $('#OTC_DIV_AvailableFunds').text('$'+parseFloat($('#<%=hidden_txt_accountbalance.ClientID%>').val()).toFixed(2));
                var amountToBeSent = calculateAmountToBeSent($('#<%=hidden_txt_accountbalance.ClientID%>').val());
                $('#OTC_amounttobesent').val(amountToBeSent);
                var ServiceCharge = calculateServiceCharge(amountToBeSent);
                $('#OTC_servicecharge').val(ServiceCharge);
                $('#OTC_div_sending').text(amountToBeSent);

                var totalsend = amountToBeSent * todayrate;
                $('#OTC_div_lkrvalue').text(totalsend);

            });

            $('#CHK_ConfirmInfo').change(function() {
                if (this.checked)
                {
                    $('#btn_kyc_add_Submit').removeClass('btn_disabled');
                    $('#btn_kyc_add_Submit').addClass('btn_red');
                    $('#btn_kyc_add_Submit').attr("disabled", false);
                }
                else
                {
                    $('#btn_kyc_add_Submit').removeClass('btn_red');
                    $('#btn_kyc_add_Submit').addClass('btn_disabled');
                    $('#btn_kyc_add_Submit').attr("disabled", true);
                }
            });

            $('#btn_saveeditbeneAccount').click(function() {
                $.ajax({
                    url: 'Processor/EditBeneficiaryAccount.aspx',
                    data: {
                        AID: $('#hidden_AccountID').val(),
                        BankID: $('#ddl_Banks').val(),
                        BankName: $('#ddl_Banks :selected').text(),
                        BranchID: $('#Edit_ddl_Branches').val(),
                        BranchName: $('#Edit_ddl_Branches :selected').text(),
                        AccNum: $('#Edit_AccountNumber').val(),
                        AccType: $('#Edit_AccountType').val(),
                        Act: $('#Edit_Active').val(),
                    },
                    success: function() {
                        alert('Beneficiary Account Successfully edited');
                        ShowEditAccount.dialog('close');
                        loadBeneficiaryAccounts($('#<%=hidden_selectedBeneficiary.ClientID%>').val());
                    },
                    error: function(xhr,err){
                        alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                        alert("responseText: "+xhr.responseText);	
                    }
                })
            });

            $('#OTC_amounttobesent').change(function() {
                //var amountToBeSent = calculateAmountToBeSent($('#OTC_amounttobesent').val());
                //$('#OTC_amounttobesent').val(amountToBeSent);
                var ServiceCharge = calculateServiceCharge($('#OTC_amounttobesent').val());
                $('#OTC_servicecharge').val(ServiceCharge);
                $('#OTC_div_sending').text($('#OTC_amounttobesent').val());
                var totalsend = $('#OTC_amounttobesent').val() * todayrate;
                $('#OTC_div_lkrvalue').text(totalsend);
            });

            $('#SM_amounttobesent').change(function() {
                var ServiceCharge = calculateServiceCharge($('#SM_amounttobesent').val());
                $('#SM_servicecharge').val(ServiceCharge);
                $('#SM_div_sending').text($('#SM_amounttobesent').val());
                var totalsend = $('#SM_amounttobesent').val() * todayrate;
                $('#SM_div_lkrvalue').text(totalsend);
            });

            $('#SM_servicecharge').change(function() {
               // alert($('#SM_amounttobesent').val() * $('#SM_exchangerate').val());
                var step1 = $('#SM_availablefunds').val() - $('#SM_servicecharge').val();
                $('#SM_amounttobesent').val(step1);
                $('#SM_div_sending').text(step1);
                var totalsend = step1 * $('#SM_exchangerate').val();
                //var totalsend = ($('#SM_amounttobesent').val() * $('#SM_exchangerate').val()) - $('#SM_servicecharge').val();
               // alert(totalsend);
                $('#SM_div_lkrvalue').text(totalsend);
            })

            $('#HD_amounttobesent').change(function() {
                var ServiceCharge = calculateServiceCharge($('#HD_amounttobesent').val());
                $('#HD_servicecharge').val(ServiceCharge);
                $('#HD_div_sending').text($('#HD_amounttobesent').val());
                var totalsend = $('#HD_amounttobesent').val() * todayrate;
                $('#HD_div_lkrvalue').text(totalsend);
            });


            $('#btn_addcreditcomplete').click(function() {
                if ($('#txt_CreditAmount').val() == "")
                {
                    $('#TR_addcrediterrormsg').show();
                    $('#addcrediterrormsg').text('Please enter a valid amount to proceed.');
                }
                else
                {
                    if ($('#txt_CreditDescription :selected').val() == "")
                    {
                        alert('Please select a deposit method');
                    }
                    else
                    {
                        $.ajax({
                            url: 'Processor/AddCredit.aspx',
                            beforeSend: function() {
                                AddCreditDialog.dialog('close');
                                LoadingDIV.dialog('open');
                            },
                            data: {
                                CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                                CreditAmount: $('#txt_CreditAmount').val(),
                                CreditDescID: $('#txt_CreditDescription').val(),
                                CreditDesc: $('#txt_CreditDescription :selected').text(),
                            },
                            complete: function() {
                                LoadingDIV.dialog('close');
                            },
                            success: function(data) {
                                $('#TR_addcrediterrormsg').show();
                                $('#addcrediterrormsg').text('Please wait. Processing account credit.');
                                openMsgDIV('None', 'Account has been successfully credited', '1');
                                //alert('Account has been credited successfully');
                                //location.reload();
                            }, 
                            error: function(xhr,err){
                                alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                                alert("responseText: "+xhr.responseText);	
                            }

                        });
                    }
                    
                }
                
            });

            $.ajax({
                url: 'JQDataFetch/getDepositMethods.aspx',
                success: function(data) {
                    var results = data.split('~');
                    $('#txt_CreditDescription').append('<option value="">-- SELECT --</option>');
                    $.each(results, function(item) {
                        var finalsplit = results[item].split('|');
                        
                            $('#txt_CreditDescription').append('<option value="' + finalsplit[0] + '">' + finalsplit[1] + '</option>');
                        
                    });
                }
            });

            $('#beneficiaryeditform').validate({
                errorPlacement: function (error, element) {
                    return false;
                },
                rules: {
                    edit_bene_fullname: {
                        required: true,
                    },
                },
                submitHandler: function (form) {
                    $.ajax ({
                        url: 'Processor/EditBeneficiary.aspx',
                        data: {
                            BID: $('#<%=hidden_selectedBeneficiary.ClientID%>').val(),
                            FullName: $('#edit_bene_fullname').val(),
                            AddressLine1: $('#edit_bene_addressline1').val(),
                            AddressLine2: $('#edit_bene_addressline2').val(),
                            Suburb: $('#edit_bene_suburb').val(),
                            State: $('#edit_bene_state').val(),
                            Postcode: $('#edit_bene_postcode').val(),
                            Country: $('#edit_bene_country').val(),
                            TelHome: $('#edit_bene_homephone').val(),
                            TelWork: $('#edit_bene_workphone').val(),
                            Mobile: $('#edit_bene_mobile').val(),
                            EmailAddress: $('#edit_bene_email').val(),
                            NCI: $('#edit_bene_nationalid').val(),
                            Relationship: $('#edit_bene_relationship').val(),
                            Active: 'Y',
                        },
                        success: function(data) {
                            alert('Beneficiary has been edited successfully');
                            EditCustomerDialog.dialog('close');
                            location.reload();
                        }, 
                        error: function(xhr,err){
                            alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                            alert("responseText: "+xhr.responseText);	
                        }

                    });
                    return false;
                }
            });

          <%--  $('#customereditform').validate({
                errorPlacement: function (error, element) {
                    return false;
                },
                rules: {
                    <%=lastname.UniqueID%> :{
                        required: true,
                    },
                    <%=firstnames.UniqueID%> :{
                        required: true,
                    },
                    <%=dob.UniqueID%> :{
                        required: true,
                    },
                    <%=addressline1.UniqueID%> :{
                        required: true,
                    },
                    <%=suburb.UniqueID%> :{
                        required: true,
                    },
                    <%=state.UniqueID%> :{
                        required: true,
                    },
                    <%=postcode.UniqueID%> :{
                        required: true,
                    },

                },
                submitHandler: function (form) {
                    // do other things for a valid form
                    //form.submit();
                    $.ajax ({
                        url: 'Processor/EditCustomerDetails.aspx',
                        data: {
                            CID: $('#<%=txt_hidden_CustomerID.ClientID%>').val(),
                            lastname: $('#<%=lastname.ClientID%>').val(),
                            firstnames: $('#<%=firstnames.ClientID%>').val(),
                            dob: $('#<%=dob.ClientID%>').val(),
                            addressline1: $('#<%=addressline1.ClientID%>').val(),
                            addressline2: $('#<%=addressline2.ClientID%>').val(),
                            suburb: $('#<%=suburb.ClientID%>').val(),
                            state: $('#<%=state.ClientID%>').val(),
                            postcode: $('#<%=postcode.ClientID%>').val(),
                            homephone: $('#<%=phonehome.ClientID%>').val(),
                            workphone: $('#<%=phonework.ClientID%>').val(),
                            mobile: $('#<%=phonemobile.ClientID%>').val(),
                            email1: $('#<%=email1.ClientID%>').val(),
                            email2: $('#<%=email2.ClientID%>').val(),
                            notes: $('#<%=edit_notes.ClientID%>').val(),
                            password: $('#<%=edit_password.ClientID%>').val(),
                        },
                        success: function(data) {
                            alert('Customer has been edited successfully');
                            EditCustomerDialog.dialog('close');
                            location.reload();
                        }, 
                        error: function(xhr,err){
                            alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                            alert("responseText: "+xhr.responseText);	
                        }

                    })
                    return false;
                }
            });--%>
            
            
            
        });

       
        
    </script>
    </asp:Content>


<asp:Content ID="header" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">



<div class="container_btns">
    <table class="tbl_width_1200">
        <tr>
            <td class="tbl_width_280">
                <table class="tbl_width_280">
                    <tr>
                        <td class="aa_label_font">RISK PROFILE</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="risk_1" id="td_risk1" runat="server">1</td>
                        <td class="risk_2" id="td_risk2" runat="server">2</td>
                        <td class="risk_neutral" id="td_risk3" runat="server">3</td>
                        <td class="risk_neutral" id="td_risk4" runat="server">4</td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="tbl_width_920">
                    <!-- MAIN TABLE STARTS HERE -->
                    <tr>
                        <td>
                            <table class="tbl_width_920">
                                <tr style="height:15px;">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="stats_02">TOTAL LAST MONTH AUD</td>
                                    <td class="stats_02">TOTAL LAST YEAR AUD</td>
                                    <td class="stats_02">TRANSACTIONS MONTH/YEAR</td>
                                </tr>
                                <tr style="height:25px;">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="stats_03">
                                        <div id="DIV_MonthlyRemittanceTotal" runat="server">$ 2,000.00</div>
                                    </td>
                                    <td class="stats_03">
                                        <div id="DIV_YearlyRemittanceTotal" runat="server">$ 50,000.00</div>
                                    </td>
                                    <td class="stats_03">
                                        <div id="DIV_Totals" runat="server">10/127</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
                <!-- MAIN TABLE ENDS HERE -->
            </td>
        </tr>
    </table>
</div>



<div class="container_one" style="background-color:#EFEFEF;">
            <table class="tbl_width_1200" >
                <tr>
                    <td style="width:280px;">
                        <table class="tbl_width_280">
                                                        
                            <!-- tbl comes here *** -->
                            <tr><td>
                            <table class="tbl_width_280 background_FFFFFF">

                            <tr style="height:25px; background-color: #76D17F;"><td>&nbsp;</td></tr>
                            <tr style="height:150px; background-color: #76D17F;">
                                <td>
                                    <table class="tbl_width_150">
                                        <tr>
                                            <td><div class="imgWrapper"><img width="160" height="160" id="img_CustomerPic" runat="server" /></div></td>
                                        </tr>
                                    </table>
                                    

                                </td>
                                    

                            </tr>
                            

                            


                            <tr style="background-color: #76D17F;">
                                <td class="cus_name"><div id="div_cust_full_name" runat="server"></div>
                                    <asp:TextBox ID="hidden_txt_accountbalance" runat="server"></asp:TextBox><asp:TextBox ID="txt_hidden_CustomerID" runat="server"></asp:TextBox><asp:TextBox ID="txt_hidden_FirstBeneficiaryID" runat="server"></asp:TextBox><asp:TextBox ID="txt_hidden_AccountID" runat="server"></asp:TextBox><asp:TextBox ID="txt_hidden_Rate" runat="server"></asp:TextBox><asp:TextBox ID="txt_hidden_agentID" runat="server"></asp:TextBox><asp:TextBox ID="hidden_SpendingPower" runat="server"></asp:TextBox><asp:TextBox ID="hidden_KYCRunBefore" runat="server"></asp:TextBox>
                                </td>

                            </tr>

                            <tr id="trnotes" runat="server">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td>
                                                <table class="tbl_width_240">
                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td class="dsh_cus_notes"><span id="shownotes" runat="server"></span></td>
                                                    </tr>
                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>



                                
                            </tr>

                                <!-- ICONS -->
                                <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <table class="tbl_width_280 dsh_KYC_tab_h">
                                            <tr style="height:45px;">
                                                <td style="width:69px; vertical-align:middle;" class="dsh_tab_active" id="TD_001">
                                                    <table style="width:100%;">
                                                        <tr><td style="height:20px; text-align:center; "><asp:Image ID="Image2" runat="server" ImageUrl="~/images/id-card-gry.png" /></td></tr>
                                                        <!-- <tr><td>KYC CHECK</td></tr> -->
                                                    </table>
                                                </td>
                                                <td style="width:2px; background-color:#EFEFEF;" class="normal_font">&nbsp;</td>
                                                <td style="width:68px; vertical-align:middle;" class="dsh_tab_inactive" id="TD_002">
                                                    <table style="width:100%;">
                                                        <tr><td style="height:20px; text-align:center; "><asp:Image ID="Image20" runat="server" ImageUrl="~/images/userx.png" Height="24px" /></td></tr>
                                                        <!-- <tr><td>KYC CHECK</td></tr> -->
                                                    </table>
                                                </td>
                                                <td style="width:2px; background-color:#EFEFEF;" class="normal_font">&nbsp;</td>
                                                <td style="width:68px;" class="dsh_tab_inactive" id="TD_003">&nbsp;</td>
                                                <td style="width:2px; background-color:#EFEFEF; color:#CCC;" class="normal_font">&nbsp;</td>
                                                <td style="width:69px;" class="dsh_tab_inactive" id="TD_004">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <div id="DIV_001">
                                            <table style="width:100%; background-color: #FFF;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>
                                                        <table class="tbl_width_280"><tr><td>&nbsp;</td><td class="dsh_KYC_close">x</td></tr></table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="tbl_width_240">
                                                            <tr>
                                                                <td>
                                                                    <div class="dsh_KYC_success" id="dsh_KYC_Success" runat="server">KYC CHECK COMPLETED !</div>
                                                                    <span class="dsh_KYC_fail" id="dsh_KYC_Fail" runat="server">KYC CHECK FAILED !</span>
                                                                    <div id="dsh_KYC_NotRun" runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td class="dsh_KYC_none">WARNING !</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="dsh_KYC_none_font">KYC Check has not been completed for this customer.</td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_240">
                                                                        <tr>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_DateLabel" runat="server" style="width:50px;">Date:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_DateValue" runat="server" style="width:190px;">2</td></tr></table></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_ByLabel" runat="server">By:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_ByValue" runat="server" style="width:190px;">2</td></tr></table></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_RefLabel" runat="server" style="width:50px;">Ref:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_RefValue" runat="server" style="width:190px;">2</td></tr></table></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="dsh_KYC_label" id="dsh_KYC_InfoLabel" runat="server">Info:</td>
                                                                            <td><table><tr><td class="dsh_KYC_contents" id="dsh_KYC_InfoValue" runat="server">2</td></tr></table></td>
                                                                        </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="spacer5"><td>&nbsp;</td></tr>
                                                                        <tr><td class="dsh_KYC_none_font">
                                                                            <asp:Label ID="lbl_KYC_ManualKYCComments" runat="server" Text="Label"></asp:Label></td></tr>


                                                                        
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr><td>&nbsp;</td></tr>
                                                            
                                                            <tr id="TR_DisplayKYCErrors" runat="server">
                                                                <td>
                                                                    <table style="width:240px;">
                                                                        <tr>
                                                                            <td>
                                                                                <table style="width:240px;" class="dsh_KYC_bg_green">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table class="tbl_width_220">
                                                                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                                                <tr><td><asp:Label ID="LBL_KYCErrors" runat="server" Text="Label"></asp:Label></td></tr>
                                                                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr><td style="background-color:#FFFFFF;">&nbsp;</td></tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <tr id="TR_RUNKYC" runat="server">
                                                                <td>
                                                                    <table class="tbl_width_240" >
                                                                        <tr>
                                                                            <td style="display:inline; float:left;"><asp:button runat="server" text="Run KYC Check"  CssClass="aa_btn_green" style="margin-bottom:0px !important;" OnClick="Unnamed1_Click" /> <input id="btn_OverrideKYC" type="button" value="Manual KYC" class="aa_btn_red" runat="server"/></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="TR_ConfirmRUNKYC" runat="server">
                                                                <td>
                                                                    <table class="tbl_width_240" >
                                                                        <tr class="spacer10" style="background-color:#FCF8E1"><td>&nbsp;</td></tr>
                                                                        <tr><td class="dsh_kyc_msg">Are you sure you want to rerun the KYC Check for this Customer?</td></tr>
                                                                        <tr class="spacer10" style="background-color:#FCF8E1"><td>&nbsp;</td></tr>
                                                                        <tr><td style="background-color:#FCF8E1; padding-left:10px;"><asp:button ID="confirm_YES_RunKYC" runat="server" text="Yes"  CssClass="aa_btn_green_slim" style="margin-bottom:0px !important;" OnClick="confirm_YES_RunKYC_Click" />&nbsp;<asp:button ID="confirm_NO_RunKYC" runat="server" text="No"  CssClass="aa_btn_red_slim" style="margin-bottom:0px !important;" OnClick="confirm_NO_RunKYC_Click" /></td></tr>
                                                                        <tr class="spacer10" style="background-color:#FCF8E1"><td>&nbsp;</td></tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="DIV_002">
                                            <table style="width:100%;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="DIV_003">
                                            <table style="width:100%;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="DIV_004">
                                            <table style="width:100%;">
                                                <tr><td style="border-top:5px solid #76D17F;" class="spacer5">&nbsp;</td></tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                                <tr style="height:20px;""><td>&nbsp;</td></tr>

                                <tr class="spacer10" style="background-color:#EFEFEF;"><td>&nbsp;</td></tr>

                            <tr class="cus_divider" style="display:none;"> <!-- START ICONS -->
                                <td style="background-color:#D6DFDE;">
                                    <table class="tbl_width_280">
                                        <tr style="height:50px;">
                                            <td class="cus_ver_divider"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/id-card-gry.png" /></td>
                                            <td class="cus_ver_divider cursor_hand"><asp:Image ID="IMD_HasKYC" runat="server" ImageUrl="~/images/group.png"  /></td>
                                            <td class="cus_ver_divider">&nbsp;</td>
                                            <td class="cus_ver_divider2 cursor_hand"><div id="OpenModalCVerified"><asp:Image ID="IMG_CompanyVerified" runat="server" ImageUrl="~/images/medal.png" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr> <!-- END ICONS -->


                            <tr>
                                <td class="dsh_cust_details_heading">Customer Details</td>
                            </tr>

                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image12" runat="server" ImageUrl="~/images/c_icon_09.png" /></td>
                                            <td class="cus_c"><div id="div_owningAgent" runat="server">AGENT</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                                <tr class="cus_divider_efefef">
								<td>
									<table class="tbl_width_280">
										<tr>
											<td class="cus_h">
												<asp:Image ID="Image24" runat="server" ImageUrl="~/images/c_icon_01.png" /></td>
											<td class="cus_c"><div id="div_customertype" runat="server">TYPE</div></td>
										</tr>
									</table>
								</td>
							</tr>
                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image5" runat="server" ImageUrl="~/images/c_icon_01.png" /></td>
                                            <td class="cus_c"><div id="div_CustomerFullAddress" runat="server">ADDRESS</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image11" runat="server" ImageUrl="~/images/c_icon_07.png" /></td>
                                            <td class="cus_c"><div id="div_CustomerDOB" runat="server">DOB</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image9" runat="server" ImageUrl="~/images/c_icon_04.png" /></td>
                                            <td class="cus_c"><div id="div_CustomerMobile" runat="server">MOBILE</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image8" runat="server" ImageUrl="~/images/c_icon_03.png" /></td>
                                            <td class="cus_c"><div id="div_CustomerHomeNumber" runat="server">HOME</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image10" runat="server" ImageUrl="~/images/c_icon_06.png" /></td>
                                            <td class="cus_c"><div id="div_CustomerWorkNumber" runat="server">WORK</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="cus_divider_efefef">
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            
                                            <td class="cus_h"><asp:Image ID="Image6" runat="server" ImageUrl="~/images/c_icon_02.png" /></td>
                                            <td class="cus_c"><div id="div_CustomerEmail1" runat="server">EMAIL1</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_280">
                                        <tr>
                                            <td class="cus_h"><asp:Image ID="Image7" runat="server" ImageUrl="~/images/c_icon_08.png" /></td>
                                            <td class="cus_c"><div id="div_CustomerEmail2" runat="server">EMAIL2</div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr style="background-color:#D6DFDE;"><td>&nbsp;</td></tr>
                            <tr><td style="padding-left:20px; background-color:#D6DFDE;"><button class="aa_btn_green"  id="btn_EditCustomer" onclick="return false"><span>More</span></button>&nbsp;&nbsp;<button class="aa_btn_green" id="BTN_DocumentsClick" onclick="return false"><span>Documents</span></button></td></tr>
                            <tr style="background-color:#D6DFDE;"><td>&nbsp;</td></tr>
                                
                                <!--tbl finishes here ***-->
                                </table></td></tr>
                            
                            <!-- <tr><td>&nbsp;</td></tr> extra line if needed-->

                        </table>
                    </td>
                    <td style="width:20px;">&nbsp;</td>
                    <td style="width:900px; vertical-align:top;"> 
                        <table> <!-- MAIN 900px TABLE START -->
                            <tr>
                                <td>

<!-- ************************* NOTES/PASSWORD PROTECTION DETAILS ************************* -->
    <div id="overallDIV" runat="server">
        <table class="tbl_width_900">
            <tr id="TR_PrimaryDocExp" runat="server"> 
                <td>
                    <table class="tbl_width_900">
                        <tr>
                            <td class="error1">
                                <table class="tbl_width_900">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_860">
                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                                <tr><td class="error1_font">Primary Document has expired.</td></tr>
                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="spacer10">&nbsp;</td></tr>
                    </table>
                </td>
            </tr>

            <tr id="TR_NoPrimaryDocs" runat="server"> 
                <td>
                    <table class="tbl_width_900">
                        <tr>
                            <td class="error1">
                                <table class="tbl_width_900">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_860">
                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                                <tr><td class="error1_font">No Primary Documents</td></tr>
                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="spacer10">&nbsp;</td></tr>
                    </table>
                </td>
            </tr>

            <tr id="TR_PrimaryDocExpSoon" runat="server"> 
                <td>
                    <table class="tbl_width_900">
                        <tr>
                            <td class="error1">
                                <table class="tbl_width_900">
                                    <tr>
                                        <td>
                                            <table class="tbl_width_860">
                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                                <tr><td class="error1_font">Primary document will expire on <span id="SPN_DocExpDate" runat="server"></span></td></tr>
                                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td class="spacer10">&nbsp;</td></tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <div id="notespassdiv" runat="server">
                        <table>
                            <tr id="ErrorMessageArea" runat="server">
                                <td>
                                    <table class="tbl_width_900">
                                        <tr>
                                            <td class="alert_01_red">
                                                <table class="tbl_width_860">
                                                    <tr>
                                                        <td style="width:60px;"><asp:Image ID="Image4" runat="server" ImageUrl="~/images/bell.png" /></td>
                                                        <td style="width:800px; vertical-align:top;" class="alert_01_red_message" id="errormessagedisplay" runat="server"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:20px;"></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>

            <tr class="background_FFFFFF"> <!-- Account Balance and Adding/Viewing Transactions -->
                <td>
                    <table class="tbl_width_860">
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                        <tr><td class="label_headings">CUSTOMER ACCOUNT BALANCE</td></tr>
                        <tr>
                            <td><table class="tbl_width_860">
                                <tr>
                                    <td class="cust_acc_bal"><div id="DIV_BankAccountBalance" runat="server"></div></td>
                                    <td style="text-align:right;"><button class="btn_green_nomargin" id="btn_ViewDebitCredit" onclick="return false"><span>View Account</span></button><button style="display:none;" class="btn_green_nomargin" id="btn_AddCredit" onclick="return false"><span>Add Credit</span></button></td>
                                </tr>
                                </table>
                                
                            </td></tr>
                        <tr><td style="height:20px;">&nbsp;</td></tr>
                    </table>
                </td>
            </tr>

            <tr><td class="spacer10">&nbsp;</td></tr>

            <tr class="background_FFFFFF"> <!-- SELECT BEN STARTS -->
                <td>
                    <table class="tbl_width_900">
                    <tr>
                        <td style="height:15px;">&nbsp;</td>
                    </tr>                    

                    <tr>
                        <td>
                            <table class="tbl_width_860">
                                <tr>
                                    <td class="label_headings">SELECT BENEFICIARY</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><asp:DropDownList ID="ddl_List_Beneficiaries" runat="server"></asp:DropDownList></td>
                                    <td style="text-align:right;"><button class="btn_green_nomargin" style="display:none;" id="btn_AddBeneficiary" onclick="return false">Add New Beneficiary</button></td>

                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                    <tr style="height:20px;"><td>&nbsp;</td></tr>
                    <tr id="tr_ben_info">
                        <td>
                            <div>
                                <table class="tbl_width_860"> <!-- BEN DETAILS TABLE START -->
                                    <tr>
                                        <td>
                                            <table class="tbl_width_860">
                                                <tr>
                                                    <td style="background-color:#EFEFEF; height:125px; width:840px; margin: 0 auto;">
                                                        <table class="tbl_width_800">
                                                            <tr style="height:20px"><td>&nbsp;</td></tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_820">
                                                                        <tr>
                                                                            <td><div class="dsh_ben_name" id="div_BeneficiaryName">&nbsp;</div></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_820">
                                                                        <tr>
                                                                            <td style="width:408px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_AKA"></div></td>
                                                                                        <td class="dsh_ben_heading_info">AKA</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width:4px;">&nbsp;</td>
                                                                            <td style="width:202px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_BeneficiaryNationalityCardID"></div></td>
                                                                                        <td class="dsh_ben_heading_info">NID</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width:4px;">&nbsp;</td>
                                                                            <td style="width:202px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_Relationship"></div></td>
                                                                                        <td class="dsh_ben_heading_info">REL</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr class="spacer5"><td>&nbsp;</td></tr>

                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_820">
                                                                        <tr>
                                                                            <td style="width:202px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_DOB"></div></td>
                                                                                        <td class="dsh_ben_heading_info">DOB</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width:4px;">&nbsp;</td>
                                                                            <td style="width:202px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_PlaceOfBirth"></div></td>
                                                                                        <td class="dsh_ben_heading_info">POB</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width:4px;">&nbsp;</td>
                                                                            <td style="width:202px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_CountryOfBirth"></div></td>
                                                                                        <td class="dsh_ben_heading_info">COB</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width:4px;">&nbsp;</td>
                                                                            <td style="width:202px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_Nationality"></div></td>
                                                                                        <td class="dsh_ben_heading_info">NAT</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr class="spacer5"><td>&nbsp;</td></tr>

                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_820">
                                                                        <tr>
                                                                            <td >
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_BeneficiaryAddress"></div></td>
                                                                                        <td class="dsh_ben_heading_info">NAT</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr class="spacer5"><td>&nbsp;</td></tr>

                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_820">
                                                                        <tr>
                                                                            <td style="width:202px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_ContactNo"></div></td>
                                                                                        <td class="dsh_ben_heading_info">TEL</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width:4px;">&nbsp;</td>
                                                                            <td style="width:610px;">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td class="dsh_ben_info"><div id="div_EmailAddress"></div></td>
                                                                                        <td class="dsh_ben_heading_info">EML</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <tr><td class="spacer10">&nbsp;</td></tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="tbl_width_820" id="BeneAccountTable">
                                                                        <thead>
                                                                            <td class="ben_acct_headings" style="padding-left: 10px;">BANK</td>
                                                                            <td class="ben_acct_headings">BRANCH</td>
                                                                            <td class="ben_acct_headings">ACCOUNT NO</td>
                                                                            <td class="ben_acct_headings" style="width:40px;">TYPE</td>
                                                                            <td class="ben_acct_headings" style="width:40px;">ACT?</td>
                                                                        </thead>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <tr><td style="height:20px;">&nbsp;</td></tr>
                                                        </table>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">&nbsp;</td>
                                    </tr>

                                    <tr >
                                        <td hidden="hidden">
                                            <!-- DO NOT DELETE -->
                                            <input type="text" id="hidden_selectedBeneficiary" name="hidden_selectedBeneficiary" runat="server" />&nbsp;
                                            <input type="text" id="hidden_hasKYC" name="hidden_hasKYC" runat="server" />
                                            <input type="text" id="hidden_CurrentSentAmount" name="hidden_CurrentSentAmount" runat="server" />
                                            <input type="text" id="hidden_KYCExpiryDays" name="hidden_KYCExpiryDays" runat="server" />
                                            <input type="text" id="hidden_NbTrans" name="hidden_NbTrans" runat="server" />
                                            <input type="text" id="sendingBank" name="sendingBank" />
                                            <input type="text" id="sendingBankBranch" name="sendingBankBranch" />
                                            <input type="text" id="hidden_hasID" name="hidden_hasID" runat="server" />
                                            <input type="text" id="hidden_FundsAvailable" name="hidden_FundsAvailable" runat="server" />
                                            <!-- END DO NOT DELETE -->

                                        </td>
                                    </tr>
                                </table> <!-- BEN DETAILS TABLE END -->
                            </div>
                        </td>
                    </tr>
                </table> <!-- SELECT BEN ENDS -->
                </td>
            </tr>

            <tr>
                <td class="spacer10">&nbsp;</td>
            </tr>

            <tr>
                <td>

    <!-- ************************* PAST TRANSACTIONS ************************* -->
    <div class="container_nobg_pad_tb no_padding_btm no_padding_top" id="Div1" runat="server">
        <table class="tbl_width_900 background_FFFFFF"> 
            <tr hidden="hidden"><td><input type="text" id="hidden_risklevel" name="hidden_risklevel" runat="server" /></td></tr>

            <tr><td class="dsh_cust_details_heading">ALL Past Transactions</td></tr>
            <tr><td>&nbsp;</td></tr>

            <tr><td>
                <table class="tbl_width_860">
                    <tr><td>

                                <div id="div-acc-pastrecords" class="ui-accordion"> <!-- ACCORDIAN DIV STARTS HERE -->
                                    
                                    <div>
                                        <table id="example" class="display nowrap" cellspacing="0" width="100%">

                                                <thead>
                                                    <tr class="tbl__full__width">
                                                        <th class="tbl__head__left">TRN ID</th>
                                                        <th class="tbl__head__left">DATE</th>
                                                        <th class="tbl__head__left">BENEFICIARY</th>
			                                            <th class="tbl__head__left">RATE</th>
                                                        <th class="tbl__head__numbers">AMOUNT</th>
                                                        <th class="tbl__head__numbers">CHARGE</th>
                                                        <th class="tbl__head__numbers">CUR</th>
                                                        <th class="tbl__head__numbers">AMOUNT</th>
                                                        <th class="tbl__head__left">STATUS</th>
			                                            <th>MORE INFO</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
		 
	                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div> <!-- ACCORDIAN DIV ENDS HERE -->

                        </td></tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                </table>

            </td></tr>
        </table>
    </div>
    <!-- ************************* PAST TRANSACTIONS ************************* -->

                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

        </table>
    </div>
    <!-- ************************* NOTES/PASSWORD PROTECTION DETAILS ************************* -->
                                </td>
                            </tr>
                        </table> <!-- MAIN 900px TABLE END -->
                    </td>
                </tr>
                </table>
    </div>










<!-- this section needs to be taken out and put into the new table format -->


    </form>

<!-- ************************* LOADING ************************* -->
<div id="LoadingDiv">
  <p>&nbsp;</p>
  <p>Please wait until beneficiary details are loaded</p>
</div>


<!-- ************************* BUTTON 01 EDIT CUSTOMER MODAL POPUP START ************************* -->    
    <div id="EditCustomerDiv">
		<div class="edit__form__main">
			<form id="formEditCust">
			<table class="tbl_width_600">
				
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                         <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_EditBusiness_001"><asp:Image ID="Image23" runat="server" ImageUrl="~/images/company.png" title="COMPANY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_EditBusiness_002"><asp:Image ID="Image25" runat="server" ImageUrl="~/images/user.png" title="AUTHORIZED CONTACT" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_EditCustomer_003"><asp:Image ID="Image26" runat="server" ImageUrl="~/images/paper.png" title="NOTES & PASSWORD" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_EditCustomerDetails">
                                
                                     <input type="hidden" id="bcountry" name="bcountry" value="" />
                <input type="hidden" id="bnationality" name="bnationality" value="" />
                                    <input type="hidden" id="hidCustId" name="hidCustId" />
                                    <input type="hidden" id="isbusi" name="isbusi" />
                                    <input type="hidden" id="type" name="type" value="update" />
            <table class="tbl_width_560"  id="divBusiness" >
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">FULL BUSINESS NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">ABN/ACN *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_393"><div><input class="aa_input" id="BBusinessName" name="BBusinessName" required style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;"></td>
                                                <td class="style__width_186"><div><input class="aa_input" id="BABN" name="BABN" style="width:100%;" required type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">TRADING NAME (IF APPLICABLE)</td>
                                               
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="BTradingName" name="BTradingName" style="width:100%;" type="text" value="" /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>
                                
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">Busi. Contct</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">Busi. Email</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">Busi. Web site</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BBContact" name="BBContact" style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BBEmail" name="BBEmail" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BBWeb" name="BBWeb" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font_heading">PRINCIPLE PLACE OF BUSINESS (PO BOX IS NOT ACCEPTABLE)</td>
                                            </tr>
                                            <tr hidden="hidden">
                                                <td><div><input class="aa_input" id="BAddressLine1" name="BAddressLine1" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">UNIT NO</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NO *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET TYPE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_50"><div><input class="aa_input" id="bus_unitno" name="bus_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td class="style__width_10">&nbsp;</td> 
                                                <td class="style__width_75"><div><input class="aa_input" required id="bus_streetno" name="bus_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_290"><div><input class="aa_input" required id="bus_streetname" name="bus_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                                        <select id="bus_streettype" name="bus_streettype" required style="width:100%;">
                                                            <option value="0">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_600"><div><input class="aa_input" id="BAddressLine2" name="BAddressLine2" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input required class="aa_input" id="bus_suburb" name="bus_suburb" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input required class="aa_input" id="bus_state" name="bus_state" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input required class="aa_input" id="bus_postcode" name="bus_postcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BSuburb" name="BSuburb" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BState" name="BState" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostcode" name="BPostcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="bus_country" name="bus_country" style="width:100%;" type="text" value="Australia"  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_seperator_white"><td>&nbsp;</td></tr>
                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                 <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font_heading">POSTAL ADDRESS (IF DIFFERENT FROM ABOVE)</td>
                                            </tr>
                                            <tr hidden="hidden">
                                                <td><div><input class="aa_input" id="BPostalAddressLine1" name="BPostalAddressLine1" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">UNIT NO</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NO *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET NAME *</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STREET TYPE *</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_50"><div><input class="aa_input" id="pos_unitno" name="pos_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td class="style__width_10">&nbsp;</td>
                                                <td class="style__width_75"><div><input required class="aa_input" id="pos_streetno" name="pos_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_290"><div><input required class="aa_input" id="pos_streetname" name="pos_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135">
                                                    <div>
                                                        <select id="pos_streettype" name="pos_streettype" required style="width:100%;">
                                                             <option value="0">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                                <tr hidden="hidden">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">POSTAL ADDRESS LINE 2</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="BPostalAddressLine2" name="BPostalAddressLine2" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">SUBURB</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">STATE</td>
                                                <td class="aa_label_font">&nbsp;</td>
                                                <td class="aa_label_font">POSTCODE</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_290"><div><input class="aa_input" id="BPostalSuburb" name="BPostalSuburb" style="width:100%;" type="text" value="" /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostalState" name="BPostalState" style="width:100%;" type="text" value=""  /></div></td>
                                                <td style="width:18px;">&nbsp;</td>
                                                <td class="style__width_135"><div><input class="aa_input" id="BPostalPostcode" name="BPostalPostcode" style="width:100%;" type="text" value=""  /></div></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr class="spacer10"><td>&nbsp;</td></tr>

                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td class="aa_label_font">COUNTRY</td>
                                            </tr>
                                            <tr>
                                                <td><div><input class="aa_input" id="pos_country" name="pos_country" style="width:100%;" type="text" value="Australia"  /></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr style="height:20px;"><td>&nbsp;</td></tr>
                                            
                            </table>
          
            <table class="tbl_width_560" id="tblIndi">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">

                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="aa_label_font">LAST NAME *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">TITLE *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">GIVEN NAME/S *</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style__width_186"><div><input class="aa_input" required id="bus_lastname" name="bus_lastname" style="width:100%;" type="text" value="" /></div></td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td style="width:83px;">
                                                                <div>
                                                                    <select id="bus_title" name="bus_title" required style="width:100%;">
                                                                        <option value="0">--Select--</option>
	                                                                    <option value="Mr">Mr</option>
	                                                                    <option value="Mrs">Mrs</option>
	                                                                    <option value="Miss">Miss</option>
	                                                                    <option value="Ms">Ms</option>
	                                                                    <option value="Dr">Dr</option>
	                                                                    <option value="Other">Other</option>
                                                                    </select>                                   
                                                                </div>
                                                            </td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td style="width:290px;"><div><input class="aa_input" required id="bus_givenNames" name="bus_givenNames" style="width:100%;" type="text"  value=""  /></div></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer10"><td>&nbsp;</td></tr>

                                            <tr>
                                                <td>
                                                    <table class="tbl_width_560">
                                                        <tr>
                                                            <td class="aa_label_font">COUNTRY OF BIRTH *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">DATE OF BIRTH (DOB) *</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                            <td class="aa_label_font">PLACE OF BIRTH *</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style__width_186">
                                                                <div>
                                                                    <select id="bus_countryofbirth" name="bus_countryofbirth" required style="width:100%;">
	                                                                    <option selected="selected" value="0">--Select--</option>
	                                                                    <option value="1">Afghanistan</option>
	                                                                    <option value="2">Albania</option>
	                                                                    <option value="3">Algeria</option>
	                                                                    <option value="4">Andorra</option>
	                                                                    <option value="5">Angola</option>
	                                                                    <option value="6">Antigua and Barbuda</option>
	                                                                    <option value="7">Argentina</option>
	                                                                    <option value="8">Armenia</option>
	                                                                    <option value="9">Australia</option>
	                                                                    <option value="10">Austria</option>
	                                                                    <option value="11">Azerbaijan</option>
	                                                                    <option value="12">Bahamas</option>
	                                                                    <option value="13">Bahrain</option>
	                                                                    <option value="14">Bangladesh</option>
	                                                                    <option value="15">Barbados</option>
	                                                                    <option value="16">Belarus</option>
	                                                                    <option value="17">Belgium</option>
	                                                                    <option value="18">Belize</option>
	                                                                    <option value="19">Benin</option>
	                                                                    <option value="20">Bhutan</option>
	                                                                    <option value="21">Bolivia</option>
	                                                                    <option value="22">Bosnia and Herzegovina</option>
	                                                                    <option value="23">Botswana</option>
	                                                                    <option value="24">Brazil</option>
	                                                                    <option value="25">Brunei</option>
	                                                                    <option value="26">Bulgaria</option>
	                                                                    <option value="27">Burkina Faso</option>
	                                                                    <option value="28">Burundi</option>
	                                                                    <option value="29">Cambodia</option>
	                                                                    <option value="30">Cameroon</option>
	                                                                    <option value="31">Canada</option>
	                                                                    <option value="32">Cape Verde</option>
	                                                                    <option value="33">Central African Republic</option>
	                                                                    <option value="34">Chad</option>
	                                                                    <option value="35">Chile</option>
	                                                                    <option value="36">China</option>
	                                                                    <option value="37">Colombia</option>
	                                                                    <option value="38">Comoros</option>
	                                                                    <option value="39">Congo (Kinshasa)</option>
	                                                                    <option value="40">Congo (Brazzaville)</option>
	                                                                    <option value="41">Costa Rica</option>
	                                                                    <option value="42">Cote d Ivoire (Ivory Coast)</option>
	                                                                    <option value="43">Croatia</option>
	                                                                    <option value="44">Cuba</option>
	                                                                    <option value="45">Cyprus</option>
	                                                                    <option value="46">Czech Republic</option>
	                                                                    <option value="47">Denmark</option>
	                                                                    <option value="48">Djibouti</option>
	                                                                    <option value="49">Dominica</option>
	                                                                    <option value="50">Dominican Republic</option>
	                                                                    <option value="51">Ecuador</option>
	                                                                    <option value="52">Egypt</option>
	                                                                    <option value="53">El Salvador</option>
	                                                                    <option value="54">Equatorial Guinea</option>
	                                                                    <option value="55">Eritrea</option>
	                                                                    <option value="56">Estonia</option>
	                                                                    <option value="57">Ethiopia</option>
	                                                                    <option value="58">Fiji</option>
	                                                                    <option value="59">Finland</option>
	                                                                    <option value="60">France</option>
	                                                                    <option value="61">Gabon</option>
	                                                                    <option value="62">Gambia</option>
	                                                                    <option value="63">Georgia</option>
	                                                                    <option value="64">Germany</option>
	                                                                    <option value="65">Ghana</option>
	                                                                    <option value="66">Greece</option>
	                                                                    <option value="67">Grenada</option>
	                                                                    <option value="68">Guatemala</option>
	                                                                    <option value="69">Guinea</option>
	                                                                    <option value="70">Guinea-Bissau</option>
	                                                                    <option value="71">Guyana</option>
	                                                                    <option value="72">Haiti</option>
	                                                                    <option value="73">Honduras</option>
	                                                                    <option value="74">Hungary</option>
	                                                                    <option value="75">Iceland</option>
	                                                                    <option value="76">India</option>
	                                                                    <option value="77">Indonesia</option>
	                                                                    <option value="78">Iran</option>
	                                                                    <option value="79">Iraq</option>
	                                                                    <option value="80">Ireland</option>
	                                                                    <option value="81">Israel</option>
	                                                                    <option value="82">Italy</option>
	                                                                    <option value="83">Jamaica</option>
	                                                                    <option value="84">Japan</option>
	                                                                    <option value="85">Jordan</option>
	                                                                    <option value="86">Kazakhstan</option>
	                                                                    <option value="87">Kenya</option>
	                                                                    <option value="88">Kiribati</option>
	                                                                    <option value="89">Korea (North)</option>
	                                                                    <option value="90">Korea (South)</option>
	                                                                    <option value="91">Kuwait</option>
	                                                                    <option value="92">Kyrgyzstan</option>
	                                                                    <option value="93">Laos</option>
	                                                                    <option value="94">Latvia</option>
	                                                                    <option value="95">Lebanon</option>
	                                                                    <option value="96">Lesotho</option>
	                                                                    <option value="97">Liberia</option>
	                                                                    <option value="98">Libya</option>
	                                                                    <option value="99">Liechtenstein</option>
	                                                                    <option value="100">Lithuania</option>
	                                                                    <option value="101">Luxembourg</option>
	                                                                    <option value="102">Macedonia</option>
	                                                                    <option value="103">Madagascar</option>
	                                                                    <option value="104">Malawi</option>
	                                                                    <option value="105">Malaysia</option>
	                                                                    <option value="106">Maldives</option>
	                                                                    <option value="107">Mali</option>
	                                                                    <option value="108">Malta</option>
	                                                                    <option value="109">Marshall Islands</option>
	                                                                    <option value="110">Mauritania</option>
	                                                                    <option value="111">Mauritius</option>
	                                                                    <option value="112">Mexico</option>
	                                                                    <option value="113">Micronesia</option>
	                                                                    <option value="114">Moldova</option>
	                                                                    <option value="115">Monaco</option>
	                                                                    <option value="116">Mongolia</option>
	                                                                    <option value="117">Montenegro</option>
	                                                                    <option value="118">Morocco</option>
	                                                                    <option value="119">Mozambique</option>
	                                                                    <option value="120">Myanmar (Burma)</option>
	                                                                    <option value="121">Namibia</option>
	                                                                    <option value="122">Nauru</option>
	                                                                    <option value="123">Nepal</option>
	                                                                    <option value="124">Netherlands</option>
	                                                                    <option value="125">New Zealand</option>
	                                                                    <option value="126">Nicaragua</option>
	                                                                    <option value="127">Niger</option>
	                                                                    <option value="128">Nigeria</option>
	                                                                    <option value="129">Norway</option>
	                                                                    <option value="130">Oman</option>
	                                                                    <option value="131">Pakistan</option>
	                                                                    <option value="132">Palau</option>
	                                                                    <option value="133">Panama</option>
	                                                                    <option value="134">Papua New Guinea</option>
	                                                                    <option value="135">Paraguay</option>
	                                                                    <option value="136">Peru</option>
	                                                                    <option value="137">Philippines</option>
	                                                                    <option value="138">Poland</option>
	                                                                    <option value="139">Portugal</option>
	                                                                    <option value="140">Qatar</option>
	                                                                    <option value="141">Romania</option>
	                                                                    <option value="142">Russia</option>
	                                                                    <option value="143">Rwanda</option>
	                                                                    <option value="144">Saint Kitts and Nevis</option>
	                                                                    <option value="145">Saint Lucia</option>
	                                                                    <option value="147">Samoa</option>
	                                                                    <option value="148">San Marino</option>
	                                                                    <option value="149">Sao Tome and Principe</option>
	                                                                    <option value="150">Saudi Arabia</option>
	                                                                    <option value="151">Senegal</option>
	                                                                    <option value="152">Serbia</option>
	                                                                    <option value="153">Seychelles</option>
	                                                                    <option value="154">Sierra Leone</option>
	                                                                    <option value="155">Singapore</option>
	                                                                    <option value="156">Slovakia</option>
	                                                                    <option value="157">Slovenia</option>
	                                                                    <option value="158">Solomon Islands</option>
	                                                                    <option value="159">Somalia</option>
	                                                                    <option value="160">South Africa</option>
	                                                                    <option value="161">Spain</option>
	                                                                    <option value="162">Sri Lanka</option>
	                                                                    <option value="163">Sudan</option>
	                                                                    <option value="164">Suriname</option>
	                                                                    <option value="165">Swaziland</option>
	                                                                    <option value="166">Sweden</option>
	                                                                    <option value="167">Switzerland</option>
	                                                                    <option value="168">Syria</option>
	                                                                    <option value="169">Tajikistan</option>
	                                                                    <option value="170">Tanzania</option>
	                                                                    <option value="171">Thailand</option>
	                                                                    <option value="172">Timor-Leste (East Timor)</option>
	                                                                    <option value="173">Togo</option>
	                                                                    <option value="174">Tonga</option>
	                                                                    <option value="175">Trinidad and Tobago</option>
	                                                                    <option value="176">Tunisia</option>
	                                                                    <option value="177">Turkey</option>
	                                                                    <option value="178">Turkmenistan</option>
	                                                                    <option value="179">Tuvalu</option>
	                                                                    <option value="180">Uganda</option>
	                                                                    <option value="181">Ukraine</option>
	                                                                    <option value="182">United Arab Emirates</option>
	                                                                    <option value="183">United Kingdom</option>
	                                                                    <option value="184">United States</option>
	                                                                    <option value="185">Uruguay</option>
	                                                                    <option value="186">Uzbekistan</option>
	                                                                    <option value="187">Vanuatu</option>
	                                                                    <option value="188">Vatican City</option>
	                                                                    <option value="189">Venezuela</option>
	                                                                    <option value="190">Vietnam</option>
	                                                                    <option value="191">Yemen</option>
	                                                                    <option value="192">Zambia</option>
	                                                                    <option value="193">Zimbabwe</option>
	                                                                    <option value="194">Abkhazia</option>
	                                                                    <option value="195">Taiwan</option>
	                                                                    <option value="197">Northern Cyprus</option>
	                                                                    <option value="198">Pridnestrovie (Transnistria)</option>
	                                                                    <option value="200">South Ossetia</option>
	                                                                    <option value="202">Christmas Island</option>
	                                                                    <option value="203">Cocos (Keeling) Islands</option>
	                                                                    <option value="206">Norfolk Island</option>
	                                                                    <option value="207">New Caledonia</option>
	                                                                    <option value="208">French Polynesia</option>
	                                                                    <option value="209">Mayotte</option>
	                                                                    <option value="210">Saint Barthelemy</option>
	                                                                    <option value="211">Saint Martin</option>
	                                                                    <option value="212">Saint Pierre and Miquelon</option>
	                                                                    <option value="213">Wallis and Futuna</option>
	                                                                    <option value="216">Bouvet Island</option>
	                                                                    <option value="217">Cook Islands</option>
	                                                                    <option value="218">Niue</option>
	                                                                    <option value="219">Tokelau</option>
	                                                                    <option value="220">Guernsey</option>
	                                                                    <option value="221">Isle of Man</option>
	                                                                    <option value="222">Jersey</option>
	                                                                    <option value="223">Anguilla</option>
	                                                                    <option value="224">Bermuda</option>
	                                                                    <option value="227">British Virgin Islands</option>
	                                                                    <option value="228">Cayman Islands</option>
	                                                                    <option value="230">Gibraltar</option>
	                                                                    <option value="231">Montserrat</option>
	                                                                    <option value="232">Pitcairn Islands</option>
	                                                                    <option value="233">Saint Helena</option>
	                                                                    <option value="235">Turks and Caicos Islands</option>
	                                                                    <option value="236">Northern Mariana Islands</option>
	                                                                    <option value="237">Puerto Rico</option>
	                                                                    <option value="238">American Samoa</option>
	                                                                    <option value="240">Guam</option>
	                                                                    <option value="248">U.S. Virgin Islands</option>
	                                                                    <option value="250">Hong Kong</option>
	                                                                    <option value="251">Macau</option>
	                                                                    <option value="252">Faroe Islands</option>
	                                                                    <option value="253">Greenland</option>
	                                                                    <option value="254">French Guiana</option>
	                                                                    <option value="255">Guadeloupe</option>
	                                                                    <option value="256">Martinique</option>
	                                                                    <option value="257">Reunion</option>
	                                                                    <option value="258">Aland Islands</option>
	                                                                    <option value="259">Aruba</option>
	                                                                    <option value="260">Netherlands Antilles</option>
	                                                                    <option value="261">Svalbard</option>
	                                                                    <option value="264">Antarctica Territories</option>
	                                                                    <option value="265">Kosovo</option>
	                                                                    <option value="266">Palestinian Territories</option>
	                                                                    <option value="267">Western Sahara</option>
	                                                                    <option value="273">Sint Maarten</option>
	                                                                    <option value="274">South Sudan</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td style="width:18px;">&nbsp;</td>
                                                            <td class="style__width_186"><div><input required class="aa_input datePickerDOB" id="bus_dob" name="bus_dob" placeholder="DD/MM/YYYY" style="width:100%;" type="text" readonly="readonly" value="" /></div></td>
                                                            <td style="width:18px;"class="style__width_21">&nbsp;</td>
                                                            <td class="style__width_186"><div><input required class="aa_input" id="bus_placeofbirth" name="bus_placeofbirth" style="width:100%;" type="text" value=""  /></div></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                    <table class="tbl_width_560">
                        <tr>
                            <td class="aa_label_font">NATIONALITY *</td>
                            <td class="aa_label_font">&nbsp;</td>
                            <td class="aa_label_font">ALSO KNOWN AS (AKA)</td>
                        </tr>
                        <tr>
                            <td class="style__width_186">
                                <div>
                                    <select id="bus_nationality" name="bus_nationality" required style="width:100%;">
	                                    <option selected="selected" value="0">--Select--</option>
	                                    <option value="1">Afghani</option>
	                                    <option value="2">Albanian</option>
	                                    <option value="3">Algerian</option>
	                                    <option value="4">American</option>
	                                    <option value="5">Andorran</option>
	                                    <option value="6">Angolan</option>
	                                    <option value="202">Anguillan</option>
	                                    <option value="203">Antarctic</option>
	                                    <option value="7">Antiguans</option>
	                                    <option value="8">Argentinean</option>
	                                    <option value="9">Armenian</option>
	                                    <option value="204">Arubian</option>
	                                    <option value="10">Australian</option>
	                                    <option value="11">Austrian</option>
	                                    <option value="12">Azerbaijani</option>
	                                    <option value="208">Bahameese</option>
	                                    <option value="13">Bahamian</option>
	                                    <option value="14">Bahraini</option>
	                                    <option value="15">Bangladeshi</option>
	                                    <option value="16">Barbadian</option>
	                                    <option value="17">Barbudans</option>
	                                    <option value="206">Barthlemois</option>
	                                    <option value="19">Belarusian</option>
	                                    <option value="20">Belgian</option>
	                                    <option value="21">Belizean</option>
	                                    <option value="22">Beninese</option>
	                                    <option value="207">Bermudan</option>
	                                    <option value="23">Bhutanese</option>
	                                    <option value="24">Bolivian</option>
	                                    <option value="25">Bosnian</option>
	                                    <option value="26">Brazilian</option>
	                                    <option value="27">British</option>
	                                    <option value="28">Bruneian</option>
	                                    <option value="29">Bulgarian</option>
	                                    <option value="30">Burkinabe</option>
	                                    <option value="31">Burmese</option>
	                                    <option value="32">Burundian</option>
	                                    <option value="33">Cambodian</option>
	                                    <option value="34">Cameroonian</option>
	                                    <option value="35">Canadian</option>
	                                    <option value="36">Cape Verdean</option>
	                                    <option value="224">Caymanian</option>
	                                    <option value="37">Central African</option>
	                                    <option value="38">Chadian</option>
	                                    <option value="39">Chilean</option>
	                                    <option value="40">Chinese</option>
	                                    <option value="212">Christmas Islander</option>
	                                    <option value="209">Cocossian</option>
	                                    <option value="41">Colombian</option>
	                                    <option value="42">Comoran</option>
	                                    <option value="43">Congolese</option>
	                                    <option value="210">Cook Islander</option>
	                                    <option value="45">Costa Rican</option>
	                                    <option value="46">Croatian</option>
	                                    <option value="47">Cuban</option>
	                                    <option value="211">Curaaoan</option>
	                                    <option value="48">Cypriot</option>
	                                    <option value="49">Czech</option>
	                                    <option value="50">Danish</option>
	                                    <option value="51">Djiboutian</option>
	                                    <option value="52">Dominican</option>
	                                    <option value="54">Dutch</option>
	                                    <option value="56">Dutchwoman</option>
	                                    <option value="58">Ecuadorean</option>
	                                    <option value="59">Egyptian</option>
	                                    <option value="60">Emirian</option>
	                                    <option value="61">Equatorial Guinean</option>
	                                    <option value="62">Eritrean</option>
	                                    <option value="63">Estonian</option>
	                                    <option value="64">Ethiopian</option>
	                                    <option value="214">Falkland Islander</option>
	                                    <option value="215">Faroese</option>
	                                    <option value="65">Fijian</option>
	                                    <option value="66">Filipino</option>
	                                    <option value="67">Finnish</option>
	                                    <option value="68">French</option>
	                                    <option value="216">French Guianese</option>
	                                    <option value="233">French Polynesian</option>
	                                    <option value="69">Gabonese</option>
	                                    <option value="70">Gambian</option>
	                                    <option value="71">Georgian</option>
	                                    <option value="72">German</option>
	                                    <option value="73">Ghanaian</option>
	                                    <option value="217">Gibralterian</option>
	                                    <option value="74">Greek</option>
	                                    <option value="218">Greenlander</option>
	                                    <option value="75">Grenadian</option>
	                                    <option value="219">Guadeloupean</option>
	                                    <option value="220">Guamanian</option>
	                                    <option value="76">Guatemalan</option>
	                                    <option value="77">Guinean</option>
	                                    <option value="79">Guyanese</option>
	                                    <option value="80">Haitian</option>
	                                    <option value="81">Herzegovinian</option>
	                                    <option value="82">Honduran</option>
	                                    <option value="221">Hong Konger</option>
	                                    <option value="83">Hungarian</option>
	                                    <option value="84">I-Kiribati</option>
	                                    <option value="85">Icelander</option>
	                                    <option value="86">Indian</option>
	                                    <option value="87">Indonesian</option>
	                                    <option value="88">Iranian</option>
	                                    <option value="89">Iraqi</option>
	                                    <option value="91">Irish</option>
	                                    <option value="92">Israeli</option>
	                                    <option value="93">Italian</option>
	                                    <option value="94">Ivorian</option>
	                                    <option value="95">Jamaican</option>
	                                    <option value="96">Japanese</option>
	                                    <option value="97">Jordanian</option>
	                                    <option value="98">Kazakhstani</option>
	                                    <option value="99">Kenyan</option>
	                                    <option value="100">Kittian and Nevisian</option>
	                                    <option value="101">Kuwaiti</option>
	                                    <option value="102">Kyrgyz</option>
	                                    <option value="223">Kyrgyzstani</option>
	                                    <option value="103">Laotian</option>
	                                    <option value="104">Latvian</option>
	                                    <option value="105">Lebanese</option>
	                                    <option value="106">Liberian</option>
	                                    <option value="107">Libyan</option>
	                                    <option value="108">Liechtensteiner</option>
	                                    <option value="109">Lithuanian</option>
	                                    <option value="110">Luxembourger</option>
	                                    <option value="226">Macanese</option>
	                                    <option value="111">Macedonian</option>
	                                    <option value="246">Mahoran</option>
	                                    <option value="112">Malagasy</option>
	                                    <option value="113">Malawian</option>
	                                    <option value="114">Malaysian</option>
	                                    <option value="115">Maldivan</option>
	                                    <option value="116">Malian</option>
	                                    <option value="117">Maltese</option>
	                                    <option value="222">Manx</option>
	                                    <option value="118">Marshallese</option>
	                                    <option value="228">Martinican</option>
	                                    <option value="119">Mauritanian</option>
	                                    <option value="120">Mauritian</option>
	                                    <option value="121">Mexican</option>
	                                    <option value="122">Micronesian</option>
	                                    <option value="123">Moldovan</option>
	                                    <option value="124">Monacan</option>
	                                    <option value="125">Mongolian</option>
	                                    <option value="225">Montenegrin</option>
	                                    <option value="229">Montserratian</option>
	                                    <option value="126">Moroccan</option>
	                                    <option value="127">Mosotho</option>
	                                    <option value="18">Motswana</option>
	                                    <option value="128">Motswana</option>
	                                    <option value="129">Mozambican</option>
	                                    <option value="130">Namibian</option>
	                                    <option value="131">Nauruan</option>
	                                    <option value="132">Nepalese</option>
	                                    <option value="230">New Caledonian</option>
	                                    <option value="134">New Zealander</option>
	                                    <option value="135">Ni-Vanuatu</option>
	                                    <option value="136">Nicaraguan</option>
	                                    <option value="137">Nigerian</option>
	                                    <option value="138">Nigerien</option>
	                                    <option value="232">Niuean</option>
	                                    <option value="231">Norfolk Islander</option>
	                                    <option value="139">North Korean</option>
	                                    <option value="140">Northern Irish</option>
	                                    <option value="227">Northern Mariana Islander</option>
	                                    <option value="141">Norwegian</option>
	                                    <option value="142">Omani</option>
	                                    <option value="143">Pakistani</option>
	                                    <option value="144">Palauan</option>
	                                    <option value="237">Palestinian</option>
	                                    <option value="145">Panamanian</option>
	                                    <option value="146">Papua New Guinean</option>
	                                    <option value="147">Paraguayan</option>
	                                    <option value="148">Peruvian</option>
	                                    <option value="235">Pitcairn Islander</option>
	                                    <option value="149">Polish</option>
	                                    <option value="150">Portuguese</option>
	                                    <option value="236">Puerto Rican</option>
	                                    <option value="151">Qatari</option>
	                                    <option value="152">Romanian</option>
	                                    <option value="153">Russian</option>
	                                    <option value="154">Rwandan</option>
	                                    <option value="238">Saint Helenian</option>
	                                    <option value="155">Saint Lucian</option>
	                                    <option value="242">Saint Vincentian</option>
	                                    <option value="234">Saint-Pierrais</option>
	                                    <option value="156">Salvadoran</option>
	                                    <option value="157">Samoan</option>
	                                    <option value="158">San Marinese</option>
	                                    <option value="159">Sao Tomean</option>
	                                    <option value="160">Saudi</option>
	                                    <option value="161">Scottish</option>
	                                    <option value="162">Senegalese</option>
	                                    <option value="163">Serbian</option>
	                                    <option value="164">Seychellois</option>
	                                    <option value="165">Sierra Leonean</option>
	                                    <option value="166">Singaporean</option>
	                                    <option value="167">Slovakian</option>
	                                    <option value="168">Slovenian</option>
	                                    <option value="169">Solomon Islander</option>
	                                    <option value="170">Somali</option>
	                                    <option value="171">South African</option>
	                                    <option value="172">South Korean</option>
	                                    <option value="173">Spanish</option>
	                                    <option value="174">Sri Lankan</option>
	                                    <option value="175">Sudanese</option>
	                                    <option value="176">Surinamer</option>
	                                    <option value="177">Swazi</option>
	                                    <option value="178">Swedish</option>
	                                    <option value="179">Swiss</option>
	                                    <option value="180">Syrian</option>
	                                    <option value="181">Taiwanese</option>
	                                    <option value="182">Tajik</option>
	                                    <option value="183">Tanzanian</option>
	                                    <option value="184">Thai</option>
	                                    <option value="247">Tibetan </option>
	                                    <option value="57">Timorese</option>
	                                    <option value="185">Togolese</option>
	                                    <option value="240">Tokelauan</option>
	                                    <option value="186">Tongan</option>
	                                    <option value="187">Trinidadian or Tobagonian</option>
	                                    <option value="188">Tunisian</option>
	                                    <option value="189">Turkish</option>
	                                    <option value="241">Turkmen</option>
	                                    <option value="239">Turks and Caicos Islander</option>
	                                    <option value="190">Tuvaluan</option>
	                                    <option value="191">Ugandan</option>
	                                    <option value="192">Ukrainian</option>
	                                    <option value="193">Uruguayan</option>
	                                    <option value="194">Uzbekistani</option>
	                                    <option value="195">Venezuelan</option>
	                                    <option value="196">Vietnamese</option>
	                                    <option value="243">Virgin Islander-UK</option>
	                                    <option value="244">Virgin Islander-US</option>
	                                    <option value="245">Wallisian</option>
	                                    <option value="198">Welsh</option>
	                                    <option value="213">Western Saharan</option>
	                                    <option value="199">Yemenese</option>
	                                    <option value="200">Zambian</option>
	                                    <option value="201">Zimbabwean</option>
	                                    <option value="205">landic</option>
                                    </select>
                                </div>
                            </td>
                            <td style="width:18px;">&nbsp;</td>
                            <td class="style__width_393"><div><input class="aa_input" id="bus_aka" name="bus_aka" style="width:100%;" type="text"  value="" /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>

                <tr class="spacer10"><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td class="aa_label_font">OCCUPATION</td>
                                    </tr>
                                    <tr>
                                        <td class="style__width_600"><div><input class="aa_input" id="bus_occupation" name="bus_occupation" style="width:100%;" type="text" value="" /></div></td>
                                    </tr>
                                    <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                    <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>

                <tr style="background-color:#EFEFEF;" hidden="hidden">
                    <td>
                    <table class="tbl_width_560" >
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td class="aa_label_font">CURRENT ADDRESS (Type address here to auto fill)</td>
                        </tr>
                        <tr hidden="hidden">
                            <td class="style__width_600"><div><input class="aa_input" id="bus_full_address" name="bus_full_address" style="width:100%;" type="text"  value="" <!--onFocus="geolocate()-->"></input></div></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                    </td>
                </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">UNIT NO</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET NO *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET NAME *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STREET TYPE *</td>
                                        </tr>
                                        <tr>
                                            <td class="style__width_50"><div><input class="aa_input" id="pbus_unitno" name="pbus_unitno" style="width:100%;" type="text" value=""  /></div></td>
                                            <td class="style__width_10">&nbsp;</td>
                                            <td class="style__width_75"><div><input class="aa_input" required id="pbus_streetno" name="pbus_streetno" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" required id="pbus_streetname" name="pbus_streetname" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135">
                                                <div>
                                        
                                                    <select id="pbus_streettype" name="pbus_streettype" required style="width:100%;">
                                                         <option value="0">--Select--</option>
	                                                        <option value="Avenue">Avenue</option>
	                                                        <option value="Access">Access</option>
	                                                        <option value="Alley">Alley</option>
	                                                        <option value="Alleyway">Alleyway</option>
	                                                        <option value="Amble">Amble</option>
	                                                        <option value="Anchorage">Anchorage</option>
	                                                        <option value="Approach">Approach</option>
	                                                        <option value="Arcade">Arcade</option>
	                                                        <option value="Artery">Artery</option>
	                                                        <option value="Boulevard">Boulevard</option>
	                                                        <option value="Bank">Bank</option>
	                                                        <option value="Basin">Basin</option>
	                                                        <option value="Beach">Beach</option>
	                                                        <option value="Bend">Bend</option>
	                                                        <option value="Block">Block</option>
	                                                        <option value="Bowl">Bowl</option>
	                                                        <option value="Brace">Brace</option>
	                                                        <option value="Brae">Brae</option>
	                                                        <option value="Break">Break</option>
	                                                        <option value="Broadway">Broadway</option>
	                                                        <option value="Brow">Brow</option>
	                                                        <option value="Bypass">Bypass</option>
	                                                        <option value="Byway">Byway</option>
	                                                        <option value="Crescent">Crescent</option>
	                                                        <option value="Causeway">Causeway</option>
	                                                        <option value="Centre">Centre</option>
	                                                        <option value="Centreway">Centreway</option>
	                                                        <option value="Chase">Chase</option>
	                                                        <option value="Circle">Circle</option>
	                                                        <option value="Circlet">Circlet</option>
	                                                        <option value="Circuit">Circuit</option>
	                                                        <option value="Circus">Circus</option>
	                                                        <option value="Close">Close</option>
	                                                        <option value="Colonnade">Colonnade</option>
	                                                        <option value="Common">Common</option>
	                                                        <option value="Concourse">Concourse</option>
	                                                        <option value="Copse">Copse</option>
	                                                        <option value="Corner">Corner</option>
	                                                        <option value="Corso">Corso</option>
	                                                        <option value="Court">Court</option>
	                                                        <option value="Courtyard">Courtyard</option>
	                                                        <option value="Cove">Cove</option>
	                                                        <option value="Crossing">Crossing</option>
	                                                        <option value="Crossroad">Crossroad</option>
	                                                        <option value="Crossway">Crossway</option>
	                                                        <option value="Cruiseway">Cruiseway</option>
	                                                        <option value="Cul-de-sac">Cul-de-sac</option>
	                                                        <option value="Cutting">Cutting</option>
	                                                        <option value="Drive">Drive</option>
	                                                        <option value="Dale">Dale</option>
	                                                        <option value="Dell">Dell</option>
	                                                        <option value="Deviation">Deviation</option>
	                                                        <option value="Dip">Dip</option>
	                                                        <option value="Distributor">Distributor</option>
	                                                        <option value="Driveway">Driveway</option>
	                                                        <option value="Entrance">Entrance</option>
	                                                        <option value="Edge">Edge</option>
	                                                        <option value="Elbow">Elbow</option>
	                                                        <option value="End">End</option>
	                                                        <option value="Esplanade">Esplanade</option>
	                                                        <option value="Estate">Estate</option>
	                                                        <option value="Expressway">Expressway</option>
	                                                        <option value="Extension">Extension</option>
	                                                        <option value="Fairway">Fairway</option>
	                                                        <option value="Fire Track">Fire Track</option>
	                                                        <option value="Firetrail">Firetrail</option>
	                                                        <option value="Flat">Flat</option>
	                                                        <option value="Follow">Follow</option>
	                                                        <option value="Footway">Footway</option>
	                                                        <option value="Foreshore">Foreshore</option>
	                                                        <option value="Formation">Formation</option>
	                                                        <option value="Freeway">Freeway</option>
	                                                        <option value="Front">Front</option>
	                                                        <option value="Frontage">Frontage</option>
	                                                        <option value="Garden">Garden</option>
	                                                        <option value="Gap">Gap</option>
	                                                        <option value="Gardens">Gardens</option>
	                                                        <option value="Gate">Gate</option>
	                                                        <option value="Gates">Gates</option>
	                                                        <option value="Glade">Glade</option>
	                                                        <option value="Glen">Glen</option>
	                                                        <option value="Grange">Grange</option>
	                                                        <option value="Green">Green</option>
	                                                        <option value="Ground">Ground</option>
	                                                        <option value="Grove">Grove</option>
	                                                        <option value="Gulley">Gulley</option>
	                                                        <option value="Heights">Heights</option>
	                                                        <option value="Highroad">Highroad</option>
	                                                        <option value="Highway">Highway</option>
	                                                        <option value="Hill">Hill</option>
	                                                        <option value="Interchange">Interchange</option>
	                                                        <option value="Intersection">Intersection</option>
	                                                        <option value="Junction">Junction</option>
	                                                        <option value="Key">Key</option>
	                                                        <option value="Lane">Lane</option>
	                                                        <option value="Landing">Landing</option>
	                                                        <option value="Laneway">Laneway</option>
	                                                        <option value="Lees">Lees</option>
	                                                        <option value="Line">Line</option>
	                                                        <option value="Link">Link</option>
	                                                        <option value="Little">Little</option>
	                                                        <option value="Lookout">Lookout</option>
	                                                        <option value="Loop">Loop</option>
	                                                        <option value="Lower">Lower</option>
	                                                        <option value="Mews">Mews</option>
	                                                        <option value="Mall">Mall</option>
	                                                        <option value="Meander">Meander</option>
	                                                        <option value="Mew">Mew</option>
	                                                        <option value="Mile">Mile</option>
	                                                        <option value="Motorway">Motorway</option>
	                                                        <option value="Mount">Mount</option>
	                                                        <option value="Nook">Nook</option>
	                                                        <option value="Outlook">Outlook</option>
	                                                        <option value="Place">Place</option>
	                                                        <option value="Parade">Parade</option>
	                                                        <option value="Park">Park</option>
	                                                        <option value="Parklands">Parklands</option>
	                                                        <option value="Parkway">Parkway</option>
	                                                        <option value="Part">Part</option>
	                                                        <option value="Pass">Pass</option>
	                                                        <option value="Path">Path</option>
	                                                        <option value="Piazza">Piazza</option>
	                                                        <option value="Pier">Pier</option>
	                                                        <option value="Plateau">Plateau</option>
	                                                        <option value="Plaza">Plaza</option>
	                                                        <option value="Pocket">Pocket</option>
	                                                        <option value="Point">Point</option>
	                                                        <option value="Port">Port</option>
	                                                        <option value="Promenade">Promenade</option>
	                                                        <option value="Quadrant">Quadrant</option>
	                                                        <option value="Quad">Quad</option>
	                                                        <option value="Quadrangle">Quadrangle</option>
	                                                        <option value="Quay">Quay</option>
	                                                        <option value="Quays">Quays</option>
	                                                        <option value="Road">Road</option>
	                                                        <option value="Ramble">Ramble</option>
	                                                        <option value="Ramp">Ramp</option>
	                                                        <option value="Range">Range</option>
	                                                        <option value="Reach">Reach</option>
	                                                        <option value="Reserve">Reserve</option>
	                                                        <option value="Rest">Rest</option>
	                                                        <option value="Retreat">Retreat</option>
	                                                        <option value="Ride">Ride</option>
	                                                        <option value="Ridge">Ridge</option>
	                                                        <option value="Ridgeway">Ridgeway</option>
	                                                        <option value="Right Of Way">Right Of Way</option>
	                                                        <option value="Ring">Ring</option>
	                                                        <option value="Rise">Rise</option>
	                                                        <option value="River">River</option>
	                                                        <option value="Riverway">Riverway</option>
	                                                        <option value="Riviera">Riviera</option>
	                                                        <option value="Roads">Roads</option>
	                                                        <option value="Roadside">Roadside</option>
	                                                        <option value="Roadway">Roadway</option>
	                                                        <option value="Ronde">Ronde</option>
	                                                        <option value="Rosebowl">Rosebowl</option>
	                                                        <option value="Rotary">Rotary</option>
	                                                        <option value="Round">Round</option>
	                                                        <option value="Route">Route</option>
	                                                        <option value="Row">Row</option>
	                                                        <option value="Rue">Rue</option>
	                                                        <option value="Run">Run</option>
	                                                        <option value="Street">Street</option>
	                                                        <option value="Service">Service</option>
	                                                        <option value="Siding">Siding</option>
	                                                        <option value="Slope">Slope</option>
	                                                        <option value="Sound">Sound</option>
	                                                        <option value="Spur">Spur</option>
	                                                        <option value="Square">Square</option>
	                                                        <option value="Stairs">Stairs</option>
	                                                        <option value="State Highway">State Highway</option>
	                                                        <option value="Steps">Steps</option>
	                                                        <option value="Streets">Streets</option>
	                                                        <option value="Strip">Strip</option>
	                                                        <option value="Terrace">Terrace</option>
	                                                        <option value="Tarnice Way">Tarnice Way</option>
	                                                        <option value="Thoroughfare">Thoroughfare</option>
	                                                        <option value="Tollway">Tollway</option>
	                                                        <option value="Top">Top</option>
	                                                        <option value="Tor">Tor</option>
	                                                        <option value="Towers">Towers</option>
	                                                        <option value="Track">Track</option>
	                                                        <option value="Trail">Trail</option>
	                                                        <option value="Trailer">Trailer</option>
	                                                        <option value="Trunkway">Trunkway</option>
	                                                        <option value="Turn">Turn</option>
	                                                        <option value="Underpass">Underpass</option>
	                                                        <option value="Upper">Upper</option>
	                                                        <option value="View">View</option>
	                                                        <option value="Vale">Vale</option>
	                                                        <option value="Viaduct">Viaduct</option>
	                                                        <option value="Villa">Villa</option>
	                                                        <option value="Vista">Vista</option>
	                                                        <option value="Way">Way</option>
	                                                        <option value="Wade">Wade</option>
	                                                        <option value="Walk">Walk</option>
	                                                        <option value="Walkway">Walkway</option>
	                                                        <option value="Wharf">Wharf</option>
	                                                        <option value="Wynd">Wynd</option>
	                                                        <option value="Yard">Yard</option>
	                                                        <option value="Domain">Domain</option>
	                                                        <option value="Strand">Strand</option>
	                                                        <option value="Boulevarde">Boulevarde</option>

                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr hidden="hidden">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">ADDRESS LINE 1</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">ADDRESS LINE 2</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_route" name="pbus_route" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_addressline2" name="pbus_addressline2" style="width:100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10" hidden="hidden"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">SUBURB *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">STATE *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">POSTCODE *</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input required class="aa_input" id="ppbus_suburb" name="ppbus_suburb" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input required class="aa_input" id="ppbus_state" name="ppbus_state" style="width:100%;" type="text" value=""  /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td class="style__width_135"><div><input required class="aa_input" id="ppbus_postcode" name="ppbus_postcode" style="width:100%;" type="text" value=""  /></div></td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">COUNTRY</td>
                                        </tr>
                                        <tr>
                                            <td><div><input class="aa_input" id="pbus_country" name="pbus_country" style="width:100%;" type="text" value="Australia"  /></div></td>
                                        </tr>
                                        <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                        <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">HOME PHONE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">WORK PHONE</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">MOBILE PHONE *</td>
                                        </tr>
                                        <tr>
                                            <td style="width:175px;"><input class="aa_input" id="pbus_phonehome" name="pbus_phonehome" style="width:100%;" type="text"/></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:174px;"><input class="aa_input" id="pbus_phonework" name="pbus_phonework" style="width:100%;" type="text"/></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:175px;"><input required class="aa_input" id="pbus_phonemobile" name="pbus_phonemobile" style="width:100%;" type="text"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="spacer10"><td>&nbsp;</td></tr>

                            <tr>
                                <td>
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td class="aa_label_font">EMAIL ADDRESS 1 *</td>
                                            <td class="aa_label_font">&nbsp;</td>
                                            <td class="aa_label_font">EMAIL ADDRESS 2</td>
                                        </tr>
                                        <tr>
                                            <td style="width:271px;"><div><input required class="aa_input" id="pbus_email1" name="pbus_email1" style="width:100%;" type="email" value="" /></div></td>
                                            <td style="width:18px;">&nbsp;</td>
                                            <td style="width:271px;"><div><input class="aa_input" id="pbus_email2" name="pbus_email2" style="width:100%;" type="email" value=""  /></div></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr style="height:20px;"><td class="spacer10">&nbsp;</td></tr>

                        </table>
                    </td>
                </tr>
                                </table>
                                   
                            </div>
                    </td>
                </tr>

                <!-- NOTES & PW DIV -->
                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <div id="DIV_EditCustomerNotesPW">
                            <table class="tbl_width_560">
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">

                                            <tr class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_520">
                                                        <tr><td>&nbsp;</td></tr>
                                                        <tr><td class="trn_notes_popup_heading">Notes</td></tr>
                                                        <tr><td class="trn_notes_popup_details" style="font-size:0.65em; color:#999;">These notes will appear at the top of the Customer Profile Page. This will only be visible to the internal staff.</td></tr>
                                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                                        <tr><td class="trn_notes_popup_heading">Password</td></tr>
                                                        <tr><td class="trn_notes_popup_details" style="font-size:0.65em; color:#999;">You can assign a password for the customer Profile if requested by the customer. If there is a password this will appear at the top of the Customer Profile Page. </td></tr>
                                                        <tr><td>&nbsp;</td></tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr><td>&nbsp;</td></tr>

                                            <tr>
								                <td class="aa_label_font">NOTES</td>
							                </tr>
                                            <tr>
                                                <td><textarea name="TXT_Add_TRN_Notes" id="edit_notes"  style="width:100%; height:55px; resize:none; line-height:16px !important; padding-top:8px;" class="aa_input" rows="3" ></textarea></td>
                                            </tr>
						                </table>
                                    </td>
				                </tr>
                            				
                                <tr class="spacer10"><td>&nbsp;</td></tr>
				                
                                <tr>
                                    <td>
                                        <table class="tbl_width_560">
							                <tr>
								                <td class="aa_label_font">PASSWORD</td>
								                <td class="aa_label_font">&nbsp;</td>
								                <td class="aa_label_font">&nbsp;</td>
							                </tr>
							                <tr>
								                <td class="style__width_290"><div><input class="aa_input" name="passwordedit" id="edit_password" style="width:100%;" type="text" value="" /></div></td>
								                <td class="style__width_20">&nbsp;</td>
								                <td class="style__width_290">&nbsp;</td>
							                </tr>
						                </table>
                                    </td>
                                </tr>
                                <tr style="height:20px;" class="wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                            </table>
                        </div>
                    </td>
                </tr>
                
                

				<tr class="spacer10"><td>&nbsp;</td></tr>


				<tr>
					<td>
						<table class="tbl_width_600">
							<tr>
								<td style="text-align: right;"><input type="text" id="Text1" name="hidden_agentid" runat="server" hidden="hidden" />
                                    <input class="aa_btn_red" id="btn_Submit" type="button" value="Update" onclick="EditBusiness()" />
                                    <%--<asp:Button runat="server" Text="Update"  CssClass="aa-btn_red" OnClick="btnSubmit_Click" OnClientClick="return true;" ID="btnSubmit" />--%>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td class="spacer10">&nbsp;</td>
				</tr>

			</table>
		 </form>
           


		</div>
	</div>
     <div id="AddBeneficiaryDiv">
		<div class="edit__form__main">
			<form method="get" id="beneficiaryaddform">
			<table class="tbl_width_600">
				
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image27" runat="server" ImageUrl="~/images/beneficiary.png" title="BENEFICIARY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">LAST NAME *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">GIVEN NAME/S *</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="AddBene_lastname" name="lastname" style="width:100%;" type="text" value="" required="required" /></div></td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_firstnames" name="firstnames" style="width:100%;" type="text"  value="" required="required" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">COUNTRY OF BIRTH *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">DATE OF BIRTH (DOB)</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">PLACE OF BIRTH</td>
						    </tr>
						    <tr>
							    <td style="width:175px;">
								    <div>
									    <select id="AddBene_countryofbirth" style="width:100%;" name="D5">
										    <option selected="selected" value="">--Select--</option>
										    <option value="1">Afghanistan</option>
										    <option value="2">Albania</option>
										    <option value="3">Algeria</option>
										    <option value="4">Andorra</option>
										    <option value="5">Angola</option>
										    <option value="6">Antigua and Barbuda</option>
										    <option value="7">Argentina</option>
										    <option value="8">Armenia</option>
										    <option value="9">Australia</option>
										    <option value="10">Austria</option>
										    <option value="11">Azerbaijan</option>
										    <option value="12">Bahamas</option>
										    <option value="13">Bahrain</option>
										    <option value="14">Bangladesh</option>
										    <option value="15">Barbados</option>
										    <option value="16">Belarus</option>
										    <option value="17">Belgium</option>
										    <option value="18">Belize</option>
										    <option value="19">Benin</option>
										    <option value="20">Bhutan</option>
										    <option value="21">Bolivia</option>
										    <option value="22">Bosnia and Herzegovina</option>
										    <option value="23">Botswana</option>
										    <option value="24">Brazil</option>
										    <option value="25">Brunei</option>
										    <option value="26">Bulgaria</option>
										    <option value="27">Burkina Faso</option>
										    <option value="28">Burundi</option>
										    <option value="29">Cambodia</option>
										    <option value="30">Cameroon</option>
										    <option value="31">Canada</option>
										    <option value="32">Cape Verde</option>
										    <option value="33">Central African Republic</option>
										    <option value="34">Chad</option>
										    <option value="35">Chile</option>
										    <option value="36">China</option>
										    <option value="37">Colombia</option>
										    <option value="38">Comoros</option>
										    <option value="39">Congo (Kinshasa)</option>
										    <option value="40">Congo (Brazzaville)</option>
										    <option value="41">Costa Rica</option>
										    <option value="42">Cote d Ivoire (Ivory Coast)</option>
										    <option value="43">Croatia</option>
										    <option value="44">Cuba</option>
										    <option value="45">Cyprus</option>
										    <option value="46">Czech Republic</option>
										    <option value="47">Denmark</option>
										    <option value="48">Djibouti</option>
										    <option value="49">Dominica</option>
										    <option value="50">Dominican Republic</option>
										    <option value="51">Ecuador</option>
										    <option value="52">Egypt</option>
										    <option value="53">El Salvador</option>
										    <option value="54">Equatorial Guinea</option>
										    <option value="55">Eritrea</option>
										    <option value="56">Estonia</option>
										    <option value="57">Ethiopia</option>
										    <option value="58">Fiji</option>
										    <option value="59">Finland</option>
										    <option value="60">France</option>
										    <option value="61">Gabon</option>
										    <option value="62">Gambia</option>
										    <option value="63">Georgia</option>
										    <option value="64">Germany</option>
										    <option value="65">Ghana</option>
										    <option value="66">Greece</option>
										    <option value="67">Grenada</option>
										    <option value="68">Guatemala</option>
										    <option value="69">Guinea</option>
										    <option value="70">Guinea-Bissau</option>
										    <option value="71">Guyana</option>
										    <option value="72">Haiti</option>
										    <option value="73">Honduras</option>
										    <option value="74">Hungary</option>
										    <option value="75">Iceland</option>
										    <option value="76">India</option>
										    <option value="77">Indonesia</option>
										    <option value="78">Iran</option>
										    <option value="79">Iraq</option>
										    <option value="80">Ireland</option>
										    <option value="81">Israel</option>
										    <option value="82">Italy</option>
										    <option value="83">Jamaica</option>
										    <option value="84">Japan</option>
										    <option value="85">Jordan</option>
										    <option value="86">Kazakhstan</option>
										    <option value="87">Kenya</option>
										    <option value="88">Kiribati</option>
										    <option value="89">Korea (North)</option>
										    <option value="90">Korea (South)</option>
										    <option value="91">Kuwait</option>
										    <option value="92">Kyrgyzstan</option>
										    <option value="93">Laos</option>
										    <option value="94">Latvia</option>
										    <option value="95">Lebanon</option>
										    <option value="96">Lesotho</option>
										    <option value="97">Liberia</option>
										    <option value="98">Libya</option>
										    <option value="99">Liechtenstein</option>
										    <option value="100">Lithuania</option>
										    <option value="101">Luxembourg</option>
										    <option value="102">Macedonia</option>
										    <option value="103">Madagascar</option>
										    <option value="104">Malawi</option>
										    <option value="105">Malaysia</option>
										    <option value="106">Maldives</option>
										    <option value="107">Mali</option>
										    <option value="108">Malta</option>
										    <option value="109">Marshall Islands</option>
										    <option value="110">Mauritania</option>
										    <option value="111">Mauritius</option>
										    <option value="112">Mexico</option>
										    <option value="113">Micronesia</option>
										    <option value="114">Moldova</option>
										    <option value="115">Monaco</option>
										    <option value="116">Mongolia</option>
										    <option value="117">Montenegro</option>
										    <option value="118">Morocco</option>
										    <option value="119">Mozambique</option>
										    <option value="120">Myanmar (Burma)</option>
										    <option value="121">Namibia</option>
										    <option value="122">Nauru</option>
										    <option value="123">Nepal</option>
										    <option value="124">Netherlands</option>
										    <option value="125">New Zealand</option>
										    <option value="126">Nicaragua</option>
										    <option value="127">Niger</option>
										    <option value="128">Nigeria</option>
										    <option value="129">Norway</option>
										    <option value="130">Oman</option>
										    <option value="131">Pakistan</option>
										    <option value="132">Palau</option>
										    <option value="133">Panama</option>
										    <option value="134">Papua New Guinea</option>
										    <option value="135">Paraguay</option>
										    <option value="136">Peru</option>
										    <option value="137">Philippines</option>
										    <option value="138">Poland</option>
										    <option value="139">Portugal</option>
										    <option value="140">Qatar</option>
										    <option value="141">Romania</option>
										    <option value="142">Russia</option>
										    <option value="143">Rwanda</option>
										    <option value="144">Saint Kitts and Nevis</option>
										    <option value="145">Saint Lucia</option>
										    <option value="147">Samoa</option>
										    <option value="148">San Marino</option>
										    <option value="149">Sao Tome and Principe</option>
										    <option value="150">Saudi Arabia</option>
										    <option value="151">Senegal</option>
										    <option value="152">Serbia</option>
										    <option value="153">Seychelles</option>
										    <option value="154">Sierra Leone</option>
										    <option value="155">Singapore</option>
										    <option value="156">Slovakia</option>
										    <option value="157">Slovenia</option>
										    <option value="158">Solomon Islands</option>
										    <option value="159">Somalia</option>
										    <option value="160">South Africa</option>
										    <option value="161">Spain</option>
										    <option value="162">Sri Lanka</option>
										    <option value="163">Sudan</option>
										    <option value="164">Suriname</option>
										    <option value="165">Swaziland</option>
										    <option value="166">Sweden</option>
										    <option value="167">Switzerland</option>
										    <option value="168">Syria</option>
										    <option value="169">Tajikistan</option>
										    <option value="170">Tanzania</option>
										    <option value="171">Thailand</option>
										    <option value="172">Timor-Leste (East Timor)</option>
										    <option value="173">Togo</option>
										    <option value="174">Tonga</option>
										    <option value="175">Trinidad and Tobago</option>
										    <option value="176">Tunisia</option>
										    <option value="177">Turkey</option>
										    <option value="178">Turkmenistan</option>
										    <option value="179">Tuvalu</option>
										    <option value="180">Uganda</option>
										    <option value="181">Ukraine</option>
										    <option value="182">United Arab Emirates</option>
										    <option value="183">United Kingdom</option>
										    <option value="184">United States</option>
										    <option value="185">Uruguay</option>
										    <option value="186">Uzbekistan</option>
										    <option value="187">Vanuatu</option>
										    <option value="188">Vatican City</option>
										    <option value="189">Venezuela</option>
										    <option value="190">Vietnam</option>
										    <option value="191">Yemen</option>
										    <option value="192">Zambia</option>
										    <option value="193">Zimbabwe</option>
										    <option value="194">Abkhazia</option>
										    <option value="195">Taiwan</option>
										    <option value="197">Northern Cyprus</option>
										    <option value="198">Pridnestrovie (Transnistria)</option>
										    <option value="200">South Ossetia</option>
										    <option value="202">Christmas Island</option>
										    <option value="203">Cocos (Keeling) Islands</option>
										    <option value="206">Norfolk Island</option>
										    <option value="207">New Caledonia</option>
										    <option value="208">French Polynesia</option>
										    <option value="209">Mayotte</option>
										    <option value="210">Saint Barthelemy</option>
										    <option value="211">Saint Martin</option>
										    <option value="212">Saint Pierre and Miquelon</option>
										    <option value="213">Wallis and Futuna</option>
										    <option value="216">Bouvet Island</option>
										    <option value="217">Cook Islands</option>
										    <option value="218">Niue</option>
										    <option value="219">Tokelau</option>
										    <option value="220">Guernsey</option>
										    <option value="221">Isle of Man</option>
										    <option value="222">Jersey</option>
										    <option value="223">Anguilla</option>
										    <option value="224">Bermuda</option>
										    <option value="227">British Virgin Islands</option>
										    <option value="228">Cayman Islands</option>
										    <option value="230">Gibraltar</option>
										    <option value="231">Montserrat</option>
										    <option value="232">Pitcairn Islands</option>
										    <option value="233">Saint Helena</option>
										    <option value="235">Turks and Caicos Islands</option>
										    <option value="236">Northern Mariana Islands</option>
										    <option value="237">Puerto Rico</option>
										    <option value="238">American Samoa</option>
										    <option value="240">Guam</option>
										    <option value="248">U.S. Virgin Islands</option>
										    <option value="250">Hong Kong</option>
										    <option value="251">Macau</option>
										    <option value="252">Faroe Islands</option>
										    <option value="253">Greenland</option>
										    <option value="254">French Guiana</option>
										    <option value="255">Guadeloupe</option>
										    <option value="256">Martinique</option>
										    <option value="257">Reunion</option>
										    <option value="258">Aland Islands</option>
										    <option value="259">Aruba</option>
										    <option value="260">Netherlands Antilles</option>
										    <option value="261">Svalbard</option>
										    <option value="264">Antarctica Territories</option>
										    <option value="265">Kosovo</option>
										    <option value="266">Palestinian Territories</option>
										    <option value="267">Western Sahara</option>
										    <option value="273">Sint Maarten</option>
										    <option value="274">South Sudan</option>
									    </select>
								    </div>
							    </td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:174px;"><div><input class="aa_input datePickerDOB" id="AddBene_dob" name="dob" placeholder="dd/mm/yyyy" style="width:100%;" readonly="readonly" type="text" value="" /></div></td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:175px;"><div><input class="aa_input" id="Addbene_placeofbirth" name="placeofbirth" style="width:100%;" type="text" value="" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">NATIONALITY *</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">ALSO KNOWN AS (AKA)</td>
						    </tr>
						    <tr>
							    <td style="width:175px;">
								    <div>
									    <select id="AddBene_nationality" required="required" style="width:100%;">
										    <option selected="selected" value="">--Select--</option>
										    <option value="1">Afghani</option>
										    <option value="2">Albanian</option>
										    <option value="3">Algerian</option>
										    <option value="4">American</option>
										    <option value="5">Andorran</option>
										    <option value="6">Angolan</option>
										    <option value="202">Anguillan</option>
										    <option value="203">Antarctic</option>
										    <option value="7">Antiguans</option>
										    <option value="8">Argentinean</option>
										    <option value="9">Armenian</option>
										    <option value="204">Arubian</option>
										    <option value="10">Australian</option>
										    <option value="11">Austrian</option>
										    <option value="12">Azerbaijani</option>
										    <option value="208">Bahameese</option>
										    <option value="13">Bahamian</option>
										    <option value="14">Bahraini</option>
										    <option value="15">Bangladeshi</option>
										    <option value="16">Barbadian</option>
										    <option value="17">Barbudans</option>
										    <option value="206">Barthlemois</option>
										    <option value="19">Belarusian</option>
										    <option value="20">Belgian</option>
										    <option value="21">Belizean</option>
										    <option value="22">Beninese</option>
										    <option value="207">Bermudan</option>
										    <option value="23">Bhutanese</option>
										    <option value="24">Bolivian</option>
										    <option value="25">Bosnian</option>
										    <option value="26">Brazilian</option>
										    <option value="27">British</option>
										    <option value="28">Bruneian</option>
										    <option value="29">Bulgarian</option>
										    <option value="30">Burkinabe</option>
										    <option value="31">Burmese</option>
										    <option value="32">Burundian</option>
										    <option value="33">Cambodian</option>
										    <option value="34">Cameroonian</option>
										    <option value="35">Canadian</option>
										    <option value="36">Cape Verdean</option>
										    <option value="224">Caymanian</option>
										    <option value="37">Central African</option>
										    <option value="38">Chadian</option>
										    <option value="39">Chilean</option>
										    <option value="40">Chinese</option>
										    <option value="212">Christmas Islander</option>
										    <option value="209">Cocossian</option>
										    <option value="41">Colombian</option>
										    <option value="42">Comoran</option>
										    <option value="43">Congolese</option>
										    <option value="210">Cook Islander</option>
										    <option value="45">Costa Rican</option>
										    <option value="46">Croatian</option>
										    <option value="47">Cuban</option>
										    <option value="211">Curaaoan</option>
										    <option value="48">Cypriot</option>
										    <option value="49">Czech</option>
										    <option value="50">Danish</option>
										    <option value="51">Djiboutian</option>
										    <option value="52">Dominican</option>
										    <option value="54">Dutch</option>
										    <option value="56">Dutchwoman</option>
										    <option value="58">Ecuadorean</option>
										    <option value="59">Egyptian</option>
										    <option value="60">Emirian</option>
										    <option value="61">Equatorial Guinean</option>
										    <option value="62">Eritrean</option>
										    <option value="63">Estonian</option>
										    <option value="64">Ethiopian</option>
										    <option value="214">Falkland Islander</option>
										    <option value="215">Faroese</option>
										    <option value="65">Fijian</option>
										    <option value="66">Filipino</option>
										    <option value="67">Finnish</option>
										    <option value="68">French</option>
										    <option value="216">French Guianese</option>
										    <option value="233">French Polynesian</option>
										    <option value="69">Gabonese</option>
										    <option value="70">Gambian</option>
										    <option value="71">Georgian</option>
										    <option value="72">German</option>
										    <option value="73">Ghanaian</option>
										    <option value="217">Gibralterian</option>
										    <option value="74">Greek</option>
										    <option value="218">Greenlander</option>
										    <option value="75">Grenadian</option>
										    <option value="219">Guadeloupean</option>
										    <option value="220">Guamanian</option>
										    <option value="76">Guatemalan</option>
										    <option value="77">Guinean</option>
										    <option value="79">Guyanese</option>
										    <option value="80">Haitian</option>
										    <option value="81">Herzegovinian</option>
										    <option value="82">Honduran</option>
										    <option value="221">Hong Konger</option>
										    <option value="83">Hungarian</option>
										    <option value="84">I-Kiribati</option>
										    <option value="85">Icelander</option>
										    <option value="86">Indian</option>
										    <option value="87">Indonesian</option>
										    <option value="88">Iranian</option>
										    <option value="89">Iraqi</option>
										    <option value="91">Irish</option>
										    <option value="92">Israeli</option>
										    <option value="93">Italian</option>
										    <option value="94">Ivorian</option>
										    <option value="95">Jamaican</option>
										    <option value="96">Japanese</option>
										    <option value="97">Jordanian</option>
										    <option value="98">Kazakhstani</option>
										    <option value="99">Kenyan</option>
										    <option value="100">Kittian and Nevisian</option>
										    <option value="101">Kuwaiti</option>
										    <option value="102">Kyrgyz</option>
										    <option value="223">Kyrgyzstani</option>
										    <option value="103">Laotian</option>
										    <option value="104">Latvian</option>
										    <option value="105">Lebanese</option>
										    <option value="106">Liberian</option>
										    <option value="107">Libyan</option>
										    <option value="108">Liechtensteiner</option>
										    <option value="109">Lithuanian</option>
										    <option value="110">Luxembourger</option>
										    <option value="226">Macanese</option>
										    <option value="111">Macedonian</option>
										    <option value="246">Mahoran</option>
										    <option value="112">Malagasy</option>
										    <option value="113">Malawian</option>
										    <option value="114">Malaysian</option>
										    <option value="115">Maldivan</option>
										    <option value="116">Malian</option>
										    <option value="117">Maltese</option>
										    <option value="222">Manx</option>
										    <option value="118">Marshallese</option>
										    <option value="228">Martinican</option>
										    <option value="119">Mauritanian</option>
										    <option value="120">Mauritian</option>
										    <option value="121">Mexican</option>
										    <option value="122">Micronesian</option>
										    <option value="123">Moldovan</option>
										    <option value="124">Monacan</option>
										    <option value="125">Mongolian</option>
										    <option value="225">Montenegrin</option>
										    <option value="229">Montserratian</option>
										    <option value="126">Moroccan</option>
										    <option value="127">Mosotho</option>
										    <option value="18">Motswana</option>
										    <option value="128">Motswana</option>
										    <option value="129">Mozambican</option>
										    <option value="130">Namibian</option>
										    <option value="131">Nauruan</option>
										    <option value="132">Nepalese</option>
										    <option value="230">New Caledonian</option>
										    <option value="134">New Zealander</option>
										    <option value="135">Ni-Vanuatu</option>
										    <option value="136">Nicaraguan</option>
										    <option value="137">Nigerian</option>
										    <option value="138">Nigerien</option>
										    <option value="232">Niuean</option>
										    <option value="231">Norfolk Islander</option>
										    <option value="139">North Korean</option>
										    <option value="140">Northern Irish</option>
										    <option value="227">Northern Mariana Islander</option>
										    <option value="141">Norwegian</option>
										    <option value="142">Omani</option>
										    <option value="143">Pakistani</option>
										    <option value="144">Palauan</option>
										    <option value="237">Palestinian</option>
										    <option value="145">Panamanian</option>
										    <option value="146">Papua New Guinean</option>
										    <option value="147">Paraguayan</option>
										    <option value="148">Peruvian</option>
										    <option value="235">Pitcairn Islander</option>
										    <option value="149">Polish</option>
										    <option value="150">Portuguese</option>
										    <option value="236">Puerto Rican</option>
										    <option value="151">Qatari</option>
										    <option value="152">Romanian</option>
										    <option value="153">Russian</option>
										    <option value="154">Rwandan</option>
										    <option value="238">Saint Helenian</option>
										    <option value="155">Saint Lucian</option>
										    <option value="242">Saint Vincentian</option>
										    <option value="234">Saint-Pierrais</option>
										    <option value="156">Salvadoran</option>
										    <option value="157">Samoan</option>
										    <option value="158">San Marinese</option>
										    <option value="159">Sao Tomean</option>
										    <option value="160">Saudi</option>
										    <option value="161">Scottish</option>
										    <option value="162">Senegalese</option>
										    <option value="163">Serbian</option>
										    <option value="164">Seychellois</option>
										    <option value="165">Sierra Leonean</option>
										    <option value="166">Singaporean</option>
										    <option value="167">Slovakian</option>
										    <option value="168">Slovenian</option>
										    <option value="169">Solomon Islander</option>
										    <option value="170">Somali</option>
										    <option value="171">South African</option>
										    <option value="172">South Korean</option>
										    <option value="173">Spanish</option>
										    <option value="174">Sri Lankan</option>
										    <option value="175">Sudanese</option>
										    <option value="176">Surinamer</option>
										    <option value="177">Swazi</option>
										    <option value="178">Swedish</option>
										    <option value="179">Swiss</option>
										    <option value="180">Syrian</option>
										    <option value="181">Taiwanese</option>
										    <option value="182">Tajik</option>
										    <option value="183">Tanzanian</option>
										    <option value="184">Thai</option>
										    <option value="247">Tibetan </option>
										    <option value="57">Timorese</option>
										    <option value="185">Togolese</option>
										    <option value="240">Tokelauan</option>
										    <option value="186">Tongan</option>
										    <option value="187">Trinidadian or Tobagonian</option>
										    <option value="188">Tunisian</option>
										    <option value="189">Turkish</option>
										    <option value="241">Turkmen</option>
										    <option value="239">Turks and Caicos Islander</option>
										    <option value="190">Tuvaluan</option>
										    <option value="191">Ugandan</option>
										    <option value="192">Ukrainian</option>
										    <option value="193">Uruguayan</option>
										    <option value="194">Uzbekistani</option>
										    <option value="195">Venezuelan</option>
										    <option value="196">Vietnamese</option>
										    <option value="243">Virgin Islander-UK</option>
										    <option value="244">Virgin Islander-US</option>
										    <option value="245">Wallisian</option>
										    <option value="198">Welsh</option>
										    <option value="213">Western Saharan</option>
										    <option value="199">Yemenese</option>
										    <option value="200">Zambian</option>
										    <option value="201">Zimbabwean</option>
										    <option value="205">landic</option>
									    </select>
								    </div>
							    </td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_aka" name="aka" style="width:100%;" type="text"  value="" /></div></td>
						    </tr>
					    </table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">ID DETAILS</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">RELATIONSHIP WITH SENDER *</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="AddBene_iddetails" name="iddetails" style="width:100%;" type="text" value=""  /></div></td>
							    <td style="width:18px;"></td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_relationship" name="relationshipwithsender" style="width:100%;" type="text"  value="" required="required"  /></div></td>
						    </tr>
                            <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
					    </table>
					</td>
				</tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
					    <table class="tbl_width_560">
						    <tr>
							    <td class="aa_label_font">CONTACT NUMBER</td>
							    <td class="aa_label_font">&nbsp;</td>
							    <td class="aa_label_font">EMAIL ADDRESS</td>
						    </tr>
						    <tr>
							    <td style="width:175px;"><div><input class="aa_input" id="AddBene_contactnumber" name="contactnumber" style="width:100%;" type="text" value="" /></div></td>
							    <td style="width:18px;">&nbsp;</td>
							    <td style="width:367px;"><div><input class="aa_input" id="AddBene_email" name="emailaddress" style="width:100%;" type="text"  value=""  /></div></td>
						    </tr>
                            <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
					    </table>
					</td>
				</tr>


				<tr class="wiz_bg_EFEFEF">
					<td>
						<table class="tbl_width_560">
							<tr>
								<td class="aa_label_font">COUNTRY*</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">STATE/PROVINCE*</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">SUBURB/DISTRICT *</td>
							</tr>
							<tr>
								<td style="width:175px;"><select id="AddBene_CountryDDL" name="D2" style="width:100%;"></select></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:174px;"><select id="AddBene_stateprovince" name="D3" style="width:100%;"><option></option></select></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:175px;"><select id="AddBene_suburbdistrict" name="D4" style="width:100%;"><option></option></select></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>

				<tr class="wiz_bg_EFEFEF">
					<td>
						<table class="tbl_width_560">
							<tr>
								<td class="aa_label_font">ADDRESS LINE 1 *</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">ADDRESS LINE 2</td>
								<td class="aa_label_font">&nbsp;</td>
								<td class="aa_label_font">POST CODE</td>
							</tr>
							<tr>
								<td style="width:175px;"><input class="aa_input" id="AddBene_addressline1" name="country" style="width:100%;" type="text" required="required"/></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:174px;"><input class="aa_input" id="AddBene_addressline2" name="stateprovince" style="width:100%;" type="text"/></td>
								<td style="width:18px;">&nbsp;</td>
								<td style="width:175px;"><input class="aa_input" id="AddBene_postzipcode" name="suburbdistrict" style="width:100%;" type="text"/></td>
							</tr>
						</table>
					</td>
				</tr>

                <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_600">
							<tr>
								<td style="text-align: right;"><input class="aa_btn_red" id="btn_AddBeneficiarySubmit" type="button" value="Add Beneficiary"/></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

			</table>
			
				</form>

		</div>
	</div> 
   <%-- <div id="EditCustomerDiv">
        <div class="edit__form__main">
            <form method="get" id="customereditform">
            <table class="tbl_width_600">
                
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active" id="TD_EditBusiness_001"><asp:Image ID="Image23" runat="server" ImageUrl="~/images/company.png" title="COMPANY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_EditBusiness_002"><asp:Image ID="Image25" runat="server" ImageUrl="~/images/user.png" title="AUTHORIZED CONTACT" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_EditCustomer_003"><asp:Image ID="Image26" runat="server" ImageUrl="~/images/paper.png" title="NOTES & PASSWORD" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">Last name</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">Date of birth</td>
                        </tr>
                        <tr>
                            <td class="style__width_393"><div class="login__input"><label for="lastname"></label> <input class="last_name_class modal__input__font" id="lastname" name="lastname" style="width:100%;" type="text" value="" runat="server" /></div></td>
                            <td class="style__width_21"></td>
                            <td class="style__width_186"><div class="login__input"><input class="dob_class modal__input__font" id="dob" placeholder="DD/MM/YYYY" style="width:100%;" type="text" value=""  runat="server" /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>

                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">Given name/s</td>
                        </tr>
                        <tr>
                            <td class="style__width_600"><div class="login__input"><input class="given_name_class modal__input__font" id="firstnames" style="width:100%;" type="text"  value="" runat="server"  /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>


                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="btm__line">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Address line 1</td>
                            </tr>
                            <tr>
                                <td class="style__width_600"><div class="login__input"><input class="address_line_1_class modal__input__font" id="addressline1" style="width:100%;" type="text" value="" runat="server"  /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Address line 2</td>
                            </tr>
                            <tr>
                                <td class="style__width_600"><div class="login__input"><input class="address_line_2_class modal__input__font" id="addressline2" style="width:100%;" type="text" value=""  runat="server" /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="style__width_600">
                            <tr>
                                <td class="label__font">Suburb</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">State</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Postcode</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input"><input class="suburb_class modal__input__font" id="suburb" style="width:100%;" type="text" value="" runat="server" /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_135"><div class="login__input"><input class="state_class modal__input__font" id="state" style="width:100%;" type="text" value="" runat="server" /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_135"><div class="login__input"><input class="postcode_class modal__input__font" id="postcode" style="width:100%;" type="text" value="" runat="server" /></div></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="btm__line">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Home phone</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Work phone</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Mobile phone</td>
                            </tr>
                            <tr>
                                <td class="style__width_186"><div class="login__input"><input class="phone_home_class modal__input__font" id="phonehome" style="width:100%;" type="text" value="" runat="server"  /></div></td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186"><div class="login__input"><input class="phone_work_class modal__input__font" id="phonework" style="width:100%;" type="text" value="" runat="server"  /></div></td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186"><div class="login__input"><input class="phone_mobile_class modal__input__font" id="phonemobile" style="width:100%;" type="text" value=""  runat="server" /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Email address 1</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Email address 2</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input"><input class="email1_class modal__input__font" id="email1" style="width:100%;" type="email" value="" runat="server"  /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290"><div class="login__input"><input class="email2_class modal__input__font" id="email2" style="width:100%;" type="email" value="" runat="server"  /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Notes</td>
                            </tr>
                            <tr>
                                <td class="style__width_600"><div class="login__input"><input class="address_line_2_class modal__input__font" id="edit_notes" style="width:100%;" type="text" value=""  runat="server" /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Password</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Agent Ownership</td>
                            </tr>
                            <tr>
                                <td class="style__width_290" style="vertical-align:top;"><div class="login__input"><input class="email2_class modal__input__font" id="edit_password" style="width:100%;" type="text" value="" runat="server"  /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290">
                                    <select id="agent_ownership">
                                        <option value="NOSELECT">-- SELECT --</option>

                                    </select></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                        

                <tr>
                    <td class="spacer10">&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_red" id="btn_Submit" type="button" value="SAVE CHANGES"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>


            </table>
            
                </form>

        </div>
    </div>--%>
<!-- ************************* BUTTON 01 EDIT CUSTOMER MODAL POPUP END ************************* --> 


<!-- ************************* BUTTON 02 VIEW ACCOUNT MODAL POPUP START ************************* --> 
    <div id="ShowCreditDebitsDIV">
        <div class="add_credit_form">

            <table class="tbl_width_600">
                
                <tr><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table class="tbl_width_600">
                            <tr style="height:50px;">
                                <td style="width:50px;" class="wiz_tab_active"><asp:Image ID="Image21" runat="server" ImageUrl="~/images/account.png" title="BENEFICIARY DETAILS" /></td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                                <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

                <tr class="wiz_bg_EFEFEF">
                    <td>
                        <table class="tbl_width_560">
                            <tr>
                                <td>
                                    <table id="DebitCreditTbl">
                                        <thead>
                                            <tr class="tbl__full__width">
                                                <th class="tbl__head__left">REF #</th>
                                                <th class="tbl__head__left">DATE/TIME</th>
			                                    <th class="tbl__head__left">CREDIT</th>
                                                <th class="tbl__head__numbers">DEBIT</th>
                                                <th class="tbl__head__numbers">DESCRIPTION</th>
			                                </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                <tr class="spacer10"><td>&nbsp;</td></tr>


            </table>
        </div>
    </div> 
<!-- ************************* BUTTON 02 VIEW ACCOUNT MODAL POPUP END ************************* --> 


<!-- ************************* BUTTON 03 ADD CREDIT MODAL POPUP START ************************* --> 
    <div id="AddCreditDiv"> 
        <div class="add_credit_form">
            <form method="get" id="viewaccountform">
            <table class="style__width_600">
                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr id="TR_addcrediterrormsg">
                    <td>
                        <table class="style__width_600">
                            <tr>
                                <td class="err_modal_general">
                                    <table class="tbl_width_560">
                                        <tr>
                                            <td><span id="addcrediterrormsg">&nbsp;</span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">Amount $</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">Deposit method</td>
                        </tr>
                        <tr>
                            <td class="style__width_135"><div class="login__input"><input class="national_id_class modal__input__font" id="txt_CreditAmount" style="width:100%;" type="number" value="" required="required" /></div></td>
                            <td class="style__width_20">&nbsp;</td>
                            <td class="style__width_445"><div class="login__input"><select id="txt_CreditDescription" style="width:445px;"></select></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_red" id="btn_addcreditcomplete" type="button" value="ADD"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            
                </form>

        </div>
    </div>
<!-- ************************* BUTTON 03 ADD CREDIT MODAL POPUP END ************************* --> 



<!-- ************************* BUTTON 05 EDIT BENEFICIARY MODAL POPUP START ************************* --> 
        <div id="EditBeneficiaryDiv">
        <div class="edit__form__main">
            <form method="get" id="beneficiaryeditform">
            <table class="style__width_600">
                
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">Full name (without initials)</td>
                        </tr>
                        <tr>
                            <td class="style__width_600"><div class="login__input"><input class="given_name_class modal__input__font" id="edit_bene_fullname" style="width:100%;" type="text"  value="" /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>

                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">National ID</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">Relationship (to the customer)</td>
                        </tr>
                        <tr>
                            <td class="style__width_135"><div class="login__input"><input class="national_id_class modal__input__font" id="edit_bene_nationalid" style="width:100%;" type="text" value="" /></div></td>
                            <td class="style__width_20">&nbsp;</td>
                            <td class="style__width_445"><div class="login__input"><input class="relationship_class modal__input__font" id="edit_bene_relationship" style="width:100%;" type="text"  value="" /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="btm__line">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Address line 1</td>
                            </tr>
                            <tr>
                                <td class="style__width_600"><div class="login__input"><input class="address_line_1_class modal__input__font" id="edit_bene_addressline1" style="width:100%;" type="text" value="" /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Address line 2</td>
                            </tr>
                            <tr>
                                <td class="style__width_600"><div class="login__input"><input class="address_line_2_class modal__input__font" id="edit_bene_addressline2" style="width:100%;" type="text" value=""   /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="style__width_600">
                            <tr>
                                <td class="label__font">Suburb</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">State</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Postcode</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input"><input class="suburb_class modal__input__font" id="edit_bene_suburb" style="width:100%;" type="text" value="" /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_135"><div class="login__input"><input class="state_class modal__input__font" id="edit_bene_state" style="width:100%;" type="text" value="" /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_135"><div class="login__input"><input class="postcode_class modal__input__font" id="edit_bene_postcode" style="width:100%;" type="text" value="" /></div></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Country</td>
                            </tr>
                            <tr>
                                <td class="style__width_600"><div class="login__input"><input class="country_class modal__input__font" id="edit_bene_country" style="width:100%;" type="text" value=""  /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="btm__line">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Home phone</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Work phone</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Mobile phone</td>
                            </tr>
                            <tr>
                                <td class="style__width_186"><div class="login__input"><input class="phone_home_class modal__input__font" id="edit_bene_homephone" style="width:100%;" type="text" value="" /></div></td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186"><div class="login__input"><input class="phone_work_class modal__input__font" id="edit_bene_workphone" style="width:100%;" type="text" value="" /></div></td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186"><div class="login__input"><input class="phone_mobile_class modal__input__font" id="edit_bene_mobile" style="width:100%;" type="text" value=""   /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Email address</td>
                            </tr>
                            <tr>
                                <td class="style__width_600"><div class="login__input"><input class="email1_class modal__input__font" id="edit_bene_email" style="width:100%;" type="email" value="" /></div></td>

                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_red" id="btn_edit_Submit" type="button" value="SAVE"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            
                </form>

        </div>
    </div> 
<!-- ************************* BUTTON 05 EDIT BENEFICIARY MODAL POPUP END ************************* --> 

<!-- ************************* BUTTON 06 EDIT ACCOUNT MODAL POPUP START ************************* --> 
    <div id="EditAccount"> 
        <div class="edit_account_form">
            <form method="get" id="editaccountform">
            <table class="style__width_600">
                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Bank</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Branch</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input"><select id="ddl_Banks" style="width:290px;" name="ddl_Banks"></select></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290"><div class="login__input"><select id="Edit_ddl_Branches" style="width:290px;" name="Edit_ddl_Branches"></select></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="style__width_600">
                            <tr>
                                <td class="label__font">Account No</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Currency</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Active?</td>
                            </tr>
                            <tr>
                                <td class="style__width_360"><div class="login__input"><input class="phone_home_class modal__input__font ali_right" id="Edit_AccountNumber" style="width:100%;" type="text" value=""  /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_100"><div class="login__input"><select id="Edit_AccountType" style="width:100px;">
                                    <option value="LKR">LKR</option>
                                    <option value="AUD">AUD</option>
                                                                                       </select></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_100"><div class="login__input"><select id="Edit_Active" style="width:100px;">
                                    <option value="Y">Y</option>
                                    <option value="N">N</option>
                                                                                       </select></div></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_red" id="btn_saveeditbeneAccount" type="button" value="SAVE"/><input type="text" id="hidden_AccountID" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                

            </table>
            
                </form>

        </div>
    </div>
<!-- ************************* BUTTON 06 EDIT ACCOUNT MODAL POPUP END ************************* --> 

<!-- ************************* BUTTON 07 SEND MONEY MODAL POPUP START ************************* --> 
    	<div id="SendMoneyDIV"> 
		<div class="send_money_form">
			<form method="get" id="sendmoneyform">
				
				<table class="style__width_440">
				
				<tr><td><span id="SM_showdollaramount"></span></td></tr>

				<tr><td>&nbsp;</td></tr>

				<tr>
					<td class="err_modal_general_green">
						<table class="tbl_width_420">
							<tr>
								<td>
									<table class="tbl_width_420">
										<tr>
											<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image14" runat="server" ImageUrl="~/images/wallet.png" /></td>
											<td class="ag_transfer_labels" style="width:285px; vertical-align:middle;">AVAILABLE FUNDS</td>
											<td style="width:100px; vertical-align:middle;" class="ag_transfer_dollars"><span id="SPN_AddTR_AvailableFunds">$ 2,000.00</span></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
	
				<tr>
					<td>
						<div id="DIV_TRN_Errors">
							<table class="tbl_width_440">
								<tr class="spacer10"><td>&nbsp;</td></tr>
								<tr>
									<td class="err_modal_general">
										<table class="tbl_width_420">
											<tr class="spacer10"><td>&nbsp;</td></tr>
											<tr>
												<td>
													<div id="DIV_TRN_Add_Error_Funds">
														<table class="tbl_width_420">
															
															<tr>
																<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image13" runat="server" ImageUrl="~/images/err.png" /></td>
																<td style="width:385px; vertical-align:middle;">Insufficient funds to complete transaction.</td>
															</tr>
															<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
														</table>
													</div>
													<div id="DIV_TRN_Add_Error_Purpose">
														<table class="tbl_width_420">
															<tr>
																<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image16" runat="server" ImageUrl="~/images/err.png" /></td>
																<td style="width:385px; vertical-align:middle;">Please select the purpsose of the transaction to complete transaction.</td>
															</tr>
															<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
														</table>
													</div>
													<div id="DIV_TRN_Add_Error_Source">
														<table class="tbl_width_420">
															<tr>
																<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image17" runat="server" ImageUrl="~/images/err.png" /></td>
																<td style="width:385px; vertical-align:middle;">Please select the source of funds to complete transaction.</td>
															</tr>
															<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
														</table>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr>
							<tr>
								<td class="ag_transfer_labels_02" style="width:340px;">FUNDS TO BE SENT</td>
								<td><div class="ali_right" style="width:100px;"><input class="aa_input ali_right background_FFFFFF" id="SM_amounttobesent" style="width:100%;" type="text" value=""  /></div></td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr> 
								<tr>
									<td class="ag_transfer_labels_02" style="width:220px;">SERVICE CHARGES</td>
									<td>
										<table>
											<tr>
												<td><input class="aa_btn_green_outline" id="BTN_PromoCode" type="button" style="width:50px;" value="Promo" /></td>
												<td style="width:10px;">&nbsp;</td>
												<td><input class="aa_btn_red_outline" id="SM_btn_override" type="button" style="width:50px;" value="O/ride" /></td>
												<td style="width:10px;">&nbsp;</td>
												<td style="width:100px;"><div><input class="aa_input ali_right" id="SM_servicecharge" style="width:100%;" type="text" value="" /></div></td>
											</tr>
										</table>
									</td>
								</tr>
						</table>
					</td>
				</tr>

				<tr id="TR_PromoCode">
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td><td>&nbsp;</td></tr> 
								<tr>
									<td class="ag_transfer_labels_02" style="width:220px;">PROMO CODE</td>
									<td>
										<table>
											<tr>
												<td style="width:220px;"><div><input class="aa_input ali_right" id="SM_PromoCode" style="width:100%;" type="text" value="" /></div></td>
											</tr>
										</table>
									</td>
								</tr>
						</table>
					</td>
				</tr>


				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr class="spacer10"><td>&nbsp;</td></tr> 
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">EXCHANGE RATE</td>
								<td>
									<table class="tbl_width_220">
										<tr>
											<td style="width:50px;">&nbsp;</td>
											<td style="width:10px;">&nbsp;</td>
											<td class="ali_right"><input class="aa_btn_red_outline" id="SM_btn_overrideexchangerate" style="width:50px;" type="button" value="O/ride" /></td>
											<td style="width:10px;">&nbsp;</td>
											<td style="width:100px;"><div><input class="aa_input ali_right" id="SM_exchangerate" style="width:100%;" type="text" value="" /></div></td>
											</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">PURPOSE OF TRANSACTION</td>
								<td style="width:220px; text-align:right;">
									<div class="login__input">
										<select id="SM_Purpose" style="width:220px;">
											<option value="NOSELECT">-- SELECT --</option>
											<option value="Family Support">Family Support</option>
											<option value="Medical Expenses">Medical Expenses</option>
											<option value="Personal Travels & Tours">Personal Travels & Tours</option>
											<option value="Repayment of Loans">Repayment of Loans</option>
											<option value="Gift">Gift</option>
											<option value="Special Occasion">Special Occasion</option>
											<option value="Medical Expenses">Medical Expenses</option>
											<option value="Investments">Investments</option>
											<option value="Goods Purchased">Goods Purchased</option>
											<option value="Other">Other</option>
										</select>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">SOURCE OF FUNDS</td>
								<td style="width:220px; text-align:right;">
									<div class="login__input">
										<select id="SM_SourceOfFunds" style="width:220px;">
											<option value="NOSELECT">-- SELECT --</option>
											<option value="Personal Savings">Personal Savings</option>
											<option value="Business Activities">Business Activities</option>
											<option value="Loan">Loan</option>
											<option value="Family Money">Family Money</option>
										</select>
									</div>
								</td>
						</tr>
					</table>
					</td>
				</tr>

				<tr class="spacer10 ag_transfer_sep_border"><td>&nbsp;</td></tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="ag_transfer_labels_02" style="width:220px;">NOTES</td>
								<td style="width:220px; text-align:right;"><div><textarea name="TXT_Add_TRN_Notes" style="width:220px; height:55px; resize:none; line-height:16px !important; padding-top:8px;" class="aa_input"></textarea></div></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr>
					<td><span id="SM_span_senderror"></span></td>
				</tr>

				<tr class="spacer10"><td>&nbsp;</td></tr>

				<tr><td><input type="text" id="txt_hidden_foreignamount" name="txt_hidden_foreignamount" hidden="hidden" /><input type="text" id="txt_remainingaccountbalance" name="txt_remainingaccountbalance" hidden="hidden" /><input type="text" id="txt_bankname" name="txt_bankname" hidden="hidden" /><input type="text" id="hidden_adminoverride" name="hidden_adminoverride" value="N" hidden="hidden" /><input type="text" id="hidden_goingStatus" name="hidden_goingStatus" value="PENDING" hidden="hidden"  /><input type="text" id="hidden_compliancereason" name="hidden_compliancereason" value="" hidden="hidden"  /><input type="text" id="hidden_enoughfunds" name="hidden_enoughfunds" value="" hidden="hidden"   /><input type="text" id="hidden_openedpromo" name="hidden_openedpromo" value="" hidden="hidden"  /><input type="text" id="hidden_currencycode" name="hidden_currencycode" value="" hidden="hidden"  /><input type="text" id="hidden_sendamountoverride" name="hidden_sendamountoverride" value="N" hidden="hidden"  /></td></tr>

				<tr>
					<td class="err_modal_general_green">
						<table class="tbl_width_420">
							<tr>
								<td>
									<table class="tbl_width_420">
										<tr>
											<td style="width:35px; vertical-align:middle;"><asp:Image ID="Image15" runat="server" ImageUrl="~/images/paid.png" /></td>
											<td class="ag_transfer_labels" style="width:285px; vertical-align:middle;">TOTAL AUD (Funds to be sent + Service Charges</td>
											<td style="width:100px; vertical-align:middle;" class="ag_transfer_dollars"><div id="SM_div_sending"></div></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table class="tbl_width_420">
										<tr>
											<td style="width:35px; vertical-align:middle;">&nbsp;</td>
											<td class="ag_transfer_labels" style="width:285px; vertical-align:middle;">TOTAL RECEIVE IN <span id="divCurrencySending">LKR&nbsp;</span></td>
											<td style="width:100px; vertical-align:middle;" class="ag_transfer_dollars"><div id="SM_div_lkrvalue"></div></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr><td>&nbsp;</td></tr>

				<tr id="TR_SM_NegativeSend"> <!-- TR to show only when sending money with a negative account balance -->
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="err_modal_general">
									<table class="tbl_width_440">
										<tr>
											<td id="SendWarningMessage">WARNING! This customer do not have sufficient funds for this transaction. If you would like to proceed please provide the following details.</td>
										</tr>
                                        
										<tr><td>&nbsp;</td></tr>
										<tr>
											<td>
											<table class="tbl_width_440">
												<tr id="WarningSection1TR">
													<td class="label__font">Admin code</td>
													<td class="label__font">&nbsp;</td>
													<td class="label__font">Reason/details</td>
												</tr>
												<tr id="WarningSection2TR">
													<td style="width:88px;"><div class="login__input"><input class="national_id_class modal__input__font" id="txt_AdminCode" style="width:100%;" type="password" value="" /></div></td>
													<td style="width:10px;">&nbsp;</td>
													<td style="width:462px;"><div class="login__input"><input class="relationship_class modal__input__font" id="txt_OverrideReason" style="width:100%;" type="text"  value=""  /></div></td>
												</tr>
											</table>
											</td>
										</tr>
									</table>
								</td>
								
							</tr>

							<tr>
								<td>&nbsp;</td>
							</tr>

						</table>
					</td>
				</tr>


                    <tr id="TR_SM_ExceedSend"> <!-- TR to show only when transaction exceeds monthly limit -->
					<td>
						<table class="tbl_width_440">
							<tr>
								<td class="err_modal_general">
									<table class="tbl_width_440">
										<tr>
											<td id="limitexceeded">WARNING! This transaction will exceed the allocated monthly limit for this customer. This transaction will be escalated to compliance for additional documentation.</td>
										</tr>
                                        
									</table>
								</td>
								
							</tr>

							<tr>
								<td>&nbsp;</td>
							</tr>

						</table>
					</td>
				</tr>

				<tr>
					<td>
						<table>
							<tr>
								<td><input class="btn_blue" id="SM_btn_Send" type="button" value="SEND"/> <input class="btn_red" id="SM_btn_Confirm" type="button" value="CONFIRM"/> <input class="btn_grey" id="SM_btn_Cancel" type="button" value="CANCEL" /></td>
							</tr>
						</table>
					</td>
				</tr>

			</table> 
			</form>
		</div>
	</div>
<!-- ************************* BUTTON 07 SEND MONEY MODAL POPUP END ************************* --> 


<!-- ************************* BUTTON 08 OVER THE COUNTER MODAL POPUP START ************************* --> 
    <div id="OverTheCounter"> 
        <div class="add_credit_form">
            <form method="get" id="overthecounterform">
            <table class="style__width_600">
                
                <tr >
                    <td >&nbsp;</td>
                </tr>

                <tr style="background-color:#EEEEEE; height:50px;">
                    <td>
                        <table class="tbl_width_600">
                            <tr>
                                <td style="border-right:2px solid #F7F7F7;">
                                    <table class="tbl_width_260">
                                        <tr><td>&nbsp;</td></tr>
                                        <tr>
                                            <td><span class="transaction_step">Step 1.</span><span class="transaction_desc"> Select the bank</span></td>
                                        </tr>
                                        <tr><td class="spacer10">&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                <div class="login__input"><select id="aaaaaaaaaaaaaaa" style="width:260px;" name="ddl_Banks"></select></div>
                                            </td>
                                        </tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr>
                                            <td><span class="transaction_step">Step 2.</span><span class="transaction_desc"> Enter the amount to send</span></td>
                                        </tr>
                                        <tr hidden="hidden">
                                            <td>
                                                <div class="login__input"><input class="phone_home_class modal__input__font ali_right" disabled="disabled" id="OTC_availablefunds" style="width:100%;" type="text" value="" hidden="hidden" /></div>
                                            </td>
                                        </tr>
                                        
                                        

                                        <tr class="spacer10"><td>&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                <div class="login__input"><input class="phone_work_class modal__input__font" id="OTC_amounttobesent" style="width:100%;" type="text" value=""  /></div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td><span class="transaction_avail">Available funds:&nbsp;</span></td>
                                                        <td class="transaction_avail"><div id="OTC_DIV_AvailableFunds"></div></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr>
                                            <td><span class="transaction_step">Step 3.</span><span class="transaction_desc"> Review the service charges</span></td>
                                        </tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                <table class="tbl_width_260">
                                                    <tr>
                                                        <td style="width:88px;"><input class="btn_red_outline" id="OTC_btn_override" type="button" value="O/RIDE" /></td>
                                                        <td style="width:10px;">&nbsp;</td>
                                                        <td style="width:88px;"><div class="login__input"><input class="phone_mobile_class modal__input__font ali_right" id="OTC_servicecharge" style="width:100%;" type="text" value="" /></div></td>
                                                    </tr>
                                                </table>
                                            <%--</td>--%>
                                        </tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                                <td style="border-left:2px solid #FFFFFF;">
                                    <table class="tbl_width_260">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                        
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>



                <tr>
                    <td><span id="showdollaramount"></span></td>
                </tr>

                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style__width_445 lbl_modals">Available funds</td>
                            <td class="style__width_20"></td>
                            <td class="style__width_135">&nbsp;</td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style__width_445 lbl_modals">Funds to be sent</td>
                            <td class="style__width_20"></td>
                            <td class="style__width_135"></td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style__width_393 lbl_modals">Service charges</td>
                            <td class="style__width_21"></td>
                            <td class="style__width_186">
                                    
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td><span id="span_senderror"></span></td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Sending $<div id="OTC_div_sending"></div></td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Today's Rate<div id="OTC_div_todayrate"></div></td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Money in LKR<div id="OTC_div_lkrvalue"></div></td>
                            </tr>
                            <tr>
                                <td class="style__width_186">&nbsp;</td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186">&nbsp;</td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_red" id="btn_overthecountercomplete" type="button" value="CONFIRM"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            
                </form>

        </div>
    </div>
<!-- ************************* BUTTON 08 OVER THE COUNTER MODAL POPUP END ************************* --> 

<!-- ************************* BUTTON 09 HOME DELIVERY MODAL POPUP START ************************* --> 
    <div id="HomeDelivery"> 
        <div class="home_delivery_form">
            <form method="get" id="homedeliveryform">
            <table class="style__width_600">
                
                <tr>
                    <td><span id="showdollaramount"></span></td>
                </tr>

                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style__width_445 lbl_modals">Available funds</td>
                            <td class="style__width_20"></td>
                            <td class="style__width_135"><div class="login__input"><input class="phone_home_class modal__input__font ali_right" disabled="disabled" id="HD_availablefunds" style="width:100%;" type="text" value=""  /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style__width_445 lbl_modals">Funds to be sent</td>
                            <td class="style__width_20"></td>
                            <td class="style__width_135"><div class="login__input"><input class="phone_work_class modal__input__font ali_right" id="HD_amounttobesent" style="width:100%;" type="text" value=""  /></div></td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table>
                        <tr>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                            <td class="label__font">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style__width_393 lbl_modals">Service charges</td>
                            <td class="style__width_21"></td>
                            <td class="style__width_186">
                                    <table class="style__width_186">
                                        <tr>
                                            <td style="width:88px;"><input class="btn_red_outline" id="HD_btn_override" type="button" value="O/RIDE" /></td>
                                            <td style="width:10px;">&nbsp;</td>
                                            <td style="width:88px;"><div class="login__input"><input class="phone_mobile_class modal__input__font ali_right" id="HD_servicecharge" style="width:100%;" type="text" value="" /></div></td>
                                        </tr>
                                    </table>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td><span id="HD_span_senderror"></span></td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Sending $<div id="HD_div_sending"></div></td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Today's Rate<div id="HD_div_todayrate"></div></td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Money in LKR<div id="HD_div_lkrvalue"></div></td>
                            </tr>
                            <tr>
                                <td class="style__width_186">&nbsp;</td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186">&nbsp;</td>
                                <td class="style__width_21">&nbsp;</td>
                                <td class="style__width_186">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_red" id="btn_overthecountercomplete" type="button" value="CONFIRM"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            
                </form>

        </div>
    </div>
<!-- ************************* BUTTON 09 HOME DELIVERY MODAL POPUP END ************************* --> 

<!-- ************************* BUTTON 10 ADD NEW ACCOUNT MODAL POPUP START ************************* --> 
    <div id="AddNewAccount"> 
        <div class="add_account_form">
            <form method="get" id="addnewaccountform">
            <table class="style__width_600">
                
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Bank</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Branch</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input"><select id="Add_ddl_Banks" style="width:290px;"><option value="">-- SELECT BANK --</option></select></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290"><div class="login__input"><select id="Add_ddl_Branches" style="width:290px;"></select></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="style__width_600">
                            <tr>
                                <td class="label__font">Account No</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Currency</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Active?</td>
                            </tr>
                            <tr>
                                <td class="style__width_360"><div class="login__input"><input class="phone_home_class modal__input__font ali_right" id="add_AccountNumber" style="width:100%;" type="text" value=""  /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_100"><div class="login__input"><select id="Add_Account_Type" style="width:100px;">
                                    <option value="LKR" selected="selected">LKR</option>
                                    <option value="AUD">AUD</option>
                                                                                       </select></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_100"><div class="login__input"><select id="Add_Account_Active" style="width:100px;">
                                    <option value="Y" selected="selected">YES</option>
                                    <option value="N">NO</option>
                                                                                       </select></div></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_red" id="btn_addnewaccountcomplete" type="button" value="SAVE"/>&nbsp;&nbsp;<input class="btn_red" id="btn_verifyhnb" name="btn_verifyhnb" type="button" value="VERIFY ACCOUNT" hidden="hidden"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="TR_VerificationNotification">
                    <td>
                        <table>
                            <tr>
                                <td><span id="VerificationMsg"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
                </form>

        </div>
    </div>
<!-- ************************* BUTTON 10 ADD NEW ACCOUNT MODAL POPUP END ************************* --> 


    <div id="DIV_Documents">
        <table class="tbl_width_600">
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height:50px;">
                            <td style="width:50px;" class="wiz_tab_active" id="TD_Documents_001"><asp:Image ID="Image19" runat="server" ImageUrl="~/images/id-card3.png" title="ID DOCUMENTS" /></td>
                            <td style="width:50px;" class="wiz_tab_inactive_right" id="TD_Documents_002"><asp:Image ID="Image22" runat="server" ImageUrl="~/images/folder2.png" title="OTHER DOCUMENTS" /></td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>
                            <td style="width:50px;" class="wiz_tab_inactive_none">&nbsp;</td>       
                        </tr>
                    </table>   
                </td>
            </tr>

            <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>


            <tr style="background-color:#F4F2F2;">
                <td>
                    <form id="form_DocUpload" method="get">
                    <table class="tbl_width_560">
                        <tr><td class="aa_label_font">TYPE OF IDENTITY DOCUMENT *</td></tr>
                        
                        <tr>
                            <td><div class="login__input"><select id="typeofdocumentddl" style="width:560px;"></select></div></td>
                        </tr>
                        <tr>
                            <td class="spacer10">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="aa_label_font">DOC NUMBER *</td>
									    <td class="aa_label_font">&nbsp;</td>
									    <td class="aa_label_font">ISSUING AUTHORITY *</td>
									    <td class="aa_label_font">&nbsp;</td>
									    <td class="aa_label_font">EXPIRY DATE *</td>
									    <td class="aa_label_font">&nbsp;</td>
								        <td class="aa_label_font">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width:127px;"><div><input class="aa_input" id="documentnumbertxt" style="width:100%;" type="text" value="" required="required" /></div></td>
                                        <td style="width:18px;">&nbsp;</td>
                                        <td style="width:126px;"><div><input class="aa_input" id="issuingauthoritytxt" style="width:100%;" type="text" value="" required="required"/></div></td>
                                        <td style="width:18px;">&nbsp;</td>
                                        <td style="width:126px;"><div><input class="aa_input datePickerMonthReport" id="expirydatetxt" style="width:100%;" type="text" value="" readonly="readonly"  required="required" /></div></td>
                                        <td style="width:18px;"><input id="hdnpoints" type="text" name="hdnpoints" value="" hidden="hidden" /></td>
                                        <td style="width:127px; vertical-align:top; text-align:right;"><input type="file" name="UploadFile1" id="UploadFile1" class="aa_btn_green" style="display: none;" /><input type="button" class="aa_btn_green" value="Browse..." onclick="document.getElementById('UploadFile1').click();" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr><td class="spacer10">&nbsp;</td></tr>

                        <tr>
                            <td class="trn_notes_popup_details" style="font-size:0.7em; color:#999;">Selected file name: <span style="color:#666;" id="selectedfilename"></span></td>
                        </tr>
                        <tr>
                            <td class="spacer10">&nbsp;</td>
                        </tr>
                        <tr>
                            <td><input class="aa_btn_red" id="btn_UploadFile" type="button" value="Upload"/>&nbsp;<input class="aa_btn_red" id="btn_CamCapture" type="button" value="Camera Capture"/></td>
                        </tr>
                        <tr class="wiz_seperator_white wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                        <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>
                            
                    </table>
                        </form>

                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table id="FileUploadMainTbl">
                        <thead>
                            <td class="tbl_width_600">Doc type</td>
                            <td class="tbl_width_600">Number</td>
                            <td class="tbl_width_600">Authority</td>
                            <td class="tbl_width_600">Points</td>
                            <td class="tbl_width_600">Expiry</td>
                            <td class="tbl_width_600">View</td>
                        </thead>
                    </table>

                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="all_popup_headings">Other Documents</td>
            </tr>
            <tr style="background-color:#F4F2F2;">
                <td>
                    <form id="doc_UploadOtherDocs" method="get">
                        <table class="tbl_width_560">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="label__font">Document description</td>
                                            <td class="label__font">&nbsp;</td>
                                            <td class="label__font">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="style_width_415"><div class="login__input"><input class="phone_home_class modal__input__font" id="docdescription" name="docdescription" style="width:100%;" type="text" value="" /></div></td>
                                            <td class="style__width_20">&nbsp;</td>
                                            <td class="style_width_125"><input type="file" name="UploadFile2" id="UploadFile2" class="btn_green_upload" style="display: none;" /><input type="button" class="btn_green" value="Browse..." onclick="document.getElementById('UploadFile2').click();" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="label__font">Selected Document: <span id="otherdoctext"></span></td>
                            </tr>
                            <tr>
                                <td class="spacer10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input class="btn_red" id="btn_UploadOtherFiles" type="button" value="UPLOAD"/>&nbsp;<input class="btn_red" id="btn_CampCaptureOtherFiles" type="button" value="CAMERA CAPTURE"/></td>
                            </tr>
                            <tr>
                                <td class="spacer10">&nbsp;</td>
                            </tr>
                        </table>
                    </form>

                </td>
            </tr>
            <tr>
                <td>
                    <table id="FileUploadOtherTbl">
                        <thead>
                            <td class="tbl_width_600">Doc Name</td>
                            <td class="tbl_width_600">View</td>
                        </thead>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>

    </div>

    <div id="DIV_Modal_CompanyVerified">
                                <table style="width:600px;">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr><td>
                                <table class="tbl_width_560">
                                    <tr>
                                        <td>
                                            <input id="chk_CompanyVerified" type="checkbox" runat="server" /></td>
                                        <td>KASI verified customer. Verified by [name] on [date]</td>
                                    </tr>
                                </table>
                                </td></tr>
                            <tr><td>&nbsp;</td></tr>
                        </table>
    </div>

    


    <div id="DIV_Modal_KYC"> <!-- KYC STARTS HERE -->
        <div class="edit__form__main">
            <form method="get" id="kycaddform">
            <table class="style__width_600">
                
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Marital status</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Number of financial dependants</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input">
                                    <select id="maritalstatus" style="width:290px;">
                                        <option value="">--SELECT--</option>
                                        <option value="Single">Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Defacto">Defacto</option>
                                        <option value="Separated">Separated</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Windowed">Windowed</option>
                                    </select>
                                    </div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290"><div class="login__input"><select id="findep" style="width:290px;">
                                    <option value="">--SELECT--</option>
                                    <option value="1-One">1-One</option>
                                    <option value="2-Two">2-Two</option>
                                    <option value="3-Three">3-Three</option>
                                    <option value="4-Four">4-Four</option>
                                    <option value="5-Five">5-Five</option>
                                    <option value="6-Six">6-Six</option>
                                    <option value="7-Seven">7-Seven</option>
                                    <option value="8-Eight">8-Eight</option>
                                    <option value="9-Nine or more">9-Nine or more</option>
                                </select></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

               <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Are you a permanent Australian resident?</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Residential status</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input"><select id="AURes" style="width:290px;">
                                    <option value="">--SELECT--</option>
                                    <option value="YES">YES</option>
                                    <option value="NO">NO</option>
                                 </select></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290"><div class="login__input"><select id="ResStat" style="width:290px;">
                                    <option value="">--SELECT--</option>
                                    <option value="Renting">Renting</option>
                                    <option value="Own home (no mortgage)">Own home (no mortgage)</option>
                                    <option value="Own home (mortgage)">Own home (mortgage)</option>
                                    <option value="Parent/Relative">Parent/Relative</option>
                                    <option value="Boarding">Boarding</option>
                                    <option value="Employer Supplied">Employer Supplied</option>
                                </select></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="spacer10">
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="btm__line">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="label__font">Employment status</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Occupation/Job title</td>
                            </tr>
                            <tr>
                                <td class="style__width_290"><div class="login__input"><select id="EmpStat" style="width:290px;">
                                    <option value="">--SELECT--</option>
                                    <option value="Full-time">Full-time</option>
                                    <option value="Part-time">Part-time</option>
                                    <option value="Self Employed">Self-Employed</option>
                                    <option value="Retired">Retired</option>
                                    <option value="Casual/Contractor">Causal/Contractor</option>
                                    <option value="Home Duties">Home Duties</option>
                                    <option value="Student">Student</option>
                                    <option value="Unemployed">Unemployed</option>
                                 </select></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290"><div class="login__input"><input class="email2_class modal__input__font" style="width:100%;" type="text" value="" id="occupation"  /></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="spacer5">&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <table class="style__width_600">
                            <tr>
                                <td class="label__font">Salary (after tax) Net</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Frequency</td>
                                <td class="label__font">&nbsp;</td>
                                <td class="label__font">Do you have other income/s</td>
                            </tr>
                            <tr>
                                <td class="style__width_135"><div class="login__input"><input class="suburb_class modal__input__font"  style="width:100%;" type="text" id="salary" value="" /></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_135"><div class="login__input"><select id="frequency" style="width:135px;">
                                    <option value="">--SELECT--</option>
                                    <option value="Weekly">Weekly</option>
                                    <option value="Fortnightly">Fortnightly</option>
                                    <option value="Monthly">Monhtly</option>
                                    <option value="Yearly">Yearly</option>
                                 </select></div></td>
                                <td class="style__width_20">&nbsp;</td>
                                <td class="style__width_290"><div class="login__input"><select id="otherincome" style="width:290px;" name="D1">
                                    <option value="">--SELECT--</option>
                                    <option value="Second job salary">Second job salary</option>
                                    <option value="Overtime">Overtime</option>
                                    <option value="Government Benefits">Government Benefits</option>
                                    <option value="Other">Other</option>
                                </select></div></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr class="spacer5">
                    <td>
                        &nbsp;
                    </td>
                </tr>

                <tr>
                    <td class="label__font">Comments</td>
                </tr>

                <tr>
                    <td>
                        <textarea id="comments" cols="20" rows="2" style="width:600px; resize:none;"></textarea>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>

                <tr style="background-color:#F4F2F2;">
                    <td>
                        <table class="tbl_width_560">
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td style="width:50px;"><input id="CHK_ConfirmInfo" type="checkbox" class="chk_confirm" /></td>
                                <td style="width:510px;"class="fnt_chkbox">I confirm that the above information was given by customer <span id="SPN_ID" runat="server">ID</span> <span id="SPN_NAME" runat="server">NAME</span></td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                        </table>
                        
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <table class="tbl_width_560"><tr><td id="TD_ExpInfo">&nbsp;</td></tr></table>
                    </td>
                </tr>
                
                <tr style="height:20px;"><td>&nbsp;</td></tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><input class="btn_disabled" id="btn_kyc_add_Submit" type="button" value="SAVE" disabled="disabled"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            
                </form>

        </div> <!-- KYC FORM END HERE -->
    </div>

    <div id="DIV_Loading"> <!-- Loading DIV -->
        <table class="tbl_width_120">
            <tr>
                <td style="text-align:center;">
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/loading.gif" Height="60px" Width="60px" />
                </td>
            </tr>
            <tr><td class="fnt_pleasewait">Please wait...</td></tr>
        </table>
    </div>

    <div id="DIV_Message_OK"> <!-- Message OK DIV -->
        <table class="tbl_width_120">
            <tr>
                <td style="text-align:center;">
                    <span id="msgtext">OK</span>
                </td>
            </tr>
            <tr><td class="fnt_pleasewait"><button class="btn_green_nomargin" id="ok_button_dialog"><span>OK</span></button></td></tr>
        </table>
    </div>

    <div id="DIV_Send_Bank_Details" hidden="hidden"> <!-- Send Bank Details to Customer DIV -->
        Here it is
    </div>
    
    <div id="DIV_ModifyKYC">
        <table class="tbl_width_600">
            <tr><td>&nbsp;</td></tr>
            <tr style="background-color:#EFEFEF;"><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table class="tbl_width_560">
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td style="width:50px;"><input id="CHK_ManKYC_Confirm" type="checkbox" class="chk_confirm" /></td>
                            <td style="width:510px;"class="fnt_chkbox">I confirm that I have manually checked and completed the KYC Check for this Customer.</td>
                                    </tr>
                                </table>
                            </td>
                            
                        </tr>
                        <tr><td>&nbsp;</td></tr>

                        <tr>
                            <td>
                                <table>
                                    <tr>
<td style="width:270px; padding-top: 0px;">
                                                    <table style="width:270px;">
                                                        <tr>
                                                            <td class="aa_select_btn_inactive" id="btn_ManKYC_Failed">KYC Failed</td>
                                                            <td style="width:10px;">&nbsp;</td>
                                                            <td class="aa_select_btn_inactive" id="btn_ManKYC_Success">KYC Success</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width:20px;">&nbsp;</td>
                                                <td style="width:270px; vertical-align:top;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>


                                                
                                            </tr>


                        <tr>
                            <td>
                                &nbsp;
                            </td></tr>

                        <tr>
                            <td>
                                &nbsp;
                                <textarea id="TXT_ManKYCNotes" cols="20" name="S1" rows="4"></textarea></td></tr>

                    </table>
                </td>
            </tr>
<tr>
            <td>
                <table class="tbl_width_600">
                    <tr>
                        <td style="text-align:right;">
                            <input id="hdn_ManKYC_Outcome" type="hidden" />
                            <input class="aa_btn_red" id="BTN_ManKYCSave" type="button" value="Save"/></td>
                    </tr>
                </table>
            </td>
        </tr>
            <tr><td>&nbsp;</td></tr>
                            
                            <tr><td>&nbsp;</td></tr>


        </table>
    </div>
    <div id="ViewTransactionDIV"> <!-- THIS IS THE APPROVAL POPUP DIALOG BOX -->
        <div class="edit__form__main">
            <form method="get" id="approvetransactionform">
            <table class="tbl_width_600">


                <tr><td>&nbsp;</td></tr>
    
        <tr>
            <td>
                <table class="tbl_width_600">
                    <tr style="height:50px;">
                        <td style="width:50px;" class="wiz_tab_active" id="Approve_TD_001"><asp:Image ID="Image18" runat="server" ImageUrl="~/images/paid.png" title="TRANSACTION" /></td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                        <td style="width:50px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>    
    
        <tr><td class="wiz_bg_EFEFEF" style="height:25px;">&nbsp;</td></tr>

        <tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr>
                        <td style="background-color:#FFF; height:40px;">
                            <table class="tbl_width_560">
                                <tr style="height:40px;">
                                    <td style="width:15px;">&nbsp;</td>
                                    <td class="txn_sml_headings">Transaction Reference: <span id="TransIDUpper"></span>&nbsp; - &nbsp; Completed</td>
                                    
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="spacer10 wiz_bg_EFEFEF"><td>&nbsp;</td></tr>


        <tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr>
                        <td style="width:275px;">
                            <table style="width:100%;">
                                <tr class="background_FFFFFF"><td class="inside_headings" style="height:40px;">SEND AMOUNT</td></tr>
                                <tr class="background_FFFFFF"><td class="txn_popup_recamount" style="color:#666 !important; font-weight:600;"><span id="DollarAmount"></span></td></tr>
                                <tr style="background-color:#F7F9FA; border-top:1px solid rgb(234, 234, 234);"><td class="txn_popup_footer_01">Transaction Fee: <span id="ServiceFee" class="txn_popup_fw600"></span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Total Paid: <span id="AUDAmount" class="txn_popup_fw600"></span></td></tr>
                            </table>
                        </td>
                        <td style="width:10px;">&nbsp;</td>
                        <td style="width:275px;">
                            <table style="width:100%;">
                                <tr class="background_FFFFFF"><td class="inside_headings" style="height:40px;">RECEIVE AMOUNT</td></tr>
                                <tr class="background_FFFFFF"><td class="txn_popup_recamount"><span id="FAmount"></span></td></tr>
                                <tr style="background-color:#F7F9FA; border-top:1px solid rgb(234, 234, 234);"><td class="txn_popup_footer_01">Exchange Rate: <span id="Rate" class="txn_popup_fw600"></span></td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="wiz_bg_EFEFEF spacer10"><td>&nbsp;</td></tr>

        <tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr>
                        <td style="width:275px; background-color:#FFF;">
                            <table class="tbl_width_245">
                                <tr class="background_FFFFFF"><td class="inside_headings" style="padding-left:0px; height:50px;">SENDER DETAILS</td></tr>
                                <tr><td class="txn_popup_h_02">CUSTOMER ID/NAME</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="CustomerID"></span>&nbsp;-&nbsp;<span id="CustName"></span></td></tr>
                                <tr><td class="txn_popup_h_02">ADDRESS</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="CustAddress"></span></td></tr>
                                <tr><td class="txn_popup_h_02">CONTACT NO</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="CustMobile"></span></td></tr>
                                <tr><td class="txn_popup_h_02">OCCUPATION</td></tr>
                                <tr><td class="txn_popup_c_01"></td></tr>
                                <tr><td class="txn_popup_h_02">PURPOSE OF TRANSACTION</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="TransPurpose"></span></td></tr>
                                <tr><td class="txn_popup_h_02">SOURCE OF FUNDS</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="TransSourceOfFunds"></span></td></tr>
                                <tr><td class="txn_popup_h_02">NOTES</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="TransNotes"></span></td></tr>
                                <tr><td>&nbsp;</td></tr>
                            </table>
                        </td>
                        <td style="width:10px;">&nbsp;</td>
                        <td style="width:275px; background-color:#FFF;">
                            <table class="tbl_width_245">
                                <tr class="background_FFFFFF"><td class="inside_headings" style="padding-left:0px; height:50px;">RECEIVER DETAILS</td></tr>
                                <tr><td class="txn_popup_h_02">NAME</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="BeneName"></span></td></tr>
                                <tr><td class="txn_popup_h_02">ADDRESS</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="BeneAddress"></span></td></tr>
                                <tr><td class="txn_popup_h_02">CONTACT NO</td></tr>
                                <tr><td class="txn_popup_c_01"></td></tr>
                                <tr><td class="txn_popup_h_02">RELATIONSHIP</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="Relationship"></span></td></tr>
                                <tr><td class="txn_popup_h_02">BANK</td></tr>
                                <tr><td class="txn_popup_c_01"><span id="OriginalBank"></span></td></tr>
                                <tr><td class="txn_popup_h_02">ACCOUNT NO</td></tr>
                                <tr><td class="txn_popup_c_01"></td></tr>
                                <tr><td>&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>






        <tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr>
                        <td style="background-color:#EFEFEF; width:275px;">
                            <table style="width:100%;">
                                <tr>
                                    <td class="trn_details_001">SENDER Details</td>
                                </tr>
                                
                                

                                
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td class="trn_details_003_heading">EMAIL</td>
                                            </tr>
                                            <tr>
                                                <td class="trn_details_004_details"><!--<span id="CustEmail"></span>--></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                
                                


                            </table>
                        </td>
                        <td style="width:10px; height:120px;">&nbsp;</td>
                        <td style="width:275px;">
                            <table style="width:100%;">
                                <tr>
                                    <td class="trn_details_001">RECEIVER Details</td>
                                </tr>
                                
                                
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td class="trn_details_003_heading">ID DETAILS</td>
                                            </tr>
                                            <tr>
                                                <td class="trn_details_004_details"><span id="IDDetails"></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td class="trn_details_003_heading">BANK NAME</td>
                                            </tr>
                                            <tr>
                                                <td class="trn_details_004_details"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td class="trn_details_003_heading">BANK ADDRESS</td>
                                            </tr>
                                            <tr>
                                                <td class="trn_details_004_details"><span id="OriginalBankAccount"></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td class="trn_details_003_heading">ACCOUNT NAME</td>
                                            </tr>
                                            <tr>
                                                <td class="trn_details_004_details"><span id="AccountName"></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td class="trn_details_003_heading">ACCOUNT NUMBER</td>
                                            </tr>
                                            <tr>
                                                <td class="trn_details_004_details"><span id="BeneAccountID"></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td class="trn_details_003_heading">TRANSACTION TYPE</td>
                                            </tr>
                                            <tr>
                                                <td class="trn_details_004_details"><span id="TransType"></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

        <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

        <tr class="spacer10"><td>&nbsp;</td></tr>

                <tr hidden="hidden">
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <input id="APPR_TransID" name="APPR_TransID" type="text" />   </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>

            </table>
            
                </form>

        </div>
    </div>

</asp:Content>
