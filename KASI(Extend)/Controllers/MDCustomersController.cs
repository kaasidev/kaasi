﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using KASI_Extend_.Models;

namespace KASI_Extend_.Controllers
{
    public class MDCustomersController : ApiController
    {
        private KASI_Extend_Context db = new KASI_Extend_Context();

        // GET: api/MDCustomers
        public IQueryable<MDCustomer> GetMDCustomers()
        {
            return db.MDCustomers;
        }

        // GET: api/MDCustomers/5
        [ResponseType(typeof(MDCustomer))]
        public IHttpActionResult GetMDCustomer(int id)
        {
            MDCustomer mDCustomer = db.MDCustomers.Find(id);
            if (mDCustomer == null)
            {
                return NotFound();
            }

            return Ok(mDCustomer);
        }

        // PUT: api/MDCustomers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMDCustomer(int id, MDCustomer mDCustomer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mDCustomer.customerID)
            {
                return BadRequest();
            }

           // db.Entry(mDCustomer).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MDCustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MDCustomers
        [ResponseType(typeof(MDCustomer))]
        public IHttpActionResult PostMDCustomer(MDCustomer mDCustomer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MDCustomers.Add(mDCustomer);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mDCustomer.customerID }, mDCustomer);
        }

        // DELETE: api/MDCustomers/5
        [ResponseType(typeof(MDCustomer))]
        public IHttpActionResult DeleteMDCustomer(int id)
        {
            MDCustomer mDCustomer = db.MDCustomers.Find(id);
            if (mDCustomer == null)
            {
                return NotFound();
            }

            db.MDCustomers.Remove(mDCustomer);
            db.SaveChanges();

            return Ok(mDCustomer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MDCustomerExists(int id)
        {
            return db.MDCustomers.Count(e => e.customerID == id) > 0;
        }
    }
}