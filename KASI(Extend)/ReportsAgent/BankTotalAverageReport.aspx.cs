﻿using Microsoft.Reporting.WebForms;
using Kapruka.Service.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend.ReportsAgent
{
    public partial class BankTotalAverageReport : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session["TypeOfLogin"].ToString() == "Agent")
                {
                    this.Page.MasterPageFile = "~/AgentControl/Reports.Master";
                }
            }
            catch (Exception ex)
            {

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {


            }
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(DateTime.Parse(FromDate.Text), DateTime.Parse(EndDate.Text), Session["AgentID"].ToString());
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "ReportsAgent/BankAverageReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("fromDate", FromDate.Text),
                new ReportParameter("toDate", EndDate.Text),
                new ReportParameter("AID", Session["AgentID"].ToString()),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(DateTime fromDate, DateTime toDate, String AID)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = new SqlCommand("REPORT_getBankRateAverages", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                cmd.Parameters.Add("@AgentID", SqlDbType.Int).Value = AID;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }

            return dt;
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(FromDate.Text) || String.IsNullOrEmpty(EndDate.Text))
            {
                // LBL_ErrorMessage.Style.Add("display", "block");
                // LBL_ErrorMessage.Text = "Please select date range before running report.";
            }
            else
            {
                ShowReport();
            }
        }
    }
}