﻿using KASI_Extend_.classes;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KASI_Extend.ReportsAgent
{
    public partial class AF01ListOfTransactionsByBank : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session["TypeOfLogin"].ToString() == "Agent")
                {
                    this.Page.MasterPageFile = "~/AgentControl/Reports.Master";
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

            KASI_Extend_.classes.Transactions TRS = new KASI_Extend_.classes.Transactions();
            float HighValue = float.Parse(TRS.getHighestAUDTransAmount());
            TXT_MaxTransVal.Text = (Math.Round(HighValue / 100d) * 100).ToString();
            TXT_MaxTransVal.Style.Add("display", "none");


            if (!IsPostBack)
            {
                
                    DataTable agents = new DataTable();

                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                        try
                        {

                            SqlDataAdapter adapter = new SqlDataAdapter("SELECT DISTINCT AFBS.BankID, BK.BankName FROM dbo.AgentForeignBankSettings AFBS INNER JOIN dbo.Banks BK ON BK.BankCode = AFBS.BankID AND AFBS.AgentID = " + Session["AgentID"].ToString(), conn);
                            adapter.Fill(agents);

                            DropDownList1.DataSource = agents;
                            DropDownList1.DataTextField = "BankName";
                            DropDownList1.DataValueField = "BankID";
                            DropDownList1.DataBind();
                        }
                        catch (Exception ex)
                        {

                        }
                        conn.Close();
                    }
                    DropDownList1.Items.Insert(0, new ListItem("-- SELECT --", "0"));
                }

            
        }

        private void ShowReport()
        {

            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text, EndDate.Text, DropDownList1.SelectedValue.ToString(), amount.Value);
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "ReportsAgent/ListofTransactionsByBank.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text),
                new ReportParameter("EndDate", EndDate.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();


        }

        private DataTable GetData(string fromDate, string toDate, String SelectedAgent, String selectedAUDRange)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            DateTime newFromDate = Convert.ToDateTime(fromDate).AddDays(1);
            DateTime newToDate = Convert.ToDateTime(toDate).AddDays(1);

            //String[] SplitAUDRange = selectedAUDRange.Split('-');
            //String AUDFrom = SplitAUDRange[0].Trim().Replace("$", "");
            //String AUDTo = SplitAUDRange[1].Trim().Replace("$", "");
            String RoutingBank = DropDownList1.SelectedValue;


            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = null;
                string query = "";
                if (Session["AgentID"] == null)
                {
                    if (DropDownList1.SelectedItem.Value == "0")
                    {
                        query = "SET DATEFORMAT dmy select A.*,(select BeneficiaryName from Beneficiaries where BeneficiaryID=A.BeneficiaryID) AS BeneficiaryName,(select CountryName from Countries where CountryID=(select CountryID from BeneficiaryPaymentMethods where AccountID=A.AccountID)) As Country,(select PreviousCustomerID from Customers where CustomerID=A.CustomerID) as PreviousCustomerID,(select top 1 AccountNumber from BeneficiaryPaymentMethods where BeneficiaryID=A.BeneficiaryID) as BeneficiaryAccountNumber,(select top 1 B.BankName from BeneficiaryPaymentMethods as BN inner join Banks as B on B.BankCode=BN.BankID where BN.BeneficiaryID= A.BeneficiaryID) as DestinationBank from (select RoundingAmount,BeneficiaryID,AccountID,TransactionID,FORMAT(CreatedDateTime,'dd/MM/yyyy') AS CreatedDateTime,CustomerLastName,CONCAT(CONVERT(Decimal(16,2), ISNULL(DollarAmount,0)),' ','AUD') AS DollarAmount,CONVERT(Decimal(16,2), ISNULL(DollarAmount,0)) as DollarAmountPlain,CONCAT(CONVERT(Decimal(16,2),ISNULL(ServiceCharge,0)),' ','AUD') AS ServiceCharge,CONVERT(Decimal(16,2),ISNULL(ServiceCharge,0)) as ServiceChargePlain,CONCAT(CONVERT(Decimal(16,2),(DollarAmount+ServiceCharge)),' ','AUD') AS TotalCharge,(DollarAmount+ServiceCharge) as TotalChargePlain,CONCAT(CONVERT(Decimal(16,2), ISNULL(RemittedAmount,0)),' ',RemittedCurrency) AS RemittedAmount,CONVERT(Decimal(16,2), ISNULL(RemittedAmount,0)) as RemittedAmountPlain,Bank,RemittedCurrency,Status,CONCAT(CONVERT(Decimal(16,2),Rate),' ',RemittedCurrency) AS Rate,CustomerID,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'' AS SelectedAgent from Transactions where CreatedDateTime between Convert(datetime,'" + fromDate + "') and Convert(datetime,'" + toDate + "') ) AS A";
                        cmd = new SqlCommand(query, conn);
                    }
                    else
                    {
                        query = "SET DATEFORMAT dmy select A.*,(select BeneficiaryName from Beneficiaries where BeneficiaryID=A.BeneficiaryID) AS BeneficiaryName,(select CountryName from Countries where CountryID=(select CountryID from BeneficiaryPaymentMethods where AccountID=A.AccountID)) As Country,(select PreviousCustomerID from Customers where CustomerID=A.CustomerID) as PreviousCustomerID,(select top 1 AccountNumber from BeneficiaryPaymentMethods where BeneficiaryID=A.BeneficiaryID) as BeneficiaryAccountNumber,(select top 1 B.BankName from BeneficiaryPaymentMethods as BN inner join Banks as B on B.BankCode=BN.BankID where BN.BeneficiaryID= A.BeneficiaryID) as DestinationBank  from (select RoundingAmount,BeneficiaryID,AccountID,TransactionID,FORMAT(CreatedDateTime,'dd/MM/yyyy') AS CreatedDateTime,CustomerLastName,CONCAT(CONVERT(Decimal(16,2), ISNULL(DollarAmount,0)),' ','AUD') AS DollarAmount,CONVERT(Decimal(16,2), ISNULL(DollarAmount,0)) as DollarAmountPlain,CONCAT(CONVERT(Decimal(16,2),ISNULL(ServiceCharge,0)),' ','AUD') AS ServiceCharge,CONVERT(Decimal(16,2),ISNULL(ServiceCharge,0)) as ServiceChargePlain,CONCAT(CONVERT(Decimal(16,2),(DollarAmount+ServiceCharge)),' ','AUD') AS TotalCharge,(DollarAmount+ServiceCharge) as TotalChargePlain,CONCAT(CONVERT(Decimal(16,2), ISNULL(RemittedAmount,0)),' ',RemittedCurrency) AS RemittedAmount,CONVERT(Decimal(16,2), ISNULL(RemittedAmount,0)) as RemittedAmountPlain,Bank,RemittedCurrency,Status,CONCAT(CONVERT(Decimal(16,2),Rate),' ',RemittedCurrency) AS Rate,CustomerID,'From " + fromDate + " to " + toDate + "' As SelectedDateRange, '" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent from Transactions where convert(date, CreatedDateTime) between Convert(date,'" + fromDate + "') and Convert(date,'" + toDate + "') and AgentID=" + DropDownList1.SelectedItem.Value + ") AS A WHERE RoutingBank = '" + RoutingBank + "'";
                        cmd = new SqlCommand(query, conn);

                    }
                }
                else
                {

                    string agentName = new Agent().getAgentNameByID(Session["AgentID"].ToString());
                    query = "SET DATEFORMAT dmy select A.*,(select BeneficiaryName from Beneficiaries where BeneficiaryID=A.BeneficiaryID) AS BeneficiaryName,(select BankName FROM dbo.Banks WHERE BankCode = '" + RoutingBank + "') AS TheSelectedBank,(select CountryName from Countries where CountryID=(select CountryID from BeneficiaryPaymentMethods where AccountID=A.AccountID)) As Country,(select PreviousCustomerID from Customers where CustomerID=A.CustomerID) as PreviousCustomerID,(select top 1 AccountNumber from BeneficiaryPaymentMethods where AccountID=A.AccountID) as BeneficiaryAccountNumber,(select top 1 B.BankName from BeneficiaryPaymentMethods as BN inner join Banks as B on B.BankCode=BN.BankID where BN.AccountID= A.AccountID) as DestinationBank from (select RoundingAmount,BeneficiaryID,AccountID,TransactionID,FORMAT(CreatedDateTime,'dd/MM/yyyy') AS CreatedDateTime,CustomerLastName,CONCAT(CONVERT(Decimal(9,2), ISNULL(DollarAmount,0)),' ','AUD') AS DollarAmount,CONVERT(Decimal(9,2), ISNULL(DollarAmount,0)) as DollarAmountPlain,CONCAT(CONVERT(Decimal(9,2),ISNULL(ServiceCharge,0)),' ','AUD') AS ServiceCharge,CONVERT(Decimal(9,2),ISNULL(ServiceCharge,0)) as ServiceChargePlain,CONCAT(CONVERT(Decimal(9,2),(DollarAmount+ServiceCharge)),' ','AUD') AS TotalCharge,(DollarAmount+ServiceCharge) as TotalChargePlain,CONCAT(CONVERT(Decimal(9,2), ISNULL(RemittedAmount,0)),' ',RemittedCurrency) AS RemittedAmount,CONVERT(Decimal(9,2), ISNULL(RemittedAmount,0)) as RemittedAmountPlain,Bank,RemittedCurrency,Status,CONCAT(CONVERT(Decimal(9,2),Rate),' ',RemittedCurrency) AS Rate,CustomerID,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + agentName + "' AS SelectedAgent from Transactions where RoutingBank = '" + RoutingBank + "' AND CreatedDateTime between Convert(datetime,'" + fromDate + "') and Convert(datetime,'" + newToDate.ToString() + "') and AgentID=" + Session["AgentID"].ToString() + ") AS A";
                    cmd = new SqlCommand(query, conn);
                }

                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);


            }

            return dt;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}