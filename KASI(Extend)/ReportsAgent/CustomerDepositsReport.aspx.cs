﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Drawing;
using System.Globalization;

namespace KASI_Extend_.Reports
{
    public partial class CustomerDepositsReport : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session["TypeOfLogin"].ToString() == "Agent")
                {
                    this.Page.MasterPageFile = "~/AgentControl/Reports.Master";
                }
            }
            catch (Exception ex)
            {

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                if (Session["AgentID"] == null)
                {
                    DataTable agents = new DataTable();

                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;
                        try
                        {

                            SqlDataAdapter adapter = new SqlDataAdapter("SELECT AccountName, BankAccountID FROM dbo.MasterCompanyBanks", conn);
                            adapter.Fill(agents);

                            DropDownList1.DataSource = agents;
                            DropDownList1.DataTextField = "AccountName";
                            DropDownList1.DataValueField = "BankAccountID";
                            DropDownList1.DataBind();
                        }
                        catch (Exception ex)
                        {

                        }
                        conn.Close();
                    }
                    DropDownList1.Items.Insert(0, new ListItem("-- SELECT --", "0"));
                }
                else
                {
                    DropDownList1.Visible = false;
                }
            }
        }


        private void ShowReport()
        {
            ReportViewer1.Reset();
            ReportViewer1.Visible = true;
            DataTable dt = GetData(FromDate.Text, EndDate.Text, DropDownList1.SelectedValue.ToString());
            ReportDataSource rds = new ReportDataSource("DataSet1.DataTable1", dt);

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = "ReportsAgent/CustomerDepositsReport.rdlc";

            ReportParameter[] rptParams = new ReportParameter[] {
                new ReportParameter("StartDate", FromDate.Text),
                new ReportParameter("EndDate", EndDate.Text),
            };

            //ReportViewer1.LocalReport.SetParameters(rptParams);

            ReportViewer1.LocalReport.Refresh();
        }

        private DataTable GetData(string fromDate, string toDate, String SelectedAgent)
        {
            DataTable dt = new DataTable();
            String constr = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = constr;
                SqlCommand cmd = null;
                if (Session["AgentID"] == null)
                {
                    if (DropDownList1.SelectedItem.Value == "0")
                    {
                        cmd = new SqlCommand("SET DATEFORMAT dmy select A.*,(select BeneficiaryName from Beneficiaries where BeneficiaryID=A.BeneficiaryID) AS BeneficiaryName,(select CountryName from Countries where CountryID=(select CountryID from BeneficiaryPaymentMethods where AccountID=A.AccountID)) As Country from (select BeneficiaryID,AccountID,TransactionID,FORMAT(CreatedDateTime,'dd/MM/yyyy') AS CreatedDateTime,CustomerLastName,CONCAT(CONVERT(Decimal(9,2), ISNULL(DollarAmount,0)),' ','AUD') AS DollarAmount,CONCAT(CONVERT(Decimal(9,2),ISNULL(ServiceCharge,0)),' ','AUD') AS ServiceCharge,CONCAT(CONVERT(Decimal(9,2),(DollarAmount+ServiceCharge)),' ','AUD') AS TotalCharge,CONCAT(CONVERT(Decimal(9,2), ISNULL(RemittedAmount,0)),' ',RemittedCurrency) AS RemittedAmount,Bank,RemittedCurrency,Status,CONCAT(CONVERT(Decimal(9,2),Rate),' ',RemittedCurrency) AS Rate,CustomerID,'From " + fromDate + " to " + toDate + "' As SelectedDateRange,'" + DropDownList1.SelectedItem.Text + "' AS SelectedAgent from Transactions where CreatedDateTime between Convert(datetime,'" + fromDate + "') and Convert(datetime,'" + toDate + "') ) AS A", conn);
                    }
                    else
                    {
                        cmd = new SqlCommand("SET DATEFORMAT dmy SELECT CTS.CustomerID, CTS.FirstName + ' ' + CTS.LastName AS FullName, CRT.CreditAmount, CRT.CreatedDateTime, CRT.Type FROM dbo.CreditTransactions CRT INNER JOIN dbo.Customers CTS ON CRT.CustomerID = CTS.CustomerID WHERE MasterBankAccountID = " + DropDownList1.SelectedValue, conn);

                    }
                }
                else
                {

                    cmd = new SqlCommand("SET DATEFORMAT dmy SELECT CTS.CustomerID, CTS.FirstName + ' ' + CTS.LastName AS FullName, CRT.CreditAmount, CRT.CreatedDateTime, CRT.Type FROM dbo.CreditTransactions CRT INNER JOIN dbo.Customers CTS ON CRT.CustomerID = CTS.CustomerID", conn);
                }

                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);


            }

            return dt;
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}