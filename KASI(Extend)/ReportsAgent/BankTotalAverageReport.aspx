﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportsNav.Master" AutoEventWireup="true" CodeBehind="BankTotalAverageReport.aspx.cs" Inherits="KASI_Extend.ReportsAgent.BankTotalAverageReport" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">
    <form id="form1" runat="server">

        <style>
            .ui-datepicker-calendar {
                display: none;
            }

            input[type="image"] {
                padding: 0px !important;
            }
.ui-datepicker .ui-datepicker-buttonpane button {
    font-family: 'Varela Round', sans-serif !important;
    font-size: 0.7em;
}



        </style>


        <div class="container_top_headings">
            <table class="tbl_width_1200">
                <tr style="height:70px; vertical-align:middle;">
                    <td style="vertical-align:middle;"><span class="all_headings">Monthly Revenue Report</span></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>

        <div class="container_one no_padding_btm wiz_bg_EFEFEF">
            <table class="tbl_width_1200">
                <tr>
                    <td class="background_FFFFFF">
                        <table class="tbl_width_1160">
                            <tr style="height: 20px;"><td>&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table class="tbl_width_1160">
                                        <tr><td><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></td></tr>
                                        <tr>
                                            <td><asp:TextBox ID="FromDate" CssClass="aa_input datePickerReport" runat="server" placeholder="Select DD/MM/YYYY"></asp:TextBox>&nbsp;<asp:TextBox ID="EndDate" CssClass="aa_input datePickerReport" runat="server" placeholder="Select DD/MM/YYYY"></asp:TextBox></td>
                                            <td>&nbsp;</td>
                                            <td style="text-align:right;"><asp:Button ID="btnViewReport" runat="server" Text="Run Report" class="aa_btn_green" OnClick="btnViewReport_Click"  /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height:20px;"><td>&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <div class="container_one no_padding_top_btm wiz_bg_EFEFEF">
            <table class="tbl_width_1200">
                <tr class="spacer10"><td>&nbsp;</td></tr>
                <tr style="background-color:#FFFFFF;">
                    <td>
                        <table class="tbl_width_1160">
                            <tr style="height:20px;"><td>&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" SizeToReportContent="True" WaitMessageFont-Size="14pt" Width="100%">
                                    </rsweb:ReportViewer>
                                </td>
                            </tr>
                            <tr style="height:20px;"><td>&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </div>

    </form>
</asp:Content>
