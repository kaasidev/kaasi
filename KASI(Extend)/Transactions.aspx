﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Initial.Master" AutoEventWireup="true" CodeBehind="Transactions.aspx.cs" Inherits="KASI_Extend_.Transactions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" charset="utf-8" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.fileupload.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/moment.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/numeric-comma.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/moment.min.js"></script>
    <script src="js/toastr.min.js"></script>
    <link href="css/toastr.min.css" rel="stylesheet" />
    <link href="css/datatables.min.css" rel="stylesheet" />
    <!-- <link href="css/dataTables.custom.dashboard.css" rel="stylesheet" /> -->
    <link href="css/select2.css" rel="stylesheet" />
    <!-- CSS STYLE FILESs -->
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/datatables.min.transactions.css" rel="stylesheet" />
    <!--<link href="css/datatables.min.css" rel="stylesheet" />-->

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:400,500,600,700,900" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Condensed:300,400,500,600,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet" />

    <style>
        .error1 {
            border-left: 5px solid rgb(192, 57, 43);
            background-color: #fff3f2;
        }

        .error1_font {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 0.7em !important;
            font-weight: normal;
            line-height: normal;
            color: rgb(192, 57, 43);
        }

        .col_011 {
            text-align: left;
            border-right: 1px solid #EFEFEF;
        }

        .txn_popup_recamount {
            font-family: 'Varela Round', sans-serif !important;
            font-size: 1.25em;
            font-weight: normal;
            color: #62cb31;
            padding-bottom: 10px;
            padding-left: 15px;
            background-color: #FFFFFF;
            font-weight: 400;
            text-align: left;
        }

        .container_tags {
            margin: 0px;
            padding: 0px;
            width: 560px;
        }

        .tags {
            zoom: 1;
        }

            .tags:before,
            .tags:after {
                content: '';
                display: table;
            }

            .tags:after {
                clear: both;
            }

            .tags li {
                position: relative;
                float: left;
                margin: 0px 5px 15px 0px;
            }

                .tags li:active {
                    margin-top: 1px;
                    margin-bottom: 7px;
                }

            .tags a {
                font-family: 'Varela Round', sans-serif !important;
                height: 22px;
                padding: 6px 10px 6px 10px;
                font-size: 10px;
                color: #EF675E;
                text-decoration: none;
                text-shadow: 0 1px white;
                background: #fafafa;
                border-width: 1px 1px 1px 1px;
                border-style: solid;
                border-color: #dadada #d2d2d2 #c5c5c5;
                border-radius: 3px 3px 3px 3px;
                background-image: -webkit-linear-gradient(top, #fcfcfc, #f0f0f0);
                background-image: -moz-linear-gradient(top, #fcfcfc, #f0f0f0);
                background-image: -o-linear-gradient(top, #fcfcfc, #f0f0f0);
                background-image: linear-gradient(to bottom, #fcfcfc, #f0f0f0);
                -webkit-box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.7), 0 1px 2px rgba(0, 0, 0, 0.05);
                box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.7), 0 1px 2px rgba(0, 0, 0, 0.05);
            }

            .tags span {
                position: absolute;
                top: 1px;
                left: 100%;
                z-index: 2;
                overflow: hidden;
                max-width: 0;
                height: 24px;
                line-height: 21px;
                padding: 0 0 0 2px;
                color: white;
                text-shadow: 0 -1px rgba(0, 0, 0, 0.3);
                background: #eb6b22;
                border: 1px solid;
                border-color: #d15813 #c85412 #bf5011;
                border-radius: 0 2px 2px 0;
                opacity: .95;
                background-image: -webkit-linear-gradient(top, #ed7b39, #df5e14);
                background-image: -moz-linear-gradient(top, #ed7b39, #df5e14);
                background-image: -o-linear-gradient(top, #ed7b39, #df5e14);
                background-image: linear-gradient(to bottom, #ed7b39, #df5e14);
                -webkit-transition: 0.3s ease-out;
                -moz-transition: 0.3s ease-out;
                -o-transition: 0.3s ease-out;
                transition: 0.3s ease-out;
                -webkit-transition-property: padding, max-width;
                -moz-transition-property: padding, max-width;
                -o-transition-property: padding, max-width;
                transition-property: padding, max-width;
            }

        /* Audit Table CSS*/

        #DIV_CompReview_Audit table.dataTable {
            width: 100% !important;
            margin: 0 auto;
            clear: both;
            border-collapse: separate;
            border-spacing: 0px;
            font-family: 'Encode Sans Condensed', sans-serif;
            font-family: 'Varela Round', sans-serif !important;
            font-size: 0.65em;
            color: #666;
            font-weight: 500;
            line-height: normal !important;
        }
        #DIV_CompReview_Audit table.dataTable tbody th,
        #DIV_CompReview_Audit table.dataTable tbody td {
            padding: 5px 5px 5px 0px;
            border-bottom: 1px solid #EFEFEF !important;
        }
        #DIV_CompReview_Audit table.dataTable tbody tr {
            background-color: #FFFFFF;
        }
        #DIV_CompReview_Audit .dataTables_wrapper table thead{
            display:none;
        }
        #DIV_CompReview_Audit table.dataTable tr.odd {
            background-color:#fff;
        }

        #new-search-area {
            width: 100%;
            clear: both;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 15px;
            background-color: #F7F9FA;
            text-align: right !important;
            border-bottom: 2px solid #EFEFEF;
        }
        #new-search-area input {
            width: 200px;
            font-size: 0.7em;
            padding: 5px;
            height: 25px !important;
            border-radius: 0 !important;
            margin-bottom: 0px !important;
        }
        .tbl_audit_c1 {
            width:140px;
        }
        .tbl_audit_c2 {
            width:390px;
        }
        .chk_approve input:checked ~ .checkmark {
            background-color: #76D17F;
            border: 1px solid #00B545 !important;
        }
        </style>




    <script type="text/javascript">

        function displayReviewNotes(sTransID) {
            //alert(sCustID);
            $.ajax({
                url: 'JQDataFetch/getCustomerReviewNotes.aspx',
                async: false,
                data: {
                    TID: sTransID,
                },
                success: function (response) {
                    
                    $('#DIV_Notes').empty();
                    if (response != "NORECORDS") {
                        $('#TR_Notes').css('display', 'table-row');
                        var initialsplit = response.split('~');
                        
                        $.each(initialsplit, function (item) {
                            var datasplit = initialsplit[item].split('|');

                            $('#DIV_Notes').append('<table class="tbl_width_540"><tr><td class="trn_notes_heading">' + datasplit[2] + ' - ' + datasplit[0] + ' - ' + datasplit[3] + ' (' + datasplit[4] + ')' + '</td></tr><tr><td class="trn_notes_contents">' + datasplit[1] + '</td></tr><tr class="spacer10"><td>&nbsp;</td></tr></table>');
                        });
                    }

                }
            })
        }

        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            }
            return val;
        }

        function EditTransactionClick(TID) {
            //alert(TID);
            FillCustomerBeneficiaryList(TID)
            FillEditTransaction(TID);
            ApproveDialog.dialog('close');
            EditTransDialog.dialog('open');
        }

        function FillCustomerBeneficiaryList(TID) {
            var custID = '';

            $.ajax({
                url: 'JQDataFetch/getCustomerIDFromTransactionID.aspx',
                async: false,
                data: {
                    TID: TID,
                },
                success: function (response) {
                    custID = response;

                }
            });

            //alert(custID);
            $.ajax({
                url: 'JQDataFetch/getCustomerBeneficiaries.aspx',
                async: false,
                data: {
                    CID: custID,
                },
                success: function (response) {
                    $('#benelist').empty();
                    var splitbene = response.split('~');
                    splitbene.sort();
                    $.each(splitbene, function (item) {
                        var datasplit = splitbene[item].split('|');
                        $('#benelist').append('<option value="' + datasplit[1] + '">' + datasplit[0] + '</option>');
                    });
                }
            })
        }

        function FillEditTransaction(TID) {

            var theCustID = '';
            var howMuchSent = 0;

            $.ajax({
                url: 'JQDataFetch/getTransactionBasicDetailsForEdit.aspx',
                async: false,
                data: {
                    TID: TID,
                },
                success: function (response) {

                    var splitres = response.split('|');
                    theCustID = splitres[1];
                    howMuchSent = splitres[4];
                    $('#TransID').text(TID);
                    $('#TransDateTime').text(splitres[0]);
                    $('#CustID').text(splitres[1]);
                    $('#CustName').text(splitres[2]);
                    $('#AUDRate').text('$' + parseFloat(splitres[3]).toFixed(2));
                    $('#TotalSending').text('$' + parseFloat(splitres[4]).toFixed(2));
                    $('#hidden_sendcalc').val(splitres[4]);
                    var text1 = splitres[5];
                    $('#benelist option').filter(function () {
                        return this.value == text1;
                    }).attr('selected', true);

                    $.ajax({
                        url: 'JQDataFetch/getBeneficiaryAccountsInOneLine.aspx',
                        async: false,
                        data: {
                            BeneID: splitres[5],
                        },
                        success: function (data) {
                            $('#accountlist').empty();
                            var splitdata = data.split('~');
                            //alert(data);
                            splitdata.sort();
                            $.each(splitdata, function (indi) {
                                var interndata = splitdata[indi].split('|');
                                $('#accountlist').append('<option value="' + interndata[1] + '">' + interndata[0] + '</option>');
                            });
                        }
                    });

                    var text2 = splitres[6];
                    $('#accountlist option').filter(function () {
                        return this.value == text2;
                    }).attr('selected', true);
                }
            });

            var AccBalance = 0;
            $.ajax({
                url: 'JQDataFetch/getCustomerAccountBalance.aspx',
                async: false,
                data: {
                    CID: theCustID,
                },
                success: function (balance) {
                    AccBalance = balance;
                }
            });

            var totalsendpower = parseFloat(AccBalance) + parseFloat(howMuchSent);
            $('#hidden_sendcalc').val(totalsendpower);

            var SCharge = 0;

            if (howMuchSent > 1000) {
                SCharge = 15;
            }
            else {
                SCharge = 10;
            }

            $('#SM_servicecharge').val(parseFloat(SCharge).toFixed(2));
            $('#fundstosend').val(parseFloat(howMuchSent).toFixed(2));
            $('#availablefunds').val(parseFloat(AccBalance).toFixed(2));
        }

        function openEditDialog(sTransID, sCustID, OBank, sRate, sDollarAmount, sStatus, sOrigin) {
            $("#txt_workingTransID").val(sTransID);
            $('#txt_workingDialog').val(sOrigin);
            $('#btn_Submit').hide();
            $('#btn_VoidTransaction').hide();
            $('#btn_EditTransaction').hide();
            $('#btn_EscalateToReview').hide();
            $("#btn_ReturnToPending").hide();

            $('#APPR_TransID').val(sTransID);
            $.ajax({
                url: 'JQDataFetch/getCustomerDetailsForTransView.aspx',
                data: {
                    CID: sCustID,
                },
                success: function (data) {
                    var sdata = data.split('|');
                    //alert(sdata[1]);
                    $('#APPR_CustomerID').html("<a href='dashboard.aspx?CustID=" + sCustID + "' target='_blank'>" + sCustID + "</a>");
                    $('#APPR_CustName').text(sdata[0]);
                    $('#APPR_CustAddress').text(sdata[1]);
                    $('#APPR_CustMobile').text(sdata[2]);
                    $('#APPR_CustEmail').text(sdata[3]);
                },

            });

            var sBeneID = '';
            $.ajax({
                url: 'JQDataFetch/getBeneficiaryIDFromTransaction.aspx',
                async: false,
                data: {
                    TID: sTransID
                },
                success: function (response) {
                    sBeneID = response;
                }
            })


            //$('#APPR_CustomerID').text(sCustID);



            var sBeneName = '';
            $.ajax({
                url: 'JQDataFetch/getBeneficiaryDetailsForTransView.aspx',
                async: false,
                data: {
                    BID: sBeneID,
                    TID: sTransID
                },
                success: function (response) {
                    //alert(response);
                    //sBeneName = response;
                    var sdata = response.split('|');
                    $('#APPR_BeneName').text(sdata[0]);
                    $('#APPR_BeneAddress').text(sdata[1]);
                    $("#APPR_BeneMobile").text(sdata[2]);
                    //$('#APPR_OriginalBank').text(sdata[3]);
                    $("#APPR_BeneAccountID").text(sdata[4]);
                    $('#APPR_TransType').text(sdata[5]);
                    $('#APPR_OriginalBankAccount').text(sdata[6]);
                    $('#APPR_AccountName').text(sdata[7]);
                    $('#APPR_TransIDUpper').text(sTransID);
                    $('#APPR_CountryName').text(sdata[8]);
                }
            })

            var sRsAmount = '';
            var CurrencyCode = '';
            $.ajax({
                url: 'JQDataFetch/getRemittedAmountFromTransaction.aspx',
                async: false,
                data: {
                    TID: sTransID
                },
                success: function (response) {
                    var data = response.split('|');
                    sRsAmount = data[0];
                    $("#APPR_ServiceFee").text(parseFloat(data[1]).toFixed(2) + ' AUD')
                    $("#APPR_AUDAmount").text(parseFloat(data[2]).toFixed(2) + ' AUD')
                    CurrencyCode = data[3];
                    $('#APPR_TransPurpose').text(data[4]);
                    $('#APPR_TransSourceOfFunds').text(data[5]);
                    $('#APPR_TransNotes').text(data[6]);
                    $('#APPR_Rate').text(data[7]);
                }
            })
            $('#APPR_RsAmount').text(parseFloat(sRsAmount).toFixed(2) + ' ' + CurrencyCode);

            var cDollar = parseFloat(sDollarAmount.replace('$', '').replace(',', ''));
            var totalSend = cDollar;
            $('#APPR_DollarAmount').text(totalSend.toFixed(2) + ' AUD');





            var BBank = '';
            $.ajax({
                url: 'JQDataFetch/getBankBranchFromTransaction.aspx',
                async: false,
                data: {
                    TID: sTransID
                },
                success: function (response) {
                    BBank = response;
                }
            })
            $('#APPR_OriginalBank').text(OBank);

            // Hide Appropriate Buttons
            $('#btn_ApproveAPI').hide();

            if (sStatus == "PENDING") {
                $('#btn_Submit').show();
                $("#btn_VoidTransaction").show();
                $('#btn_EditTransaction').show();
                $('#btn_EscalateToReview').show();
                $("#APPR_RoutingBank").show();
            }
            else if (sStatus == "AWAITING TRANSFER") {
                $("#btn_VoidTransaction").show();
                $('#btn_EscalateToReview').show();
                $("#btn_ReturnToPending").show();
                $("#APPR_RoutingBank").hide();
            }
            else if (sStatus == "SUCCESSFUL") {
                $("#btn_VoidTransaction").show();
                $('#btn_EscalateToReview').show();
                $("#btn_ReturnToPending").show();
                $("#APPR_RoutingBank").hide();
            }
            else if (sStatus == "CANCELLED") {
                $("#APPR_RoutingBank").hide();
            }

            ApproveDialog.dialog('open');
        }

        function openReviewDialog(sTransID, sCustID, sDollarAmount, sRate, OBank, sOrigin, sStatus) {
         
            $('#TR_Notes').css('display', 'none');
            if ($('#<%=txt_IsComplianceManager.ClientID%>').val() == "False") {
                $('#REVW_btn_Submit').hide();
            }

            if (sStatus == "MASTER REVIEW") {
                $('#TR_Confirmation').hide();
                $('#REVW_btn_Submit').show();
                $('#SPN_Status').text("RNP REVIEW PENDING");
            }
            else if (sStatus == "PENDING") {
                $('#TR_Confirmation').show();
                $("#REVW_btn_Submit").hide();
                $('#SPN_Status').text("PENDING");
            }
            else if (sStatus == "REVIEW") {
                $("#REVW_btn_Submit").hide();
                $('#TR_Confirmation').hide();
                $("#SPN_Status").text("AFFILIATE REVIEW PENDING");
            }
            else {
                $('#REVW_btn_Submit').hide();
                $('#TR_Confirmation').hide();
                $('#SPN_Status').text(sStatus);
            }

            $('#REVW_DisplayTransID').text(sTransID);

            var AMonthlyAUD;
            var AYearlyAUD;
            var AMonthlyNb;
            var AYearlyNb;

            var CMonthlyAUD;
            var CYearlyAUD;
            var CMonthlyNb;
            var CYearlyNb;

            var SMonthlyAUD;
            var SYearlyAUD;
            var SMonthlyNb;
            var SYearlyNb;


            var AgentID;

            $.ajax({
                url: 'JQDataFetch/getAgentIDFromTransactionID.aspx',
                async: false,
                data: {
                    TID: sTransID,
                },
                success: function (response) {
                    AgentID = response;
                }
            })

            $.ajax({
                url: 'JQDataFetch/getAgentTransLimit.aspx',
                async: false,
                data: {
                    AgentID: AgentID
                },
                success: function (response) {
                    var datasplit = response.split('|');
                    AMonthlyAUD = datasplit[0];
                    AYearlyAUD = datasplit[1];
                    AMonthlyNb = datasplit[2];
                    AYearlyNb = datasplit[3];
                }
            });

            $.ajax({
                url: 'JQDataFetch/getRNPTransLimit.aspx',
                async: false,
                success: function (response) {
                    var datasplit = response.split('|');
                    SMonthlyAUD = datasplit[0];
                    SYearlyAUD = datasplit[1];
                    SMonthlyNb = datasplit[2];
                    SYearlyNb = datasplit[3];
                }
            })

            $.ajax({
                url: 'JQDataFetch/getCustomerTotals.aspx',
                async: false,
                data: {
                    CID: sCustID,
                },
                success: function (data) {
                    var datasplit = data.split('|');
                    CMonthlyAUD = datasplit[0];
                    CYearlyAUD = datasplit[1];
                    CMonthlyNb = datasplit[2];
                    CYearlyNb = datasplit[3];
                }
            })

            $("#Last30daysDollar").text(commaSeparateNumber(parseFloat(CMonthlyAUD).toFixed(2)));
            $('#ALast30daysDollar').text(commaSeparateNumber(parseFloat(AMonthlyAUD).toFixed(2)));

            $("#LastYearDollar").text(commaSeparateNumber(parseFloat(CYearlyAUD).toFixed(2)));
            $("#ALastYearDollar").text(commaSeparateNumber(parseFloat(AYearlyAUD).toFixed(2)));

            $("#Last30daysNb").text(CMonthlyNb);
            $('#ALast30daysNb').text(AMonthlyNb);

            $('#LastYearNb').text(CYearlyNb);
            $('#ALastYearNb').text(AYearlyNb);

            // ********** FORMAT WITH RED IF EXCEEDED *********
            if (parseFloat(CMonthlyAUD.replace("$", "").replace(",", "")) > parseFloat(SMonthlyAUD.replace("$", "").replace(",", "")))
            {
                $("#Last30daysDollar").toggleClass('trn_amounts_red');
            }
            else
            {
                $("#Last30daysDollar").toggleClass('trn_amounts_green');
            }

            if (parseFloat(CYearlyAUD.replace("$", "").replace(",", "")) > parseFloat(SYearlyAUD.replace("$", "").replace(",", "")))
            {
                $("#LastYearDollar").toggleClass('trn_amounts_red');
            }
            else
            {
                $("#LastYearDollar").toggleClass('trn_amounts_green');
            }

            if (parseFloat(CMonthlyNb) > parseFloat(SMonthlyNb))
            {
                $('#Last30daysNb').toggleClass('trn_amounts_red');
            }
            else
            {
                $('#Last30daysNb').toggleClass('trn_amounts_green');
            }

            if (parseFloat(CYearlyNb) > parseFloat(SYearlyNb))
            {
                $('#LastYearNb').toggleClass('trn_amounts_red');
            }
            else
            {
                $('#LastYearNb').toggleClass('trn_amounts_green');
            }


            //*************************************************

            $('#REWV_TransID').text(sTransID);

            if (sStatus == "REVIEW" || sStatus == "MASTER REVIEW")
            {
                $.ajax({
                    url: 'JQDataFetch/getTransactionReviewReasons.aspx',
                    async: false,
                    data: {
                        TID: sTransID,
                    },
                    success: function (data) {
                        $("#LST_ReviewReasons").empty();
                        $("#TBL_ReviewReasons").empty();
                        //$("#TBL_ReviewReasons").append('<tr><td>&nbsp;</td></tr>');
                        $("#TBL_ReviewReasons").append('<tr><td class="auto-style1">ERROR!</td></tr>');
                        $("#TBL_ReviewReasons").append('<tr><td class="error_font" style="height:15px;">This Transaction needs to be reviewed by a Compliance Officer for the following reason/s.</td></tr>');
                        if (data != "NORECORDS") {

                            var splitdata = data.split('|');
                            $.each(splitdata, function (key, value) {
                                if (sStatus == "MASTER REVIEW") {
                                    if (value.indexOf('RNP') >= 0) {
                                        $('#TBL_ReviewReasons').append('<tr><td class="error_font" style="height:15px;">* ' + value + "</td></tr>");
                                        $('#LST_ReviewReasons').append('<li><a>' + value + '</a></li>');
                                    }
                                }
                                else {
                                    if (value.indexOf('RNP') < 0) {
                                        $('#TBL_ReviewReasons').append('<tr><td class="error_font" style="height:15px;">* ' + value + "</td></tr>");
                                        $('#LST_ReviewReasons').append('<li><a>' + value + '</a></li>');
                                    }
                                }


                            });
                            $('#TBL_ReviewReasons').append('<tr class="spacer10"><td>&nbsp;</td></tr>');
                        }

                    }
                })
            }
            

            $('#txt_workingDialog').val(sOrigin);
            $('#REVW_TransID').val(sTransID);
            $('#REVW_CustID').val(sCustID);
            //alert($('#REVW_CustID').val());

            $.ajax({
                url: 'JQDataFetch/getTransactionType.aspx',
                data: {
                    TID: sTransID,
                },
                success: function (TransType) {
                    $("#REVW_TransType").text(TransType);
                }
            })

            //var sCustID = $('td', this).eq(2).text();
            $('#REVW_CustomerID').html('<a href="dashboard.aspx?CustID=' + sCustID + '" target="_blank">' + sCustID + '</a>');

            $.ajax({
                url: 'JQDataFetch/getCustomerDetailsForTransView.aspx',
                async: false,
                data: {
                    CID: sCustID,
                },
                success: function (data) {
                    var splitdata = data.split('|');
                    $('#REVW_CustName').text(splitdata[0]);
                    $("#REVW_CustAddress").text(splitdata[1]);
                    $("#REVW_CustMobile").text(splitdata[2]);
                    $("#REVW_Occupation").text(splitdata[4]);
                },

            });

            $.ajax({
                url: 'JQDataFetch/getAgentNameFromTransactionID.aspx',
                async: false,
                data: {
                    TID: sTransID,
                },
                success: function (response)
                {
                    $('#REVW_AffiliateName').text(response);
                }
            })

            $.ajax({
                url: 'JQDataFetch/getBeneficiaryIDFromTransaction.aspx',
                async: false,
                data: {
                    TID: sTransID
                },
                success: function (response) {
                    sBeneID = response;
                }
            })

            var sBeneName = '';
            $.ajax({
                url: 'JQDataFetch/getBeneficiaryDetailsForTransView.aspx',
                async: false,
                data: {
                    BID: sBeneID,
                    TID: sTransID
                },
                success: function (response) {
                    //alert(response);
                    //sBeneName = response;
                    var sdata = response.split('|');
                    $('#REVW_BeneName').text(sdata[0]);
                    $("#REVW_BeneAddress").text(sdata[1]);
                    $("#REVW_BeneMobile").text(sdata[2]);
                    $('#REVW_BeneRelationship').text(sdata[9]);
                    $("#REVW_OriginalBank").text(sdata[10]);
                }
            })



            $('#REVW_DollarAmount').text(sDollarAmount + ' AUD');




            var sRsAmount = '';
            $.ajax({
                url: 'JQDataFetch/getRemittedAmountFromTransaction.aspx',
                async: false,
                data: {
                    TID: sTransID
                },
                success: function (response) {
                    //alert(response);
                    data = response.split('|');
                    $('#REVW_CountryID').val(data[8]);
                    $("#REVW_ServiceFee").text(parseFloat(data[1]).toFixed(2));
                    $("#txt_ServiceFee").val(parseFloat(data[1]).toFixed(2));
                    $("#REVW_AUDAmount").text(commaSeparateNumber(parseFloat(data[2]).toFixed(2)));
                    sRsAmount = parseFloat(data[2]).toFixed(2) + ' AUD';
                    $("#REWV_DateTime").text(data[9]);
                    $("#REVW_RsAmount").text(commaSeparateNumber(parseFloat(data[0]).toFixed(2)) + ' ' + data[3])
                    $('#REVW_Rate').text(parseFloat(data[7]).toFixed(2));
                    $("#REVW_Purpose").text(data[4]);
                    $("#REVW_SourceOfFunds").text(data[5]);
                    $("#REVW_Notes").text(data[6]);
                }
            })
            // $('#REVW_RsAmount').text(parseFloat(sRsAmount).toFixed(2));


            var BBank = '';
            $.ajax({
                url: 'JQDataFetch/getBankBranchFromTransaction.aspx',
                async: false,
                data: {
                    TID: sTransID
                },
                success: function (response) {
                    BBank = response;
                }
            })
            //$('#REVW_OriginalBank').text(OBank + " / " + BBank);






            displayReviewNotes(sTransID);
            //if (ReviewDocsTable != null)
                ReviewDocsTable.destroy();

            ReviewDocsTable = $('#REWV_Docs').DataTable({
                ajax: {
                    url: 'JQDataFetch/getCustomerOtherFiles.aspx',
                    async: false,
                    data: {
                        CID: sCustID,
                        TID: sTransID,
                    }
                }
            });
           // if (ReviewAuditTable != null)
                ReviewAuditTable.destroy();

            ReviewAuditTable = $("#TBL_REWV_Audit").DataTable({
                initComplete: function () {
                    $("#TBL_REWV_Audit_filter").detach().appendTo('#new-search-area');
                },
                "bLengthChange": false,
                "columnDefs": [
                    { className: "tbl_audit_c1", "targets": [0] },
                    { className: "tbl_audit_c2", "targets": [1] },
                ],
                ajax: {
                    url: 'JQDataFetch/getTransAudit.aspx',
                    async: false,
                    data: {
                        TID: sTransID,
                    }
                },
                

            });


            //alert(sTransID);
            ReviewDialog.dialog('open');
        }

        $(document).ready(function () {

            ReviewDocsTable = $('#REWV_Docs').DataTable({
                "columnDefs": [
                    //{ className: "ali_right", "targets": [0, 1] },
                    { "width": "145px", "targets": 1 }, // TXN REF
                ],
            });

            ReviewAuditTable = $('#TBL_REWV_Audit').DataTable({

            })

            $('#DIV_AllTrans').hide();
            $('#DIV_ComplianceTable').hide();
            $('#DIV_CancelledTable').hide();
            $('#DIV_AdminReview').hide();
            $("#DIV_CompReview_Limits").hide();
            $("#DIV_CompReview_Notes").hide();
            $("#DIV_CompReview_Documents").hide();
            $("#DIV_CompReview_Audit").hide();
            $('#DIV_Cancelledtrans').hide();

            // TRANSACTION
            $("#Review_TD_001").click(function () {
                $("#DIV_CompReview_Main").show();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("wiz_tab_active");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("wiz_tab_inactive_right");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("wiz_tab_inactive_right");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("wiz_tab_inactive_right");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("wiz_tab_inactive_right");
            });

            // LIMITS
            $("#Review_TD_002").click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").show();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("wiz_tab_active");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("wiz_tab_inactive_right");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("wiz_tab_inactive_right");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("wiz_tab_inactive_right");
            });

            // NOTES
            $("#Review_TD_003").click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").show();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").hide();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("wiz_tab_inactive_left");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("wiz_tab_active");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("wiz_tab_inactive_right");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("wiz_tab_inactive_right");
            });

            // DOCUMENTS
            $("#Review_TD_004").click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Audit").hide();
                $("#DIV_CompReview_Documents").show();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("wiz_tab_inactive_left");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("wiz_tab_inactive_left");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("wiz_tab_active");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("wiz_tab_inactive_right");
            });

            $('#Review_TD_005').click(function () {
                $("#DIV_CompReview_Main").hide();
                $("#DIV_CompReview_Limits").hide();
                $("#DIV_CompReview_Notes").hide();
                $("#DIV_CompReview_Documents").hide();
                $("#DIV_CompReview_Audit").show();
                $('#Review_TD_001').removeClass();
                $('#Review_TD_001').addClass("wiz_tab_inactive_left");
                $('#Review_TD_002').removeClass();
                $('#Review_TD_002').addClass("wiz_tab_inactive_left");
                $('#Review_TD_003').removeClass();
                $('#Review_TD_003').addClass("wiz_tab_inactive_left");
                $('#Review_TD_004').removeClass();
                $('#Review_TD_004').addClass("wiz_tab_inactive_left");
                $('#Review_TD_005').removeClass();
                $('#Review_TD_005').addClass("wiz_tab_active");
            })

            $('#PendTD').click(function () {
                $('#DIV_PendingTrans').show();
                $('#DIV_AllTrans').hide();
                $('#DIV_ComplianceTable').hide();
                $('#DIV_CancelledTable').hide();
                $('#DIV_AdminReview').hide();
                $('#PendTD').removeClass();
                $('#PendTD').addClass("tr_tabs_active");
                $('#CompTD').removeClass();
                $('#CompTD').addClass("tr_tabs_inactive_right");
                $('#FailTD').removeClass();
                $('#FailTD').addClass("tr_tabs_inactive_right");
                $('#AllTD').removeClass();
                $('#AllTD').addClass("tr_tabs_inactive_right");
            });

            $("#btn_EditTransaction").click(function () {
                ApproveDialog.dialog('close');
            });

            $("#BTN_CloseReviewWindow").click(function () {
                ReviewDialog.dialog('close');
            });

            $('#FailTD').click(function () {
                $('#DIV_PendingTrans').hide();
                $('#DIV_AllTrans').hide();
                $('#DIV_ComplianceTable').hide();
                $('#DIV_AdminReview').hide();
                $('#DIV_CancelledTable').show();
                $('#PendTD').removeClass();
                $('#PendTD').addClass("tr_tabs_inactive_left");
                $('#CompTD').removeClass();
                $('#CompTD').addClass("tr_tabs_inactive_right");
                $('#FailTD').removeClass();
                $('#FailTD').addClass("tr_tabs_active");
                $('#AllTD').removeClass();
                $('#AllTD').addClass("tr_tabs_inactive_right");
                $("#AdminTD").removeClass();
                $('#AdminTD').addClass("tr_tabs_inactive_right");
            })

            $('#AllTD').click(function () {
                $('#DIV_PendingTrans').hide();
                $('#DIV_AllTrans').show();
                $('#DIV_ComplianceTable').hide();
                $('#DIV_CancelledTable').hide();
                $('#DIV_AdminReview').hide();
                $('#PendTD').removeClass();
                $('#PendTD').addClass("tr_tabs_inactive_left");
                $('#CompTD').removeClass();
                $('#CompTD').addClass("tr_tabs_inactive_left");
                $('#FailTD').removeClass("");
                $('#FailTD').addClass("tr_tabs_inactive_left");
                $('#AllTD').removeClass();
                $('#AllTD').addClass("tr_tabs_active");
                $("#AdminTD").removeClass();
                $('#AdminTD').addClass("tr_tabs_inactive_right");
            });

            $('#CompTD').click(function () {
                $('#DIV_PendingTrans').hide();
                $('#DIV_AllTrans').hide();
                $('#DIV_ComplianceTable').show();
                $('#DIV_CancelledTable').hide();
                $('#DIV_AdminReview').hide();
                $('#PendTD').removeClass();
                $('#PendTD').addClass("tr_tabs_inactive_left");
                $('#CompTD').removeClass();
                $('#CompTD').addClass("tr_tabs_active");
                $('#FailTD').removeClass();
                $('#FailTD').addClass("tr_tabs_inactive_left");
                $('#AllTD').removeClass();
                $('#AllTD').addClass("tr_tabs_inactive_left");
                $("#AdminTD").removeClass();
                $('#AdminTD').addClass("tr_tabs_inactive_right");
            });

            $('#AdminTD').click(function () {
                $('#DIV_PendingTrans').hide();
                $('#DIV_AllTrans').hide();
                $('#DIV_ComplianceTable').hide();
                $('#DIV_CancelledTable').hide();
                $('#DIV_AdminReview').show();
                $('#PendTD').removeClass();
                $('#PendTD').addClass("tr_tabs_inactive_left");
                $('#CompTD').removeClass();
                $('#CompTD').addClass("tr_tabs_inactive_left");
                $('#FailTD').removeClass();
                $('#FailTD').addClass("tr_tabs_inactive_left");
                $('#AllTD').removeClass();
                $('#AllTD').addClass("tr_tabs_inactive_right");
                $("#AdminTD").removeClass();
                $('#AdminTD').addClass("tr_tabs_active");
            })

            ApproveDialog = $('#ApproveTransactionDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "View Transaction",
            });

            CancelledDialog = $('#DIV_Cancelledtrans').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "Cancelled Transaction",
            });

            ReviewDialog = $('#ReviewTransactionDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "Transaction Details",
            })

            EditTransDialog = $('#EditTransactionDiv').dialog({
                modal: true,
                autoOpen: false,
                width: 640,
                title: "Edit Transaction",
            });

            TransVoid = $('#Void_Confirmation').dialog({
                modal: true,
                autoOpen: false,
                width: 600,
                title: "Void Transaction",
            })

            LoadingDIV = $('#DIV_Loading').dialog({
                modal: true,
                autoOpen: false,
                width: 120,
                dialogClass: 'dialog_transparent_background',
            });

            ApprovePINDialog = $('#DIV_ApprovePIN').dialog({
                modal: true,
                autoOpen: false,
                width: 480,
                title: "Complete Transaction Review",
            })

            LoadingDIV.siblings('.ui-dialog-titlebar').remove();

            $('#fundstosend').blur(function () {
                $('#fundstosend').val(parseFloat($('#fundstosend').val()).toFixed(2));
                var avfunds = $('#hidden_sendcalc').val();
                var fts = $('#fundstosend').val();



                if (fts > 1000) {
                    SCharge = 15;
                }
                else {
                    SCharge = 10
                }

                sendp = avfunds - fts - SCharge;
                $('#SM_servicecharge').val(parseFloat(SCharge).toFixed(2));
                $('#availablefunds').val(parseFloat(sendp).toFixed(2));
            })

            $('#btn_void_no').click(function () {
                TransVoid.dialog('close');
            });

            $("#CHK_ConfirmApproval").click(function () {
                if ($('#CHK_ConfirmApproval').is(':checked')) {
                    $("#btn_ConfimApproval_OK").show();
                    $('#REVW_ApprovePIN').show();
                }
                else {
                    $("#btn_ConfimApproval_OK").hide();
                    $('#REVW_ApprovePIN').hide();
                }
            })

            $("#btn_ConfimApproval_OK").click(function () {
                $.ajax({
                    url: 'Processor/updateTransactionQueue.aspx',
                    data: {
                        TID: $("#REVW_TransID").val(),
                        NewStatus: 'PENDING',
                        VR: '',
                    },
                    success: function (response) {
                        alert('Transaction Successfully Approved');
                        RTransTable.ajax.reload(null, false);
                        PTransTable.ajax.reload(null, false);
                        AdminReviewDataTable.ajax.reload(null, false);
                        ReviewDialog.dialog('close');
                        ApprovePINDialog.dialog('close');
                    }
                })
            });

            $('#REVW_btn_Submit').click(function () {
                $("#btn_ConfimApproval_OK").hide();
                ApprovePINDialog.dialog('open');
            })

            $('#btn_void_yes').click(function () {
                $.ajax({
                    url: 'Processor/reverseTransaction.aspx',
                    data: {
                        TID: $("#APPR_TransID").val(),
                        Reason: $('#voidreason').val(),
                    },
                    success: function (data) {
                        alert('Transaction successfully cancelled');
                        TransVoid.dialog('close');
                        ApproveDialog.dialog('close');
                        PTransTable.ajax.reload(null, false);
                    }
                })
            });

            var AdminReviewDataTable = $("#adminreviewtrans").DataTable({
                columns: [
                    { 'data': 'TransactionID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'CustomerID' },
                    { 'data': 'CustomerLastName' },
                    { 'data': 'Mobile' },
                    { 'data': 'CountryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'ServiceCharge' },
                    { 'data': 'RemittedAmount' },
                    { 'data': 'RemittedCurrency' },
                    { 'data': 'Status' },
                ],
                bServerSide: true,
                fixedColumns: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/AdminReviewTransDataHandler.ashx',
                "order": [[0, "desc"]],
                "columnDefs": [
                    { className: "ali_left tr_dt_right_brdr", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 11] },
                    { className: "ali_center tr_dt_right_brdr", "targets": [12] },
                    { className: "ali_right tr_dt_right_brdr", "targets": [8, 9, 10] },
                    { className: "ali_center", "targets": [12] },
                    { "width": "45px", "targets": 0 }, // TXN REF
                    { "width": "60px", "targets": 1 },
                    //{ "width": "145px", "targets": 2 }, // AGENT
                    { "width": "35px", "targets": 3 }, // CUS ID
                    //{ "width": "215px", "targets": 4 }, // CUS NAME
                    { "width": "80px", "targets": 5 }, // MOBILE
                    //{ "width": "100px", "targets": 6 },
                    //{ "width": "150px", "targets": 7 }, // FOREIGN BANK
                    { "width": "60px", "targets": 8 },
                    { "width": "60px", "targets": 9 },
                    { "width": "60px", "targets": 10 },
                    { "width": "20px", "targets": 11 }, // CUR
                    //{ "width": "130px", "targets": 12 }, // STATUS

                ],
            });

            $('#fulltransactions').DataTable({
                columns: [
                    { 'data': 'TransactionID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'theCustomerID' },
                    { 'data': 'CustomerLastName' },
                    { 'data': 'Mobile' },
                    { 'data': 'CountryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'ServiceCharge' },
                    { 'data': 'RemittedAmount' },
                    { 'data': 'RemittedCurrency' },
                    { 'data': 'Status' },
                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/TransactionDataHandler.ashx',
                "order": [[0, "desc"]],
                "columnDefs": [
                    { className: "ali_left tr_dt_right_brdr", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 11] },
                    { className: "ali_center tr_dt_right_brdr", "targets": [12] },
                    { className: "ali_right tr_dt_right_brdr", "targets": [8, 9, 10] },
                    { className: "ali_center", "targets": [12] },
                    { "width": "45px", "targets": 0 }, // TXN REF
                    { "width": "60px", "targets": 1 },
                    //{ "width": "145px", "targets": 2 }, // AGENT
                    { "width": "35px", "targets": 3 }, // CUS ID
                    //{ "width": "215px", "targets": 4 }, // CUS NAME
                    { "width": "80px", "targets": 5 }, // MOBILE
                    //{ "width": "100px", "targets": 6 },
                    //{ "width": "150px", "targets": 7 }, // FOREIGN BANK
                    { "width": "60px", "targets": 8 },
                    { "width": "60px", "targets": 9 },
                    { "width": "60px", "targets": 10 },
                    { "width": "20px", "targets": 11 }, // CUR
                    //{ "width": "130px", "targets": 12 }, // STATUS

                ],

            });

            var PTransTable = $('#pendingtransactions').DataTable({
                columns: [
                    { 'data': 'TransactionID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'CustomerID' },
                    { 'data': 'CustomerLastName' },
                    { 'data': 'Mobile' },
                    { 'data': 'CountryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'ServiceCharge' },
                    { 'data': 'RemittedAmount' },
                    { 'data': 'RemittedCurrency' },
                    { 'data': 'Status' },
                ],
                fixedColumns: true,
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/PendingTransactionsDataHandler.ashx',
                "order": [[0, "desc"]],
                "columnDefs": [
                    { className: "ali_left tr_dt_right_brdr", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 11] },
                    //{ className: "ali_center tr_dt_right_brdr", "targets": [12] }, 
                    { className: "ali_right tr_dt_right_brdr", "targets": [8, 9, 10] },
                    { className: "ali_center", "targets": [12] },
                    { "width": "45px", "targets": 0 }, // TXN REF
                    { "width": "60px", "targets": 1 },
                    //{ "width": "145px", "targets": 2 }, // AGENT
                    { "width": "35px", "targets": 3 }, // CUS ID
                    //{ "width": "215px", "targets": 4 }, // CUS NAME
                    { "width": "80px", "targets": 5 }, // MOBILE
                    //{ "width": "100px", "targets": 6 },
                    //{ "width": "150px", "targets": 7 }, // FOREIGN BANK
                    { "width": "60px", "targets": 8 },
                    { "width": "60px", "targets": 9 },
                    { "width": "60px", "targets": 10 },
                    { "width": "20px", "targets": 11 }, // CUR
                    //{ "width": "130px", "targets": 12 }, // STATUS

                ],



            });

            var CanTransTable = $('#cancelledtrans').DataTable({
                columns: [
                    { 'data': 'TransactionID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'CustomerID' },
                    { 'data': 'CustomerLastName' },
                    { 'data': 'Mobile' },
                    { 'data': 'CountryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'ServiceCharge' },
                    { 'data': 'RemittedAmount' },
                    { 'data': 'RemittedCurrency' },
                    { 'data': 'Status' },
                ],
                bServerSide: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/CancelledTransactionDataHandler.ashx',
                "order": [[0, "desc"]],
                "columnDefs": [
                    { className: "ali_left tr_dt_right_brdr", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 11] },
                    { className: "ali_center tr_dt_right_brdr", "targets": [12] },
                    { className: "ali_right tr_dt_right_brdr", "targets": [8, 9, 10] },
                    { className: "ali_center", "targets": [12] },
                    { "width": "45px", "targets": 0 }, // TXN REF
                    { "width": "60px", "targets": 1 },
                    //{ "width": "145px", "targets": 2 }, // AGENT
                    { "width": "35px", "targets": 3 }, // CUS ID
                    //{ "width": "215px", "targets": 4 }, // CUS NAME
                    { "width": "80px", "targets": 5 }, // MOBILE
                    //{ "width": "100px", "targets": 6 },
                    //{ "width": "150px", "targets": 7 }, // FOREIGN BANK
                    { "width": "60px", "targets": 8 },
                    { "width": "60px", "targets": 9 },
                    { "width": "60px", "targets": 10 },
                    { "width": "20px", "targets": 11 }, // CUR
                    //{ "width": "130px", "targets": 12 }, // STATUS

                ],
                fixedColumns: true,

            })

            var RTransTable = $('#noncomptrans').DataTable({
                columns: [
                    { 'data': 'TransactionID' },
                    { 'data': 'CreatedDateTime' },
                    { 'data': 'AgentName' },
                    { 'data': 'CustomerID' },
                    { 'data': 'CustomerLastName' },
                    { 'data': 'Mobile' },
                    { 'data': 'CountryName' },
                    { 'data': 'BankName' },
                    { 'data': 'DollarAmount' },
                    { 'data': 'ServiceCharge' },
                    { 'data': 'RemittedAmount' },
                    { 'data': 'RemittedCurrency' },
                    { 'data': 'Status' },
                ],
                bServerSide: true,
                fixedColumns: true,
                stateSave: true,
                sAjaxSource: 'DataHandlers/NonCompliantTransDataHandler.ashx',
                "order": [[0, "desc"]],
                "columnDefs": [
                    { className: "ali_left tr_dt_right_brdr", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 11] },
                    { className: "ali_center tr_dt_right_brdr", "targets": [12] },
                    { className: "ali_right tr_dt_right_brdr", "targets": [8, 9, 10] },
                    { className: "ali_center", "targets": [12] },
                    { "width": "45px", "targets": 0 }, // TXN REF
                    { "width": "60px", "targets": 1 },
                    //{ "width": "145px", "targets": 2 }, // AGENT
                    { "width": "35px", "targets": 3 }, // CUS ID
                    //{ "width": "215px", "targets": 4 }, // CUS NAME
                    { "width": "80px", "targets": 5 }, // MOBILE
                    //{ "width": "100px", "targets": 6 },
                    //{ "width": "150px", "targets": 7 }, // FOREIGN BANK
                    { "width": "60px", "targets": 8 },
                    { "width": "60px", "targets": 9 },
                    { "width": "60px", "targets": 10 },
                    { "width": "20px", "targets": 11 }, // CUR
                    //{ "width": "130px", "targets": 12 }, // STATUS

                ],
            })

            $('#btn_Cancel').click(function () {
                ApproveDialog.dialog('close');
            });

            $("#btn_SaveNewNote").click(function () {
                if ($('#TXT_Add_TRN_Notes').val() != "") {
                    $.ajax({
                        url: 'Processor/AddNewTransNote.aspx',
                        data: {
                            TID: $("#REVW_TransID").val(),
                            CID: $('#REVW_CustID').val(),
                            Notes: $('#TXT_Add_TRN_Notes').val(),
                            SendNotif: $('#CHK_NotifyAffiliate').is(":checked"),
                        },
                        success: function (response) {
                            //alert('Notes added successfully');
                            $("#TXT_Add_TRN_Notes").val('');
                            $('#DIV_Notes').empty();
                            displayReviewNotes($("#REVW_TransID").val());
                        }
                    })
                }
            });

            

            $('#benelist').change(function () {
                $.ajax({
                    url: 'JQDataFetch/getBeneficiaryAccountsInOneLine.aspx',
                    data: {
                        BeneID: $('#benelist').val(),
                    },
                    success: function (data) {
                        $('#accountlist').empty();
                        var splitdata = data.split('~');
                        //alert(data);
                        splitdata.sort();
                        $.each(splitdata, function (indi) {
                            var interndata = splitdata[indi].split('|');
                            $('#accountlist').append('<option value="' + interndata[1] + '">' + interndata[0] + '</option>');
                        });
                    }
                });
            });

            $("#adminreviewtrans tbody").on('click', 'tr', function (event) {
                var id = this.id;

                var sTransID = $('td', this).eq(0).text();
                $("#txt_workingTransID").val(sTransID);
                var sCustID = $('td', this).eq(3).text();
                var sDollarAmount = $('td', this).eq(8).text();
                var sRate = $('td', this).eq(9).text();
                var OBank = $('td', this).eq(7).text();
                var sOrigin = "RTransTable"
                $("#txt_TransTotal").val(sDollarAmount);
                var sStatus = $('td', this).eq(12).text();

                openReviewDialog(sTransID, sCustID, sDollarAmount, sRate, OBank, sOrigin, sStatus)
            })

            $('#noncomptrans tbody').on('click', 'tr', function (event) {
                var id = this.id;

                var sTransID = $('td', this).eq(0).text();
                $("#txt_workingTransID").val(sTransID);
                var sCustID = $('td', this).eq(3).text();
                var sDollarAmount = $('td', this).eq(8).text();
                var sRate = $('td', this).eq(9).text();
                var OBank = $('td', this).eq(7).text();
                var sOrigin = "RTransTable"
                $("#txt_TransTotal").val(sDollarAmount);
                var sStatus = $('td', this).eq(12).text();

                openReviewDialog(sTransID, sCustID, sDollarAmount, sRate, OBank, sOrigin, sStatus)
            });

            $('#cancelledtrans tbody').on('click', 'tr', function (event) {
                var id = this.id;;

                var sTransID = $('td', this).eq(0).text();
                var sCustID = $('td', this).eq(3).text();
                var OBank = $('td', this).eq(6).text();
                var sDollarAmount = $('td', this).eq(8).text();
                var sRate = $('td', this).eq(9).text();
                var sStatus = $('td', this).eq(11).text();
                var sOrigin = "CanTransTable";

                openReviewDialog(sTransID, sCustID, sDollarAmount, sRate, OBank, sOrigin, sStatus);
            });

            $('#fulltransactions tbody').on('click', 'tr', function (event) {
                //var id = this.id;

                //var sTransID = $('td', this).eq(0).text();
                //var sTransStatus = $('td', this).eq(11).text();

                var id = this.id;;

                var sTransID = $('td', this).eq(0).text();
                var sCustID = $('td', this).eq(3).text();
                var OBank = $('td', this).eq(6).text();
                var sDollarAmount = $('td', this).eq(8).text();
                var sRate = $('td', this).eq(9).text();
                var sStatus = $('td', this).eq(12).text();
                var sOrigin = "FTransTable";

                openReviewDialog(sTransID, sCustID, sDollarAmount, sRate, OBank, sOrigin, sStatus);




                //$('#CAN_TransID').val(sTransID);


            });


            $('#pendingtransactions tbody').on('click', 'tr', function (event) {
                var id = this.id;;

                var sTransID = $('td', this).eq(0).text();
                var sCustID = $('td', this).eq(3).text();
                var OBank = $('td', this).eq(6).text();
                var sDollarAmount = $('td', this).eq(8).text();
                var sRate = $('td', this).eq(9).text();
                var sStatus = $('td', this).eq(12).text();
                var sOrigin = "PTransTable";

                openReviewDialog(sTransID, sCustID, sDollarAmount, sRate, OBank, sOrigin, sStatus);
                //openEditDialog(sTransID, sCustID, OBank, sRate, sDollarAmount, sStatus, sOrigin);
            });

            $('#btn_ApproveAPI').click(function () {
                if ($('#RoutingBankddl').val() == "7278") {
                    $.ajax({
                        url: 'APIValidations/SAMPATH_processTransaction.aspx',
                        data: {
                            TID: $('#APPR_TransID').val(),
                        },
                        success: function (response) {
                            alert(response);
                            ApproveDialog.dialog('close');

                        }
                    })
                }
            });

            $('#btn_VoidTransaction').click(function () {
                TransVoid.dialog('open');
            });

            $('#btn_OverideRoutingBank').click(function () {
                $('#RoutingBankddl').attr('disabled', false);
                $('#btn_OverideRoutingBank').hide();
            });

            $('#btn_SaveTrans').click(function () {
                $.ajax({
                    url: 'Processor/updateTransaction.aspx',
                    data: {
                        TID: $('#APPR_TransID').val(),
                        BID: $('#benelist option:selected').val(),
                        BName: $('#benelist option:selected').text(),
                        AID: $('#accountlist option:selected').val(),
                        DAmount: $('#fundstosend').val(),
                        SCharge: $("#SM_servicecharge").val(),
                        avfunds: $('#availablefunds').val(),
                    },
                    success: function (data) {
                        alert('Transaction successfully updated');
                        location.reload();
                    }
                })
            });

            $('#RoutingBankddl').change(function () {
                $('#TR_RoutingInfo').hide();
                $('#TR_RoutingInfo_5px').hide();
            })

            $('#btn_Submit').click(function () {
                $.ajax({
                    url: 'Processor/ApproveTransaction.aspx',
                    beforeSend: function () {
                        ApproveDialog.dialog('close');
                        LoadingDIV.dialog('open');
                    },
                    data: {
                        TransID: $('#APPR_TransID').val(),
                        RBank: $('#RoutingBankddl').val(),
                    },
                    complete: function () {
                        LoadingDIV.dialog('close');
                    },
                    success: function (data) {
                        alert('successfully approved');
                        location.reload();
                    }
                })
            });

           
           // setTimeout(function () { openReviewDialog(305099, 7972, 300.0000, 113.1000, 'SAMPATH BANK', 'Personal Savings', 'SUCCESSFUL'); }, 3000); 

            callTransactionPopUpAuto();

        });

        function callTransactionPopUpAuto() {

            var propVal = $("#tranObJectVal").val();
            var doamount = $("#dollaramount").val();
           // console.log($("#dollaramount").val());
            var dorate = $("#rateTran").val();
            var cusIdVal = $("#cusIdVal ").val();
            var tranIDval = $("#tranIDval").val();
            var banckOrign = $("#banckOrign").val();
            var bankName = $("#bankName ").val();
            if (propVal != "") {
               // console.log(tranIDval, cusIdVal, doamount, dorate, bankName, banckOrign, propVal);
                openReviewDialog(tranIDval, cusIdVal, doamount, dorate, bankName, banckOrign, propVal);
            }


        }

    </script>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMainSection" runat="server">

    <form runat="server">
        <asp:HiddenField ID="tranObJectVal" ClientIDMode="Static" runat="server" />
         <asp:HiddenField ID="dollaramount" ClientIDMode="Static" runat="server" /> 
        <asp:HiddenField ID="tranIDval" ClientIDMode="Static" runat="server" />
         <asp:HiddenField ID="rateTran" ClientIDMode="Static" runat="server" />
         <asp:HiddenField ID="cusIdVal" ClientIDMode="Static" runat="server" />
         <asp:HiddenField ID="banckOrign" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="bankName" ClientIDMode="Static" runat="server" />

        <div class="container_top_headings">
            <table class="tbl_width_1200">
                <tr style="height: 70px; vertical-align: middle;">
                    <td style="vertical-align: middle;"><span class="all_headings">Transactions</span></td>
                    <td style="vertical-align: middle; text-align: right;">&nbsp;</td>
                </tr>
            </table>
        </div>


        <div class="container_one no_padding_btm wiz_bg_EFEFEF">
            <!-- TABS START -->
            <table class="tbl_width_1200">
                <tr>
                    <td>
                        <table class="tbl_width_1200">
                            <tr>
                                <td style="width: 150px;" class="tr_tabs_active" id="PendTD">Pending Transactions</td>
                                <td style="width: 150px;" class="tr_tabs_inactive_right" id="FailTD">Awaiting Transfer</td>
                                <td style="width: 150px;" class="tr_tabs_inactive_right" id="CompTD">Compliance Review</td>
                                <td style="width: 150px;" class="tr_tabs_inactive_right" id="AllTD">All Transactions</td>
                                <td style="width: 150px; color: #ee4055;" class="tr_tabs_inactive_right" id="AdminTD">Admin Review</td>
                                <td style="width: 150px;">
                                    <input type="text" id="txt_workingTransID" hidden="hidden" /><input type="text" id="txt_workingDialog" hidden="hidden" /><input type="text" id="txt_IsComplianceManager" hidden="hidden" runat="server" /><input type="text" id="txt_hiddenCompliancePIN" hidden="hidden" runat="server" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 5px solid #76D17F; background-color: #FFFFFF; height: 20px;">&nbsp;</td>
                </tr>

            </table>
        </div>
        <!-- TABS END -->

        <div class="container_one no_padding_top wiz_bg_EFEFEF" id="DIV_PendingTrans">
            <!-- PENDING TRANSACTIONS -->
            <table class="tbl_width_1200 background_FFFFFF">
                <!-- MAIN TABLE STARTS HERE -->
                <tr>
                    <td>
                        <table class="tbl_width_1160">

                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" id="pendingtransactions">
                                        <thead>
                                            <tr>
                                                <td>TXN REF</td>
                                                <td>DATE</td>
                                                <td>AGENT</td>
                                                <td>CUS ID</td>
                                                <td>CUS NAME</td>
                                                <td>MOBILE</td>
                                                <td>COUNTRY</td>
                                                <td>FOREIGN BANK</td>
                                                <td>SND AMT</td>
                                                <td>TXN FEE</td>
                                                <td>REC AMT</td>
                                                <td>CUR</td>
                                                <td>STATUS</td>
                                            </tr>
                                        </thead>

                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <div class="container_one no_padding_top wiz_bg_EFEFEF" id="DIV_AllTrans">
            <table class="tbl_width_1200 background_FFFFFF no_padding_top">
                <!-- MAIN TABLE STARTS HERE -->
                <tr>
                    <td>
                        <table class="tbl_width_1160">
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" id="fulltransactions">
                                        <thead>
                                            <tr>
                                                <td>TXN REF</td>
                                                <td>DATE</td>
                                                <td>AGENT</td>
                                                <td>CUS ID</td>
                                                <td>CUS NAME</td>
                                                <td>MOBILE</td>
                                                <td>COUNTRY</td>
                                                <td>FOREIGN BANK</td>
                                                <td>AMOUNT</td>
                                                <td>TXN FEE</td>
                                                <td>F AMOUNT</td>
                                                <td>CUR</td>
                                                <td>STATUS</td>
                                            </tr>
                                        </thead>

                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- MAIN TABLE ENDS HERE -->
        </div>


        <div class="container_one no_padding_top wiz_bg_EFEFEF" id="DIV_ComplianceTable">
            <table class="tbl_width_1200 background_FFFFFF no_padding_top">
                <!-- MAIN TABLE STARTS HERE -->
                <tr>
                    <td>
                        <table class="tbl_width_1160">
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" id="noncomptrans">
                                        <thead>
                                            <tr>
                                                <td>TXN REF</td>
                                                <td>DATE</td>
                                                <td>AGENT</td>
                                                <td>CUS ID</td>
                                                <td>CUS NAME</td>
                                                <td>MOBILE</td>
                                                <td>COUNTRY</td>
                                                <td>FOREIGN BANK</td>
                                                <td>SND AMT</td>
                                                <td>TXN FEE</td>
                                                <td>REC AMT</td>
                                                <td>CUR</td>
                                                <td>STATUS</td>
                                            </tr>
                                        </thead>

                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- MAIN TABLE ENDS HERE -->
        </div>


        <div class="container_one no_padding_top wiz_bg_EFEFEF" id="DIV_CancelledTable">
            <table class="tbl_width_1200 background_FFFFFF no_padding_top">
                <tr>
                    <td>
                        <table class="tbl_width_1160">
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" id="cancelledtrans">
                                        <thead>
                                            <tr>
                                                <td>TXN REF</td>
                                                <td>DATE</td>
                                                <td>AGENT</td>
                                                <td>CUS ID</td>
                                                <td>CUS NAME</td>
                                                <td>MOBILE</td>
                                                <td>COUNTRY</td>
                                                <td>FOREIGN BANK</td>
                                                <td>SND AMT</td>
                                                <td>TXN FEE</td>
                                                <td>REC AMT</td>
                                                <td>CUR</td>
                                                <td>STATUS</td>
                                            </tr>
                                        </thead>

                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- MAIN TABLE ENDS HERE -->
        </div>

        <div class="container_one no_padding_top wiz_bg_EFEFEF" id="DIV_AdminReview">
            <table class="tbl_width_1200 background_FFFFFF no_padding_top">
                <!-- MAIN TABLE STARTS HERE -->
                <tr>
                    <td>
                        <table class="tbl_width_1160">
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" id="adminreviewtrans">
                                        <thead>
                                            <tr>
                                                <td>TXN REF</td>
                                                <td>DATE</td>
                                                <td>AGENT</td>
                                                <td>CUS ID</td>
                                                <td>CUS NAME</td>
                                                <td>MOBILE</td>
                                                <td>COUNTRY</td>
                                                <td>FOREIGN BANK</td>
                                                <td>SND AMT</td>
                                                <td>TXN FEE</td>
                                                <td>REC AMT</td>
                                                <td>CUR</td>
                                                <td>STATUS</td>
                                            </tr>
                                        </thead>

                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- MAIN TABLE ENDS HERE -->
        </div>
    </form>


    <div id="ApproveTransactionDiv">
        <!-- THIS IS THE APPROVAL POPUP DIALOG BOX -->
        <div class="edit__form__main">
            <form method="get" id="approvetransactionform">
                <table class="tbl_width_600">


                    <tr>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>
                            <table class="tbl_width_600">
                                <tr style="height: 50px;">
                                    <td style="width: 50px;" class="wiz_tab_active" id="Approve_TD_001">
                                        <asp:Image ID="Image9" runat="server" ImageUrl="~/images/paid.png" title="TRANSACTION" /></td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                    <td style="width: 50px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="wiz_bg_EFEFEF" style="height: 25px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td class="wiz_bg_EFEFEF">
                            <table class="tbl_width_560">
                                <tr>
                                    <td class="txn_sml_headings">Transaction Reference:&nbsp;<span style="font-weight: 600;" id="APPR_TransIDUpper"></span></td>
                                    <td style="width: 5px; background-color: #EFEFEF;">&nbsp;</td>
                                    <td style="width: 145px;" class="trn_details_country"><span id="APPR_CountryName"></span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="spacer10 wiz_bg_EFEFEF">
                        <td>&nbsp;</td>
                    </tr>

                    <tr class="wiz_bg_EFEFEF">
                        <td>
                            <table class="tbl_width_560">
                                <tr>
                                    <td class="trn_popup_top_001" style="background-color: #D6DFDE; width: 145px; height: 50px;"><span id="APPR_DollarAmount"></span></td>
                                    <td style="width: 5px;">&nbsp;</td>
                                    <td class="trn_popup_top_002" style="background-color: #D6DFDE; width: 90px;"><span id="APPR_ServiceFee"></span></td>
                                    <td style="width: 5px;">&nbsp;</td>
                                    <td class="trn_popup_top_002" style="background-color: #D6DFDE; width: 90px;"><span id="APPR_AUDAmount"></span></td>
                                    <td style="width: 5px;">&nbsp;</td>
                                    <td class="trn_popup_top_002" style="background-color: #D6DFDE; width: 90px;"><span id="APPR_Rate"></span></td>
                                    <td style="width: 5px;">&nbsp;</td>
                                    <td class="trn_popup_top_005" style="background-color: #76D17F; width: 145px;"><span id="APPR_RsAmount"></span></td>
                                </tr>
                                <tr>
                                    <td class="trn_popup_headings">Send Amount</td>
                                    <td>&nbsp;</td>
                                    <td class="trn_popup_headings">Fee</td>
                                    <td>&nbsp;</td>
                                    <td class="trn_popup_headings">Total</td>
                                    <td>&nbsp;</td>
                                    <td class="trn_popup_headings">Exchange Rate</td>
                                    <td>&nbsp;</td>
                                    <td class="trn_popup_headings">Receive Amount</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="wiz_bg_EFEFEF spacer10">
                        <td>&nbsp;</td>
                    </tr>

                    <tr class="wiz_bg_EFEFEF">
                        <td>
                            <table class="tbl_width_560">
                                <tr>
                                    <td style="background-color: #EFEFEF; width: 275px;">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td class="trn_details_001">SENDER Details</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">CUSTOMER ID</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_CustomerID"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">NAME</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_CustName"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">ADDRESS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_CustAddress"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">MOBILE NUMBER</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_CustMobile"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">EMAIL</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_CustEmail"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">PURPOSE OF TRANSACTION</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_TransPurpose"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">SOURCE OF FUNDS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_TransSourceOfFunds"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">NOTES</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_TransNotes"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>


                                        </table>
                                    </td>
                                    <td style="width: 10px; height: 120px;">&nbsp;</td>
                                    <td style="width: 275px;">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td class="trn_details_001">RECEIVER Details</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">NAME</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_BeneName"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">ADDRESS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_BeneAddress"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">ID DETAILS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">BANK NAME</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_OriginalBank"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">BANK ADDRESS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_OriginalBankAccount"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">ACCOUNT NAME</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_AccountName"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">ACCOUNT NUMBER</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_BeneAccountID"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="trn_details_003_heading">TRANSACTION TYPE</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_details_004_details"><span id="APPR_TransType"></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                    <tr class="wiz_bg_EFEFEF" style="height: 20px;">
                        <td>&nbsp;</td>
                    </tr>

                    <tr class="spacer10">
                        <td>&nbsp;</td>
                    </tr>

                    <tr hidden="hidden">
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <input id="APPR_TransID" name="APPR_TransID" type="text" />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>

            </form>

        </div>
    </div>

    <div id="ReviewTransactionDiv">
        <!-- THIS IS THE COMPLAINCE REVIEW POPUP DIALOG BOX -->

        <table class="tbl_width_600">

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr style="height: 50px;">
                            <td style="width: 50px;" class="wiz_tab_active" id="Review_TD_001">
                                <asp:Image ID="Image11" runat="server" ImageUrl="~/images/paid.png" title="TRANSACTION" /></td>
                            <td style="width: 50px;" class="wiz_tab_inactive_right" id="Review_TD_002">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/profits.png" title="LIMITS" /></td>
                            <td style="width: 50px;" class="wiz_tab_inactive_right" id="Review_TD_003">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/paper.png" title="NOTES" /></td>
                            <td style="width: 50px;" class="wiz_tab_inactive_right" id="Review_TD_004">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/folder.png" title="DOCUMENTS" /></td>
                            <td style="width: 50px;" class="wiz_tab_inactive_right" id="Review_TD_005">
                                <asp:Image ID="Image19" runat="server" ImageUrl="~/images/audit.png" title="AUDIT" /></td>
                            <td style="width: 50px;">&nbsp;</td>
                            <td style="width: 50px;">&nbsp;</td>
                            <td style="width: 50px;">&nbsp;</td>
                            <td style="width: 50px;">&nbsp;</td>
                            <td style="width: 50px;">&nbsp;</td>
                            <td style="width: 50px;">&nbsp;</td>
                            <td style="width: 50px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td class="wiz_bg_EFEFEF" style="height: 25px;">&nbsp;</td>
            </tr>

            <!--<tr class="wiz_bg_EFEFEF">
            <td>
                <table class="tbl_width_560">
                    <tr><td class="settings_headings">Transaction Details - <span id="REWV_TransID"></span>&nbsp;<span id="REWV_DateTime"></span></td></tr>
                    <tr class="spacer5"><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>-->

            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">
                        <!--<tr>
                        <td>
                            <table class="tbl_width_560">
                                <tr class="">
                                    <td style="width:54px; vertical-align:top;"><table style="width:54px;"><tr><td class="txn_status_line_green spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/trn_ok.png" title="TRANSACTION" /></td>
                                    <td style="width:108px; vertical-align:top;"><table style="width:108px;"><tr><td class="txn_status_line_green spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/trn_ok.png" title="TRANSACTION" /></td>
                                    <td style="width:108px; vertical-align:top;"><table style="width:108px;"><tr><td class="txn_status_line_gray spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image4" runat="server" ImageUrl="~/images/trn_no.png" title="TRANSACTION" /></td>
                                    <td style="width:108px; vertical-align:top;"><table style="width:108px;"><tr><td class="txn_status_line_gray spacer10">&nbsp;</td></tr></table></td>
                                    <td style="width:32px;"><asp:Image ID="Image5" runat="server" ImageUrl="~/images/trn_no.png" title="TRANSACTION" /></td>
                                    <td style="width:54px; vertical-align:top;"><table style="width:54px;"><tr><td class="txn_status_line_gray spacer10">&nbsp;</td></tr></table></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr class="">
                        <td>
                            <table class="tbl_width_560">
                                <tr>
                                    <td class="txn_status_headings">TXN CREATE</td>
                                    <td class="txn_status_headings">TXN REVIEW</td>
                                    <td class="txn_status_headings">TXN APPROVE</td>
                                    <td class="txn_status_headings">TXN COMPLETE</td>
                                </tr>
                            </table>
                        </td>
                    </tr> -->

                        <tr hidden="hidden" style="background-color: #FBE2E2;">
                            <td>
                                <table class="tbl_width_520" id="TBL_ReviewReasons">
                                </table>
                            </td>
                        </tr>
                        <tr hidden="hidden" class="wiz_seperator_white spacer10">
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <section class="container_tags">
                                    <ul class="tags" id="LST_ReviewReasons"></ul>
                                </section>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>


            <tr class="wiz_bg_EFEFEF">
                <td>
                    <table class="tbl_width_560">
                        <tr>
                            <td style="background-color: #FFF; height: 35px;">
                                <table class="tbl_width_560">
                                    <tr style="height: 35px;">
                                        <td style="width: 15px;">&nbsp;</td>
                                        <td class="txn_sml_headings">TXN REF: <span id="REVW_DisplayTransID"></span>&nbsp; - &nbsp;<span id="SPN_Status"></span></td>
                                        <!--<td style="width: 40px;" class="txn_btns_01">&nbsp;</td>
                                        <td style="width: 40px;" class="txn_btns_01">&nbsp;</td>-->
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>






            <tr class="wiz_bg_EFEFEF spacer10">
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>
                    <div id="DIV_CompReview_Main">
                        <!-- MAIN TAB -->
                        <form method="get" id="reviewtransactionform">
                            <table class="tbl_width_600">

                                <tr hidden="hidden">
                                    <td>
                                        <input id="REVW_TransID" name="REVW_TransID" type="text" /><input id="REVW_CustID" name="REVW_CustID" type="text" /></td>
                                </tr>




                                <tr class="wiz_bg_EFEFEF">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td style="width: 275px;">
                                                    <table style="width: 100%;">
                                                        <tr class="background_FFFFFF"><td class="inside_headings" style="height: 40px;">SEND AMOUNT</td></tr>
                                                        <tr class="background_FFFFFF"><td style="padding-left:15px;"><span id="REVW_DollarAmount" class="trn_amounts_gray"></span></td></tr>
                                                        <tr class="background_FFFFFF spacer10"><td>&nbsp;</td></tr>
                                                        <tr style="background-color: #F7F9FA; border-top: 1px solid rgb(234, 234, 234);"><td class="txn_popup_footer_01">Transaction Fee: <span id="REVW_ServiceFee" class="txn_popup_fw600"></span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Total Paid: <span id="REVW_AUDAmount" class="txn_popup_fw600"></span></td></tr>
                                                    </table>
                                                </td>
                                                <td style="width: 10px;">&nbsp;</td>
                                                <td style="width: 275px;">
                                                    <table style="width: 100%;">
                                                        <tr class="background_FFFFFF"><td class="inside_headings" style="height: 40px;">RECEIVE AMOUNT</td></tr>
                                                        <tr class="background_FFFFFF"><td style="padding-left:15px;"><span id="REVW_RsAmount" class="trn_amounts_green" ></span></td></tr>
                                                        <tr class="background_FFFFFF spacer10"><td>&nbsp;</td></tr>
                                                        <tr style="background-color: #F7F9FA; border-top: 1px solid rgb(234, 234, 234);"><td class="txn_popup_footer_01">Exchange Rate: <span id="REVW_Rate" class="txn_popup_fw600"></span></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>





                                <tr class="wiz_bg_EFEFEF spacer10">
                                    <td>&nbsp;</td>
                                </tr>

                                <tr class="wiz_bg_EFEFEF">
                                    <td>
                                        <table class="tbl_width_560">
                                            <tr>
                                                <td style="width: 275px; background-color: #FFF;">
                                                    <table class="tbl_width_245">
                                                        <tr class="background_FFFFFF">
                                                            <td class="inside_headings" style="padding-left: 0px; height: 50px;">SENDER DETAILS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">CUSTOMER ID/NAME</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_CustomerID"></span>&nbsp;-&nbsp;<span id="REVW_CustName"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">ADDRESS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_CustAddress"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">CONTACT NO</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_CustMobile"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">AFFILIATE</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_AffiliateName"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">OCCUPATION</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_Occupation"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">PURPOSE OF TRANSACTION</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_Purpose"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">SOURCE OF FUNDS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_SourceOfFunds"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">NOTES</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_Notes"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 10px;">&nbsp;</td>
                                                <td style="width: 275px; background-color: #FFF;">
                                                    <table class="tbl_width_245">
                                                        <tr class="background_FFFFFF">
                                                            <td class="inside_headings" style="padding-left: 0px; height: 50px;">RECEIVER DETAILS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">NAME</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_BeneName"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">ADDRESS</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_BeneAddress"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">CONTACT NO</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_BeneMobile"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">RELATIONSHIP</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_BeneRelationship"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_h_02">BANK</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txn_popup_c_01"><span id="REVW_OriginalBank"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="wiz_bg_EFEFEF">
                                    <td>
                                        <table class="tbl_width_560">


                                            <tr hidden="hidden" class="background_FFFFFF">
                                                <td>
                                                    <table class="tbl_width_520">
                                                        <tr class="spacer10">
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="tbl_width_520">
                                                                    
                                                                    <tr>
                                                                        <td class="trn_popup_heading">Receive Option</td>
                                                                        <td class="trn_popup_details"><span id="REVW_TransType"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="trn_popup_heading">Destination</td>
                                                                        <td class="trn_popup_details"><span id="REVW_Destination"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="trn_popup_heading">Fee</td>
                                                                        <td class="trn_popup_details"><span id="REVW_Fee"></span></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>

                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="height: 20px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <!-- <tr class="spacer5" id="REVW_TR_RoutingInfo_5px"><td>What is this?</td></tr> -->

                            </table>
                        </form>
                    </div>

                    <div id="DIV_CompReview_Notes">
                        <!-- NOTES TAB -->
                        <table class="tbl_width_600">
                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr class="background_FFFFFF">
                                            <td>
                                                <table class="tbl_width_530">
                                                    <tr class="background_FFFFFF">
                                                        <td class="inside_headings" style="padding-left: 0px; height: 45px;">ADD A NEW TRANSACTION NOTE</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <textarea name="TXT_Add_TRN_Notes" id="TXT_Add_TRN_Notes" style="width: 100%; height: 55px; resize: none; line-height:normal !important; padding-top: 8px;" class="aa_input"></textarea></td>
                                                    </tr>
                                                    <tr class="spacer5"><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td>
                                                            <table class="tbl_width_530">
                                                                <tr>
                                                                    <td style="vertical-align:top; width:35px;"><label class="chk_approve"><input id="CHK_NotifyAffiliate" type="checkbox" checked="checked" /><span class="checkmark"></span></label></td>
                                                                    <td>Notify Affiliate</td>
                                                                    <td class="ali_right">
                                                                        <input class="aa_btn_green" id="btn_SaveNewNote" type="button" value="Add Note" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="spacer15"><td>&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="wiz_bg_EFEFEF spacer10"><td>&nbsp;</td></tr>

                            <tr class="wiz_bg_EFEFEF" id="TR_Notes" style="display:none;">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr class="background_FFFFFF">
                                            <td class="inside_headings" style="padding-left:15px; height: 45px;">TRANSACTION NOTES</td>
                                        </tr>
                                        <tr class="background_FFFFFF">
                                            <td>
                                                <div id="DIV_Notes" style="overflow-y: auto; height: 250px;" class="tbl_width_530"></div>
                                            </td>
                                        </tr>
                                        <tr class="background_FFFFFF spacer15"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="wiz_bg_EFEFEF" style="height: 20px;"><td>&nbsp;</td></tr>
                        </table>
                    </div>

                    <div id="DIV_CompReview_Documents">
                        <!-- DOCUMENTS TAB -->
                        <table class="tbl_width_600">
                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">

                                        <tr class="background_FFFFFF">
                                            <td>
                                                <table class="tbl_width_530">
                                                    <tr class="background_FFFFFF">
                                                        <td class="inside_headings" style="padding-left:0px; height: 45px;">UPLOAD A SUPPORTING DOCUMENT</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="trn_help_font">Please upload any required supporting documents for this transaction below. This may include proof of funds, purpose of transfer, etc. If you need to update/edit any identification documents please do that under customer profile. </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="background_FFFFFF"><td>&nbsp;</td></tr>

                                        <tr>
                                            <td class="background_FFFFFF">
                                                <form id="doc_UploadOtherDocs" method="get">
                                                    <table class="tbl_width_530">
                                                        <tr>
                                                            <td class="aa_label_font">DOCUMENT DESCRIPTION</td>
                                                            <td class="aa_label_font">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <input class="aa_input" id="docdescription" name="docdescription" style="width: 430px;" type="text" value="" />
                                                                </div>
                                                            </td>
                                                            <td style="text-align: right; vertical-align:top;">
                                                                <input type="file" name="UploadFile1" id="UploadFile1" style="display: none;" /><input type="button" class="aa_btn_green" value="Browse..." onclick="document.getElementById('UploadFile1').click();" /></td>
                                                        </tr>
                                                        <tr class="spacer5">
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trn_notes_popup_details" style="font-size: 0.7em; color: #999;">Selected File: <span id="otherdoctext" style="color: #666;">Please browse for a file...</span></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr class="spacer5">
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="aa_btn_red" id="btn_UploadOtherFiles" type="button" value="Upload File" /></td>
                                                            <td style="text-align: right;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </td>
                                        </tr>

                                        <tr class="background_FFFFFF spacer15"><td>&nbsp;</td></tr>

                                        <tr class="spacer10"><td>&nbsp;</td></tr>

                                        <tr class="background_FFFFFF">
                                            <td class="inside_headings" style="padding-left:15px; height: 50px;">SUPPORTING DOCUMENTS</td>
                                        </tr>

                                        <tr class="background_FFFFFF">
                                            <td>
                                                <table class="tbl_width_530">
                                                    <tr>
                                                        <td>
                                                            <table id="REWV_Docs">
                                                                <thead>
                                                                    <!-- <td>Date</td> INCLUDE THIS -->
                                                                    <td>DOCUMENT DESCRIPTION</td>
                                                                    <td>VIEW</td>
                                                                </thead>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr class="spacer15 background_FFFFFF"><td>&nbsp;</td></tr>

                                        <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div id="DIV_CompReview_Limits">
                        <!-- LIMITS TAB -->
                        <table class="tbl_width_600">
                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr class="background_FFFFFF">
                                            <td style="width: 275px;">
                                                <table style="width:100%">
                                                    <tr class="background_FFFFFF">
                                                        <td>
                                                            <table class="tbl_width_245">
                                                                <tr><td class="inside_headings" style="padding-left:0px; height: 40px;">MONTHLY AUD AMOUNT</td><td>&nbsp;</td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr><td style="padding-left:15px;"><span id="Last30daysDollar"></span><span class="trn_amounts_limits">&nbsp;&nbsp;Last 30 Days</span></td></tr>
                                                    <tr><td class="spacer10">&nbsp;</td></tr>
                                                    <tr style="background-color: #F7F9FA; border-top: 1px solid rgb(234, 234, 234);">
                                                        <td class="txn_popup_footer_01">Monthly Affiliate Limit: <span id="ALast30daysDollar" class="txn_popup_fw600"></span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10px;" class="wiz_bg_EFEFEF">&nbsp;</td>
                                            <td style="width: 275px;">
                                                <table style="width:100%">
                                                    <tr class="background_FFFFFF">
                                                        <td>
                                                            <table class="tbl_width_245">
                                                                <tr><td class="inside_headings" style="padding-left:0px; height: 40px;">YEARLY AUD AMOUNT</td><td>&nbsp;</td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr><td style="padding-left:15px;"><span id="LastYearDollar" ></span><span class="trn_amounts_limits">&nbsp;&nbsp;Last 365 Days</span></td></tr>
                                                    <tr><td class="spacer10">&nbsp;</td></tr>
                                                    <tr style="background-color: #F7F9FA; border-top: 1px solid rgb(234, 234, 234);">
                                                        <td class="txn_popup_footer_01">Yearly Affiliate Limit: <span id="ALastYearDollar" class="txn_popup_fw600"></span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="spacer10">
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr class="background_FFFFFF">
                                            <td style="width: 275px;">
                                                <table style="width:100%">
                                                    <tr class="background_FFFFFF">
                                                        <td>
                                                            <table class="tbl_width_245">
                                                                <tr><td class="inside_headings" style="padding-left:0px; height: 40px;">MONTHLY TRANSACTIONS</td><td>&nbsp;</td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr><td style="padding-left:15px;"><span id="Last30daysNb"></span><span class="trn_amounts_limits">&nbsp;&nbsp;Last 30 Days</span></td></tr>
                                                    <tr><td class="spacer10">&nbsp;</td></tr>
                                                    <tr style="background-color: #F7F9FA; border-top: 1px solid rgb(234, 234, 234);">
                                                        <td class="txn_popup_footer_01">Monthly Affiliate Limit: <span id="ALast30daysNb" class="txn_popup_fw600"></span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10px;" class="wiz_bg_EFEFEF">&nbsp;</td>
                                            <td style="width: 275px;">
                                                <table style="width:100%">
                                                    <tr class="background_FFFFFF">
                                                        <td>
                                                            <table class="tbl_width_245">
                                                                <tr><td class="inside_headings" style="padding-left:0px; height: 40px;">YEARLY TRANSACTIONS</td><td>&nbsp;</td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr><td style="padding-left:15px;"><span id="LastYearNb"></span><span class="trn_amounts_limits">&nbsp;&nbsp;Last 365 Days</span></td></tr>
                                                    <tr><td class="spacer10">&nbsp;</td></tr>
                                                    <tr style="background-color: #F7F9FA; border-top: 1px solid rgb(234, 234, 234);">
                                                        <td class="txn_popup_footer_01">Yearly Affiliate Limit: <span id="ALastYearNb" class="txn_popup_fw600"></span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr class="wiz_bg_EFEFEF" style="height: 20px;">
                                <td>&nbsp;</td>
                            </tr>
                        </table>

                    </div>

                    <div id="DIV_CompReview_Audit">
                        <!-- AUDIT TAB -->
                        <table class="tbl_width_600">
                            <tr class="wiz_bg_EFEFEF">
                                <td>
                                    <table class="tbl_width_560">
                                        <tr class="background_FFFFFF">
                                            <td class="inside_headings" style="padding-left:15px; height: 45px;">TRANSACTION AUDIT TRAIL</td>
                                        </tr>
                                        
                                        <tr class="background_FFFFFF">
                                            <td>
                                                <table class="tbl_width_530">
                                                    
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr><td><div id="new-search-area"></div></td></tr>
                                                                <tr>
                                                                    <td>
                                                                        
                                                                        <table id="TBL_REWV_Audit" class="tbl_width_530">
                                                                            <thead>
                                                                                <tr>
                                                                                    <td style="width:230px;">Record Date</td>
                                                                                    <td style="width:300px;">Audit Record</td>
                                                                                </tr>
                                                                            </thead>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>



                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="background_FFFFFF spacer15"><td>&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="wiz_bg_EFEFEF" style="height:20px;"><td>&nbsp;</td></tr>
                        </table>


                    </div>

                </td>
            </tr>

            <tr class="spacer10">
                <td>
                    <input type="text" id="REVW_CountryID" hidden="hidden" /></td>
            </tr>

            <tr>
                <td>
                    <table class="tbl_width_600">
                        <tr>
                            <td style="text-align: right;">
                                <input class="aa_btn_green" id="REVW_btn_Submit" type="button" value="Complete Transaction Review" />&nbsp;<input class="aa_btn_red" id="BTN_CloseReviewWindow" type="button" value="Close" /></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="spacer10">
                <td>&nbsp;</td>
            </tr>

        </table>
    </div>


    <div id="EditTransactionDiv">
        <!-- THIS IS THE EDIT POPUP DIALOG BOX -->
        <div class="edit__form__main">
            <form method="get" id="edittransactionform">
                <table class="style__width_600">

                    <tr style="height: 20px;">
                        <td>
                            <table>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr hidden="hidden">
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <input id="EDIT_TransID" name="APPR_TransID" type="text" />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="trn_popup_sub_headings">TRANSACTION DETAILS</td>
                    </tr>

                    <tr>
                        <td>
                            <table class="tbl_width_600">
                                <tr>
                                    <td class="trn_popup_details_h">Transaction ID</td>
                                    <td class="trn_popup_details_c"><span id="TransID"></span></td>
                                </tr>
                                <tr>
                                    <td class="trn_popup_details_h">Transaction date/time</td>
                                    <td class="trn_popup_details_c"><span id="TransDateTime"></span></td>
                                </tr>
                                <tr>
                                    <td class="trn_popup_details_h">Customer ID</td>
                                    <td class="trn_popup_details_c"><span id="CustID"></span></td>
                                </tr>
                                <tr>
                                    <td class="trn_popup_details_h">Customer name</td>
                                    <td class="trn_popup_details_c"><span id="CustName"></span></td>
                                </tr>
                                <tr>
                                    <td class="trn_popup_details_h">AUD rate</td>
                                    <td class="trn_popup_details_c"><span id="AUDRate"></span></td>
                                </tr>
                                <tr>
                                    <td class="trn_popup_details_h">Total sending</td>
                                    <td class="trn_popup_details_c"><span id="TotalSending"></span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr style="height: 20px;">
                        <td>&nbsp;</td>
                    </tr>

                    <tr style="background-color: #EFEFEF;">
                        <td>

                            <table class="tbl_width_560">

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="label__font">Beneficiary name</td>
                                                <td class="label__font">&nbsp;</td>
                                                <td class="label__font">Bank account</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_270">
                                                    <div class="login__input">
                                                        <select id="benelist" style="width: 270px;"></select>
                                                    </div>
                                                </td>
                                                <td class="style__width_20">&nbsp;</td>
                                                <td class="style__width_270">
                                                    <div class="login__input">
                                                        <select id="accountlist" style="width: 270px;"></select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="label__font">Available funds</td>
                                                <td class="label__font">&nbsp;</td>
                                                <td class="label__font">Funds to be sent</td>
                                                <td class="label__font">&nbsp;</td>
                                                <td class="label__font">Service charges</td>
                                            </tr>
                                            <tr>
                                                <td class="style__width_174">
                                                    <div class="login__input">
                                                        <input class="phone_home_class modal__input__font ali_right" id="availablefunds" style="width: 100%;" type="text" value="" readonly="readonly" />
                                                    </div>
                                                </td>
                                                <td class="style__width_19">&nbsp;</td>
                                                <td class="style__width_174">
                                                    <div class="login__input">
                                                        <input class="phone_work_class modal__input__font ali_right" id="fundstosend" style="width: 100%;" type="text" value="" />
                                                    </div>
                                                </td>
                                                <td class="style__width_19">&nbsp;</td>
                                                <td class="style__width_174">
                                                    <table class="style__width_174">
                                                        <tr>
                                                            <td style="width: 82px;">
                                                                <div class="login__input">
                                                                    <input class="phone_mobile_class modal__input__font ali_right" id="SM_servicecharge" style="width: 100px;" type="text" value="" readonly="readonly" />
                                                                </div>
                                                            </td>
                                                            <td style="width: 10px;">&nbsp;</td>
                                                            <td style="width: 82px; text-align: right;">
                                                                <input class="btn_red_outline" id="SM_btn_override" type="button" value="O/RIDE" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr hidden="hidden">
                                                <td>
                                                    <input type="text" id="hidden_sendcalc" name="hidden_sendcalc" /><input type="text" id="hidden_transID" name="hidden_transID" /></td>
                                            </tr>
                                            <tr class="spacer10">
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>

                    <tr>
                        <td style="height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td>
                            <table class="tbl_width_600">
                                <tr>
                                    <td>
                                        <input class="btn_red" id="btn_SaveTrans" type="button" value="SAVE CHANGES" />
                                    </td>
                                    <td>&nbsp;</td>
                                    <td style="text-align: right;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>

            </form>

        </div>
    </div>

    <div id="Void_Confirmation">
        <table>
            <tr>
                <td>This transaction is about to be cancelled.</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Cancellation Reason</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <input type="text" id="voidreason" name="voidreason" /></td>
            </tr>
            <tr>
                <td>
                    <input class="btn_red" id="btn_void_yes" type="button" value="YES" /><input class="btn_red" id="btn_void_no" type="button" value="NO" /></td>
            </tr>
        </table>
    </div>

    <div id="DIV_Loading">
        <!-- Loading DIV -->
        <table class="tbl_width_120">
            <tr>
                <td style="text-align: center;">
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/loading.gif" Height="60px" Width="60px" />
                </td>
            </tr>
            <tr>
                <td class="fnt_pleasewait">Please wait...</td>
            </tr>
        </table>
    </div>
    <div id="DIV_Cancelledtrans">
    </div>
    <div id="DIV_ApprovePIN">
        <table class="tbl_width_440">
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="vertical-align:top;"><input id="CHK_ConfirmApproval" type="checkbox" class="chk_pin" /></td>
                            <td style="width:20px;">&nbsp;</td>
                            <td class="confirm_font">I, <span id="SPN_LoggedUserName" runat="server"></span>, confirm that I have reviewed this Transaction and complied with the relevant Instructions from the AML/CTF program and where appropriate added relevant documents/notes to support this transaction.</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="tbl_width_440">
                        <tr id="REVW_ApprovePIN">
                            <td>
                                <table class="tbl_width_440">
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td class="wiz_bg_EFEFEF">
                                            <table class="tbl_width_400">
                                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                <tr>
                                                    <td style="width:200px;">&nbsp;</td>
                                                    <td style="width:95px;">&nbsp;</td>
                                                    <td style="width:10px;">&nbsp;</td>
                                                    <td style="width:95px; vertical-align:top;"><input class="aa_btn_green" id="btn_ConfimApproval_OK" style="width: 100%;" type="button" value="OK" /></td>
                                                </tr>
                                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="spacer10"><td>&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
        </table>

                </td>
            </tr>
            </table>
        
    </div>


    <script type="text/javascript">
        $('#UploadFile1').change(function () {
            var filename = $(this).val().split('\\').pop();
            $('#otherdoctext').text(filename);
        });



        $('#UploadFile1').fileupload({
            url: 'DataHandlers/UploadOtherFiles.ashx?upload=start',
            autoUpload: false,
            replaceFileInput: false,
            add: function (e, data) {
                $('#btn_UploadOtherFiles').off('click').on('click', function () {
                    if ($('#docdescription').val() == "") {
                        showerror('Please provide a description to upload this file');
                    }
                    else {
                        data.submit();
                    }

                });
            },
            success: function (response, status) {
                showsuccess('', 'File Uploaded Successfully');
                //openMsgDIV('None', 'File has been successfully uploaded', '2');
                ReviewDocsTable.ajax.reload(null, false);
                $('#docdescription').val('');
                $('#otherdoctext').text('');
            }
        });

        $('#UploadFile1').bind('fileuploadsubmit', function (e, data) {
            data.formData = {
                CustID: $('#REVW_CustID').val(),
                TransID: $('#REVW_TransID').val(),
                DocDesc: $('#docdescription').val(),
            };
            if (!data.formData.CustID) {
                return false;
            }
        });

        function showerror(msg) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.error(msg, 'Error');

        }
        function showsuccess(msg, body) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 6000
            };
            toastr.success(msg, body);

        }

    </script>
</asp:Content>
