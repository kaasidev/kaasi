﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace KASI_Extend.lk.sampath.uatweb {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="ERMWSAgentSoapBinding", Namespace="http://service.ws.erm.sampath")]
    public partial class ERMWSAgentService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback revokeTransactionOperationCompleted;
        
        private System.Threading.SendOrPostCallback balanceInquiryOperationCompleted;
        
        private System.Threading.SendOrPostCallback validateAccountNumberOperationCompleted;
        
        private System.Threading.SendOrPostCallback inquireTransactionOperationCompleted;
        
        private System.Threading.SendOrPostCallback processTransactionOperationCompleted;
        
        private System.Threading.SendOrPostCallback cancelTransactionOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ERMWSAgentService() {
            //this.Url = global::KASI_Extend.Properties.Settings.Default.KASI_Extend__lk_sampath_uatweb_ERMWSAgentService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event revokeTransactionCompletedEventHandler revokeTransactionCompleted;
        
        /// <remarks/>
        public event balanceInquiryCompletedEventHandler balanceInquiryCompleted;
        
        /// <remarks/>
        public event validateAccountNumberCompletedEventHandler validateAccountNumberCompleted;
        
        /// <remarks/>
        public event inquireTransactionCompletedEventHandler inquireTransactionCompleted;
        
        /// <remarks/>
        public event processTransactionCompletedEventHandler processTransactionCompleted;
        
        /// <remarks/>
        public event cancelTransactionCompletedEventHandler cancelTransactionCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://service.ws.erm.sampath", ResponseNamespace="http://service.ws.erm.sampath", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("revokeTransactionReturn")]
        public string[] revokeTransaction([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string user, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string accessCode, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string companyId, [System.Xml.Serialization.XmlElementAttribute("txnData")] string[] txnData) {
            object[] results = this.Invoke("revokeTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnData});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void revokeTransactionAsync(string user, string accessCode, string companyId, string[] txnData) {
            this.revokeTransactionAsync(user, accessCode, companyId, txnData, null);
        }
        
        /// <remarks/>
        public void revokeTransactionAsync(string user, string accessCode, string companyId, string[] txnData, object userState) {
            if ((this.revokeTransactionOperationCompleted == null)) {
                this.revokeTransactionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnrevokeTransactionOperationCompleted);
            }
            this.InvokeAsync("revokeTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnData}, this.revokeTransactionOperationCompleted, userState);
        }
        
        private void OnrevokeTransactionOperationCompleted(object arg) {
            if ((this.revokeTransactionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.revokeTransactionCompleted(this, new revokeTransactionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://service.ws.erm.sampath", ResponseNamespace="http://service.ws.erm.sampath", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("balanceInquiryReturn")]
        public WSBalInqData[] balanceInquiry([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string user, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string accessCode, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string companyId) {
            object[] results = this.Invoke("balanceInquiry", new object[] {
                        user,
                        accessCode,
                        companyId});
            return ((WSBalInqData[])(results[0]));
        }
        
        /// <remarks/>
        public void balanceInquiryAsync(string user, string accessCode, string companyId) {
            this.balanceInquiryAsync(user, accessCode, companyId, null);
        }
        
        /// <remarks/>
        public void balanceInquiryAsync(string user, string accessCode, string companyId, object userState) {
            if ((this.balanceInquiryOperationCompleted == null)) {
                this.balanceInquiryOperationCompleted = new System.Threading.SendOrPostCallback(this.OnbalanceInquiryOperationCompleted);
            }
            this.InvokeAsync("balanceInquiry", new object[] {
                        user,
                        accessCode,
                        companyId}, this.balanceInquiryOperationCompleted, userState);
        }
        
        private void OnbalanceInquiryOperationCompleted(object arg) {
            if ((this.balanceInquiryCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.balanceInquiryCompleted(this, new balanceInquiryCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://service.ws.erm.sampath", ResponseNamespace="http://service.ws.erm.sampath", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("validateAccountNumberReturn")]
        public string[] validateAccountNumber([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string user, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string accessCode, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string companyId, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string accountNo) {
            object[] results = this.Invoke("validateAccountNumber", new object[] {
                        user,
                        accessCode,
                        companyId,
                        accountNo});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void validateAccountNumberAsync(string user, string accessCode, string companyId, string accountNo) {
            this.validateAccountNumberAsync(user, accessCode, companyId, accountNo, null);
        }
        
        /// <remarks/>
        public void validateAccountNumberAsync(string user, string accessCode, string companyId, string accountNo, object userState) {
            if ((this.validateAccountNumberOperationCompleted == null)) {
                this.validateAccountNumberOperationCompleted = new System.Threading.SendOrPostCallback(this.OnvalidateAccountNumberOperationCompleted);
            }
            this.InvokeAsync("validateAccountNumber", new object[] {
                        user,
                        accessCode,
                        companyId,
                        accountNo}, this.validateAccountNumberOperationCompleted, userState);
        }
        
        private void OnvalidateAccountNumberOperationCompleted(object arg) {
            if ((this.validateAccountNumberCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.validateAccountNumberCompleted(this, new validateAccountNumberCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://service.ws.erm.sampath", ResponseNamespace="http://service.ws.erm.sampath", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("inquireTransactionReturn")]
        public WSTxnInqData[] inquireTransaction([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string user, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string accessCode, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string companyId, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string txnPin) {
            object[] results = this.Invoke("inquireTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnPin});
            return ((WSTxnInqData[])(results[0]));
        }
        
        /// <remarks/>
        public void inquireTransactionAsync(string user, string accessCode, string companyId, string txnPin) {
            this.inquireTransactionAsync(user, accessCode, companyId, txnPin, null);
        }
        
        /// <remarks/>
        public void inquireTransactionAsync(string user, string accessCode, string companyId, string txnPin, object userState) {
            if ((this.inquireTransactionOperationCompleted == null)) {
                this.inquireTransactionOperationCompleted = new System.Threading.SendOrPostCallback(this.OninquireTransactionOperationCompleted);
            }
            this.InvokeAsync("inquireTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnPin}, this.inquireTransactionOperationCompleted, userState);
        }
        
        private void OninquireTransactionOperationCompleted(object arg) {
            if ((this.inquireTransactionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.inquireTransactionCompleted(this, new inquireTransactionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://service.ws.erm.sampath", ResponseNamespace="http://service.ws.erm.sampath", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("processTransactionReturn")]
        public string[] processTransaction([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string user, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string accessCode, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string companyId, [System.Xml.Serialization.XmlElementAttribute("txnData")] string[] txnData) {
            object[] results = this.Invoke("processTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnData});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void processTransactionAsync(string user, string accessCode, string companyId, string[] txnData) {
            this.processTransactionAsync(user, accessCode, companyId, txnData, null);
        }
        
        /// <remarks/>
        public void processTransactionAsync(string user, string accessCode, string companyId, string[] txnData, object userState) {
            if ((this.processTransactionOperationCompleted == null)) {
                this.processTransactionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnprocessTransactionOperationCompleted);
            }
            this.InvokeAsync("processTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnData}, this.processTransactionOperationCompleted, userState);
        }
        
        private void OnprocessTransactionOperationCompleted(object arg) {
            if ((this.processTransactionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.processTransactionCompleted(this, new processTransactionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://service.ws.erm.sampath", ResponseNamespace="http://service.ws.erm.sampath", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("cancelTransactionReturn")]
        public string[] cancelTransaction([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string user, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string accessCode, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string companyId, [System.Xml.Serialization.XmlElementAttribute("txnData")] string[] txnData, bool cancellingMode) {
            object[] results = this.Invoke("cancelTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnData,
                        cancellingMode});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void cancelTransactionAsync(string user, string accessCode, string companyId, string[] txnData, bool cancellingMode) {
            this.cancelTransactionAsync(user, accessCode, companyId, txnData, cancellingMode, null);
        }
        
        /// <remarks/>
        public void cancelTransactionAsync(string user, string accessCode, string companyId, string[] txnData, bool cancellingMode, object userState) {
            if ((this.cancelTransactionOperationCompleted == null)) {
                this.cancelTransactionOperationCompleted = new System.Threading.SendOrPostCallback(this.OncancelTransactionOperationCompleted);
            }
            this.InvokeAsync("cancelTransaction", new object[] {
                        user,
                        accessCode,
                        companyId,
                        txnData,
                        cancellingMode}, this.cancelTransactionOperationCompleted, userState);
        }
        
        private void OncancelTransactionOperationCompleted(object arg) {
            if ((this.cancelTransactionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.cancelTransactionCompleted(this, new cancelTransactionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://service.ws.erm.sampath")]
    public partial class WSBalInqData {
        
        private string accBalanceField;
        
        private string accountNoField;
        
        private string currCodeField;
        
        private string msgTextField;
        
        private string recStatusField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string accBalance {
            get {
                return this.accBalanceField;
            }
            set {
                this.accBalanceField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string accountNo {
            get {
                return this.accountNoField;
            }
            set {
                this.accountNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string currCode {
            get {
                return this.currCodeField;
            }
            set {
                this.currCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string msgText {
            get {
                return this.msgTextField;
            }
            set {
                this.msgTextField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string recStatus {
            get {
                return this.recStatusField;
            }
            set {
                this.recStatusField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://service.ws.erm.sampath")]
    public partial class WSTxnInqData {
        
        private string amountField;
        
        private string currCodeField;
        
        private string msgTextField;
        
        private string pinField;
        
        private string receiveDateField;
        
        private string recStatusField;
        
        private string txnStatusField;
        
        private string txnTypeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string amount {
            get {
                return this.amountField;
            }
            set {
                this.amountField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string currCode {
            get {
                return this.currCodeField;
            }
            set {
                this.currCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string msgText {
            get {
                return this.msgTextField;
            }
            set {
                this.msgTextField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string pin {
            get {
                return this.pinField;
            }
            set {
                this.pinField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string receiveDate {
            get {
                return this.receiveDateField;
            }
            set {
                this.receiveDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string recStatus {
            get {
                return this.recStatusField;
            }
            set {
                this.recStatusField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string txnStatus {
            get {
                return this.txnStatusField;
            }
            set {
                this.txnStatusField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string txnType {
            get {
                return this.txnTypeField;
            }
            set {
                this.txnTypeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void revokeTransactionCompletedEventHandler(object sender, revokeTransactionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class revokeTransactionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal revokeTransactionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void balanceInquiryCompletedEventHandler(object sender, balanceInquiryCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class balanceInquiryCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal balanceInquiryCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public WSBalInqData[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((WSBalInqData[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void validateAccountNumberCompletedEventHandler(object sender, validateAccountNumberCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class validateAccountNumberCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal validateAccountNumberCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void inquireTransactionCompletedEventHandler(object sender, inquireTransactionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class inquireTransactionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal inquireTransactionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public WSTxnInqData[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((WSTxnInqData[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void processTransactionCompletedEventHandler(object sender, processTransactionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class processTransactionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal processTransactionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void cancelTransactionCompletedEventHandler(object sender, cancelTransactionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class cancelTransactionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal cancelTransactionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591