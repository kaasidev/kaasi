﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KASI_Extend_.classes;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Kapruka.Service;
using Kapruka.Domain;
using Kapruka.Enterprise;
using Kapruka.Repository;
using System.Globalization;
using System.IO;

namespace KASI_Extend_
{
    public partial class DashboardW : System.Web.UI.Page
    {
        classes.Customer customer = new classes.Customer();
        classes.Beneficiary beneficiary = new classes.Beneficiary();
        Transactions transaction = new Transactions();
        HtmlGenericControl li;
        public string selStreetVal = "";
        public string posselStreetVal = "";
        public string titlesel = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["TypeOfLogin"] == null)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseTab", "closeWindow();", true);
            }

            if (!IsPostBack)
            {

                String requestedCID = Request.QueryString["custid"].ToString();
                String CID = requestedCID;
                String FirstBeneficiary = String.Empty;
                loadAllCustomerDetails(CID);
                loadCustomerBeneficiaries(CID);
                loadAllCustomerDetailsForEdit(CID);
                txt_hidden_CustomerID.Text = CID;
                txt_hidden_CustomerID.Style.Add("display", "none");
                txt_hidden_FirstBeneficiaryID.Style.Add("display", "none");
                txt_hidden_AccountID.Style.Add("display", "none");
                txt_hidden_Rate.Style.Add("display", "none");
                hidden_SpendingPower.Style.Add("display", "none");
                hidden_CustomerFullName.Style.Add("display", "none");

                dsh_KYC_Fail.Style.Add("display", "none");
                dsh_KYC_Success.Style.Add("display", "none");
                dsh_KYC_NotRun.Style.Add("display", "none");
                dsh_KYC_DateLabel.Style.Add("display", "none");
                dsh_KYC_DateValue.Style.Add("display", "none");
                dsh_KYC_ByLabel.Style.Add("display", "none");
                dsh_KYC_ByValue.Style.Add("display", "none");
                dsh_KYC_InfoLabel.Style.Add("display", "none");
                dsh_KYC_InfoValue.Style.Add("display", "none");
                lbl_KYC_ManualKYCComments.Style.Add("display", "none");
                dsh_KYC_RefLabel.Style.Add("display", "none");
                dsh_KYC_RefValue.Style.Add("display", "none");

                TR_DisplayKYCErrors.Style.Add("display", "none");
                TR_ConfirmRUNKYC.Style.Add("display", "none");

                txt_AUDMLimit.Style.Add("display", "none");
                txt_AUDYLimit.Style.Add("display", "none");
                txt_NbMLimit.Style.Add("display", "none");
                txt_NbYLimit.Style.Add("display", "none");

                ErrorMessageArea.Visible = false;
                MessageArea.Visible = false;
                //MessageArea.Style.Add("display", "none");
                //loadAllBankAccounts();
                // WarningMessages(CID);
                checkDocuments(CID);
                checkIfKYCHasBeenPerformed(CID);
                displayKYCInfo(CID);
                setAgentLimit(CID);
            }
        }

        public void setAgentLimit(String CID)
        {
            classes.Customer Cust = new classes.Customer();
            classes.Agent AGT = new classes.Agent();
            String AID = Cust.getOwnerAgentIDFromCustomerID(CID);

            String MAUDLimit = AGT.getAgentMonthlyAUDLimit(AID);
            String YAUDLimit = AGT.getAgentYearlyAUDLimit(AID);
            String MNbLimit = AGT.getAgentMonthlyTransLimit(AID);
            String YNbLimit = AGT.getAgentYearlyTransLimit(AID);

            txt_AUDMLimit.Text = MAUDLimit;
            txt_AUDYLimit.Text = YAUDLimit;
            txt_NbMLimit.Text = MNbLimit;
            txt_NbYLimit.Text = YNbLimit;
        }

        public void displayKYCInfo(String CID)
        {
            string[] formats = {"dd/MM/yyyy", "dd-MMM-yyyy", "yyyy-MM-dd",
                   "dd-MM-yyyy", "M/d/yyyy", "dd MMM yyyy"};
            var KYCCode = customer.getCustomerKYCResultCode(CID);

            if (KYCCode == "0")
            {
                dsh_KYC_Success.Style.Add("display", "block");
                dsh_KYC_DateLabel.Style.Add("display", "block");
                dsh_KYC_DateValue.Style.Add("display", "block");
                dsh_KYC_ByLabel.Style.Add("display", "block");
                dsh_KYC_ByValue.Style.Add("display", "block");
                dsh_KYC_InfoLabel.Style.Add("display", "block");
                dsh_KYC_InfoValue.Style.Add("display", "block");
                dsh_KYC_RefLabel.Style.Add("display", "block");
                dsh_KYC_RefValue.Style.Add("display", "block");
                dsh_KYC_Fail.Style.Add("display", "none");
                dsh_KYC_NotRun.Style.Add("display", "none");
                String CheckType = customer.getCustomerKYCCheckType(CID);

                dsh_KYC_RefValue.InnerText = customer.getCustomerKYCResultTransactionID(CID);
                // Always ensure that it is always USA and then convert to AUS
                dsh_KYC_DateValue.InnerText = DateTime.ParseExact(customer.getCustomerKYCCheckDateDate(CID), formats, CultureInfo.InvariantCulture, DateTimeStyles.None).ToString("dd/MM/yyyy");
                dsh_KYC_ByValue.InnerText = customer.getCustomerKYCRunBy(CID);

                if (CheckType == "MANUAL")
                {
                    lbl_KYC_ManualKYCComments.Style.Add("display", "block");
                    lbl_KYC_ManualKYCComments.Text = customer.getCustomerKYCNotes(CID);
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID) + " - " + CheckType;
                }
                else
                {
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID);
                }
                //showKYCResult(CID);
            }
            else if (String.IsNullOrEmpty(KYCCode))
            {
                dsh_KYC_NotRun.Style.Add("display", "block");
            }
            else
            {
                dsh_KYC_DateLabel.Style.Add("display", "block");
                dsh_KYC_DateValue.Style.Add("display", "block");
                dsh_KYC_ByLabel.Style.Add("display", "block");
                dsh_KYC_InfoLabel.Style.Add("display", "block");
                dsh_KYC_InfoValue.Style.Add("display", "block");
                dsh_KYC_RefLabel.Style.Add("display", "block");
                dsh_KYC_RefValue.Style.Add("display", "block");
                String CheckType = customer.getCustomerKYCCheckType(CID);

                dsh_KYC_RefValue.InnerText = customer.getCustomerKYCResultTransactionID(CID);
                dsh_KYC_DateValue.InnerText = customer.getCustomerKYCCheckDateDate(CID);
                dsh_KYC_Fail.Style.Add("display", "block");


                if (CheckType == "MANUAL")
                {
                    lbl_KYC_ManualKYCComments.Style.Add("display", "block");
                    lbl_KYC_ManualKYCComments.Text = customer.getCustomerKYCNotes(CID);
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID) + " - " + CheckType;
                }
                else
                {
                    dsh_KYC_InfoValue.InnerText = customer.getCustomerKYCResultDescription(CID);
                }
            }
        }

        public void checkDocuments(String CID)
        {
            ErrorMessageArea.Visible = false;
            //String RiskReason = customer.getRiskReason(CID);

            //if (RiskReason == "NOID")
            // {
            //    ErrorMessageArea.Visible = true;
            //    errormessagedisplay.InnerText = "WARNING: This Customer has no identification documents.";
            // }
            // else if (RiskReason == "EXPDOC")
            // {
            //     ErrorMessageArea.Visible = true;
            //     errormessagedisplay.InnerText = "WARNING: One or more documents have expired.";
            //}
            // else
            // {
            //     
            //}

            classes.Customer CUST = new classes.Customer();
            String DocStatus = CUST.getCustomerDocumentCount(CID);

            if (DocStatus == "NODOCS")
            {
                ErrorMessageArea.Visible = true;
                errormessagedisplay.InnerText = "WARNING: This Customer has no identification documents.";
            }
            else
            {
                var DocExp = CUST.checkIfCustomerDocumentsHaveAllExpired(CID);

                if (DocExp == true)
                {
                    CUST.ExpireKYC(CID);
                    ErrorMessageArea.Visible = true;
                    errormessagedisplay.InnerText = "WARNING: One or more documents have expired.";
                }
            }
        }

        public void loadAllCustomerDetailsForEdit(String CID)
        {
            //string isIndi = customer.getCustomerType(CID);
            //Kapruka.Repository.Customer cust = new Kapruka.Repository.Customer();
            //int custId = int.Parse(CID);
            //hidCustId.Value = CID;
            //CustomerService service = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            //cust = service.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault();
            //if (isIndi == "False")
            //{
            //    hidCustType.Value = "Busi";
            //    divBusiness.Visible = true;
            //    tblIndi.Visible = false;
            //    BBusinessName.Value = cust.BusinessName;
            //    BABN.Value = cust.ABN;
            //    BTradingName.Value = cust.TradingName;
            //    bus_unitno.Value = cust.UnitNo;
            //    bus_streetno.Value = cust.StreetNo;
            //    bus_state.Value = cust.State;
            //    bus_streetname.Value = cust.StreetName;
            //    bus_postcode.Value = cust.Postcode;
            //    bus_unitno.Value = cust.PostalAddressUnitNo;
            //    pos_streetno.Value = cust.PostalAddressStreetNo;
            //    pos_streetname.Value = cust.PostalAddressStreetName;

            //    ListItem li = bus_streettype.Items.FindByText(cust.StreetType);
            //    selStreetVal = cust.StreetType;
            //    posselStreetVal = cust.PostalAddressStreetType;
            //    //if (li != null)
            //    //{
            //    ////    li.Selected = true;
            //    //}
            //    bus_suburb.Value = cust.Suburb;
            //    BPostalState.Value = cust.PostalAddressState;
            //    BPostalPostcode.Value = cust.Postcode;
            //    pos_country.Value = cust.PostalAddressCountry;
            //    BLastName.Value = cust.LastName;
            //    ListItem lit = Btitle.Items.FindByText(cust.Title);
            //    titlesel = cust.Title;
            //    //if (lit != null)
            //    //{
            //    ////    lit.Selected = true;
            //    //}
            //    BDOB.Value = cust.DOB;
            //    BFirstNames.Value = cust.FirstName;
            //    BHomePhone.Value = cust.TelHome;
            //    BPhoneWork.Value = cust.TelWork;
            //    BMobilePhone.Value = cust.Mobile;
            //    BEmail1.Value = cust.EmailAddress;
            //    BEmail2.Value = cust.EmailAddress2;
            //    BBContact.Value = cust.BusinessContact;
            //    BBEmail.Value = cust.BusinessEmail;
            //    BBWeb.Value = cust.BusinessWebSite;

            //}
            //else
            //{
            //    hidCustType.Value = "Indi";
            //    divBusiness.Visible = false;
            //    tblIndi.Visible = true;
            //    lastname.Value = cust.LastName;// customer.getCustomerLastName(CID);
            //    firstnames.Value = cust.FirstName;// customer.getCustomerFirstName(CID);
            //    //addressline1.Value = customer.getCustomerAddressLine1(CID);
            //    //addressline2.Value = customer.getCustomerAddressLine2(CID);
            //    suburb.Value = cust.Suburb;// customer.getCustomerSuburb(CID);
            //    state.Value = cust.State;// customer.getCustomerState(CID);
            //    postcode.Value = cust.Postcode;// customer.getCustomerPostcode(CID);
            //    phonehome.Value = cust.TelHome;// customer.getCustomerHomeNumber(CID);
            //    phonework.Value = cust.TelWork;// customer.getCustomerWorkNumber(CID);
            //    phonemobile.Value = cust.Mobile;// customer.getCustomerMobile(CID);
            //    dob.Value = cust.DOB;// customer.getCustomerDOB(CID);
            //    email1.Value = cust.EmailAddress;// customer.getCustomerEmailAddress1(CID);
            //    email2.Value = cust.EmailAddress2;// customer.getCustomerEmailAddress2(CID);
            //    edit_notes.Value = cust.Notes;// customer.getCustomerNotes(CID);
            //    edit_password.Value = cust.AccountPassword;// customer.getCustomerPassword(CID);
            //    if(!string.IsNullOrEmpty(cust.AccountPassword))
            //    {
            //        showpassword.InnerHtml = cust.AccountPassword.ToUpper();// customer.getCustomerPassword(CID).ToUpper();
            //    }

            //    shownotes.InnerHtml = cust.Notes;// customer.getCustomerNotes(CID);
            //    placeofbirth.Value = cust.PlaceOfBirth;// customer.getCustomerPlaceOfBirth(CID);
            //    aka.Value = cust.AKA;// customer.getCustomerAKA(CID);
            //    Cust_occupation.Value = cust.Occupation;// customer.getCustomerOccupation(CID);
            //    unitno.Value = cust.UnitNo;// customer.getCustomerUnitNo(CID);
            //    streetno.Value = cust.StreetNo;// customer.getCustomerStreetNo(CID);
            //    streetname.Value = cust.StreetName;// customer.getCustomerStreetName(CID);
            //    country.Value = cust.Country;// customer.getCustomerCountry(CID);


            //    ListItem li = streettype.Items.FindByText(cust.StreetType);
            //    if (li != null)
            //    {
            //     //   li.Selected = true;
            //    }


            //    ListItem COB = countryofbirth.Items.FindByText(cust.CountryOfBirth);
            //    if (COB != null)
            //    {
            //     //   COB.Selected = true;
            //    }


            //    ListItem National = nationality.Items.FindByText(cust.Nationality);
            //    if (National != null)
            //    {
            //     //   National.Selected = true;
            //    }


            //}



            //if (edit_password.Value == "")
            //{
            //    trpassword.Visible = false;
            //    trpassworspacer.Visible = false;
            //}

            //if (edit_notes.Value == "")
            //{
            //    trnotes.Visible = false;
            //    trpassworspacer.Visible = false;
            //}

            //if (edit_password.Value == "" && edit_notes.Value == "")
            //{
            //    NotesAreaTR.Visible = false;
            //}
        }

        public void loadAllCustomerDetails(String CID)
        {
            String TypeOfCustomer = customer.getCustomerTypeNamed(CID);

            if (TypeOfCustomer == "INDIVIDUAL")
            {
                div_cust_full_name.InnerText = CID + " - " + customer.getCustomerFullName(CID);
            }
            else
            {
                div_cust_full_name.InnerText = CID + " - " + customer.getCustomerBusinessName(CID);
            }

            if (customer.confirmCustomerKYCHasBeenDone(CID) == true)
            {
                hidden_KYCRunBefore.Value = "true";
            }
            else
            {
                hidden_KYCRunBefore.Value = "false";
            }


            lbl_custname.Text = customer.getCustomerFullName(CID);
            div_CustomerDOB.InnerText = customer.getCustomerDOB(CID);
            div_CustomerFullAddress.InnerText = customer.getCustomerFullAddress(CID);
            div_CustomerEmail1.InnerText = customer.getCustomerEmailAddress1(CID);
            div_CustomerEmail2.InnerText = customer.getCustomerEmailAddress2(CID);
            div_CustomerMobile.InnerText = customer.getCustomerMobile(CID);
            div_CustomerHomeNumber.InnerText = customer.getCustomerHomeNumber(CID);
            div_CustomerWorkNumber.InnerText = customer.getCustomerWorkNumber(CID);
            div_customertype.InnerText = customer.getCustomerTypeNamed(CID);
            String AccBalance = customer.getCustomerAccountBalance(CID);
            DIV_BankAccountBalance.InnerText = String.Format("{0:C2}", float.Parse(AccBalance));

            String DollarValue = customer.getCustomerAccountBalance(CID);
            Decimal DDollarValue = decimal.Parse(Regex.Replace(DollarValue, @"[^\d.]", ""));
            hidden_txt_accountbalance.Text = DDollarValue.ToString();
            hidden_txt_accountbalance.Style.Add("display", "none");
            hidden_risklevel.Value = customer.getCustomerRiskLevel(CID);
            setRiskLevel(hidden_risklevel.Value);
            hidden_AgentID.Value = Session["LoggedID"].ToString();
            hidden_SpendingPower.Text = customer.getCustomerSpendingPower(CID);
            hidden_CustomerFullName.Text = customer.getCustomerFullName(CID);
            //hidden_risklevel.Style.Add("display", "none");
            string CustomerHasPic = customer.getCustomerPicPath(CID);
            showStatsAbove(CID);


            SPN_ID.InnerText = CID;
            SPN_NAME.InnerText = customer.getCustomerFullName(CID);
            hidden_hasKYC.Value = customer.getCustomerHasKYC(CID);
            hidden_CurrentSentAmount.Value = customer.getYearlyRemittancetotal(CID);
            hidden_KYCExpiryDays.Value = customer.KYCExpiryDays(CID);
            hidden_NbTrans.Value = customer.getSumYearlyRemit(CID);

            String hasNotes = customer.getCustomerNotes(CID);
            trnotes.Style.Add("display", "none");
            if (!String.IsNullOrEmpty(hasNotes))
            {
                shownotes.InnerHtml = hasNotes;
                trnotes.Style.Add("display", "block");
            }


            String HasHundredPoints = customer.getCustomerIDPointsTotal(CID);
            Int32 HHP = 0;
            if (HasHundredPoints != "")
            {
                if (HasHundredPoints != "0")
                {
                    HHP = Int32.Parse(HasHundredPoints);
                    hidden_hasID.Value = "Y";
                }
                else
                {
                    HHP = 0;
                    hidden_hasID.Value = "N";
                }
            }
            else
            {
                HHP = 0;
                hidden_hasID.Value = "N";
            }


            if (HHP >= 100)
            {
                Image1.Attributes["src"] = "images/id-card.png";
                Image1.Attributes["title"] = "Customer Identifcation Completed";
            }
            else
            {
                Image1.Attributes["title"] = "Customer Identifcation Pending";
            }







            if (CustomerHasPic.Equals("NA"))
            {
                string customerGender = customer.getCustomerGender(CID);
                if (customerGender.ToString().Equals("Male"))
                {
                    img_CustomerPic.Attributes.Add("src", "images/avatar-male.jpg");
                }
                else if (customerGender.ToString().Equals("Female"))
                {
                    img_CustomerPic.Attributes.Add("src", "images/avatar-female.jpg");
                }
                else
                {
                    img_CustomerPic.Attributes.Add("src", "images/avatar-unknown.jpg");
                }
            }
            else
            {
                img_CustomerPic.Attributes.Add("src", CustomerHasPic);
            }
        }

        public void setRiskLevel(String RiskLevel)
        {
            if (RiskLevel == "4")
            {

                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_3");
                td_risk4.Attributes.Add("class", "risk_4");
            }
            else if (RiskLevel == "3")
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_3");
                td_risk4.Attributes.Add("class", "risk_neutral");
            }
            else if (RiskLevel == "2")
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_neutral");
                td_risk4.Attributes.Add("class", "risk_neutral");
            }
            else if (RiskLevel == "1")
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_neutral");
                td_risk3.Attributes.Add("class", "risk_neutral");
                td_risk4.Attributes.Add("class", "risk_neutral");
            }
            else
            {
                td_risk1.Attributes.Add("class", "risk_1");
                td_risk2.Attributes.Add("class", "risk_2");
                td_risk3.Attributes.Add("class", "risk_3");
                td_risk4.Attributes.Add("class", "risk_4");
            }
        }

        public void showStatsAbove(String CID)
        {

            //var _loggingService = new LoggingService();
            // _loggingService.LogError(ex);

            classes.Agent AGT = new classes.Agent();
            // Get the Agent's Limits to ensure customer has not gone past those limits
            String owningAgent = customer.getOwnerAgentIDFromCustomerID(CID);
            float AgentMAUDLimit = float.Parse(AGT.getAgentMonthlyAUDLimit(owningAgent));
            float AgentYAUDLimit = float.Parse(AGT.getAgentYearlyAUDLimit(owningAgent));
            float AgentMNbLimit = float.Parse(AGT.getAgentMonthlyTransLimit(owningAgent));
            float AgentYNbLimit = float.Parse(AGT.getAgentYearlyTransLimit(owningAgent));

            String MonthlyRemit = customer.getMonthlyAgentRemittanceTotal(CID);
            float MRemit = float.Parse(MonthlyRemit);
            DIV_MonthlyRemittanceTotal.InnerText = String.Format("{0:C2}", MRemit);
            DIV_MonthlyRemittanceTotal.Attributes.Add("title", String.Format("{0:C2}", MRemit) + " / " + String.Format("{0:C2}", AgentMAUDLimit));

            if (MRemit >= AgentMAUDLimit)
            {
                DIV_MonthlyRemittanceTotal.Style.Add("color", "red");

            }

            String YearlyRemit = customer.getAgentYearlyRemittanceTotal(CID);
            float YRemit = float.Parse(YearlyRemit);
            DIV_YearlyRemittanceTotal.InnerText = String.Format("{0:C2}", YRemit);
            DIV_YearlyRemittanceTotal.Attributes.Add("title", String.Format("{0:C2}", YRemit) + " / " + String.Format("{0:C2}", AgentYAUDLimit));
            DIV_Totals.Attributes.Add("title", "Monthly Limit: " + AgentMNbLimit + " / Yearly Limit: " + AgentYNbLimit);

            DIV_Totals.InnerText = customer.getAgentSumMonthlyRemit(CID) + "/" + customer.getAgentSumYearlyRemit(CID);

            if (YRemit >= AgentYAUDLimit)
            {
                DIV_YearlyRemittanceTotal.Style.Add("color", "red");
            }
        }

        public void loadCustomerBeneficiaries(String CID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;

            String strQuery = "SELECT * FROM dbo.Beneficiaries WHERE CustomerID = " + CID + " AND Active = 'Y'";
            String FirstBeneficiary = String.Empty;


            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    ddl_List_Beneficiaries.Items.Add(new ListItem("--SELECT--", "NOSELECT"));
                    while (sdr.Read())
                    {
                        var items = new ListItem
                        {
                            Text = sdr["BeneficiaryName"].ToString(),
                            Value = sdr["BeneficiaryID"].ToString()
                        };

                        ddl_List_Beneficiaries.Items.Add(items);
                        //ddl_List_Beneficiaries.Attributes["onchange"] = "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")";
                        /*li = new HtmlGenericControl("li");
                        list_beneficiaries.Controls.Add(li);

                        HtmlGenericControl divAnchor = new HtmlGenericControl("div");
                        divAnchor.Attributes.Add("class", "beneficiary_bullet");

                        HtmlGenericControl imgAnchor = new HtmlGenericControl("img");
                        imgAnchor.Attributes.Add("src", "images/ben-avatar.png");
                        imgAnchor.Attributes.Add("width", "16px");

                        divAnchor.Controls.Add(imgAnchor);
                        li.Controls.Add(divAnchor);

                        HtmlGenericControl divAnchor2 = new HtmlGenericControl("div");
                        divAnchor2.InnerText = sdr["BeneficiaryName"].ToString();*/


                        //divAnchor2.Attributes.Add("onclick", "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")");

                        // li.Controls.Add(divAnchor2);


                        /*li.Attributes.Add("onclick", "populateBeneficaryDetails(" + sdr["BeneficiaryID"].ToString() + ")");
                        li.Attributes.Add("class", "beneficiary_link");
                        li.InnerText = sdr["BeneficiaryName"].ToString();

                        list_beneficiaries.Controls.Add(li);
                        */

                    }

                }

            }
        }

        public void WarningMessages(String CID)
        {
            // Check if customer has KYC After Yearly $1000 limit

            float YearRemit = float.Parse(customer.getYearlyRemittancetotal(CID).ToString());
            String HasKYC = customer.getCustomerHasKYC(CID);

            if (YearRemit >= 1000)
            {
                if (HasKYC == "false")
                {
                    //TD_WarningMessage.InnerText = "Customer has reached transaction limit without KYC. Please complete KYC or this transaction will have to be approved by either an Admin or Compliance Team.";
                }
            }

        }

        private string ConvertDateFormat(string date)
        {
            var dateS = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
            return dateS;
        }

        private CustomerValidDoc ValidateCustomerDocsAndKcyStatus(List<CustomerDocs> docs, int customerId)
        {
            var obj = new CustomerValidDoc();

            if (docs.Count() == 0)  // check doc valid
            {
                obj.Message = "No valid documents found";
                return obj;
            }

            if (CheckDocSValidDate(docs) == false)  // check doc valid
            {
                obj.Message = "All Documents Have Expired. Please upload new documents to proceed.";
                return obj;
            }

            var serVic = new CustomerHandlerService();
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        var doNumber = item.DocumentNumber;
                        var checkDocNumberList = serVic.GetCustomerAllDocsByDocumentNumber(doNumber);
                        if (checkDocNumberList.Count() > 0)
                        {
                            var validCustomerTran = IsCustomerHasValidTransactionId(checkDocNumberList);
                            if (validCustomerTran.status)
                            {
                                if (validCustomerTran.Customer != null)
                                {
                                    var updateKyc = new CustomerOrMerchantResponseKyced();
                                    updateKyc.agentTransactionId = validCustomerTran.Customer.agentTransactionID;
                                    updateKyc.resultCode = validCustomerTran.Customer.resultCode;
                                    updateKyc.resultDescription = validCustomerTran.Customer.resultDescription;
                                    updateKyc.transactionId = validCustomerTran.Customer.TransactionID;
                                    var cusInfo = serVic.GetCustomerById(validCustomerTran.customerId);

                                    var runBy = "";
                                    if (HttpContext.Current.Session["usernameLogged"] != null)
                                        runBy = HttpContext.Current.Session["usernameLogged"].ToString();
                                    var UpdateNeedCustomer = serVic.GetCustomerById(customerId);
                                    serVic.UpdateCustomerByKASIWEBAPIData(updateKyc, customerId,
                                        UpdateNeedCustomer.RecordID, runBy, false);
                                    var updateInfoCustomer = serVic.GetCustomerById(UpdateNeedCustomer.CustomerID);
                                    obj.Status = true;
                                    obj.RecordExist = true;
                                    obj.Message = " Result " + updateInfoCustomer.KCYResult + ". KYC checked date " + updateInfoCustomer.KYCCheckDate.Date.ToShortDateString();
                                    return obj;
                                }
                            }
                            else
                            {
                                obj.Status = true;
                                obj.CanConnectApi = true;
                                return obj;
                            }

                        }
                        else
                        {
                            obj.Status = true;
                            obj.CanConnectApi = true;
                            return obj;
                        }
                    }

                }

            }

            return obj;

        }

        protected class CustomerValidYear
        {
            public int customerId { get; set; }
            public bool status { get; set; }
            public CustomerForKyc Customer { get; set; }
        }

        private CustomerValidYear IsCustomerHasKycMoreThanOneYear(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {

                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (customerKcCheck.KYCCheckDate != null)
                {
                    if (customerKcCheck.KYCCheckDate != DateTime.MinValue)
                    {
                        var KYCCheckDateyear = customerKcCheck.KYCCheckDate.AddYears(1);
                        var currentDate = DateTime.Now;
                        if (currentDate > KYCCheckDateyear)
                        {
                            validDocFound.status = true;
                            validDocFound.customerId = customerforKyc.CustomerID;
                            validDocFound.Customer = customerKcCheck;

                        }
                    }

                }
            }

            return validDocFound;
        }

        private CustomerValidYear IsCustomerHasKycLessThanOneYear(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {
                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (customerKcCheck.KYCCheckDate != null || customerKcCheck.KYCCheckDate != DateTime.MinValue)
                {
                    var KYCCheckDateyear = customerKcCheck.KYCCheckDate.AddYears(1);
                    var currentDate = DateTime.Now;
                    if (currentDate < KYCCheckDateyear)
                    {
                        validDocFound.status = true;
                        validDocFound.customerId = customerforKyc.CustomerID;

                    }

                }
            }

            return validDocFound;
        }

        private CustomerValidYear IsCustomerHasValidTransactionId(List<CustomerDocs> docs)
        {
            var serVic = new CustomerHandlerService();
            var validDocFound = new CustomerValidYear();
            foreach (var customerforKyc in docs)
            {
                var customerKcCheck = serVic.GetCustomerById(customerforKyc.CustomerID);

                if (!string.IsNullOrEmpty(customerKcCheck.TransactionID))
                {

                    validDocFound.status = true;
                    validDocFound.customerId = customerforKyc.CustomerID;
                    validDocFound.Customer = customerKcCheck;

                    return validDocFound;
                }
            }

            return validDocFound;
        }

        private bool CheckDocSValidDate(List<CustomerDocs> docs)
        {
            var validDocFound = false;
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    DateTime trpDate = new DateTime();
                   DateTime.TryParse(item.ExpiryDate,out trpDate);
                    var expiryDate = trpDate.Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        validDocFound = true;
                        return validDocFound;
                    }
                }
            }

            return validDocFound;
        }

        private void CreateKYCEInfo(int customerId, bool recheckKyc = false)
        {
            var cService = new CustomerHandlerService();
            var customerForKyc = cService.GetCustomerById(customerId);
            classes.Customer CUST = new classes.Customer();
            var resulCall = new ResultKycCall();
            if (customerForKyc.IsIndividual)
            {
                if (string.IsNullOrEmpty(customerForKyc.FirstName))
                {
                    TR_DisplayKYCErrors.Style.Add("display", "block");
                    LBL_KYCErrors.Text = "Customer First Name is Mandatory.";
                    //KYC_PleaseWait.Style.Add("display", "none");
                    return;

                }

                var customerAllDocs = cService.GetCustomerAllDocs(customerId);

                var resultCallMe = new CustomerValidDoc();

                if (customerAllDocs.Count() == 0)  // check doc valid
                {
                    resultCallMe.Message = "No valid documents found";

                }
                else
                {

                    if (CheckDocSValidDate(customerAllDocs) == false)  // check doc valid
                    {
                        resultCallMe.Message = "All Documents Have Expired. Please upload new documents to proceed.";

                    }
                    else
                    {
                        resultCallMe.CanConnectApi = true;
                    }
                }

                if (recheckKyc == false)
                {
                    resultCallMe = ValidateCustomerDocsAndKcyStatus(customerAllDocs, customerId);
                    if (resultCallMe.Status == false)
                    {
                        TR_DisplayKYCErrors.Style.Add("display", "block");
                        LBL_KYCErrors.Text = resultCallMe.Message;
                        //KYC_PleaseWait.Style.Add("display", "none");
                        return;
                    }
                    else
                        if (resultCallMe.RecordExist)
                        {
                            TR_DisplayKYCErrors.Style.Add("display", "block");
                            LBL_KYCErrors.Text = resultCallMe.Message;
                            //KYC_PleaseWait.Style.Add("display", "none");
                            displayKYCInfo(customerId.ToString());
                            return;
                        }
                }

                if (resultCallMe.CanConnectApi)
                    resulCall = CallCustomerKcy(customerId, cService, customerForKyc, resulCall, customerAllDocs);
            }

            else
            {

                if (string.IsNullOrEmpty(customerForKyc.BusinessName))
                {
                    TR_DisplayKYCErrors.Style.Add("display", "block");
                    LBL_KYCErrors.Text = "Warning: Business Legal Name is Mandatory";
                    //KYC_PleaseWait.Style.Add("display", "none");
                    return;

                }
                var customerAllDocs = cService.GetCustomerAllDocs(customerId);
                var resultCallMe = ValidateCustomerDocsAndKcyStatus(customerAllDocs, customerId);

                if (resultCallMe.Status == false)
                {
                    //ErrorMessageArea.Visible = true;
                    //errormessagedisplay.InnerText = "Info: " + resultCallMe.Message;
                    TR_DisplayKYCErrors.Style.Add("display", "inline");
                    LBL_KYCErrors.Text = resultCallMe.Message;

                    return;
                }
                else
                    if (resultCallMe.RecordExist)
                    {
                        //ErrorMessageArea.Visible = true;
                        //errormessagedisplay.InnerText = "Info: " + resultCallMe.Message;
                        TR_DisplayKYCErrors.Style.Add("display", "inline");
                        LBL_KYCErrors.Text = resultCallMe.Message;
                        displayKYCInfo(customerId.ToString());
                        return;
                    }
                    else
                        resulCall = CallMerchant(customerId, cService, customerForKyc, resulCall);



            }

            if (resulCall.Status)
            {
                TR_DisplayKYCErrors.Style.Add("display", "block");
                dsh_KYC_NotRun.Style.Add("display", "none");
                dsh_KYC_Fail.Style.Add("display", "none");
                dsh_KYC_Success.Style.Add("display", "block");
                TD_KYCResultDisplay.Attributes.Remove("class");
                TD_KYCResultDisplay.Attributes.Add("class", "kyc_info_green");
                LBL_KYCErrors.Text = "KYC Check Successful";
                String splitResult = String.Empty;
                setRiskLevel(CUST.getCustomerRiskLevel(customerId.ToString()));
                //KYC_PleaseWait.Style.Add("display", "none");
                displayKYCInfo(customerId.ToString());
            }
            else
            {
                TR_DisplayKYCErrors.Style.Add("display", "block");
                LBL_KYCErrors.Text = "KYC Failed: " + resulCall.Message;
                //KYC_PleaseWait.Style.Add("display", "none");
                //displayKYCInfo(customerId.ToString());
            }


        }

        private static ResultKycCall CallMerchant(int customerId, CustomerHandlerService cService,
            CustomerForKyc customerForKyc, ResultKycCall resulCall)
        {
            var merchant = new MerchantKyced();
            if (customerForKyc.TradingName != null)
                merchant.dba = customerForKyc.TradingName;
            else
                merchant.dba = "";

            merchant.amn = customerForKyc.BusinessName;

            merchant.website = "";
            if (customerForKyc.City != null)
                merchant.ac = customerForKyc.City;
            else
                merchant.ac = "";
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    merchant.aco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    merchant.aco = "AU";
            }
            else
                merchant.aco = "AU";
            if (customerForKyc.FirstName != null)
                merchant.afn = customerForKyc.FirstName;
            else
                merchant.afn = "";
            if (customerForKyc.LastName != null)
                merchant.aln = customerForKyc.LastName;
            else
                merchant.aln = "";
            if (customerForKyc.Mobile != null)
                merchant.aph = customerForKyc.Mobile;
            else
                merchant.aph = "";
            if (customerForKyc.Postcode != null)
                merchant.az = customerForKyc.Postcode;
            else
                merchant.az = "";

            if (customerForKyc.State != null)
                merchant.@as = customerForKyc.State;
            else
                merchant.@as = "";
            if (customerForKyc.StreetName != null)
                merchant.asn = customerForKyc.StreetName;
            else
                merchant.asn = "";
            if (customerForKyc.ABN != null)
                merchant.ataxId = customerForKyc.ABN;
            else
                merchant.ataxId = "";

            merchant.bin = "";
            if (customerForKyc.RiskLevel != null)
                merchant.bin = customerForKyc.RiskLevel;

            var runBy = "";
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            resulCall = cService.CreateKycedMerchant(merchant, customerId, customerForKyc.RecordID, runBy);
            return resulCall;
        }

        private ResultKycCall CallCustomerKcy(int customerId, CustomerHandlerService cService,
            CustomerForKyc customerForKyc,
            ResultKycCall resulCall, List<CustomerDocs> docsCustomer)
        {

            var cusKyc = new CustomerKyced();
            cusKyc.profile = "FLEXEWALLET";
            if (customerForKyc.Title != null)
                cusKyc.title = customerForKyc.Title;
            else
                cusKyc.title = null;
            if (customerForKyc.FirstName != null)
                cusKyc.bfn = customerForKyc.FirstName;
            else
                cusKyc.bfn = null;

            if (customerForKyc.LastName != null)
                cusKyc.bln = customerForKyc.LastName;
            else
                cusKyc.bln = null;
            if (customerForKyc.DOB != null)
            {

                cusKyc.dob = ConvertDateFormat(customerForKyc.DOB);
            }
            else
                cusKyc.dob = null;
            if (customerForKyc.CustomerID > 0)
                cusKyc.man = customerForKyc.CustomerID.ToString();
            else
                cusKyc.man = null;
            if (customerForKyc.City != null)
                cusKyc.bc = customerForKyc.City;
            else
                cusKyc.bc = null;
            if (customerForKyc.Country != null)
            {
                if (customerForKyc.Country.Length >= 2)
                    cusKyc.bco = customerForKyc.Country.Substring(0, 2).ToUpper();
                else
                    cusKyc.bco = "AU";
            }
            else
                cusKyc.bco = "AU";

            if (customerForKyc.State != null)
                cusKyc.bs = customerForKyc.State;
            else
                cusKyc.bs = null;
            if (customerForKyc.StreetName != null)
                cusKyc.bsn = customerForKyc.StreetName;
            else
                cusKyc.bsn = null;
            if (customerForKyc.Postcode != null)
                cusKyc.bz = customerForKyc.Postcode;
            else
                cusKyc.bz = null;
            if (customerForKyc.Mobile != null)
                cusKyc.pw = customerForKyc.Mobile;
            else
                cusKyc.pw = null;
            if (customerForKyc.EmailAddress != null)
                cusKyc.tea = customerForKyc.EmailAddress;
            else
                cusKyc.tea = "";
            cusKyc.faceImage = null;
            string bas64Convert = null;
            var pathFormat = "~/CustomerDocuments/{0}";
            pathFormat = string.Format(pathFormat, customerId);
            var filePath = Server.MapPath(pathFormat);
           
            //if (Directory.Exists(filePath))
            //{
            //    var files = Directory.GetFiles(filePath);

            //    var valiedOldestDoc = GetOldestDoc(docsCustomer);
            //    var stringDocumentGuid = "";
            //    if (valiedOldestDoc.Count > 0)
            //        stringDocumentGuid = valiedOldestDoc[0].DocumentGUID;
              
            //    cusKyc.faceImage = null;
            //    if (files != null)
            //    {
            //        if (!string.IsNullOrEmpty(stringDocumentGuid))
            //        {
            //            var selectedFile = (from sef in files where Path.GetFileName(sef) == stringDocumentGuid select sef).ToList();
            //            if (selectedFile.Count > 0)
            //            {
            //                var firstFileObj = selectedFile[0];
            //                var decFilePath = firstFileObj;//DecryptFile(firstFileObj, customerId.ToString()); // disabled for audti turn it on back later
            //                bas64Convert = Utilities.ConvertFileToBase64(decFilePath);
            //                //  if (File.Exists(decFilePath))
            //                //   File.Delete(decFilePath);
            //            }
            //        }
            //    }
            //}
           // if (bas64Convert != null)
              //  cusKyc.faceImage = bas64Convert;


            cusKyc.documentImageFront = null;
            //cusKyc.documentImangeBack = "";
            cusKyc.securityNumber = null;
            cusKyc.ip = null;
            //if (customerdoc != null)
            //{
            //    if (customerdoc.hasDriverLicence)
            //    {
            //        //var imageDriver = Utilities.ByteArrayToImage(customerdoc.DocumentDriverImage1);
            //        cusKyc.documentImageFront = customerdoc.DocumentDriverImage1;
            //    }
            //}
            var runBy = "";
            if (HttpContext.Current.Session["usernameLogged"] != null)
                runBy = HttpContext.Current.Session["usernameLogged"].ToString();
            resulCall = cService.CreateKycedCustomer(cusKyc, customerId, customerForKyc.RecordID, runBy);

            CustAuditLogService CALServ = new CustAuditLogService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            CustAuditLog CustLog = new CustAuditLog();
            CustLog.CustomerID = customerId;
            CustLog.Username = HttpContext.Current.Session["usernameLogged"].ToString();
            CustLog.AgentName = Session["LoggedCompany"].ToString();
            CustLog.ActionType = "EDIT";
            CustLog.Description = "has run the KYC";
            CustLog.AuditDateTime = DateTime.Now;

            CALServ.Add(CustLog);

            return resulCall;
        }

        private string DecryptFile(string filePath, string cusID)
        {
            string fPath = "";

            var tempPath = HttpContext.Current.Server.MapPath("~/temp/");
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);
            var decFiLeName = "customerFile_" + cusID + Path.GetExtension(filePath);
            fPath = tempPath + decFiLeName;
            Utilities.Decrypt(filePath, fPath);

            return fPath;
        }

        private List<ValidOldestDoc> GetOldestDoc(List<CustomerDocs> docsCustomer)
        {
            var objList = new List<ValidOldestDoc>();
            foreach (var item in docsCustomer)
            {
                var obj = new ValidOldestDoc();
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    obj.ExpiryDate = expiryDate;
                    obj.RecordID = item.RecordID;
                    obj.CustomerID = item.CustomerID;
                    obj.DocumentGUID = item.DocumentGUID;
                    obj.DocumentName = item.DocumentName;
                    obj.TypeOfDocument = item.TypeOfDocument;
                    obj.TypeOfDocumentID = item.TypeOfDocumentID;
                    obj.DocumentNumber = item.DocumentNumber;
                    obj.hasPassport = item.hasPassport;
                    objList.Add(obj);
                }
            }

            objList = objList.OrderByDescending(x => x.ExpiryDate).ToList();
            return objList;
        }



        private bool iSCustomerHasValidDocs()
        {
            return true;
        }


        private CustomerValidDoc ValidateKYCRecords(List<CustomerDocs> docs, int customerId)
        {
            var obj = new CustomerValidDoc();

            if (docs.Count() == 0)  // check doc valid
            {
                obj.Message = "No valid documents found";
                return obj;
            }

            if (CheckDocSValidDate(docs) == false)  // check doc valid
            {
                obj.Message = "No valid documents found";
                return obj;
            }

            var serVic = new CustomerHandlerService();
            foreach (var item in docs)
            {
                if (!string.IsNullOrEmpty(item.ExpiryDate))
                {
                    var expiryDate = Convert.ToDateTime(item.ExpiryDate).Date;
                    if (expiryDate > DateTime.Now.Date)
                    {
                        var doNumber = item.DocumentNumber;
                        var checkDocNumberList = serVic.GetCustomerAllDocsByDocumentNumber(doNumber);
                        if (checkDocNumberList.Count() > 1)
                        {
                            var oneYearLessCheck = IsCustomerHasKycLessThanOneYear(checkDocNumberList);
                            if (oneYearLessCheck.status)
                            {
                                var cusInfo = serVic.GetCustomerById(oneYearLessCheck.customerId);
                                obj.Status = true;
                                obj.RecordExist = true;
                                obj.Message = " Result " + cusInfo.KCYResult + ". KYC checked date " + cusInfo.KYCCheckDate.Date.ToShortDateString();
                                return obj;
                            }
                        }
                    }
                }
            }

            return obj;
        }


        protected void checkIfKYCHasBeenPerformed(String CID)
        {
            classes.Customer cust = new classes.Customer();
            String hasKYC = cust.getCustomerHasKYC(CID);
            var customerId = Convert.ToInt32(CID);
            var customerAllDocs = new CustomerHandlerService().GetCustomerAllDocs(customerId);
            var validateResultKYC = ValidateKYCRecords(customerAllDocs, customerId);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Kapruka.Repository.Customer cust = new Kapruka.Repository.Customer();
            // int custId = int.Parse(hidCustId.Value);
            // string type = hidCustType.Value;
            // cust.CustomerID = custId;
            // CustomerService service = new CustomerService(new Kapruka.Repository.UnitOfWorks(new Kapruka.Repository.KaprukaEntities()));
            //// cust = service.GetAll(x => x.CustomerID == custId, null, "").SingleOrDefault();
            // if (type=="Busi")
            // {

            //     cust.BusinessName=BBusinessName.Value;
            //     cust.ABN=BABN.Value;
            //     cust.TradingName=BTradingName.Value;
            //     cust.UnitNo=bus_unitno.Value;
            //     cust.StreetNo = bus_streetno.Value;
            //     cust.StreetName=bus_streetname.Value;
            //    // cust.StreetType=bus_streettype
            //    // ListItem li = bus_streettype.Items.FindByText(cust.StreetType);
            //    // if (li != null)
            //   //  {
            //   //      li.Selected = true;
            //   //  }
            //     cust.Suburb=bus_suburb.Value;
            //     cust.PostalAddressState=BPostalState.Value;
            //     cust.Postcode=BPostalPostcode.Value;
            //     cust.PostalAddressCountry=pos_country.Value;
            //     //cust.LastName=BLastName.Value;
            //     //ListItem lit = Btitle.Items.FindByText(cust.Title);
            //     //if (lit != null)
            //     //{
            //     //    lit.Selected = true;
            //     //}
            //     //cust.DOB=BDOB.Value;
            //     //cust.FirstName=BFirstNames.Value;
            //     //cust.TelHome=BHomePhone.Value;
            //     //cust.TelWork=BPhoneWork.Value;
            //     //cust.Mobile=BMobilePhone.Value;
            //     //cust.EmailAddress=BEmail1.Value;
            //     //cust.EmailAddress2=BEmail2.Value;
            //     cust.BusinessContact=BBContact.Value;
            //     cust.BusinessEmail=BBEmail.Value;
            //     cust.BusinessWebSite=BBWeb.Value;

            // }
            // else
            // {

            //     divBusiness.Visible = false;
            //     tblIndi.Visible = true;
            //     cust.LastName=lastname.Value;// customer.getCustomerLastName(CID);
            //     cust.FirstName=firstnames.Value;// customer.getCustomerFirstName(CID);
            //     //addressline1.Value = customer.getCustomerAddressLine1(CID);
            //     //addressline2.Value = customer.getCustomerAddressLine2(CID);
            //     cust.Suburb=suburb.Value;// customer.getCustomerSuburb(CID);
            //     cust.State=state.Value;// customer.getCustomerState(CID);
            //     cust.Postcode=postcode.Value;// customer.getCustomerPostcode(CID);
            //     cust.TelHome=phonehome.Value;// customer.getCustomerHomeNumber(CID);
            //     cust.TelWork=phonework.Value;// customer.getCustomerWorkNumber(CID);
            //     cust.Mobile=phonemobile.Value;// customer.getCustomerMobile(CID);
            //     cust.DOB=dob.Value;// customer.getCustomerDOB(CID);
            //     cust.EmailAddress=email1.Value;// customer.getCustomerEmailAddress1(CID);
            //     cust.EmailAddress2=email2.Value;// customer.getCustomerEmailAddress2(CID);
            //     cust.Notes=edit_notes.Value;// customer.getCustomerNotes(CID);
            //     cust.AccountPassword=edit_password.Value;// customer.getCustomerPassword(CID);
            //     //showpassword.InnerHtml;// customer.getCustomerPassword(CID).ToUpper();
            //     //shownotes.InnerHtml = cust.Notes;// customer.getCustomerNotes(CID);
            //     cust.PlaceOfBirth=placeofbirth.Value;// customer.getCustomerPlaceOfBirth(CID);
            //     cust.AKA=aka.Value;// customer.getCustomerAKA(CID);
            //     cust.Occupation=Cust_occupation.Value;// customer.getCustomerOccupation(CID);
            //     cust.UnitNo=unitno.Value;// customer.getCustomerUnitNo(CID);
            //     cust.StreetNo=streetno.Value;// customer.getCustomerStreetNo(CID);
            //     cust.StreetName=streetname.Value;// customer.getCustomerStreetName(CID);
            //     cust.Country=country.Value;// customer.getCustomerCountry(CID);


            //     //ListItem li = streettype.Items.FindByText(cust.StreetType);
            //     //if (li != null)
            //     //{
            //     //    li.Selected = true;
            //     //}


            //     //ListItem COB = countryofbirth.Items.FindByText(cust.CountryOfBirth);
            //     //if (COB != null)
            //     //{
            //     //    COB.Selected = true;
            //     //}


            //     //ListItem National = nationality.Items.FindByText(cust.Nationality);
            //     //if (National != null)
            //     //{
            //     //    National.Selected = true;
            //     //}


            // }

            // service.Update(cust);

            // //if (edit_password.Value == "")
            // //{
            // //    trpassword.Visible = false;
            // //    trpassworspacer.Visible = false;
            // //}

            // //if (edit_notes.Value == "")
            // //{
            // //    trnotes.Visible = false;
            // //    trpassworspacer.Visible = false;
            // //}

            // //if (edit_password.Value == "" && edit_notes.Value == "")
            // //{
            // //    NotesAreaTR.Visible = false;
            // //}
        }

        protected void BTN_CheckKYC_Click(object sender, EventArgs e)
        {
            // First check if customer has valid documents

            var customerId = Convert.ToInt32(Request.QueryString["custid"].ToString());
            //KYC_PleaseWait.Style.Add("display", "block");
            if (hidden_KYCRunBefore.Value == "true")
            {
                TR_ConfirmRUNKYC.Style.Add("display", "block");
                TR_RUNKYC.Style.Add("display", "none");
            }
            else
            {
                CreateKYCEInfo(customerId);
                Response.Redirect("DashboardW.aspx?custid=" + customerId);
            }
        }

        protected void confirm_YES_RunKYC_Click(object sender, EventArgs e)
        {
            
              //  buttonyesKychidden.Value = "";
                var customerId = Convert.ToInt32(Request.QueryString["custid"].ToString());
                CreateKYCEInfo(customerId, true);
                TR_ConfirmRUNKYC.Style.Add("display", "none");
                TR_RUNKYC.Style.Add("display", "block");
                Response.Redirect("DashboardW.aspx?custid=" + customerId);
            
        }

        protected void confirm_NO_RunKYC_Click(object sender, EventArgs e)
        {
            TR_ConfirmRUNKYC.Style.Add("display", "none");
            TR_RUNKYC.Style.Add("display", "block");
        }

        protected void btn_ValidateAccount_Click(object sender, EventArgs e)
        {

        }
    }

    public class CustomerValidDoc
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public bool CanConnectApi { get; set; }
        public bool RecordExist { get; set; }

    }
}