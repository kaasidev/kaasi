﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KASI_Extend_.classes;

namespace KASI_Extend_
{
    public partial class TramsactionsW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TypeOfLogin"] == null)
            {
                Response.Redirect("/Login.aspx");
            }

            txt_IsComplianceManager.Value = Session["IsComplianceManager"].ToString();
            txt_hiddenCompliancePIN.Value = Session["CompliancePIN"].ToString();
            SPN_APPR_LoggedUser.InnerText = Session["LoggedPersonName"].ToString();
            SPN_LoggedUserName.InnerText = Session["LoggedPersonName"].ToString();
        }
    }
}