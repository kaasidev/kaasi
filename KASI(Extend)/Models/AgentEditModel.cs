﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.Models
{
    public class AgentEditModel
    {
        public string AgentName { get; set; }
        public string AgentAddress1 { get; set; }
        public string AgentAddress2 { get; set; }
        public string AgentCity { get; set; }
        public string AgentState { get; set; } 
        public string AgentPostcode { get; set; }
        public string AgentPhone { get; set; }
        public string AgentFax { get; set; }
        public string AgentEmail { get; set; }
        public string AgentABN { get; set; }
        public string AgentACN { get; set; }
        public string AgentCommissionStructure { get; set; }
        public string AgentFeeCommission { get; set; }
        public string AgentFeeFlat { get; set; }
        public string AgentTransGainCommission { get; set; }
        public string AgentAustracNumber { get; set; }
        public string AgentAustracRegDate { get; set; }
        public string AgentAustracRenewalDate { get; set; }
        public string AgentCountries { get; set; }
        public string BankIds { get; set; }

                   
    }
}