﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.Models
{
    public class LoginUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string LoginGroup { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string LoginType { get; set; }
        public string AgentID { get; set; }
        public string CompMan { get; set; }
        public string CompManPIN { get; set; }
    }
}