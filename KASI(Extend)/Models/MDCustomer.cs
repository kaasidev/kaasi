﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace KASI_Extend_.Models
{
    [Table("customers")]
    public class MDCustomer
    {
        [Key]
        public int customerID { get; set; }
        [Required]
        public string custFirstName { get; set; }
        [Required]
        public string custLastName { get; set; }
        public decimal accountBalance { get; set; }
    }
}