﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KASI_Extend_.Models
{
    public class MasterBankModel
    {
        public string BankName { get; set; }
        public string BSB { get; set; }
        public string BSB2 { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
    }
}