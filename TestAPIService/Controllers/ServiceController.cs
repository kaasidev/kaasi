﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAPIService.Models;
namespace TestAPIService.Controllers
{
    public class ServiceController : ApiController
    {
        //
        // GET: /Service/

        [AllowAnonymous]
        [Route("kyc/customer")]
        [HttpPost]
        public CustomerOrMerchantResponseKyced CreateCustomerKyc(CustomerKyced cc)
        {
            var obj = new CustomerOrMerchantResponseKyced()
            {
                agentTransactionId = "tran" + cc.fname,
                resultCode="res",
                resultDescription="des",
                transactionId="idTran"
            };

            return obj;
        }

        [AllowAnonymous]
        [Route("kyc/merchant")]
        [HttpPost]
        public CustomerOrMerchantResponseKyced CreateMerchantKyc(MerchantKyced cc)
        {
            var obj = new CustomerOrMerchantResponseKyced()
            {
                agentTransactionId = "tran" + cc.firstName,
                resultCode = "res",
                resultDescription = "des",
                transactionId = "idTran"
            };

            return obj;
        }

    }
}
