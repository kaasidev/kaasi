﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPIService.Models
{
    public class CustomerOrMerchantResponseKyced
    {
        public string agentTransactionId { get; set; }
        public string transactionId { get; set; }
        public string resultCode { get; set; }
        public string resultDescription { get; set; }
    }

    public class MerchantKyced
    {
        public string businessCode { get; set; }
        public string taxId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string streetAddress { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string postCode { get; set; }
        public string phoneNumber { get; set; }
        public string alternativeName { get; set; }
        public string riskId { get; set; }
        public string url { get; set; }
        public string businessType { get; set; }

    }

    public class CustomerKyced
    {
        public string title { get; set; }
        public string username { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string dob { get; set; }
        public string securityNumber { get; set; }
        public string documentImageFront { get; set; }
        public string documentImangeBack { get; set; }
        public string faceimage { get; set; }


    }
}