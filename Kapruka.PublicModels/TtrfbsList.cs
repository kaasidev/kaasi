﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kapruka.PublicModels
{
    [XmlRoot(ElementName = "ttr-fbsList", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class TtrfbsList
    {
        [XmlElement(ElementName = "reNumber", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string ReNumber { get; set; }
        [XmlElement(ElementName = "fileName", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string FileName { get; set; }
        [XmlElement(ElementName = "reportCount", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string ReportCount { get; set; }
        [XmlElement(ElementName = "ttr-fbs", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public List<Ttrfbs> Ttrfbs { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }
        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }
        [XmlAttribute(AttributeName = "versionMajor")]
        public string VersionMajor { get; set; }
        [XmlAttribute(AttributeName = "versionMinor")]
        public string VersionMinor { get; set; }
    }
    [XmlRoot(ElementName = "header", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class HeaderTTR
    {
        [XmlElement(ElementName = "txnRefNo", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string TxnRefNo { get; set; }
        [XmlElement(ElementName = "interceptFlag", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string InterceptFlag { get; set; }
        [XmlElement(ElementName = "reportingBranch", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string ReportingBranch { get; set; }
    }

    [XmlRoot(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class AccountTTR
    {
        [XmlElement(ElementName = "type", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "agent", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class Agent
    {
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "altName", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public List<string> AltName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "postalAddress", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string PostalAddress { get; set; }
        [XmlElement(ElementName = "phone", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "indOcc", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string IndOcc { get; set; }
        [XmlElement(ElementName = "abn", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Abn { get; set; }
        [XmlElement(ElementName = "acn", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Acn { get; set; }
        [XmlElement(ElementName = "arbn", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Arbn { get; set; }
        [XmlElement(ElementName = "businessStruct", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string BusinessStruct { get; set; }
        [XmlElement(ElementName = "dob", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Dob { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public AccountTTR Account { get; set; }
        [XmlElement(ElementName = "identification", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Identification { get; set; }
        [XmlElement(ElementName = "electDataSrc", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string ElectDataSrc { get; set; }
        [XmlElement(ElementName = "partyIsRecipient", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string PartyIsRecipient { get; set; }
    }

    [XmlRoot(ElementName = "customer", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class CustomerTTR
    {
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "altName", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string AltName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "postalAddress", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string PostalAddress { get; set; }
        [XmlElement(ElementName = "phone", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "indOcc", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string IndOcc { get; set; }
        [XmlElement(ElementName = "abn", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Abn { get; set; }
        [XmlElement(ElementName = "acn", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Acn { get; set; }
        [XmlElement(ElementName = "arbn", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Arbn { get; set; }
        [XmlElement(ElementName = "businessStruct", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string BusinessStruct { get; set; }
        [XmlElement(ElementName = "dob", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Dob { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public AccountTTR Account { get; set; }
        [XmlElement(ElementName = "identification", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Identification { get; set; }
        [XmlElement(ElementName = "electDataSrc", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string ElectDataSrc { get; set; }
        [XmlElement(ElementName = "partyIsRecipient", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string PartyIsRecipient { get; set; }
    }

    [XmlRoot(ElementName = "recipient", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class Recipient
    {
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public AccountTTR Account { get; set; }
        [XmlElement(ElementName = "dob", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string Dob { get; set; }
    }

    [XmlRoot(ElementName = "cash", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class Cash
    {
        [XmlElement(ElementName = "ausCash", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string AusCash { get; set; }
        [XmlElement(ElementName = "foreignCash", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public List<string> ForeignCash { get; set; }
    }

    [XmlRoot(ElementName = "moneyReceived", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class MoneyReceived
    {
        [XmlElement(ElementName = "cash", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public Cash Cash { get; set; }
        [XmlElement(ElementName = "nonCash", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string NonCash { get; set; }
    }

    [XmlRoot(ElementName = "moneyProvided", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class MoneyProvided
    {
        [XmlElement(ElementName = "cash", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public Cash Cash { get; set; }
        [XmlElement(ElementName = "nonCash", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string NonCash { get; set; }
    }

    [XmlRoot(ElementName = "transaction", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class TransactionTTR
    {
        [XmlElement(ElementName = "designatedSvc", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string DesignatedSvc { get; set; }
        [XmlElement(ElementName = "txnDate", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "totalAmount", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public string TotalAmount { get; set; }
        [XmlElement(ElementName = "moneyReceived", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public MoneyReceived MoneyReceived { get; set; }
        [XmlElement(ElementName = "moneyProvided", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public MoneyProvided MoneyProvided { get; set; }
    }

    [XmlRoot(ElementName = "ttr-fbs", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
    public class Ttrfbs
    {
        [XmlElement(ElementName = "header", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public HeaderTTR Header { get; set; }
        [XmlElement(ElementName = "agent", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public List<Agent> Agent { get; set; }
        [XmlElement(ElementName = "customer", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public List<CustomerTTR> Customer { get; set; }
        [XmlElement(ElementName = "recipient", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public List<Recipient> Recipient { get; set; }
        [XmlElement(ElementName = "transaction", Namespace = "http://austrac.gov.au/schema/reporting/TTR-FBS-1")]
        public TransactionTTR Transaction { get; set; }
    }

}
