﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kapruka.PublicModels
{
    [XmlRoot(ElementName = "smrList", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class SmrList
    {
        [XmlElement(ElementName = "reNumber", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string ReNumber { get; set; }
        [XmlElement(ElementName = "fileName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string FileName { get; set; }
        [XmlElement(ElementName = "reportCount", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string ReportCount { get; set; }
        [XmlElement(ElementName = "smr", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<Smr> Smr { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }
        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }
        [XmlAttribute(AttributeName = "versionMajor")]
        public string VersionMajor { get; set; }
        [XmlAttribute(AttributeName = "versionMinor")]
        public string VersionMinor { get; set; }
    }

    [XmlRoot(ElementName = "header", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class HeaderSMR
    {
        [XmlElement(ElementName = "reReportRef", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string ReReportRef { get; set; }
        [XmlElement(ElementName = "interceptFlag", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string InterceptFlag { get; set; }
        [XmlElement(ElementName = "reportingBranch", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string ReportingBranch { get; set; }
    }

    [XmlRoot(ElementName = "smDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class SmDetails
    {
        [XmlElement(ElementName = "designatedSvc", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string DesignatedSvc { get; set; }
        [XmlElement(ElementName = "designatedSvcProvided", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string DesignatedSvcProvided { get; set; }
        [XmlElement(ElementName = "designatedSvcRequested", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string DesignatedSvcRequested { get; set; }
        [XmlElement(ElementName = "designatedSvcEnquiry", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string DesignatedSvcEnquiry { get; set; }
        [XmlElement(ElementName = "suspReason", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SuspReason { get; set; }
        [XmlElement(ElementName = "suspReasonOther", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SuspReasonOther { get; set; }
        [XmlElement(ElementName = "grandTotal", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string GrandTotal { get; set; }
    }

    [XmlRoot(ElementName = "suspGrounds", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class SuspGrounds
    {
        [XmlElement(ElementName = "groundsForSuspicion", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string GroundsForSuspicion { get; set; }
    }

    [XmlRoot(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class AccountSMR
    {
        [XmlElement(ElementName = "type", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Type { get; set; }
        [XmlElement(ElementName = "acctSigName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> AcctSigName { get; set; }
        [XmlElement(ElementName = "acctOpenDate", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string AcctOpenDate { get; set; }
        [XmlElement(ElementName = "acctBal", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string AcctBal { get; set; }
        [XmlElement(ElementName = "documentation", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Documentation { get; set; }
    }

    [XmlRoot(ElementName = "businessDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class BusinessDetails
    {
        [XmlElement(ElementName = "businessStruct", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string BusinessStruct { get; set; }
        [XmlElement(ElementName = "benName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> BenName { get; set; }
        [XmlElement(ElementName = "holderName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> HolderName { get; set; }
        [XmlElement(ElementName = "incorpCountry", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string IncorpCountry { get; set; }
        [XmlElement(ElementName = "documentation", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Documentation { get; set; }
    }

    [XmlRoot(ElementName = "individualDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class IndividualDetailsSMR
    {
        [XmlElement(ElementName = "dob", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Dob { get; set; }
        [XmlElement(ElementName = "citizenCountry", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> CitizenCountry { get; set; }
    }

    [XmlRoot(ElementName = "identification", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class IdentificationSMR
    {
        [XmlElement(ElementName = "idIssueDate", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string IdIssueDate { get; set; }
        [XmlElement(ElementName = "idExpiryDate", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string IdExpiryDate { get; set; }
    }

    [XmlRoot(ElementName = "suspPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class SuspPerson
    {
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "altName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> AltName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "postalAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PostalAddress { get; set; }
        [XmlElement(ElementName = "phone", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Phone { get; set; }
        [XmlElement(ElementName = "email", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Email { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<AccountSMR> Account { get; set; }
        [XmlElement(ElementName = "indOcc", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string IndOcc { get; set; }
        [XmlElement(ElementName = "abn", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Abn { get; set; }
        [XmlElement(ElementName = "acn", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Acn { get; set; }
        [XmlElement(ElementName = "arbn", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Arbn { get; set; }
        [XmlElement(ElementName = "businessDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public BusinessDetails BusinessDetails { get; set; }
        [XmlElement(ElementName = "individualDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public IndividualDetailsSMR IndividualDetails { get; set; }
        [XmlElement(ElementName = "identification", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<IdentificationSMR> Identification { get; set; }
        [XmlElement(ElementName = "electDataSrc", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> ElectDataSrc { get; set; }
        [XmlElement(ElementName = "personIsCustomer", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PersonIsCustomer { get; set; }
    }

    [XmlRoot(ElementName = "otherPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class OtherPerson
    {
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "altName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> AltName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "postalAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PostalAddress { get; set; }
        [XmlElement(ElementName = "phone", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Phone { get; set; }
        [XmlElement(ElementName = "email", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Email { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<AccountSMR> Account { get; set; }
        [XmlElement(ElementName = "indOcc", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string IndOcc { get; set; }
        [XmlElement(ElementName = "abn", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Abn { get; set; }
        [XmlElement(ElementName = "acn", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Acn { get; set; }
        [XmlElement(ElementName = "arbn", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Arbn { get; set; }
        [XmlElement(ElementName = "businessDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public BusinessDetails BusinessDetails { get; set; }
        [XmlElement(ElementName = "individualDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public IndividualDetailsSMR IndividualDetails { get; set; }
        [XmlElement(ElementName = "identification", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<IdentificationSMR> Identification { get; set; }
        [XmlElement(ElementName = "electDataSrc", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> ElectDataSrc { get; set; }
        [XmlElement(ElementName = "partyIsCustomer", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PartyIsCustomer { get; set; }
        [XmlElement(ElementName = "partyIsAgent", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PartyIsAgent { get; set; }
        [XmlElement(ElementName = "relationship", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Relationship { get; set; }
        [XmlElement(ElementName = "evidence", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Evidence { get; set; }
    }

    [XmlRoot(ElementName = "unidentPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class UnidentPerson
    {
        [XmlElement(ElementName = "descOfPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string DescOfPerson { get; set; }
        [XmlElement(ElementName = "descOfDocs", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> DescOfDocs { get; set; }
    }

    [XmlRoot(ElementName = "senderDrawerIssuer", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class SenderDrawerIssuer
    {
        [XmlElement(ElementName = "sameAsSuspPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SameAsSuspPerson { get; set; }
        [XmlElement(ElementName = "sameAsOtherPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SameAsOtherPerson { get; set; }
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "postalAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PostalAddress { get; set; }
        [XmlElement(ElementName = "phone", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "email", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Email { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Account { get; set; }
    }

    [XmlRoot(ElementName = "payee", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class PayeeSMR
    {
        [XmlElement(ElementName = "sameAsSuspPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SameAsSuspPerson { get; set; }
        [XmlElement(ElementName = "sameAsOtherPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SameAsOtherPerson { get; set; }
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "postalAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PostalAddress { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Account { get; set; }
    }

    [XmlRoot(ElementName = "beneficiary", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class BeneficiarySMR
    {
        [XmlElement(ElementName = "sameAsSuspPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SameAsSuspPerson { get; set; }
        [XmlElement(ElementName = "sameAsOtherPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string SameAsOtherPerson { get; set; }
        [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string MainAddress { get; set; }
        [XmlElement(ElementName = "postalAddress", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PostalAddress { get; set; }
        [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> Account { get; set; }
    }

    [XmlRoot(ElementName = "txnDetail", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class TxnDetail
    {
        [XmlElement(ElementName = "txnDate", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "txnType", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string TxnType { get; set; }
        [XmlElement(ElementName = "tfrType", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string TfrType { get; set; }
        [XmlElement(ElementName = "txnCompleted", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string TxnCompleted { get; set; }
        [XmlElement(ElementName = "txnRefNo", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> TxnRefNo { get; set; }
        [XmlElement(ElementName = "txnAmount", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string TxnAmount { get; set; }
        [XmlElement(ElementName = "cashAmount", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string CashAmount { get; set; }
        [XmlElement(ElementName = "foreignCurr", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> ForeignCurr { get; set; }
        [XmlElement(ElementName = "senderDrawerIssuer", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<SenderDrawerIssuer> SenderDrawerIssuer { get; set; }
        [XmlElement(ElementName = "sendingInstitution", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> SendingInstitution { get; set; }
        [XmlElement(ElementName = "payee", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<PayeeSMR> Payee { get; set; }
        [XmlElement(ElementName = "beneficiary", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<BeneficiarySMR> Beneficiary { get; set; }
        [XmlElement(ElementName = "receivingInstitution", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> ReceivingInstitution { get; set; }
    }

    [XmlRoot(ElementName = "otherInstitutions", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class OtherInstitutions
    {
        [XmlElement(ElementName = "involvedInstn", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<string> InvolvedInstn { get; set; }
    }

    [XmlRoot(ElementName = "prevReported", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class PrevReported
    {
        [XmlElement(ElementName = "prevReportDate", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PrevReportDate { get; set; }
        [XmlElement(ElementName = "prevReportRef", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string PrevReportRef { get; set; }
    }

    [XmlRoot(ElementName = "otherAusGov", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class OtherAusGov
    {
        [XmlElement(ElementName = "name", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Name { get; set; }
        [XmlElement(ElementName = "address", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Address { get; set; }
        [XmlElement(ElementName = "dateReported", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string DateReported { get; set; }
        [XmlElement(ElementName = "infoProvided", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string InfoProvided { get; set; }
    }

    [XmlRoot(ElementName = "additionalDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class AdditionalDetailsSMR
    {
        [XmlElement(ElementName = "offence", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public string Offence { get; set; }
        [XmlElement(ElementName = "prevReported", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<PrevReported> PrevReported { get; set; }
        [XmlElement(ElementName = "otherAusGov", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<OtherAusGov> OtherAusGov { get; set; }
    }

    [XmlRoot(ElementName = "smr", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
    public class Smr
    {
        [XmlElement(ElementName = "header", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public HeaderSMR Header { get; set; }
        [XmlElement(ElementName = "smDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public SmDetails SmDetails { get; set; }
        [XmlElement(ElementName = "suspGrounds", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public SuspGrounds SuspGrounds { get; set; }
        [XmlElement(ElementName = "suspPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<SuspPerson> SuspPerson { get; set; }
        [XmlElement(ElementName = "otherPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<OtherPerson> OtherPerson { get; set; }
        [XmlElement(ElementName = "unidentPerson", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<UnidentPerson> UnidentPerson { get; set; }
        [XmlElement(ElementName = "txnDetail", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<TxnDetail> TxnDetail { get; set; }
        [XmlElement(ElementName = "otherInstitutions", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public List<OtherInstitutions> OtherInstitutions { get; set; }
        [XmlElement(ElementName = "additionalDetails", Namespace = "http://austrac.gov.au/schema/reporting/SMR-1")]
        public AdditionalDetailsSMR AdditionalDetails { get; set; }
    }


}
