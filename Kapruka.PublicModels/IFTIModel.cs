﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kapruka.PublicModels
{
   public class IFTIModel
    {

       [XmlRoot(ElementName = "ifti-draList", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
       public class IftidraList
       {
           [XmlElement(ElementName = "reNumber", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
           public string renumber { get; set; }
           [XmlElement(ElementName = "fileName", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
           public string FileName { get; set; }
           [XmlElement(ElementName = "reportCount", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
           public int ReportCount { get; set; }
           [XmlElement(ElementName = "ifti-dra", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
           public List<Iftidra> Iftidra { get; set; }

           [XmlAttribute(AttributeName = "versionMajor")]
           public string VersionMajor { get; set; }

           [XmlAttribute(AttributeName = "versionMinor")]
           public string VersionMinor { get; set; }
         
           public string xmlns { get; set; }
       }

        [XmlRoot(ElementName = "header", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Header
        {
            [XmlElement(ElementName = "txnRefNo", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string TxnRefNo { get; set; }
            [XmlElement(ElementName = "interceptFlag", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string InterceptFlag { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "currencyAmount", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class CurrencyAmount
        {
            [XmlElement(ElementName = "currency", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Currency { get; set; }
            [XmlElement(ElementName = "amount", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Amount { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "tfrType", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class TfrType
        {
            [XmlElement(ElementName = "type", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Type { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "transaction", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Transaction
        {
            [XmlElement(ElementName = "txnDate", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string TxnDate { get; set; }
            [XmlElement(ElementName = "currencyAmount", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public CurrencyAmount CurrencyAmount { get; set; }
            [XmlElement(ElementName = "direction", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string direction { get; set; }
            [XmlElement(ElementName = "tfrType", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public TfrType TfrType { get; set; }
            [XmlElement(ElementName = "valueDate", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string ValueDate { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class MainAddress
        {
            [XmlElement(ElementName = "addr", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Addr { get; set; }
            [XmlElement(ElementName = "suburb", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Suburb { get; set; }
            [XmlElement(ElementName = "state", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string State { get; set; }
            [XmlElement(ElementName = "postcode", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Postcode { get; set; }
            [XmlElement(ElementName = "country", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Country { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "identification", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Identification
        {
            [XmlElement(ElementName = "type", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Type { get; set; }
            [XmlElement(ElementName = "number", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Number { get; set; }
            [XmlElement(ElementName = "issuer", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Issuer { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "transferor", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Transferor
        {
            [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string FullName { get; set; }
            [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public MainAddress MainAddress { get; set; }
            [XmlElement(ElementName = "phone", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Phone { get; set; }
            [XmlElement(ElementName = "email", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Email { get; set; }
            [XmlElement(ElementName = "custNo", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string CustNo { get; set; }
            [XmlElement(ElementName = "dob", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Dob { get; set; }
            [XmlElement(ElementName = "identification", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public Identification Identification { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "branch", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Branch
        {
            [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string FullName { get; set; }
            [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public MainAddress MainAddress { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "orderingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class OrderingInstn
        {
            [XmlElement(ElementName = "branch", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public Branch Branch { get; set; }
            [XmlElement(ElementName = "foreignBased", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string ForeignBased { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "initiatingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class InitiatingInstn
        {
            [XmlElement(ElementName = "sameAsOrderingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string SameAsOrderingInstn { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
            [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string FullName { get; set; }
            [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public MainAddress MainAddress { get; set; }
            
        }

        [XmlRoot(ElementName = "sendingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class SendingInstn
        {
            [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string FullName { get; set; }
            [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public MainAddress MainAddress { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "receivingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class ReceivingInstn
        {
            [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string FullName { get; set; }
            [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public MainAddress MainAddress { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "beneficiaryInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class BeneficiaryInstn
        {
            [XmlElement(ElementName = "sameAsReceivingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string SameAsReceivingInstn { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Account
        {
            [XmlElement(ElementName = "acctNumber", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string AcctNumber { get; set; }
            [XmlElement(ElementName = "name", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Name { get; set; }
            [XmlElement(ElementName = "city", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string City { get; set; }
            [XmlElement(ElementName = "country", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Country { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "transferee", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Transferee
        {
            [XmlElement(ElementName = "fullName", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string FullName { get; set; }
            [XmlElement(ElementName = "mainAddress", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public MainAddress MainAddress { get; set; }
            [XmlElement(ElementName = "phone", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string Phone { get; set; }
            [XmlElement(ElementName = "account", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public Account Account { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "additionalDetails", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class AdditionalDetails
        {
            [XmlElement(ElementName = "reasonForTransfer", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public string reasonForTransfer { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "ifti-dra", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
        public class Iftidra
        {
            [XmlElement(ElementName = "header", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public Header Header { get; set; }
            [XmlElement(ElementName = "transaction", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public Transaction Transaction { get; set; }
            [XmlElement(ElementName = "transferor", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public Transferor Transferor { get; set; }
            [XmlElement(ElementName = "orderingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public OrderingInstn OrderingInstn { get; set; }
            [XmlElement(ElementName = "initiatingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public InitiatingInstn InitiatingInstn { get; set; }
            [XmlElement(ElementName = "sendingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public SendingInstn SendingInstn { get; set; }
            [XmlElement(ElementName = "receivingInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public ReceivingInstn ReceivingInstn { get; set; }
            [XmlElement(ElementName = "beneficiaryInstn", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public BeneficiaryInstn BeneficiaryInstn { get; set; }
            [XmlElement(ElementName = "transferee", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public Transferee Transferee { get; set; }
            [XmlElement(ElementName = "additionalDetails", Namespace = "http://austrac.gov.au/schema/reporting/IFTI-DRA-1")]
            public AdditionalDetails AdditionalDetails { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

    }
}
