﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kapruka.PublicModels
{
    [XmlRoot(ElementName = "ifti-eList", Namespace = "xs")]
    public class IftieList
    {
        [XmlElement(ElementName = "reNumber", Namespace = "xs")]
        public string ReNumber { get; set; }

        [XmlElement(ElementName = "fileName", Namespace = "xs")]
        public string FileName { get; set; }

        [XmlElement(ElementName = "reportCount", Namespace = "xs")]
        public string ReportCount { get; set; }

        [XmlElement(ElementName = "structured", Namespace = "xs")]
        public List<Structured> Structured { get; set; }

        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }

        [XmlAttribute(AttributeName = "ns2", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns2 { get; set; }

        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }

        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }

        [XmlAttribute(AttributeName = "versionMajor")]
        public string VersionMajor { get; set; }

        [XmlAttribute(AttributeName = "versionMinor")]
        public string VersionMinor { get; set; }
    }

    [XmlRoot(ElementName = "header", Namespace = "xs")]
    public class HeaderIFT
    {
        [XmlElement(ElementName = "txnRefNo", Namespace = "xs")]
        public string TxnRefNo { get; set; }
        [XmlElement(ElementName = "interceptFlag", Namespace = "xs")]
        public string InterceptFlag { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "currencyAmount", Namespace = "xs")]
    public class CurrencyAmount
    {
        [XmlElement(ElementName = "currency", Namespace = "xs")]
        public string Currency { get; set; }
        [XmlElement(ElementName = "amount", Namespace = "xs")]
        public string Amount { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "transaction", Namespace = "xs")]
    public class TransactionIFT
    {
        [XmlElement(ElementName = "txnDate", Namespace = "xs")]
        public string txnDate { get; set; }
        [XmlElement(ElementName = "transferDate", Namespace = "xs")]
        public string TransferDate { get; set; }
        [XmlElement(ElementName = "direction", Namespace = "xs")]
        public string Direction { get; set; }
        [XmlElement(ElementName = "currencyAmount", Namespace = "xs")]
        public CurrencyAmount CurrencyAmount { get; set; }
        [XmlElement(ElementName = "valueDate", Namespace = "xs")]
        public string ValueDate { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "mainAddress", Namespace = "xs")]
    public class MainAddress
    {
        [XmlElement(ElementName = "addr", Namespace = "xs")]
        public string Addr { get; set; }
        [XmlElement(ElementName = "suburb", Namespace = "xs")]
        public string Suburb { get; set; }
        [XmlElement(ElementName = "state", Namespace = "xs")]
        public string State { get; set; }
        [XmlElement(ElementName = "postcode", Namespace = "xs")]
        public string Postcode { get; set; }
        [XmlElement(ElementName = "country", Namespace = "xs")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "account", Namespace = "xs")]
    public class AccountIFT
    {
        [XmlElement(ElementName = "bsb", Namespace = "xs")]
        public string Bsb { get; set; }
        [XmlElement(ElementName = "number", Namespace = "xs")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "identification", Namespace = "xs")]
    public class IdentificationIFT
    {
        [XmlElement(ElementName = "type", Namespace = "xs")]
        public string Type { get; set; }
        [XmlElement(ElementName = "number", Namespace = "xs")]
        public string Number { get; set; }
        [XmlElement(ElementName = "issuer", Namespace = "xs")]
        public string Issuer { get; set; }
        [XmlElement(ElementName = "country", Namespace = "xs")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "placeOfBirth", Namespace = "xs")]
    public class PlaceOfBirth
    {
        [XmlElement(ElementName = "town", Namespace = "xs")]
        public string Town { get; set; }
        [XmlElement(ElementName = "country", Namespace = "xs")]
        public string Country { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "individualDetails", Namespace = "xs")]
    public class IndividualDetailsIFT
    {
        [XmlElement(ElementName = "dob", Namespace = "xs")]
        public string Dob { get; set; }
        [XmlElement(ElementName = "placeOfBirth", Namespace = "xs")]
        public PlaceOfBirth PlaceOfBirth { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "payer", Namespace = "xs")]
    public class PayerIFT
    {
        [XmlElement(ElementName = "fullName", Namespace = "xs")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "xs")]
        public MainAddress MainAddress { get; set; }
        [XmlElement(ElementName = "account", Namespace = "xs")]
        public List<AccountIFT> Account { get; set; }
        [XmlElement(ElementName = "abn", Namespace = "xs")]
        public string Abn { get; set; }
        [XmlElement(ElementName = "acn", Namespace = "xs")]
        public string Acn { get; set; }
        [XmlElement(ElementName = "arbn", Namespace = "xs")]
        public string Arbn { get; set; }
        [XmlElement(ElementName = "identification", Namespace = "xs")]
        public List<IdentificationIFT> Identification { get; set; }
        [XmlElement(ElementName = "custNo", Namespace = "xs")]
        public string CustNo { get; set; }
        [XmlElement(ElementName = "individualDetails", Namespace = "xs")]
        public IndividualDetailsIFT IndividualDetails { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "payee", Namespace = "xs")]
    public class PayeeIFT
    {
        [XmlElement(ElementName = "fullName", Namespace = "xs")]
        public string FullName { get; set; }
        [XmlElement(ElementName = "mainAddress", Namespace = "xs")]
        public MainAddress MainAddress { get; set; }
        [XmlElement(ElementName = "account", Namespace = "xs")]
        public List<AccountIFT> Account { get; set; }
        [XmlElement(ElementName = "identification", Namespace = "xs")]
        public List<IdentificationIFT> Identification { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "institution", Namespace = "xs")]
    public class Institution
    {
        [XmlElement(ElementName = "code", Namespace = "xs")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "branch", Namespace = "xs")]
    public class Branch
    {
        [XmlElement(ElementName = "bsb", Namespace = "xs")]
        public string Bsb { get; set; }
        [XmlElement(ElementName = "name", Namespace = "xs")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "orderingInstn", Namespace = "xs")]
    public class OrderingInstn
    {
        [XmlElement(ElementName = "institution", Namespace = "xs")]
        public Institution Institution { get; set; }
        [XmlElement(ElementName = "branch", Namespace = "xs")]
        public Branch Branch { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "sendingInstn", Namespace = "xs")]
    public class SendingInstn
    {
        [XmlElement(ElementName = "institution", Namespace = "xs")]
        public Institution Institution { get; set; }
        [XmlElement(ElementName = "branch", Namespace = "xs")]
        public Branch Branch { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "sendersCorrespondent", Namespace = "xs")]
    public class SendersCorrespondent
    {
        [XmlElement(ElementName = "institution", Namespace = "xs")]
        public Institution Institution { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "beneficiaryInstn", Namespace = "xs")]
    public class BeneficiaryInstn
    {
        [XmlElement(ElementName = "institution", Namespace = "xs")]
        public Institution Institution { get; set; }
        [XmlElement(ElementName = "branch", Namespace = "xs")]
        public Branch Branch { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "receivingInstn", Namespace = "xs")]
    public class ReceivingInstn
    {
        [XmlElement(ElementName = "institution", Namespace = "xs")]
        public Institution Institution { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "receiversCorrespondent", Namespace = "xs")]
    public class ReceiversCorrespondent
    {
        [XmlElement(ElementName = "institution", Namespace = "xs")]
        public Institution Institution { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "benefInstnAcct", Namespace = "xs")]
    public class BenefInstnAcct
    {
        [XmlElement(ElementName = "institution", Namespace = "xs")]
        public Institution Institution { get; set; }
        [XmlElement(ElementName = "account", Namespace = "xs")]
        public AccountIFT Account { get; set; }
    }

    [XmlRoot(ElementName = "additionalDetails", Namespace = "xs")]
    public class AdditionalDetailsIFT
    {
        [XmlElement(ElementName = "detailsOfPayment", Namespace = "xs")]
        public string DetailsOfPayment { get; set; }
        [XmlElement(ElementName = "otherDetails", Namespace = "xs")]
        public string OtherDetails { get; set; }
        [XmlElement(ElementName = "benefInstnAcct", Namespace = "xs")]
        public BenefInstnAcct BenefInstnAcct { get; set; }
        [XmlElement(ElementName = "senderToReceiverInfo", Namespace = "xs")]
        public List<string> SenderToReceiverInfo { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "structured", Namespace = "xs")]
    public class Structured
    {
        [XmlElement(ElementName = "header", Namespace = "xs")]
        public HeaderIFT Header { get; set; }

        [XmlElement(ElementName = "transaction", Namespace = "xs")]
        public TransactionIFT Transaction { get; set; }

        [XmlElement(ElementName = "payer", Namespace = "xs")]
        public List<PayerIFT> Payer { get; set; }

        [XmlElement(ElementName = "payee", Namespace = "xs")]
        public List<PayeeIFT> Payee { get; set; }

        [XmlElement(ElementName = "orderingInstn", Namespace = "xs")]
        public OrderingInstn OrderingInstn { get; set; }

        [XmlElement(ElementName = "sendingInstn", Namespace = "xs")]
        public SendingInstn SendingInstn { get; set; }

        [XmlElement(ElementName = "sendersCorrespondent", Namespace = "xs")]
        public List<SendersCorrespondent> SendersCorrespondent { get; set; }

        [XmlElement(ElementName = "beneficiaryInstn", Namespace = "xs")]
        public BeneficiaryInstn BeneficiaryInstn { get; set; }

        [XmlElement(ElementName = "receivingInstn", Namespace = "xs")]
        public ReceivingInstn ReceivingInstn { get; set; }

        [XmlElement(ElementName = "receiversCorrespondent", Namespace = "xs")]
        public List<ReceiversCorrespondent> ReceiversCorrespondent { get; set; }

        [XmlElement(ElementName = "additionalDetails", Namespace = "xs")]
        public AdditionalDetailsIFT AdditionalDetails { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }
}
