﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.PublicModels
{
   public class ResponseResult
    {
        public string Message { get; set; }
        public int IsSuccess { get; set; }
    }
}
