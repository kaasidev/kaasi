﻿using Kapruka.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Data
{
    public class CountryCurrencyHandlerData
    {

        public IList<CountryCurrencyApi> GetCountryCurrency()
        {
            var listObj = new List<CountryCurrencyApi>();
            var query = @"Select CurrencyID,CurrencyName ,CurrencyCode from Currencies";
            try
            {
                var customerData = DbConnectionKASI.FillDataSet(query);
                var dataTable = customerData.Tables[0];
                foreach (DataRow row in dataTable.Rows)
                {
                    var obj = new CountryCurrencyApi();
                    obj.CurrencyID = Convert.ToInt32(row["CurrencyID"].ToString());
                    if (row["CurrencyName"] != DBNull.Value)
                        obj.CurrencyName = row["CurrencyName"].ToString();
                    if (row["CurrencyCode"] != DBNull.Value)
                        obj.CurrencyCode = row["CurrencyCode"].ToString();
                    listObj.Add(obj);

                }
            }
            catch (Exception ex)
            {
                return listObj;
            }

            return listObj;
        }


        public void UpdateMasterCompanyBuyRates(IList<FilteredCurrency> listFitler)
        {

            string SqlStmt = @"update MasterCompanyBuyRates SET BuyingRate = @buyingRate,
                          BuyRateDate=CURRENT_TIMESTAMP, AmendedDateTime=CURRENT_TIMESTAMP where CurrencyID=@currencyId";

            foreach (var item in listFitler)
            {
                try
                {
                    SqlParameter[] parameter = {new SqlParameter("@currencyId",item.CurrencyID), 
                                       new SqlParameter("@buyingRate",item.CurrencValue)};

                    DbConnectionKASI.ExecuteNonQuery(SqlStmt, parameter);
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void UpdateMasterCompanyTransferBuyRates(IList<FilteredCurrency> listFitler)
        {

            string SqlStmt = @"update MasterCompanyBuyRates SET TranfertoBuyRate = @buyingRate,
                          TranfertoBuyRateModifiedDate=CURRENT_TIMESTAMP where CurrencyID=@currencyId";

            foreach (var item in listFitler)
            {
                try
                {
                    SqlParameter[] parameter = {new SqlParameter("@currencyId",item.CurrencyID), 
                                       new SqlParameter("@buyingRate",item.CurrencValue)};

                    DbConnectionKASI.ExecuteNonQuery(SqlStmt, parameter);
                }
                catch (Exception ex)
                {

                }
            }
        }


        public IList<MasterCompanyRateMarginsCurrency> GetMasterCompanyRateMargins()
        {
            var listObj = new List<MasterCompanyRateMarginsCurrency>();
            var query = @"Select CurrencyID,Margin from MasterCompanyRateMargins";
            try
            {
                var customerData = DbConnectionKASI.FillDataSet(query);
                var dataTable = customerData.Tables[0];
                foreach (DataRow row in dataTable.Rows)
                {
                    var obj = new MasterCompanyRateMarginsCurrency();
                    obj.CurrencyID = Convert.ToInt32(row["CurrencyID"].ToString());
                    if (row["Margin"] != DBNull.Value)
                        obj.Margin = row["Margin"].ToString();

                    listObj.Add(obj);

                }
            }
            catch (Exception ex)
            {
                return listObj;
            }

            return listObj;
        }

        public IList<AgentForRateMarginUpdate> GetAgentsByCommissionStructure(string commissionStructre)
        {
            var listObj = new List<AgentForRateMarginUpdate>();
            var query = @"Select AgentID,AgentName from Agents 
                     where AgentCommissionStructure=@agentCommissionStructure";
            try
            {
                SqlParameter[] parameter = { new SqlParameter("@agentCommissionStructure", commissionStructre) };

                var result = DbConnectionKASI.FillDataSet(query, parameter);
                var dataTable = result.Tables[0];
                foreach (DataRow row in dataTable.Rows)
                {
                    var obj = new AgentForRateMarginUpdate();
                    obj.AgentId = Convert.ToInt32(row["AgentID"].ToString());
                    if (row["AgentName"] != DBNull.Value)
                        obj.AgentName = row["AgentName"].ToString();

                    listObj.Add(obj);

                }
            }
            catch (Exception ex)
            {
                return listObj;
            }

            return listObj;
        }

        protected List<AgentCurrencyMarginsCurrency> GetAgentCurrencyMargins
            (IList<AgentForRateMarginUpdate> listAgents)
        {

            var listObj = new List<AgentCurrencyMarginsCurrency>();
            var query = @"Select CurrencyID,Margin,AgentID from AgentCurrencyMargins where AgentID=@agentId";
            try
            {
                foreach (var item in listAgents)
                {
                    SqlParameter[] parameter = { new SqlParameter("@agentId", item.AgentId) };

                    var result = DbConnectionKASI.FillDataSet(query, parameter);
                    var dataTable = result.Tables[0];
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var obj = new AgentCurrencyMarginsCurrency();
                        obj.AgentID = Convert.ToInt32(row["AgentID"].ToString());
                        obj.CurrencyID = Convert.ToInt32(row["CurrencyID"].ToString());
                        if (row["Margin"] != DBNull.Value)
                            obj.Margin = row["Margin"].ToString();

                        listObj.Add(obj);

                    }
                }
            }
            catch (Exception ex)
            {
                return listObj;
            }

            return listObj;
        }

        public void UpdateAgentCurrencyRateAndAgentSetRate(IList<AgentForRateMarginUpdate> listAgents,
            IList<MasterCompanyRateMarginsCurrency> masterCompanyRateMarginCurrency,
            IList<FilteredCurrency> masterCompanyBuyRates)
        {
            var agentsCurrencyMargin = GetAgentCurrencyMargins(listAgents);  // agent and marin --AgentCurrencyMargins
            var agentsForUpdateFromAgentCurrencyRates = GetAgentCurrencyRatesForUpdate(listAgents);// agents and currencyids


            string SqlStmt = @"update AgentCurrencyRates SET AssignedRate = @assignedRate,
                          AssignedDate=CURRENT_TIMESTAMP, AmendedDateTime=CURRENT_TIMESTAMP 
                         where CurrencyID=@currencyId and AgentID=@agentId";

            foreach (var item in listAgents)  // agents by agentCommissionStructure FXGAIN
            {
                var currencyIdsByAgent = (from agCr
                                      in agentsForUpdateFromAgentCurrencyRates
                                          where agCr.AgentID == item.AgentId
                                          select agCr).ToList();                          // gettting currencyid and agentid
                foreach (var agCrrencyId in currencyIdsByAgent)
                {
                    try
                    {
                        UpdateAgentAssignedAgentCurrencyRate(masterCompanyRateMarginCurrency,
                            masterCompanyBuyRates, SqlStmt, item, agCrrencyId, agentsCurrencyMargin); // updating agentcurrencyrates and agent rates
                    }
                    catch (Exception ex)
                    {
                        continue;

                    }
                }
            }
        }

        private static void UpdateAgentAssignedAgentCurrencyRate(IList<MasterCompanyRateMarginsCurrency>
            masterCompanyRateMarginCurrency, IList<FilteredCurrency> masterCompanyBuyRates,
            string SqlStmt, AgentForRateMarginUpdate item, AgentCurrencyRatesCurrency agCrrencyId,
            List<AgentCurrencyMarginsCurrency> agentCurrencyMargin)
        {
            var marginBYCurrencyIdFromMasterRate = (from maCurrency in masterCompanyRateMarginCurrency
                                                    where maCurrency.CurrencyID == agCrrencyId.CurrencyID
                                                    select maCurrency).ToList(); // maring from mastercompany rates by currency
            if (marginBYCurrencyIdFromMasterRate.Count() >0)
            {
                var firstMasterCurrency=marginBYCurrencyIdFromMasterRate[0];
                var feedValueByCurrencyId = (from mas in masterCompanyBuyRates         // currency rate from companybuyrates , the value from API
                                             where mas.CurrencyID == agCrrencyId.CurrencyID
                                             select mas).ToList();
                if (feedValueByCurrencyId.Count() > 0)
                {
                    var ofirst = feedValueByCurrencyId[0];
                    var assignedValue = Convert.ToDecimal(ofirst.CurrencValue)
                          - Convert.ToDecimal(firstMasterCurrency.Margin);  // reduce the marin 

                    SqlParameter[] parameter = {new SqlParameter("@currencyId",agCrrencyId.CurrencyID), 
                                                      new SqlParameter("@assignedRate",assignedValue),
                                       new SqlParameter("@agentId",item.AgentId)};

                    DbConnectionKASI.ExecuteNonQuery(SqlStmt, parameter);
                    UpdateAgenteSetRateFromAgentCurrencyRatesAssignedRate(assignedValue,
                        item.AgentId, agCrrencyId.CurrencyID, agentCurrencyMargin);
                }
            }
        }

        private static void UpdateAgenteSetRateFromAgentCurrencyRatesAssignedRate
            (decimal agentAssignedRate, int agentId,
            int currencyId, List<AgentCurrencyMarginsCurrency> agentCurrencyMargin)
        {

            string SqlStmtAgentSetRates = @"update AgentSetRates SET SetRate = @setRate,
                          SetDate=CURRENT_TIMESTAMP, AlteredDateTime=CURRENT_TIMESTAMP 
                         where CurrencyID=@currencyId and AgentID=@agentId";

            var agentCurrencyMarginFromAgentCurrencyMargins = (from maCurrency in agentCurrencyMargin
                                                               where maCurrency.CurrencyID == currencyId
                                                                && maCurrency.AgentID == agentId
                                                               select maCurrency).ToList();
            if (agentCurrencyMarginFromAgentCurrencyMargins.Count() > 0)
            {

                var FirstObj = agentCurrencyMarginFromAgentCurrencyMargins[0];
                var setRate = agentAssignedRate - Convert.ToDecimal(FirstObj.Margin);
                SqlParameter[] parameter = {new SqlParameter("@currencyId",currencyId), 
                                                      new SqlParameter("@setRate",setRate),
                                       new SqlParameter("@agentId",agentId)};
                DbConnectionKASI.ExecuteNonQuery(SqlStmtAgentSetRates, parameter);



            }
        }

        public IList<AgentCurrencyRatesCurrency> GetAgentCurrencyRatesForUpdate
            (IList<AgentForRateMarginUpdate> listAgents)
        {

            var listObj = new List<AgentCurrencyRatesCurrency>();
            var query = @"Select CurrencyID,AgentID from AgentCurrencyRates where AgentID=@agentId";
            try
            {
                foreach (var item in listAgents)
                {
                    SqlParameter[] parameter = { new SqlParameter("@agentId", item.AgentId) };

                    var result = DbConnectionKASI.FillDataSet(query, parameter);
                    var dataTable = result.Tables[0];
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var obj = new AgentCurrencyRatesCurrency();
                        obj.AgentID = Convert.ToInt32(row["AgentID"].ToString());
                        obj.CurrencyID = Convert.ToInt32(row["CurrencyID"].ToString());

                        listObj.Add(obj);

                    }
                }
            }
            catch (Exception ex)
            {
                return listObj;
            }

            return listObj;
        }

    }
}
