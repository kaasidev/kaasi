﻿using Kapruka.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kapruka.Data
{
    public class CustomerHandlerData
    {

        public bool UpdateCustomerByKASIWEBAPIData(CustomerOrMerchantResponseKyced csObj,
            int csId, int recordId, string calledUser, bool cancallAudit = true)
        {
            var suditKycCheck = new KYCAuditIDRun();
            suditKycCheck.CustomerID = csId;
            suditKycCheck.RunBy = calledUser;
            suditKycCheck.Result = csObj.resultDescription;
            suditKycCheck.agentTransactionID = csObj.transactionId;
            if (cancallAudit)
                InsertKCYAuditRecord(suditKycCheck);

            string SqlStmt = @"update Customers SET agentTransactionID = @agentTransactionID,resultDescription = @resultDescription, 
                           TransactionID = @transactionID,resultCode = @resultCode,
                          HasKYC=@hasRec, RiskLevel=@riskLevel , KYCCheckDate=CURRENT_TIMESTAMP, KYCBy=@kycby where RecordID=@recordID";

            var cutomerInKyce = (int)CustomerINKYCE.Yes;
            var riskLevel = 1;

            if (csObj.agentTransactionId == null)
                csObj.agentTransactionId = "";
            if (csObj.resultCode == null)
                csObj.resultCode = "";
            if (csObj.resultDescription == null)
                csObj.resultDescription = "";

            SqlParameter[] parameter = {new SqlParameter("@agentTransactionID",csObj.agentTransactionId),
                                        new SqlParameter("@resultDescription",csObj.resultDescription),
                                        new SqlParameter("@transactionID",csObj.transactionId),
                                        new SqlParameter("@resultCode",csObj.resultCode),
                                        new SqlParameter("@recordID",recordId),
                                          new SqlParameter("@riskLevel",riskLevel),
                                       new SqlParameter("@hasRec",cutomerInKyce),
                                        new SqlParameter("@kycby", calledUser)};

            return DbConnectionKASI.ExecuteNonQuery(SqlStmt, parameter);
        }

        public bool UpdateAgentByKASIWEBAPIData(CustomerOrMerchantResponseKyced csObj, int agentId, string calledUser)
        {
            var suditKycCheck = new KYCAuditIDRun();
            suditKycCheck.CustomerID = agentId;
            suditKycCheck.RunBy = calledUser;
            suditKycCheck.Result = csObj.resultDescription;
            suditKycCheck.agentTransactionID = csObj.transactionId;
            InsertKCYAuditRecord(suditKycCheck);

            string SqlStmt = @"update Agents SET agentTransactionID = @agentTransactionID,resultDescription = @resultDescription, 
                           TransactionID = @transactionID,resultCode = @resultCode,
                          HasKYC=@hasRec , KYCCheckDate=CURRENT_TIMESTAMP where AgentID=@agentId";

            var cutomerInKyce = (int)CustomerINKYCE.Yes;

            if (csObj.agentTransactionId == null)
                csObj.agentTransactionId = "";
            if (csObj.resultCode == null)
                csObj.resultCode = "";
            if (csObj.resultDescription == null)
                csObj.resultDescription = "";

            SqlParameter[] parameter = {new SqlParameter("@agentTransactionID",csObj.agentTransactionId),
                                        new SqlParameter("@resultDescription",csObj.resultDescription),
                                        new SqlParameter("@transactionID",csObj.transactionId),
                                        new SqlParameter("@resultCode",csObj.resultCode),
                                        new SqlParameter("@agentId",agentId),
                                       new SqlParameter("@hasRec",cutomerInKyce),
                                       new SqlParameter("@kycby", calledUser)};

            return DbConnectionKASI.ExecuteNonQuery(SqlStmt, parameter);
        }

        public bool InsertKCYAuditRecord(KYCAuditIDRun csObj)
        {
            var sqlStatem = @"INSERT INTO KYCAudit (CustomerID,KYCRunDate,agentTransactionID,Result,RunBy,OverrideNotes)
                             VALUES (@customerID,CURRENT_TIMESTAMP,@agentTransactionID,@result,@runBy,@overrideNotes)";

            if (csObj.agentTransactionID == null)
                csObj.agentTransactionID = "";
            if (csObj.Result == null)
                csObj.Result = "";
            if (csObj.OverrideNotes == null)
                csObj.OverrideNotes = "";

            SqlParameter[] parameter = {new SqlParameter("@agentTransactionID",csObj.agentTransactionID),
                                        new SqlParameter("@result",csObj.Result),
                                        new SqlParameter("@overrideNotes",csObj.OverrideNotes),
                                        new SqlParameter("@runBy",csObj.RunBy),
                                       new SqlParameter("@customerID",csObj.CustomerID)};

            return DbConnectionKASI.ExecuteNonQuery(sqlStatem, parameter);

        }

        public int GetCustomerRecordIdByCustomerId(int customerid)
        {
            var customerRecordId = 0;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = DbConnectionKASI.CONNECTION_STRING;

            String strQuery = "SELECT RecordID  FROM dbo.Customers where CustomerID=@customerid";
            String LastCustomerID = String.Empty;

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strQuery;
                cmd.Parameters.AddWithValue("customerid", customerid);
                cmd.Connection = conn;
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        customerRecordId = Convert.ToInt32(sdr["RecordID"].ToString());
                    }
                }
                conn.Close();
            }

            return customerRecordId;

        }

        public CustomerDocs GetCustomerDocs(int customerId)
        {
            var obj = new CustomerDocs();
            obj.CustomerID = customerId;
            var queryString = @"select TypeOfDocument,ImageAsBase64 from CustomerDocuments  where CustomerID=@customerId 
                          and TypeOfDocumentID= 6 and ImageAsBase64 IS NOT NULL ";
            SqlParameter[] parameter = { new SqlParameter("@customerId", customerId) };
            try
            {
                var customerData = DbConnectionKASI.FillDataSet(queryString, parameter);
                var dataTable = customerData.Tables[0];
                foreach (DataRow row in dataTable.Rows)
                {
                    if (row["ImageAsBase64"] != DBNull.Value)
                    {
                        obj.DocumentDriverImage1 = row["ImageAsBase64"].ToString();
                        obj.hasDriverLicence = true;
                        return obj;
                    }
                }
            }
            catch (Exception ex)
            {
                return obj;
            }
            return obj;
        }

        public List<CustomerDocs> GetCustomerAllDocs(int customerId)
        {
            var objlist = new List<CustomerDocs>();

            var queryString = @"select DocumentName,TypeOfDocument,TypeOfDocumentID,DocumentNumber,ExpiryDate,DocumentGUID
                           from CustomerDocuments  where CustomerID=@customerId  ";
            SqlParameter[] parameter = { new SqlParameter("@customerId", customerId) };
            try
            {
                var customerData = DbConnectionKASI.FillDataSet(queryString, parameter);
                var dataTable = customerData.Tables[0];
                foreach (DataRow row in dataTable.Rows)
                {
                    var obj = new CustomerDocs();
                    if (row["DocumentName"] != DBNull.Value)
                        obj.DocumentName = row["DocumentName"].ToString();
                    if (row["TypeOfDocument"] != DBNull.Value)
                        obj.TypeOfDocument = row["TypeOfDocument"].ToString();
                    if (row["TypeOfDocumentID"] != DBNull.Value)
                        obj.TypeOfDocumentID = row["TypeOfDocumentID"].ToString();
                    if (row["DocumentNumber"] != DBNull.Value)
                        obj.DocumentNumber = row["DocumentNumber"].ToString();
                    if (row["ExpiryDate"] != DBNull.Value)
                        obj.ExpiryDate = row["ExpiryDate"].ToString();
                    if (row["DocumentGUID"] != DBNull.Value)
                        obj.DocumentGUID = row["DocumentGUID"].ToString();

                    objlist.Add(obj);

                }
            }
            catch (Exception ex)
            {
                return objlist;
            }
            return objlist;
        }


        public List<CustomerDocs> GetCustomerAllDocsByDocumentNumber(string documentNumber)
        {
            var objlist = new List<CustomerDocs>();

            var queryString = @"select CustomerID,DocumentName,TypeOfDocument,TypeOfDocumentID,DocumentNumber,ExpiryDate
                           from CustomerDocuments  where DocumentNumber=@documentId  ";
            SqlParameter[] parameter = { new SqlParameter("@documentId", documentNumber) };
            try
            {
                var customerData = DbConnectionKASI.FillDataSet(queryString, parameter);
                var dataTable = customerData.Tables[0];
                foreach (DataRow row in dataTable.Rows)
                {
                    var obj = new CustomerDocs();
                    if (row["CustomerID"] != DBNull.Value)
                        obj.CustomerID = Convert.ToInt32(row["CustomerID"].ToString());
                    if (row["DocumentName"] != DBNull.Value)
                        obj.DocumentName = row["DocumentName"].ToString();
                    if (row["TypeOfDocument"] != DBNull.Value)
                        obj.TypeOfDocument = row["TypeOfDocument"].ToString();
                    if (row["TypeOfDocumentID"] != DBNull.Value)
                        obj.TypeOfDocumentID = row["TypeOfDocumentID"].ToString();
                    if (row["DocumentNumber"] != DBNull.Value)
                        obj.DocumentNumber = row["DocumentNumber"].ToString();
                    if (row["ExpiryDate"] != DBNull.Value)
                        obj.ExpiryDate = row["ExpiryDate"].ToString();

                    objlist.Add(obj);

                }
            }
            catch (Exception ex)
            {
                return objlist;
            }
            return objlist;
        }


        public CustomerForKyc GetCustomerById(int customerId)
        {
            var obj = new CustomerForKyc();
            obj.CustomerID = customerId;
            var queryString = @"select RecordID,Title,DOB,FirstName,LastName,Mobile,EmailAddress,Suburb,KYCBy,agentTransactionID,TransactionID,resultCode,
                                 CustomerType,ABN,BusinessName,TradingName,StreetName,Suburb,State,Postcode,Country,
                                 RiskLevel,HasKYC,KYCCheckDate,resultDescription,PicImage,IsIndividual from Customers  where CustomerID=@customerId";
            SqlParameter[] parameter = { new SqlParameter("@customerId", customerId) };
            var customerData = DbConnectionKASI.FillDataSet(queryString, parameter);

            var dataTable = customerData.Tables[0];
            foreach (DataRow row in dataTable.Rows)
            {
                if (row["RecordID"] != DBNull.Value)
                    obj.RecordID = Convert.ToInt32(row["RecordID"].ToString());
                if (row["Title"] != DBNull.Value)
                    obj.Title = row["Title"].ToString();
                if (row["DOB"] != DBNull.Value)
                    obj.DOB = row["DOB"].ToString();
                if (row["FirstName"] != DBNull.Value)
                    obj.FirstName = row["FirstName"].ToString();
                if (row["LastName"] != DBNull.Value)
                    obj.LastName = row["LastName"].ToString();
                if (row["Suburb"] != DBNull.Value)
                    obj.City = row["Suburb"].ToString();
                if (row["Mobile"] != DBNull.Value)
                    obj.Mobile = row["Mobile"].ToString();
                if (row["EmailAddress"] != DBNull.Value)
                    obj.EmailAddress = row["EmailAddress"].ToString();
                if (row["CustomerType"] != DBNull.Value)
                    obj.CustomerType = Convert.ToInt32(row["CustomerType"].ToString());
                if (row["ABN"] != DBNull.Value)
                    obj.ABN = row["ABN"].ToString();
                if (row["BusinessName"] != DBNull.Value)
                    obj.BusinessName = row["BusinessName"].ToString();
                if (row["TradingName"] != DBNull.Value)
                    obj.TradingName = row["TradingName"].ToString();
                if (row["StreetName"] != DBNull.Value)
                    obj.StreetName = row["StreetName"].ToString();
                if (row["Suburb"] != DBNull.Value)
                    obj.Suburb = row["Suburb"].ToString();
                if (row["State"] != DBNull.Value)
                    obj.State = row["State"].ToString();
                if (row["Postcode"] != DBNull.Value)
                    obj.Postcode = row["Postcode"].ToString();
                if (row["Country"] != DBNull.Value)
                    obj.Country = row["Country"].ToString();
                if (row["RiskLevel"] != DBNull.Value)
                    obj.RiskLevel = row["RiskLevel"].ToString();
                if (row["HasKYC"] != DBNull.Value)
                    obj.HasKYC = row["HasKYC"].ToString();
                if (row["resultDescription"] != DBNull.Value)
                    obj.KCYResult = row["resultDescription"].ToString();
                if (row["KYCCheckDate"] != DBNull.Value)
                    obj.KYCCheckDate = Convert.ToDateTime(row["KYCCheckDate"].ToString());
                else
                    obj.KYCCheckDate = DateTime.MinValue;
                if (row["KYCBy"] != DBNull.Value)
                    obj.KCYRunBy = (row["KYCBy"].ToString());
                if (row["agentTransactionID"] != DBNull.Value)
                    obj.agentTransactionID = (row["agentTransactionID"].ToString());
                if (row["resultDescription"] != DBNull.Value)
                    obj.resultDescription = (row["resultDescription"].ToString());
                if (row["TransactionID"] != DBNull.Value)
                    obj.TransactionID = (row["TransactionID"].ToString());
                if (row["resultCode"] != DBNull.Value)
                    obj.resultCode = (row["resultCode"].ToString());
                if (row["IsIndividual"] != DBNull.Value)
                    obj.IsIndividual = Convert.ToBoolean(row["IsIndividual"].ToString());
                //if (row["PicImage"] != DBNull.Value)
                // {
                // obj.FaceImage = (Byte[])(row["PicImage"]);

                // }
            }

            return obj;
        }



        public AgentKYC GetAgentById(int agentId)
        {
            var obj = new AgentKYC();
            obj.AgentID = agentId;
            var queryString = @"select AgentName,AgentAddress1,AgentCity,AgentState,AgentPostcode,AgentPhone,AgentFax,
                                 AgentEmail,AgentABN,AgentACN,AustracNumber from Agents  where AgentID=@agentId";
            SqlParameter[] parameter = { new SqlParameter("@agentId", agentId) };
            var customerData = DbConnectionKASI.FillDataSet(queryString, parameter);

            var dataTable = customerData.Tables[0];
            foreach (DataRow row in dataTable.Rows)
            {
                if (row["AgentName"] != DBNull.Value)
                    obj.AgentName = row["AgentName"].ToString();
                if (row["AgentAddress1"] != DBNull.Value)
                    obj.AgentAddress1 = row["AgentAddress1"].ToString();
                if (row["AgentCity"] != DBNull.Value)
                    obj.AgentCity = row["AgentCity"].ToString();
                if (row["AgentState"] != DBNull.Value)
                    obj.AgentState = row["AgentState"].ToString();
                if (row["AgentPostcode"] != DBNull.Value)
                    obj.AgentPostcode = row["AgentPostcode"].ToString();
                if (row["AgentPhone"] != DBNull.Value)
                    obj.AgentPhone = row["AgentPhone"].ToString();
                if (row["AgentFax"] != DBNull.Value)
                    obj.AgentFax = (row["AgentFax"].ToString());
                if (row["AgentABN"] != DBNull.Value)
                    obj.AgentABN = (row["AgentABN"].ToString());
                if (row["AgentEmail"] != DBNull.Value)
                    obj.AgentEmail = row["AgentEmail"].ToString();
                if (row["AgentACN"] != DBNull.Value)
                    obj.AgentACN = row["AgentACN"].ToString();
                if (row["AustracNumber"] != DBNull.Value)
                    obj.AustracNumber = row["AustracNumber"].ToString();
                //if (row["StreetName"] != DBNull.Value)
                //    obj.StreetName = row["StreetName"].ToString();
                //if (row["StreetName"] != DBNull.Value)
                //    obj.StreetName = row["StreetName"].ToString();
                //if (row["State"] != DBNull.Value)
                //    obj.State = row["State"].ToString();
                //if (row["Postcode"] != DBNull.Value)
                //    obj.Postcode = row["Postcode"].ToString();


            }

            return obj;
        }

    }



}
