﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kapruka.Data
{
    public class DbConnectionKASI
    {
        public SqlConnection Con;
        public SqlCommand Cmd;
        public SqlDataReader Dr;
        public DataSet Ds;
        SqlDataAdapter Add;
        SqlTransaction Tran;
        //public event  SqlRowsCopied(  Object  Sender,  System.Data.SqlClient.SqlRowsCopiedEventArgs e);
        public DbConnectionKASI()
        {
            Con = new SqlConnection();

            Con.ConnectionString = ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString;


            Con.Open();
        }

        public static string CONNECTION_STRING
        {
            get { return ConfigurationManager.ConnectionStrings["connKASISTRING"].ConnectionString; }
        }



        public void cancel()
        {
            Cmd.Cancel();
        }
        public void class1(string SqlCon)
        {
            Con = new SqlConnection();
            Con.ConnectionString = SqlCon;
            Con.Open();
        }
        public bool OPen()
        {
            if (Con.State != ConnectionState.Open)
            {
                try
                {
                    Con.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
                return true;

        }

        //public void Bulkcopy(string Destination, DataTable DataSource, int NotifyAfter)
        //{
        //    try
        //    {
        //        Bcp = new SqlBulkCopy(Con);
        //        Bcp.BulkCopyTimeout = 300;
        //        Bcp.DestinationTableName = Destination;
        //        Bcp.NotifyAfter = NotifyAfter;
        //        Bcp.WriteToServer(DataSource);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}


        public int MaxValue(string Table, string Column, string DefaultValue)
        {
            return (MaxValue(Table, Column, DefaultValue, ""));
        }

        public int MaxValue(string Table, string Column, string DefaultValue, string Condition)
        {
            return (Convert.ToInt32(ExecuteScalar("Select isnull(max(" + Column + ")," + DefaultValue + ") from " + Table + (Condition == "" ? "" : " Where ") + Condition)));


        }


        public DataRow FillRow(string Sqlstr)
        {
            CommandText(Sqlstr);
            SqlDataAdapter Ad = new SqlDataAdapter();
            Ad.SelectCommand = Cmd;
            DataSet LDs = new DataSet();
            Ad.Fill(LDs, "Table");
            if (LDs.Tables["Table"].Rows.Count > 0)
                return ((LDs.Tables["Table"].Rows[0]));
            else
                return (null);

        }
        public void FillDataSet(DataSet LDataSet, string Sqlstr, string TableName)
        {
            CommandText(Sqlstr);
            SqlDataAdapter Ad = new SqlDataAdapter();

            Ad.SelectCommand = Cmd;
            Ad.Fill(LDataSet, TableName);
        }
        public void ClearDataSet(DataSet LDataSet)
        {
            LDataSet.Clear();
        }
        public void BeginTrans()
        {
            Tran = Con.BeginTransaction();
        }
        public void CommitTrans()
        {
            Tran.Commit();
        }
        public void RollBackTrans()
        {
            Tran.Rollback();
        }
        public void CommandText(string SqlStr)
        {
            Cmd = new SqlCommand();
            Cmd.CommandText = SqlStr;
            Cmd.Connection = Con;
            Cmd.Transaction = Tran;
        }
        public static bool ExecuteNonQuery(string QueryString, params SqlParameter[] arrParam)
        {
            string qry = "";
            SqlConnection Con = new SqlConnection();


            Con.ConnectionString = CONNECTION_STRING;
            Con.Open();

            if (arrParam != null)
            {

                qry = QueryString;
                SqlCommand cmd = new SqlCommand(qry, Con);
                cmd.Parameters.AddRange(arrParam);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                Con.Close();

                return true;


            }


            return false;

        }

        public static string changeNameloop(int len)
        {

            System.Threading.Thread.Sleep(100);
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            //string _allowedChars = "0123456789";
            Random randNum = new Random();
            char[] chars = new char[len];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < len; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);

        }
        public int ExecuteNonQuery(string SqlStr)
        {
            CommandText(SqlStr);
            return (Cmd.ExecuteNonQuery());
        }

        public object ExecuteScalar(string SqlStr)
        {
            CommandText(SqlStr);
            return (Cmd.ExecuteScalar());

        }

        public int ExecuteLineReader(string SqlStr)
        {
            CommandText(SqlStr);
            Dr = Cmd.ExecuteReader();
            if (Dr.Read())
                return (1);
            else
                return (0);

        }

        public static DataSet FillDataSet(string sqlStr, params SqlParameter[] arrParam)
        {
            var ds = new DataSet();
            SqlConnection Con = new SqlConnection();
            Con.ConnectionString = CONNECTION_STRING;
            Con.Open();
            if (arrParam != null)
            {
                SqlCommand cmd = new SqlCommand(sqlStr, Con);
                cmd.Parameters.AddRange(arrParam);
                SqlDataAdapter Ad = new SqlDataAdapter(cmd);
                Ad.Fill(ds);
                Con.Close();
               
            }

            return ds;
        }

        public static DataSet FillDataSet(string sqlStr)
        {
            var ds = new DataSet();
            SqlConnection Con = new SqlConnection();
            Con.ConnectionString = CONNECTION_STRING;
            Con.Open();
            
                SqlCommand cmd = new SqlCommand(sqlStr, Con);
                
                SqlDataAdapter Ad = new SqlDataAdapter(cmd);
                Ad.Fill(ds);
                Con.Close();

           

            return ds;
        }

        public DataTable FillTable(string Sqlstr, string TableName)
        {
            CommandText(Sqlstr);
            SqlDataAdapter Ad = new SqlDataAdapter();
            Ad.SelectCommand = Cmd;
            Ds = new DataSet();
            Ad.Fill(Ds, TableName);
            return (Ds.Tables[TableName]);
        }
        public DataTable ExecuteProc(string ProcName, params     object[] Parameter)
        {
            CommandText(ProcName);
            Cmd.CommandType = CommandType.StoredProcedure;
            int i = 0;
            while (i < Parameter.Length)
            {
                Cmd.Parameters.Add(Cmd.CreateParameter());
                Cmd.Parameters[i].ParameterName = Convert.ToString(Parameter[i]);
                Cmd.Parameters[i].Value = Parameter[i * 2 + 1];
                i = i + 1;
            }
            SqlDataAdapter Ad = new SqlDataAdapter();
            Ad.SelectCommand = Cmd;
            Ds = new DataSet();
            Ad.Fill(Ds, ProcName);
            return (Ds.Tables[ProcName]);
        }

        public void CloseReader()
        {
            Dr.Close();
        }

        public void Close()
        {
            if (Con.State != ConnectionState.Closed)
                Con.Close();

        }
    }
}
